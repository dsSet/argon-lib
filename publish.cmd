@echo off

IF NOT "%1"=="" (
SET stage=1
GOTO :%1
)

ECHO ------------------------------------
ECHO stage 1: update package dependencies
ECHO ------------------------------------
call npm run deps:update || goto :error

:2
ECHO ------------------------------------
ECHO stage 2: bootstrap dependencies
ECHO ------------------------------------
call npm run deps:bootstrap || goto :error

:3
REM ECHO ------------------------------------
REM ECHO stage 3: build packages
REM ECHO ------------------------------------
REM call npm run deps:build || goto :error

:4
ECHO ------------------------------------
ECHO stage 4: run unit tests
ECHO ------------------------------------
call npm run deps:test || goto :error

:5
ECHO ------------------------------------
ECHO stage 5: update readme file
ECHO ------------------------------------
call npm run cli:update-readme || goto :error


:6
REM ECHO ------------------------------------
REM ECHO stage 6: run aot test
REM ECHO ------------------------------------
REM call npm run aot-test || goto :error

:7
ECHO ------------------------------------
ECHO stage 7: publish to npm registry
ECHO ------------------------------------
call npm run deps:publish || goto :error

goto :EOF

:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
