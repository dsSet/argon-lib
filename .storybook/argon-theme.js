import { create } from '@storybook/theming';

export default create({
  base: 'dark',

  colorPrimary: 'rgba(134, 21, 209, 0.95)',
  colorSecondary: 'rgba(134, 21, 209, 0.95)',

  // UI
  appBg: '#350b74',
  appContentBg: '#350b74',
  appBorderColor: 'rgba(134, 21, 209, 0.95)',
  appBorderRadius: 2,

  // Typography
  fontBase: '"Open Sans", sans-serif',
  fontCode: 'monospace',

  // Text colors
  textColor: '#ffffff',
  textInverseColor: 'rgba(134, 21, 209, 0.95)',

  // Toolbar default and active colors
  barTextColor: '#ffffff',
  barSelectedColor: 'rgba(134, 21, 209, 0.95)',
  barBg: '#350b74',

  // Form colors
  inputBg: 'rgba(21, 3, 32, 0.95)',
  inputBorder: 'rgba(134, 21, 209, 0.95)',
  inputTextColor: '#ffffff',
  inputBorderRadius: 4,

  brandTitle: 'Argon library',
  brandUrl: 'https://bitbucket.org/dsSet/argon-lib/src/master',
  brandImage: false
}, {
  background: {
    app: 'rgba(21, 3, 32, 0.95)'
  },
  addonNotesTheme: {
    color: '#2f321F',
    backgroundColor: '#ffffff'
  }
});
