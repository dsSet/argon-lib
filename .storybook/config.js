import { configure, addDecorator, addParameters } from '@storybook/angular';
import { withPosition } from '../tools/storybook-position/lib';
import argonTheme from './argon-theme';

addDecorator(withPosition);

addParameters({
  options: {
    theme: argonTheme,
    showPanel: false
  },
});

function loadStories() {
  const req = require.context('../projects', true, /^(?!.*?[\\\/]dist[\\\/]).*\.stories\.ts$/);
  req.keys().forEach(filename => req(filename));
}

configure(loadStories, module);
