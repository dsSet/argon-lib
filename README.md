# ArgonLib - UI patterns library

## Install and Run

// TODO: add information

## Publishing

// TODO: add information

## Known issues

### Build issue

Current ng-pakcagr create incorrect dependencies inside packages compiled code.
For example

````typescript
declare const BooleanControlShape_base: typeof UIShape & import("../../../../tools/dist/argon-tools").Constructor<import("../../../../tools/dist/argon-tools").UITabindexInterface> & import("../../../../tools/dist/argon-tools").Constructor<import("../../../../tools/dist/argon-tools").CompareValuesInterface>;
````

``import("../../../../tools/dist/argon-tools")`` should be absolute ``import("@argon/tools")``

For that reason root ts config contains ``"preserveSymlinks": true`` flag. In that case we will able to build packages with correct imports,
but still have an error in case with nested dependencies. For example in case with complex dependencies:

- TargetPackage -> PackageA
- TargetPackage -> PackageB
- PackageA -> PackageB

In this case wi will have nested node_modules folders linking by symlinks with lerna. 
In some cases angular builder will skip imports for second level.

Current solution: create symlink inside root node_modules folred for:

``node_modules/@argon <==> projects``

root `tsconfig.json` also contains path relative to `node_modules` folder. `tsconfig.spec.json` still should have path relative to the `packages` folder





   
