import { makeDecorator } from '@storybook/addons';
import { moduleMetadata } from '@storybook/angular';
import { WrapperComponent } from './components/WrapperComponent';

export const withPosition = makeDecorator({
  name: 'withPosition',
  skipIfNoParametersOrOptions: false,
  wrapper: (getStory, context, { parameters }) => {
    const metadataForMerge = moduleMetadata({
      declarations: [
        WrapperComponent
      ]
    });

    const content = metadataForMerge(getStory);

    if (content.template) {
      content.template = `<story-wrapper>${content.template}</story-wrapper>`;
    } else if (content.component) {
      const selector = content.component.__annotations__[0].selector;
      content.template = `<story-wrapper><${selector}></${selector}></story-wrapper>`;
    }

    return content;
  }
});
