import React from 'react';
import addons, { types } from '@storybook/addons';
import { ToolPanel } from './components/ToolPanel';
import { ADDON_ID, TOOLBAR_ID } from './constants';

import './styles.css';


addons.register(ADDON_ID, api => {
  const render = () => <ToolPanel api={api} />;

  addons.add(TOOLBAR_ID, {
    type: types.TOOL,
    title: "Story Position",
    render,
  });
});
