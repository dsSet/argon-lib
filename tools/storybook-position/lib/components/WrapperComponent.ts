import { ChangeDetectorRef, Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import addons from '@storybook/addons';
import { STORY_CHANGED } from '@storybook/core-events';
import { CHANGE_POSITION_ACTION } from '../actions.js';
import { defaultPosition } from './PositionPicker.js';

const channel = addons.getChannel();

interface WrapperPositionInterface {

  justifyContent?: string;

  alignItems?: string;

  display?: string;

}

@Component({
  selector: 'story-wrapper',
  template: `
    <div [ngStyle]="style">
      <div>
        <ng-content></ng-content>
      </div>
    </div>
  `
})
export class WrapperComponent implements OnInit, OnDestroy {

  public get style(): object {
    return {
      display: 'flex',
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      ...this.position
    };
  }

  public position: WrapperPositionInterface = defaultPosition;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private ngZone: NgZone
  ) { }

  ngOnInit(): void {
    channel.on(CHANGE_POSITION_ACTION, this.handleChangePosition);
    channel.on(STORY_CHANGED, this.handleStoryChanged);
  }

  ngOnDestroy(): void {
    channel.removeAllListeners(CHANGE_POSITION_ACTION);
    channel.removeAllListeners(STORY_CHANGED);
  }

  private handleStoryChanged = () => {
    this.handleChangePosition(defaultPosition);
  }

  private handleChangePosition = (position: WrapperPositionInterface) => {
    this.ngZone.run(() => {
      this.position = position;
    });
    // this.position = position;
    // this.changeDetector.markForCheck();
  }

}
