import React from 'react';

export const positions = [
  { justifyContent: 'flex-start', alignItems: 'flex-start' },
  { justifyContent: 'center', alignItems: 'flex-start' },
  { justifyContent: 'flex-end', alignItems: 'flex-start' },
  { justifyContent: 'flex-start', alignItems: 'center' },
  { justifyContent: 'center', alignItems: 'center' },
  { justifyContent: 'flex-end', alignItems: 'center' },
  { justifyContent: 'flex-start', alignItems: 'flex-end' },
  { justifyContent: 'center', alignItems: 'flex-end' },
  { justifyContent: 'flex-end', alignItems: 'flex-end' },
];

const disabledPosition = { display: 'block' };

export const defaultPosition = null;

const PositionItem = ({ isActive, item, onClick }) => (
  <div onClick={() => { onClick(item); }} className={isActive ? 'qlx-position-item qlx-position-item-active' : 'qlx-position-item'} />
)

export const PositionPicker = ({ position, onChange }) => (
  <>
    <div className="qlx-position-picker">
      {
        positions.map((item, key) => <PositionItem key={key} item={item} isActive={position === item} onClick={onChange} />)
      }
    </div>
    <div onClick={() => { onChange(disabledPosition); }}>Disable</div>
  </>
)
