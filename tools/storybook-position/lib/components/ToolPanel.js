import React from 'react';
import { Icons, IconButton, WithTooltip } from '@storybook/components';
import { STORY_CHANGED } from '@storybook/core-events';
import { defaultPosition, PositionPicker } from './PositionPicker';
import { CHANGE_POSITION_ACTION } from '../actions';

export class ToolPanel extends React.Component {

  state = {
    position: defaultPosition,
    isVisible: false
  };

  constructor(props) {
    super(props);
    this.state = { position: defaultPosition };
    const { api } = props;
    api.on(STORY_CHANGED, this.handleStoryChanged);
  }

  componentWillUnmount() {
    const { api } = this.props;
    api.off(STORY_CHANGED, this.handleStoryChanged);
  }

  handleToolOpen = (isVisible) => this.setState({ isVisible });

  handleChangePositions = (position) => {
    const { api } = this.props;
    api.emit(CHANGE_POSITION_ACTION, position);
    this.setState({ position });
  };

  handleStoryChanged = () => {
    this.setState({ position: defaultPosition });
  }

  render() {
    const { isVisible, position } = this.state;

    return (
      <WithTooltip
        placement="top"
        trigger="click"
        tooltipShown={isVisible}
        onVisibilityChange={this.handleToolOpen}
        tooltip={<PositionPicker position={position} onChange={this.handleChangePositions} />}
        closeOnClick
      >
        <IconButton>
          <Icons icon="location" />
        </IconButton>
      </WithTooltip>
    );
  }
}
