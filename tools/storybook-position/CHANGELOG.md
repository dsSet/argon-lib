# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/storybook-position@1.2.0...@qulix/storybook-position@1.2.1) (2019-09-06)


### Bug Fixes

* **storybook-position:** fix story container ([6b505c2](https://bitbucket.org/dsSet/argon-lib/commits/6b505c2))





# [1.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/storybook-position@1.1.2...@qulix/storybook-position@1.2.0) (2019-06-30)


### Features

* **storybook-style-picker:** add style picker addon for storybook ([b69c266](https://bitbucket.org/dsSet/argon-lib/commits/b69c266))





## [1.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/storybook-position@1.1.1...@qulix/storybook-position@1.1.2) (2019-06-26)

**Note:** Version bump only for package @qulix/storybook-position





## [1.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/storybook-position@1.1.0...@qulix/storybook-position@1.1.1) (2019-06-26)

**Note:** Version bump only for package @qulix/storybook-position





# 1.1.0 (2019-06-26)


### Bug Fixes

* **storybook:** add missed files ([0c72469](https://bitbucket.org/dsSet/argon-lib/commits/0c72469))


### Features

* **storybook:** add center addon for storybook ([acc1c0b](https://bitbucket.org/dsSet/argon-lib/commits/acc1c0b))
