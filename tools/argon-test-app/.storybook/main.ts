import type { StorybookConfig } from '@storybook/angular';
const config: StorybookConfig = {
  stories: [
    // '../src/**/*.mdx',
    '../src/**/*.stories.@(js|jsx|mjs|ts|tsx)',
    '../../../projects/*/stories/**/*.stories.@(js|jsx|mjs|ts|tsx)',
  ],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-interactions',
  ],
  framework: {
    name: '@storybook/angular',
    options: { },
  },
  docs: {
    autodocs: 'tag',
  },
  features: {
    storyStoreV7: false,
  },
};
export default config;
