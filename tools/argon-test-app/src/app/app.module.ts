import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TagsModule } from '@argon/tags';

import { AppComponent } from './app.component';
import { CheckboxModule } from '@argon/checkbox';
import { BsContextModule } from '@argon/bs-context';
import { ButtonsModule } from '@argon/buttons';
import { DropdownModule } from '@argon/dropdown';
import { IconsModule } from '@argon/icons';
import { KeyboardModule } from '@argon/keyboard';
import { MediaModule } from '@argon/media';
import { ModalModule } from '@argon/modal';
import { SpinnerModule } from '@argon/spinner';
import { StyleKitModule } from '@argon/style-kit';
import { ToolsModule } from '@argon/tools';
import { MessageBusModule } from '@argon/message-bus';
import { TooltipModule } from '@argon/tooltip';

@NgModule({
  imports: [
    BrowserModule,
    CheckboxModule,
    BsContextModule,
    ButtonsModule,
    DropdownModule,
    IconsModule,
    KeyboardModule,
    MediaModule,
    ModalModule,
    SpinnerModule,
    StyleKitModule,
    ToolsModule,
    TooltipModule,
    MessageBusModule,
    TagsModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
