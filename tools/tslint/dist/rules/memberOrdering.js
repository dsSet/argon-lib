
module.exports = {
  rules: {
    "member-ordering": [
      true,
      [
        "public-static-field",
        "protected-static-field",
        "private-static-field",
        "public-static-method",
        "protected-static-method",
        "private-static-method",
        "public-instance-field",
        "protected-instance-field",
        "private-instance-field",
        "public-constructor",
        "protected-constructor",
        "private-constructor",
        "public-instance-method",
        "protected-instance-method",
        "private-instance-method"
      ]
    ]
  }
};
