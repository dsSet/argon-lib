# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.18](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.17...@qulix/tslint@1.1.18) (2019-09-07)

**Note:** Version bump only for package @qulix/tslint





## [1.1.17](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.16...@qulix/tslint@1.1.17) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.16](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.15...@qulix/tslint@1.1.16) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.15](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.14...@qulix/tslint@1.1.15) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.14](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.13...@qulix/tslint@1.1.14) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.13](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.12...@qulix/tslint@1.1.13) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.12](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.11...@qulix/tslint@1.1.12) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.11](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.10...@qulix/tslint@1.1.11) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.10](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.9...@qulix/tslint@1.1.10) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.9](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.8...@qulix/tslint@1.1.9) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.8](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.7...@qulix/tslint@1.1.8) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.7](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.6...@qulix/tslint@1.1.7) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.5...@qulix/tslint@1.1.6) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.4...@qulix/tslint@1.1.5) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.3...@qulix/tslint@1.1.4) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.2...@qulix/tslint@1.1.3) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.1...@qulix/tslint@1.1.2) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





## [1.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@qulix/tslint@1.1.0...@qulix/tslint@1.1.1) (2019-06-23)

**Note:** Version bump only for package @qulix/tslint





# 1.1.0 (2019-06-22)


### Features

* **main:** add @argon/tslint ([ef2c799](https://bitbucket.org/dsSet/argon-lib/commits/ef2c799))
