# @argon/tslint package

## Versions

- `Angular`: ~8.1.3
- `Rxjs`: ~6.5.2  
- `Cdk`: ^8.1.1

## Install

> npm i --save @argon/tslint 

## Documentation

[@argon/tslint documentation](./docs/index.md)

## Package content

| Source | Content |
|--------|---------|
|`@argon/tslint/assets` | static assets folder |
|`@argon/tslint/style` | styles folder |
|`@argon/tslint/stories` | storybook folder |
|`@argon/tslint/docs` | documentation folder |

## Styling components

Package component are supporting Bootstrap 4 styles. Package also contains custom styles according by BEM technology.

> Custom styles should be included globally.

Custom styles can be applied in 3 ways.

### Include styles as is

Include `index.scss` file to your global app styles

```scss
@import "~@argon/tslint/style/index.scss";
```  

### Override default variables

```scss
@import "~@argon/tslint/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/tslint/style/index.scss";
```

### Use mixins optional

```scss
@import "~@argon/tslint/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/tslint/style/[package-mixin].scss";

.ar-component-to-styled {
  @include any-package-mixin();
  color: red;
}

```  

Otherwise you can skip components default styles and implement new ones by yourself. 
