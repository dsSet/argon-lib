var path = require("path");
var rimraf = require("rimraf");
var fs = require("fs");

function copyFileSync( source, target ) {

  var targetFile = target;

  //if target is a directory a new file with the same name will be created
  if ( fs.existsSync( target ) ) {
    if ( fs.lstatSync( target ).isDirectory() ) {
      targetFile = path.join( target, path.basename( source ) );
    }
  }

  fs.writeFileSync(targetFile, fs.readFileSync(source));
}

function copyFolderRecursiveSync( source, target ) {
  var files = [];

  //check if folder needs to be created or integrated
  var targetFolder = path.join( target, path.basename( source ) );
  if ( !fs.existsSync( targetFolder ) ) {
    fs.mkdirSync( targetFolder );
  }

  //copy
  if ( fs.lstatSync( source ).isDirectory() ) {
    files = fs.readdirSync( source );
    files.forEach( function ( file ) {
      var curSource = path.join( source, file );
      if ( fs.lstatSync( curSource ).isDirectory() ) {
        copyFolderRecursiveSync( curSource, targetFolder );
      } else {
        copyFileSync( curSource, targetFolder );
      }
    } );
  }
}

const root = path.resolve(__dirname, "../");
const dist = path.resolve(root, "dist");

const artifacts = [
  'CHANGELOG.md',
  'package.json',
  'README.md',
  'tslint-base.json',
  'tslint-config.json'
];
const artifactsDir = [
  'rules'
];

rimraf.sync(dist);
fs.mkdirSync(dist);

for(var i = 0; i < artifacts.length; i++) {
  var name = artifacts[i];
  copyFileSync(path.resolve(root, name), path.resolve(dist, name));
}

for(var i = 0; i < artifactsDir.length; i++) {
  var name = artifactsDir[i];
  copyFolderRecursiveSync(path.resolve(root, name), path.resolve(dist));
}


