const kebabCase = require('lodash/kebabCase');
const startCase = require('lodash/startCase');
const api = require('./api');
const fs = require('fs');
const copydir = require('copy-dir');
const path = require('path');
const config = require('./config');
const { execSync } = require('child_process');

let output;
const [ packageName ] = process.argv.slice(2);
const packageNameKebab = kebabCase(packageName);
const packageNamePascal = startCase(packageName);


/**
 * 0. Remove node_modules symlink
 */
try {
  console.log(`Remove @argon symlink from node_modules`);
  const TARGET_PATH = path.resolve(config.ROOT_PATH, 'node_modules', '@argon');
  if (fs.existsSync(TARGET_PATH)) {
    fs.rmdirSync(TARGET_PATH);
  }
} catch (e) {
  console.log(e);
}


/**
 * 1. GENERATE ANGULAR LIB
 */
try {
  console.log(`Generate package: ${packageNamePascal}`);
  const generateCmd = api.getGenerateAngularLibraryCommand(packageNameKebab);
  output = execSync(generateCmd, { encoding: "utf-8" });
  console.log(output);
} catch (e) {
  throw new Error(e.stderr);
}

/**
 * 2. UPDATE ROOT TSCONFIG FILES
 */
try {
  console.log('Update root tsconfig files');
  console.log('Update tsconfig.json');
  const mainConfig = api.getTsConfig('main');
  mainConfig.compilerOptions.paths[packageNameKebab] = [`node_modules/@argon/${packageNameKebab}/dist`];
  delete mainConfig.compilerOptions.paths[`${packageNameKebab}/*`];
  api.saveTsConfig(mainConfig, 'main');

  console.log('Update tsconfig.spec.json');
  const specConfig = api.getTsConfig('spec');
  specConfig .compilerOptions.paths[`@argon/${packageNameKebab}`] = [`projects/${packageNameKebab}`];
  api.saveTsConfig(specConfig, 'spec');
} catch (e) {
  console.log(e);
}

/**
 * 3. Replace original package files
 */
try {
  console.log('Replace original package files');
  const packagePath = api.getPackagePath(packageNameKebab, true);
  if (fs.existsSync(packagePath)) {
    api.rimraf(packagePath);
  }
  fs.mkdirSync(packagePath);
  copydir.sync(config.PACKAGE_TEMPLATE_PATH, packagePath);

  // package.json
  console.log('Update package.json file');
  const packageJsonPath = path.resolve(packagePath, 'package.json');
  fs.writeFileSync(packageJsonPath, api.updateFileContent(packageJsonPath, { packageName: packageNameKebab }));

  // karma.conf.js
  console.log('Update karma.conf.js file');
  const karmaConfigPath = path.resolve(packagePath, 'karma.conf.js');
  fs.writeFileSync(karmaConfigPath, api.updateFileContent(karmaConfigPath, { packageName: packageNameKebab }));

  // karma.conf.js
  console.log('Update public-api.ts file');
  const publicApiPath = path.resolve(packagePath, 'src/public-api.ts');
  fs.writeFileSync(publicApiPath, api.updateFileContent(publicApiPath, { packageName: packageNamePascal }));

  // Module
  console.log(`Generate ${packageNamePascal}.ts file`);
  const moduleFilePath = path.resolve(packagePath, `src/lib/${packageNamePascal}Module.ts`);
  const moduleFileContent = api.updateFileContent(config.NG_MODULE_TEMPLATE_PATH, { packageName: packageNamePascal });
  fs.writeFileSync(moduleFilePath, moduleFileContent);
} catch (e) {
  console.log(e);
}

/**
 * 4. populate package
 */
try {
  console.log('Add assets');
  api.populatePackage(packageNameKebab);
  const summary = api.getPackageSummary();
  const readmeContent = api.createReadme({ ...summary, packageName: packageNameKebab });
  api.updateReadme(packageNameKebab, readmeContent);
} catch (e) {
  console.log(e);
}

/**
 * 5. Build and bootstrap
 */
try {
  console.log('Initial build');
  output = execSync(`ng build ${packageNameKebab}`, { encoding: "utf-8" });
  console.log(output);
  console.log('Bootstrap');
  output = execSync(`npm run deps:bootstrap`, { encoding: "utf-8" });
  console.log(output);
} catch (e) {
  console.log(e);
}
