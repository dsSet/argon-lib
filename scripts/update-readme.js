const api = require('./api');

const summary = api.getPackageSummary();

console.log("Updating README.md files for all packages");

api.getProjectsNames().forEach((packageName) => {
  const readmeContent = api.createReadme({ ...summary, packageName });
  api.updateReadme(packageName, readmeContent);
});



