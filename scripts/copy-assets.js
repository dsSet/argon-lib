const api = require('./api');

console.log('Coping projects assets to publish folder.');

const projects = api.getProjects();
projects.forEach(api.copyPackageAssets);
projects.forEach(api.updateStorybookDependencies);
