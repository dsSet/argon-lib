const api = require('./api');

console.log('Coping projects assets to publish folder.');

const [ packageName ] = process.argv.slice(2);

const projectPath = api.getPackagePath(packageName);

api.copyPackageAssets(projectPath);
api.updateStorybookDependencies(projectPath);
