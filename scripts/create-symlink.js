const config = require('./config');
const path = require('path');
const fs = require('fs');
const { execSync } = require('child_process');

const TARGET_PATH = path.resolve(config.ROOT_PATH, 'node_modules', '@argon');

if (fs.existsSync(TARGET_PATH)) {
  console.log(`Directory ${TARGET_PATH} already exists.`);
} else {
  const cmd = `mklink /D ${TARGET_PATH} ${config.PROJECTS_PATH}`;
  execSync(cmd);
}

