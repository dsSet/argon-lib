const fs = require('fs');
const path = require('path');
const config = require('./config');

try {
  console.log(`Remove @argon symlink from node_modules`);
  const TARGET_PATH = path.resolve(config.ROOT_PATH, 'node_modules', '@argon');
  if (fs.existsSync(TARGET_PATH)) {
    fs.rmdirSync(TARGET_PATH);
  }
} catch (e) {
  console.log(e);
}
