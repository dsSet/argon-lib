const lodash = require('lodash');

class PackageSummary {

  constructor(meta) {
    this.angularVersion = lodash.get(meta, 'dependencies[@angular/core]');
    this.cdkVersion = lodash.get(meta, 'dependencies[@angular/cdk]');
    this.rxjsVersion = lodash.get(meta, 'dependencies[rxjs]');
    Object.defineProperty(this, 'meta', {
      get: () => meta,
      enumerable: false
    })
  }

}

module.exports = PackageSummary;
