const lodash = require('lodash');
const config = require('./config');
const copydir = require('copy-dir');
const path = require('path');
const fs = require('fs');
const PackageSummary = require('./models/PackageSummary');

function getPackagePath(name, silent) {
  const projectPath = path.resolve(config.PROJECTS_PATH, name);
  if (!fs.existsSync(projectPath) && !silent) {
    throw new Error('Path ' + projectPath + ' does not exist');
  }
  return projectPath;
}

function getProjectsNames() {
  return fs.readdirSync(config.PROJECTS_PATH);
}

function getProjects() {
  return getProjectsNames().map(getPackagePath);
}

function copyPackageAssets(projectPath) {
  const targetPath = path.resolve(projectPath, config.DIST_FOLDER_NAME);
  if (!fs.existsSync(targetPath)) {
    return;
  }

  config.ASSETS.forEach((assetName) => {
    const assetPath = path.resolve(projectPath, assetName);
    if (!fs.existsSync(assetPath)) {
      return;
    }
    const distPath = path.resolve(targetPath, assetName);
    copydir.sync(assetPath, distPath);
    console.log("Copy files from: ", assetPath, " to ", distPath);
  });
}

function replaceStorybookFile(projectPath, fileName) {
  const targetDir = path.resolve(projectPath, config.DIST_FOLDER_NAME, 'stories');
  if (!fs.existsSync(targetDir)) {
    return;
  }
  const targetPath = path.resolve(targetDir, fileName);
  const sourcePath = path.resolve(config.TEMPLATE_PATH, 'stories',fileName);
  copydir.sync(sourcePath, targetPath);
}

function updateStorybookDependencies(projectPath) {
  replaceStorybookFile(projectPath, 'index.ts');
  replaceStorybookFile(projectPath, 'style.scss');
}

function getPackageJsonMeta() {
  const content = fs.readFileSync(config.PACKAGE_JSON_PATH).toString();
  return JSON.parse(content);
}

function getPackageSummary() {
  const meta = getPackageJsonMeta();
  return new PackageSummary(meta);
}

function createReadme(summary) {
  return updateFileContent(config.README_TEMPLATE_PATH, summary);
}

function updateFileContent(path, summary) {
  let content = fs.readFileSync(path).toString();
  lodash.each(summary, (value, name) => {
    content = content.replace(new RegExp(`\{\{${name}\}\}`, 'g'), value);
  });

  return content;
}

function updateReadme(packageName, content) {
  const packagePath = getPackagePath(packageName);
  const readmePath = path.resolve(packagePath, 'README.md');
  fs.writeFileSync(readmePath, content);
}

function populatePackage(packageName) {
  const packagePath = getPackagePath(packageName);
  fs.readdirSync(config.BOILERPLATE_PATH).forEach((sourceName) => {
    const sourceDir = path.resolve(config.BOILERPLATE_PATH, sourceName);
    const targetPath = path.resolve(packagePath, sourceName);
    if (fs.existsSync(targetPath)) {
      console.log('Skip ', targetPath);
      return;
    }
    copydir.sync(sourceDir, targetPath);
  });

}

function getGenerateAngularLibraryCommand(name) {
  return `ng generate lib ${name}`;
}

/**
 * @param {'main', 'spec'} instance
 */
function getTsConfigPath(instance) {
  const name = instance === 'spec' ? 'tsconfig.spec.json' : 'tsconfig.json';
  return path.resolve(config.ROOT_PATH, name);
}

/**
 * @param {'main', 'spec'} instance
 */
function getTsConfig(instance) {
  const content = fs.readFileSync(getTsConfigPath(instance)).toString();
  return JSON.parse(content);
}

/**
 * @param data
 * @param {'main', 'spec'} instance
 */
function saveTsConfig(data, instance) {
  fs.writeFileSync(getTsConfigPath(instance), JSON.stringify(data, null, 2));
}

/**
 * Remove directory recursively
 * @param {string} dir_path
 * @see https://stackoverflow.com/a/42505874/3027390
 */
function rimraf(dir_path) {
  if (fs.existsSync(dir_path)) {
    fs.readdirSync(dir_path).forEach(function(entry) {
      var entry_path = path.join(dir_path, entry);
      if (fs.lstatSync(entry_path).isDirectory()) {
        rimraf(entry_path);
      } else {
        fs.unlinkSync(entry_path);
      }
    });
    fs.rmdirSync(dir_path);
  }
}

module.exports.getPackagePath = getPackagePath;
module.exports.getProjects = getProjects;
module.exports.getProjectsNames = getProjectsNames;
module.exports.copyPackageAssets = copyPackageAssets;
module.exports.getPackageJsonMeta = getPackageJsonMeta;
module.exports.getPackageSummary = getPackageSummary;
module.exports.createReadme = createReadme;
module.exports.updateReadme = updateReadme;
module.exports.populatePackage = populatePackage;
module.exports.updateStorybookDependencies = updateStorybookDependencies;
module.exports.getGenerateAngularLibraryCommand = getGenerateAngularLibraryCommand;
module.exports.getTsConfig = getTsConfig;
module.exports.saveTsConfig = saveTsConfig;
module.exports.getTsConfigPath = getTsConfigPath;
module.exports.rimraf = rimraf;
module.exports.updateFileContent = updateFileContent;
