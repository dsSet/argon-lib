const api = require('./api');

console.log("Populating projects with boilerplate code");

api.getProjectsNames().forEach(api.populatePackage);
