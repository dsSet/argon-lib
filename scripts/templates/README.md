# @argon/{{packageName}} package

## Versions

- `Angular`: {{angularVersion}}
- `Rxjs`: {{rxjsVersion}}  
- `Cdk`: {{cdkVersion}}

## Install

> npm i --save @argon/{{packageName}} 

## Documentation

[@argon/{{packageName}} documentation](./docs/index.md)

## Package content

| Source | Content |
|--------|---------|
|`@argon/{{packageName}}/assets` | static assets folder |
|`@argon/{{packageName}}/style` | styles folder |
|`@argon/{{packageName}}/stories` | storybook folder |
|`@argon/{{packageName}}/docs` | documentation folder |

## Styling components

Package component are supporting Bootstrap 4 styles. Package also contains custom styles according by BEM technology.

> Custom styles should be included globally.

Custom styles can be applied in 3 ways.

### Include styles as is

Include `index.scss` file to your global app styles

```scss
@import "~@argon/{{packageName}}/style/index.scss";
```  

### Override default variables

```scss
@import "~@argon/{{packageName}}/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/{{packageName}}/style/index.scss";
```

### Use mixins optional

```scss
@import "~@argon/{{packageName}}/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/{{packageName}}/style/[package-mixin].scss";

.ar-component-to-styled {
  @include any-package-mixin();
  color: red;
}

```  

Otherwise you can skip components default styles and implement new ones by yourself. 
