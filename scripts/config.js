const path = require('path');

const ROOT_PATH = path.resolve(__dirname, '../');
const PROJECTS_PATH = path.resolve(ROOT_PATH, 'projects');
const PACKAGE_JSON_PATH = path.resolve(ROOT_PATH, 'package.json');
const DIST_FOLDER_NAME = 'dist';
const TEMPLATE_PATH = path.resolve(__dirname, 'templates');
const BOILERPLATE_PATH = path.resolve(TEMPLATE_PATH, 'boilerplate');
const PACKAGE_TEMPLATE_PATH = path.resolve(TEMPLATE_PATH, 'package');
const README_TEMPLATE_PATH = path.resolve(TEMPLATE_PATH, 'README.md');
const NG_MODULE_TEMPLATE_PATH = path.resolve(TEMPLATE_PATH, 'NgModule.ts');

const ASSETS = ['stories', 'style', 'assets', 'docs', 'README.md', 'CHANGELOG.md'];

module.exports = {
  ROOT_PATH,
  PROJECTS_PATH,
  PACKAGE_JSON_PATH,
  ASSETS,
  DIST_FOLDER_NAME,

  TEMPLATE_PATH,
  BOILERPLATE_PATH,
  README_TEMPLATE_PATH,
  PACKAGE_TEMPLATE_PATH,
  NG_MODULE_TEMPLATE_PATH
};
