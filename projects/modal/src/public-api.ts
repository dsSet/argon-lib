/*
 * Public API Surface of modal
 */

export { DialogComponent } from './lib/components/Dialog/DialogComponent';
export { DialogBodyComponent } from './lib/components/DialogBody/DialogBodyComponent';
export { DialogFooterComponent } from './lib/components/DialogFooter/DialogFooterComponent';
export { DialogHeaderComponent } from './lib/components/DialogHeader/DialogHeaderComponent';

export { ModalModule } from './lib/ModalModule';

export { DialogService } from './lib/services/DialogService';
export { DialogInterface } from './lib/interfaces/DialogInterface';
export { DialogProxy } from './lib/models/DialogProxy';
export { DialogCloseSourceEnum, DIALOG_CLOSE_LEVEL_ALL, DIALOG_CLOSE_LEVEL_MINIMAL } from './lib/types/DialogCloseSourceEnum';
