import { fakeAsync, TestBed, waitForAsync } from '@angular/core/testing';
import { ModalModule } from '../ModalModule';
import {
  BlockScrollStrategy,
  GlobalPositionStrategy,
  Overlay,
  OverlayConfig,
  OverlayModule
} from '@angular/cdk/overlay';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { DialogService } from './DialogService';
import { NavigationEnd, NavigationStart, Router, RouterEvent, RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentPortal } from '@angular/cdk/portal';
import { DialogProxy } from '../models/DialogProxy';
import { Component, EventEmitter } from '@angular/core';
import { DialogInterface } from '../interfaces/DialogInterface';
import createSpyObj = jasmine.createSpyObj;
import { Subject } from 'rxjs';
import { DialogCloseSourceEnum } from '../types/DialogCloseSourceEnum';
import createSpy = jasmine.createSpy;

describe('DialogService', () => {

  let service: DialogService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [OverlayModule, RouterTestingModule],
    providers: [
      DialogService
    ]
  })));

  beforeEach(() => {
    service = TestBed.inject(DialogService) as any;
  });

  it('should have defaults', () => {
    expect(service.overlayRef).toBeDefined();
    expect(service.navigationObserver).toBeTruthy();
  });

  it('ngOnDestroy should dispose overlay', () => {
    spyOn(service.overlayRef, 'dispose');
    service.ngOnDestroy();
    expect(service.overlayRef.dispose).toHaveBeenCalled();
  });

  it('createDialog should return DialogProxy class for portal', () => {
    const portal = new ComponentPortal<any>(null);
    const dialog = service.createDialog(portal);
    expect(dialog instanceof DialogProxy).toBeTruthy();
    expect(dialog.portal).toBe(portal);
  });

  it('createDialog should return DialogProxy class for component', () => {
    @Component({ selector: 'spec-component', template: '' })
    class MockComponent implements DialogInterface {
      close: EventEmitter<void>;
    }
    const dialog = service.createDialog(MockComponent);
    expect(dialog instanceof DialogProxy).toBeTruthy();
    expect(dialog.portal.component).toBe(MockComponent);
  });

  it('setOverlay should dispose current one', () => {
    const overlay: Overlay = TestBed.inject(Overlay) as any;
    const prevOverlayRef = service.overlayRef;
    const nextOverlay = overlay.create({ });
    spyOn(prevOverlayRef, 'dispose');
    service.setOverlay(nextOverlay);
    expect(prevOverlayRef.dispose).toHaveBeenCalled();
    expect(service.overlayRef).toBe(nextOverlay);
  });

  it('createDefaultOverlayConfig should create overlay config', () => {
    const config = service.createDefaultOverlayConfig();
    expect(config instanceof OverlayConfig).toBeTruthy();
    expect(config.hasBackdrop).toBeTruthy();
    expect(config.backdropClass).toEqual(DialogService.BACKDROP_CLASS);
    expect(config.scrollStrategy instanceof BlockScrollStrategy).toBeTruthy();
    expect(config.positionStrategy instanceof GlobalPositionStrategy).toBeTruthy();
  });

  it('close should detach current dialog if attached', () => {
    spyOn(service.overlayRef, 'hasAttached').and.returnValue(true);
    spyOn(service.overlayRef, 'detach');
    service.close();
    expect(service.overlayRef.detach).toHaveBeenCalled();
  });

  it('close should not detach current dialog if has no attached', () => {
    spyOn(service.overlayRef, 'hasAttached').and.returnValue(false);
    spyOn(service.overlayRef, 'detach');
    service.close();
    expect(service.overlayRef.detach).not.toHaveBeenCalled();
  });

  it('navigationObserver should be null if have no router', () => {
    const dialogService = new DialogService(TestBed.inject(Overlay) as any, null);
    expect(dialogService.navigationObserver).toBeFalsy();
  });

  it('navigationObserver should emit DialogCloseSourceEnum.Router value', (done) => {
    const MockRouter = {
      events: new Subject<RouterEvent>()
    };

    const dialogService = new DialogService(TestBed.inject(Overlay) as any, MockRouter as any);

    dialogService.navigationObserver.subscribe((value: DialogCloseSourceEnum) => {
      expect(value).toEqual(DialogCloseSourceEnum.Router);
    });

    MockRouter.events.next(new NavigationStart(1, '/mock'));
    MockRouter.events.complete();
    done();
  });

  it('navigationObserver should emit NavigationStart events only', (done) => {
    const MockRouter = {
      events: new Subject<RouterEvent>()
    };

    const dialogService = new DialogService(TestBed.inject(Overlay) as any, MockRouter as any);

    const spy = createSpy('spyFn');

    dialogService.navigationObserver.subscribe(spy);

    MockRouter.events.next(new NavigationEnd(1, '/mock', '/mock'));
    expect(spy).not.toHaveBeenCalled();
    MockRouter.events.next(new NavigationStart(1, '/mock'));
    expect(spy).toHaveBeenCalled();
    MockRouter.events.complete();
    done();
    expect(spy.calls.allArgs()).toEqual([[DialogCloseSourceEnum.Router]]);
  });
});
