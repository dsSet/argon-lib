import compact from 'lodash/compact';
import { ComponentFactoryResolver, Injectable, Injector, OnDestroy, Optional, Type, ViewContainerRef } from '@angular/core';
import { NavigationStart, Router, RouterEvent } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ComponentPortal } from '@angular/cdk/portal';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { KeyCodesEnum } from '@argon/keyboard';
import { Unsubscribable } from '@argon/tools';
import { DialogInterface } from '../interfaces/DialogInterface';
import { DialogProxy } from '../models/DialogProxy';
import { DialogCloseSourceEnum } from '../types/DialogCloseSourceEnum';
import { OverlayRefProviderInterface } from '../interfaces/OverlayRefProviderInterface';


@Injectable({ providedIn: 'root' })
export class DialogService implements OnDestroy, OverlayRefProviderInterface {

  static BACKDROP_CLASS = 'ar-dialog-backdrop';

  static PANEL_CLASS = 'ar-dialog-panel';

  public get overlayRef(): OverlayRef { return this.overlayChanged ? this.overlayChanged.value : null; }

  public overlayChanged: BehaviorSubject<OverlayRef>;

  public get navigationObserver(): Observable<DialogCloseSourceEnum> {
    return this.router
      ? this.router.events.pipe(
          filter((event: RouterEvent) => event instanceof NavigationStart),
          map(() => DialogCloseSourceEnum.Router)
        )
      : null;
  }

  constructor(
    public readonly overlay: Overlay,
    @Optional() public readonly router: Router
  ) {
    const config = this.createDefaultOverlayConfig();
    this.setOverlay(this.overlay.create(config));
  }

  @Unsubscribable
  ngOnDestroy(): void {
    this.overlayRef.dispose();
  }

  public createDialog<TDialogComponent extends DialogInterface>(dialog: ComponentPortal<TDialogComponent>): DialogProxy<TDialogComponent>;
  public createDialog<TDialogComponent extends DialogInterface>(
    dialog: Type<TDialogComponent>,
    viewContainerRef?: ViewContainerRef | null,
    injector?: Injector | null,
    componentFactoryResolver?: ComponentFactoryResolver | null
  ): DialogProxy<TDialogComponent>;
  public createDialog<TDialogComponent extends DialogInterface>(
    dialog: ComponentPortal<TDialogComponent> | Type<TDialogComponent>,
    viewContainerRef?: ViewContainerRef | null,
    injector?: Injector | null,
    componentFactoryResolver?: ComponentFactoryResolver | null
  ): DialogProxy<TDialogComponent> {
    const dialogPortal = (dialog instanceof ComponentPortal)
      ? dialog
      : new ComponentPortal(dialog, viewContainerRef, injector, componentFactoryResolver);

    return new DialogProxy(dialogPortal, this, this.getDialogCloseSources());
  }

  public setOverlay(overlayRef: OverlayRef) {
    if (this.overlayRef) {
      this.overlayRef.dispose();
    }

    if (!this.overlayChanged) {
      this.overlayChanged = new BehaviorSubject<OverlayRef>(overlayRef);
    } else {
      this.overlayChanged.next(overlayRef);
    }
  }

  public close = () => {
    if (this.overlayRef.hasAttached()) {
      this.overlayRef.detach();
    }
  }

  public createDefaultOverlayConfig(): OverlayConfig {
    const positionStrategy = this.overlay.position().global();
    positionStrategy.centerHorizontally();
    const scrollStrategy = this.overlay.scrollStrategies.block();
    return new OverlayConfig({
      positionStrategy,
      scrollStrategy,
      backdropClass: DialogService.BACKDROP_CLASS,
      panelClass: DialogService.PANEL_CLASS,
      hasBackdrop: true
    });
  }

  private getDialogCloseSources(): Array<Observable<DialogCloseSourceEnum>> {
    return compact([
      this.navigationObserver,
      this.overlayRef.keydownEvents().pipe(
        filter((event: KeyboardEvent) => event.code === KeyCodesEnum.Esc),
        map(() => DialogCloseSourceEnum.Keyboard)
      ),
      this.overlayRef.backdropClick().pipe(
        map(() => DialogCloseSourceEnum.Backdrop)
      )
    ]);
  }

}
