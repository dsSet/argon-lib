import { NgModule } from '@angular/core';
import { MediaModule } from '@argon/media';
import { OverlayModule } from '@angular/cdk/overlay';
import { DialogComponent } from './components/Dialog/DialogComponent';
import { DialogHeaderComponent } from './components/DialogHeader/DialogHeaderComponent';
import { DialogBodyComponent } from './components/DialogBody/DialogBodyComponent';
import { DialogFooterComponent } from './components/DialogFooter/DialogFooterComponent';

@NgModule({
  imports: [
    OverlayModule,
    MediaModule
  ],
  declarations: [
    DialogComponent,
    DialogHeaderComponent,
    DialogBodyComponent,
    DialogFooterComponent
  ],
  exports: [
    DialogComponent,
    DialogHeaderComponent,
    DialogBodyComponent,
    DialogFooterComponent
  ]
})
export class ModalModule { }
