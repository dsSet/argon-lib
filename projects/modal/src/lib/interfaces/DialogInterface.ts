import { EventEmitter } from '@angular/core';

export interface DialogInterface {

  close: EventEmitter<void>;

}
