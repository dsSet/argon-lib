import { OverlayRef } from '@angular/cdk/overlay';
import { BehaviorSubject } from 'rxjs';

export interface OverlayRefProviderInterface {

  overlayRef: OverlayRef;

  overlayChanged: BehaviorSubject<OverlayRef>;

}
