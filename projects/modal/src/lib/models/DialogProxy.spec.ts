import { DialogProxy } from './DialogProxy';
import { ModalModule } from '../ModalModule';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { Overlay, OverlayModule, OverlayRef } from '@angular/cdk/overlay';
import { Component, EventEmitter } from '@angular/core';
import { DialogInterface } from '../interfaces/DialogInterface';
import { DIALOG_CLOSE_LEVEL_ALL, DialogCloseSourceEnum } from '../types/DialogCloseSourceEnum';
import { ComponentPortal } from '@angular/cdk/portal';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { OverlayRefProviderInterface } from '../interfaces/OverlayRefProviderInterface';

describe('DialogProxy', () => {

  @Component({ selector: 'spec-dialog', template: '' })
  class MockDialogComponent implements DialogInterface {
    public close = new EventEmitter<void>();
  }

  let model: DialogProxy<MockDialogComponent>;
  let overlayRef: OverlayRef;
  let overlaySource: OverlayRefProviderInterface;
  let overlay: Overlay;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ModalModule, OverlayModule],
    declarations: [MockDialogComponent]
  }).overrideModule(BrowserDynamicTestingModule, {
    set: {
      entryComponents: [ MockDialogComponent ],
    }
  }).compileComponents()));

  beforeEach(() => {
    overlay = TestBed.inject(Overlay) as any;
    overlayRef = overlay.create({ });
    overlaySource = { overlayRef, overlayChanged: new BehaviorSubject(overlayRef) };
    model = new DialogProxy(new ComponentPortal(MockDialogComponent), overlaySource);
  });

  afterEach(() => {
    overlayRef.dispose();
    model.dispose();
  });

  it('should have defaults', () => {
    expect(model.closeLevel).toEqual(DIALOG_CLOSE_LEVEL_ALL);
  });

  it('isOpened should return correct state', () => {
    expect(model.isOpened).toEqual(false);
    model.open();
    expect(model.isOpened).toEqual(true);
    model.close();
    expect(model.isOpened).toEqual(false);
  });

  it('instance should be instance of mounted component or null', () => {
    expect(model.instance).toEqual(null);
    model.open();
    expect(model.instance instanceof MockDialogComponent).toBeTruthy();
  });

  it('componentRef should be defined if dialog opened', () => {
    expect(model.componentRef).not.toBeDefined();
    model.open();
    expect(model.componentRef).toBeDefined();
    model.close();
    expect(model.componentRef).toEqual(null);
  });

  it('open should throw error if dialog already opened', () => {
    model.open();
    expect(() => { model.open(); }).toThrowError('Dialog already opened');
  });

  it('open should close other dialogs', () => {
    overlayRef.attach(new ComponentPortal(MockDialogComponent));
    spyOn(model.overlayProvider.overlayRef, 'detach').and.callThrough();
    model.open();
    expect(model.overlayProvider.overlayRef.detach).toHaveBeenCalled();
  });

  it('open should attach portal', () => {
    expect(overlayRef.hasAttached()).toBeFalsy();
    model.open();
    expect(overlayRef.hasAttached()).toBeTruthy();
  });

  it('open should return detach stream', () => {
    expect(model.open() instanceof Observable).toBeTruthy();
  });

  it('close should detach portal', () => {
    model.open();
    spyOn(model.portal, 'detach');
    model.close();
    expect(model.portal.detach).toHaveBeenCalled();
  });

  it('close should not call detach if dialog closed', () => {
    spyOn(model.portal, 'detach');
    model.close();
    expect(model.portal.detach).not.toHaveBeenCalled();
  });

  it('markForCheck should call changeDetector.markForCheck for opened dialog', () => {
    model.open();
    spyOn(model.changeDetector, 'markForCheck');
    model.markForCheck();
    expect(model.changeDetector.markForCheck).toHaveBeenCalled();
  });

  it('dispose should emit detach subject if dialog opened', () => {
    spyOn(model.detachSubject, 'next');
    spyOn(model.detachSubject, 'complete');
    model.open();
    model.dispose();
    expect(model.detachSubject.next).toHaveBeenCalled();
    expect(model.detachSubject.complete).toHaveBeenCalled();
  });

  it('dispose should call close action for the opened dialog', () => {
    spyOn(model, 'close');
    model.open();
    model.dispose();
    expect(model.close).toHaveBeenCalled();
  });

  it('dispose should not call close action and dispatch close subject if dialog is not open', () => {
    spyOn(model, 'close');
    spyOn(model.detachSubject, 'next');
    model.dispose();
    expect(model.close).not.toHaveBeenCalled();
    expect(model.detachSubject.next).not.toHaveBeenCalled();
  });

  it('overlay detachments should close dialog', () => {
    spyOn(model.detachSubject, 'next');
    model.open();
    expect(model.componentRef).toBeTruthy();
    expect(model.changeDetector).toBeTruthy();
    overlayRef.detach();
    expect(model.componentRef).toBeFalsy();
    expect(model.changeDetector).toBeFalsy();
    expect(model.detachSubject.next).toHaveBeenCalled();
  });

  it('overlay detachments should not call close actions if dialog is not open', () => {
    spyOn(model.detachSubject, 'next');
    overlayRef.attach(new ComponentPortal(MockDialogComponent));
    overlayRef.detach();
    expect(model.detachSubject.next).not.toHaveBeenCalled();
  });

  it('component close emitter should close dialog', fakeAsync( () => {
    spyOn(model, 'close');
    model.closeLevel = DialogCloseSourceEnum.Event;
    model.open();
    tick(5);
    model.instance.close.emit();
    expect(model.close).toHaveBeenCalled();
  }));

  it('close emitter should be based on different levels', fakeAsync(() => {
    const mockScheduler = new Subject<DialogCloseSourceEnum>();
    const dialog = new DialogProxy(
      new ComponentPortal(MockDialogComponent),
      { overlayRef, overlayChanged: new BehaviorSubject(overlayRef) },
      [mockScheduler]
    );
    const spy = spyOn(dialog, 'close');
    dialog.closeLevel = DialogCloseSourceEnum.Keyboard + DialogCloseSourceEnum.Router;
    dialog.open();

    tick(5);
    mockScheduler.next(DialogCloseSourceEnum.Keyboard);
    expect(dialog.close).toHaveBeenCalled();
    spy.calls.reset();

    tick(5);
    mockScheduler.next(DialogCloseSourceEnum.Event);
    expect(dialog.close).not.toHaveBeenCalled();
    spy.calls.reset();

    tick(5);
    mockScheduler.next(DialogCloseSourceEnum.Router);
    expect(dialog.close).toHaveBeenCalled();
    spy.calls.reset();

    tick(5);
    mockScheduler.next(DialogCloseSourceEnum.Backdrop);
    expect(dialog.close).not.toHaveBeenCalled();
    spy.calls.reset();
  }));

  it('should update overlay events on overlay changes', () => {
    const nextOverlay = overlay.create({ });
    spyOn(nextOverlay, 'detachments').and.returnValue(of(null));
    overlaySource.overlayChanged.next(nextOverlay);
    expect(nextOverlay.detachments).toHaveBeenCalled();
  });
});
