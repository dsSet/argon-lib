import { ChangeDetectorRef, ComponentRef } from '@angular/core';
import { asyncScheduler, Observable, scheduled, Subject, Subscription } from 'rxjs';
import { filter, map, mergeAll } from 'rxjs/operators';
import { ComponentPortal } from '@angular/cdk/portal';
import { OverlayRef } from '@angular/cdk/overlay';
import { DialogInterface } from '../interfaces/DialogInterface';
import { DIALOG_CLOSE_LEVEL_ALL, DialogCloseSourceEnum } from '../types/DialogCloseSourceEnum';
import { OverlayRefProviderInterface } from '../interfaces/OverlayRefProviderInterface';


export class DialogProxy<TDialogComponent extends DialogInterface> {

  public closeLevel: number = DIALOG_CLOSE_LEVEL_ALL;

  public componentRef: ComponentRef<TDialogComponent>;

  public get isOpened(): boolean { return this.portal.isAttached; }

  public get instance(): TDialogComponent | null {
    return this.isOpened ? this.componentRef.instance : null;
  }

  public changeDetector: ChangeDetectorRef;

  public readonly detachSubject = new Subject<void>();

  private closeSubscription: Subscription;

  private overlaySubscription: Subscription;

  private overlayChangeSubscription: Subscription;

  constructor(
    public readonly portal: ComponentPortal<TDialogComponent>,
    public readonly overlayProvider: OverlayRefProviderInterface,
    private closeSources: Array<Observable<DialogCloseSourceEnum>> = []
  ) {
    this.overlayChangeSubscription = this.overlayProvider.overlayChanged.subscribe(this.updateOverlayEvents);
  }

  public open = (): Observable<void> => {
    if (this.portal.isAttached) {
      throw new Error('Dialog already opened');
    }
    if (this.overlayProvider.overlayRef.hasAttached()) {
      this.overlayProvider.overlayRef.detach();
    }
    this.componentRef = this.portal.attach(this.overlayProvider.overlayRef);
    this.changeDetector = this.componentRef.injector.get(ChangeDetectorRef);
    this.closeSubscription = scheduled([
      ...this.closeSources,
      this.instance.close.pipe(map(() => DialogCloseSourceEnum.Event))
    ], asyncScheduler).pipe(
      mergeAll(),
      // tslint:disable-next-line:no-bitwise
      filter((event: DialogCloseSourceEnum) => Boolean(event & this.closeLevel))
    ).subscribe(this.close);
    return this.detachSubject.asObservable();
  }

  public close = () => {
    if (this.isOpened) {
      this.portal.detach();
    }
  }

  public markForCheck = () => {
    if (this.isOpened) {
      this.changeDetector.markForCheck();
    }
  }

  public dispose = () => {
    if (this.closeSubscription) {
      this.closeSubscription.unsubscribe();
    }
    this.overlaySubscription.unsubscribe();
    this.overlayChangeSubscription.unsubscribe();

    if (this.isOpened) {
      this.detachSubject.next();
      this.close();
    }

    this.detachSubject.complete();
  }

  private handleDialogClose = () => {
    if (this.closeSubscription) {
      this.closeSubscription.unsubscribe();
      this.closeSubscription = null;
    }
    this.componentRef = null;
    this.changeDetector = null;
    this.detachSubject.next();
  }

  private updateOverlayEvents = (overlayRef: OverlayRef) => {
    if (this.overlaySubscription) {
      this.overlaySubscription.unsubscribe();
    }
    this.overlaySubscription = overlayRef.detachments().pipe(
      filter(() => !this.portal.isAttached && Boolean(this.componentRef))
    ).subscribe(this.handleDialogClose);
  }

}
