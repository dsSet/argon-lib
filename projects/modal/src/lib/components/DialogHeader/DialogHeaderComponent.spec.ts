import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { DialogHeaderComponent } from './DialogHeaderComponent';

describe('DialogHeaderComponent', () => {

  let component: DialogHeaderComponent;
  let fixture: ComponentFixture<DialogHeaderComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [DialogHeaderComponent],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogHeaderComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-dialog-header class', () => {
    fixture.detectChanges();
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-dialog-header');
  });

  it('should have defaults', () => {
    expect(component.className).toEqual(true);
  });

});
