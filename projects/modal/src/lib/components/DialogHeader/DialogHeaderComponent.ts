import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ar-dialog-header',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogHeaderComponent {

  @HostBinding('class.ar-dialog-header') className = true;

}
