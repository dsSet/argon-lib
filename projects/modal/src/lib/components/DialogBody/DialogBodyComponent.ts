import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { ScrollContainerDirective } from '@argon/style-kit';

@Component({
  selector: 'ar-dialog-body',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogBodyComponent extends ScrollContainerDirective {

  @HostBinding('class.ar-dialog-body') className = true;

  @Input() xScroll = false;

  @Input() yScroll = true;

}
