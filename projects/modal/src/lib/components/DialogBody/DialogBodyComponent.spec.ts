import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { DialogBodyComponent } from './DialogBodyComponent';

describe('DialogBodyComponent', () => {

  let component: DialogBodyComponent;
  let fixture: ComponentFixture<DialogBodyComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [DialogBodyComponent],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogBodyComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-dialog-body class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-dialog-body');
  });

  it('should have defaults', () => {
    expect(component.className).toEqual(true);
    expect(component.xScroll).toEqual(false);
    expect(component.yScroll).toEqual(true);
  });

});
