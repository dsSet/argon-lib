import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { DialogComponent } from './DialogComponent';
import { BreakpointService, MediaModule } from '@argon/media';
import { BsSizeEnum } from '@argon/bs-context';

describe('DialogComponent', () => {

  let component: DialogComponent;
  let fixture: ComponentFixture<DialogComponent>;
  let breakpointService: BreakpointService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ MediaModule ],
    declarations: [ DialogComponent ],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogComponent);
    component = fixture.componentInstance;
    breakpointService = TestBed.inject(BreakpointService) as any;
  });

  it('className should toggle .ar-dialog class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-dialog');
  });

  it('fullWidth should toggle .ar-dialog--full-width class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'fullWidth', 'ar-dialog--full-width');
  });

  it('should append role attribute', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('role')).toEqual('dialog');
  });

  it('should have defaults', () => {
    expect(component.className).toEqual(true);
    expect(component.arBsSize).toEqual(BsSizeEnum.MD);
    expect(component.role).toEqual('dialog');
  });

  it('should toggle fullWidth on resolution change', () => {
    expect(component.fullWidth).toBeFalsy();
    fixture.detectChanges();
    breakpointService.isPhone.next(true);
    expect(component.fullWidth).toEqual(true);
    breakpointService.isPhone.next(false);
    expect(component.fullWidth).toEqual(false);
  });

  it('should not toggle fullWidth if set from the props', () => {
    component.fullWidth = false;
    fixture.detectChanges();
    breakpointService.isPhone.next(true);
    expect(component.fullWidth).toEqual(false);
  });

});
