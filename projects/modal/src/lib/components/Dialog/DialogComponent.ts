import isBoolean from 'lodash/isBoolean';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Injector,
  Input, OnChanges,
  OnDestroy,
  OnInit, SimpleChanges
} from '@angular/core';
import { Subscription } from 'rxjs';
import { ContextMixin, BsSizeType, BsContextType } from '@argon/bs-context';
import { BreakpointService } from '@argon/media';
import { Unsubscribable, BaseShape } from '@argon/tools';

@Component({
  selector: 'ar-dialog',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogComponent extends ContextMixin(BaseShape) implements OnInit, OnDestroy, OnChanges {

  @HostBinding('class.ar-dialog') className = true;

  @Input() arBsSize: BsSizeType = 'md';

  @Input() arBsContext: BsContextType;

  @Input() @HostBinding('class.ar-dialog--full-width') fullWidth: boolean;

  @HostBinding('attr.role') role = 'dialog';

  bsClassName = 'ar-dialog';

  private breakpointSubscription: Subscription;

  constructor(
    private breakpointService: BreakpointService,
    private changeDetectorRef: ChangeDetectorRef,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    if (!isBoolean(this.fullWidth)) {
      this.breakpointSubscription = this.breakpointService.isPhone.subscribe(this.handlePhoneSize);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  private handlePhoneSize = (isPhone: boolean) => {
    this.fullWidth = isPhone;
    this.changeDetectorRef.markForCheck();
  }

}
