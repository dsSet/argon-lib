import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { DialogFooterComponent } from './DialogFooterComponent';

describe('DialogFooterComponent', () => {

  let component: DialogFooterComponent;
  let fixture: ComponentFixture<DialogFooterComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [DialogFooterComponent],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogFooterComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-dialog-footer class', () => {
    fixture.detectChanges();
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-dialog-footer');
  });

  it('should have defaults', () => {
    expect(component.className).toEqual(true);
  });

});
