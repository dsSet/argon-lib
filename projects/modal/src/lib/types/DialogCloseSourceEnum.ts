
export enum DialogCloseSourceEnum {

  Router = 1,

  Backdrop = 2,

  Keyboard = 4,

  Event = 8

}

export const DIALOG_CLOSE_LEVEL_MINIMAL = DialogCloseSourceEnum.Router + DialogCloseSourceEnum.Event;
export const DIALOG_CLOSE_LEVEL_ALL = DialogCloseSourceEnum.Router
  + DialogCloseSourceEnum.Backdrop + DialogCloseSourceEnum.Keyboard + DialogCloseSourceEnum.Event;
