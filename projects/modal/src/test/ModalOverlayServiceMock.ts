import createSpy = jasmine.createSpy;

export class ModalOverlayServiceMock {

  createModal = createSpy('createModal');

  closeModal = createSpy('createModal');

}
