# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@2.0.1...@argon/modal@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/modal





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@1.0.4...@argon/modal@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/modal






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@1.0.4...@argon/modal@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/modal






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@1.0.3...@argon/modal@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/modal





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@1.0.2...@argon/modal@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/modal





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@1.0.1...@argon/modal@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/modal






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.10...@argon/modal@1.0.1) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.10...@argon/modal@1.0.0) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))






## [0.2.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.9...@argon/modal@0.2.10) (2020-08-04)

**Note:** Version bump only for package @argon/modal






## [0.2.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.8...@argon/modal@0.2.9) (2020-06-24)


### Bug Fixes

* **modal:** fix overlay ref updates for the DialogProxy model ([ae0ac11](https://bitbucket.org/dsSet/argon-lib/commits/ae0ac11))






## [0.2.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.7...@argon/modal@0.2.8) (2020-05-21)

**Note:** Version bump only for package @argon/modal






## [0.2.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.6...@argon/modal@0.2.7) (2020-04-18)

**Note:** Version bump only for package @argon/modal





## [0.2.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.5...@argon/modal@0.2.6) (2020-04-18)


### Bug Fixes

* **modal:** fix dialog base shape ([7b94bea](https://bitbucket.org/dsSet/argon-lib/commits/7b94bea))






## [0.2.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.4...@argon/modal@0.2.5) (2020-03-24)

**Note:** Version bump only for package @argon/modal





## [0.2.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.3...@argon/modal@0.2.4) (2020-03-24)

**Note:** Version bump only for package @argon/modal





## [0.2.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.2...@argon/modal@0.2.3) (2020-03-24)


### Bug Fixes

* **modal:** fix fullscreen switch on window resizing ([95fbd9c](https://bitbucket.org/dsSet/argon-lib/commits/95fbd9c))






## [0.2.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.1...@argon/modal@0.2.2) (2020-03-19)

**Note:** Version bump only for package @argon/modal






## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.2.0...@argon/modal@0.2.1) (2020-03-18)

**Note:** Version bump only for package @argon/modal





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.14...@argon/modal@0.2.0) (2020-03-18)


### Features

* **modals:** reimplement dialog component ([3e7f45f](https://bitbucket.org/dsSet/argon-lib/commits/3e7f45f))





## [0.1.14](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.13...@argon/modal@0.1.14) (2020-02-14)

**Note:** Version bump only for package @argon/modal






## [0.1.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.12...@argon/modal@0.1.13) (2019-11-29)

**Note:** Version bump only for package @argon/modal





## [0.1.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.11...@argon/modal@0.1.12) (2019-11-22)

**Note:** Version bump only for package @argon/modal





## [0.1.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.10...@argon/modal@0.1.11) (2019-11-22)

**Note:** Version bump only for package @argon/modal





## [0.1.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.9...@argon/modal@0.1.10) (2019-10-28)

**Note:** Version bump only for package @argon/modal





## [0.1.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.8...@argon/modal@0.1.9) (2019-10-11)

**Note:** Version bump only for package @argon/modal





## [0.1.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.7...@argon/modal@0.1.8) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





## [0.1.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.6...@argon/modal@0.1.7) (2019-10-08)

**Note:** Version bump only for package @argon/modal





## [0.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.5...@argon/modal@0.1.6) (2019-09-21)

**Note:** Version bump only for package @argon/modal





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.4...@argon/modal@0.1.5) (2019-09-17)

**Note:** Version bump only for package @argon/modal





## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.3...@argon/modal@0.1.4) (2019-09-13)

**Note:** Version bump only for package @argon/modal






## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.2...@argon/modal@0.1.3) (2019-09-07)

**Note:** Version bump only for package @argon/modal





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.1...@argon/modal@0.1.2) (2019-09-06)

**Note:** Version bump only for package @argon/modal





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/modal@0.1.0...@argon/modal@0.1.1) (2019-08-14)

**Note:** Version bump only for package @argon/modal





# 0.1.0 (2019-08-11)


### Features

* **modal:** add arModalClose directive ([5e8887d](https://bitbucket.org/dsSet/argon-lib/commits/5e8887d))
* **modal:** add modal package ([2a1344e](https://bitbucket.org/dsSet/argon-lib/commits/2a1344e))
* **modal:** implement modal dialog functionality ([e0cc551](https://bitbucket.org/dsSet/argon-lib/commits/e0cc551))
