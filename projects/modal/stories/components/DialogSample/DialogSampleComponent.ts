import { Component, OnDestroy, OnInit } from '@angular/core';
import { DIALOG_CLOSE_LEVEL_MINIMAL, DialogProxy, DialogService } from '../../index';
import { DialogComponent } from '../Dialog/DialogComponent';
import { finalize, takeUntil } from 'rxjs/operators';
import { timer } from 'rxjs';


@Component({
  selector: 'story-dialog-sample',
  template: '<button (click)="handleOpenDialog()" class="btn btn-sm btn-success">Open Dialog</button>'
})
export class DialogSampleComponent implements OnInit, OnDestroy {

  private dialog: DialogProxy<DialogComponent>;

  constructor(
    private dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this.dialog = this.dialogService.createDialog(DialogComponent);
    // this.dialog.closeLevel = DIALOG_CLOSE_LEVEL_MINIMAL;
  }

  ngOnDestroy(): void {
    this.dialog.dispose();
  }

  public handleOpenDialog() {
    if (this.dialog.isOpened) {
      return;
    }

    const dialogClosed = this.dialog.open();

    timer(0, 1000).pipe(
      takeUntil(dialogClosed)
    ).subscribe((n: number) => {
      this.dialog.instance.message = `Custom data updated by internal scheduler. Tick number ${n}`;
      this.dialog.markForCheck();
    });

    this.dialog.instance.customEvent.asObservable().pipe(
      takeUntil(dialogClosed),
      finalize(() => { console.log('Dialog event was disposed'); })
    ).subscribe((message: string) => {
      alert(`Dialog event handled: ${message}`);
    });
  }

}
