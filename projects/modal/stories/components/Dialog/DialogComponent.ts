import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import { DialogInterface } from '../../index';

@Component({
  selector: 'story-dialog',
  template: `
    <ar-dialog arBsSize="sm" [arBsSize]="current">
      <ar-dialog-header>Dialog</ar-dialog-header>
      <ar-dialog-body>
        <p>Dialog data: <strong>{{ message }}</strong></p>
        <label class="form-label">Text to emit with custom event</label>
        <input class="form-control" #input />
      </ar-dialog-body>
      <ar-dialog-footer class="form-inline">
        <select class="form-control form-control-sm mr-1" [(ngModel)]="current">
          <option *ngFor="let size of sizes" [ngValue]="size">{{size}}</option>
        </select>
        <button class="btn btn-sm btn-danger mr-1" (click)="close.emit()">close</button>
        <button class="btn btn-sm btn-secondary" (click)="customEvent.emit(input.value)">emit custom event</button>
      </ar-dialog-footer>
    </ar-dialog>
  `,
  styles: [`
    :host {
      display: block;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DialogComponent implements DialogInterface {

  @Input() message: string;

  @Output() customEvent = new EventEmitter<string>();

  public close = new EventEmitter<void>();

  public sizes = ['xxs', 'xs', 'sm', 'md', 'lg', 'xl'];

  public current = 'md';
}
