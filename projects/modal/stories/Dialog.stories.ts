import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ModalModule } from './index';
import { CommonModule } from '@angular/common';

import '!!style-loader!css-loader!sass-loader!./style.scss';

storiesOf('Argon|modal/Dialog Component', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        ModalModule,
        CommonModule
      ]
    })
  )
  .add('Dialog Component', () => ({
    props: {
      sizes: ['xxs', 'xs', 'sm', 'md', 'lg', 'xl'],
      current: 'md',
      content: false,
      full: false
    },
    template: `
      <ar-dialog [arBsSize]="current" [fullWidth]="full">
        <ar-dialog-header class="bg-secondary text-white">Dialog Header</ar-dialog-header>
        <ar-dialog-body>
          <ng-container *ngIf="content">
            <div class="story-dialog-body-item">Dialog Body</div>
            <div class="story-dialog-body-item">Dialog Body</div>
            <div class="story-dialog-body-item">Dialog Body</div>
            <div class="story-dialog-body-item">Dialog Body</div>
            <div class="story-dialog-body-item">Dialog Body</div>
            <div class="story-dialog-body-item">Dialog Body</div>
            <div class="story-dialog-body-item">Dialog Body</div>
            <div class="story-dialog-body-item">Dialog Body</div>
            <div class="story-dialog-body-item">Dialog Body</div>
            <div class="story-dialog-body-item">Dialog Body</div>
          </ng-container>
        </ar-dialog-body>
        <ar-dialog-footer>
          <div class="form-inline">
            <select class="form-control form-control-sm mr-1" [(ngModel)]="current">
              <option *ngFor="let size of sizes" [ngValue]="size">{{size}}</option>
            </select>
            <button class="btn btn-sm btn-secondary mr-1" (click)="content = !content">Toggle content</button>
            <button class="btn btn-sm btn-secondary" (click)="full = !full">Toggle full width mode</button>
          </div>
        </ar-dialog-footer>
      </ar-dialog>
    `
  }));
