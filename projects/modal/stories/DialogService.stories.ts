import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ModalModule } from './index';
import { DialogComponent } from './components/Dialog/DialogComponent';
import { DialogSampleComponent } from './components/DialogSample/DialogSampleComponent';

import '!!style-loader!css-loader!sass-loader!./style.scss';

storiesOf('Argon|modal/Dialog Service', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        ModalModule
      ],
      declarations: [
        DialogComponent,
        DialogSampleComponent,
      ],
      entryComponents: [
        DialogComponent
      ]
    })
  )
  .add('Create Dialog', () => ({ component: DialogSampleComponent, moduleMetadata: { } }));
