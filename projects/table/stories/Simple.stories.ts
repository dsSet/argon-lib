import { moduleMetadata, storiesOf } from '@storybook/angular';
import { TableModule } from './index';

import '!!style-loader!css-loader!sass-loader!./styles.scss';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { StyleKitModule } from '@argon/style-kit';

storiesOf('Argon|table/Simple Table', module)
  .addDecorator(
      moduleMetadata({
        imports: [
          TableModule,
          ScrollingModule,
          StyleKitModule
        ]
      })
    )
    .add('Header + Footer', () => ({
      props: {
        itemsCount: 5,
        getItems() {
          const items = [];
          for (let i = 1; i < this.itemsCount; i++) {
            items.push({
              cell1: `row ${i} col 1`,
              cell2: `row ${i} col 2`,
              cell3: `row ${i} col 3`,
              cell4: `row ${i} col 4`,
              cell5: `row ${i} col 5`,
            });
          }
          return items;
        },
        addRows() {
          this.itemsCount += 5;
        },
        removeRows() {
          if (this.itemsCount >= 5) {
            this.itemsCount -= 5;
          }
        }
      },
      template: `
        <div style="width: 900px">
          <button class="btn btn-sm" style="margin: 2rem;" (click)="addRows()">Add 5 rows</button>
          <button class="btn btn-sm" style="margin: 2rem;" (click)="removeRows()">Remove 5 rows</button>
          <ar-table-simple style="max-height: 400px; width: 100%" [scrollable]="true" [columns]="['col1', 'col2', 'col3', 'col4', 'col5']">
            <ar-table-header>
              <ar-table-row>
                <ar-table-cell>Head 1</ar-table-cell>
                <ar-table-cell>Head 2</ar-table-cell>
                <ar-table-cell>Head 3</ar-table-cell>
                <ar-table-cell>Head 4</ar-table-cell>
                <ar-table-cell>Head 5</ar-table-cell>
                <ar-table-cell>Head 6</ar-table-cell>
                <ar-table-cell>Head 7</ar-table-cell>
              </ar-table-row>
            </ar-table-header>
            <ar-table-body arScrollContainer [xScroll]="false">
              <ar-table-row *ngFor="let row of getItems()">
                <ar-table-cell>{{ row.cell1 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell2 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell3 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell4 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
              </ar-table-row>
            </ar-table-body>
            <ar-table-footer arScrollContainer [yScroll]="false">
              <ar-table-row>
                <ar-table-cell>Foot 1</ar-table-cell>
                <ar-table-cell>Foot 2</ar-table-cell>
                <ar-table-cell>Foot 2</ar-table-cell>
                <ar-table-cell>Foot 2</ar-table-cell>
                <ar-table-cell>Foot 2</ar-table-cell>
                <ar-table-cell>Foot 2</ar-table-cell>
                <ar-table-cell>Foot 2</ar-table-cell>
              </ar-table-row>
            </ar-table-footer>
          </ar-table-simple>
        </div>
      `
    }))
  .add('Header', () => ({
    props: {
      itemsCount: 5,
      getItems() {
        const items = [];
        for (let i = 1; i < this.itemsCount; i++) {
          items.push({
            cell1: `row ${i} col 1`,
            cell2: `row ${i} col 2`,
            cell3: `row ${i} col 3`,
            cell4: `row ${i} col 4`,
            cell5: `row ${i} col 5`,
          });
        }
        return items;
      },
      addRows() {
        this.itemsCount += 5;
      },
      removeRows() {
        if (this.itemsCount >= 5) {
          this.itemsCount -= 5;
        }
      }
    },
    template: `
        <div style="width: 900px">
          <button class="btn btn-sm" style="margin: 2rem;" (click)="addRows()">Add 5 rows</button>
          <button class="btn btn-sm" style="margin: 2rem;" (click)="removeRows()">Remove 5 rows</button>
          <ar-table-simple style="max-height: 400px; width: 100%" [scrollable]="true">
            <ar-table-header>
              <ar-table-row>
                <ar-table-cell>Head 1</ar-table-cell>
                <ar-table-cell>Head 2</ar-table-cell>
                <ar-table-cell>Head 3</ar-table-cell>
                <ar-table-cell>Head 4</ar-table-cell>
                <ar-table-cell>Head 5</ar-table-cell>
                <ar-table-cell>Head 6</ar-table-cell>
                <ar-table-cell>Head 7</ar-table-cell>
              </ar-table-row>
            </ar-table-header>
            <ar-table-body arScrollContainer>
              <ar-table-row *ngFor="let row of getItems()">
                <ar-table-cell>{{ row.cell1 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell2 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell3 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell4 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
              </ar-table-row>
            </ar-table-body>
          </ar-table-simple>
        </div>
      `
  }))
  .add('Header with scroll', () => ({
    props: {
      itemsCount: 5,
      getItems() {
        const items = [];
        for (let i = 1; i < this.itemsCount; i++) {
          items.push({
            cell1: `row ${i} col 1`,
            cell2: `row ${i} col 2`,
            cell3: `row ${i} col 3`,
            cell4: `row ${i} col 4`,
            cell5: `row ${i} col 5`,
          });
        }
        return items;
      },
      addRows() {
        this.itemsCount += 5;
      },
      removeRows() {
        if (this.itemsCount >= 5) {
          this.itemsCount -= 5;
        }
      }
    },
    template: `
        <div style="width: 900px">
          <button class="btn btn-sm" style="margin: 2rem;" (click)="addRows()">Add 5 rows</button>
          <button class="btn btn-sm" style="margin: 2rem;" (click)="removeRows()">Remove 5 rows</button>
          <ar-table-simple style="width: 100%" [scrollable]="true">
            <ar-table-header arScrollContainer [yScroll]="false">
              <ar-table-row>
                <ar-table-cell>Head 1</ar-table-cell>
                <ar-table-cell>Head 2</ar-table-cell>
                <ar-table-cell>Head 3</ar-table-cell>
                <ar-table-cell>Head 4</ar-table-cell>
                <ar-table-cell>Head 5</ar-table-cell>
                <ar-table-cell>Head 6</ar-table-cell>
                <ar-table-cell>Head 7</ar-table-cell>
              </ar-table-row>
            </ar-table-header>
            <ar-table-body arScrollContainer>
              <ar-table-row *ngFor="let row of getItems()">
                <ar-table-cell>{{ row.cell1 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell2 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell3 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell4 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
                <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
              </ar-table-row>
            </ar-table-body>
          </ar-table-simple>
        </div>
      `
  }));
