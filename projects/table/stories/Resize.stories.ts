import { moduleMetadata, storiesOf } from '@storybook/angular';
import { TableModule } from './index';
import { CommonModule } from '@angular/common';

import '!!style-loader!css-loader!sass-loader!./styles.scss';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { StyleKitModule } from '@argon/style-kit';

storiesOf('Argon|table/Plugins - Resize', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        TableModule,
        CommonModule,
        ReactiveFormsModule,
        StyleKitModule
      ]
    })
  )
  .add('Base', () => ({
    props: {
      field: new UntypedFormControl('head3'),
      direction: new UntypedFormControl('desc'),
      data: null,
      itemsCount: 15,
      getItems() {
        const items = [];
        for (let i = 1; i < this.itemsCount; i++) {
          items.push({
            id: i,
            cell1: `row ${i} col 1`,
            cell2: `row ${i} col 2`,
            cell3: `row ${i} col 3`,
            cell4: `row ${i} col 4`,
            cell5: `row ${i} col 5`,
          });
        }
        return items;
      },
      handleClick() {
        alert('CELL CLICK');
      },
      trackFn(item) {
        return item.id;
      }
    },
    template: `
        <ar-table-simple [scrollable]="true" style="width: auto" [columns]="['col1', 'col2', 'col3', 'col4', 'col5']">
          <ar-table-order-plugin></ar-table-order-plugin>
          <ar-table-resize-plugin></ar-table-resize-plugin>
          <ar-table-header arScrollContainer [yScroll]="false">
            <ar-table-row>
              <ar-table-cell arTableOrder="col1">
                <ar-table-order-anchor></ar-table-order-anchor>
                Head 1
                <ar-table-resize-handler></ar-table-resize-handler>
              </ar-table-cell>
              <ar-table-cell>
                Head 2
                <ar-table-resize-handler></ar-table-resize-handler>
              </ar-table-cell>
              <ar-table-cell>
                Head 3
                <ar-table-resize-handler></ar-table-resize-handler>
              </ar-table-cell>
              <ar-table-cell>
                Head 4
                <ar-table-resize-handler></ar-table-resize-handler>
              </ar-table-cell>
              <ar-table-cell>
                Head 5
                <ar-table-resize-handler></ar-table-resize-handler>
              </ar-table-cell>
            </ar-table-row>
          </ar-table-header>
          <ar-table-body arScrollContainer >
            <ar-table-row *ngFor="let row of getItems(); trackBy: trackFn">
              <ar-table-cell>{{ row.cell1 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell2 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell3 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell4 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
            </ar-table-row>
          </ar-table-body>
        </ar-table-simple>
      `
  }));
