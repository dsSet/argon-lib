import { moduleMetadata, storiesOf } from '@storybook/angular';
import { TableModule } from './index';
import { CommonModule } from '@angular/common';

import '!!style-loader!css-loader!sass-loader!./styles.scss';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { StyleKitModule } from '@argon/style-kit';
import { DragDropModule } from '@angular/cdk/drag-drop';

storiesOf('Argon|table/Plugins - Column Order', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        TableModule,
        CommonModule,
        ReactiveFormsModule,
        StyleKitModule,
        DragDropModule,
      ]
    })
  )
  .add('Base', () => ({
    props: {
      itemsCount: 7,
      columns: ['col-1', 'col-2', 'col-3', 'col-4', 'col-5'],
      visibleCols: new UntypedFormControl([]),
      getItems() {
        const items = [];
        for (let i = 1; i < this.itemsCount; i++) {
          items.push({
            id: i,
            cell1: `row ${i} col 1`,
            cell2: `row ${i} col 2`,
            cell3: `row ${i} col 3`,
            cell4: `row ${i} col 4`,
            cell5: `row ${i} col 5`,
          });
        }
        return items;
      },
      trackFn(item) {
        return item.id;
      }
    },
    template: `
        <h4>Hidden columns</h4>
        <select multiple [formControl]="visibleCols" style="width: 100%">
          <option *ngFor="let col of columns" [value]="col">{{ col }}</option>
        </select>
        <hr>
        <ar-table-simple [scrollable]="true" style="width: auto" [columns]="columns">
          <ar-table-toggle-plugin [hidden]="visibleCols.value"></ar-table-toggle-plugin>
          <ar-table-resize-plugin></ar-table-resize-plugin>
          <ar-table-column-order-plugin #columnOrder [columns]="columns"></ar-table-column-order-plugin>
          <ar-table-header arScrollContainer [yScroll]="false">
            <ar-table-row cdkDropList (cdkDropListDropped)="columnOrder.handleDropEvent($event)" cdkDropListOrientation="horizontal">
              <ar-table-cell cdkDrag cdkDragBoundary=".ar-table-row" [cdkDragStartDelay]="100">
                Head 1
                <ar-table-resize-handler></ar-table-resize-handler>
              </ar-table-cell>
              <ar-table-cell cdkDrag cdkDragBoundary=".ar-table-row" [cdkDragStartDelay]="100">
                  Head 2
                <ar-table-resize-handler></ar-table-resize-handler>
              </ar-table-cell>
              <ar-table-cell cdkDrag cdkDragBoundary=".ar-table-row" [cdkDragStartDelay]="100">
                Head 3
              </ar-table-cell>
              <ar-table-cell cdkDrag cdkDragBoundary=".ar-table-row" [cdkDragStartDelay]="100">
                Head 4
              </ar-table-cell>
              <ar-table-cell cdkDrag cdkDragBoundary=".ar-table-row" [cdkDragStartDelay]="100">
                Head 5
              </ar-table-cell>
            </ar-table-row>
          </ar-table-header>
          <ar-table-body arScrollContainer >
            <ar-table-row *ngFor="let row of getItems(); trackBy: trackFn">
              <ar-table-cell>{{ row.cell1 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell2 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell3 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell4 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
            </ar-table-row>
          </ar-table-body>
        </ar-table-simple>
      `
  }));
