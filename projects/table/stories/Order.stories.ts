import { moduleMetadata, storiesOf } from '@storybook/angular';
import { TableModule } from './index';
import { CommonModule } from '@angular/common';

import '!!style-loader!css-loader!sass-loader!./styles.scss';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';

storiesOf('Argon|table/Plugins - Order', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        TableModule,
        CommonModule,
        ReactiveFormsModule
      ]
    })
  )
  .add('Base', () => ({
    props: {
      field: new UntypedFormControl('head3'),
      direction: new UntypedFormControl('desc'),
      data: null,
      itemsCount: 15,
      getItems() {
        const items = [];
        for (let i = 1; i < this.itemsCount; i++) {
          items.push({
            id: i,
            cell1: `row ${i} col 1`,
            cell2: `row ${i} col 2`,
            cell3: `row ${i} col 3`,
            cell4: `row ${i} col 4`,
            cell5: `row ${i} col 5`,
          });
        }
        return items;
      },
      handleClick() {
        alert('CELL CLICK');
      },
      trackFn(item) {
        return item.id;
      }
    },
    template: `
        <pre>{{ data | json }}</pre>
        <select [formControl]="field" [value]="field.value">
          <option value="head1">col 1</option>
          <option value="head2">col 2</option>
          <option value="head3">col 3</option>
          <option value="head4">col 4</option>
          <option value="head5">col 5</option>
        </select>
        <select [formControl]="direction" [value]="direction.value">
          <option value="asc">asc</option>
          <option value="desc">desc</option>
        </select>
        <ar-table-simple style="width: 100%" [columns]="['col1', 'col2', 'col3', 'col4', 'col5']">
          <ar-table-order-plugin
            (dataChange)="data = $event; field.setValue(data.orderKey); direction.setValue(data.orderDirection)"
            [orderDirection]="direction.value"
            [orderKey]="field.value"
            defaultOrderDirection="desc"
          ></ar-table-order-plugin>
          <ar-table-header>
            <ar-table-row>
              <ar-table-cell>
                <ar-table-order-anchor arTableOrder="head1" [stopPropagation]="true">Order</ar-table-order-anchor>Head 1
              </ar-table-cell>
              <ar-table-cell>
                <ar-table-order-anchor arTableOrder="head2" [stopPropagation]="true">Order</ar-table-order-anchor>Head 2
              </ar-table-cell>
              <ar-table-cell arTableOrder="head3">
                <ar-table-order-anchor></ar-table-order-anchor>Head 3
              </ar-table-cell>
              <ar-table-cell arTableOrder="head4">
                <ar-table-order-anchor></ar-table-order-anchor>Head 4
              </ar-table-cell>
              <ar-table-cell arTableOrder="head5">
                <ar-table-order-anchor></ar-table-order-anchor>Head 5
              </ar-table-cell>
            </ar-table-row>
          </ar-table-header>
          <ar-table-body>
            <ar-table-row *ngFor="let row of getItems(); trackBy: trackFn">
              <ar-table-cell>{{ row.cell1 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell2 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell3 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell4 }}</ar-table-cell>
              <ar-table-cell>{{ row.cell5 }}</ar-table-cell>
            </ar-table-row>
          </ar-table-body>
        </ar-table-simple>
      `
  }));
