# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@2.0.2...@argon/table@2.0.3) (2023-10-18)


### Bug Fixes

* **stories:** fix scss imports ([4c0c91a](https://bitbucket.org/dsSet/argon-lib/commits/4c0c91a2440d316156fc1e7475299ba066906053))
* **table:** fix scss imports ([de3ad42](https://bitbucket.org/dsSet/argon-lib/commits/de3ad42c39b0c26d3c1719b003858a940fdd8efc))






## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@2.0.1...@argon/table@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/table





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@1.0.4...@argon/table@2.0.1) (2023-10-12)


### Bug Fixes

* tsfix ([6b405d8](https://bitbucket.org/dsSet/argon-lib/commits/6b405d8621988dd8b6fa34479bdb573e8bac2cc5))






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@1.0.4...@argon/table@2.0.0) (2023-10-12)


### Bug Fixes

* tsfix ([6b405d8](https://bitbucket.org/dsSet/argon-lib/commits/6b405d8621988dd8b6fa34479bdb573e8bac2cc5))






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@1.0.3...@argon/table@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/table





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@1.0.2...@argon/table@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/table





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@1.0.1...@argon/table@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/table






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.8...@argon/table@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/table





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.8...@argon/table@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/table






## [0.3.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.7...@argon/table@0.3.8) (2020-08-04)

**Note:** Version bump only for package @argon/table






## [0.3.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.6...@argon/table@0.3.7) (2020-05-21)

**Note:** Version bump only for package @argon/table






## [0.3.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.5...@argon/table@0.3.6) (2020-04-18)

**Note:** Version bump only for package @argon/table





## [0.3.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.4...@argon/table@0.3.5) (2020-04-18)

**Note:** Version bump only for package @argon/table






## [0.3.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.3...@argon/table@0.3.4) (2020-03-24)

**Note:** Version bump only for package @argon/table





## [0.3.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.2...@argon/table@0.3.3) (2020-03-24)

**Note:** Version bump only for package @argon/table






## [0.3.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.1...@argon/table@0.3.2) (2020-03-18)


### Bug Fixes

* **table:** fix table container constructor ([7deab3b](https://bitbucket.org/dsSet/argon-lib/commits/7deab3b))





## [0.3.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.3.0...@argon/table@0.3.1) (2020-02-14)

**Note:** Version bump only for package @argon/table






# [0.3.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.2.1...@argon/table@0.3.0) (2019-11-29)


### Bug Fixes

* **table:** fix hooks calls ([ad08099](https://bitbucket.org/dsSet/argon-lib/commits/ad08099))


### Features

* **table:** Implement Table column order plugin ([d47cd88](https://bitbucket.org/dsSet/argon-lib/commits/d47cd88))





## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.2.0...@argon/table@0.2.1) (2019-11-22)

**Note:** Version bump only for package @argon/table





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/table@0.1.0...@argon/table@0.2.0) (2019-11-22)


### Features

* **table:** add order plugin ([42e11b0](https://bitbucket.org/dsSet/argon-lib/commits/42e11b0))
* **table:** base table structure + styles ([b82fa0a](https://bitbucket.org/dsSet/argon-lib/commits/b82fa0a))
* **table:** implement plugin mixins. refactoring order plugin ([98e104d](https://bitbucket.org/dsSet/argon-lib/commits/98e104d))
* **table:** implement Resize plugin ([1ba9c77](https://bitbucket.org/dsSet/argon-lib/commits/1ba9c77))
* **table:** implement toggle columns plugin ([e5a517b](https://bitbucket.org/dsSet/argon-lib/commits/e5a517b))






# 0.1.0 (2019-10-31)


### Features

* **table:** add table module ([b68ebaf](https://bitbucket.org/dsSet/argon-lib/commits/b68ebaf))
