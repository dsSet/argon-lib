/*
 * Public API Surface
 */

export { TableResizePluginComponent } from './lib/plugins/Resize/TableResizePluginComponent';
export { TableComponent } from './lib/components/Table/TableComponent';
export { TableBodyComponent } from './lib/components/TableBody/TableBodyComponent';
export { TableCellComponent } from './lib/components/TableCell/TableCellComponent';
export { TableColumnComponent } from './lib/components/TableColumn/TableColumnComponent';
export { TableFooterComponent } from './lib/components/TableFooter/TableFooterComponent';
export { TableHeaderComponent } from './lib/components/TableHeader/TableHeaderComponent';
export { TableRowComponent } from './lib/components/TableRow/TableRowComponent';
export { TableColumnOrderPluginComponent } from './lib/plugins/ColumnOrder/TableColumnOrderPluginComponent';
export { TableOrderAnchorComponent } from './lib/plugins/Order/TableOrderAnchorComponent';
export { TableOrderDirective } from './lib/plugins/Order/TableOrderDirective';
export { TableOrderPluginComponent } from './lib/plugins/Order/TableOrderPluginComponent';
export { TableResizeHandlerComponent } from './lib/plugins/Resize/TableResizeHandlerComponent';
export { TableTogglePluginComponent } from './lib/plugins/Toggle/TableTogglePluginComponent';

export { TableModule } from './lib/TableModule';
