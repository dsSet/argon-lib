import { TableScrollInterface } from './TableScrollInterface';
import { TableStylesInterface } from './TableStylesInterface';
import { ElementRef } from '@angular/core';

export interface TableStateInterface {

  host: ElementRef<HTMLElement>;

  scroll: Partial<TableScrollInterface>;

  columns: Array<string>;

  styles: TableStylesInterface;

  [extensionKey: string]: any;

}
