import { SimpleChange } from '@angular/core';
import { TableStateInterface } from './TableStateInterface';


export interface TablePluginShapeInterface<TPluginData> {

  pluginName: string;

  pluginInstance: any;

  ngOnInit(): void;

  ngOnDestroy(): void;

  handlePluginDataChange(data: SimpleChange): void;

  getPluginData(): TPluginData;

  setPluginData(data: TPluginData, tableState?: Partial<TableStateInterface>);

}
