
export interface TableScrollInterface {

  [key: number]: boolean;

}
