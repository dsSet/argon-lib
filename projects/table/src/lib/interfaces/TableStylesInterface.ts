

export interface TableStylesInterface {

  [key: string]: Array<string>;

}
