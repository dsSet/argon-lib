import { EventEmitter } from '@angular/core';
import { TablePluginShapeInterface } from './TablePluginShapeInterface';


export interface TablePluginInterface<TPluginData> extends TablePluginShapeInterface<TPluginData> {

  dataChange: EventEmitter<TPluginData>;

  getInitialData(): TPluginData;

}
