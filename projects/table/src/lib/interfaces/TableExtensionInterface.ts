import { TableStateService } from '../services/TableStateService';
import { ChangeDetectorRef, ElementRef, OnDestroy, OnInit, SimpleChange } from '@angular/core';
import { StateInterface } from '@argon/tools';
import { TableStateInterface } from './TableStateInterface';
import { Observable } from 'rxjs';

export interface TableExtensionInterface extends OnInit, OnDestroy {

  host: ElementRef<HTMLElement>;

  tableStateService: TableStateService;

  changeDetector: ChangeDetectorRef;

  handleTableStateChange(nextState: StateInterface<TableStateInterface>): void;

  onPluginDataChange(name: string): Observable<SimpleChange>;

}
