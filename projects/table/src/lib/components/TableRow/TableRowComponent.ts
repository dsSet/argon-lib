import get from 'lodash/get';
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  HostBinding,
  OnDestroy,
  OnInit,
  QueryList
} from '@angular/core';
import { BaseShape } from '@argon/tools';
import { TableCellComponent } from '../TableCell/TableCellComponent';
import { TableStateMixin } from '../../mixins/TableStateMixin';
import { TableStateInterface } from '../../interfaces/TableStateInterface';
import { StateInterface } from '@argon/tools';

@Component({
  selector: 'ar-table-row',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableRowComponent extends TableStateMixin(BaseShape) implements AfterContentInit, OnInit, OnDestroy {

  @HostBinding('class.ar-table-row') className = true;

  @ContentChildren(TableCellComponent, { read: TableCellComponent, descendants: false }) cells: QueryList<TableCellComponent>;

  ngOnDestroy(): void {
    super.ngOnInit();
  }

  ngOnInit(): void {
    super.ngOnDestroy();
  }

  ngAfterContentInit(): void {
    this.updateCellsIndex(this.tableStateService.state.columns);
  }

  handleTableStateChange(nextState: StateInterface<TableStateInterface>): void {
    const { changes } = nextState;
    const { columns } = changes;
    if (columns && this.cells) {
      this.updateCellsIndex(columns.currentValue);
    }
    super.handleTableStateChange(nextState);
  }

  private updateCellsIndex(columns: Array<string>) {
    this.cells.forEach((cell: TableCellComponent, index: number) => {
      cell.setKey(get(columns, index, String(index)));
    });
  }
}
