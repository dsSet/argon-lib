import { ChangeDetectionStrategy, Component, forwardRef, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { columnScrollGroup } from '../../constants/ScrollGroups';
import { TableContainerDirective } from '../../directives/TableContainerDirective';
import { ContainerPositionEnum } from '../../types/ContainerPositionEnum';

@Component({
  selector: 'ar-table-footer',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: TableContainerDirective, useExisting: forwardRef(() => TableFooterComponent) }
  ]
})
export class TableFooterComponent extends TableContainerDirective implements OnInit, OnDestroy {

  @HostBinding('class.ar-table-footer') className = true;

  public arScrollGroup = [columnScrollGroup];

  public columnPosition = ContainerPositionEnum.CENTER;

  public rowPosition = ContainerPositionEnum.BOTTOM_PIN;

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
