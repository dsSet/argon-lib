import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { ScrollManager } from '@argon/tools';
import { TableDirective } from '../../directives/TableDirective';
import { TableStateService } from '../../services/TableStateService';


@Component({
  selector: 'ar-table-simple',
  template: '<ng-content></ng-content>',
  providers: [ TableStateService, ScrollManager ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableComponent extends TableDirective {

  @HostBinding('class.ar-table-simple') className = true;

}
