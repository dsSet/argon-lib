import { ChangeDetectionStrategy, Component, forwardRef, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { columnScrollGroup, rowScrollGroup } from '../../constants/ScrollGroups';
import { TableContainerDirective } from '../../directives/TableContainerDirective';
import { ContainerPositionEnum } from '../../types/ContainerPositionEnum';

@Component({
  selector: 'ar-table-body',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: TableContainerDirective, useExisting: forwardRef(() => TableBodyComponent) }
  ]
})
export class TableBodyComponent extends TableContainerDirective implements OnInit, OnDestroy {

  @HostBinding('class.ar-table-body') className = true;

  public arScrollGroup = [columnScrollGroup, rowScrollGroup];

  public columnPosition = ContainerPositionEnum.CENTER;

  public rowPosition = ContainerPositionEnum.MIDDLE;

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
