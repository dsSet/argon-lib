import { ChangeDetectionStrategy, Component, forwardRef, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { columnScrollGroup } from '../../constants/ScrollGroups';
import { TableContainerDirective } from '../../directives/TableContainerDirective';
import { ContainerPositionEnum } from '../../types/ContainerPositionEnum';

@Component({
  selector: 'ar-table-header',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: TableContainerDirective, useExisting: forwardRef(() => TableHeaderComponent) }
  ]
})
export class TableHeaderComponent extends TableContainerDirective implements OnInit, OnDestroy {

  @HostBinding('class.ar-table-header') className = true;

  public arScrollGroup = [columnScrollGroup];

  public columnPosition = ContainerPositionEnum.CENTER;

  public rowPosition = ContainerPositionEnum.TOP_PIN;

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
