import { AfterContentInit, ChangeDetectionStrategy, Component, ContentChildren, QueryList } from '@angular/core';
import { TableColumnComponent } from '../TableColumn/TableColumnComponent';

@Component({
  selector: 'ar-table-columns',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableColumnsComponent implements AfterContentInit {

  @ContentChildren(TableColumnComponent, { read: TableColumnComponent, descendants: false }) cells: QueryList<TableColumnComponent>;

  ngAfterContentInit(): void {

  }

}
