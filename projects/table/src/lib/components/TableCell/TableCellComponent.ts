import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  HostBinding,
  OnDestroy,
  OnInit,
  Optional,
  Renderer2,
  Self
} from '@angular/core';
import { Subscription } from 'rxjs';
import { CdkDrag } from '@angular/cdk/drag-drop';
import { Unsubscribable } from '@argon/tools';
import { TableColumnService } from '../../services/TableColumnService';

@Component({
  selector: 'ar-table-cell',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableCellComponent implements OnInit, OnDestroy {

  @HostBinding('class.ar-table-cell') className = true;

  public key: string;

  private dropSubscription: Subscription;

  constructor(
    public element: ElementRef<HTMLElement>,
    public tableColumnService: TableColumnService,
    public renderer: Renderer2,
    @Self() @Optional() public cdkDrag: CdkDrag
  ) { }

  ngOnInit(): void {
    if (this.cdkDrag) {
      this.dropSubscription = this.cdkDrag.dropped.subscribe(() => { this.setKey(this.key); });
    }
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  public setKey(key: string) {
    this.key = key;
    this.renderer.setStyle(this.element.nativeElement, 'width', this.tableColumnService.getColumnWidthVariableDefinition(key));
    this.renderer.setStyle(this.element.nativeElement, 'min-width', this.tableColumnService.getColumnWidthVariableDefinition(key));
    this.renderer.setStyle(this.element.nativeElement, 'max-width', this.tableColumnService.getColumnWidthVariableDefinition(key));
    this.renderer.setStyle(this.element.nativeElement, 'display', this.tableColumnService.getColumnDisplayVariableDefinition(key));
    this.renderer.setStyle(this.element.nativeElement, 'order', this.tableColumnService.getColumnOrderVariableDefinition(key));
  }
}
