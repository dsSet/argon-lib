import { AfterContentChecked, ContentChildren, Directive, ElementRef, HostBinding, Input, OnInit, QueryList } from '@angular/core';
import { TableContainerDirective } from './TableContainerDirective';
import { TableScrollService } from '../services/TableScrollService';
import { TableStateService } from '../services/TableStateService';
import { filter, map } from 'rxjs/operators';
import { StateInterface } from '@argon/tools';
import { TableStateInterface } from '../interfaces/TableStateInterface';
import { TableStylesInterface } from '../interfaces/TableStylesInterface';
import { SafeStyle } from '@angular/platform-browser';
import { TableStylesService } from '../services/TableStylesService';

@Directive({
  selector: '[arTable]'
})
export class TableDirective implements AfterContentChecked, OnInit {

  @HostBinding('class.ar-table') className = true;

  @HostBinding('class.ar-table--scrollable') @Input() scrollable: boolean;

  @HostBinding('attr.style') style: SafeStyle;

  @Input() columns: Array<string> = [];

  @ContentChildren(TableContainerDirective, { descendants: true, read: TableContainerDirective})
  containers: QueryList<TableContainerDirective>;

  constructor(
    public elementRef: ElementRef<HTMLElement>,
    public tableScrollService: TableScrollService,
    public tableStateService: TableStateService,
    public tableStyleService: TableStylesService
  ) { }

  ngOnInit(): void {
    this.tableStateService.tableState.setState({ columns: this.columns, host: this.elementRef });
    this.tableStateService.tableState.pipe(
      map(this.getNextStyles),
      filter(this.stylesFilter)
    ).subscribe(this.handleStylesChange);
  }

  ngAfterContentChecked(): void {
    const scroll = this.tableScrollService.calculateTableScroll(this.containers);
    this.tableStateService.setScroll(scroll);
  }

  private getNextStyles = (nextState: StateInterface<TableStateInterface>): TableStylesInterface | null => {
    return nextState.changes.styles ? nextState.changes.styles.currentValue : null;
  }

  private stylesFilter = (styles: TableStylesInterface | null) => {
    return Boolean(styles);
  }

  private handleStylesChange = (styles: TableStylesInterface) => {
    this.style = this.tableStyleService.getSafeStyles(styles);
  }

}
