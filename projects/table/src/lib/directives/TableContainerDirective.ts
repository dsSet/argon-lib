import { Directive, ElementRef, HostBinding, Injector, NgZone, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { ContainerPositionEnum } from '../types/ContainerPositionEnum';
import { UIScrollShape, ScrollGroupMixin, StateInterface } from '@argon/tools';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import { TableStateMixin } from '../mixins/TableStateMixin';
import { TableStateInterface } from '../interfaces/TableStateInterface';
import { TableScrollService } from '../services/TableScrollService';
import { TableScrollInterface } from '../interfaces/TableScrollInterface';


@Directive({
  selector: '[arTableContainer]'
})
export class TableContainerDirective extends TableStateMixin(ScrollGroupMixin(UIScrollShape)) implements OnInit, OnDestroy {

  static PADDING_CLASS_NAME = 'ar-table-container--padding';

  @HostBinding('class.ar-table-container') className = true;

  public get position(): ContainerPositionEnum {
    return this.columnPosition + this.rowPosition;
  }

  public columnPosition: ContainerPositionEnum;

  public rowPosition: ContainerPositionEnum;

  public renderer: Renderer2;

  public tableScrollService: TableScrollService;

  constructor(
    injector: Injector,
    elementRef: ElementRef<HTMLElement>,
    scrollDispatcher: ScrollDispatcher,
    ngZone: NgZone
  ) {
    super(injector, elementRef, scrollDispatcher, ngZone);
    this.renderer = this.injector.get(Renderer2);
    this.tableScrollService = this.injector.get(TableScrollService);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  handleTableStateChange(nextState: StateInterface<TableStateInterface>): void {
    const { changes } = nextState;
    const { scroll } = changes;

    if (scroll) {
      this.updateScrollPadding(scroll.currentValue);
    }

    super.handleTableStateChange(nextState);
  }

  private updateScrollPadding(scroll: Partial<TableScrollInterface>) {
    const shouldHavePadding = this.tableScrollService.shouldHaveScrollPadding(this, scroll);
    if (shouldHavePadding) {
      this.renderer.addClass(this.elementRef.nativeElement, TableContainerDirective.PADDING_CLASS_NAME);
    } else {
      this.renderer.removeClass(this.elementRef.nativeElement, TableContainerDirective.PADDING_CLASS_NAME);
    }
  }

}
