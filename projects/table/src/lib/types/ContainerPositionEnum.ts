

export enum ContainerPositionEnum {

  // COLUMN
  LEFT_PIN = 1,

  CENTER = 2,

  RIGHT_PIN = 4,

  // ROW
  TOP_PIN = 8,

  MIDDLE = 16,

  BOTTOM_PIN = 32,

}
