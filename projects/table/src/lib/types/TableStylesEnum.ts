

export enum TableStylesEnum {

  ColumnWidth = 'ColumnWidth',

  ColumnWidthDefault = 'ColumnWidthDefault',

  ColumnDisplay = 'ColumnDisplay',

  ColumnOrder = 'ColumnOrder',

}
