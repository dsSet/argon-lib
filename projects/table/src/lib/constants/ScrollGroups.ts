import { ScrollGroupInterface } from '@argon/tools';

export const columnScrollGroup: ScrollGroupInterface = { name: 'column', axis: 'x' };

export const rowScrollGroup: ScrollGroupInterface = { name: 'row', axis: 'y' };
