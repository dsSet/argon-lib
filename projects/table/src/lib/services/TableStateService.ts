import isEqual from 'lodash/isEqual';
import get from 'lodash/get';
import has from 'lodash/has';
import omit from 'lodash/omit';
import { Injectable, OnDestroy } from '@angular/core';
import { StateSubject, Unsubscribable } from '@argon/tools';
import { TableStateInterface } from '../interfaces/TableStateInterface';
import { TableScrollInterface } from '../interfaces/TableScrollInterface';
import { TablePluginInterface } from '../interfaces/TablePluginInterface';
import { TableStylesEnum } from '../types/TableStylesEnum';

@Injectable()
export class TableStateService implements OnDestroy {

  static getInitialTableState(): TableStateInterface {
    return {
      host: null,
      scroll: { },
      columns: [ ],
      styles: { }
    };
  }

  public readonly tableState = new StateSubject<TableStateInterface>(TableStateService.getInitialTableState());

  public get state(): TableStateInterface { return this.tableState.value.state; }

  public readonly plugins = new Map<string, TablePluginInterface<any>>([]);

  constructor() {
    // TODO: remove. debug only
    this.tableState.subscribe((v: any) => { console.log(v); });
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  public setScroll(scroll: Partial<TableScrollInterface>) {
    if (!isEqual(scroll, this.state.scroll)) {
      this.tableState.setState({ scroll });
    }
  }

  public setColumns(columns: Array<string>) {
    if (!isEqual(columns, this.state.columns)) {
      this.tableState.setState({ columns });
    }
  }

  public setStyles(key: TableStylesEnum, styleDefs: Array<string>) {
    const styles = { ...this.state.styles, [key]: styleDefs };
    this.tableState.setState({ styles });
  }

  public removeStyles(key: TableStylesEnum) {
    const styles = this.state.styles;
    if (has(styles, key)) {
      this.tableState.setState({ styles: omit(styles, key) });
    }
  }

  public registerPlugin(plugin: TablePluginInterface<any>) {
    this.plugins.set(plugin.pluginName, plugin);
    this.tableState.setState({ [plugin.pluginName]: plugin.getInitialData() });
  }

  public unregisterPlugin(plugin: TablePluginInterface<any>) {
    this.plugins.delete(plugin.pluginName);
    this.tableState.setState({ [plugin.pluginName]: null });
  }

  public getPlugin(name: string): TablePluginInterface<any> | null {
    return this.plugins.has(name) ? this.plugins.get(name) : null;
  }

  public setPluginData<TExtensionData>(name: string, data: TExtensionData, tableState: Partial<TableStateInterface> = { }) {
    if (isEqual(this.getPluginData(name), data)) {
      return;
    }
    this.tableState.setState({ ...tableState, [name]: data });
  }

  public getPluginData<TExtensionData>(name: string): TExtensionData | null {
    return get(this.tableState.state, name, null);
  }

}
