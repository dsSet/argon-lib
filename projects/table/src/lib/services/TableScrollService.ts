import find from 'lodash/find';
import { ElementRef, Injectable, QueryList, Renderer2, RendererFactory2 } from '@angular/core';
import { TableScrollInterface } from '../interfaces/TableScrollInterface';
import { TableContainerDirective } from '../directives/TableContainerDirective';
import { ContainerPositionEnum } from '../types/ContainerPositionEnum';

@Injectable({ providedIn: 'root' })
export class TableScrollService {

  public renderer: Renderer2;

  constructor(
    rendererFactory: RendererFactory2
  ) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  public calculateTableScroll(containers: QueryList<TableContainerDirective>): Partial<TableScrollInterface> {
    const scroll: Partial<TableScrollInterface> = { };
    containers.forEach((container: TableContainerDirective) => {
      scroll[container.position] = this.hasRowScroll(container.getElementRef());
    });

    return scroll;
  }

  public hasColumnScroll(element: ElementRef<HTMLElement>): boolean {
    const scrollWidth = element.nativeElement.scrollWidth;
    const clientWidth = element.nativeElement.clientWidth;
    const styles = window.getComputedStyle(element.nativeElement);

    return scrollWidth > clientWidth && (styles['overflow-x'] === 'auto' || styles['overflow-x'] === 'scroll');
  }

  public hasRowScroll(element: ElementRef<HTMLElement>): boolean {
    const scrollHeight = element.nativeElement.scrollHeight;
    const clientHeight = element.nativeElement.clientHeight;
    const styles = window.getComputedStyle(element.nativeElement);

    return scrollHeight > clientHeight && (styles['overflow-y'] === 'auto' || styles['overflow-y'] === 'scroll');
  }

  public shouldHaveScrollPadding(container: TableContainerDirective, scroll: Partial<TableScrollInterface>): boolean {
    const hasRowScroll = find(scroll, (hasScroll: boolean, position: string | number) => {
      // tslint:disable-next-line:no-bitwise
      return hasScroll && ((position as ContainerPositionEnum) & container.position) > 0;
    });
    if (!hasRowScroll) {
      return false;
    }

    const element = container.getElementRef();
    return !this.hasRowScroll(element);
  }

}
