import { Injectable } from '@angular/core';

@Injectable()
export class TableColumnService {

  static DEFAULT_CELL_WIDTH = 'var(--ar-table-column-default-width, 10rem)';

  static DEFAULT_CELL_DISPLAY = 'block';

  public getColumnDisplayVariableDefinition(key: string): string {
    return `var(${this.getColumnDisplayVariable(key)}, ${TableColumnService.DEFAULT_CELL_DISPLAY})`;
  }

  public getColumnDisplayVariable(key: string): string {
    return `--ar-table-column-display-${key}`;
  }

  public getColumnDisplayStyle(key: string, value: string): string {
    return `${this.getColumnDisplayVariable(key)}: ${value}`;
  }

  public getColumnOrderVariableDefinition(key: string): string {
    return `var(${this.getColumnOrderVariable(key)}, initial)`;
  }

  public getColumnOrderVariable(key: string): string {
    return `--ar-table-column-order-${key}`;
  }

  public getColumnOrderStyle(key: string, value: number): string {
    return `${this.getColumnOrderVariable(key)}: ${value}`;
  }

  public getColumnWidthVariableDefinition(key: string): string {
    return `var(${this.getColumnWidthVariable(key)}, ${TableColumnService.DEFAULT_CELL_WIDTH})`;
  }

  public getColumnWidthVariable(key: string): string {
    return `--ar-table-column-with-${key}`;
  }

  public getColumnWidthStyle(columnKey: string, width: number): string {
    const variableKey = this.getColumnWidthVariable(columnKey);
    return `${variableKey}: ${width}px`;
  }

  public getColumnDefaultWidthStyle(width: string): string {
    return `--ar-table-column-default-width: ${width}`;
  }

}
