import values from 'lodash/values';
import flatten from 'lodash/flatten';
import join from 'lodash/join';
import { Injectable } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { TableStylesInterface } from '../interfaces/TableStylesInterface';


@Injectable({ providedIn: 'root' })
export class TableStylesService {

  static STYLE_SEP = ';';

  constructor(
    private domSanitizer: DomSanitizer
  ) { }

  public getSafeStyles(styles: TableStylesInterface): SafeStyle {
    const styleString = join(flatten(values(styles)), TableStylesService.STYLE_SEP);
    return this.domSanitizer.bypassSecurityTrustStyle(styleString);
  }

}
