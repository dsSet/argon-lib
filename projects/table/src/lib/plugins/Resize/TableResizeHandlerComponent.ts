import get from 'lodash/get';
import map from 'lodash/map';
import { ChangeDetectionStrategy, Component, HostBinding, HostListener, Injector, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { finalize, take, takeUntil, throttleTime } from 'rxjs/operators';
import { BaseShape, Unsubscribable } from '@argon/tools';
import { TablePluginShapeMixin } from '../../mixins/TablePluginShapeMixin';
import { TableStateMixin } from '../../mixins/TableStateMixin';
import { PLUGIN_NAME } from './constants';
import { TableCellComponent } from '../../components/TableCell/TableCellComponent';
import { TableColumnsSizeInterface } from './TableColumnsSizeInterface';
import { TableStylesEnum } from '../../types/TableStylesEnum';
import { TableColumnService } from '../../services/TableColumnService';


@Component({
  selector: 'ar-table-resize-handler',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableResizeHandlerComponent
  extends TablePluginShapeMixin<TableColumnsSizeInterface>(TableStateMixin(BaseShape), PLUGIN_NAME) implements OnDestroy, OnInit {

  static ACTIVE_RESIZE_CLASS = 'ar-table-resize--active-cell';

  @HostBinding('class.ar-table-resize-handler') className = true;

  public initialWidth: number;

  public initialX: number;

  private mouseListener: Subscription;

  constructor(
    injector: Injector,
    public tableCell: TableCellComponent,
    public tableColumnService: TableColumnService,
    public renderer: Renderer2
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.renderer.addClass(this.tableCell.element.nativeElement, TableResizeHandlerComponent.ACTIVE_RESIZE_CLASS);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  @HostListener('mousedown', ['$event'])
  public handleMouseDown(event: MouseEvent) {
    event.stopPropagation();
    this.initialX = event.clientX;
    this.initialWidth = this.tableCell.element.nativeElement.clientWidth;
    this.setPluginState(true);
    const data = this.getPluginData();
    this.mouseListener = this.addMouseListener(data.throttle);
  }

  public handleMouseMove(event: MouseEvent) {
    if (!this.initialX || !this.initialWidth) {
      return;
    }

    let nextWidth = this.initialWidth + (event.clientX - this.initialX);
    const data = this.getPluginData();

    if (nextWidth < data.min) {
      nextWidth = data.min;
    } else if (nextWidth > data.max) {
      nextWidth = data.max;
    }

    if (get(data.columns, this.tableCell.key) === nextWidth) {
      return;
    }

    const columns = {
      ...data.columns,
      [this.tableCell.key]: nextWidth
    };

    const tableStyles = {
      ...this.tableStateService.state.styles,
      [TableStylesEnum.ColumnWidth]: map(columns, this.getStyleDefs)
    };

    this.setPluginData({ ...data, columns }, { styles: tableStyles });
  }

  public handleMouseUp() {
    this.initialX = null;
    this.initialWidth = null;
    this.setPluginState(false);
  }

  public setPluginState(active: boolean) {
    const data = this.getPluginData();
    this.setPluginData({ ...data, active });
  }

  public addMouseListener(throttle: number = 25): Subscription {
    return fromEvent(document, 'mousemove').pipe(
      takeUntil(fromEvent(document, 'mouseup').pipe(take(1))),
      throttleTime(throttle),
      finalize(() => this.handleMouseUp())
    ).subscribe((event: MouseEvent) => this.handleMouseMove(event));
  }

  private getStyleDefs = (width: number, key: string): string => {
    return this.tableColumnService.getColumnWidthStyle(key, width);
  }

}
