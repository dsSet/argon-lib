
export const PLUGIN_NAME = 'resize';

export const DEFAULT_COL_WIDTH = 250;

export const DEFAULT_COL_MIN_WIDTH = 150;

export const DEFAULT_COL_MAX_WIDTH = 350;

export const DEFAULT_THROTTLE = 50;

export const HOST_CLASS_NAME = 'ar-table-resize';

export const HOST_CLASS_NAME_ACTIVE = 'ar-table-resize--active';
