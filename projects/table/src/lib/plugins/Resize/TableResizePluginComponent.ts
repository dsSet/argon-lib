import { ChangeDetectionStrategy, Component, Injector, Input, OnDestroy, OnInit, Renderer2, SimpleChange } from '@angular/core';
import { BaseShape } from '@argon/tools';
import {
  DEFAULT_COL_MAX_WIDTH,
  DEFAULT_COL_MIN_WIDTH,
  DEFAULT_COL_WIDTH,
  DEFAULT_THROTTLE,
  HOST_CLASS_NAME, HOST_CLASS_NAME_ACTIVE,
  PLUGIN_NAME
} from './constants';
import { TablePluginMixin } from '../../mixins/TablePluginMixin';
import { TableStateMixin } from '../../mixins/TableStateMixin';
import { TableColumnsSizeInterface } from './TableColumnsSizeInterface';
import { TableColumnService } from '../../services/TableColumnService';
import { TableStylesEnum } from '../../types/TableStylesEnum';

@Component({
  selector: 'ar-table-resize-plugin',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableResizePluginComponent
  extends TablePluginMixin<TableColumnsSizeInterface>(TableStateMixin(BaseShape), PLUGIN_NAME, null)
  implements OnInit, OnDestroy {

  @Input() defaultWidth = DEFAULT_COL_WIDTH;

  @Input() minWidth = DEFAULT_COL_MIN_WIDTH;

  @Input() maxWidth = DEFAULT_COL_MAX_WIDTH;

  @Input() throttle = DEFAULT_THROTTLE;

  constructor(
    injector: Injector,
    public renderer: Renderer2,
    public tableColumnService: TableColumnService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.renderer.addClass(this.host.nativeElement, HOST_CLASS_NAME);
    this.tableStateService.setStyles(
      TableStylesEnum.ColumnWidthDefault,
      [this.tableColumnService.getColumnDefaultWidthStyle(`${this.defaultWidth}px`)]
    );
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.renderer.removeClass(this.host.nativeElement, HOST_CLASS_NAME);
    this.renderer.removeClass(this.host.nativeElement, HOST_CLASS_NAME_ACTIVE);
  }

  getInitialData(): TableColumnsSizeInterface {
    return {
      active: false,
      min: this.minWidth,
      max: this.maxWidth,
      throttle: this.throttle,
      defaultWidth: this.defaultWidth,
      columns: { }
    };
  }

  handlePluginDataChange(data: SimpleChange): void {
    const { active } = data.currentValue;
    const isActiveChange = data.currentValue && data.previousValue && data.currentValue.active !== data.previousValue.active;

    if (isActiveChange && active) {
      this.renderer.addClass(this.host.nativeElement, HOST_CLASS_NAME_ACTIVE);
    } else if (isActiveChange && !active) {
      this.renderer.removeClass(this.host.nativeElement, HOST_CLASS_NAME_ACTIVE);
    }

    super.handlePluginDataChange(data);
  }

}
