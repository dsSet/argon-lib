

export interface TableColumnsSizeInterface {

  active: boolean;

  min: number;

  max: number;

  defaultWidth: number;

  throttle: number;

  columns: {
    [columnKey: string]: number;
  };

}
