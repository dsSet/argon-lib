import { OrderDirectionEnum } from './OrderDirectionEnum';

export const PLUGIN_NAME = 'order';

export const DEFAULT_DIRECTION = OrderDirectionEnum.ASC;
