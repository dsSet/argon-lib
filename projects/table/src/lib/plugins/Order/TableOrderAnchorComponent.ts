import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';


@Component({
  selector: 'ar-table-order-anchor',
  template: `
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="ar-table-order-anchor__content">
      <path d="M16 1l-15 15h9v16h12v-16h9z"></path>
    </svg>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableOrderAnchorComponent {

  @HostBinding('class.ar-table-order-anchor') className = true;

}
