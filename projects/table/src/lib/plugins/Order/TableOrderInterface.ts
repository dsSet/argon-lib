import { OrderDirectionEnum } from './OrderDirectionEnum';


export interface TableOrderInterface {

  orderKey: string;

  orderDirection?: OrderDirectionEnum;

}
