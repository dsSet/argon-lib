import {
  ChangeDetectionStrategy,
  Component, EventEmitter, Injector,
  Input,
  OnChanges, OnDestroy, OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { BaseShape } from '@argon/tools';
import { TableStateMixin } from '../../mixins/TableStateMixin';
import { DEFAULT_DIRECTION, PLUGIN_NAME } from './constants';
import { TableOrderInterface } from './TableOrderInterface';
import { OrderDirectionEnum } from './OrderDirectionEnum';
import { TablePluginMixin } from '../../mixins/TablePluginMixin';

@Component({
  selector: 'ar-table-order-plugin',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableOrderPluginComponent
  extends TablePluginMixin<TableOrderInterface>(TableStateMixin(BaseShape), PLUGIN_NAME) implements OnChanges, OnInit, OnDestroy {

  @Input() defaultOrderDirection: OrderDirectionEnum = DEFAULT_DIRECTION;

  @Input() orderKey: string;

  @Input() orderDirection: OrderDirectionEnum = DEFAULT_DIRECTION;

  @Output() dataChange = new EventEmitter<TableOrderInterface>();

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { orderKey, orderDirection } = changes;
    if (!orderKey && !orderDirection) {
      return;
    }

    if ((orderKey && orderKey.isFirstChange()) || (orderDirection && orderDirection.isFirstChange())) {
      return;
    }

    let nextState: TableOrderInterface;

    if (orderKey && orderDirection) {
      nextState = { orderKey: orderKey.currentValue, orderDirection: orderDirection.currentValue };
    } else if (orderKey) {
      nextState = { orderKey: orderKey.currentValue, orderDirection: this.orderDirection };
    } else {
      nextState = { orderKey: this.orderKey, orderDirection: orderDirection.currentValue };
    }

    this.setPluginData(nextState);
  }

  getInitialData(): TableOrderInterface {
    return this.orderKey
      ? { orderKey: this.orderKey, orderDirection: this.orderDirection || this.defaultOrderDirection }
      : super.getInitialData();
  }

}
