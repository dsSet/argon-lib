import {
  Directive,
  HostBinding,
  HostListener,
  Injector,
  Input, OnDestroy, OnInit,
  SimpleChange
} from '@angular/core';
import { BaseShape } from '@argon/tools';
import { TableStateMixin } from '../../mixins/TableStateMixin';
import { PLUGIN_NAME } from './constants';
import { TableOrderInterface } from './TableOrderInterface';
import { OrderDirectionEnum } from './OrderDirectionEnum';
import { TablePluginShapeMixin } from '../../mixins/TablePluginShapeMixin';
import { TableOrderPluginComponent } from './TableOrderPluginComponent';

@Directive({
  selector: '[arTableOrder]'
})
export class TableOrderDirective
  extends TablePluginShapeMixin<TableOrderInterface>(TableStateMixin(BaseShape), PLUGIN_NAME)
  implements OnInit, OnDestroy {

  @Input() arTableOrder: string;

  @Input() stopPropagation: boolean;

  @HostBinding('class.ar-table-order') className = true;

  @HostBinding('class.ar-table-order--asc') asc: boolean;

  @HostBinding('class.ar-table-order--desc') desc: boolean;

  public get defaultOrderDirection(): OrderDirectionEnum {
    const plugin: TableOrderPluginComponent = this.pluginInstance;
    return plugin ? plugin.defaultOrderDirection : OrderDirectionEnum.ASC;
  }

  constructor(
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  @HostListener('click', ['$event'])
  public handleClick(event: MouseEvent) {
    const currentData = this.tableStateService.getPluginData<TableOrderInterface>(PLUGIN_NAME);
    const orderDirection = currentData && currentData.orderKey === this.arTableOrder
      ? (currentData.orderDirection === OrderDirectionEnum.ASC ? OrderDirectionEnum.DESC : OrderDirectionEnum.ASC)
      : this.defaultOrderDirection;
    const nextData: TableOrderInterface = {
      orderDirection,
      orderKey: this.arTableOrder
    };
    this.setPluginData(nextData);

    if (this.stopPropagation) {
      event.stopPropagation();
      return false;
    }
  }

  public handlePluginDataChange(data: SimpleChange) {
    if (!data.currentValue || data.currentValue.orderKey !== this.arTableOrder) {
      this.asc = false;
      this.desc = false;
    } else {
      this.asc = data.currentValue.orderDirection === OrderDirectionEnum.ASC;
      this.desc = data.currentValue.orderDirection === OrderDirectionEnum.DESC;
    }
    super.handlePluginDataChange(data);
  }

}
