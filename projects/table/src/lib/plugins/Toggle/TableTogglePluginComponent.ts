import map from 'lodash/map';
import difference from 'lodash/difference';
import { ChangeDetectionStrategy, Component, Injector, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { BaseShape } from '@argon/tools';
import { PLUGIN_NAME } from './constants';
import { TablePluginMixin } from '../../mixins/TablePluginMixin';
import { TableStateMixin } from '../../mixins/TableStateMixin';
import { TableColumnVisibilityInterface } from './TableColumnVisibilityInterface';
import { TableColumnService } from '../../services/TableColumnService';
import { TableStylesEnum } from '../../types/TableStylesEnum';


@Component({
  selector: 'ar-table-toggle-plugin',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableTogglePluginComponent
  extends TablePluginMixin<TableColumnVisibilityInterface>(TableStateMixin(BaseShape), PLUGIN_NAME)
  implements OnChanges, OnInit, OnDestroy {

  @Input() visible: Array<string>;

  @Input() hidden: Array<string>;

  constructor(
    injector: Injector,
    public tableColumnService: TableColumnService
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    if (this.visible) {
      this.setData(this.visible, this.getColumnsDiff(this.visible));
    } else if (this.hidden) {
      this.setData(this.getColumnsDiff(this.hidden), this.hidden);
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { visible, hidden } = changes;
    if (visible && !visible.isFirstChange()) {
      this.setData(visible.currentValue, this.getColumnsDiff(visible.currentValue));
    } else if (hidden && !hidden.isFirstChange()) {
      this.setData(this.getColumnsDiff(hidden.currentValue), hidden.currentValue);
    }
  }

  public setData(visible: Array<string>, hidden: Array<string>) {
    const columnStyleDefs = map(hidden, (key: string) => this.tableColumnService.getColumnDisplayStyle(key, 'none'));
    const styles = {
      ...this.tableStateService.state.styles,
      [TableStylesEnum.ColumnDisplay]: columnStyleDefs
    };
    this.setPluginData({ hidden, visible }, { styles });
  }

  public getColumnsDiff(original: Array<string>): Array<string> {
    return difference(this.tableStateService.state.columns, original);
  }

}
