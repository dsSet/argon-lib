

export interface TableColumnVisibilityInterface {

  visible: Array<string>;

  hidden: Array<string>;

}
