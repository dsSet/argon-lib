import cloneDeep from 'lodash/cloneDeep';
import map from 'lodash/map';
import indexOf from 'lodash/indexOf';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChange,
  SimpleChanges
} from '@angular/core';
import { BaseShape } from '@argon/tools';
import { TablePluginMixin } from '../../mixins/TablePluginMixin';
import { TableStateMixin } from '../../mixins/TableStateMixin';
import { PLUGIN_NAME } from './constants';
import { TableColumnOrderInterface } from './TableColumnOrderInterface';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { TableColumnService } from '../../services/TableColumnService';
import { TableStylesEnum } from '../../types/TableStylesEnum';


@Component({
  selector: 'ar-table-column-order-plugin',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableColumnOrderPluginComponent
  extends TablePluginMixin<TableColumnOrderInterface>(TableStateMixin(BaseShape), PLUGIN_NAME) implements OnChanges, OnInit, OnDestroy {

  @Input() columns: Array<string> = [];

  constructor(
    public tableColumnService: TableColumnService,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.setPluginData({ columns: this.columns });
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { columns } = changes;

    if (columns && !columns.isFirstChange()) {
      this.setPluginData({ columns: columns.currentValue });
    }
  }

  getInitialData(): TableColumnOrderInterface {
    return { columns: this.columns };
  }

  handlePluginDataChange(data: SimpleChange): void {
    const { columns } = data.currentValue;
    const styles = map(columns, this.getStyleDefs);
    this.tableStateService.setStyles(TableStylesEnum.ColumnOrder, styles);
    super.handlePluginDataChange(data);
  }

  public handleDropEvent(event: CdkDragDrop<Array<string>>): any {
    const { columns } = this.getPluginData();
    const prevIndex = indexOf(columns, this.columns[event.previousIndex]);
    const nextColumns = cloneDeep(columns);
    moveItemInArray(nextColumns, prevIndex, event.currentIndex);
    this.setPluginData({ columns: nextColumns });
  }

  private getStyleDefs = (column: string, index: number): string => {
    return this.tableColumnService.getColumnOrderStyle(column, index);
  }
}
