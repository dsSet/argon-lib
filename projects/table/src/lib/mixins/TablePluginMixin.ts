import { EventEmitter, Injectable, OnDestroy, OnInit, Output, SimpleChange, Directive } from '@angular/core';
import { Constructor, HasInjectorInterface } from '@argon/tools';
import { TableExtensionInterface } from '../interfaces/TableExtensionInterface';
import { TablePluginInterface } from '../interfaces/TablePluginInterface';
import { TablePluginShapeMixin } from './TablePluginShapeMixin';


export function TablePluginMixin<TPluginData>(
  BaseClass: Constructor<TableExtensionInterface & HasInjectorInterface>,
  pluginName: string,
  defaultData: TPluginData = null
): Constructor<TableExtensionInterface & HasInjectorInterface & TablePluginInterface<TPluginData>> {

  @Directive()
@Injectable()
  class TablePluginExtension
    extends TablePluginShapeMixin<TPluginData>(BaseClass, pluginName)
    implements TablePluginInterface<TPluginData>, OnInit, OnDestroy {

    @Output() dataChange = new EventEmitter<TPluginData>();

    ngOnInit(): void {
      super.ngOnInit();
      this.tableStateService.registerPlugin(this);
    }

    ngOnDestroy(): void {
      super.ngOnDestroy();
      this.tableStateService.unregisterPlugin(this);
    }

    getInitialData(): TPluginData {
      return defaultData;
    }

    public handlePluginDataChange(data: SimpleChange) {
      this.dataChange.emit(data.currentValue);
      super.handlePluginDataChange(data);
    }
  }

  return TablePluginExtension;

}
