import has from 'lodash/has';
import { ChangeDetectorRef, ElementRef, Injectable, OnDestroy, OnInit, SimpleChange } from '@angular/core';
import { Unsubscribable, HasLifecircleHooksInterface, HasInjectorInterface, Constructor, StateInterface } from '@argon/tools';
import { TableExtensionInterface } from '../interfaces/TableExtensionInterface';
import { TableStateService } from '../services/TableStateService';
import { TableStateInterface } from '../interfaces/TableStateInterface';
import { Observable, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

export function TableStateMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<TableExtensionInterface> {

  @Injectable()
  class TableStateExtension extends BaseClass implements TableExtensionInterface, OnInit, OnDestroy {

    public changeDetector: ChangeDetectorRef;

    public tableStateService: TableStateService;

    public get host(): ElementRef<HTMLElement> {
      return this.tableStateService.state.host;
    }

    private tableStateSubscription: Subscription;

    ngOnInit(): void {
      if (super.ngOnInit) {
        super.ngOnInit();
      }
      this.tableStateService = this.injector.get(TableStateService);
      this.changeDetector = this.injector.get(ChangeDetectorRef);
      this.tableStateSubscription = this.tableStateService.tableState
        .subscribe((nextState: StateInterface<TableStateInterface>) => { this.handleTableStateChange(nextState); });
    }

    @Unsubscribable
    ngOnDestroy(): void {
      if (super.ngOnDestroy) {
        super.ngOnDestroy();
      }
    }

    public handleTableStateChange(nextState: StateInterface<TableStateInterface>): void {
      this.changeDetector.markForCheck();
    }

    public onPluginDataChange(name: string): Observable<SimpleChange> {
      return this.tableStateService.tableState.pipe(
        filter((nextState: StateInterface<TableStateInterface>) => has(nextState.changes, name)),
        map((nextState: StateInterface<TableStateInterface>) => nextState.changes[name])
      );
    }

  }

  return TableStateExtension;
}
