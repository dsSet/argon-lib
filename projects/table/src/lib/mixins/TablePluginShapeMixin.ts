import { Injectable, OnDestroy, OnInit, SimpleChange } from '@angular/core';
import { Subscription } from 'rxjs';
import { Constructor, Unsubscribable, HasInjectorInterface } from '@argon/tools';
import { TableExtensionInterface } from '../interfaces/TableExtensionInterface';
import { TablePluginShapeInterface } from '../interfaces/TablePluginShapeInterface';
import { TableStateInterface } from '../interfaces/TableStateInterface';


export function TablePluginShapeMixin<TPluginData>(
  BaseClass: Constructor<TableExtensionInterface & HasInjectorInterface>,
  pluginName: string
): Constructor<TablePluginShapeInterface<TPluginData> & TableExtensionInterface & HasInjectorInterface> {

  @Injectable()
  class TablePluginShapeExtension extends BaseClass implements TablePluginShapeInterface<TPluginData>, OnInit, OnDestroy {

    public pluginName: string = pluginName;

    public get pluginInstance(): any {
      return this.tableStateService.getPlugin(this.pluginName);
    }

    private dataSubscription: Subscription;

    ngOnInit(): void {
      super.ngOnInit();
      this.dataSubscription = this.onPluginDataChange(pluginName).subscribe((data: SimpleChange) => this.handlePluginDataChange(data));
    }

    @Unsubscribable
    ngOnDestroy(): void {
      super.ngOnDestroy();
    }

    public handlePluginDataChange(data: SimpleChange) {
      this.changeDetector.markForCheck();
    }

    public getPluginData(): TPluginData {
      return this.tableStateService.getPluginData(this.pluginName);
    }

    public setPluginData(data: TPluginData, tableState: Partial<TableStateInterface> = { }) {
      this.tableStateService.setPluginData(this.pluginName, data, tableState);
    }

  }

  return TablePluginShapeExtension;
}
