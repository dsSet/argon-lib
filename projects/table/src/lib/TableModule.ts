import { NgModule } from '@angular/core';
import { TableComponent } from './components/Table/TableComponent';
import { TableBodyComponent } from './components/TableBody/TableBodyComponent';
import { TableCellComponent } from './components/TableCell/TableCellComponent';
import { TableColumnComponent } from './components/TableColumn/TableColumnComponent';
import { TableColumnsComponent } from './components/TableColumns/TableColumnsComponent';
import { TableFooterComponent } from './components/TableFooter/TableFooterComponent';
import { TableHeaderComponent } from './components/TableHeader/TableHeaderComponent';
import { TableRowComponent } from './components/TableRow/TableRowComponent';
import { TableContainerDirective } from './directives/TableContainerDirective';
import { TableDirective } from './directives/TableDirective';
import { TableColumnService } from './services/TableColumnService';
import { TableOrderPluginComponent } from './plugins/Order/TableOrderPluginComponent';
import { TableOrderDirective } from './plugins/Order/TableOrderDirective';
import { TableOrderAnchorComponent } from './plugins/Order/TableOrderAnchorComponent';
import { TableResizeHandlerComponent } from './plugins/Resize/TableResizeHandlerComponent';
import { TableResizePluginComponent } from './plugins/Resize/TableResizePluginComponent';
import { TableTogglePluginComponent } from './plugins/Toggle/TableTogglePluginComponent';
import { TableColumnOrderPluginComponent } from './plugins/ColumnOrder/TableColumnOrderPluginComponent';

@NgModule({
  declarations: [
    TableContainerDirective,
    TableDirective,
    TableComponent,
    TableBodyComponent,
    TableCellComponent,
    TableColumnComponent,
    TableFooterComponent,
    TableHeaderComponent,
    TableRowComponent,
    // PLUGINS
    // ORDER
    TableOrderDirective,
    TableOrderPluginComponent,
    TableOrderAnchorComponent,
    // RESIZE
    TableResizeHandlerComponent,
    TableResizePluginComponent,
    // TOGGLE
    TableTogglePluginComponent,
    // COLUMN ORDER
    TableColumnOrderPluginComponent,
    TableColumnsComponent
  ],
  exports: [
    TableComponent,
    TableBodyComponent,
    TableCellComponent,
    TableColumnComponent,
    TableFooterComponent,
    TableHeaderComponent,
    TableRowComponent,
    // PLUGINS
    // ORDER
    TableOrderDirective,
    TableOrderPluginComponent,
    TableOrderAnchorComponent,
    // RESIZE
    TableResizeHandlerComponent,
    TableResizePluginComponent,
    // TOGGLE
    TableTogglePluginComponent,
    // COLUMN ORDER
    TableColumnOrderPluginComponent
  ],
  providers: [
    TableColumnService
  ]
})
export class TableModule {

}
