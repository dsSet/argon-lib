#Radio Group component

`<ar-radio-group>` allow to group radio buttons. This component will bind form control to each child `<ar-radio>`.

````typescript
const values = [1, 2, 3, 4, 5, 6];
const control = new FormControl(2);
````

##Usage

````html
<ar-radio-group [formControl]="control">
  <ar-radio *ngFor="let value of values" [value]="value">
    <ar-checkbox-label>Label {{ value }}</ar-checkbox-label>
  </ar-radio>
</ar-radio-group>
````

##Make radio buttons inline

````html
<ar-radio-group [formControl]="control" [inline]="true">
  <ar-radio *ngFor="let value of values" [value]="value">
    <ar-checkbox-label>Label {{ value }}</ar-checkbox-label>
  </ar-radio>
</ar-radio-group>
````


