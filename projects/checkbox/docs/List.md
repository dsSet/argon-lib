#Using Checkbox for list data

`<ar-bulk-checkbox>` and `<ar-list-checkbox>` allow to works with list data.

````typescript
const values = [1, 2, 3, 4, 5];
const control = new FormControl([2]);
````

##Usage

````html
<ar-list-checkbox *ngFor="let n of values" [formControl]="control" [value]="n">
  <ar-checkbox-label>Checkbox with value "{{ n }}"</ar-checkbox-label>
</ar-list-checkbox>
````

##Bulk operation

````html
<ar-bulk-checkbox [formControl]="control" [value]="values" type="checkbox-indeterminate">
  <ar-checkbox-label>Bulk operations</ar-checkbox-label>
</ar-bulk-checkbox>
<ar-list-checkbox *ngFor="let n of values" [formControl]="control" [value]="n">
  <ar-checkbox-label>Checkbox with value "{{ n }}"</ar-checkbox-label>
</ar-list-checkbox>
````
