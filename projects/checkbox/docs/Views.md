#Using control view

##Build in checkbox views

Use indeterminate checkbox:

````html
<ar-checkbox [formControl]="control" type="checkbox-indeterminate">
   <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
</ar-checkbox>
````

`checkbox` is provided as view by default. The next statements will be the equal:

````html
<ar-checkbox [formControl]="control" type="checkbox">
   <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
</ar-checkbox>

<ar-checkbox [formControl]="control">
   <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
</ar-checkbox>
````

##Implement custom view:

Feel a free to implement any view that you need. Let's implement custom simple view for checkbox.
The new view component will display `+`, `-` and `?` for the checkbox states.
Also will have optional ability to render this chars with bold font.

##### Step 1: create new view component.

````typescript
import { CheckboxViewDirective } from '@argon/checkbox';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'story-custom-checkbox-view',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomCheckboxComponent extends CheckboxViewDirective {

}
````

We extends this component from `CheckboxViewDirective` to make it compatible with checkbox UI.
> You should extends component from `RadioViewDirective` for radio view components.

##### Step 2: apply chars output logic

````typescript
import { CheckboxViewDirective } from '@argon/checkbox';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'story-custom-checkbox-view',
  template: '{{ content }}',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomCheckboxComponent extends CheckboxViewDirective {
  
  public get content(): string {
    if (this.isIndeterminate) {
      return '?';
    }

    return this.isOn ? '+' : '-';
  }
  
}
````

`CheckboxViewDirective` and `RadioViewDirective` contains a list of properties to determinate current view state
and also bind base attributes to the view `role` and `class names`.


````typescript
class BooleanShape {

  public disabled = false;

  public viewData: any;

  public isOn: boolean;

  public isOff: boolean;

  public isIndeterminate: boolean;

  public currentState: CheckboxStateEnum;

  protected renderer: Renderer2;

  protected stateService: CheckboxStateService;

  protected changeDetector: ChangeDetectorRef;

  protected element: ElementRef;

  protected injector: Injector;

}
````

`RadioViewDirective` also has `value` property to determinate current radio view value.

##### Step 3: provide view data

Let's provide custom view data for the view component. It will be a string to determinate style.

````typescript
import { CheckboxViewDirective } from '@argon/checkbox';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'story-custom-checkbox-view',
  template: `
    <span *ngIf="!isBold">{{ content }}</span>
    <b *ngIf="isBold">{{ content }}</span>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomCheckboxComponent extends CheckboxViewDirective {
  
  viewData: string;
  
  public get isBold(): boolean {
    return this.viewData === 'bold';
  }
  
  public get content(): string {
    if (this.isIndeterminate) {
      return '?';
    }

    return this.isOn ? '+' : '-';
  }
  
}
````

We will send view data to the `<ar-checkbox>` control and receive this data at the view.


##### Step 4: provide view component as checkbox view

````typescript
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ARGON_CHECKBOX_VIEW, CheckboxConfigModel } from '@argon/checkbox';
import { CustomCheckboxComponent } from '..';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CustomCheckboxComponent
  ],
  entryComponents: [
    CustomCheckboxComponent
  ],
  providers: [
    {
      provide: ARGON_CHECKBOX_VIEW,
      useValue: new CheckboxConfigModel(CustomCheckboxComponent, 'custom-checkbox'),
      multi: true
    },
    {
      provide: ARGON_CHECKBOX_VIEW,
      useValue: new CheckboxConfigModel(CustomCheckboxComponent, 'custom-checkbox-indeterminate', true),
      multi: true
    }
  ]
})
export class CustomModule { }
````

We are providing custom view twice both for the regular checkbox and checkbox with indeterminate state. 

##### Step 5: usage

````html
<ar-checkbox type="custom-checkbox">
  <ar-checkbox-label>Regular Checkbox</ar-checkbox-label>
</ar-checkbox>

<ar-checkbox type="custom-checkbox" viewData="bold">
  <ar-checkbox-label>Regular Checkbox (should be bold)</ar-checkbox-label>
</ar-checkbox>

<ar-checkbox type="custom-checkbox-indeterminate">
  <ar-checkbox-label>Checkbox with indeterminate state</ar-checkbox-label>
</ar-checkbox>
````

##### Step 6: provide view as default

As you see from prev example you are need to set `type` property for every include of custom checkbox.
If you wanna to use your checkbox as default (without type property) you needs to set up `ARGON_CHECKBOX_DEFAULT_VIEW`

````typescript

@NgModule({
  // ...
  providers: [
    {
      provide: ARGON_CHECKBOX_VIEW,
      useValue: new CheckboxConfigModel(CustomCheckboxComponent, 'custom-checkbox'),
      multi: true
    },
    {
      provide: ARGON_CHECKBOX_DEFAULT_VIEW,
      useValue: 'custom-checkbox'
    }
  ]
})
export class CustomModule { }

```` 

##### PS: radio view

You can implement radio view with the same way. Just provide it with different tokens `ARGON_RADIO_VIEW` and `ARGON_RADIO_DEFAULT_VIEW`.


