#Checkbox and Radio basic usage

## Checkbox with label
````html
<ar-checkbox>
  <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
</ar-checkbox>

<ar-checkbox-label>
  Checkbox <ar-checkbox></ar-checkbox> inside content
</ar-checkbox-label>
````

## Placing label inside checkbox
````html
<ar-checkbox>
  <ar-checkbox-label before>Checkbox Label before the checkbox</ar-checkbox-label>
</ar-checkbox>
````

## Using `for` property
````html
<div>
  <ar-checkbox-label [for]="checkbox">Standalone checkbox label</ar-checkbox-label>
</div>
<div>
  <ar-checkbox #checkbox></ar-checkbox>
</div>
````

## Using `resize` property
Checkbox will have the same size as `<h2>` tag font size:

````html
<h2>
  <ar-checkbox-label [resize]="true">
    <ar-checkbox></ar-checkbox> Checkbox Label
  </ar-checkbox-label>
</h2>
````

##Disabled
````html
<ar-checkbox [disabled]="true">
  <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
</ar-checkbox>
````

> `<ar-bulk-checkbox>`, `<ar-list-checkbox>`, `<ar-radio>` can be used by the same way.

## With forms

`<ar-checkbox>`, `<ar-bulk-checkbox>`, `<ar-list-checkbox>`, `<ar-radio>` can be used both with `Template-driven forms` and `Reactive Forms`.

###Template-driven forms

Checkbox:

````html
<ar-checkbox [(ngModel)]="value" [disabled]="disabled">
   <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
</ar-checkbox>
````

Radio:

````html
<ar-radio [value]="1" [(ngModel)]="value">
  <ar-checkbox-label>Label 1</ar-checkbox-label>
</ar-radio>
<ar-radio [value]="2" [(ngModel)]="value">
  <ar-checkbox-label>Label 2</ar-checkbox-label>
</ar-radio>
<ar-radio [value]="3" [(ngModel)]="value">
  <ar-checkbox-label>Label 3</ar-checkbox-label>
</ar-radio>
````

###Reactive Forms
Checkbox:

````html
<ar-checkbox [formControl]="control">
   <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
</ar-checkbox>
````

Radio:

````html
<ar-radio [value]="1" [formControl]="control">
  <ar-checkbox-label>Label 1</ar-checkbox-label>
</ar-radio>
<ar-radio [value]="2" [formControl]="control">
  <ar-checkbox-label>Label 2</ar-checkbox-label>
</ar-radio>
````

###Standalone mode

> Standalone mode can be used only for `<ar-checkbox>` and `<ar-radio>` components!

Checkbox:

````html
<ar-checkbox (change)="checked = $event" [checked]="checked">
  <ar-checkbox-label>Checkbox</ar-checkbox-label>
</ar-checkbox>
````

Radio:

````typescript
const value = 1;

const = handleChange(value: number) {
  this.value = value;
};

const = checked(value: number): boolean {
  return this.value === value;
}

````

````html
<ar-radio (change)="handleChange($event)" [checked]="checked(1)" [value]="1">
  <ar-checkbox-label>Radio with value 1</ar-checkbox-label>
</ar-radio>
<ar-radio (change)="handleChange($event)" [checked]="checked(2)" [value]="2">
  <ar-checkbox-label>Radio with value 2</ar-checkbox-label>
</ar-radio>
````

