#@argon/checkbox

 - [Base usage](./BasicUsage.md)
 - [Radio Group](./RadioGroup.md)
 - [List operations](./List.md)
 - [Use objects as values](./CompareFn.md)
 - [Customize views](./Views.md)
 
 
##Components

### Common properties `BooleanControlShape`
The next properties are available for all boolean controls `<ar-checkbox>`, `<ar-bulk-checkbox>`, `<ar-list-checkbox>`, `<ar-radio>`.
All these components provided as `BooleanControlShape`.

|Property |Default value|Description
|---      |---          |---
|`@Input() disabled: boolean`|`undefined`| Disable control flag
|`@Input() tabIndex`|`0`| Html tabindex attribute value
|`@Input() compareFn: CompareFn`| `Comparators.defaultComparator` | Function for value compare `===` is using by default
|`@Input() type: string`| `undefined` | Type to select control view. If not defined, then default view will be used
|`@Input() viewData: any`| `undefined` | Any custom data which can be used inside view
|`@Input() checked: boolean`| `undefined` | Checked state. (!) This property should be used only for standalone controls
|`@Output() click: EventEmitter<void>`| ` `| Click event emitter
  
### Boolean controls
 
#### `<ar-checkbox>`
|Property |Default value|Description
|---      |---          |---
|`@Output() change: EventEmitter<boolean>`| ` ` | State changes event emitter. (!) This property should be used only for standalone controls

#### `<ar-bulk-checkbox>`
|Property |Default value|Description
|---      |---          |---
|`@Input() value: Array<any>`| `undefined` | Control values. Will be used for determinate state and update control values on click.  

#### `<ar-list-checkbox>`
|Property |Default value|Description
|---      |---          |---
|`@Input() value: any`| `undefined` | Control values. Will be used for determinate state and update control values on click.  

#### `<ar-radio>`
|Property |Default value|Description
|---      |---          |---
|`@Input() value: any`| `undefined` | Control values. Will be used for determinate state and update control values on click.  
|`@Output() change: EventEmitter<boolean>`| ` ` | State changes event emitter. (!) This property should be used only for standalone controls

### `<ar-radio-group>`
|Property |Default value|Description
|---      |---          |---
|`@Input() inline: boolean`| `undefined` | Flag for inline radio buttons styling

### `<ar-checkbox-label>`
|Property |Default value|Description
|---      |---          |---
|`@Input() resize: boolean`| `undefined` | Flag to make Boolean control size the same as current font size
|`@Input() for: BooleanControlShape`| `undefined` | Link to boolean control. Will be used to trigger control changes on label click for cases when label and control placed in different DOM nones.

##Directives

###`CheckboxViewDirective`, `RadioViewDirective`
This directives can be used for custom view implementation

|Property |Default value|Description
|---      |---          |---
|`@Input() disabled: boolean`|`undefined`| Disable control flag

###Interfaces

````typescript
interface CheckboxStateInterface {
  
  // Boolean control state
  state: CheckboxStateEnum;

  // Disabled state
  disabled: boolean;

  // Boolean control view data
  viewData?: any;

}


enum CheckboxStateEnum {

  on = 'on',

  off = 'off',

  indeterminate = 'indeterminate'

}
````

##Models

###`CheckboxConfigModel`

````typescript
import { CheckboxViewDirective } from '..';
import { Type } from '@angular/core';

export class CheckboxConfigModel {

  // View component for rendering with Boolean controls. (don't forget include it with entryComponents)
  public component: Type<CheckboxViewDirective>;
  
  // Uniq key for this view
  public type: string;
  
  // Determinate is current view has indeterminate state.
  public hasIndeterminateState: boolean = false;
  
}
````

##Providers
`ARGON_CHECKBOX_VIEW` - provide a list of views for checkbox control. Should be provided with `multi` flag. Provide `checkbox` and `checkbox-indeterminate` types by default.
> `InjectionToken<CheckboxConfigModel>('ARGON_CHECKBOX_VIEW')`

`ARGON_CHECKBOX_DEFAULT_VIEW` - default checkbox view key. This key will be used for rendering checkboxes without defined `type` property. Provide `checkbox` value by default
> `InjectionToken<string>('ARGON_CHECKBOX_DEFAULT_VIEW')`

`ARGON_RADIO_VIEW` - provide a list of views for radio control. Should be provided with `multi` flag. Provide `radio` type by default
>`InjectionToken<RadioConfigModel>('ARGON_RADIO_VIEW')`

`ARGON_RADIO_DEFAULT_VIEW` - default radio view key. This key will be used for rendering radio buttons without defined `type` property. Provide `radio` value by default
> `InjectionToken<string>('ARGON_RADIO_DEFAULT_VIEW')`


