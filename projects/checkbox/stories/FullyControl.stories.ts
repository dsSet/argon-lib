import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CheckboxModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';
import { CommonModule } from '@angular/common';

const notes = {
  markdown: require('../docs/BasicUsage.md').default + require('../docs/index.md').default
};


storiesOf('Argon|checkbox/Standalone', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        CheckboxModule,
      ]
    })
  )
  .add('Checkbox', () => ({
    props: {
      checked: false,
      handleChange(value: boolean) { this.checked = value; }
    },
    template: `
      <div>Status: {{ checked | json }}</div>
      <ar-checkbox (change)="handleChange($event)" [checked]="checked">
        <ar-checkbox-label>Checkbox</ar-checkbox-label>
      </ar-checkbox>
    `
  }), { notes })
  .add('Radiobutton', () => ({
    props: {
      value: 1,
      handleChange(value: number) { this.value = value; },
      checked(value: number): boolean { return this.value === value; }
    },
    template: `
      <div>Status: {{ value | json }}</div>
      <ar-radio (change)="handleChange($event)" [checked]="checked(1)" [value]="1">
        <ar-checkbox-label>Radio with value 1</ar-checkbox-label>
      </ar-radio>
      <ar-radio (change)="handleChange($event)" [checked]="checked(2)" [value]="2">
        <ar-checkbox-label>Radio with value 2</ar-checkbox-label>
      </ar-radio>
    `
  }), { notes });
