import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CheckboxModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/BasicUsage.md').default + require('../docs/index.md').default
};

storiesOf('Argon|checkbox/Basic properties', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CheckboxModule
      ]
    })
  )
  .add('With Label', () => ({
    template: `
      <div class="label-story-wrapper">
        <div>
          <ar-checkbox>
            <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
          </ar-checkbox>
        </div>
        <div>
          <ar-checkbox>
            <ar-checkbox-label before>Checkbox Label before the checkbox</ar-checkbox-label>
          </ar-checkbox>
        </div>
        <div>
          <div>
            <ar-checkbox-label [for]="checkbox">Standalone checkbox label</ar-checkbox-label>
          </div>
          <div>
            <ar-checkbox #checkbox></ar-checkbox>
          </div>
        </div>
        <div>
          <ar-checkbox-label>
            Checkbox <ar-checkbox></ar-checkbox> inside content
          </ar-checkbox-label>
        </div>
        <div>
          <ar-checkbox-label [for]="checkbox2">
            Checkbox <ar-checkbox #checkbox2></ar-checkbox> inside content with clickable text
          </ar-checkbox-label>
        </div>
        <div>
          <p>Label font sizes:</p>
          <h2>
            <ar-checkbox>
              <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
            </ar-checkbox>
          </h2>
          <h2>
            <ar-checkbox-label [for]="checkbox3">
              <ar-checkbox #checkbox3></ar-checkbox> Checkbox Label
            </ar-checkbox-label>
          </h2>
          <h2>
            <ar-checkbox-label [for]="checkbox4" [resize]="true">
              <ar-checkbox #checkbox4></ar-checkbox> Checkbox Label
            </ar-checkbox-label>
          </h2>
          <ar-checkbox-label [resize]="true" [for]="checkbox5">
            <small>
              <ar-checkbox #checkbox5></ar-checkbox> Checkbox Label
            </small>
          </ar-checkbox-label>
        </div>
      </div>
    `
  }), { notes })
  .add('Disabled state', () => ({
    template: `
      <div class="label-story-wrapper">
        <ar-checkbox [disabled]="true">
          <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
        </ar-checkbox>
        <ar-checkbox [disabled]="true" [ngModel]="true">
          <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
        </ar-checkbox>
      </div>
    `
  }), { notes });
