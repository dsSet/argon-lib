import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CheckboxModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';
import { CommonModule } from '@angular/common';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';

const notes = {
  markdown: require('../docs/CompareFn.md').default + require('../docs/index.md').default
};


storiesOf('Argon|checkbox/Use objects as value', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        CheckboxModule,
        ReactiveFormsModule
      ]
    })
  )
  .add('List Checkbox', () => ({
    props: {
      compareFn(itemA: any, itemB: any) { return itemA && itemB && itemA.id === itemB.id; },
      control: new UntypedFormControl({ id: 2, data: 'Random data' })
    },
    template: `
      <ar-bulk-checkbox type="checkbox-indeterminate" [formControl]="control" [value]="[{ id: 1 }, { id: 2}]" [compareFn]="compareFn">
        <ar-checkbox-label>Bulk operation</ar-checkbox-label>
      </ar-bulk-checkbox>
      <ar-list-checkbox [formControl]="control" [value]="{ id: 1, data: 'Item 1 data' }" [compareFn]="compareFn">
        <ar-checkbox-label>Checkbox 1</ar-checkbox-label>
      </ar-list-checkbox>
      <ar-list-checkbox [formControl]="control" [value]="{ id: 2, data: 'Item 2 data' }" [compareFn]="compareFn">
        <ar-checkbox-label>Checkbox 2</ar-checkbox-label>
      </ar-list-checkbox>
    `
  }), { notes })
  .add('Radiobutton', () => ({
    props: {
      compareFn(itemA: any, itemB: any) { return itemA && itemB && itemA.id === itemB.id; },
      control: new UntypedFormControl({ id: 2, data: 'Random data' })
    },
    template: `
      <ar-radio [formControl]="control" [value]="{ id: 1, data: 'Item 1 data' }" [compareFn]="compareFn">
        <ar-checkbox-label>Radio 1</ar-checkbox-label>
      </ar-radio>
      <ar-radio [formControl]="control" [value]="{ id: 2, data: 'Item 2 data' }" [compareFn]="compareFn">
        <ar-checkbox-label>Radio 2</ar-checkbox-label>
      </ar-radio>
    `
  }), { notes });
