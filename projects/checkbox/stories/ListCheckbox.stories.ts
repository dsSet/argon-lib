import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CheckboxModule } from './index';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

const notes = {
  markdown: require('../docs/List.md').default + require('../docs/index.md').default
};

storiesOf('Argon|checkbox/List Checkbox', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CheckboxModule,
        ReactiveFormsModule,
        CommonModule
      ]
    })
  )
  .add('Base Usage', () => ({
    props: {
      control: new UntypedFormControl([1, 4])
    },
    template: `
      <div>Control value: {{ control.value }}</div>
      <ar-list-checkbox [value]="1" [formControl]="control">
        <ar-checkbox-label>Toggle value 1</ar-checkbox-label>
      </ar-list-checkbox>
      <ar-list-checkbox [value]="2" [formControl]="control">
        <ar-checkbox-label>Toggle value 2</ar-checkbox-label>
      </ar-list-checkbox>
      <ar-list-checkbox [value]="3" [formControl]="control">
        <ar-checkbox-label>Toggle value 3</ar-checkbox-label>
      </ar-list-checkbox>
      <ar-list-checkbox [value]="4" [formControl]="control">
        <ar-checkbox-label>Toggle value 4</ar-checkbox-label>
      </ar-list-checkbox>
    `
  }), { notes })
  .add('Bulk Checkbox', () => ({
    props: {
      values: [1, 2, 3, 4, 5],
      control: new UntypedFormControl([2])
    },
    template: `
      <div>Control value: {{ control.value }}</div>
      <div>
        <ar-bulk-checkbox [formControl]="control" [value]="values" type="checkbox-indeterminate">
          <ar-checkbox-label>Bulk operations</ar-checkbox-label>
        </ar-bulk-checkbox>
      </div>
      <ar-list-checkbox *ngFor="let n of values" [formControl]="control" [value]="n">
        <ar-checkbox-label>Checkbox with value "{{ n }}"</ar-checkbox-label>
      </ar-list-checkbox>
    `
  }), { notes })
  .add('Bulk Checkbox (change value)', () => ({
    props: {
      values: [1, 2, 3, 4, 5],
      control: new UntypedFormControl([2]),
      changeValue() {
        this.values = [1, 3, 4, 5, 6, 7];
      }
    },
    template: `
      <div>Control value: {{ control.value }}</div>
      <div class="btn btn-sm" (click)="changeValue()">Change Value</div>
      <div>
        <ar-bulk-checkbox [formControl]="control" [value]="values" type="checkbox-indeterminate">
          <ar-checkbox-label>Bulk operations</ar-checkbox-label>
        </ar-bulk-checkbox>
      </div>
      <ar-list-checkbox *ngFor="let n of values" [formControl]="control" [value]="n">
        <ar-checkbox-label>Checkbox with value "{{ n }}"</ar-checkbox-label>
      </ar-list-checkbox>
    `
  }), { notes })
  .add('Use Custom CompareFn', () => ({
    props: {
      compareFn: (a: any, b: any) => a.id === b.id,
      values: [
        { id: 1, name: 'test 1' },
        { id: 2, name: 'test 2' },
        { id: 3, name: 'test 3' }
      ],
      control: new UntypedFormControl()
    },
    template: `
      <h5>Compare objects by 'id' field.</h5>
      <div>Original values: {{ values | json }}</div>
      <div>Control value: {{ control.value | json }}</div>
      <div>
        <ar-bulk-checkbox [formControl]="control" [value]="values" type="checkbox-indeterminate" [compareFn]="compareFn">
          <ar-checkbox-label>Bulk operations</ar-checkbox-label>
        </ar-bulk-checkbox>
      </div>
      <ar-list-checkbox
        *ngFor="let n of values"
        [formControl]="control"
        [value]="{ id: n.id, name: 'Different obj ' + n.id }"
        [compareFn]="compareFn"
      >
        <ar-checkbox-label>Checkbox with id "{{ n.id }}"</ar-checkbox-label>
      </ar-list-checkbox>
    `
  }), { notes })
  .add('List Checkbox( change value)', () => ({
    props: {
      checkbox1: 2,
      checkbox2: 4,
      checkbox3: 105,
      control: new UntypedFormControl()
    },
    template: `
      <h5>Compare objects by 'id' field.</h5>
      <pre>{{ control.value | json }}</pre>
      <div class="btn btn-sm" (click)="checkbox1 = checkbox1 + 1">Change Value</div>
      <div>
        <ar-bulk-checkbox [formControl]="control"
          [value]="[checkbox1, checkbox2, checkbox3]"
          type="checkbox-indeterminate"
        >
          <ar-checkbox-label>Bulk operations</ar-checkbox-label>
        </ar-bulk-checkbox>
      </div>
      <ar-list-checkbox [formControl]="control" [value]="checkbox1">
        <ar-checkbox-label>Checkbox 1 ({{ checkbox1 }})</ar-checkbox-label>
      </ar-list-checkbox>
      <ar-list-checkbox [formControl]="control" [value]="checkbox2">
        <ar-checkbox-label>Checkbox 2 ({{ checkbox2 }})</ar-checkbox-label>
      </ar-list-checkbox>
      <ar-list-checkbox [formControl]="control" [value]="checkbox3">
        <ar-checkbox-label>Checkbox 3 ({{ checkbox3 }})</ar-checkbox-label>
      </ar-list-checkbox>
    `
  }), { notes });
