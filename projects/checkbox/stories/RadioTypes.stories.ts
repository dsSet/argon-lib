import { moduleMetadata, storiesOf } from '@storybook/angular';
import {
  ARGON_RADIO_DEFAULT_VIEW, ARGON_RADIO_VIEW,
  CheckboxModule, RadioConfigModel
} from './index';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { CustomRadioComponent } from './components/CustomRadioComponent';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/Views.md').default + require('../docs/index.md').default
};


storiesOf('Argon|checkbox/Radio types', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        ReactiveFormsModule,
        CheckboxModule
      ]
    })
  )
  .add('provide custom radio type', () => ({
    props: {
      control: new UntypedFormControl('smile'),
      toggleControlStatus: (control) => {
        if (control.disabled) {
          control.enable();
        } else {
          control.disable();
        }
      }
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="toggleControlStatus(control)">
          Toggle disabled state
        </button>
      </div>
      <ar-radio-group [formControl]="control" [inline]="true">
        <ar-radio value="happy"></ar-radio>
        <ar-radio value="smile"></ar-radio>
        <ar-radio value="tongue"></ar-radio>
        <ar-radio value="sad"></ar-radio>
      </ar-radio-group>
    `,
    moduleMetadata: {
      declarations: [
        CustomRadioComponent
      ],
      entryComponents: [
        CustomRadioComponent
      ],
      providers: [
        { provide: ARGON_RADIO_VIEW, useValue: new RadioConfigModel(CustomRadioComponent, 'custom-radio'), multi: true },
        { provide: ARGON_RADIO_DEFAULT_VIEW, useValue: 'custom-radio' }
      ]
    }
  }), { notes });
