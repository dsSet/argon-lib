import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CheckboxModule } from './index';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import '!!style-loader!css-loader!sass-loader!./style.scss';
const notes = {
  markdown: require('../docs/BasicUsage.md').default + require('../docs/index.md').default
};

storiesOf('Argon|checkbox/Data binding', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        ReactiveFormsModule,
        CheckboxModule
      ]
    })
  )
  .add('Using ngModel data binding', () => ({
    props: {
      value: true,
      disabled: false
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="disabled = !disabled">
          Toggle disabled state
        </button>
        checkbox status is {{ value | json }}
      </div>
      <ar-checkbox [(ngModel)]="value" [disabled]="disabled">
         <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
      </ar-checkbox>
    `
  }), { notes })
  .add('Using reactive forms', () => ({
    props: {
      control: new UntypedFormControl(),
      toggleControlStatus: (control) => {
        if (control.disabled) {
          control.enable();
        } else {
          control.disable();
        }
      }
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="toggleControlStatus(control)">
          Toggle disabled state
        </button>
        checkbox status is {{ control.value | json }}
      </div>
      <ar-checkbox [formControl]="control">
         <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
      </ar-checkbox>
    `
  }), { notes });
