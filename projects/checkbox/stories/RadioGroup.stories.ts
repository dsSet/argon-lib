import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CheckboxModule } from './index';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/RadioGroup.md').default + require('../docs/index.md').default
};


storiesOf('Argon|checkbox/Radio Group', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CheckboxModule,
        ReactiveFormsModule,
        CommonModule,
      ]
    })
  )
  .add('Base usage', () => ({
    props: {
      values: [1, 2, 3, 4, 5, 6],
      control: new UntypedFormControl({ value: 2, disabled: true }),
      toggleControlStatus: (control) => {
        if (control.disabled) {
          control.enable();
        } else {
          control.disable();
        }
      }
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="toggleControlStatus(control)">
          Toggle disabled state
        </button>
        value is {{ control.value | json }}
      </div>
      <ar-radio-group [formControl]="control">
        <ar-radio *ngFor="let value of values" [value]="value">
          <ar-checkbox-label>Label {{ value }}</ar-checkbox-label>
        </ar-radio>
      </ar-radio-group>
    `
  }), { notes })
  .add('Inline view', () => ({
    props: {
      values: [1, 2, 3, 4, 5, 6],
      control: new UntypedFormControl({ value: 2, disabled: true }),
      toggleControlStatus: (control) => {
        if (control.disabled) {
          control.enable();
        } else {
          control.disable();
        }
      }
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="toggleControlStatus(control)">
          Toggle disabled state
        </button>
        value is {{ control.value | json }}
      </div>
      <div style="width: 300px">
        <ar-radio-group [formControl]="control" [inline]="true">
          <ar-radio *ngFor="let value of values" [value]="value">
            <ar-checkbox-label>Label {{ value }}</ar-checkbox-label>
          </ar-radio>
        </ar-radio-group>
      </div>
    `
  }), { notes });
