import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CheckboxModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';
import { CommonModule } from '@angular/common';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';

const notes = {
  markdown: require('../docs/BasicUsage.md').default + require('../docs/index.md').default
};

storiesOf('Argon|checkbox/Labels', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        CheckboxModule,
        ReactiveFormsModule
      ]
    })
  )
  .add('Disabled state', () => ({
    props: {
      control: new UntypedFormControl(),
      toggle() {
        if (this.control.disabled) {
          this.control.enable();
        } else {
          this.control.disable();
        }
      }
    },
    template: `
      <div class="btn btn-sm" (click)="toggle()">Toggle disabled</div>
      <ar-checkbox [formControl]="control">
        <ar-checkbox-label>Label Inside</ar-checkbox-label>
      </ar-checkbox>
      <ar-checkbox-label>
        <ar-checkbox [formControl]="control"></ar-checkbox>
        Label Outside
      </ar-checkbox-label>
    `
  }), { notes });
