import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ARGON_CHECKBOX_DEFAULT_VIEW, ARGON_CHECKBOX_VIEW, CheckboxConfigModel, CheckboxModule } from './index';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { CustomCheckboxComponent } from './components/CustomCheckboxComponent';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/Views.md').default + require('../docs/index.md').default
};


storiesOf('Argon|checkbox/Checkbox types', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        ReactiveFormsModule,
        CheckboxModule
      ]
    })
  )
  .add('checkbox', () => ({
    props: {
      control: new UntypedFormControl(),
      toggleControlStatus: (control) => {
        if (control.disabled) {
          control.enable();
        } else {
          control.disable();
        }
      }
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="toggleControlStatus(control)">
          Toggle disabled state
        </button>
        checkbox status is {{ control.value | json }}
      </div>
      <ar-checkbox [formControl]="control" type="checkbox">
         <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
      </ar-checkbox>
    `
  }), { notes })
  .add('checkbox-indeterminate', () => ({
    props: {
      control: new UntypedFormControl(),
      toggleControlStatus: (control) => {
        if (control.disabled) {
          control.enable();
        } else {
          control.disable();
        }
      }
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="toggleControlStatus(control)">
          Toggle disabled state
        </button>
        checkbox status is {{ control.value | json }}
      </div>
      <ar-checkbox [formControl]="control" type="checkbox-indeterminate">
         <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
      </ar-checkbox>
    `
  }), { notes })
  .add('provide custom checkbox type', () => ({
    props: {
      control: new UntypedFormControl(),
      toggleControlStatus: (control) => {
        if (control.disabled) {
          control.enable();
        } else {
          control.disable();
        }
      }
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="toggleControlStatus(control)">
          Toggle disabled state
        </button>
        checkbox status is {{ control.value | json }}
      </div>
      <ar-checkbox [formControl]="control" type="story-checkbox">
         <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
      </ar-checkbox>
    `,
    moduleMetadata: {
      declarations: [
        CustomCheckboxComponent
      ],
      entryComponents: [
        CustomCheckboxComponent
      ],
      providers: [
        { provide: ARGON_CHECKBOX_VIEW, useValue: new CheckboxConfigModel(CustomCheckboxComponent, 'story-checkbox', true), multi: true }
      ]
    }
  }), { notes })
  .add('provide custom checkbox type as default', () => ({
    props: {
      control: new UntypedFormControl(),
      toggleControlStatus: (control) => {
        if (control.disabled) {
          control.enable();
        } else {
          control.disable();
        }
      }
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="toggleControlStatus(control)">
          Toggle disabled state
        </button>
        checkbox status is {{ control.value | json }}
      </div>
      <ar-checkbox [formControl]="control">
         <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
      </ar-checkbox>
    `,
    moduleMetadata: {
      declarations: [
        CustomCheckboxComponent
      ],
      entryComponents: [
        CustomCheckboxComponent
      ],
      providers: [
        { provide: ARGON_CHECKBOX_VIEW, useValue: new CheckboxConfigModel(CustomCheckboxComponent, 'story-checkbox', true), multi: true },
        { provide: ARGON_CHECKBOX_DEFAULT_VIEW, useValue: 'story-checkbox' }
      ]
    }
  }), { notes });
