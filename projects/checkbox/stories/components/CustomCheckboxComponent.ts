import { CheckboxViewDirective } from '../index';
import { ChangeDetectionStrategy, Component } from '@angular/core';


@Component({
  selector: 'story-custom-checkbox-view',
  template: '{{ content }}',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomCheckboxComponent extends CheckboxViewDirective {

  public get content(): string {
    if (this.isIndeterminate) {
      return '?';
    }

    return this.isOn ? '+' : '-';
  }

}
