import { CheckboxViewDirective } from '../index';
import { ChangeDetectionStrategy, Component } from '@angular/core';


@Component({
  selector: 'story-custom-checkbox-view-data',
  template: '{{ content }} (view data is: {{ viewData | json }})',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ViewDataTextComponent extends CheckboxViewDirective {

  public get content(): string {
    if (this.isIndeterminate) {
      return '?';
    }

    return this.isOn ? '+' : '-';
  }

}
