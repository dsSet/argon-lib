import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CheckboxModule } from './index';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/BasicUsage.md').default + require('../docs/index.md').default
};

storiesOf('Argon|checkbox/Radio Button', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CheckboxModule,
        ReactiveFormsModule
      ]
    })
  )
  .add('Radio button', () => ({
    props: {
      value: null
    },
    template: `
      <ar-radio [value]="1" [(ngModel)]="value">
        <ar-checkbox-label>Label 1</ar-checkbox-label>
      </ar-radio>
      <ar-radio [value]="2" [(ngModel)]="value">
        <ar-checkbox-label>Label 2</ar-checkbox-label>
      </ar-radio>
      <ar-radio [value]="3" [(ngModel)]="value">
        <ar-checkbox-label>Label 3</ar-checkbox-label>
      </ar-radio>
    `
  }), { notes })
  .add('Disabled state test', () => ({
    props: {
      control: new UntypedFormControl(),
      toggleControlStatus: (control) => {
        if (control.disabled) {
          control.enable();
        } else {
          control.disable();
        }
      }
    },
    template: `
      <div class="data-story-control">
        <button class="btn btn-sm btn-secondary" (click)="toggleControlStatus(control)">
          Toggle disabled state
        </button>
        radio status is {{ control.value | json }}
      </div>
      <ar-radio [value]="1" [formControl]="control">
        <ar-checkbox-label>Label 1</ar-checkbox-label>
      </ar-radio>
      <ar-radio [value]="2" [formControl]="control">
        <ar-checkbox-label>Label 2</ar-checkbox-label>
      </ar-radio>
    `
  }), { notes })
  .add('Label Test', () => ({
    template: `
      <div class="label-story-wrapper">
        <div>
          <ar-radio>
            <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
          </ar-radio>
        </div>
        <div>
          <ar-radio>
            <ar-checkbox-label before>Checkbox Label before the checkbox</ar-checkbox-label>
          </ar-radio>
        </div>
        <div>
          <div>
            <ar-checkbox-label [for]="checkbox">Standalone checkbox label</ar-checkbox-label>
          </div>
          <div>
            <ar-radio #checkbox></ar-radio>
          </div>
        </div>
        <div>
          <ar-checkbox-label>
            Checkbox <ar-radio></ar-radio> inside content
          </ar-checkbox-label>
        </div>
        <div>
          <ar-checkbox-label [for]="checkbox2">
            Checkbox <ar-radio #checkbox2></ar-radio> inside content with clickable text
          </ar-checkbox-label>
        </div>
        <div>
          <p>Label font sizes:</p>
          <h2>
            <ar-radio>
              <ar-checkbox-label>Checkbox Label</ar-checkbox-label>
            </ar-radio>
          </h2>
          <h2>
            <ar-checkbox-label [for]="checkbox3">
              <ar-radio #checkbox3></ar-radio> Checkbox Label
            </ar-checkbox-label>
          </h2>
          <h2>
            <ar-checkbox-label [for]="checkbox4" [resize]="true">
              <ar-radio #checkbox4></ar-radio> Checkbox Label
            </ar-checkbox-label>
          </h2>
          <ar-checkbox-label [resize]="true" [for]="checkbox5">
            <small>
              <ar-radio #checkbox5></ar-radio> Checkbox Label
            </small>
          </ar-checkbox-label>
        </div>
      </div>
    `
  }), { notes });
