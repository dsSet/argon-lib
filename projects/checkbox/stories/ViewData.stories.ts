import { CommonModule } from '@angular/common';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ARGON_CHECKBOX_VIEW, CheckboxConfigModel, CheckboxModule } from './index';
import { ViewDataTextComponent } from './components/ViewDataTextComponent';

import '!!style-loader!css-loader!sass-loader!./style.scss';
const notes = {
  markdown: require('../docs/Views.md').default + require('../docs/index.md').default
};

storiesOf('Argon|checkbox/View Data', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        CheckboxModule,
      ],
      declarations: [
        ViewDataTextComponent
      ],
      entryComponents: [
        ViewDataTextComponent
      ],
      providers: [
        { provide: ARGON_CHECKBOX_VIEW, useValue: new CheckboxConfigModel(ViewDataTextComponent, 'story-checkbox', true), multi: true }
      ]
    })
  )
  .add('Pass view data to the control', () => ({
    template: `
      <ar-checkbox [viewData]="123" type="story-checkbox">
        <ar-checkbox-label>Checkbox</ar-checkbox-label>
      </ar-checkbox>
    `
  }), { notes })
  .add('Update view data', () => ({
    props: {
      viewData: 0,
      updateViewData() { this.viewData += 1; }
    },
    template: `
      <div class="btn btn-sm btn-warning" (click)="updateViewData()">Increment view data counter</div>
      <ar-checkbox [viewData]="viewData" type="story-checkbox">
        <ar-checkbox-label>Checkbox</ar-checkbox-label>
      </ar-checkbox>
    `
  }), { notes });
