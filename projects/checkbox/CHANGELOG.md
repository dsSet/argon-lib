# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@2.0.2...@argon/checkbox@2.0.3) (2023-12-12)


### Bug Fixes

* update control value accessors to handle disabled state correctly ([b9d29c5](https://bitbucket.org/dsSet/argon-lib/commits/b9d29c56fb1ba76738ce5fbf2824ede9b736af82))






## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@2.0.1...@argon/checkbox@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/checkbox





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@1.0.4...@argon/checkbox@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/checkbox






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@1.0.4...@argon/checkbox@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/checkbox






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@1.0.3...@argon/checkbox@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/checkbox





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@1.0.2...@argon/checkbox@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/checkbox





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@1.0.1...@argon/checkbox@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/checkbox






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.15...@argon/checkbox@1.0.1) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.15...@argon/checkbox@1.0.0) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))






## [0.3.15](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.14...@argon/checkbox@0.3.15) (2020-08-04)

**Note:** Version bump only for package @argon/checkbox






## [0.3.14](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.13...@argon/checkbox@0.3.14) (2020-05-21)

**Note:** Version bump only for package @argon/checkbox






## [0.3.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.12...@argon/checkbox@0.3.13) (2020-04-18)


### Bug Fixes

* **checkbox:** fix host binding props ([c7c3fe2](https://bitbucket.org/dsSet/argon-lib/commits/c7c3fe2))






## [0.3.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.11...@argon/checkbox@0.3.12) (2020-03-24)

**Note:** Version bump only for package @argon/checkbox





## [0.3.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.10...@argon/checkbox@0.3.11) (2020-03-24)

**Note:** Version bump only for package @argon/checkbox






## [0.3.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.9...@argon/checkbox@0.3.10) (2020-03-18)

**Note:** Version bump only for package @argon/checkbox





## [0.3.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.8...@argon/checkbox@0.3.9) (2020-02-14)

**Note:** Version bump only for package @argon/checkbox






## [0.3.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.7...@argon/checkbox@0.3.8) (2019-11-29)


### Bug Fixes

* **checkbox:** fix lifecircle hooks calls ([e279df9](https://bitbucket.org/dsSet/argon-lib/commits/e279df9))





## [0.3.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.6...@argon/checkbox@0.3.7) (2019-11-22)

**Note:** Version bump only for package @argon/checkbox





## [0.3.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.5...@argon/checkbox@0.3.6) (2019-11-22)

**Note:** Version bump only for package @argon/checkbox





## [0.3.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.4...@argon/checkbox@0.3.5) (2019-10-28)


### Bug Fixes

* **checkbox:** Add change output for checkbox to use pattern without control ([da9034c](https://bitbucket.org/dsSet/argon-lib/commits/da9034c))
* **checkbox:** Add disabled state for boolean shape and managed label ([1f8e02c](https://bitbucket.org/dsSet/argon-lib/commits/1f8e02c))
* **checkbox:** add viewData property for extend custom views ([5909ad2](https://bitbucket.org/dsSet/argon-lib/commits/5909ad2))
* **checkbox:** minor fixes ([61610ac](https://bitbucket.org/dsSet/argon-lib/commits/61610ac))





## [0.3.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.3...@argon/checkbox@0.3.4) (2019-10-11)


### Bug Fixes

* **checkbox:** fix constructor injections for view shapes ([28345f2](https://bitbucket.org/dsSet/argon-lib/commits/28345f2))





## [0.3.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.2...@argon/checkbox@0.3.3) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





## [0.3.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.1...@argon/checkbox@0.3.2) (2019-10-08)

**Note:** Version bump only for package @argon/checkbox





## [0.3.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.3.0...@argon/checkbox@0.3.1) (2019-09-21)


### Bug Fixes

* **checkbox:** remove change emitter from checkbox ([eeabd73](https://bitbucket.org/dsSet/argon-lib/commits/eeabd73))





# [0.3.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.2.1...@argon/checkbox@0.3.0) (2019-09-17)


### Features

* **checkbox:** Add ListCheckbox and BulkCheckbox components ([a266512](https://bitbucket.org/dsSet/argon-lib/commits/a266512))





## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.2.0...@argon/checkbox@0.2.1) (2019-09-13)

**Note:** Version bump only for package @argon/checkbox






# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.1.2...@argon/checkbox@0.2.0) (2019-09-08)


### Features

* **checkbox:** add radio and radiogroup components ([bfe1e64](https://bitbucket.org/dsSet/argon-lib/commits/bfe1e64))





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.1.1...@argon/checkbox@0.1.2) (2019-09-07)


### Bug Fixes

* **checkbox:** fix aot build ([f15cae7](https://bitbucket.org/dsSet/argon-lib/commits/f15cae7))





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/checkbox@0.1.0...@argon/checkbox@0.1.1) (2019-09-06)

**Note:** Version bump only for package @argon/checkbox





# 0.1.0 (2019-09-06)


### Features

* **checkbox:** add checkbox package ([6e34a94](https://bitbucket.org/dsSet/argon-lib/commits/6e34a94))
* **checkbox:** implement checkbox module ([12f3a91](https://bitbucket.org/dsSet/argon-lib/commits/12f3a91))
