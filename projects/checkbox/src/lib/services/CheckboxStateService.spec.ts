import { CheckboxStateService } from './CheckboxStateService';
import { CheckboxStateEnum } from '../types/CheckboxStateEnum';

describe('CheckboxStateService', () => {
  let service: CheckboxStateService;

  beforeEach(() => {
    service = new CheckboxStateService();
  });

  afterEach(() => {
    service.state.complete();
  });

  it('should have default state', () => {
    expect(service.state.value).toEqual({ state: CheckboxStateEnum.off, disabled: false, viewData: null });
  });

  it('value should return value from state', () => {
    service.state.next({ disabled: false, state: CheckboxStateEnum.on, viewData: null });
    expect(service.value).toEqual(true);
    service.state.next({ disabled: false, state: CheckboxStateEnum.off, viewData: null });
    expect(service.value).toEqual(false);
    service.state.next({ disabled: false, state: CheckboxStateEnum.indeterminate, viewData: null });
    expect(service.value).toEqual(null);
  });

  it('set value should call state changes', () => {
    service.disabled = true;
    spyOn(service.state, 'next');
    service.value = true;
    expect(service.state.next).toHaveBeenCalledWith({ disabled: true, state: CheckboxStateEnum.on, viewData: null });
    service.value = false;
    expect(service.state.next).toHaveBeenCalledWith({ disabled: true, state: CheckboxStateEnum.off, viewData: null });
    service.value = null;
    expect(service.state.next).toHaveBeenCalledWith({ disabled: true, state: CheckboxStateEnum.off, viewData: null });
    service.hasIndeterminateState = true;
    service.value = null;
    expect(service.state.next).toHaveBeenCalledWith({ disabled: true, state: CheckboxStateEnum.indeterminate, viewData: null });
  });

  it('disabled should return state value', () => {
    service.state.next({ disabled: false, state: CheckboxStateEnum.on });
    expect(service.disabled).toEqual(false);
    service.state.next({ disabled: true, state: CheckboxStateEnum.off });
    expect(service.disabled).toEqual(true);
  });

  it('set disabled should call state changes', () => {
    service.value = true;
    spyOn(service.state, 'next');
    service.disabled = true;
    expect(service.state.next).toHaveBeenCalledWith({ disabled: true, state: CheckboxStateEnum.on, viewData: null });
    service.disabled = false;
    expect(service.state.next).toHaveBeenCalledWith({ disabled: true, state: CheckboxStateEnum.on, viewData: null });
  });

  it('viewData should return state value', () => {
    service.state.next({ disabled: false, state: CheckboxStateEnum.on, viewData: 333 });
    expect(service.viewData).toEqual(333);
  });

  it('set disabled should call state changes', () => {
    service.value = true;
    service.disabled = true;
    spyOn(service.state, 'next');
    service.viewData = 85;
    expect(service.state.next).toHaveBeenCalledWith({ disabled: true, state: CheckboxStateEnum.on, viewData: 85 });
  });

  it('toggle() should inverse value', () => {
    service.value = true;
    service.toggle();
    expect(service.value).toEqual(false);

    service.value = false;
    service.toggle();
    expect(service.value).toEqual(true);

    service.value = null;
    service.toggle();
    expect(service.value).toEqual(true);
  });
});
