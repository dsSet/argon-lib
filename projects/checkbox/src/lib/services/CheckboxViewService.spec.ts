import { TestBed, waitForAsync } from '@angular/core/testing';
import { Injector, NO_ERRORS_SCHEMA } from '@angular/core';
import { CheckboxViewService } from './CheckboxViewService';
import {
  ARGON_CHECKBOX_DEFAULT_VIEW,
  ARGON_CHECKBOX_VIEW,
  ARGON_RADIO_DEFAULT_VIEW,
  ARGON_RADIO_VIEW
} from '../providers/CheckboxViewProvider';
import { CheckboxConfigModel } from '../models/CheckboxConfigModel';
import { CheckboxViewComponent } from '../components/CheckboxView/CheckboxViewComponent';
import { RadioConfigModel } from '../models/RadioConfigModel';
import { RadioViewComponent } from '../components/RadioView/RadioViewComponent';
import { ComponentPortal } from '@angular/cdk/portal';

describe('CheckboxViewService', () => {

  let service: CheckboxViewService;

  const checkboxConfig1 = new CheckboxConfigModel(CheckboxViewComponent, 'checkbox1');
  const checkboxConfig2 = new CheckboxConfigModel(CheckboxViewComponent, 'checkbox2', true);
  const radioConfig1 = new RadioConfigModel(RadioViewComponent, 'radio1');
  const radioConfig2 = new RadioConfigModel(RadioViewComponent, 'radio2');

  beforeEach(waitForAsync(() => {
      return TestBed.configureTestingModule({
        providers: [
          CheckboxViewService,
          { provide: ARGON_CHECKBOX_VIEW, useValue: checkboxConfig1, multi: true },
          { provide: ARGON_CHECKBOX_VIEW, useValue: checkboxConfig2, multi: true },
          { provide: ARGON_RADIO_VIEW, useValue: radioConfig1, multi: true },
          { provide: ARGON_RADIO_VIEW, useValue: radioConfig2, multi: true },
          { provide: ARGON_CHECKBOX_DEFAULT_VIEW, useValue: 'checkbox1' },
          { provide: ARGON_RADIO_DEFAULT_VIEW, useValue: 'radio2' },
        ],
        schemas: [ NO_ERRORS_SCHEMA ]
      }).compileComponents();
    }
  ));

  beforeEach(() => {
    service = TestBed.inject(CheckboxViewService) as any;
  });

  it('getCheckboxConfig should return default config without type', () => {
    expect(service.getCheckboxConfig()).toEqual(checkboxConfig1);
  });

  it('getCheckboxConfig should return config by type', () => {
    expect(service.getCheckboxConfig('checkbox2')).toEqual(checkboxConfig2);
  });

  it('getCheckboxConfig should throw error for unknown types', () => {
    expect(() => { service.getCheckboxConfig('mocktype'); } ).toThrowError();
  });

  it('getRadioConfig should return default config without type', () => {
    expect(service.getRadioConfig()).toEqual(radioConfig2);
  });

  it('getRadioConfig should return config by type', () => {
    expect(service.getRadioConfig('radio1')).toEqual(radioConfig1);
  });

  it('getRadioConfig should throw error for unknown types', () => {
    expect(() => { service.getRadioConfig('mocktype'); } ).toThrowError();
  });

  it('createCheckboxPortal should create portal from config', () => {
    const injector = TestBed.inject(Injector) as any;
    const expectedResult = new ComponentPortal(checkboxConfig2.component, null, injector);
    expect(service.createCheckboxPortal(injector, 'checkbox2')).toEqual(expectedResult);
  });

  it('createRadioPortal should create portal from config', () => {
    const injector = TestBed.inject(Injector) as any;
    const expectedResult = new ComponentPortal(radioConfig1.component, null, injector);
    expect(service.createRadioPortal(injector, 'radio1')).toEqual(expectedResult);
  });

  it('hasIndeterminateState should return flag from config', () => {
    expect(service.hasIndeterminateState('checkbox1')).toEqual(false);
    expect(service.hasIndeterminateState('checkbox2')).toEqual(true);
  });
});
