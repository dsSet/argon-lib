import { BooleanControlRegistryService } from './BooleanControlRegistryService';
import { ValueControlShape } from '../models/ValueControlShape';
import { SimpleChange, Directive } from '@angular/core';
import createSpy = jasmine.createSpy;
import { AbstractControl, UntypedFormControl } from '@angular/forms';

describe('BooleanControlRegistryService', () => {

  @Directive()
class MockShape extends ValueControlShape {
    fireUpdate = createSpy('fireUpdate');
    constructor(control: AbstractControl = null) {
      super(null, null, null);
      this.ngControl = { control } as any;
    }
    protected handleClickEvent() { }
    protected handleTypeChange(type?: SimpleChange) { }
  }

  let service: BooleanControlRegistryService;

  beforeEach(() => {
    service = new BooleanControlRegistryService();
  });

  it('shapes list should be empty array by default', () => {
    expect(service.shapesList).toEqual([]);
  });

  it('register should add shape', () => {
    const shape1 = new MockShape();
    service.register(shape1);
    expect(service.shapesList).toEqual([shape1]);
    service.register(shape1);
    expect(service.shapesList).toEqual([shape1]);
  });

  it('remove should remove shape', () => {
    const shape1 = new MockShape();
    const shape2 = new MockShape();
    const shape3 = new MockShape();
    service.register(shape1);
    service.register(shape2);
    service.register(shape3);
    expect(service.shapesList).toEqual([shape1, shape2, shape3]);
    service.remove(shape2);
    expect(service.shapesList).toEqual([shape1, shape3]);
    service.remove(shape1);
    expect(service.shapesList).toEqual([shape3]);
    service.remove(shape3);
    expect(service.shapesList).toEqual([]);
  });

  it('hasShape should check shape inside list', () => {
    const shape1 = new MockShape();
    const shape2 = new MockShape();
    const shape3 = new MockShape();
    service.register(shape1);
    service.register(shape2);
    expect(service.hasShape(shape1)).toBeTruthy();
    expect(service.hasShape(shape2)).toBeTruthy();
    expect(service.hasShape(shape3)).toBeFalsy();
  });

  it('select should call fireUpdate for all shapes with the same control', () => {
    const control = new UntypedFormControl();
    const value = 'mock value';
    const shape1 = new MockShape(control);
    const shape2 = new MockShape(control);
    service.register(shape1);
    service.register(shape2);
    service.select(shape1, value);
    expect(shape2.fireUpdate).toHaveBeenCalledWith(value);
    expect(shape1.fireUpdate).not.toHaveBeenCalled();
  });

  it('select should skip all shapes without ngControl', () => {
    const control = new UntypedFormControl();
    const value = 'mock value';
    const shape1 = new MockShape(control);
    const shape2 = new MockShape(control);
    shape2.ngControl = null;
    service.register(shape1);
    service.register(shape2);
    service.select(shape1, value);
    expect(shape2.fireUpdate).not.toHaveBeenCalled();
  });

  it('select should skip all shapes without ngControl.control', () => {
    const control = new UntypedFormControl();
    const value = 'mock value';
    const shape1 = new MockShape(control);
    const shape2 = new MockShape();
    service.register(shape1);
    service.register(shape2);
    service.select(shape1, value);
    expect(shape2.fireUpdate).not.toHaveBeenCalled();
  });

  it('select should skip all shapes with different ngControl.control', () => {
    const control = new UntypedFormControl();
    const control2 = new UntypedFormControl();
    const value = 'mock value';
    const shape1 = new MockShape(control);
    const shape2 = new MockShape(control2);
    service.register(shape1);
    service.register(shape2);
    service.select(shape1, value);
    expect(shape2.fireUpdate).not.toHaveBeenCalled();
  });
});
