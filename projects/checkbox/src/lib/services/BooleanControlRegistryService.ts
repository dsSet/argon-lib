import indexOf from 'lodash/indexOf';
import filter from 'lodash/filter';
import each from 'lodash/each';
import without from 'lodash/without';
import { Injectable } from '@angular/core';
import { ValueControlShape } from '../models/ValueControlShape';

@Injectable({
  providedIn: 'root'
})
export class BooleanControlRegistryService {

  private shapes: Array<ValueControlShape> = [];

  public get shapesList(): Array<ValueControlShape> { return this.shapes; }

  public register(control: ValueControlShape) {
    if (!this.hasShape(control)) {
      this.shapes.push(control);
    }
  }

  public remove(control: ValueControlShape) {
    this.shapes = without(this.shapes, control);
  }

  public hasShape(shape: ValueControlShape): boolean {
    return indexOf(this.shapes, shape) >= 0;
  }

  public select(control: ValueControlShape, value: any) {
    if (!control.ngControl || !control.ngControl.control) {
      return;
    }
    const toUpdate = filter(this.shapes,
      (shape: ValueControlShape) => {
        if (!shape.ngControl || !shape.ngControl.control) {
          return false;
        }

        return shape.ngControl.control === control.ngControl.control && shape !== control;
      }
    );
    each(toUpdate,
      (shape: ValueControlShape) => {
        shape.fireUpdate(value);
      }
    );
  }

}
