import { Injectable, OnDestroy } from '@angular/core';
import { Unsubscribable } from '@argon/tools';
import { BehaviorSubject } from 'rxjs';
import { CheckboxStateInterface } from '../interfaces/CheckboxStateInterface';
import { CheckboxStateEnum } from '../types/CheckboxStateEnum';

@Injectable()
export class CheckboxStateService implements OnDestroy {

  public state = new BehaviorSubject<CheckboxStateInterface>(this.createState());

  public data: any;

  public get value(): boolean {
    const value = this.state.value.state;
    if (value === CheckboxStateEnum.on) {
      return true;
    }

    if (value === CheckboxStateEnum.off) {
      return false;
    }

    return null;
  }

  public set value(value: boolean) {
    this.state.next(this.createState(value, this.disabled, this.viewData));
  }

  public get disabled(): boolean {
    return this.state.value.disabled;
  }

  public set disabled(disabled: boolean) {
    this.state.next(this.createState(this.value, disabled, this.viewData));
  }

  public set viewData(viewData: any) {
    this.state.next(this.createState(this.value, this.disabled, viewData));
  }

  public get viewData(): any {
    return this.state.value.viewData;
  }

  public hasIndeterminateState: boolean;

  @Unsubscribable
  ngOnDestroy(): void { }

  public toggle() {
    const currentValue = this.value;
    this.value = !currentValue;
  }

  private getCheckboxState(value?: boolean): CheckboxStateEnum {
    if (value === true) {
      return CheckboxStateEnum.on;
    }

    if (value === false) {
      return CheckboxStateEnum.off;
    }

    return this.hasIndeterminateState ? CheckboxStateEnum.indeterminate : CheckboxStateEnum.off;
  }

  private createState(value?: boolean, disabled?: boolean, viewData?: any): CheckboxStateInterface {
    return {
      state: this.getCheckboxState(value),
      disabled: Boolean(disabled),
      viewData: viewData || null
    };
  }

}
