import find from 'lodash/find';
import map from 'lodash/map';
import { Inject, Injectable, Injector } from '@angular/core';
import {
  ARGON_CHECKBOX_DEFAULT_VIEW,
  ARGON_CHECKBOX_VIEW,
  ARGON_RADIO_DEFAULT_VIEW,
  ARGON_RADIO_VIEW
} from '../providers/CheckboxViewProvider';
import { CheckboxConfigModel } from '../models/CheckboxConfigModel';
import { ComponentPortal } from '@angular/cdk/portal';
import { CheckboxViewDirective } from '../dircetives/CheckboxViewDirective';
import { RadioConfigModel } from '../models/RadioConfigModel';
import { RadioViewDirective } from '../dircetives/RadioViewDirective';

@Injectable({
  providedIn: 'root'
})
export class CheckboxViewService {

  constructor(
    @Inject(ARGON_CHECKBOX_VIEW) private checkboxViewConfig: Array<CheckboxConfigModel>,
    @Inject(ARGON_CHECKBOX_DEFAULT_VIEW) private checkboxDefault: string,
    @Inject(ARGON_RADIO_VIEW) private radioViewConfig: Array<RadioConfigModel>,
    @Inject(ARGON_RADIO_DEFAULT_VIEW) private radioDefault: string,
  ) { }

  public getCheckboxConfig(type: string = this.checkboxDefault): CheckboxConfigModel {
    const config = find(this.checkboxViewConfig, { type });
    if (!config) {
      const availableConfig = map(this.checkboxViewConfig, 'type');
      throw new Error(`Argon checkbox: config for "${type}" is not available. Current types are ${availableConfig}`);
    }

    return config;
  }

  public createCheckboxPortal(injector: Injector, type?: string): ComponentPortal<CheckboxViewDirective> {
    const config = this.getCheckboxConfig(type);
    return new ComponentPortal(config.component, null, injector);
  }

  public hasIndeterminateState(type?: string): boolean {
    const config = this.getCheckboxConfig(type);
    return config.hasIndeterminateState;
  }

  public getRadioConfig(type: string = this.radioDefault): RadioConfigModel {
    const config = find(this.radioViewConfig, { type });
    if (!config) {
      const availableConfig = map(this.radioViewConfig, 'type');
      throw new Error(`Argon radio: config for "${type}" is not available. Current types are ${availableConfig}`);
    }

    return config;
  }

  public createRadioPortal(injector: Injector, type?: string): ComponentPortal<RadioViewDirective> {
    const config = this.getRadioConfig(type);
    return new ComponentPortal(config.component, null, injector);
  }
}
