import { ValueControlShape } from './ValueControlShape';
import { Component, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import createSpy = jasmine.createSpy;
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CheckboxStateServiceMock } from '../../test/CheckboxStateServiceMock';
import { BooleanControlRegistryServiceMock } from '../../test/BooleanControlRegistryServiceMock';
import { CheckboxStateService } from '../services/CheckboxStateService';
import { BooleanControlRegistryService } from '../services/BooleanControlRegistryService';
import { CheckboxViewService } from '../services/CheckboxViewService';
import { CheckboxViewServiceMock } from '../../test/CheckboxViewServiceMock';
import { KeyboardServiceMock } from '../../../../../test/KeyboardServiceMock';
import { KeyboardService } from '@argon/keyboard';

describe('ValueControlShape', () => {

  @Component({ selector: 'spec-component', template: '' })
  class MockComponent extends ValueControlShape {

    public mockClick = createSpy('mockClick');
    public mockTypeChange = createSpy('mockTypeChange');
    public get registryService(): BooleanControlRegistryService { return this.registry; }

    public triggerOnChange(value: any) {
      this.onChange(value);
    }

    protected handleClickEvent() {
      this.mockClick();
    }

    protected handleTypeChange(type?: SimpleChange) {
      this.mockTypeChange(type);
    }
  }

  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;
  let stateService: CheckboxStateServiceMock;
  let registryService: BooleanControlRegistryServiceMock;

  beforeEach(waitForAsync(() => {
      return TestBed.configureTestingModule({
        declarations: [
          MockComponent
        ],
        providers: [
          { provide: CheckboxViewService, useClass: CheckboxViewServiceMock },
          { provide: KeyboardService, useClass: KeyboardServiceMock },
          { provide: CheckboxStateService, useClass: CheckboxStateServiceMock },
          { provide: BooleanControlRegistryService, useClass: BooleanControlRegistryServiceMock }
        ],
        schemas: [ NO_ERRORS_SCHEMA ]
      }).compileComponents();
    }

  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
    stateService = TestBed.inject(CheckboxStateService) as any;
    registryService = TestBed.inject(BooleanControlRegistryService) as any;
    fixture.detectChanges();
  });

  it('should have registry service onInit', () => {
    expect(component.registryService).toBeDefined();
  });

  it('should have ngControl onInit', () => {
    expect(component.ngControl).toBeDefined();
  });

  it('should register in registry onInit', () => {
    expect(registryService.register).toHaveBeenCalledWith(component);
  });

  it('should update data in stateService on value change', () => {
    const expectedValue = 'mock-value';
    const value = new SimpleChange(null, expectedValue, false);
    component.ngOnChanges({ value });
    expect(stateService.data).toEqual(expectedValue);
  });

  it('should not update data in stateService if has no value changes', () => {
    const expectedValue = 'mock-value';
    stateService.data = expectedValue;
    component.ngOnChanges({ });
    expect(stateService.data).toEqual(expectedValue);
  });

  it('should unregister from registry on destroy', () => {
    component.ngOnDestroy();
    expect(registryService.remove).toHaveBeenCalledWith(component);
  });

  it('registerOnChange should extend original change function with registry select call', () => {
    const originalChangeFn = createSpy('originalChangeFn');
    const value = 'mock-value';
    component.registerOnChange(originalChangeFn);
    component.triggerOnChange(value);
    expect(originalChangeFn).toHaveBeenCalledWith(value);
    expect(registryService.select).toHaveBeenCalledWith(component, value);
  });

  it('fireUpdate should call writeValue', () => {
    const value = 'mock data';
    spyOn(component, 'writeValue');
    component.fireUpdate(value);
    expect(component.writeValue).toHaveBeenCalledWith(value);
  });
});
