import { BooleanControlShape } from './BooleanControlShape';
import createSpy = jasmine.createSpy;
import { Component, Injector, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CheckboxStateServiceMock } from '../../test/CheckboxStateServiceMock';
import { CheckboxViewServiceMock } from '../../test/CheckboxViewServiceMock';
import { CheckboxViewService } from '../services/CheckboxViewService';
import { KeyboardServiceMock } from '../../../../../test/KeyboardServiceMock';
import { CheckboxStateService } from '../services/CheckboxStateService';
import { KeyboardService } from '@argon/keyboard';
import createSpyObj = jasmine.createSpyObj;
import { HostBindingTestHelper } from '../../../../../test/helpers/HostBindingTestHelper';

describe('BooleanControlShape', () => {

  @Component({ selector: 'spec-component', template: '' })
  class SpecComponent extends BooleanControlShape {
    public mockTypeChanged = createSpy('mockTypeChanged');
    public mockClickEvent = createSpy('mockTypeChanged');
    public get onChangeFn(): any { return this.onChange; }
    public get onTouchFn(): any { return this.onTouch; }
    constructor(componentStateService: CheckboxStateService, componentViewService: CheckboxViewService, injector: Injector) {
      super(componentStateService, componentViewService, injector);
    }
    protected handleTypeChange(type?: SimpleChange) { this.mockTypeChanged(type); }
    protected handleClickEvent() { this.mockClickEvent(); }
  }

  let component: SpecComponent;
  let fixture: ComponentFixture<SpecComponent>;
  let stateService: CheckboxStateServiceMock;
  let viewService: CheckboxViewServiceMock;

  beforeEach(waitForAsync(() => {
      return TestBed.configureTestingModule({
        declarations: [
          SpecComponent
        ],
        providers: [
          { provide: CheckboxViewService, useClass: CheckboxViewServiceMock },
          { provide: KeyboardService, useClass: KeyboardServiceMock },
          { provide: CheckboxStateService, useClass: CheckboxStateServiceMock }
        ],
        schemas: [ NO_ERRORS_SCHEMA ]
      }).compileComponents();
    }

  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecComponent);
    component = fixture.componentInstance;
    viewService = TestBed.inject(CheckboxViewService) as any;
    stateService = TestBed.inject(CheckboxStateService) as any;
  });

  it('checked setter should update stateService value', () => {
    expect(stateService.value).toBeFalsy();
    component.checked = true;
    expect(stateService.value).toBeTruthy();
  });

  it('disabled should bind to the state service', () => {
    component.ngOnChanges({ disabled: new SimpleChange(true, false, false) });
    expect(stateService.disabled).toEqual(false);

    component.ngOnChanges({ disabled: new SimpleChange(false, true, false) });
    expect(stateService.disabled).toEqual(true);

    component.ngOnChanges({ disabled: new SimpleChange(true, null, false) });
    expect(stateService.disabled).toEqual(false);
  });

  it('handleClick should call event.stopPropagation', () => {
    const event = createSpyObj(['stopPropagation']);
    component.disabled = false;
    component.handleClick(event);
    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('handleClick should call handleClickEvent', () => {
    component.disabled = false;
    component.handleClick();
    expect(component.mockClickEvent).toHaveBeenCalled();
  });

  it('handleClick should not call handleClickEvent in disabled state', () => {
    component.disabled = true;
    component.handleClick();
    expect(component.mockClickEvent).not.toHaveBeenCalled();
  });

  it('ngOnInit should call handleTypeChange if type not exist', () => {
    component.type = null;
    component.ngOnInit();
    expect(component.mockTypeChanged).toHaveBeenCalled();
    expect(component.mockTypeChanged.calls.first().args[0]).toEqual(new SimpleChange(null, null, true));
  });

  it('ngOnInit should not call handleTypeChange if type exist', () => {
    component.type = 'fake-type';
    component.ngOnInit();
    expect(component.mockTypeChanged).not.toHaveBeenCalled();
  });

  it('ngOnChanges should not call handleTypeChange', () => {
    const type = new SimpleChange('prev', 'next', false);
    component.ngOnChanges({ type });
    expect(component.mockTypeChanged).toHaveBeenCalledWith(type);
  });

  it('registerOnChange should set disabled to false', () => {
    fixture.detectChanges();
    component.disabled = true;
    component.registerOnChange(() => { });
    stateService.emitNext();
    fixture.detectChanges();
    expect(component.disabled).toEqual(false);
  });

  it('registerOnChange should store onChangeFn', () => {
    const onChangeFn = () => 'Fake On Change Fn';
    component.registerOnChange(onChangeFn);
    expect(component.onChangeFn).toEqual(onChangeFn);
  });

  it('registerOnTouched should store onTouchFn', () => {
    const onTouchFn = () => 'Fake On Touch Fn';
    component.registerOnTouched(onTouchFn);
    expect(component.onTouchFn).toEqual(onTouchFn);
  });

  it('setDisabledState should switch disabled state', () => {
    fixture.detectChanges();
    component.disabled = false;
    component.setDisabledState(true);
    stateService.emitNext();
    fixture.detectChanges();
    expect(component.disabled).toEqual(true);
    component.setDisabledState(false);
    stateService.emitNext();
    fixture.detectChanges();
    expect(component.disabled).toEqual(false);
  });

  it('writeValue should update state value', () => {
    const value = true;
    component.writeValue(value);
    expect(stateService.value).toEqual(value);
  });

  it('className should toggle .ar-boolean class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-boolean');
  });

  it('disabled should toggle .ar-boolean--disabled class', () => {
    fixture.detectChanges();
    component.disabled = true;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-boolean--disabled').toBeTruthy();
    component.disabled = false;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-boolean--disabled').toBeFalsy();
  });

  it('set viewData should update state service view data', () => {
    const viewData = 123123;
    component.viewData = viewData;
    fixture.detectChanges();
    expect(stateService.viewData).toEqual(viewData);
  });
});
