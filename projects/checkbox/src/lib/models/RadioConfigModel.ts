import { Type } from '@angular/core';
import { RadioViewDirective } from '../dircetives/RadioViewDirective';

export class RadioConfigModel {

  constructor(
    public component: Type<RadioViewDirective>,
    public type: string
  ) { }

}
