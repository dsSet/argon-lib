import { CheckboxViewDirective } from '../dircetives/CheckboxViewDirective';
import { Type } from '@angular/core';

export class CheckboxConfigModel {

  constructor(
    public component: Type<CheckboxViewDirective>,
    public type: string,
    public hasIndeterminateState: boolean = false
  ) { }

}
