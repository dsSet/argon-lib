import { BooleanShape } from './BooleanShape';
import { CheckboxStateEnum } from '../types/CheckboxStateEnum';
import { Component, ElementRef, Injector, NO_ERRORS_SCHEMA } from '@angular/core';
import { CheckboxStateService } from '../services/CheckboxStateService';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CheckboxStateServiceMock } from '../../test/CheckboxStateServiceMock';
import { HostBindingTestHelper } from '../../../../../test/helpers/HostBindingTestHelper';
import { Subscription } from 'rxjs';
import createSpy = jasmine.createSpy;

describe('BooleanShape', () => {

  @Component({ selector: 'spec-component', template: '' })
  class SpecComponent extends BooleanShape {
    public getStateClassMock = createSpy('getStateClassMock').and.callFake((state: any) => {
      if (state === CheckboxStateEnum.on) {
        return 'mock-on';
      }

      return 'mock-off';
    });

    constructor(element: ElementRef<any>, injector: Injector) {
      super(element, injector);
    }

    protected getStateClass(state: CheckboxStateEnum): string {
      return this.getStateClassMock(state);
    }
  }

  let component: SpecComponent;
  let fixture: ComponentFixture<SpecComponent>;
  let stateService: CheckboxStateServiceMock;

  beforeEach(waitForAsync(() => {
      return TestBed.configureTestingModule({
        declarations: [
          SpecComponent
        ],
        providers: [
          { provide: CheckboxStateService, useClass: CheckboxStateServiceMock }
        ],
        schemas: [ NO_ERRORS_SCHEMA ]
      }).compileComponents();
    }

  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecComponent);
    component = fixture.componentInstance;
    stateService = TestBed.inject(CheckboxStateService) as any;
  });

  it('should have default values', () => {
    expect(component.disabled).toEqual(false);
    expect(component.className).toEqual(true);
  });

  it('state change should update current state', () => {
    component.state = CheckboxStateEnum.indeterminate;
    expect(component.currentState).toEqual(CheckboxStateEnum.indeterminate);
  });

  it('state change should update class name', () => {
    spyOn(stateService.state, 'subscribe').and.returnValue(new Subscription());
    component.currentState = CheckboxStateEnum.off;
    component.state = CheckboxStateEnum.on;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'mock-on').toBeTruthy();
    expect(component.getStateClassMock).toHaveBeenCalledWith(CheckboxStateEnum.off);
    expect(component.getStateClassMock).toHaveBeenCalledWith(CheckboxStateEnum.on);
  });

  it('should not change className if next state is the same as current state', () => {
    spyOn(stateService.state, 'subscribe').and.returnValue(new Subscription());
    component.currentState = CheckboxStateEnum.on;
    component.state = CheckboxStateEnum.on;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'mock-on').toBeFalsy();
    expect(component.getStateClassMock).not.toHaveBeenCalled();
  });

  it('isOn should return true if current state is on', () => {
    component.currentState = CheckboxStateEnum.on;
    expect(component.isOn).toBeTruthy();
    component.currentState = CheckboxStateEnum.off;
    expect(component.isOn).toBeFalsy();
  });

  it('isOff should return true if current state is off', () => {
    component.currentState = CheckboxStateEnum.off;
    expect(component.isOff).toBeTruthy();
    component.currentState = CheckboxStateEnum.on;
    expect(component.isOff).toBeFalsy();
  });

  it('isIndeterminate should return true if current state is indeterminate', () => {
    component.currentState = CheckboxStateEnum.indeterminate;
    expect(component.isIndeterminate).toBeTruthy();
    component.currentState = CheckboxStateEnum.on;
    expect(component.isIndeterminate).toBeFalsy();
  });

  it('ngOnInit should subscribe to the state', () => {
    spyOn(stateService.state, 'subscribe').and.returnValue(new Subscription());
    component.ngOnInit();
    expect(stateService.state.subscribe).toHaveBeenCalled();
  });

  it('state changes should update component state', () => {
    fixture.detectChanges();
    component.state = CheckboxStateEnum.on;
    component.disabled = true;
    stateService.state.next({ state: CheckboxStateEnum.off, disabled: false, viewData: 321 });
    expect(component.currentState).toEqual(CheckboxStateEnum.off);
    expect(component.disabled).toEqual(false);
    expect(component.viewData).toEqual(321);
  });
});
