import { ChangeDetectorRef, ElementRef, Injector, Input, OnInit, Renderer2, Directive } from '@angular/core';
import { CheckboxStateEnum } from '../types/CheckboxStateEnum';
import { CheckboxStateService } from '../services/CheckboxStateService';
import { CheckboxStateInterface } from '../interfaces/CheckboxStateInterface';

@Directive()
export abstract class BooleanShape implements OnInit {

  @Input() public disabled = false;

  public viewData: any;

  public className = true;

  public set state(value: CheckboxStateEnum) {
    if (value === this.currentState) {
      return;
    }

    this.removeClassByState(this.currentState);
    this.addClassByState(value);
    this.currentState = value;
  }

  public get isOn(): boolean {
    return this.currentState === CheckboxStateEnum.on;
  }

  public get isOff(): boolean {
    return this.currentState === CheckboxStateEnum.off;
  }

  public get isIndeterminate(): boolean {
    return this.currentState === CheckboxStateEnum.indeterminate;
  }

  public currentState: CheckboxStateEnum;

  protected renderer: Renderer2;

  protected stateService: CheckboxStateService;

  protected changeDetector: ChangeDetectorRef;

  protected constructor(
    protected element: ElementRef,
    protected injector: Injector
  ) {
    this.renderer = this.injector.get(Renderer2);
    this.stateService = this.injector.get(CheckboxStateService);
    this.changeDetector = this.injector.get(ChangeDetectorRef);
  }

  ngOnInit(): void {
    this.stateService.state.subscribe(this.handleStateChange);
  }

  protected handleStateChange = ({ disabled, state, viewData }: CheckboxStateInterface) => {
    this.state = state;
    this.disabled = disabled;
    this.viewData = viewData;
    this.changeDetector.markForCheck();
  }

  protected removeClassByState(state: CheckboxStateEnum) {
    const className = this.getStateClass(state);
    if (className) {
      this.renderer.removeClass(this.element.nativeElement, className);
    }
  }

  protected addClassByState(state: CheckboxStateEnum) {
    const className = this.getStateClass(state);
    if (className) {
      this.renderer.addClass(this.element.nativeElement, className);
    }
  }

  protected abstract getStateClass(state: CheckboxStateEnum): string;

}
