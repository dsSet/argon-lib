import { ControlValueAccessor } from '@angular/forms';
import {
  EventEmitter,
  HostBinding,
  HostListener,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChange,
  SimpleChanges,
  Directive
} from '@angular/core';
import { UIShape, UITabindexMixin, CompareValuesMixin, CompareFn, Unsubscribable } from '@argon/tools';
import { ComponentPortal } from '@angular/cdk/portal';
import { Subscription } from 'rxjs';
import { BooleanShape } from './BooleanShape';
import { CheckboxStateService } from '../services/CheckboxStateService';
import { CheckboxViewService } from '../services/CheckboxViewService';

@Directive()
export abstract class BooleanControlShape
  extends CompareValuesMixin(UITabindexMixin(UIShape))
  implements ControlValueAccessor, OnInit, OnChanges, OnDestroy {

  @Output() click = new EventEmitter<void>();

  @Input() compareFn: CompareFn;

  @Input() type: string;

  @Input() set viewData(viewData: any) {
    this.stateService.viewData = viewData;
  }

  @Input() set checked(value: boolean) {
    this.stateService.value = Boolean(value);
  }

  @HostBinding('class.ar-boolean') public className = true;

  @HostBinding('class.ar-boolean--disabled')
  @Input() public disabled: boolean;

  public view: ComponentPortal<BooleanShape>;

  private disabledSubscription: Subscription;

  constructor(
    protected stateService: CheckboxStateService,
    protected viewService: CheckboxViewService,
    injector: Injector
  ) {
    super(injector);
  }

  @HostListener('click', ['$event'])
  public handleClick = (event?: Event) => {
    if (!this.disabled) {
      if (event) {
        event.stopPropagation();
      }
      this.handleClickEvent();
    }
  }

  ngOnInit(): void {
    super.ngOnInit();
    if (!this.type) {
      const change = new SimpleChange(this.type, this.type, true);
      this.handleTypeChange(change);
    }

    this.disabledSubscription = this.stateService.state.subscribe((state) => {
      this.disabled = Boolean(state?.disabled);
    });
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { type, disabled } = changes;
    this.handleTypeChange(type);

    if (disabled && disabled.currentValue !== this.stateService.disabled) {
      this.stateService.disabled = Boolean(disabled.currentValue);
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.stateService.disabled = isDisabled;
  }

  writeValue(obj: any): void {
    this.stateService.value = obj;
  }

  protected abstract handleTypeChange(type?: SimpleChange);

  protected abstract handleClickEvent();

  protected onChange = (value: any) => { };

  protected onTouch = () => { };

}
