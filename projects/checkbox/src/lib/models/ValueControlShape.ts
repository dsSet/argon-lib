import { BooleanControlShape } from './BooleanControlShape';
import { Injector, Input, OnChanges, OnDestroy, OnInit, SimpleChange, SimpleChanges, Directive } from '@angular/core';
import { NgControl } from '@angular/forms';
import { CheckboxStateService } from '../services/CheckboxStateService';
import { CheckboxViewService } from '../services/CheckboxViewService';
import { BooleanControlRegistryService } from '../services/BooleanControlRegistryService';
import { CompareFn } from '@argon/tools';


@Directive()
export abstract class ValueControlShape extends BooleanControlShape implements OnInit, OnDestroy, OnChanges {

  @Input() compareFn: CompareFn;

  @Input() value: any;

  public ngControl: NgControl;

  protected registry: BooleanControlRegistryService;

  constructor(
    stateService: CheckboxStateService,
    viewService: CheckboxViewService,
    injector: Injector
  ) {
    super(stateService, viewService, injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.ngControl = this.injector.get(NgControl, null, { optional: true });
    this.registry = this.injector.get(BooleanControlRegistryService);
    this.registry.register(this);
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
    const { value } = changes;
    this.handleValueChange(value);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.registry.remove(this);
  }

  registerOnChange(fn: any): void {
    super.registerOnChange((value: any) => {
      fn(value);
      this.registry.select(this, value);
    });
  }

  public fireUpdate = (value: any) => {
    this.writeValue(value);
  }

  protected handleValueChange(value?: SimpleChange) {
    if (!value) {
      return;
    }

    this.stateService.data = value.currentValue;
  }
}
