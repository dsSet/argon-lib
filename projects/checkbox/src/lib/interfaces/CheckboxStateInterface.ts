import { CheckboxStateEnum } from '../types/CheckboxStateEnum';

export interface CheckboxStateInterface {

  state: CheckboxStateEnum;

  disabled: boolean;

  viewData?: any;

}
