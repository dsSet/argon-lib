import { InjectionToken } from '@angular/core';
import { CheckboxConfigModel } from '../models/CheckboxConfigModel';
import { RadioConfigModel } from '../models/RadioConfigModel';

export const ARGON_CHECKBOX_VIEW = new InjectionToken<CheckboxConfigModel>('ARGON_CHECKBOX_VIEW');

export const ARGON_CHECKBOX_DEFAULT_VIEW = new InjectionToken<string>('ARGON_CHECKBOX_DEFAULT_VIEW');

export const ARGON_RADIO_VIEW = new InjectionToken<RadioConfigModel>('ARGON_RADIO_VIEW');

export const ARGON_RADIO_DEFAULT_VIEW = new InjectionToken<string>('ARGON_RADIO_DEFAULT_VIEW');
