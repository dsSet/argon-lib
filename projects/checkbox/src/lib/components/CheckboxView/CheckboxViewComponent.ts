import { ChangeDetectionStrategy, Component, ElementRef, HostBinding, Injector } from '@angular/core';
import { CheckboxViewDirective } from '../../dircetives/CheckboxViewDirective';

@Component({
  selector: 'ar-checkbox-view',
  templateUrl: './CheckboxViewComponent.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxViewComponent extends CheckboxViewDirective {

  @HostBinding('class.ar-default-checkbox') className = true;

  constructor(element: ElementRef<any>, injector: Injector) {
    super(element, injector);
  }
}
