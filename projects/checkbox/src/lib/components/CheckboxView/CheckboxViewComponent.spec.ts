import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CheckboxViewComponent } from './CheckboxViewComponent';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CheckboxStateService } from '../../services/CheckboxStateService';
import { CheckboxStateServiceMock } from '../../../test/CheckboxStateServiceMock';
import { CheckboxViewDirective } from '../../dircetives/CheckboxViewDirective';

describe('CheckboxViewComponent', () => {
  let component: CheckboxViewComponent;
  let fixture: ComponentFixture<CheckboxViewComponent>;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      declarations: [
        CheckboxViewComponent,
        CheckboxViewDirective,
      ],
      providers: [
        { provide: CheckboxStateService, useClass: CheckboxStateServiceMock }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxViewComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-default-checkbox class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-default-checkbox');
  });
});
