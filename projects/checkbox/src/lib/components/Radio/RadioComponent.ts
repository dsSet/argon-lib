import {
  ChangeDetectionStrategy,
  Component, EventEmitter,
  forwardRef,
  HostBinding,
  Injector,
  Input, OnChanges, OnDestroy, OnInit, Output,
  SimpleChange, SimpleChanges
} from '@angular/core';
import { CheckboxStateService } from '../../services/CheckboxStateService';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CheckboxViewService } from '../../services/CheckboxViewService';
import { CompareFn } from '@argon/tools';
import { ValueControlShape } from '../../models/ValueControlShape';
import { BooleanControlShape } from '../../models/BooleanControlShape';

@Component({
  selector: 'ar-radio',
  template: `
    <ng-content select="[before]"></ng-content>
    <ng-template [cdkPortalOutlet]="view"></ng-template>
    <ng-content></ng-content>
  `,
  providers: [
    CheckboxStateService,
    { provide: BooleanControlShape, useExisting: forwardRef(() => RadioComponent) },
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => RadioComponent), multi: true }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    role: 'radio',
    '[class.disabled]': 'disabled',
    '[attr.disabled]': 'attrDisabled',
    '[attr.tabindex]': 'attrTabIndex'
  }
})
export class RadioComponent extends ValueControlShape implements OnInit, OnChanges, OnDestroy {

  @HostBinding('class.ar-radio') className;

  @Input() compareFn: CompareFn;

  @Output() change = new EventEmitter<boolean>();

  @Input() disabled: boolean;

  @Output() click = new EventEmitter<void>();

  constructor(stateService: CheckboxStateService, viewService: CheckboxViewService, injector: Injector) {
    super(stateService, viewService, injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  writeValue(obj: any): void {
    const nextState = this.isEqual(obj, this.value);
    if (nextState !== this.stateService.value) {
      this.stateService.value = nextState;
      this.change.emit(this.value);
    }
  }

  protected handleClickEvent() {
    if (this.stateService.value) {
      return;
    }

    this.stateService.value = true;
    this.change.emit(this.value);
    this.onChange(this.value);
    this.onTouch();
  }

  protected handleTypeChange(type?: SimpleChange) {
    if (!type) {
      return;
    }

    if (type.isFirstChange() || type.currentValue !== type.previousValue) {
      this.view = this.viewService.createRadioPortal(this.injector, type.currentValue);
    }
  }

}
