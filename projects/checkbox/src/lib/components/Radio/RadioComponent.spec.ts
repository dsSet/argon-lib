import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { CheckboxStateService } from '../../services/CheckboxStateService';
import { CheckboxStateServiceMock } from '../../../test/CheckboxStateServiceMock';
import { CheckboxViewService } from '../../services/CheckboxViewService';
import { CheckboxViewServiceMock } from '../../../test/CheckboxViewServiceMock';
import { PortalModule } from '@angular/cdk/portal';
import { KeyboardService } from '@argon/keyboard';
import { KeyboardServiceMock } from '../../../../../../test/KeyboardServiceMock';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { MockRadioViewComponent } from '../../../test/MockViewComponent';
import createSpy = jasmine.createSpy;
import { RadioComponent } from './RadioComponent';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { BooleanControlRegistryService } from '../../services/BooleanControlRegistryService';

describe('RadioComponent', () => {

  let component: RadioComponent;
  let fixture: ComponentFixture<RadioComponent>;
  let stateService: CheckboxStateServiceMock;
  let viewService: CheckboxViewServiceMock;

  beforeEach(waitForAsync(() => {
    stateService = new CheckboxStateServiceMock();
    return TestBed.configureTestingModule({
        imports: [
          PortalModule
        ],
        declarations: [
          RadioComponent,
          MockRadioViewComponent
        ],
        providers: [
          { provide: CheckboxViewService, useClass: CheckboxViewServiceMock },
          { provide: KeyboardService, useClass: KeyboardServiceMock },
          BooleanControlRegistryService
        ],
        schemas: [ NO_ERRORS_SCHEMA ]
      })
        .overrideModule(BrowserDynamicTestingModule, {
          set: {
            entryComponents: [MockRadioViewComponent]
          }
        })
        .overrideComponent(RadioComponent, {
          set: {
            providers: [
              { provide: CheckboxStateService, useValue: stateService }
            ]
          }
        })
        .compileComponents();
    }

  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioComponent);
    component = fixture.componentInstance;
    viewService = TestBed.inject(CheckboxViewService) as any;
    fixture.detectChanges();
  });

  it('className should toggle .ar-radio class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-radio');
  });


  it('writeValue should toggle state if prev state is false and next value is equal component.value', () => {
    component.value = 'VALUE1';
    const nextValue = 'VALUE1';
    stateService.value = false;
    spyOn(component.change, 'emit');
    component.writeValue(nextValue);
    expect(stateService.value).toEqual(true);
    expect(component.change.emit).toHaveBeenCalledWith(component.value);
  });

  it('writeValue should toggle state if prev state is true and next value is not equal component.value', () => {
    component.value = 'VALUE1';
    const nextValue = 'VALUE2';
    stateService.value = true;
    spyOn(component.change, 'emit');
    component.writeValue(nextValue);
    expect(stateService.value).toEqual(false);
    expect(component.change.emit).toHaveBeenCalledWith(component.value);
  });

  it('writeValue should not toggle state if prev state is true and next value is equal component.value', () => {
    component.value = 'VALUE1';
    const nextValue = 'VALUE1';
    stateService.value = true;

    component.writeValue(nextValue);
    expect(stateService.value).toEqual(true);
  });

  it('writeValue should not toggle state if prev state is false and next value is not equal component.value', () => {
    component.value = 'VALUE1';
    const nextValue = 'VALUE2';
    stateService.value = false;

    component.writeValue(nextValue);
    expect(stateService.value).toEqual(false);
  });

  it('should handle click actions', () => {
    stateService.value = false;
    stateService.disabled = false;
    component.value = 'Fake Value';
    const onChange = createSpy('onChange');
    const onTouch = createSpy('onTouch');
    spyOn(component.change, 'emit');

    component.registerOnChange(onChange);
    component.registerOnTouched(onTouch);

    component.handleClick();
    expect(stateService.value).toEqual(true);
    expect(onChange).toHaveBeenCalledWith(component.value);
    expect(onTouch).toHaveBeenCalled();
    expect(component.change.emit).toHaveBeenCalledWith(component.value);
  });

  it('should not handle click actions if current state is true', () => {
    fixture.detectChanges();
    stateService.value = true;
    stateService.disabled = false;
    component.value = 'Fake Value';
    const onChange = createSpy('onChange');
    const onTouch = createSpy('onTouch');
    spyOn(component.change, 'emit');

    component.registerOnChange(onChange);
    component.registerOnTouched(onTouch);

    component.handleClick();
    expect(onChange).not.toHaveBeenCalled();
    expect(onTouch).not.toHaveBeenCalled();
    expect(component.change.emit).not.toHaveBeenCalled();
  });

  it('should handle type changes', () => {
    const type = new SimpleChange(null, 'next-type', false);
    viewService.createRadioPortal.calls.reset();
    component.ngOnChanges({ type });
    expect(viewService.createRadioPortal).toHaveBeenCalledWith(component.injector, type.currentValue);
    expect(component.view).toEqual(viewService.radioPortal);
  });

  it('should handle type first change', () => {
    const type = new SimpleChange('next-type', 'next-type', true);
    viewService.createRadioPortal.calls.reset();
    component.ngOnChanges({ type });
    expect(viewService.createRadioPortal).toHaveBeenCalledWith(component.injector, type.currentValue);
    expect(component.view).toEqual(viewService.radioPortal);
  });

  it('should not call services without type changes', () => {
    viewService.createRadioPortal.calls.reset();
    const type = new SimpleChange('next-type', 'next-type', false);
    component.ngOnChanges({ type });
    expect(viewService.createRadioPortal).not.toHaveBeenCalled();
  });

  it('should not call services without type', () => {
    viewService.createRadioPortal.calls.reset();
    component.ngOnChanges({ });
    expect(viewService.createRadioPortal).not.toHaveBeenCalled();
  });

  it('should handle value changes', () => {
    const value = new SimpleChange('value 1', 'value 2', false);
    component.ngOnChanges({ value });
    expect(stateService.data).toEqual(value.currentValue);
  });

});
