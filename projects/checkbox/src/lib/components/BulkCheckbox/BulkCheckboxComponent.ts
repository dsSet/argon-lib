import size from 'lodash/size';
import isArray from 'lodash/isArray';
import {
  ChangeDetectionStrategy,
  Component, EventEmitter,
  forwardRef,
  HostBinding,
  Injector,
  Input,
  OnChanges, OnDestroy,
  OnInit, Output,
  SimpleChange, SimpleChanges
} from '@angular/core';
import { CheckboxStateService } from '../../services/CheckboxStateService';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CheckboxViewService } from '../../services/CheckboxViewService';
import { Comparators } from '@argon/tools';
import { ValueControlShape } from '../../models/ValueControlShape';
import { BooleanControlShape } from '../../models/BooleanControlShape';

@Component({
  selector: 'ar-bulk-checkbox',
  template: `
    <ng-content select="[before]"></ng-content>
    <ng-template [cdkPortalOutlet]="view"></ng-template>
    <ng-content></ng-content>
  `,
  providers: [
    CheckboxStateService,
    { provide: BooleanControlShape, useExisting: forwardRef(() => BulkCheckboxComponent) },
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => BulkCheckboxComponent), multi: true }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    role: 'checkbox',
    '[class.disabled]': 'disabled',
    '[attr.disabled]': 'attrDisabled',
    '[attr.tabindex]': 'attrTabIndex'
  }
})
export class BulkCheckboxComponent extends ValueControlShape implements OnInit, OnChanges, OnDestroy {

  @HostBinding('class.ar-checkbox') className;

  @Input() value: Array<any>;

  @Input() compareFn = Comparators.deepEqualComparator;

  @Input() disabled: boolean;

  @Output() click = new EventEmitter<void>();

  public controlValue: Array<any>;

  constructor(
    protected stateService: CheckboxStateService,
    protected viewService: CheckboxViewService,
    injector: Injector
  ) {
    super(stateService, viewService, injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  writeValue(obj: any): void {
    this.controlValue = obj || [];
    const nextValue = this.resolveControlState(obj);
    super.writeValue(nextValue);
  }

  protected handleClickEvent() {
    this.stateService.toggle();
    this.controlValue = this.stateService.value ? this.value : [];
    this.onChange(this.controlValue);
    this.onTouch();
  }

  protected handleTypeChange(type?: SimpleChange) {
    if (!type) {
      return;
    }

    if (type.isFirstChange() || type.currentValue !== type.previousValue) {
      this.view = this.viewService.createCheckboxPortal(this.injector, type.currentValue);
      this.stateService.hasIndeterminateState = this.viewService.hasIndeterminateState(type.currentValue);
    }
  }

  protected handleValueChange(value?: SimpleChange) {
    super.handleValueChange(value);
    this.writeValue(this.controlValue);
  }

  private resolveControlState(value: Array<any> | any = []): boolean | null {
    const safeValue = isArray(value) ? value : [value];
    const diff = this.diff(this.value || [], safeValue);
    if (size(diff) === 0) {
      return true;
    }

    if (size(this.value) === size(diff)) {
      return false;
    }

    return null;
  }

}
