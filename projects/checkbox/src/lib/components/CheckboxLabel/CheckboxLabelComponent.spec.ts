import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CheckboxLabelComponent } from './CheckboxLabelComponent';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import createSpyObj = jasmine.createSpyObj;

describe('CheckboxLabelComponent', () => {
  let component: CheckboxLabelComponent;
  let fixture: ComponentFixture<CheckboxLabelComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        CheckboxLabelComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxLabelComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-checkbox-label class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-checkbox-label');
  });

  it('resize should toggle .ar-checkbox-label--resize class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'resize', 'ar-checkbox-label--resize');
  });

  it('handleClick should call for.handleClick action', () => {
    const shape = createSpyObj(['handleClick']);
    const innerShape = createSpyObj(['handleClick']);
    component.for = shape;
    component.innerShape = innerShape;
    component.handleClick();
    expect(shape.handleClick).toHaveBeenCalled();
    expect(innerShape.handleClick).not.toHaveBeenCalled();
  });

  it('handleClick should call inner shape click', () => {
    const innerShape = createSpyObj(['handleClick']);
    component.innerShape = innerShape;
    component.handleClick();
    expect(innerShape.handleClick).toHaveBeenCalled();
  });

  it('disabled should return false if has no inner and outer shapes', () => {
    component.innerShape = null;
    component.outerShape = null;
    expect(component.disabled).toEqual(false);
  });

  it('disabled should be received from outerShape', () => {
    component.innerShape = null;
    component.outerShape = { disabled: true } as any;
    expect(component.disabled).toEqual(true);
  });

  it('disabled should be received from innerShape', () => {
    component.innerShape = { disabled: true } as any;
    component.outerShape = null;
    expect(component.disabled).toEqual(true);
  });

  it('outerShape should be prioritized', () => {
    component.innerShape = { disabled: true } as any;
    component.outerShape = { disabled: false } as any;
    expect(component.disabled).toEqual(false);
  });
});
