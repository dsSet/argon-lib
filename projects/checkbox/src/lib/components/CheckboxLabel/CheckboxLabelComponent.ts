import { ChangeDetectionStrategy, Component, ContentChild, HostBinding, HostListener, Input, Optional } from '@angular/core';
import { BooleanControlShape } from '../../models/BooleanControlShape';

@Component({
  selector: 'ar-checkbox-label',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxLabelComponent {

  @HostBinding('class.ar-checkbox-label') className = true;

  @HostBinding('class.ar-checkbox-label--resize') @Input() resize: boolean;

  @HostBinding('class.ar-checkbox-label--disabled')
  get disabled(): boolean {
    if (this.outerShape) {
      return this.outerShape.disabled;
    }

    if (this.innerShape) {
      return this.innerShape.disabled;
    }

    return false;
  }

  @Input() for: BooleanControlShape;

  @ContentChild(BooleanControlShape, { read: BooleanControlShape }) innerShape: BooleanControlShape;

  constructor(
    @Optional() public outerShape: BooleanControlShape
  ) { }

  @HostListener('click')
  public handleClick() {
    if (this.for) {
      this.for.handleClick();
    } else if (this.innerShape) {
      this.innerShape.handleClick();
    }
  }

}
