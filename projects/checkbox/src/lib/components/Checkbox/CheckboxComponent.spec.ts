import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { CheckboxComponent } from './CheckboxComponent';
import { CheckboxStateService } from '../../services/CheckboxStateService';
import { CheckboxStateServiceMock } from '../../../test/CheckboxStateServiceMock';
import { CheckboxViewService } from '../../services/CheckboxViewService';
import { CheckboxViewServiceMock } from '../../../test/CheckboxViewServiceMock';
import { PortalModule } from '@angular/cdk/portal';
import { KeyboardService } from '@argon/keyboard';
import { KeyboardServiceMock } from '../../../../../../test/KeyboardServiceMock';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { MockCheckboxViewComponent } from '../../../test/MockViewComponent';
import createSpy = jasmine.createSpy;
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';

describe('CheckboxComponent', () => {

  let component: CheckboxComponent;
  let fixture: ComponentFixture<CheckboxComponent>;
  let stateService: CheckboxStateServiceMock;
  let viewService: CheckboxViewServiceMock;

  beforeEach(waitForAsync(() => {
    stateService = new CheckboxStateServiceMock();
    return TestBed.configureTestingModule({
        imports: [
          PortalModule
        ],
        declarations: [
          CheckboxComponent,
          MockCheckboxViewComponent
        ],
        providers: [
          { provide: CheckboxViewService, useClass: CheckboxViewServiceMock },
          { provide: KeyboardService, useClass: KeyboardServiceMock }
        ],
        schemas: [ NO_ERRORS_SCHEMA ]
      })
        .overrideModule(BrowserDynamicTestingModule, {
          set: {
            entryComponents: [MockCheckboxViewComponent]
          }
        })
        .overrideComponent(CheckboxComponent, {
          set: {
            providers: [
              { provide: CheckboxStateService, useValue: stateService }
            ]
          }
        })
        .compileComponents();
    }

  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxComponent);
    component = fixture.componentInstance;
    viewService = TestBed.inject(CheckboxViewService) as any;
  });

  it('should handle click actions', () => {
    const onChange = createSpy('onChange');
    const onTouch = createSpy('onTouch');
    component.registerOnChange(onChange);
    component.registerOnTouched(onTouch);
    stateService.disabled = false;
    stateService.value = true;

    component.handleClick();
    expect(stateService.toggle).toHaveBeenCalled();
    expect(onChange).toHaveBeenCalledWith(stateService.value);
    expect(onTouch).toHaveBeenCalled();
  });

  it('should handle type changes', () => {
    const type = new SimpleChange(null, 'next-type', false);
    stateService.hasIndeterminateState = true;
    viewService.checkboxIndeterminate = false;

    component.ngOnChanges({ type });
    expect(viewService.createCheckboxPortal).toHaveBeenCalledWith(component.injector, type.currentValue);
    expect(viewService.hasIndeterminateState).toHaveBeenCalledWith(type.currentValue);
    expect(component.view).toEqual(viewService.checkboxPortal);
    expect(stateService.hasIndeterminateState).toEqual(viewService.checkboxIndeterminate);
  });

  it('should handle type first change', () => {
    const type = new SimpleChange('next-type', 'next-type', true);
    stateService.hasIndeterminateState = true;
    viewService.checkboxIndeterminate = false;

    component.ngOnChanges({ type });
    expect(viewService.createCheckboxPortal).toHaveBeenCalledWith(component.injector, type.currentValue);
    expect(viewService.hasIndeterminateState).toHaveBeenCalledWith(type.currentValue);
    expect(component.view).toEqual(viewService.checkboxPortal);
    expect(stateService.hasIndeterminateState).toEqual(viewService.checkboxIndeterminate);
  });

  it('should not call services without type changes', () => {
    const type = new SimpleChange('next-type', 'next-type', false);
    component.ngOnChanges({ type });
    expect(viewService.createCheckboxPortal).not.toHaveBeenCalled();
    expect(viewService.hasIndeterminateState).not.toHaveBeenCalled();
  });

  it('should not call services without type', () => {
    component.ngOnChanges({ });
    expect(viewService.createCheckboxPortal).not.toHaveBeenCalled();
    expect(viewService.hasIndeterminateState).not.toHaveBeenCalled();
  });

  it('className should toggle .ar-checkbox class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-checkbox');
  });

  it('write value should call change.emit', () => {
    spyOn(component.change, 'emit');
    component.writeValue(true);
    expect(component.change.emit).toHaveBeenCalledWith(true);
  });

  it('handleClickEvent should emit change', () => {
    spyOn(component.change, 'emit');
    stateService.disabled = false;
    stateService.value = true;

    component.handleClick();
    expect(component.change.emit).toHaveBeenCalledWith(true);
    stateService.value = false;
    component.handleClick();
    expect(component.change.emit).toHaveBeenCalledWith(false);
  });
});
