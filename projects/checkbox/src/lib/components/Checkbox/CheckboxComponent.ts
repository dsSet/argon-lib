import {
  ChangeDetectionStrategy,
  Component, EventEmitter,
  forwardRef, HostBinding,
  Injector, Input, OnChanges, OnDestroy, OnInit, Output,
  SimpleChange, SimpleChanges
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CheckboxViewService } from '../../services/CheckboxViewService';
import { CheckboxStateService } from '../../services/CheckboxStateService';
import { BooleanControlShape } from '../../models/BooleanControlShape';
import { Comparators } from '@argon/tools';


@Component({
  selector: 'ar-checkbox',
  template: `
    <ng-content select="[before]"></ng-content>
    <ng-template [cdkPortalOutlet]="view"></ng-template>
    <ng-content></ng-content>
  `,
  providers: [
    CheckboxStateService,
    { provide: BooleanControlShape, useExisting: forwardRef(() => CheckboxComponent) },
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => CheckboxComponent), multi: true }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    role: 'checkbox',
    '[class.disabled]': 'disabled',
    '[attr.disabled]': 'attrDisabled',
    '[attr.tabindex]': 'attrTabIndex'
  }
})
export class CheckboxComponent extends BooleanControlShape implements OnInit, OnChanges, OnDestroy {

  @HostBinding('class.ar-checkbox') className;

  @Output() change = new EventEmitter<boolean>();

  @Input() compareFn = Comparators.deepEqualComparator;

  @Input() disabled: boolean;

  @Output() click = new EventEmitter<void>();

  constructor(
    protected stateService: CheckboxStateService,
    protected viewService: CheckboxViewService,
    injector: Injector
  ) {
    super(stateService, viewService, injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

  writeValue(obj: any): void {
    super.writeValue(obj);
    this.change.emit(obj);
  }

  protected handleClickEvent() {
    this.stateService.toggle();
    this.onChange(this.stateService.value);
    this.change.emit(this.stateService.value);
    this.onTouch();
  }

  protected handleTypeChange(type?: SimpleChange) {
    if (!type) {
      return;
    }

    if (type.isFirstChange() || type.currentValue !== type.previousValue) {
      this.view = this.viewService.createCheckboxPortal(this.injector, type.currentValue);
      this.stateService.hasIndeterminateState = this.viewService.hasIndeterminateState(type.currentValue);
    }
  }
}
