import { RadioViewDirective } from '../../dircetives/RadioViewDirective';
import { Component, ElementRef, HostBinding, Injector } from '@angular/core';

@Component({
  selector: 'ar-radio-view',
  templateUrl: './RadioViewComponent.html'
})
export class RadioViewComponent extends RadioViewDirective {

  @HostBinding('class.ar-default-radio') className = true;

  constructor(element: ElementRef<any>, injector: Injector) {
    super(element, injector);
  }
}
