import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CheckboxStateService } from '../../services/CheckboxStateService';
import { CheckboxStateServiceMock } from '../../../test/CheckboxStateServiceMock';
import { RadioViewDirective } from '../../dircetives/RadioViewDirective';
import { RadioViewComponent } from './RadioViewComponent';

describe('RadioViewComponent', () => {
  let component: RadioViewComponent;
  let fixture: ComponentFixture<RadioViewComponent>;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      declarations: [
        RadioViewDirective,
        RadioViewComponent,
      ],
      providers: [
        { provide: CheckboxStateService, useClass: CheckboxStateServiceMock }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioViewComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-default-radio class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-default-radio');
  });
});
