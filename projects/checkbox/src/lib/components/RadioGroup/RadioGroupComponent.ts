import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  forwardRef, HostBinding, Input,
  QueryList
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { RadioComponent } from '../Radio/RadioComponent';

@Component({
  selector: 'ar-radio-group',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => RadioGroupComponent), multi: true }
  ],
  host: {
    role: 'radiogroup',
    '[class.disabled]': 'isDisabled'
  }
})
export class RadioGroupComponent implements ControlValueAccessor, AfterContentInit {

  @ContentChildren(RadioComponent) radio: QueryList<RadioGroupComponent>;

  @HostBinding('class.ar-radio-group') className = true;

  @HostBinding('class.ar-radio-group--inline') @Input() inline: boolean;

  public isDisabled: boolean;

  public value: any;

  public initialized: boolean;

  ngAfterContentInit(): void {
    this.initialized = true;
    this.cloneState(this.radio);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
    if (this.initialized) {
      this.radio.forEach((radio: RadioGroupComponent) => { radio.registerOnChange(fn); });
    }
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
    if (this.initialized) {
      this.radio.forEach((radio: RadioGroupComponent) => { radio.registerOnTouched(fn); });
    }
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
    if (this.initialized) {
      this.radio.forEach((radio: RadioGroupComponent) => { radio.setDisabledState(isDisabled); });
    }
  }

  writeValue(obj: any): void {
    this.value = obj;
    if (this.initialized) {
      this.radio.forEach((radio: RadioGroupComponent) => { radio.writeValue(obj); });
    }
  }

  protected cloneState = (list: any) => {
    this.registerOnChange(this.onChange);
    this.registerOnTouched(this.onTouch);
    this.setDisabledState(this.isDisabled);
    this.writeValue(this.value);
  }

  protected onChange = (value: boolean) => { };

  protected onTouch = () => { };

}
