import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import createSpyObj = jasmine.createSpyObj;
import { RadioGroupComponent } from './RadioGroupComponent';
import createSpy = jasmine.createSpy;
import { QueryList } from '@angular/core';

describe('RadioGroupComponent', () => {
  let component: RadioGroupComponent;
  let fixture: ComponentFixture<RadioGroupComponent>;

  const createRadioSpy = () => createSpyObj(['registerOnChange', 'registerOnTouched', 'setDisabledState', 'writeValue']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        RadioGroupComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioGroupComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-radio-group class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-radio-group');
  });

  it('inline should toggle .ar-radio-group--inline class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'inline', 'ar-radio-group--inline');
  });

  it('should have role attribute', () => {
    HostBindingTestHelper.testRoleAttribute(component, fixture, 'radiogroup');
  });

  it('ngAfterContentInit should toggle initialized flag', () => {
    expect(component.initialized).toBeFalsy();
    fixture.detectChanges();
    expect(component.initialized).toBeTruthy();
  });

  it('ngAfterContentInit should clone state', () => {
    spyOn(component, 'registerOnChange');
    spyOn(component, 'registerOnTouched');
    spyOn(component, 'setDisabledState');
    spyOn(component, 'writeValue');
    component.value = 'Fake Value';
    component.isDisabled = true;

    fixture.detectChanges();
    expect(component.registerOnChange).toHaveBeenCalled();
    expect(component.registerOnTouched).toHaveBeenCalled();
    expect(component.setDisabledState).toHaveBeenCalledWith(component.isDisabled);
    expect(component.writeValue).toHaveBeenCalledWith(component.value);
  });

  describe('collection updates', () => {
    let radio: any;
    beforeEach(() => {
      radio = createRadioSpy();
      const queryList = new QueryList<any>();
      queryList.reset([radio]);
      component.radio = queryList;
    });

    it('registerOnChange should update radio collection', () => {
      const onChange = { data: 'Mock FN' };
      component.initialized = true;
      component.registerOnChange(onChange);
      expect(radio.registerOnChange).toHaveBeenCalledWith(onChange);
    });

    it('registerOnChange should not update radio collection if not initialized', () => {
      const onChange = { data: 'Mock FN' };
      component.initialized = false;
      component.registerOnChange(onChange);
      expect(radio.registerOnChange).not.toHaveBeenCalled();
    });

    it('registerOnTouched should update radio collection', () => {
      const onTouch = { data: 'Mock FN' };
      component.initialized = true;
      component.registerOnTouched(onTouch);
      expect(radio.registerOnTouched).toHaveBeenCalledWith(onTouch);
    });

    it('registerOnTouched should not update radio collection if not initialized', () => {
      const onTouch = { data: 'Mock FN' };
      component.initialized = false;
      component.registerOnTouched(onTouch);
      expect(radio.registerOnTouched).not.toHaveBeenCalled();
    });

    it('setDisabledState should update radio collection', () => {
      const onTouch = { data: 'Mock FN' };
      component.initialized = true;
      component.setDisabledState(true);
      expect(radio.setDisabledState).toHaveBeenCalledWith(true);
    });

    it('setDisabledState should not update radio collection if not initialized', () => {
      component.initialized = false;
      component.setDisabledState(true);
      expect(radio.setDisabledState).not.toHaveBeenCalled();
    });

    it('writeValue should update radio collection', () => {
      const value = 312;
      component.initialized = true;
      component.writeValue(value);
      expect(radio.writeValue).toHaveBeenCalledWith(value);
    });

    it('writeValue should not update radio collection if not initialized', () => {
      component.initialized = false;
      component.writeValue(true);
      expect(radio.writeValue).not.toHaveBeenCalled();
    });
  });


});
