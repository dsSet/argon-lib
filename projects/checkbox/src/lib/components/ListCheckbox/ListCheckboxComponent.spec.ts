import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { CheckboxStateServiceMock } from '../../../test/CheckboxStateServiceMock';
import { CheckboxViewServiceMock } from '../../../test/CheckboxViewServiceMock';
import { PortalModule } from '@angular/cdk/portal';
import { MockCheckboxViewComponent } from '../../../test/MockViewComponent';
import { CheckboxViewService } from '../../services/CheckboxViewService';
import { KeyboardServiceMock } from '../../../../../../test/KeyboardServiceMock';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { CheckboxStateService } from '../../services/CheckboxStateService';
import { ListCheckboxComponent } from './ListCheckboxComponent';
import { KeyboardService } from '@argon/keyboard';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { BooleanControlRegistryService } from '../../services/BooleanControlRegistryService';
import { BooleanControlRegistryServiceMock } from '../../../test/BooleanControlRegistryServiceMock';
import createSpy = jasmine.createSpy;


describe('ListCheckboxComponent', () => {

  let component: ListCheckboxComponent;
  let fixture: ComponentFixture<ListCheckboxComponent>;
  let stateService: CheckboxStateServiceMock;
  let viewService: CheckboxViewServiceMock;

  beforeEach(waitForAsync(() => {
      stateService = new CheckboxStateServiceMock();
      return TestBed.configureTestingModule({
        imports: [
          PortalModule
        ],
        declarations: [
          ListCheckboxComponent,
          MockCheckboxViewComponent
        ],
        providers: [
          { provide: CheckboxViewService, useClass: CheckboxViewServiceMock },
          { provide: KeyboardService, useClass: KeyboardServiceMock },
          { provide: BooleanControlRegistryService, useClass: BooleanControlRegistryServiceMock }
        ],
        schemas: [ NO_ERRORS_SCHEMA ]
      })
        .overrideModule(BrowserDynamicTestingModule, {
          set: {
            entryComponents: [MockCheckboxViewComponent]
          }
        })
        .overrideComponent(ListCheckboxComponent, {
          set: {
            providers: [
              { provide: CheckboxStateService, useValue: stateService }
            ]
          }
        })
        .compileComponents();
    }

  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCheckboxComponent);
    component = fixture.componentInstance;
    viewService = TestBed.inject(CheckboxViewService) as any;
  });

  it('className should toggle .ar-checkbox class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-checkbox');
  });

  it('should add value to control on click if state service has value true', () => {
    component.value = 8;
    component.controlValue = [1, 2];
    stateService.value = true;
    fixture.detectChanges();
    const onChange = createSpy('onChange');
    component.registerOnChange(onChange);
    fixture.elementRef.nativeElement.click();
    expect(onChange).toHaveBeenCalledWith([1, 2, 8]);
    expect(stateService.toggle).toHaveBeenCalled();
  });

  it('should remove value from control on click if state service has value false', () => {
    component.value = 8;
    component.controlValue = [1, 8, 2, 3];
    stateService.value = false;
    fixture.detectChanges();
    const onChange = createSpy('onChange');
    component.registerOnChange(onChange);
    fixture.elementRef.nativeElement.click();
    expect(onChange).toHaveBeenCalledWith([1, 2, 3]);
    expect(stateService.toggle).toHaveBeenCalled();
  });

  it('writeValue should store control value internally', () => {
    fixture.detectChanges();
    const nextValue = [2];
    component.writeValue(nextValue);
    expect(component.controlValue).toEqual(nextValue);
    component.writeValue(null);
    expect(component.controlValue).toEqual([]);
  });

  it('should call onTouch fn on click', () => {
    fixture.detectChanges();
    const onTouch = createSpy('onTouch');
    component.registerOnTouched(onTouch);
    fixture.elementRef.nativeElement.click();
    expect(onTouch).toHaveBeenCalled();
  });

  it('writeValue should set true value if next control value contains control value', () => {
    fixture.detectChanges();
    stateService.value = null;
    const nextValue = [1, 2, 4];
    component.value = 2;
    component.writeValue(nextValue);
    expect(stateService.value).toEqual(true);
  });

  it('writeValue should set false value if next value collection does not contains component value', () => {
    fixture.detectChanges();
    stateService.value = null;
    const nextValue = [1, 2, 4];
    component.value = 81;
    component.writeValue(nextValue);
    expect(stateService.value).toEqual(false);
  });

  it('should receive view from viewService', () => {
    stateService.hasIndeterminateState = !viewService.checkboxIndeterminate;
    fixture.detectChanges();
    expect(viewService.createCheckboxPortal).toHaveBeenCalled();
    expect(viewService.hasIndeterminateState).toHaveBeenCalled();
    expect(stateService.hasIndeterminateState).toEqual(viewService.checkboxIndeterminate);
  });

  it('should handle value changes', () => {
    fixture.detectChanges();
    const value = new SimpleChange(3, 22, false);
    component.controlValue = [8, 3, 4];
    spyOn(component, 'writeValue');
    component.ngOnChanges({ value });
    expect(component.writeValue).toHaveBeenCalledWith(component.controlValue);
  });

});
