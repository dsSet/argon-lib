import isArray from 'lodash/isArray';
import {
  ChangeDetectionStrategy,
  Component, EventEmitter,
  forwardRef,
  HostBinding,
  Injector,
  Input, OnChanges, OnDestroy, OnInit, Output,
  SimpleChange, SimpleChanges
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CheckboxStateService } from '../../services/CheckboxStateService';
import { CheckboxViewService } from '../../services/CheckboxViewService';
import { ValueControlShape } from '../../models/ValueControlShape';
import { CompareFn } from '@argon/tools';
import { BooleanControlShape } from '../../models/BooleanControlShape';

@Component({
  selector: 'ar-list-checkbox',
  template: `
    <ng-content select="[before]"></ng-content>
    <ng-template [cdkPortalOutlet]="view"></ng-template>
    <ng-content></ng-content>
  `,
  providers: [
    CheckboxStateService,
    { provide: BooleanControlShape, useExisting: forwardRef(() => ListCheckboxComponent) },
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ListCheckboxComponent), multi: true }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    role: 'checkbox',
    '[class.disabled]': 'disabled',
    '[attr.disabled]': 'attrDisabled',
    '[attr.tabindex]': 'attrTabIndex'
  }
})
export class ListCheckboxComponent extends ValueControlShape implements OnInit, OnChanges, OnDestroy {

  @HostBinding('class.ar-checkbox') className;

  @Input() compareFn: CompareFn;

  @Input() disabled: boolean;

  @Output() click = new EventEmitter<void>();

  public controlValue: any;

  constructor(
    protected stateService: CheckboxStateService,
    protected viewService: CheckboxViewService,
    injector: Injector
  ) {
    super(stateService, viewService, injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  writeValue(obj: any): void {
    this.controlValue = obj || [];
    super.writeValue(this.resolveValue(obj));
  }

  protected handleClickEvent() {
    const controlValue = this.controlValue || [];
    this.stateService.toggle();
    this.controlValue = this.stateService.value ? this.push(controlValue, this.value) : this.remove(controlValue, this.value);
    this.onChange(this.controlValue);
    this.onTouch();
  }

  protected handleTypeChange(type?: SimpleChange) {
    if (!type) {
      return;
    }

    if (type.isFirstChange() || type.currentValue !== type.previousValue) {
      this.view = this.viewService.createCheckboxPortal(this.injector, type.currentValue);
      this.stateService.hasIndeterminateState = this.viewService.hasIndeterminateState(type.currentValue);
    }
  }

  protected handleValueChange(value?: SimpleChange) {
    super.handleValueChange(value);
    this.writeValue(this.controlValue);
  }

  private resolveValue(value: any): boolean {
    const safeValue = isArray(value) ? value : [value];
    return this.findIndex(safeValue, this.value) >= 0;
  }
}
