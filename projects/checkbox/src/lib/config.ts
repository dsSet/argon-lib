import { Provider } from '@angular/core';
import {
  ARGON_CHECKBOX_DEFAULT_VIEW,
  ARGON_CHECKBOX_VIEW,
  ARGON_RADIO_DEFAULT_VIEW,
  ARGON_RADIO_VIEW
} from './providers/CheckboxViewProvider';
import { CheckboxViewComponent } from './components/CheckboxView/CheckboxViewComponent';
import { CheckboxTypesEnum } from './types/CheckboxTypesEnum';
import { RadioConfigModel } from './models/RadioConfigModel';
import { RadioTypesEnum } from './types/RadioTypesEnum';
import { RadioViewComponent } from './components/RadioView/RadioViewComponent';

export const checkboxConfig: Array<Provider> = [
  {
    provide: ARGON_CHECKBOX_VIEW,
    useValue: {
      hasIndeterminateState: false,
      type: CheckboxTypesEnum.checkbox,
      component: CheckboxViewComponent
    },
    multi: true
  },
  {
    provide: ARGON_CHECKBOX_VIEW,
    useValue: {
      hasIndeterminateState: true,
      type: CheckboxTypesEnum.checkboxIndeterminate,
      component: CheckboxViewComponent
    },
    multi: true
  }
];


export const defaultCheckboxViewConfig: Provider = {
  provide: ARGON_CHECKBOX_DEFAULT_VIEW,
  useValue: CheckboxTypesEnum.checkbox
};

export const radioConfig: Array<Provider> = [
  {
    provide: ARGON_RADIO_VIEW,
    useValue: {
      type: RadioTypesEnum.radio,
      component: RadioViewComponent
    } as RadioConfigModel,
    multi: true
  }
];

export const defaultRadioViewConfig: Provider = {
  provide: ARGON_RADIO_DEFAULT_VIEW,
  useValue: RadioTypesEnum.radio
};

