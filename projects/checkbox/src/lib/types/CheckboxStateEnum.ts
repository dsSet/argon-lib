

export enum CheckboxStateEnum {

  on = 'on',

  off = 'off',

  indeterminate = 'indeterminate'

}
