
export enum CheckboxTypesEnum {

  checkbox = 'checkbox',

  checkboxIndeterminate = 'checkbox-indeterminate',

}
