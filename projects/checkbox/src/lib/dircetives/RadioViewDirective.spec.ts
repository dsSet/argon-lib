import { Component, ElementRef, Injector } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../test/helpers/HostBindingTestHelper';
import { CheckboxStateService } from '../services/CheckboxStateService';
import { CheckboxStateServiceMock } from '../../test/CheckboxStateServiceMock';
import { CheckboxStateEnum } from '../types/CheckboxStateEnum';
import { RadioViewDirective } from './RadioViewDirective';

describe('RadioViewDirective', () => {

  @Component({ selector: 'spec-component', template: '' })
  class SpecComponent extends RadioViewDirective {
    constructor(element: ElementRef<any>, injector: Injector) {
      super(element, injector);
    }
  }

  let component: SpecComponent;
  let fixture: ComponentFixture<SpecComponent>;
  let stateService: CheckboxStateServiceMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SpecComponent
      ],
      providers: [
        { provide: CheckboxStateService, useClass: CheckboxStateServiceMock }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecComponent);
    component = fixture.componentInstance;
    stateService = TestBed.inject(CheckboxStateService) as any;
  });

  it('className should toggle .ar-radio-view class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-radio-view');
  });

  it('disabled should toggle .ar-radio-view--disabled class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'disabled', 'ar-radio-view--disabled');
  });

  it('should have role attribute', () => {
    HostBindingTestHelper.testRoleAttribute(component, fixture, 'radio');
  });

  it('should have classes for each state', () => {
    fixture.detectChanges();
    component.currentState = null;
    component.state = CheckboxStateEnum.on;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-radio--on').toBeTruthy();

    component.state = CheckboxStateEnum.off;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-radio--on').toBeFalsy();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-radio--off').toBeTruthy();

    component.state = null;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-radio--off').toBeFalsy();
  });

  it('should provide data from state service', () => {
    stateService.data = 'Expected Result';
    expect(component.value).toEqual(stateService.data);
  });
});
