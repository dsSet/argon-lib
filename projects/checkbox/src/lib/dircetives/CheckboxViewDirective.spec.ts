import { CheckboxViewDirective } from './CheckboxViewDirective';
import { Component, ElementRef, Injector } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../test/helpers/HostBindingTestHelper';
import { CheckboxStateService } from '../services/CheckboxStateService';
import { CheckboxStateServiceMock } from '../../test/CheckboxStateServiceMock';
import { CheckboxStateEnum } from '../types/CheckboxStateEnum';

describe('CheckboxViewDirective', () => {

  @Component({ selector: 'spec-component', template: '' })
  class SpecComponent extends CheckboxViewDirective {
    constructor(element: ElementRef<any>, injector: Injector) {
      super(element, injector);
    }
  }

  let component: SpecComponent;
  let fixture: ComponentFixture<SpecComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SpecComponent
      ],
      providers: [
        { provide: CheckboxStateService, useClass: CheckboxStateServiceMock }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-checkbox-view class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-checkbox-view');
  });

  it('disabled should toggle .ar-checkbox-view--disabled class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'disabled', 'ar-checkbox-view--disabled');
  });

  it('should have role attribute', () => {
    HostBindingTestHelper.testRoleAttribute(component, fixture, 'checkbox');
  });

  it('should have classes for each state', () => {
    fixture.detectChanges();
    component.currentState = null;
    component.state = CheckboxStateEnum.on;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-checkbox--on').toBeTruthy();

    component.state = CheckboxStateEnum.off;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-checkbox--on').toBeFalsy();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-checkbox--off').toBeTruthy();

    component.state = CheckboxStateEnum.indeterminate;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-checkbox--off').toBeFalsy();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-checkbox--indeterminate').toBeTruthy();

    component.state = null;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-checkbox--indeterminate').toBeFalsy();
  });
});
