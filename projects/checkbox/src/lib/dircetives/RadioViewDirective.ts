import {
  Directive,
  ElementRef,
  HostBinding, Injector
} from '@angular/core';
import { CheckboxStateEnum } from '../types/CheckboxStateEnum';
import { BooleanShape } from '../models/BooleanShape';

@Directive({
  selector: '[arRadioView]'
})
export class RadioViewDirective extends BooleanShape {

  @HostBinding('class.ar-radio-view') className;

  @HostBinding('class.ar-radio-view--disabled') disabled;

  @HostBinding('attr.role') role = 'radio';

  public get value(): any {
    return this.stateService.data;
  }

  constructor(element: ElementRef<any>, injector: Injector) {
    super(element, injector);
  }

  protected getStateClass(state: CheckboxStateEnum): string {
    switch (state) {
      case CheckboxStateEnum.on: return 'ar-radio--on';
      case CheckboxStateEnum.off: return 'ar-radio--off';
      default: return null;
    }
  }
}
