import {
  Directive,
  ElementRef,
  HostBinding, Injector,
} from '@angular/core';
import { CheckboxStateEnum } from '../types/CheckboxStateEnum';
import { BooleanShape } from '../models/BooleanShape';

@Directive({
  selector: '[arCheckboxView]'
})
export class CheckboxViewDirective extends BooleanShape {

  @HostBinding('class.ar-checkbox-view') className;

  @HostBinding('class.ar-checkbox-view--disabled') disabled;

  @HostBinding('attr.role') role = 'checkbox';

  constructor(element: ElementRef<any>, injector: Injector) {
    super(element, injector);
  }

  protected getStateClass(state: CheckboxStateEnum): string {
    switch (state) {
      case CheckboxStateEnum.on: return 'ar-checkbox--on';
      case CheckboxStateEnum.off: return 'ar-checkbox--off';
      case CheckboxStateEnum.indeterminate: return 'ar-checkbox--indeterminate';
      default: return null;
    }
  }
}
