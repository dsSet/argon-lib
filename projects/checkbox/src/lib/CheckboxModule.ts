import { NgModule } from '@angular/core';
import { CheckboxViewComponent } from './components/CheckboxView/CheckboxViewComponent';
import { CheckboxComponent } from './components/Checkbox/CheckboxComponent';
import { KeyboardModule } from '@argon/keyboard';
import { PortalModule } from '@angular/cdk/portal';
import { CheckboxViewDirective } from './dircetives/CheckboxViewDirective';
import { CommonModule } from '@angular/common';
import { CheckboxViewService } from './services/CheckboxViewService';
import { checkboxConfig, defaultCheckboxViewConfig, defaultRadioViewConfig, radioConfig } from './config';
import { CheckboxLabelComponent } from './components/CheckboxLabel/CheckboxLabelComponent';
import { RadioViewDirective } from './dircetives/RadioViewDirective';
import { RadioViewComponent } from './components/RadioView/RadioViewComponent';
import { RadioComponent } from './components/Radio/RadioComponent';
import { RadioGroupComponent } from './components/RadioGroup/RadioGroupComponent';
import { ListCheckboxComponent } from './components/ListCheckbox/ListCheckboxComponent';
import { BulkCheckboxComponent } from './components/BulkCheckbox/BulkCheckboxComponent';
import { BooleanControlRegistryService } from './services/BooleanControlRegistryService';

@NgModule({
    imports: [
        PortalModule,
        KeyboardModule,
        CommonModule,
    ],
    declarations: [
        CheckboxLabelComponent,
        CheckboxViewComponent,
        CheckboxComponent,
        CheckboxViewDirective,
        RadioViewDirective,
        RadioViewComponent,
        RadioComponent,
        RadioGroupComponent,
        ListCheckboxComponent,
        BulkCheckboxComponent,
    ],
    exports: [
        CheckboxComponent,
        RadioComponent,
        CheckboxLabelComponent,
        RadioGroupComponent,
        ListCheckboxComponent,
        BulkCheckboxComponent
    ],
    providers: [
        BooleanControlRegistryService,
        CheckboxViewService,
        checkboxConfig,
        defaultCheckboxViewConfig,
        radioConfig,
        defaultRadioViewConfig,
    ]
})
export class CheckboxModule { }
