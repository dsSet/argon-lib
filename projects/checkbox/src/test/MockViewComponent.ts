import { CheckboxViewDirective } from '../lib/dircetives/CheckboxViewDirective';
import { Component } from '@angular/core';
import { RadioViewDirective } from '../lib/dircetives/RadioViewDirective';

@Component({
  selector: 'spec-mock-checkbox-view',
  template: ''
})
export class MockCheckboxViewComponent extends CheckboxViewDirective { }


@Component({
  selector: 'spec-mock-checkbox-view',
  template: ''
})
export class MockRadioViewComponent extends RadioViewDirective { }
