import createSpy = jasmine.createSpy;


export class BooleanControlRegistryServiceMock {
  register = createSpy('register');
  remove = createSpy('remove');
  select = createSpy('select');
}
