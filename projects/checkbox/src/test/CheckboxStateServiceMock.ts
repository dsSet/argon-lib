import { BehaviorSubject } from 'rxjs';
import { CheckboxStateInterface } from '../lib/interfaces/CheckboxStateInterface';
import { CheckboxStateEnum } from '../lib/types/CheckboxStateEnum';
import createSpy = jasmine.createSpy;

export class CheckboxStateServiceMock {

  public initialState: CheckboxStateInterface = {
    state: CheckboxStateEnum.off,
    disabled: false
  };

  public state = new BehaviorSubject<CheckboxStateInterface>(this.initialState);

  public data: any;

  public value: boolean;

  public disabled: boolean;

  public viewData: any;

  public hasIndeterminateState: boolean;

  public toggle = createSpy('toggle');

  public emitNext() {
    this.state.next({ viewData: this.viewData, disabled: this.disabled, state: this.state.value.state });
  }

}
