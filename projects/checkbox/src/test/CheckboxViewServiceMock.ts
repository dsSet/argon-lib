import createSpy = jasmine.createSpy;
import { CheckboxConfigModel } from '../lib/models/CheckboxConfigModel';
import { ComponentPortal } from '@angular/cdk/portal';
import { RadioConfigModel } from '../lib/models/RadioConfigModel';


import { MockCheckboxViewComponent, MockRadioViewComponent } from './MockViewComponent';

export class CheckboxViewServiceMock {

  public checkboxConfig = new CheckboxConfigModel(MockCheckboxViewComponent, 'mock-checkbox', true);

  public checkboxPortal = new ComponentPortal(MockCheckboxViewComponent);

  public checkboxIndeterminate = true;


  public radioConfig = new RadioConfigModel(MockRadioViewComponent, 'mock-radio');

  public radioPortal = new ComponentPortal(MockRadioViewComponent);


  public getCheckboxConfig = createSpy('getCheckboxConfig').and.returnValue(this.checkboxConfig);

  public createCheckboxPortal = createSpy('createCheckboxPortal').and.returnValue(this.checkboxPortal);

  public hasIndeterminateState = createSpy('hasIndeterminateState').and.callFake(() => this.checkboxIndeterminate);


  public getRadioConfig = createSpy('getRadioConfig').and.returnValue(this.radioConfig);

  public createRadioPortal = createSpy('createRadioPortal').and.returnValue(this.radioPortal);

}
