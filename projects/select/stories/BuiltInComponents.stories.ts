import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { SelectModule, StaticSelectDataSource } from './index';
import { options } from './data';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/Select.md').default + require('../docs/index.md').default
};

storiesOf('Argon|select/Basic Usage', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        SelectModule,
        BrowserAnimationsModule,
        ReactiveFormsModule
      ]
    })
  )
  .add('Select', () => (
    {
      props: {
        control: new UntypedFormControl()
      },
      template: `
        <ar-select [formControl]="control">
          <ar-select-option
            [value]="1"
            label="Static option 1 with very long label to display text overflow property. This text should be displayed as title!"
          >
            Static option 1 with very long label to display text overflow property. This text should be displayed as title!
          </ar-select-option>
          <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
          <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
          <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
        </ar-select>
      `
    }
  ), { notes })
  .add('Multi Select', () => (
    {
      props: {
        control: new UntypedFormControl()
      },
      template: `
        <ar-select-multiple [formControl]="control">
          <ar-select-option
            [value]="1"
            label="Static option 1 with very long label to display text overflow property. This text should be displayed as title!"
          >
            Static option 1 with very long label to display text overflow property. This text should be displayed as title!
          </ar-select-option>
          <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
          <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
          <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
        </ar-select-multiple>
      `
    }
  ), { notes })
  .add('Autosuggest', () => ({
    props: {
      control: new UntypedFormControl(),
      dataSource: new StaticSelectDataSource(options)
    },
    template: `
      <ar-autosuggest [formControl]="control" placeholder="Autosuggest component">
        <ar-select-option-async [dataSource]="dataSource">
          <ng-template let-label="label" let-data="data">
            <div>{{ label }}</div>
            <small>{{ data?.subtitle }}</small>
          </ng-template>
        </ar-select-option-async>
      </ar-autosuggest>
    `
  }), { notes })
  .add('Custom markup inside dropdown', () => (
    {
      props: {
        control: new UntypedFormControl()
      },
      template: `
        <ar-select [formControl]="control">
          <ng-container before>
            <span>This text will be placed as static text at the top of dropdown</span>
          </ng-container>
          <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
          <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
          <div>I appear at the options section </div>
          <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
          <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="5" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="6" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="7" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="8" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="9" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="10" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="11" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="12" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="13" label="Static option 4">Static option 4</ar-select-option>
          <ar-select-option [value]="14" label="Static option 4">Static option 4</ar-select-option>
          <ng-container after>
            <span>This text will be placed as static text at the bottom of dropdown</span>
          </ng-container>
        </ar-select>
      `
    }
  ), { notes })
  .add('Options group', () => (
    {
      props: {
        control: new UntypedFormControl()
      },
      template: `
        <ar-select [formControl]="control">
          <ar-select-option-group>
            <h5>Group 1</h5>
            <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
            <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
            <small>Feel a free to place any content.</small>
          </ar-select-option-group>
          <ar-select-option-group>
            <h5>Group 2</h5>
            <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
            <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
          </ar-select-option-group>
        </ar-select>
      `
    }
  ), { notes })
  .add('Use object values', () => (
    {
      props: {
        data: [
          { id: 1, label: 'label 1', otherStaff: { data: 'test' } },
          { id: 2, label: 'label 2', otherStaff: { data: 'test' } },
          { id: 3, label: 'label 3', otherStaff: { data: 'test' } },
        ],
        compareFn: (itemA: any, itemB: any) => itemA && itemB && (itemA.id === itemB.id),
        control: new UntypedFormControl()
      },
      template: `
        <ar-select [formControl]="control" [compareFn]="compareFn">
          <ar-select-option *ngFor="let item of data"
            [value]="item"
            [label]="item.label">
            {{ item.label }}
          </ar-select-option>
        </ar-select>
      `
    }
  ), { notes })
  .add('Placeholder & sizing', () => (
    {
      props: {
        control: new UntypedFormControl()
      },
      template: `
        <ar-select
          [formControl]="control"
          arBsSize="sm"
          placeholder="This placeholder should have text overflow styles to display as single line"
        >
          <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
          <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
          <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
          <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
        </ar-select>
        <ar-select-multiple [formControl]="control" arBsSize="md"
          placeholder="This placeholder should have text overflow styles to display as single line"
        >
          <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
          <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
          <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
          <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
        </ar-select-multiple>
        <ar-select [formControl]="control" arBsSize="lg" placeholder="Large">
          <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
          <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
          <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
          <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
        </ar-select>
      `
    }
  ), { notes });
