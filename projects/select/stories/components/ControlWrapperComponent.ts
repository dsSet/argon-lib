import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';


@Component({
  selector: 'story-control-wrapper',
  template: `
    <div class="btn-group">
      <button class="btn btn-sm btn-secondary" *ngIf="control?.enabled" (click)="disable()">Disable</button>
      <button class="btn btn-sm btn-secondary" *ngIf="control?.disabled" (click)="enable()">Enable</button>
      <button class="btn btn-sm btn-secondary" (click)="reset()">Reset</button>
      <button class="btn btn-sm btn-secondary" (click)="validate()">Validate</button>
    </div>
    <pre class="state">
      value: {{ control?.value | json }},
      disabled: {{ control?.disabled | json }},
      touched: {{ control?.touched | json }},
      valid: {{ control?.valid | json }}
    </pre>
    <ng-content></ng-content>
  `,
  styles: [
    `:host {
      display: block;
    }`,
    `.btn-group {
      margin-bottom: 2rem;
    }`,
    `.state {
      margin-bottom: 2rem;
    }`
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlWrapperComponent {

  @Input() control: UntypedFormControl;

  disable() {
    this.control.disable();
  }

  enable() {
    this.control.enable();
  }

  reset() {
    this.control.reset();
  }

  validate() {
    this.control.markAsTouched();
    this.control.updateValueAndValidity();
  }

}
