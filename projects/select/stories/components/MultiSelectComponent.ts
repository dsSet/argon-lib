import { Component, forwardRef, HostBinding, OnDestroy, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractRegistry, SelectDirective, SelectMultipleRegistryService } from '../index';

@Component({
  selector: 'story-select-multiple',
  template: `
    <button class="btn btn-sm btn-link btn-block" (click)="cleanSelect()">Clean</button>
    <ng-content></ng-content>
  `,
  styles: [
    `:host {
      display: block;
      width: 300px;
      height: 450px;
      outline: 0;
      border: 1px solid #6c757d;
      border-radius: 3px;
      overflow: auto;
    }`,
    `:host:focus, :host:active {
      outline: 0;
      border-color: #22A293;
    }`
  ],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MultiSelectComponent), multi: true },
    { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
  ]
})
export class MultiSelectComponent extends SelectDirective implements OnInit, OnDestroy {

  @HostBinding('attr.tabindex') tabIndex = 0;

  @HostBinding('attr.role') role = 'listbox';

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  filterEvent(event: KeyboardEvent): boolean {
    return true;
  }

  public cleanSelect() {
    this.registry.fireValueUpdate(null);
    this.keyManager.setActiveItem(-1);
  }
}
