import { BaseShape, StateInterface } from '@argon/tools';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SelectDataItemInterface, SelectStateInterface, SelectRegistryMixin } from '../index';

@Component({
  selector: 'story-select-all',
  template: '<button class="btn btn-sm btn-link btn-block" (click)="selectAll()" [disabled]="disabled">Select All</button>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectAllButtonComponent extends SelectRegistryMixin(BaseShape) {

  public disabled: boolean;

  public selectAll() {
    const values = this.registry.optionItems.toArray().map((item: SelectDataItemInterface) => item.value);
    this.registry.fireValueUpdate(values);
  }

  handleUpdateState(nextState: StateInterface<SelectStateInterface>): void {
    const { changes } = nextState;

    if (changes.value) {
      this.disabled = changes.value.currentValue && this.registry.optionItems.length === changes.value.currentValue.length;
    }

    super.handleUpdateState(nextState);
  }
}
