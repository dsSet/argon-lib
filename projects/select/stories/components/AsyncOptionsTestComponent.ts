import { Component, OnInit, SimpleChanges } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { Observable, Subject, Subscription, timer } from 'rxjs';
import { map } from 'rxjs/operators';
import { AbstractRegistry, SelectDataItemInterface, SelectDataSource } from '../index';

/**
 * Should correct update state view ic case when component receive options after initial mount.
 */


class AsyncDataSource extends SelectDataSource {

  private subject = new Subject<Array<SelectDataItemInterface>>();

  private subscription: Subscription;

  connect(registry: AbstractRegistry): Observable<Array<SelectDataItemInterface>> {
    this.subscription = timer(1500, 1500).pipe(
      map((n: number) => ([
        {
          label: `Async 1 - ${n} - tick`,
          value: 1
        },
        {
          label: `Async 1 - ${n} - tick`,
          value: 2
        },
        {
          label: `Async 1 - ${n} - tick`,
          value: 3
        },
      ] as Array<SelectDataItemInterface>))
    ).subscribe((data: any) => {
      this.subject.next(data);
    });

    return this.subject.asObservable();
  }

  disconnect(): void {
    this.subscription.unsubscribe();
  }

  onChange(changes: SimpleChanges) {
  }


}

@Component({
  selector: 'story-async-options-test',
  template: `
    <ar-select-multiple [formControl]="control" [trackBy]="trackByFunction">
      <ar-select-option-async [dataSource]="dataSource" [trackBy]="trackByFunction" ></ar-select-option-async>
    </ar-select-multiple>
  `
})
export class AsyncOptionsTestComponent implements OnInit {

  public control = new UntypedFormControl([1]);

  public dataSource: SelectDataSource;

  public trackByFunction = (index: number, item: SelectDataItemInterface) => item.label;

  ngOnInit(): void {
    this.dataSource = new AsyncDataSource();
  }

}
