import { BehaviorSubject, Observable } from 'rxjs';
import { SimpleChanges } from '@angular/core';
import { AbstractRegistry, SelectDataSource, SelectDataItemInterface } from '../index';

export class CustomDataSource extends SelectDataSource {

  private options: BehaviorSubject<Array<SelectDataItemInterface>>;

  private registry: AbstractRegistry;

  private optionsCount = 15;

  private isOdd = true;

  private isEven = true;

  connect(registry: AbstractRegistry): Observable<Array<SelectDataItemInterface>> {
    this.registry = registry;
    this.options = new BehaviorSubject(this.createFakeOptions(this.optionsCount));
    return this.options.asObservable();
  }

  disconnect(): void {
    this.options.complete();
  }

  onChange(changes: SimpleChanges) {
    const { query } = changes;
    if (!query) {
      return;
    }

    this.updateOptions(query.currentValue);
  }

  public setOdd() {
    this.isOdd = true;
    this.isEven = false;
    this.updateOptions();
  }

  public setEven() {
    this.isEven = true;
    this.isOdd = false;
    this.updateOptions();
  }

  public setAll() {
    this.isOdd = true;
    this.isEven = true;
    this.updateOptions();
  }

  private updateOptions(query: string = '') {
    this.options.next(this.createFakeOptions(this.optionsCount, query));
  }

  private createFakeOptions(count: number = 0, query: string = ''): Array<SelectDataItemInterface> {
    const options = [];
    for (let i = 0; i < count; i++) {
      if (i % 2 === 0 && this.isOdd) {
        options.push(this.createFakeOption(i, query));
      }

      if (i % 2 !== 0 && this.isEven) {
        options.push(this.createFakeOption(i, query));
      }
    }
    return options;
  }

  private createFakeOption(id: number, query: string): SelectDataItemInterface {
    return {
      value: id,
      label: query ? `Mock Option ${id} (created with query "${query}")` : `Mock Option ${id}`,
      data: id,
      disabled: false
    };
  }

}
