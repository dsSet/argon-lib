import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UntypedFormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { ControlWrapperComponent } from './components/ControlWrapperComponent';
import { SelectModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/index.md').default
};

storiesOf('Argon|select/NgControl States', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        ControlWrapperComponent
      ],
      imports: [
        CommonModule,
        SelectModule,
        BrowserAnimationsModule,
        ReactiveFormsModule
      ]
    })
  )
  .add('Select Single', () => (
    {
      props: {
        control: new UntypedFormControl(null, [Validators.required])
      },
      template: `
        <story-control-wrapper [control]="control">
          <ar-select [formControl]="control">
            <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
            <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
            <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
            <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
          </ar-select>
        </story-control-wrapper>
      `
    }
  ), { notes })
  .add('Select Multiple', () => (
    {
      props: {
        control: new UntypedFormControl(null, [Validators.required])
      },
      template: `
        <story-control-wrapper [control]="control">
          <ar-select-multiple [formControl]="control">
            <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
            <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
            <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
            <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
          </ar-select-multiple>
        </story-control-wrapper>
      `
    }
  ), { notes })
  .add('Sync select controls', () => (
    {
      props: {
        control: new UntypedFormControl(2, [Validators.required])
      },
      template: `
        <story-control-wrapper [control]="control">
          <ar-select [formControl]="control" [value]="control.value">
            <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
            <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
            <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
            <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
          </ar-select>
          <ar-select [formControl]="control" [value]="control.value">
            <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
            <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
            <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
            <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
          </ar-select>
        </story-control-wrapper>
      `
    }
  ), { notes });
