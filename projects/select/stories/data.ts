import { SelectDataItemInterface } from './index';

export const options: Array<SelectDataItemInterface> = [
  { value: 1, label: 'Option 1', data: { subtitle: 'subtitle for option 1' } },
  { value: 2, label: 'Option 2', data: { subtitle: 'subtitle for option 2' } },
  { value: 3, label: 'Option 3', data: { subtitle: 'subtitle for option 3' } },
  { value: 4, label: 'Option 4', data: { subtitle: 'subtitle for option 4' } },
  { value: 5, label: 'Option 5', data: { subtitle: 'subtitle for option 5' } },
  { value: 6, label: 'Option 6', data: { subtitle: 'subtitle for option 6' } },
  { value: 7, label: 'Option 7', data: { subtitle: 'subtitle for option 7' } },
  { value: 8, label: 'Option 8', data: { subtitle: 'subtitle for option 8' } },
  { value: 9, label: 'Option 9', data: { subtitle: 'subtitle for option 9' } },
];
