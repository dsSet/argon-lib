import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { CustomDataSource } from './models/CustomDataSource';
import { SelectModule, StaticSelectDataSource } from './index';
import { options } from './data';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/Async.md').default + require('../docs/index.md').default
};

storiesOf('Argon|select/Async options and Filter', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        SelectModule,
        BrowserAnimationsModule,
        ReactiveFormsModule
      ]
    })
  )
  .add('Implement options filter', () => ({
    props: {
      control: new UntypedFormControl(),
      dataSource: new StaticSelectDataSource(options)
    },
    template: `
      <ar-select [formControl]="control">
        <ar-select-search before placeholder="Type to filter"></ar-select-search>
        <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
      </ar-select>
    `
  }), { notes })
  .add('Filter with static options', () => ({
    props: {
      control: new UntypedFormControl(),
      dataSource: new StaticSelectDataSource(options)
    },
    template: `
      <ar-select [formControl]="control">
        <ar-select-search before placeholder="Type to filter"></ar-select-search>
        <ar-select-option [value]="10" label="Static option">This is option outside of filter</ar-select-option>
        <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
      </ar-select>
    `
  }), { notes })
  .add('Template for async option', () => ({
    props: {
      control: new UntypedFormControl(),
      dataSource: new StaticSelectDataSource(options)
    },
    template: `
      <ar-select [formControl]="control">
        <ar-select-search before placeholder="Type to filter"></ar-select-search>
        <ar-select-option-async [dataSource]="dataSource">
          <ng-template let-value="value" let-label="label" let-data="data">
            <div [ngStyle]="{ color: value % 2 === 0 ? 'purple' : 'gold' }">
              <div>{{label}}</div>
              <small>{{ data?.subtitle }}</small>
            </div>
          </ng-template>
        </ar-select-option-async>
      </ar-select>
    `
  }), { notes })
  .add('Use Custom DataSource', () => ({
    props: {
      control: new UntypedFormControl(),
      dataSource: new CustomDataSource()
    },
    template: `
      <ar-select [formControl]="control">
        <ar-select-search before placeholder="Type to filter"></ar-select-search>
        <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
      </ar-select>
    `
  }), { notes })
  .add('Add custom filter to data source', () => ({
    props: {
      control: new UntypedFormControl(),
      dataSource: new CustomDataSource()
    },
    template: `
      <ar-select [formControl]="control">
        <div before class="btn-group">
          <button class="btn btn-sm btn-dark" (click)="dataSource.setOdd()">Odd</button>
          <button class="btn btn-sm btn-success" (click)="dataSource.setAll()">All</button>
          <button class="btn btn-sm btn-primary" (click)="dataSource.setEven()">Even</button>
        </div>
        <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
      </ar-select>
    `
  }), { notes })
  .add('Add Several Data Sources', () => ({
    props: {
      control: new UntypedFormControl(),
      dataSourceCustom: new CustomDataSource(),
      dataSourceStatic: new StaticSelectDataSource(options)
    },
    template: `
      <ar-select [formControl]="control">
        <ar-select-search before placeholder="Type to filter"></ar-select-search>
        <ar-select-option-group [hideEmpty]="true">
          <small>Static Data Source</small>
          <ar-select-option-async [dataSource]="dataSourceStatic"></ar-select-option-async>
        </ar-select-option-group>
        <ar-select-option-group>
          <small>Custom Data Source</small>
          <ar-select-option-async [dataSource]="dataSourceCustom"></ar-select-option-async>
        </ar-select-option-group>
      </ar-select>
    `
  }), { notes })
  .add('Set labels for options out of data source', () => ({
    props: {
      control: new UntypedFormControl(55),
      dataSource: new StaticSelectDataSource(options)
    },
    template: `
      <ar-select [formControl]="control">
        <ar-select-option-default [value]="55" label="I'm not present in data source (55)">
          <strong>I'm not present in data source (55)</strong>
        </ar-select-option-default>
        <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
      </ar-select>
    `
  }), { notes });
