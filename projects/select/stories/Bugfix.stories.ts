import map from 'lodash/map';
import range from 'lodash/range';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { AsyncOptionsTestComponent } from './components/AsyncOptionsTestComponent';
import { SelectDataItemInterface, SelectModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/Select.md').default + require('../docs/index.md').default
};

storiesOf('Argon|select/Bugfix', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        SelectModule,
        BrowserAnimationsModule,
        ReactiveFormsModule
      ],
      declarations: [
        AsyncOptionsTestComponent
      ]
    })
  )
  .add('Dynamic options update', () => (
    {
      component: AsyncOptionsTestComponent,
      moduleMetadata: { }
    }
  ), { notes })
  .add('Render 1000 async items', () => (
    {
      props: {
        options: map(range(1, 1000), n => ({ value: n, label: `option ${n}` })),
        trackBy: (index: number, item: SelectDataItemInterface) => item.value
      },
      template: `
        <ar-select-multiple>
          <ar-select-search before placeholder="Type to filter"></ar-select-search>
          <ar-select-option-filter [trackBy]="trackBy" [options]="options"></ar-select-option-filter>
        </ar-select-multiple>
      `
    }
  ), { notes })
  .add('Update label on control changes', () => ({
    props: {
      control: new UntypedFormControl(1),
      value: 1,
      handleClick() {
        this.value = Math.round(Math.random() * 52);
        this.control = new UntypedFormControl(this.value);
      }
    },
    template: `
      <ar-autosuggest arFormControl [formControl]="control">
        <ar-select-option-default [value]="value" [label]="'test' + value"></ar-select-option-default>
      </ar-autosuggest>
      <button (click)="handleClick()">Change</button>
    `
  }));
