import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { SelectAllButtonComponent } from './components/SelectAllButtonComponent';
import { MultiSelectComponent } from './components/MultiSelectComponent';
import { SelectModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/CustomSelect.md').default + require('../docs/index.md').default
};

storiesOf('Argon|select/Custom Select', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        SelectAllButtonComponent,
        MultiSelectComponent
      ],
      imports: [
        CommonModule,
        SelectModule,
        BrowserAnimationsModule,
        ReactiveFormsModule
      ]
    })
  )
  .add('Custom Select component', () => (
    {
      props: {
        control: new UntypedFormControl()
      },
      template: `
        <story-select-multiple [formControl]="control">
          <story-select-all before></story-select-all>
          <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
          <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
          <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
          <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
        </story-select-multiple>
      `
    }
  ), { notes });
