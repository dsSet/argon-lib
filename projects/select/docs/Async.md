#Async options and Filtering

##Static options with filter
````typescript

const options: Array<SelectDataItemInterface> = [
  { value: 1, label: 'Option 1' },
  { value: 2, label: 'Option 2' },
  { value: 3, label: 'Option 3' },
]

const dataSource = new StaticSelectDataSource(options); 

````

````html
<ar-select [formControl]="control">
  <ar-select-search before placeholder="Type to filter"></ar-select-search>
  <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
</ar-select>
````

##Mix filtering and non filtering options
````html
<ar-select [formControl]="control">
  <ar-select-search before placeholder="Type to filter"></ar-select-search>
  <ar-select-option [value]="10" label="Static option">This is option outside of filter</ar-select-option>
  <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
</ar-select>
````

##Add custom template for the async options
````html
<ar-select [formControl]="control">
  <ar-select-search before placeholder="Type to filter"></ar-select-search>
  <ar-select-option-async [dataSource]="dataSource">
    <ng-template let-value="value" let-label="label" let-data="data">
      <div [ngStyle]="{ color: value % 2 === 0 ? 'purple' : 'gold' }">
        <div>{{label}}</div>
        <small>{{ data?.subtitle }}</small>
      </div>
    </ng-template>
  </ar-select-option-async>
</ar-select>
````

##Implement custom data source
````typescript
import { SelectDataSource } from '../';
import { SelectDataItemInterface } from '../';
import { BehaviorSubject, Observable } from 'rxjs';
import { AbstractRegistry } from '../';
import { SimpleChanges } from '@angular/core';


export class CustomDataSource extends SelectDataSource {

  private options: BehaviorSubject<Array<SelectDataItemInterface>>;

  private registry: AbstractRegistry;

  private optionsCount = 15;

  private isOdd = true;

  private isEven = true;

  connect(registry: AbstractRegistry): Observable<Array<SelectDataItemInterface>> {
    this.registry = registry;
    this.options = new BehaviorSubject(this.createFakeOptions(this.optionsCount));
    return this.options.asObservable();
  }

  disconnect(): void {
    this.options.complete();
  }

  onChange(changes: SimpleChanges) {
    const { query } = changes;
    if (!query) {
      return;
    }

    this.updateOptions(query.currentValue);
  }

  public setOdd() {
    this.isOdd = true;
    this.isEven = false;
    this.updateOptions();
  }

  public setEven() {
    this.isEven = true;
    this.isOdd = false;
    this.updateOptions();
  }

  public setAll() {
    this.isOdd = true;
    this.isEven = true;
    this.updateOptions();
  }

  private updateOptions(query: string = '') {
    this.options.next(this.createFakeOptions(this.optionsCount, query));
  }

  private createFakeOptions(count: number = 0, query: string = ''): Array<SelectDataItemInterface> {
    const options = [];
    for (let i = 0; i < count; i++) {
      if (i % 2 === 0 && this.isOdd) {
        options.push(this.createFakeOption(i, query));
      }

      if (i % 2 !== 0 && this.isEven) {
        options.push(this.createFakeOption(i, query));
      }
    }
    return options;
  }

  private createFakeOption(id: number, query: string): SelectDataItemInterface {
    return {
      value: id,
      label: query ? `Mock Option ${id} (created with query "${query}")` : `Mock Option ${id}`,
      data: id,
      disabled: false
    };
  }

}
````

##Apply any filter to the custom data source
````html
<ar-select [formControl]="control">
  <div before class="btn-group">
    <button class="btn btn-sm btn-dark" (click)="dataSource.setOdd()">Odd</button>
    <button class="btn btn-sm btn-success" (click)="dataSource.setAll()">All</button>
    <button class="btn btn-sm btn-primary" (click)="dataSource.setEven()">Even</button>
  </div>
  <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
</ar-select>
````

##Add Several Data Sources

````html
<ar-select [formControl]="control">
  <ar-select-search before placeholder="Type to filter"></ar-select-search>
  <ar-select-option-group [hideEmpty]="true">
    <small>Static Data Source</small>
    <ar-select-option-async [dataSource]="dataSourceStatic"></ar-select-option-async>
  </ar-select-option-group>
  <ar-select-option-group>
    <small>Custom Data Source</small>
    <ar-select-option-async [dataSource]="dataSourceCustom"></ar-select-option-async>
  </ar-select-option-group>
</ar-select>
````

##Set labels for options out of data source

````html
<ar-select [formControl]="control">
  <ar-select-option-default [value]="55" label="I'm not present in data source (55)">
    <strong>I'm not present in data source (55)</strong>
  </ar-select-option-default>
  <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
</ar-select>
````

