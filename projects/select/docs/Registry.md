#Select Registry

````typescript
import { Injectable, Injector, OnDestroy, QueryList } from '@angular/core';
import { BaseShape, CompareValuesMixin, StateSubject, StateInterface } from '@argon/tools';
import { SelectStateInterface } from '..';
import { SelectDataItemInterface } from '..';
import { SelectOptionInterface } from '..';

@Injectable()
export class AbstractRegistry extends CompareValuesMixin(BaseShape) implements OnDestroy {

  // Current select state with latest changes
  public readonly selectState: StateSubject<SelectStateInterface>;

  // alias for the selectState. Contains only current state
  public readonly state: SelectStateInterface;

  // Flag for check select value
  public readonly hasValue: boolean;

  // List of active (selected) data items.
  public readonly activeDataItems: Array<SelectDataItemInterface>;

  // Full list of data items (For some reasons may contains duplicated values. For example in case with async options)
  public readonly dataItems: Array<SelectDataItemInterface>;

  // List of current options
  public readonly optionItems: QueryList<SelectOptionInterface>;

  constructor(injector: Injector);

  ngOnDestroy(): void;

  /** Update isOpened state data */
  public setIsOpened(isOpened: boolean): void;

  /** Update value state data */
  public setValue(value: any): void;

  /** Update disabled state data */
  public setDisabled(disabled: boolean): void;

  /** Update query state data */
  public setQuery(query: string): void;

  /** Register onChange fn for ValueAccessor compatibility */
  public registerOnChange(fn: any);

  /** Register onTouch fn for ValueAccessor compatibility */
  public registerOnTouched(fn: any);

  /** Call onChange function with next value. Should not based on state. */
  public handleValueChanges(value: any);

  /** Toggle single option in value */
  public handleOptionToggle(value: any);

  /** determinate is value selected */
  public isOptionSelected(value: any): boolean;

  /** change value inside state and emit changes with changeFn */
  public fireValueUpdate(value: any): void;

  /** register option item at the service */
  public registerOption(option: SelectOptionInterface): void;

  /** remove option item from the service */
  public unregisterOption(option: SelectOptionInterface): void;

  /** register data item at the service */
  public registerDataItem(item: SelectDataItemInterface): void;

  /** remove data item from the service */
  public unregisterDataItem(item: SelectDataItemInterface): void;

  /** update select options order (for correct keyboard events selection) */
  protected reorderItems(items: Array<SelectOptionInterface>): Array<SelectOptionInterface>;

  /** Find data items list by value */
  protected findDataItems(value: any): Array<SelectDataItemInterface>;

}

````

##Set values.
There is 3 ways to update value with Registry:
1. `public setValue(value: any): void;` - Update value only inside internal state.
For example this method is using with `writeValue(obj: any): void` method from `ControlValueAccessor` interface.

2. `public handleValueChanges(value: any);` - Update value with `onChange` callback only.
This callback comes from `ControlValueAccessor` interface.

3. `public fireValueUpdate(value: any): void;` - Update value both with `onChange` and inside internal state.
This method can be called from select UI.


##Difference between `SelectOptionInterface` and `SelectDataItemInterface`.
`SelectOptionInterface` represent logic of option view. So user able to select, highlight or just see inside options list.

`SelectDataItemInterface` represent only data logic and hidden from user.
This interface is using for determinate option label, data, value or disabled state even in cases when select has no this options at the view. 


##`public readonly selectState: StateSubject<SelectStateInterface>;`

Select component is using custom private state. This property contains both state values and latest state changes.
Subject will emit `StateInterface` data. `changes` property has the same logic as on `ngOnChanges` hook.

````typescript
import { SimpleChanges } from '@angular/core';

export interface StateInterface {

  state: SelectStateInterface;

  changes: SimpleChanges;

}

// where

export interface SelectStateInterface {

  /** dropdown (or other ui) opened */
  isOpened: boolean;

  /** current select value */
  value: any;

  /** disable flag */
  disabled: boolean;

  /** current search query */
  query: string;

}

````


##Implementations

`AbstractRegistry` already implemented with two services `SelectMultipleRegistryService` (for multi select) and `SelectRegistryService` (for single select).



