#Custom UI component

You can implement custom ui for working with select component.
The first step of implementation - extends new component with `SelectRegistryMixin`.

Signature: 

````
function SelectRegistryMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<SelectRegistryExtensionInterface>
````

> Tip: Use `BaseShape` as prototype.

````typescript

import { SelectRegistryMixin } from '../';
import { BaseShape, StateInterface } from '@argon/tools';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SelectStateInterface } from '../';
import { SelectDataItemInterface } from '../';

@Component({
  selector: 'story-select-all',
  template: '<button class="btn btn-sm btn-link" (click)="selectAll()" [disabled]="disabled">Select All</button>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectAllButtonComponent extends SelectRegistryMixin(BaseShape) {

  //...
}

````

What mixin doing? Add abstract implementation for your component.
In practice you will get property for accessing to the select registry: `public registry: AbstractRegistry;`
and callback to determinate select state and select state changes `public handleUpdateState(state: StateInterface<SelectStateInterface>): void`. For the full list of properties see `SelectRegistryExtensionInterface`.
 
The first part of implementation: to add action for select component value change. Let's implement select all function for the multi select:

 ````typescript
 
 import { SelectRegistryMixin } from '../';
 import { BaseShape, StateInterface } from '@argon/tools';
 import { ChangeDetectionStrategy, Component } from '@angular/core';
 import { SelectStateInterface } from '../';
 import { SelectDataItemInterface } from '../';
 
 @Component({
   selector: 'story-select-all',
   template: '<button class="btn btn-sm btn-link" (click)="selectAll()" [disabled]="disabled">Select All</button>',
   changeDetection: ChangeDetectionStrategy.OnPush
 })
 export class SelectAllButtonComponent extends SelectRegistryMixin(BaseShape) {
 
   public selectAll() {
       // Map current select options to array of values
       const values = this.registry.optionItems.toArray().map((item: SelectDataItemInterface) => item.value);
       // Update select with values
       this.registry.fireValueUpdate(values);
     }
   
   //...
 }
 
 ````
 
The next step: to disable select all button in case if all options already selected:

````typescript

import { SelectRegistryMixin } from '../';
import { BaseShape, StateInterface } from '@argon/tools';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SelectStateInterface } from '../';
import { SelectDataItemInterface } from '../';

@Component({
  selector: 'story-select-all',
  template: '<button class="btn btn-sm btn-link" (click)="selectAll()" [disabled]="disabled">Select All</button>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectAllButtonComponent extends SelectRegistryMixin(BaseShape) {

  public disabled: boolean;

  //...
  
  handleUpdateState(nextState: StateInterface<SelectStateInterface>): void {
    const { changes } = nextState;

    if (changes.value) {
      this.disabled = changes.value.currentValue && this.registry.optionItems.length === changes.value.currentValue.length;
    }

    super.handleUpdateState(nextState);
  }
  
}

````

For this purposes we override `handleUpdateState` method. If current value length has the same length as current options list we will set disabled flag to `true`.
Important to keep parent call for that method `super.handleUpdateState(nextState);` because parent class update component view with `ChangeDetectorRef` call. 

Several words about `nextState: StateInterface<SelectStateInterface>` impression. Select registry is using `StateInterface` for keep state and calculate state changes.


````typescript
import { SimpleChanges } from '@angular/core';

export interface StateInterface<TModel> {

  state: TModel;

  changes: SimpleChanges;

}
````

So you are able to determinate both current state and difference between current and prev states. `changes` property implemented by the same way as inside angular `ngOnChanges` hook. 

> You can find `AbstractRegistry` documentation at the `@argon/select/docs/Registry.md` file

Complete component code:

````typescript
import { SelectRegistryMixin } from '../';
import { BaseShape, StateInterface } from '@argon/tools';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SelectStateInterface } from '../';
import { SelectDataItemInterface } from '../';

@Component({
  selector: 'story-select-all',
  template: '<button class="btn btn-sm btn-link" (click)="selectAll()" [disabled]="disabled">Select All</button>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectAllButtonComponent extends SelectRegistryMixin(BaseShape) {

  public disabled: boolean;

  public selectAll() {
    const values = this.registry.optionItems.toArray().map((item: SelectDataItemInterface) => item.value);
    this.registry.fireValueUpdate(values);
  }

  handleUpdateState(nextState: StateInterface<SelectStateInterface>): void {
    const { changes } = nextState;

    if (changes.value) {
      this.disabled = changes.value.currentValue && this.registry.optionItems.length === changes.value.currentValue.length;
    }

    super.handleUpdateState(nextState);
  }
}
````
