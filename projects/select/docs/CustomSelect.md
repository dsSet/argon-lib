# Custom Select implementation

Still need more functionality for the Select component? Feel a free to build custom one.

Let's implement simple list as multi select component.

## 1. The first implementation step is extend new component from `SelectDirective`:

````typescript
import { SelectDirective } from '..';
import { Component, forwardRef, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'story-select-multiple',
  template: `
    <ng-content></ng-content>
    <ar-active-data-items [trackBy]="trackBy" ></ar-active-data-items>
  `
})
export class MultiSelectComponent extends SelectDirective {
  
  // ...
  
}
````

Select directive contains boilerplate code for connecting component to `NG_VALUE_ACCESSOR`, `AbstractRegistry` and keyboard events.

> `<ar-active-data-items [trackBy]="trackBy" >` - sometimes you are need to keep selected options data inside control (During async filtering for example).
> Just place this component as a part of template. It will keep selected data for you.

## 2. The next step: add providers for the component:

````typescript
import { SelectDirective } from '../';
import { Component, forwardRef, HostBinding, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractRegistry } from '..';
import { SelectMultipleRegistryService } from '..';

@Component({
  selector: 'story-select-multiple',
  template: `
    <ng-content></ng-content>
    <ar-active-data-items [trackBy]="trackBy" ></ar-active-data-items>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MultiSelectComponent), multi: true },
    { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
  ]
})
export class MultiSelectComponent extends SelectDirective {
  
  // ...
  
}
````

`{ provide: AbstractRegistry, useClass: SelectMultipleRegistryService }` this provider needs to add Select registry service to your component.
You can provide one of the existing services `SelectMultipleRegistryService` for multi select and `SelectRegistryService` for single select.

`{ provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MultiSelectComponent), multi: true }`: `SelectDirective` already contains functionality
for connecting `AbstractRegistry` to the `NG_VALUE_ACCESSOR`, you just need to add this provider for get forms functionality.

Select directive already extended from `SeelctRegistryMixin` so you have access to the `registry` property.
Directive also has two input properties: 

````typescript
// function for compare option values
@Input() compareFn: CompareFn = Comparators.defaultComparator;

// function for improve data items rendering
@Input() trackBy: TrackByFunction<SelectDataItemInterface>;
````

## 3. Keyboard events

`SelectDirective` contains keyboard events for the options selection (using `ActiveDescendantKeyManager` from `@angular/cdk/a11y` package).
Also has filter for the keyboard events with `filterEvent(event: KeyboardEvent): boolean` method.

> This method is using select `isOpened` state by default. You can disable keyboard events or implement custom logic just with overriding this method.  

Let's allow keyboard events for this component:

````typescript
import { SelectDirective } from '../';
import { Component, forwardRef, HostBinding, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractRegistry } from '..';
import { SelectMultipleRegistryService } from '..';

@Component({
  selector: 'story-select-multiple',
  template: `
    <ng-content></ng-content>
    <ar-active-data-items [trackBy]="trackBy" ></ar-active-data-items>
  `,
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MultiSelectComponent), multi: true },
    { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
  ]
})
export class MultiSelectComponent extends SelectDirective {
  
  // ...
   
  filterEvent(event: KeyboardEvent): boolean {
    return true;
  }
  
  // ...
  
}
````

## 4. Let's make our control focusable

````typescript
import { SelectDirective } from '../';
import { Component, forwardRef, HostBinding, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractRegistry } from '..';
import { SelectMultipleRegistryService } from '..';

@Component({
  selector: 'story-select-multiple',
  template: `
    <ng-content></ng-content>
    <ar-active-data-items [trackBy]="trackBy" ></ar-active-data-items>
  `,
  styles: [
      `:host {
        display: block;
        width: 300px;
        height: 450px;
        outline: 0;
        border: 1px solid #6c757d;
        border-radius: 3px;
        overflow: auto;
      }`,
      `:host:focus, :host:active {
        outline: 0;
        border-color: #22A293;
      }`
    ],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MultiSelectComponent), multi: true },
    { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
  ]
})
export class MultiSelectComponent extends SelectDirective {
  
  @HostBinding('attr.tabindex') tabIndex = 0;
  
  // Don't forget about a11y role
  @HostBinding('attr.role') role = 'listbox';
 
  // ...
}
````

## 5. The last step: to add clean button for this list:

````typescript
import { SelectDirective } from '../';
import { Component, forwardRef, HostBinding, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractRegistry } from '..';
import { SelectMultipleRegistryService } from '..';

@Component({
  selector: 'story-select-multiple',
  template: `
    <button class="btn btn-sm btn-link btn-block" (click)="cleanSelect()">Clean</button>
    <ng-content></ng-content>
    <ar-active-data-items [trackBy]="trackBy" ></ar-active-data-items>
  `,
  styles: [
      `:host {
        display: block;
        width: 300px;
        height: 450px;
        outline: 0;
        border: 1px solid #6c757d;
        border-radius: 3px;
        overflow: auto;
      }`,
      `:host:focus, :host:active {
        outline: 0;
        border-color: #22A293;
      }`
    ],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MultiSelectComponent), multi: true },
    { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
  ]
})
export class MultiSelectComponent extends SelectDirective {
  
  public cleanSelect() {
    // remove value from control and state
    this.registry.fireValueUpdate(null);
    // remove keyboard selection
    this.keyManager.setActiveItem(-1);
  }
 
  // ...
}
````

`keyManager` is `ActiveDescendantKeyManager` instance. `registry` is `AbstarctRegistry` service.

## 6. And finally full example:

````typescript
import { SelectDirective } from '..';
import { Component, forwardRef, HostBinding, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractRegistry } from '..';
import { SelectMultipleRegistryService } from '..';

@Component({
  selector: 'story-select-multiple',
  template: `
    <button class="btn btn-sm btn-link btn-block" (click)="cleanSelect()">Clean</button>
    <ng-content></ng-content>
    <ar-active-data-items [trackBy]="trackBy" ></ar-active-data-items>
  `,
  styles: [
    `:host {
      display: block;
      width: 300px;
      height: 450px;
      outline: 0;
      border: 1px solid #6c757d;
      border-radius: 3px;
      overflow: auto;
    }`,
    `:host:focus, :host:active {
      outline: 0;
      border-color: #22A293;
    }`
  ],
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MultiSelectComponent), multi: true },
    { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
  ]
})
export class MultiSelectComponent extends SelectDirective {

  @HostBinding('attr.tabindex') tabIndex = 0;

  @HostBinding('attr.role') role = 'listbox';

  filterEvent(event: KeyboardEvent): boolean {
    return true;
  }

  public cleanSelect() {
    this.registry.fireValueUpdate(null);
    this.keyManager.setActiveItem(-1);
  }
}
````

## 7. Usage

````html
<story-select-multiple [formControl]="control">
  <story-select-all before></story-select-all>
  <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
  <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
  <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
  <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
</story-select-multiple>
````

> Now we can use any select UI with this component.


