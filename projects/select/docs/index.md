# @argon/select package

- [Built in Components](./Select.md)
- [Async and Filtered Options](./Async.md)
- [Custom Select UI](./CustomUI.md)
- [Build Custom Select](./CustomSelect.md)
- [Abstract Registry](./Registry.md) 
  
## Components

### Select Component `<ar-select>` `<ar-select-multiple>` `<ar-autosuggest>`
|Property |Default value|Description
|---      |---          |---
|`@Input() placeholder: string`| `''`|placeholder message
|`@Input() arBsSize: BsSizeEnum`| `undefiend`| Size for the form control wrapper (e.g. `sm`, `md`, `lg`) 
|`@Input() compareFn: CompareFn`| `Comparators.defaultComparator`| Function for compare values for calculating state. By default strict compare is using (`valueA === valueB`). Can be useful if you working with `objects` as value 
|`@Input() trackBy: TrackByFunction<SelectDataItemInterface>`| `undefined`| Native `trackBy` for improve `*ngFor` rendering. This function will be apply for rendering Option like objects. 
|`@Input() value: any`| `undefined`| YOU DON'T NEED THIS PROPERTY IN REGULAR CASES. Pass value for sync select component with other UI form controls.  

### Select Option component `<ar-select-option>` `<ar-select-option-default>`

|Property |Default value|Description
|---      |---          |---
|`@Input() value: any`|`undefined`| Will be used for populate state (control value)
|`@Input() data: any`|`undefined`| Additional data for the option. Can be used for implementing custom select UI. 
|`@Input() label: any`|`undefined`| Option label. Will be used for display current value inside UI components  
|`@Input() disabled: boolean`|`undefined`| Disabled state for the property

### Select Option Group component `<ar-select-option-group>`

|Property |Default value|Description
|---      |---          |---
|`@Input() bordered: boolean`|`true`| Add border at the end of group
|`@Input() hideEmpty: boolean`|`undefined`| Hide group if has no options 

### Select Option Async `<ar-select-option-async>`

|Property |Default value|Description
|---      |---          |---
|`@Input() dataSource: SelectDataSource`|`undefined`| Data source for rendering options 
|`@Input() trackBy: TrackByFunction<SelectDataItemInterface>`|`undefined`| Native `trackBy` for improve `*ngFor` rendering. This function will be apply for rendering Option like objects.
|`@Output() change: EventEmitter<Array<SelectDataItemInterface>>()`| | Notification for options set changes

### Select Search component `<ar-select-search>`

|Property |Default value|Description
|---      |---          |---
|`@Input() placeholder: string`| `''`|placeholder message

### `<ar-active-data-items>`

|Property |Default value|Description
|---      |---          |--- 
|`@Input() trackBy: TrackByFunction<SelectDataItemInterface>`|`undefined`| Native `trackBy` for improve `*ngFor` rendering. This function will be apply for rendering Option like objects.

### Select Directive

|Property |Default value|Description
|---      |---          |---
|`@Input() compareFn: CompareFn`| `Comparators.defaultComparator`| Function for compare values for calculating state. By default strict compare is using (`valueA === valueB`). Can be useful if you working with `objects` as value 
|`@Input() trackBy: TrackByFunction<SelectDataItemInterface>`| `undefined`| Native `trackBy` for improve `*ngFor` rendering. This function will be apply for rendering Option like objects. 
|`@Input() value: any`| `undefined`| YOU DON'T NEED THIS PROPERTY IN REGULAR CASES. Pass value for sync select component with other UI form controls.

## Interfaces

### SelectDataItemInterface

Base interface for working with single option.


````typescript

export interface SelectDataItemInterface {

  /** option value */
  value: any;

  /** option label */
  label: string;

  /** option additional data */
  data?: any;

  /** option disable flag */
  disabled?: boolean;

}

````


### SelectOptionInterface

Option view interface.

````typescript
import { SelectDataItemInterface } from '../';
import { Highlightable } from '@angular/cdk/a11y';
import { ElementRef } from '@angular/core';


export interface SelectOptionInterface extends SelectDataItemInterface, Highlightable {

  element: ElementRef<HTMLElement>;

}
````


### SelectStateInterface

Internal state of select component.

````typescript

export interface SelectStateInterface {

  /** dropdown (or other ui) opened */
  isOpened: boolean;

  /** current select value */
  value: any;

  /** disable flag */
  disabled: boolean;

  /** current search query */
  query: string;

}

````

### SelectRegistryExtensionInterface

Definition of abstract class which will be created with `SelectRegistryMixin`.

````typescript
import { ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { StateInterface } from '@argon/tools';
import { AbstractRegistry } from '../';
import { SelectStateInterface } from '../';

export interface SelectRegistryExtensionInterface extends OnInit, OnDestroy {

  /** select component state service */
  registry: AbstractRegistry;

  changeDetectorRef: ChangeDetectorRef;

  ngOnInit(): void;

  ngOnDestroy(): void;

  /** callback for handling select state changes */
  handleUpdateState(state: StateInterface<SelectStateInterface>): void;

}

````


## Models

### SelectDataSource (abstract)

This is abstract class for connecting async options to select component. Method `onChange` will be called every time on select state changed.
See `SelectStateInterface` for clarification possible changes. `connect` method should returns stream array of select data items, `disconnect` method should close that stream.  

````typescript
import { Observable } from 'rxjs';
import { SimpleChanges } from '@angular/core';
import { AbstractRegistry } from '../';
import { SelectDataItemInterface } from '../';

export abstract class SelectDataSource {
  
  /** Will be used for connecting data source to the select component */
  public abstract connect(registry: AbstractRegistry): Observable<Array<SelectDataItemInterface>>;

  /** Will be called on removing data source from the model */
  public abstract disconnect(): void;

  /** Will be used for notify about current select state changes */
  public abstract onChange(changes: SimpleChanges);

}

````

### StaticSelectDataSource

Implementation of `SelectDataSource` for filtering static options list. Will filter options by `label`.

Usage:

````typescript

const options: Array<SelectDataItemInterface> = [
  { value: 1, label: 'Option 1' },
  { value: 2, label: 'Option 2' },
  { value: 3, label: 'Option 3' },
]

const dataSource = new StaticSelectDataSource(options); 

````

````html
<ar-select [formControl]="control">
  <ar-select-search before placeholder="Type to filter"></ar-select-search>
  <ar-select-option-async [dataSource]="dataSource"></ar-select-option-async>
</ar-select>
````
