# Select Component

## Usage

### Base Usage:

````html
<ar-select [formControl]="control">
  <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
  <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
  <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
  <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
</ar-select>
````

### Add custom html to dropdown

````html
<ar-select [formControl]="control">
  <ng-container before>
    <span>This text will be placed as static text at the top of dropdown</span>
  </ng-container>
  <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
  <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
  <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
  <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
  <ng-container after>
      <span>This text will be placed as static text at the bottom of dropdown</span>
    </ng-container>
</ar-select>
````

### Group options

````html
<ar-select [formControl]="control">
  <ar-select-option-group>
    <h5>Group 1</h5>
    <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
    <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
    <small>Feel a free to place any content.</small>
  </ar-select-option-group>
  <ar-select-option-group>
    <h5>Group 2</h5>
    <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>  
    <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
  </ar-select-option-group>
</ar-select>
````

### Use object as values

````typescript

const data: Array<any> = [
  { id: 1, label: 'label 1', otherStaff: { data: 'test' } },
  { id: 2, label: 'label 2', otherStaff: { data: 'test' } },
  { id: 3, label: 'label 3', otherStaff: { data: 'test' } },
];

const compareFn = (itemA: any, itemB: any) => itemA.id === itemB.id;

````

````html
<ar-select [formControl]="control" [compareFn]="compareFn">
  <ar-select-option *ngFor="let item of data"
    [value]="item"
    [label]="item.label">
    {{ item.label }}  
  </ar-select-option>
</ar-select>
````

### Multi Select

Multi Select Component works with the same UI interface.

````html
<ar-select-multiple [formControl]="control">
  <ar-select-option [value]="1" label="Static option 1">Static option 1</ar-select-option>
  <ar-select-option [value]="2" label="Static option 2">Static option 2</ar-select-option>
  <ar-select-option [value]="3" label="Static option 3">Static option 3</ar-select-option>
  <ar-select-option [value]="4" label="Static option 4">Static option 4</ar-select-option>
</ar-select-multiple>
```` 

### Autosuggest

Autosuggest Component works with the same UI interface.

````html
<ar-autosuggest [formControl]="control" placeholder="Autosuggest component">
  <ar-select-option-async [dataSource]="dataSource">
    <ng-template let-label="label" let-data="data">
      <div>{{ label }}</div>
      <small>{{ data?.subtitle }}</small>
    </ng-template>
  </ar-select-option-async>
</ar-autosuggest>
````
