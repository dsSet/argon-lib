import createSpy = jasmine.createSpy;
import { Subject } from 'rxjs';
import { SelectDataItemInterface } from '../lib/interfaces/SelectDataItemInterface';

export class MockDataSource {

  connectSubject = new Subject<Array<SelectDataItemInterface>>();

  connect = createSpy('connect').and.returnValue(this.connectSubject);

  disconnect = createSpy('disconnect');

  onChange = createSpy('onChange');

  destroy() {
    this.connectSubject.complete();
  }
}
