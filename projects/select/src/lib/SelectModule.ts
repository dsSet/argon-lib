import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { PortalModule } from '@angular/cdk/portal';
import { DropdownModule } from '@argon/dropdown';
import { KeyboardModule } from '@argon/keyboard';
import { StyleKitModule } from '@argon/style-kit';
import { SelectHandlerComponent } from './components/SelectHandler/SelectHandlerComponent';
import { SelectComponent } from './components/Select/SelectComponent';
import { SelectOptionComponent } from './components/SelectOption/SelectOptionComponent';
import { SelectOptionFilterComponent } from './components/SelectOptionFilter/SelectOptionFilterComponent';
import { SelectDirective } from './directives/SelectDirective';
import { SelectPlaceholderComponent } from './components/SelectPlaceholder/SelectPlaceholderComponent';
import { SelectLabelComponent } from './components/SelectLabel/SelectLabelComponent';
import { SelectCleanComponent } from './components/SelectClean/SelectCleanComponent';
import { SelectLayoutComponent } from './components/SelectLayout/SelectLayoutComponent';
import { SelectOptionGroupComponent } from './components/SelectOptionGroup/SelectOptionGroupComponent';
import { SelectSearchComponent } from './components/SelectSearch/SelectSearchComponent';
import { SelectOptionAsyncComponent } from './components/SelectOptionAsync/SelectOptionAsyncComponent';
import { SelectOptionDefaultComponent } from './components/SelectOptionDefault/SelectOptionDefaultComponent';
import { IconCloseComponent } from './components/IconClose/IconCloseComponent';
import { SelectBadgeComponent } from './components/SelectBadge/SelectBadgeComponent';
import { MultiSelectComponent } from './components/MultiSelect/MultiSelectComponent';
import { IconChevronComponent } from './components/IconChevron/IconChevronComponent';
import { ARGON_DROPDOWN_LAYOUT_PROVIDER, DropdownLayoutModel } from '@argon/dropdown';
import { AutosuggestComponent } from './components/Autosuggest/AutosuggestComponent';
import { ActiveDataItemsComponent } from './components/ActiveDataItems/ActiveDataItemsComponent';

@NgModule({
    imports: [
        CommonModule,
        PortalModule,
        ReactiveFormsModule,
        DropdownModule,
        KeyboardModule,
        StyleKitModule
    ],
    declarations: [
        SelectComponent,
        MultiSelectComponent,
        SelectHandlerComponent,
        SelectOptionComponent,
        SelectPlaceholderComponent,
        SelectDirective,
        SelectLabelComponent,
        SelectCleanComponent,
        SelectLayoutComponent,
        SelectOptionGroupComponent,
        SelectSearchComponent,
        SelectOptionAsyncComponent,
        SelectOptionDefaultComponent,
        IconCloseComponent,
        IconChevronComponent,
        SelectBadgeComponent,
        AutosuggestComponent,
        ActiveDataItemsComponent,
        SelectOptionFilterComponent
    ],
    exports: [
        SelectComponent,
        MultiSelectComponent,
        SelectHandlerComponent,
        SelectOptionComponent,
        SelectPlaceholderComponent,
        SelectLabelComponent,
        SelectCleanComponent,
        SelectLayoutComponent,
        SelectOptionGroupComponent,
        SelectSearchComponent,
        SelectOptionAsyncComponent,
        SelectOptionDefaultComponent,
        SelectBadgeComponent,
        AutosuggestComponent,
        ActiveDataItemsComponent,
        SelectOptionFilterComponent
    ],
    providers: [
        {
            provide: ARGON_DROPDOWN_LAYOUT_PROVIDER,
            useValue: { type: 'argon-select', component: SelectLayoutComponent } as DropdownLayoutModel,
            multi: true
        }
    ]
})
export class SelectModule { }
