import {
  AfterViewInit,
  Directive,
  Input,
  OnChanges, OnDestroy,
  OnInit,
  SimpleChange,
  SimpleChanges, TrackByFunction
} from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
import { A11yListMixin } from '@argon/a11y-list';
import { BaseShape, Comparators, CompareFn, StateInterface } from '@argon/tools';
import { SelectRegistryMixin } from '../mixins/SelectRegistryMixin';
import { SelectStateInterface } from '../interfaces/SelectStateInterface';
import { SelectDataItemInterface } from '../interfaces/SelectDataItemInterface';

@Directive({
  selector: '[arSelect]'
})
export class SelectDirective
  extends SelectRegistryMixin(A11yListMixin(BaseShape))
  implements ControlValueAccessor, AfterViewInit, OnInit, OnChanges, OnDestroy {

  @Input() compareFn: CompareFn = Comparators.defaultComparator;

  @Input() trackBy: TrackByFunction<SelectDataItemInterface> = (index: number, item: SelectDataItemInterface) => item.value;

  @Input() set value(value: any) {
    if (this.registry) {
      this.registry.setValue(value);
    }
  }

  public get disabled(): boolean { return this.registry.state.disabled; }

  ngOnInit(): void {
    super.ngOnInit();
    const compareFnChange = new SimpleChange(null, this.compareFn, false);
    this.setCompareFn(compareFnChange);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { compareFn } = changes;
    this.setCompareFn(compareFn);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  ngAfterViewInit(): void {
    this.connect(this.registry.optionItems);
  }

  registerOnChange(fn: any): void {
    this.registry.registerOnChange(fn);
  }

  registerOnTouched(fn: any): void {
    this.registry.registerOnTouched(fn);
  }

  setDisabledState(isDisabled: boolean): void {
    this.registry.setDisabled(isDisabled);
  }

  writeValue(obj: any): void {
    this.registry.setValue(obj);
  }

  handleUpdateState(state: StateInterface<SelectStateInterface>): void {
    super.handleUpdateState(state);
    const { isOpened } = state.changes;
    if (isOpened && !isOpened.currentValue) {
      this.keyManager.setActiveItem(-1);
    }
  }

  filterEvent(event: KeyboardEvent): boolean {
    return this.registry.state.isOpened;
  }

  private setCompareFn(compareFn: SimpleChange) {
    if (!compareFn || !this.registry) {
      return;
    }

    this.registry.compareFn = compareFn.currentValue || Comparators.defaultComparator;
  }

}
