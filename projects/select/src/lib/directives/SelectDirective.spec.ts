import { SelectDirective } from './SelectDirective';
import { Component, ElementRef, Injector, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SelectRegistryService } from '../services/SelectRegistryService';
import { AbstractRegistry } from '../services/AbstractRegistry';
import { Comparators, StateInterface } from '@argon/tools';
import { KeyboardModule } from '@argon/keyboard';
import { SelectStateInterface } from '../interfaces/SelectStateInterface';

describe('SelectDirective', () => {

  @Component({ selector: 'spec-component', template: '' })
  class MockComponent extends SelectDirective {
    constructor(injector: Injector) {
      super(injector);
    }
  }

  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;
  let registry: SelectRegistryService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ KeyboardModule ],
    declarations: [MockComponent],
    providers: [
      { provide: AbstractRegistry, useClass: SelectRegistryService }
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(component.compareFn).toEqual(Comparators.defaultComparator);
  });

  it('value setter should call setValue', () => {
    spyOn(registry, 'setValue');
    fixture.detectChanges();
    component.value = 123;
    expect(registry.setValue).toHaveBeenCalledWith(123);
  });

  it('disabled getter should return value from registry', () => {
    fixture.detectChanges();
    registry.setDisabled(true);
    expect(component.disabled).toEqual(true);
    registry.setDisabled(false);
    expect(component.disabled).toEqual(false);
  });

  it('ngOnInit should set default compareFn to registry', () => {
    component.compareFn = null;
    fixture.detectChanges();
    expect(registry.compareFn).toEqual(Comparators.defaultComparator);
  });

  it('ngOnInit should set compareFn to registry', () => {
    component.compareFn = Comparators.deepEqualComparator;
    fixture.detectChanges();
    expect(registry.compareFn).toEqual(Comparators.deepEqualComparator);
  });

  it('ngOnChanges should set default compareFn for null compare fn', () => {
    component.compareFn = Comparators.deepEqualComparator;
    fixture.detectChanges();
    const compareFn = new SimpleChange(Comparators.deepEqualComparator, null, false);
    component.ngOnChanges({ compareFn });
    expect(registry.compareFn).toEqual(Comparators.defaultComparator);
  });

  it('ngOnChanges not update compare fn without registry', () => {
    const compareFn = new SimpleChange(Comparators.deepEqualComparator, Comparators.deepEqualComparator, false);
    component.ngOnChanges({ compareFn });
    expect(registry.compareFn).toEqual(Comparators.defaultComparator);
    fixture.detectChanges();
  });

  it('ngOnChanges should update compareFn', () => {
    component.compareFn = null;
    fixture.detectChanges();
    const compareFn = new SimpleChange(null, Comparators.deepEqualComparator, false);
    component.ngOnChanges({ compareFn });
    expect(registry.compareFn).toEqual(Comparators.deepEqualComparator);
  });

  it('ngAfterViewInit should connect optionItems from registry to directive', () => {
    fixture.detectChanges();
    const option1 = { data: 'Mock option 1', element: new ElementRef(document.createElement('div')) } as any;
    const option2 = { data: 'Mock option 2', element: new ElementRef(document.createElement('div')) } as any;
    registry.registerOption(option1);
    registry.registerOption(option2);
    spyOn(component, 'connect');
    component.ngAfterViewInit();
    expect(registry.optionItems.length).toEqual(2);
    expect(component.connect).toHaveBeenCalledWith(registry.optionItems);
  });

  it('registerOnChange should call registry registerOnChange', () => {
    spyOn(registry, 'registerOnChange');
    fixture.detectChanges();
    const mockFn = () => { };
    component.registerOnChange(mockFn);
    expect(registry.registerOnChange).toHaveBeenCalledWith(mockFn);
  });

  it('registerOnTouched should call registry registerOnTouched', () => {
    spyOn(registry, 'registerOnTouched');
    fixture.detectChanges();
    const mockFn = () => { };
    component.registerOnTouched(mockFn);
    expect(registry.registerOnTouched).toHaveBeenCalledWith(mockFn);
  });

  it('setDisabledState should call registry setDisabled', () => {
    spyOn(registry, 'setDisabled');
    fixture.detectChanges();
    component.setDisabledState(true);
    expect(registry.setDisabled).toHaveBeenCalledWith(true);
  });

  it('writeValue should call registry setValue', () => {
    spyOn(registry, 'setValue');
    fixture.detectChanges();
    component.writeValue(326);
    expect(registry.setValue).toHaveBeenCalledWith(326);
  });

  it('handleUpdateState should clean up keyManager', () => {
    fixture.detectChanges();
    spyOn(component.keyManager, 'setActiveItem');
    const state: StateInterface<SelectStateInterface> = {
      state: {
        disabled: false,
        value: 2,
        query: 'qwe',
        isOpened: true
      },
      changes: {
        isOpened: new SimpleChange(true, false, false)
      }
    };
    component.handleUpdateState(state);
    expect(component.keyManager.setActiveItem).toHaveBeenCalledWith(-1 as never);
  });

  it('handleUpdateState should not clean up keyManager for other changes', () => {
    fixture.detectChanges();
    spyOn(component.keyManager, 'setActiveItem');
    const state: StateInterface<SelectStateInterface> = {
      state: {
        disabled: false,
        value: 2,
        query: 'qwe',
        isOpened: true
      },
      changes: {
        value: new SimpleChange(1, 2, false),
        disabled: new SimpleChange(false, true, false),
        query: new SimpleChange('d2', 'dasd', false),
        isOpened: new SimpleChange(false, true, false),
      }
    };
    component.handleUpdateState(state);
    expect(component.keyManager.setActiveItem).not.toHaveBeenCalled();
  });

  it('filterEvent should returns registry isOpened state', () => {
    fixture.detectChanges();
    registry.setIsOpened(true);
    expect(component.filterEvent(new KeyboardEvent('keydown'))).toEqual(true);
    registry.setIsOpened(false);
    expect(component.filterEvent(new KeyboardEvent('keydown'))).toEqual(false);
  });
});
