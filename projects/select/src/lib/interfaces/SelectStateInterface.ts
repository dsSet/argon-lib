
export interface SelectStateInterface {

  isOpened: boolean;

  value: any;

  disabled: boolean;

  query: string;

  dataItemsChanged?: boolean;

}
