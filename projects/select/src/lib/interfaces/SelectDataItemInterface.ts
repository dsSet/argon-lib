
export interface SelectDataItemInterface {

  value: any;

  label: string;

  data?: any;

  disabled?: boolean;

}
