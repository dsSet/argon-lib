import { ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { StateInterface } from '@argon/tools';
import { AbstractRegistry } from '../services/AbstractRegistry';
import { SelectStateInterface } from './SelectStateInterface';

export interface SelectRegistryExtensionInterface extends OnInit, OnDestroy {

  registry: AbstractRegistry;

  changeDetectorRef: ChangeDetectorRef;

  ngOnInit(): void;

  ngOnDestroy(): void;

  handleUpdateState(state: StateInterface<SelectStateInterface>): void;

}
