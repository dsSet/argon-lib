import { SelectDataItemInterface } from './SelectDataItemInterface';
import { Highlightable } from '@angular/cdk/a11y';
import { ElementRef } from '@angular/core';

export interface SelectOptionInterface extends SelectDataItemInterface, Highlightable {

  element: ElementRef<HTMLElement>;

}
