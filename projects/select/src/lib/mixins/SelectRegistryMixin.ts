import { Constructor, HasInjectorInterface, HasLifecircleHooksInterface, Unsubscribable, StateInterface } from '@argon/tools';
import { ChangeDetectorRef, Injectable, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SelectRegistryExtensionInterface } from '../interfaces/SelectRegistryExtensionInterface';
import { AbstractRegistry } from '../services/AbstractRegistry';
import { SelectStateInterface } from '../interfaces/SelectStateInterface';

export function SelectRegistryMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<SelectRegistryExtensionInterface> {

  @Injectable()
  class SelectRegistryExtension extends BaseClass implements SelectRegistryExtensionInterface, OnInit, OnDestroy {

    public registry: AbstractRegistry;

    public changeDetectorRef: ChangeDetectorRef;

    private changesSubscription: Subscription;

    ngOnInit(): void {
      if (super.ngOnInit) {
        super.ngOnInit();
      }

      this.registry = this.injector.get(AbstractRegistry);
      this.changeDetectorRef = this.injector.get(ChangeDetectorRef);
      this.changesSubscription = this.registry.selectState.subscribe((state: StateInterface<SelectStateInterface>) => {
        this.handleUpdateState(state);
      });
    }

    @Unsubscribable
    ngOnDestroy(): void {
      if (super.ngOnDestroy) {
        super.ngOnDestroy();
      }
    }

    public handleUpdateState(state: StateInterface<SelectStateInterface>) {
      this.changeDetectorRef.markForCheck();
    }
  }

  return SelectRegistryExtension;
}
