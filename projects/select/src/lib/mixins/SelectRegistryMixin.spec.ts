import { SelectRegistryMixin } from './SelectRegistryMixin';
import { BaseShape } from '@argon/tools';
import { ChangeDetectorRef, Component, Injector, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SelectRegistryService } from '../services/SelectRegistryService';
import { AbstractRegistry } from '../services/AbstractRegistry';

describe('SelectRegistryExtension', () => {

  @Component({ selector: 'spec-component', template: '' })
  class MockComponent extends SelectRegistryMixin(BaseShape) {
    constructor(injector: Injector) {
      super(injector);
    }
  }

  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;
  let registry: SelectRegistryService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [MockComponent],
    providers: [
      { provide: AbstractRegistry, useClass: SelectRegistryService }
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(component.registry instanceof AbstractRegistry).toBeTruthy();
    expect(component.changeDetectorRef).toEqual(component.injector.get(ChangeDetectorRef));
  });

  it('selectState changes should call changeDetectorRef.markForCheck', () => {
    fixture.detectChanges();
    spyOn(component.changeDetectorRef, 'markForCheck');
    registry.setIsOpened(true);
    expect(component.changeDetectorRef.markForCheck).toHaveBeenCalled();
  });

});
