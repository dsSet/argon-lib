import filter from 'lodash/filter';
import size from 'lodash/size';
import toLower from 'lodash/toLower';
import { SelectDataSource } from './SelectDataSource';
import { SelectDataItemInterface } from '../interfaces/SelectDataItemInterface';
import { AbstractRegistry } from '../services/AbstractRegistry';
import { BehaviorSubject, Observable } from 'rxjs';
import { SimpleChanges } from '@angular/core';

export class StaticSelectDataSource extends SelectDataSource {

  public optionsSubject: BehaviorSubject<Array<SelectDataItemInterface>>;

  constructor(
    protected options: Array<SelectDataItemInterface>
  ) {
    super();
  }

  public get hasOptions(): boolean {
    return size(this.optionsSubject.value) > 0;
  }

  connect(registry: AbstractRegistry): Observable<Array<SelectDataItemInterface>> {
    if (this.optionsSubject) {
      throw new Error('DataSource already connected!');
    }

    this.optionsSubject = new BehaviorSubject(this.getFilteredOptions(registry.state.query));
    return this.optionsSubject.asObservable();
  }

  disconnect(): void {
    if (this.optionsSubject) {
      this.optionsSubject.complete();
      this.optionsSubject = null;
    }
  }

  onChange(changes: SimpleChanges) {
    if (changes.query) {
      this.optionsSubject.next(this.getFilteredOptions(changes.query.currentValue));
    }
  }

  protected getFilteredOptions(query: string): Array<SelectDataItemInterface> {
    if (!query) {
      return this.options;
    }

    return filter(this.options, (item: SelectDataItemInterface) => toLower(String(item.label)).indexOf(toLower(query)) >= 0);
  }

}
