import { Observable } from 'rxjs';
import { SimpleChanges } from '@angular/core';
import { AbstractRegistry } from '../services/AbstractRegistry';
import { SelectDataItemInterface } from '../interfaces/SelectDataItemInterface';


export abstract class SelectDataSource {

  public abstract connect(registry: AbstractRegistry): Observable<Array<SelectDataItemInterface>>;

  public abstract disconnect(): void;

  public abstract onChange(changes: SimpleChanges);

}

