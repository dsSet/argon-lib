import { StaticSelectDataSource } from './StaticSelectDataSource';
import { SelectDataItemInterface } from '../interfaces/SelectDataItemInterface';
import { BehaviorSubject, Observable } from 'rxjs';
import { SimpleChange } from '@angular/core';

describe('StaticSelectDataSource', () => {

  let dataSource: StaticSelectDataSource;

  const createMockRegistry = (query: string = ''): any => ({
    state: {
      query
    }
  });

  const options: Array<SelectDataItemInterface> = [
    { label: 'Option 1', value: 1 },
    { label: 'Option 2', value: 2 },
    { label: 'Option 3', value: 3 },
    { label: 'Option 4', value: 4 }
  ];

  beforeEach(() => {
    dataSource = new StaticSelectDataSource(options);
  });

  afterEach(() => {
    dataSource.disconnect();
  });

  it('connect should create new subject', () => {
    const output = dataSource.connect(createMockRegistry());
    expect(dataSource.optionsSubject instanceof BehaviorSubject).toBeTruthy();
    expect(output instanceof Observable).toBeTruthy();
    expect(dataSource.optionsSubject.value).toEqual(options);
  });

  it('connect should apply filter', () => {
    dataSource.connect(createMockRegistry('option 1'));
    expect(dataSource.optionsSubject.value).toEqual([options[0]]);
  });

  it('connect should throw error for second connection', () => {
    dataSource.connect(createMockRegistry());
    expect(() => { dataSource.connect(createMockRegistry('2')); })
      .toThrow(new Error('DataSource already connected!'));
  });

  it('disconnect should complete optionsSubject', () => {
    const subject = new BehaviorSubject([]);
    dataSource.optionsSubject = subject;
    spyOn(subject, 'complete');
    dataSource.disconnect();
    expect(subject.complete).toHaveBeenCalled();
    expect(dataSource.optionsSubject).toEqual(null);
  });

  it('should be reusable', () => {
    expect(() => {
      dataSource.connect(createMockRegistry());
      dataSource.disconnect();
      dataSource.connect(createMockRegistry());
    }).not.toThrow();
  });

  it('onChange should update filter for query changes', () => {
    dataSource.connect(createMockRegistry());
    expect(dataSource.optionsSubject.value).toEqual(options);
    const query = new SimpleChange('', '3', false);
    dataSource.onChange({ query });
    expect(dataSource.optionsSubject.value).toEqual([options[2]]);
  });

  it('hasOptions should based on values size', () => {
    dataSource.optionsSubject = new BehaviorSubject([]);
    expect(dataSource.hasOptions).toEqual(false);
    dataSource.optionsSubject.next(options);
    expect(dataSource.hasOptions).toEqual(true);
  });
});
