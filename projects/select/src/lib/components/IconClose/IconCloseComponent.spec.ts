import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { A11yModule } from '@angular/cdk/a11y';
import { ReactiveFormsModule } from '@angular/forms';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { IconCloseComponent } from './IconCloseComponent';

describe('IconCloseComponent', () => {

  let component: IconCloseComponent;
  let fixture: ComponentFixture<IconCloseComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ IconCloseComponent ],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconCloseComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-select-close-icon class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-close-icon');
  });

  it('iconClassName should toggle .ar-select-icon class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'iconClassName', 'ar-select-icon');
  });
});
