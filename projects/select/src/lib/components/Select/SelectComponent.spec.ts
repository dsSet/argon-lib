import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { SelectComponent } from './SelectComponent';
import { DropdownModule } from '@argon/dropdown';
import { PortalModule } from '@angular/cdk/portal';

describe('SelectComponent', () => {

  let component: SelectComponent;
  let fixture: ComponentFixture<SelectComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [DropdownModule, PortalModule],
    declarations: [SelectComponent],
    schemas: [NO_ERRORS_SCHEMA]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-select-single class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-single');
  });

  it('disabled should toggle .ar-select-single--disabled class', () => {
    fixture.detectChanges();
    component.registry.setDisabled(true);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-single--disabled').toBeTruthy();
    component.registry.setDisabled(false);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-single--disabled').toBeFalsy();
  });

  it('should have default values', () => {
    fixture.detectChanges();
    expect(component.dropdown).toBeDefined();
    expect(component.placeholder).toEqual('');
    expect(component.className).toEqual(true);
  });

  it('handleUpdateState should close dropdown if value changes with opened state', () => {
    fixture.detectChanges();
    spyOn(component.dropdown, 'close');
    component.registry.setValue(2);
    expect(component.dropdown.close).not.toHaveBeenCalled();
    component.registry.setIsOpened(true);
    expect(component.dropdown.close).not.toHaveBeenCalled();
    component.registry.setValue(3);
    expect(component.dropdown.close).toHaveBeenCalled();
  });

});
