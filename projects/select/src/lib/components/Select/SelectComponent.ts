import { ChangeDetectionStrategy, Component, forwardRef, HostBinding, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { SelectRegistryService } from '../../services/SelectRegistryService';
import { SelectDirective } from '../../directives/SelectDirective';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { StateInterface } from '@argon/tools';
import { SelectStateInterface } from '../../interfaces/SelectStateInterface';
import { DropdownDirective } from '@argon/dropdown';

@Component({
  selector: 'ar-select',
  templateUrl: './SelectComponent.html',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SelectComponent), multi: true },
    { provide: AbstractRegistry, useClass: SelectRegistryService }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectComponent extends SelectDirective implements OnInit, OnDestroy {

  @Input() placeholder = '';

  @Input() arBsSize: string;

  @ViewChild(DropdownDirective, { static: true }) dropdown: DropdownDirective;

  @HostBinding('class.ar-select-single') className = true;

  @HostBinding('class.ar-select-single--disabled')
  public get disabled(): boolean { return super.disabled; }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  handleUpdateState(state: StateInterface<SelectStateInterface>): void {
    if (state.changes.value && state.state.isOpened) {
      this.dropdown.close();
    }
    super.handleUpdateState(state);
  }

}
