import {
  ChangeDetectionStrategy,
  Component,
  ElementRef, EventEmitter,
  HostBinding,
  HostListener,
  Injector,
  Input, OnChanges,
  OnDestroy, OnInit, Output, SimpleChanges
} from '@angular/core';
import { A11yListItem } from '@argon/a11y-list';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { SelectOptionInterface } from '../../interfaces/SelectOptionInterface';
import { UIClickableInterface } from '@argon/tools';

@Component({
  selector: 'ar-select-option',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectOptionComponent
  extends SelectRegistryMixin(A11yListItem)
  implements SelectOptionInterface, OnDestroy, OnInit, UIClickableInterface, OnChanges {

  @HostBinding('class.ar-select-option') className = true;

  @HostBinding('class.ar-select-option--disabled') @Input() disabled: boolean;

  @HostBinding('class.ar-select-option--selected') get selected(): boolean { return this.registry.isOptionSelected(this.value); }

  @HostBinding('class.ar-select-option--active') active: boolean;

  @HostBinding('attr.role') role = 'listitem';

  @Input() value: any;

  @Input() data: any;

  @Input() label: any;

  @Output() public click = new EventEmitter<void>();

  constructor(
    public element: ElementRef<HTMLElement>,
    injector: Injector,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.registry.registerOption(this);
    this.registry.registerDataItem(this);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { value, data, label } = changes;
    const change = value || data || label;
    if (!change || change.isFirstChange()) {
      return;
    }

    this.registry.fireDataItemChanges();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.registry.unregisterOption(this);
    this.registry.unregisterDataItem(this);
  }

  @HostListener('click')
  public handleClick() {
    if (!this.disabled) {
      this.registry.handleOptionToggle(this.value);
    }
  }

  handleStateChange(): void {
    if (this.active) {
      this.element.nativeElement.scrollIntoView({ block: 'center', behavior: 'smooth' });
    }
    this.changeDetectorRef.markForCheck();
  }

  getLabel(): string {
    return this.label || String(this.value);
  }

  setActiveStyles(): void {
  }

  setInactiveStyles(): void {
  }
}
