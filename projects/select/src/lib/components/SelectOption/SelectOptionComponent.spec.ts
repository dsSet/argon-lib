import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { SelectOptionComponent } from './SelectOptionComponent';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectRegistryService } from '../../services/SelectRegistryService';
import { SimpleChange } from '@angular/core';

describe('SelectOptionComponent', () => {

  let component: SelectOptionComponent;
  let fixture: ComponentFixture<SelectOptionComponent>;
  let registry: AbstractRegistry;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [ SelectOptionComponent ],
    providers: [
      { provide: AbstractRegistry, useClass: SelectRegistryService }
    ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectOptionComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
  });

  it('className should toggle .ar-select-option class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-option');
  });

  it('disabled should toggle .ar-select-option--disabled class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'disabled', 'ar-select-option--disabled');
  });

  it('active should toggle .ar-select-option--active class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'active', 'ar-select-option--active');
  });

  it('should have listitem role', () => {
    HostBindingTestHelper.testRoleAttribute(component, fixture, 'listitem');
  });

  it('selected should toggle .ar-select-option--selected class', () => {
    fixture.detectChanges();
    const spy = spyOn(registry, 'isOptionSelected').and.returnValue(true);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-option--selected').toBeTruthy();
    spy.calls.reset();
    spy.and.returnValue(false);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-option--selected').toBeFalsy();
  });

  it('should call registerOption and registerDataItem onInit', () => {
    spyOn(registry, 'registerOption');
    spyOn(registry, 'registerDataItem');
    component.ngOnInit();
    expect(registry.registerOption).toHaveBeenCalledWith(component);
    expect(registry.registerDataItem).toHaveBeenCalledWith(component);
  });

  it('should call unregisterOption and unregisterDataItem onInit', () => {
    spyOn(registry, 'unregisterOption');
    spyOn(registry, 'unregisterDataItem');
    component.ngOnInit();
    component.ngOnDestroy();
    expect(registry.unregisterOption).toHaveBeenCalledWith(component);
    expect(registry.unregisterDataItem).toHaveBeenCalledWith(component);
  });

  it('click event should call handleOptionToggle with value', () => {
    component.disabled = false;
    component.value = 12345;
    spyOn(registry, 'handleOptionToggle');
    fixture.detectChanges();
    fixture.elementRef.nativeElement.click();
    expect(registry.handleOptionToggle).toHaveBeenCalledWith(component.value);
  });

  it('click event should not call handleOptionToggle if disabled', () => {
    component.disabled = true;
    component.value = 12345;
    spyOn(registry, 'handleOptionToggle');
    fixture.detectChanges();
    fixture.elementRef.nativeElement.click();
    expect(registry.handleOptionToggle).not.toHaveBeenCalled();
  });

  it('handleStateChange should call scrollIntoView if active', () => {
    fixture.detectChanges();
    spyOn(component.element.nativeElement, 'scrollIntoView');
    component.active = true;
    component.handleStateChange();
    expect(component.element.nativeElement.scrollIntoView).toHaveBeenCalledWith({ block: 'center', behavior: 'smooth' });
  });

  it('handleStateChange should not call scrollIntoView if not active', () => {
    fixture.detectChanges();
    spyOn(component.element.nativeElement, 'scrollIntoView');
    component.active = false;
    component.handleStateChange();
    expect(component.element.nativeElement.scrollIntoView).not.toHaveBeenCalled();
  });

  it('getLabel should return label if exist', () => {
    fixture.detectChanges();
    component.label = 'Mock Label';
    component.value = 563;
    expect(component.getLabel()).toEqual(component.label);
  });

  it('getLabel should return value as string if label soes not exist', () => {
    fixture.detectChanges();
    component.label = null;
    component.value = 563;
    expect(component.getLabel()).toEqual(String(component.value));
  });

  it('ngOnChanges should call fireDataItemChanges for value updates', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    const value = new SimpleChange(1, 2, false);
    component.ngOnChanges({ value });
    expect(component.registry.fireDataItemChanges).toHaveBeenCalled();
  });

  it('ngOnChanges should call fireDataItemChanges for data updates', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    const data = new SimpleChange(1, 2, false);
    component.ngOnChanges({ data });
    expect(component.registry.fireDataItemChanges).toHaveBeenCalled();
  });

  it('ngOnChanges should call fireDataItemChanges for label updates', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    const label = new SimpleChange(1, 2, false);
    component.ngOnChanges({ label });
    expect(component.registry.fireDataItemChanges).toHaveBeenCalled();
  });

  it('ngOnChanges should not call fireDataItemChanges for first update', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    const label = new SimpleChange(1, 2, true);
    component.ngOnChanges({ label });
    expect(component.registry.fireDataItemChanges).not.toHaveBeenCalled();
  });

  it('ngOnChanges should not call fireDataItemChanges without updates', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    component.ngOnChanges({ });
    expect(component.registry.fireDataItemChanges).not.toHaveBeenCalled();
  });
});
