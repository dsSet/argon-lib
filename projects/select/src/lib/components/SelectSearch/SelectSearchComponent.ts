import { Component, ElementRef, HostBinding, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { BaseShape, StateInterface, Unsubscribable } from '@argon/tools';
import { UntypedFormControl } from '@angular/forms';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { SelectStateInterface } from '../../interfaces/SelectStateInterface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ar-select-search',
  template: `
    <input
      #input
      class="ar-select-search__input"
      [placeholder]="placeholder"
      [formControl]="searchControl"
    />
    <div class="ar-select-search__icon" (click)="cleanSearch()">
      <ar-close-icon class="ar-select-search__close"></ar-close-icon>
    </div>
  `
})
export class SelectSearchComponent extends SelectRegistryMixin(BaseShape) implements OnInit, OnDestroy {

  @ViewChild('input', { static: true, read: ElementRef }) input: ElementRef<HTMLInputElement>;

  @HostBinding('class.ar-select-search') className = true;

  @HostBinding('class.ar-select-search--active') get active(): boolean { return Boolean(this.searchControl.value); }

  @Input() placeholder = '';

  public searchControl: UntypedFormControl;

  private searchSubscription: Subscription;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.searchControl = new UntypedFormControl(this.registry.state.query);
    this.searchSubscription = this.searchControl.valueChanges.pipe(
      debounceTime(150),
      distinctUntilChanged()
    ).subscribe(this.handleQuery);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public focus() {
    if (this.searchControl) {
      this.searchControl.markAsTouched();
    }

    this.input.nativeElement.focus();
  }

  public cleanSearch() {
    this.searchControl.setValue('');
    this.focus();
  }

  handleUpdateState(state: StateInterface<SelectStateInterface>): void {
    const { changes, state: { isOpened } } = state;
    if (changes.isOpened && isOpened) {
      this.focus();
    }
    super.handleUpdateState(state);
  }

  private handleQuery = (query: string) => {
    this.registry.setQuery(query);
  }

}
