import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { A11yModule } from '@angular/cdk/a11y';
import { ReactiveFormsModule } from '@angular/forms';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { SelectSearchComponent } from './SelectSearchComponent';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectRegistryService } from '../../services/SelectRegistryService';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('SelectSearchComponent', () => {

  let component: SelectSearchComponent;
  let fixture: ComponentFixture<SelectSearchComponent>;
  let registry: SelectRegistryService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ SelectSearchComponent ],
    providers: [
      { provide: AbstractRegistry, useClass: SelectRegistryService }
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSearchComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
    fixture.detectChanges();
  });

  it('className should toggle .ar-select-search class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-search');
  });

  it('control value should toggle .ar-select-search--active class', () => {
    component.searchControl.setValue(null);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-search--active').toBeFalsy();
    component.searchControl.setValue('123');
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-search--active').toBeTruthy();
  });

  it('should have defaults', () => {
    expect(component.placeholder).toEqual('');
    expect(component.className).toEqual(true);
    expect(component.searchControl).toBeDefined();
  });

  it('search control changes should call setQuery with 0.15 sec debounce', fakeAsync(() => {
    spyOn(registry, 'setQuery');
    component.searchControl.setValue('qwe123');
    tick(50);
    expect(registry.setQuery).not.toHaveBeenCalled();
    component.searchControl.setValue('888');
    tick(100);
    expect(registry.setQuery).not.toHaveBeenCalled();
    component.searchControl.setValue('qqq');
    tick(150);
    expect(registry.setQuery).toHaveBeenCalledWith('qqq');
  }));

  it('focus should mark search control as touched', () => {
    expect(component.searchControl.touched).toBeFalsy();
    component.focus();
    expect(component.searchControl.touched).toBeTruthy();
  });

  it('focus should set input focus', () => {
    spyOn(component.input.nativeElement, 'focus');
    component.focus();
    expect(component.input.nativeElement.focus).toHaveBeenCalled();
  });

  it('cleanSearch should clean up search control', () => {
    component.searchControl.setValue('test');
    component.cleanSearch();
    expect(component.searchControl.value).toEqual('');
  });

  it('cleanSearch should call focus', () => {
    spyOn(component, 'focus');
    component.cleanSearch();
    expect(component.focus).toHaveBeenCalled();
  });

  it('should call focus on isOpened set to true', () => {
    const spy = spyOn(component, 'focus');
    registry.setIsOpened(true);
    expect(component.focus).toHaveBeenCalled();

    spy.calls.reset();
    registry.setIsOpened(true);
    expect(component.focus).not.toHaveBeenCalled();

    spy.calls.reset();
    registry.setValue(5);
    expect(component.focus).not.toHaveBeenCalled();
  });

});
