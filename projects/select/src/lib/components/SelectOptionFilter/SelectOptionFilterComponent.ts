import toLower from 'lodash/toLower';
import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  TrackByFunction,
  ViewChild
} from '@angular/core';
import { BaseShape } from '@argon/tools';
import { SelectDataItemInterface } from '../../interfaces/SelectDataItemInterface';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';

@Component({
  selector: 'ar-select-option-filter',
  template: `
    <ar-select-option
      *ngFor="let item of options; trackBy: trackBy"
      [value]="item.value"
      [label]="item.label"
      [data]="item.data"
      [disabled]="item.disabled"
      [hidden]="isOptionHidden(item)"
    >
      <ng-container *ngTemplateOutlet="optionTemplate; context: item"></ng-container>
    </ar-select-option>

    <ng-template #defaultTemplate let-label="label">{{ label }}</ng-template>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectOptionFilterComponent extends SelectRegistryMixin(BaseShape) implements OnDestroy, OnInit {

  @Input() options: Array<SelectDataItemInterface> = [];

  @Input() trackBy: TrackByFunction<SelectDataItemInterface>;

  @ContentChild(TemplateRef, { static: true }) template: TemplateRef<SelectDataItemInterface>;

  @ViewChild('defaultTemplate', { static: true, read: TemplateRef }) defaultTemplate: TemplateRef<SelectDataItemInterface>;

  public get optionTemplate(): TemplateRef<SelectDataItemInterface> {
    return this.template || this.defaultTemplate;
  }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public isOptionHidden(option: SelectDataItemInterface): boolean {
    const query = this.registry.state.query;
    if (!query) {
      return false;
    }

    return toLower(String(option.label)).indexOf(toLower(query)) < 0;
  }
}
