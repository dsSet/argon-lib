import { BaseShape } from '@argon/tools';
import { ChangeDetectionStrategy, Component, HostBinding, HostListener, Injector, Input, OnDestroy, OnInit } from '@angular/core';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { SelectDataItemInterface } from '../../interfaces/SelectDataItemInterface';

@Component({
  selector: 'ar-select-badge',
  template: `
    <span class="ar-select-badge__text" [arTextCut]="data?.label">{{ data?.label }}</span>
    <ar-close-icon *ngIf="!disabled" class="ar-select-badge__icon" (click)="handleRemove($event)"></ar-close-icon>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectBadgeComponent extends SelectRegistryMixin(BaseShape) implements OnInit, OnDestroy {

  @Input() data: SelectDataItemInterface;

  @HostBinding('class.ar-select-badge') className = true;

  @HostBinding('class.ar-select-badge--disabled') get disabled() { return this.registry.state.disabled; }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  @HostListener('mousedown', ['$event'])
  @HostListener('click', ['$event'])
  public handleClick(event: Event) {
    event.stopPropagation();
    return false;
  }

  public handleRemove(event: Event) {
    if (this.registry.state.disabled || !this.data) {
      return;
    }
    event.stopPropagation();
    this.registry.handleOptionToggle(this.data.value);
  }

}
