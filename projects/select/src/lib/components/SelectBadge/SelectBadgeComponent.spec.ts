import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectMultipleRegistryService } from '../../services/SelectMultipleRegistryService';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import createSpyObj = jasmine.createSpyObj;
import { SelectBadgeComponent } from './SelectBadgeComponent';

describe('SelectBadgeComponent', () => {

  let component: SelectBadgeComponent;
  let fixture: ComponentFixture<SelectBadgeComponent>;
  let registry: AbstractRegistry;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [ SelectBadgeComponent ],
    providers: [
      { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBadgeComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
  });

  it('className should toggle .ar-select-badge class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-badge');
  });

  it('disabled should toggle .ar-select-badge--disabled class', () => {
    fixture.detectChanges();
    registry.setDisabled(true);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-badge--disabled').toBeTruthy();
    registry.setDisabled(false);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-badge--disabled').toBeFalsy();
  });

  it('handleClick should call event.stopPropagation', () => {
    fixture.detectChanges();
    const event = createSpyObj(['stopPropagation']);
    const result = component.handleClick(event);
    expect(event.stopPropagation).toHaveBeenCalled();
    expect(result).toEqual(false);
  });

  it('handleRemove should call handleOptionToggle with data.value', () => {
    fixture.detectChanges();
    registry.setDisabled(false);
    component.data = {
      label: 'Mock',
      value: 321
    };
    spyOn(registry, 'handleOptionToggle');
    const event = createSpyObj(['stopPropagation']);
    component.handleRemove(event);
    expect(registry.handleOptionToggle).toHaveBeenCalledWith(component.data.value);
    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('handleRemove should not call handleOptionToggle if disabled', () => {
    fixture.detectChanges();
    registry.setDisabled(true);
    component.data = {
      label: 'Mock',
      value: 321
    };
    spyOn(registry, 'handleOptionToggle');
    const event = createSpyObj(['stopPropagation']);
    component.handleRemove(event);
    expect(registry.handleOptionToggle).not.toHaveBeenCalled();
    expect(event.stopPropagation).not.toHaveBeenCalled();
  });

  it('handleRemove should not call handleOptionToggle has no data', () => {
    fixture.detectChanges();
    registry.setDisabled(false);
    component.data = null;
    spyOn(registry, 'handleOptionToggle');
    const event = createSpyObj(['stopPropagation']);
    component.handleRemove(event);
    expect(registry.handleOptionToggle).not.toHaveBeenCalled();
    expect(event.stopPropagation).not.toHaveBeenCalled();
  });

});
