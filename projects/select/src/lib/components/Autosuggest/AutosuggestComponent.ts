import { SelectDirective } from '../../directives/SelectDirective';
import { ChangeDetectionStrategy, Component, forwardRef, HostBinding, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DropdownDirective } from '@argon/dropdown';
import { StateInterface } from '@argon/tools';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectRegistryService } from '../../services/SelectRegistryService';
import { SelectStateInterface } from '../../interfaces/SelectStateInterface';

@Component({
  selector: 'ar-autosuggest',
  templateUrl: './AutosuggestComponent.html',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => AutosuggestComponent), multi: true },
    { provide: AbstractRegistry, useClass: SelectRegistryService }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AutosuggestComponent extends SelectDirective implements OnInit, OnDestroy {

  @Input() placeholder = '';

  @Input() arBsSize: string;

  @ViewChild(DropdownDirective, { static: true }) dropdown: DropdownDirective;

  @HostBinding('class.ar-autosuggest') className = true;

  @HostBinding('class.ar-autosuggest--disabled')
  public get disabled(): boolean { return super.disabled; }

  public get hasPlaceholder(): boolean {
    return !this.registry.hasValue && !this.registry.state.isOpened;
  }

  public get hasLabel(): boolean {
    return this.registry.hasValue && !this.registry.state.isOpened;
  }

  public get hasSearch(): boolean {
    return this.registry.state.isOpened;
  }

  public get hasRemove(): boolean {
    return this.registry.hasValue && !this.registry.state.disabled && !this.registry.state.isOpened;
  }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  handleUpdateState(state: StateInterface<SelectStateInterface>): void {
    if (state.changes.value && state.state.isOpened) {
      this.dropdown.close();
    }
    super.handleUpdateState(state);
  }

}
