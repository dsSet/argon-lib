import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { AutosuggestComponent } from './AutosuggestComponent';
import { DropdownModule } from '@argon/dropdown';
import { PortalModule } from '@angular/cdk/portal';

describe('AutosuggestComponent', () => {

  let component: AutosuggestComponent;
  let fixture: ComponentFixture<AutosuggestComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [DropdownModule, PortalModule],
    declarations: [AutosuggestComponent],
    schemas: [NO_ERRORS_SCHEMA]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutosuggestComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-autosuggest class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-autosuggest');
  });

  it('disabled should toggle .ar-autosuggest--disabled class', () => {
    fixture.detectChanges();
    component.registry.setDisabled(true);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-autosuggest--disabled').toBeTruthy();
    component.registry.setDisabled(false);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-autosuggest--disabled').toBeFalsy();
  });

  it('should have default values', () => {
    fixture.detectChanges();
    expect(component.dropdown).toBeDefined();
    expect(component.placeholder).toEqual('');
    expect(component.className).toEqual(true);
  });

  it('handleUpdateState should close dropdown if value changes with opened state', () => {
    fixture.detectChanges();
    spyOn(component.dropdown, 'close');
    component.registry.setValue(2);
    expect(component.dropdown.close).not.toHaveBeenCalled();
    component.registry.setIsOpened(true);
    expect(component.dropdown.close).not.toHaveBeenCalled();
    component.registry.setValue(3);
    expect(component.dropdown.close).toHaveBeenCalled();
  });

  it('should show placeholder if has no value and closed', () => {
    fixture.detectChanges();
    component.registry.setIsOpened(false);
    component.registry.setValue(null);
    expect(component.hasPlaceholder).toBeTruthy();
    component.registry.setIsOpened(true);
    component.registry.setValue(null);
    expect(component.hasPlaceholder).toBeFalsy();
    component.registry.setIsOpened(false);
    component.registry.setValue(1);
    expect(component.hasPlaceholder).toBeFalsy();
  });

  it('should display label if has value and closed', () => {
    fixture.detectChanges();
    component.registry.setIsOpened(false);
    component.registry.setValue(1);
    expect(component.hasLabel).toBeTruthy();
    component.registry.setIsOpened(true);
    component.registry.setValue(1);
    expect(component.hasLabel).toBeFalsy();
    component.registry.setIsOpened(false);
    component.registry.setValue(null);
    expect(component.hasLabel).toBeFalsy();
  });

  it('should display search if opened', () => {
    fixture.detectChanges();
    component.registry.setIsOpened(false);
    expect(component.hasSearch).toBeFalsy();
    component.registry.setIsOpened(true);
    expect(component.hasSearch).toBeTruthy();
  });

  it('should have remove button if has value not disabled and closed', () => {
    fixture.detectChanges();
    component.registry.setValue(1);
    component.registry.setIsOpened(false);
    component.registry.setDisabled(false);
    expect(component.hasRemove).toBeTruthy();

    component.registry.setValue(null);
    component.registry.setIsOpened(false);
    component.registry.setDisabled(false);
    expect(component.hasRemove).toBeFalsy();

    component.registry.setValue(1);
    component.registry.setIsOpened(true);
    component.registry.setDisabled(false);
    expect(component.hasRemove).toBeFalsy();

    component.registry.setValue(1);
    component.registry.setIsOpened(false);
    component.registry.setDisabled(true);
    expect(component.hasRemove).toBeFalsy();
  });

});
