import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { DropdownModule } from '@argon/dropdown';
import { PortalModule } from '@angular/cdk/portal';
import { MultiSelectComponent } from './MultiSelectComponent';

describe('MultiSelectComponent', () => {

  let component: MultiSelectComponent;
  let fixture: ComponentFixture<MultiSelectComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [DropdownModule, PortalModule],
    declarations: [MultiSelectComponent],
    schemas: [NO_ERRORS_SCHEMA]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultiSelectComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-select-multiple class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-multiple');
  });

  it('disabled should toggle .ar-select-multiple--disabled class', () => {
    fixture.detectChanges();
    component.registry.setDisabled(true);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-multiple--disabled').toBeTruthy();
    component.registry.setDisabled(false);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-multiple--disabled').toBeFalsy();
  });

  it('should have default values', () => {
    fixture.detectChanges();
    expect(component.placeholder).toEqual('');
    expect(component.className).toEqual(true);
  });

});
