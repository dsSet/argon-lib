import { ChangeDetectionStrategy, Component, forwardRef, HostBinding, Injector, Input, OnDestroy, OnInit } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { SelectDirective } from '../../directives/SelectDirective';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectMultipleRegistryService } from '../../services/SelectMultipleRegistryService';

@Component({
  selector: 'ar-select-multiple',
  templateUrl: './MultiSelectComponent.html',
  providers: [
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MultiSelectComponent), multi: true },
    { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiSelectComponent extends SelectDirective implements OnInit, OnDestroy {

  @HostBinding('class.ar-select-multiple') className = true;

  @HostBinding('class.ar-select-multiple--disabled')
  public get disabled(): boolean { return super.disabled; }

  @Input() placeholder = '';

  @Input() arBsSize: string;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
