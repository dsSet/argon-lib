import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { SelectLabelComponent } from './SelectLabelComponent';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectDataItemInterface } from '../../interfaces/SelectDataItemInterface';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { SelectStateInterface } from '../../interfaces/SelectStateInterface';
import { StateInterface } from '@argon/tools';
import { SelectMultipleRegistryService } from '../../services/SelectMultipleRegistryService';

describe('SelectLabelComponent', () => {

  let component: SelectLabelComponent;
  let fixture: ComponentFixture<SelectLabelComponent>;
  let registry: AbstractRegistry;

  const createDataItem = (label, value): SelectDataItemInterface => ({
    value,
    label
  });

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [ SelectLabelComponent ],
    providers: [
      { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectLabelComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
  });

  it('className should toggle .ar-select-label class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-label');
  });

  it('should fill displayText with dataItems labels on init', () => {
    fixture.detectChanges();
    registry.setValue([1, 2]);
    registry.registerDataItem(createDataItem('item1', 1));
    registry.registerDataItem(createDataItem('item2', 2));
    component.ngOnInit();
    expect(component.displayText).toEqual(['item1', 'item2']);
  });

  it('should fill displayText with dataItems labels on handleUpdateState if has value changes', () => {
    const state: StateInterface<SelectStateInterface> = {
      state: {
        disabled: false,
        value: [],
        query: '',
        isOpened: false
      },
      changes: {
        value: new SimpleChange([], ['1'], false)
      }
    };
    fixture.detectChanges();
    registry.setValue([1, 2]);
    registry.registerDataItem(createDataItem('item1', 1));
    registry.registerDataItem(createDataItem('item2', 2));
    component.displayText = [];
    component.handleUpdateState(state);
    expect(component.displayText).toEqual(['item1', 'item2']);
  });

  it('should fill displayText with dataItems labels on handleUpdateState if dataItemsChanged changes', () => {
    const state: StateInterface<SelectStateInterface> = {
      state: {
        disabled: false,
        value: [],
        query: '',
        isOpened: false
      },
      changes: {
        dataItemsChanged: new SimpleChange(false, true, false)
      }
    };
    fixture.detectChanges();
    registry.setValue([1, 2]);
    registry.registerDataItem(createDataItem('item1', 1));
    registry.registerDataItem(createDataItem('item2', 2));
    component.displayText = [];
    component.handleUpdateState(state);
    expect(component.displayText).toEqual(['item1', 'item2']);
  });

  it('should not fill displayText with dataItems labels on handleUpdateState if has no value changes', () => {
    const state: StateInterface<SelectStateInterface> = {
      state: {
        disabled: false,
        value: [],
        query: '',
        isOpened: false
      },
      changes: { }
    };
    fixture.detectChanges();
    registry.setValue([1, 2]);
    registry.registerDataItem(createDataItem('item1', 1));
    registry.registerDataItem(createDataItem('item2', 2));
    component.displayText = [];
    component.handleUpdateState(state);
    expect(component.displayText).toEqual([]);
  });

});
