import map from 'lodash/map';
import { ChangeDetectionStrategy, Component, HostBinding, Injector, OnDestroy, OnInit } from '@angular/core';
import { BaseShape, StateInterface } from '@argon/tools';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { SelectStateInterface } from '../../interfaces/SelectStateInterface';
import { SelectDataItemInterface } from '../../interfaces/SelectDataItemInterface';

@Component({
  selector: 'ar-select-label',
  template: `
    <span [arTextCut]="title" class="ar-select-label__text">{{ displayText }}</span>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectLabelComponent extends SelectRegistryMixin(BaseShape) implements OnInit, OnDestroy {

  @HostBinding('class.ar-select-label') className = true;

  public displayText: Array<string> = [];

  public get title(): string { return String(this.displayText); }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.displayText = this.getTextToDisplay();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public handleUpdateState(nextState: StateInterface<SelectStateInterface>): void {
    const { changes } = nextState;
    if (changes.value || changes.dataItemsChanged) {
      this.displayText = this.getTextToDisplay();
    }

    super.handleUpdateState(nextState);
  }

  private getTextToDisplay(): Array<string> {
    return map(this.registry.activeDataItems, (item: SelectDataItemInterface) => item.label);
  }
}
