import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { SelectPlaceholderComponent } from './SelectPlaceholderComponent';

describe('SelectPlaceholderComponent', () => {

  let component: SelectPlaceholderComponent;
  let fixture: ComponentFixture<SelectPlaceholderComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [ SelectPlaceholderComponent ],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPlaceholderComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-select-placeholder class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-placeholder');
  });

});
