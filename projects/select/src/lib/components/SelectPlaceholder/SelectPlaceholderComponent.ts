import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ar-select-placeholder',
  template: '<ng-content></ng-content>&nbsp;',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectPlaceholderComponent {

  @HostBinding('class.ar-select-placeholder') className = true;

}
