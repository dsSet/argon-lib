import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ar-icon-chevron',
  template: `
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="ar-select-icon__content">
      <path d="M15.997 13.374l-7.081 7.081L7 18.54l8.997-8.998 9.003 9-1.916 1.916z"></path>
    </svg>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconChevronComponent {

  @HostBinding('class.ar-select-chevron-icon') className = true;

  @HostBinding('class.ar-select-icon') iconClassName = true;

}
