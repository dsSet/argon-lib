import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { A11yModule } from '@angular/cdk/a11y';
import { ReactiveFormsModule } from '@angular/forms';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { IconChevronComponent } from './IconChevronComponent';

describe('IconChevronComponent', () => {

  let component: IconChevronComponent;
  let fixture: ComponentFixture<IconChevronComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ IconChevronComponent ],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconChevronComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-select-chevron-icon class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-chevron-icon');
  });

  it('iconClassName should toggle .ar-select-icon class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'iconClassName', 'ar-select-icon');
  });
});
