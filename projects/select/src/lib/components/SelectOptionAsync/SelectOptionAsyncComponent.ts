import {
  ChangeDetectionStrategy,
  Component, ContentChild, EventEmitter, Injector,
  Input,
  OnChanges,
  OnDestroy, OnInit, Output,
  SimpleChanges,
  TemplateRef, TrackByFunction,
  ViewChild
} from '@angular/core';
import { Subscription } from 'rxjs';
import { BaseShape, Unsubscribable, StateInterface } from '@argon/tools';
import { SelectDataSource } from '../../models/SelectDataSource';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { SelectDataItemInterface } from '../../interfaces/SelectDataItemInterface';
import { SelectStateInterface } from '../../interfaces/SelectStateInterface';

@Component({
  selector: 'ar-select-option-async',
  templateUrl: './SelectOptionAsyncComponent.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectOptionAsyncComponent extends SelectRegistryMixin(BaseShape) implements OnDestroy, OnChanges, OnInit {

  @Input() dataSource: SelectDataSource;

  @Input() trackBy: TrackByFunction<SelectDataItemInterface>;

  @Output() change = new EventEmitter<Array<SelectDataItemInterface>>();

  @ContentChild(TemplateRef, { static: true }) template: TemplateRef<SelectDataItemInterface>;

  @ViewChild('defaultTemplate', { static: true, read: TemplateRef }) defaultTemplate: TemplateRef<SelectDataItemInterface>;

  public get optionTemplate(): TemplateRef<SelectDataItemInterface> {
    return this.template || this.defaultTemplate;
  }

  public options: Array<SelectDataItemInterface>;

  private dataSourceSubscription: Subscription;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.connectDataSource(this.dataSource);
  }

  ngOnChanges({ dataSource }: SimpleChanges): void {
    if (!dataSource || dataSource.isFirstChange()) {
      return;
    }

    if (this.dataSourceSubscription) {
      this.dataSourceSubscription.unsubscribe();
      this.dataSourceSubscription = null;
    }

    const currentValue: SelectDataSource = dataSource.currentValue;
    const previousValue: SelectDataSource = dataSource.previousValue;

    if (previousValue) {
      previousValue.disconnect();
    }

    this.connectDataSource(currentValue);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
    if (this.dataSource) {
      this.dataSource.disconnect();
    }
  }

  handleUpdateState(nextState: StateInterface<SelectStateInterface>): void {
    const { changes } = nextState;
    if (this.dataSource) {
      this.dataSource.onChange(changes);
    }
    super.handleUpdateState(nextState);
  }

  private handleOptionsChange = (options: Array<SelectDataItemInterface>) => {
    this.options = options;
    this.change.emit(this.options);
    this.changeDetectorRef.markForCheck();
  }

  private connectDataSource(dataSource: SelectDataSource) {
    if (dataSource) {
      this.dataSourceSubscription = dataSource.connect(this.registry).subscribe(this.handleOptionsChange);
    }
  }
}
