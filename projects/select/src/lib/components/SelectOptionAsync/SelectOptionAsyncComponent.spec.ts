import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SelectOptionAsyncComponent } from './SelectOptionAsyncComponent';
import { SelectRegistryService } from '../../services/SelectRegistryService';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { MockDataSource } from '../../../test/MockDataSource';
import { SelectStateInterface } from '../../interfaces/SelectStateInterface';
import { StateInterface } from '@argon/tools';
import { SelectDataItemInterface } from '../../interfaces/SelectDataItemInterface';


describe('SelectOptionAsyncComponent', () => {

  let component: SelectOptionAsyncComponent;
  let fixture: ComponentFixture<SelectOptionAsyncComponent>;
  let registry: SelectRegistryService;
  let dataSource: MockDataSource;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [SelectOptionAsyncComponent],
    providers: [
      { provide: AbstractRegistry, useClass: SelectRegistryService }
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectOptionAsyncComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
    dataSource = new MockDataSource();
  });

  afterEach(() => {
    dataSource.destroy();
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(component.defaultTemplate).toBeDefined();
    expect(component.change).toBeDefined();
  });

  it('optionTemplate should return defaultTemplate if template not defined', () => {
    fixture.detectChanges();
    expect(component.optionTemplate).toEqual(component.defaultTemplate);
  });

  it('optionTemplate should return template', () => {
    fixture.detectChanges();
    component.template = { data: 'Mock Template' } as any;
    expect(component.optionTemplate).toEqual(component.template);
  });

  it('ngOnInit should connect dataSource', () => {
    component.dataSource = dataSource;
    fixture.detectChanges();
    expect(dataSource.connect).toHaveBeenCalledWith(registry);
  });

  it('ngOnChanges should disconnect prev data source', () => {
    fixture.detectChanges();
    const ds1 = new MockDataSource();
    const ds2 = new MockDataSource();
    component.dataSource = ds1;
    const dataSourceChange = new SimpleChange(ds1, ds2, false);
    component.ngOnChanges({ dataSource: dataSourceChange });
    expect(ds1.disconnect).toHaveBeenCalled();
    expect(ds2.connect).toHaveBeenCalledWith(registry);
    ds1.destroy();
    ds2.destroy();
  });

  it('ngOnChanges should not works with data source on first change', () => {
    fixture.detectChanges();
    const ds1 = new MockDataSource();
    const ds2 = new MockDataSource();
    component.dataSource = ds1;
    const dataSourceChange = new SimpleChange(ds1, ds2, true);
    component.ngOnChanges({ dataSource: dataSourceChange });
    expect(ds1.disconnect).not.toHaveBeenCalled();
    expect(ds2.connect).not.toHaveBeenCalled();
    ds1.destroy();
    ds2.destroy();
  });

  it('ngOnChanges should unsubscribe from initial data source', () => {
    const ds1 = new MockDataSource();
    component.dataSource = ds1;
    fixture.detectChanges();
    expect(ds1.connectSubject.observers.length).toEqual(1);
    const ds2 = new MockDataSource();
    const dataSourceChange = new SimpleChange(ds1, ds2, false);
    component.ngOnChanges({ dataSource: dataSourceChange });
    expect(ds1.connectSubject.observers.length).toEqual(0);
    ds1.destroy();
    ds2.destroy();
  });

  it('ngOnDestroy should call data source disconnect', () => {
    component.dataSource = dataSource;
    fixture.detectChanges();
    component.ngOnDestroy();
    expect(dataSource.disconnect).toHaveBeenCalled();
  });

  it('handleUpdateState should call data source onChange with changes', () => {
    component.dataSource = dataSource;
    fixture.detectChanges();
    const state: StateInterface<SelectStateInterface> = {
      state: {
        isOpened: false,
        query: '',
        value: null,
        disabled: false
      },
      changes: {
        query: new SimpleChange('', 'Mock Query', false)
      }
    };
    component.handleUpdateState(state);
    expect(dataSource.onChange).toHaveBeenCalledWith(state.changes);
  });

  it('data source connection should update data source options', () => {
    const options: Array<SelectDataItemInterface> = [
      { value: 1, label: 'Mock option 1' },
      { value: 2, label: 'Mock option 2' }
    ];
    component.dataSource = dataSource;
    spyOn(component.change, 'emit');
    fixture.detectChanges();
    dataSource.connectSubject.next(options);
    expect(component.options).toEqual(options);
    expect(component.change.emit).toHaveBeenCalledWith(options);
  });

});
