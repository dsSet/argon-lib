import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { SelectHandlerComponent } from './SelectHandlerComponent';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectMultipleRegistryService } from '../../services/SelectMultipleRegistryService';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('SelectHandlerComponent', () => {

  let component: SelectHandlerComponent;
  let fixture: ComponentFixture<SelectHandlerComponent>;
  let registry: AbstractRegistry;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [ SelectHandlerComponent ],
    providers: [
      { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectHandlerComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
  });

  it('className should toggle .ar-select-handler class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-handler');
  });

  it('active should toggle .ar-select-handler--active class', () => {
    fixture.detectChanges();
    registry.setIsOpened(true);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-handler--active').toBeTruthy();
    registry.setIsOpened(false);
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, 'ar-select-handler--active').toBeFalsy();
  });

});
