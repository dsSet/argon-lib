import { ChangeDetectionStrategy, Component, HostBinding, Injector, OnDestroy, OnInit } from '@angular/core';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { BaseShape } from '@argon/tools';

@Component({
  selector: 'ar-select-handler',
  template: `
    <ar-icon-chevron class="ar-select-handler__content"></ar-icon-chevron>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectHandlerComponent extends SelectRegistryMixin(BaseShape) implements OnInit, OnDestroy {

  @HostBinding('class.ar-select-handler') className = true;

  @HostBinding('class.ar-select-handler--active') get active(): boolean { return this.registry.state.isOpened; }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
