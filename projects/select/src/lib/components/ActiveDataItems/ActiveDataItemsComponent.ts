import { ChangeDetectionStrategy, Component, Injector, Input, OnDestroy, OnInit, TrackByFunction } from '@angular/core';
import { BaseShape, StateInterface } from '@argon/tools';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { SelectStateInterface } from '../../interfaces/SelectStateInterface';
import { SelectDataItemInterface } from '../../interfaces/SelectDataItemInterface';

@Component({
  selector: 'ar-active-data-items',
  template: `
    <ar-select-option-default
      *ngFor="let item of dataItems; trackBy: trackBy"
      [data]="item.data"
      [label]="item.label"
      [value]="item.value"
      [disabled]="item.disabled"
    ></ar-select-option-default>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActiveDataItemsComponent extends SelectRegistryMixin(BaseShape) implements OnInit, OnDestroy {

  @Input() trackBy: TrackByFunction<SelectDataItemInterface>;

  public dataItems: Array<SelectDataItemInterface>;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  handleUpdateState(state: StateInterface<SelectStateInterface>): void {
    if (state.changes.value) {
      this.dataItems = this.registry.activeDataItems;
    }

    super.handleUpdateState(state);
  }
}
