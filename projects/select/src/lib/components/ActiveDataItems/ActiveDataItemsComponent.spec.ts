import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectMultipleRegistryService } from '../../services/SelectMultipleRegistryService';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ActiveDataItemsComponent } from './ActiveDataItemsComponent';

describe('ActiveDataItemsComponent', () => {

  let component: ActiveDataItemsComponent;
  let fixture: ComponentFixture<ActiveDataItemsComponent>;
  let registry: AbstractRegistry;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [ ActiveDataItemsComponent ],
    providers: [
      { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveDataItemsComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
  });

  it('handleUpdateState should update dataItems', () => {
    fixture.detectChanges();
    const item1 = { label: 'Mock1', value: 1 };
    const item2 = { label: 'Mock2', value: 2 };
    expect(component.dataItems).not.toBeDefined();
    registry.registerDataItem(item1);
    registry.registerDataItem(item2);
    registry.setValue([item1.value]);
    expect(component.dataItems).toEqual([item1]);
    registry.setValue([item2.value]);
    expect(component.dataItems).toEqual([item2]);
    registry.setValue(null);
    expect(component.dataItems).toEqual([]);
  });

});
