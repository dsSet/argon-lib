import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { BaseShape } from '@argon/tools';
import { SelectDataItemInterface } from '../../interfaces/SelectDataItemInterface';

@Component({
  selector: 'ar-select-option-default',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectOptionDefaultComponent extends SelectRegistryMixin(BaseShape)
  implements SelectDataItemInterface, OnInit, OnDestroy, OnChanges {

  @Input() data: any;

  @Input() disabled: boolean;

  @Input() label: string;

  @Input() value: any;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.registry.registerDataItem(this);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { value, data, label } = changes;
    const change = value || data || label;
    if (!change || change.isFirstChange()) {
      return;
    }

    this.registry.fireDataItemChanges();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.registry.unregisterDataItem(this);
  }
}
