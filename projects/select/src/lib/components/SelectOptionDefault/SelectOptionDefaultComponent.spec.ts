import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectRegistryService } from '../../services/SelectRegistryService';
import { SelectOptionDefaultComponent } from './SelectOptionDefaultComponent';
import { SimpleChange } from '@angular/core';


describe('SelectOptionDefaultComponent', () => {

  let component: SelectOptionDefaultComponent;
  let fixture: ComponentFixture<SelectOptionDefaultComponent>;
  let registry: SelectRegistryService;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [SelectOptionDefaultComponent],
    providers: [
      { provide: AbstractRegistry, useClass: SelectRegistryService }
    ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectOptionDefaultComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
  });

  it('should call registerDataItem onInit', () => {
    spyOn(registry, 'registerDataItem');
    component.ngOnInit();
    expect(registry.registerDataItem).toHaveBeenCalledWith(component);
  });

  it('should call unregisterDataItem onDestroy', () => {
    spyOn(registry, 'unregisterDataItem');
    component.ngOnInit();
    component.ngOnDestroy();
    expect(registry.unregisterDataItem).toHaveBeenCalledWith(component);
  });

  it('ngOnChanges should call fireDataItemChanges for value updates', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    const value = new SimpleChange(1, 2, false);
    component.ngOnChanges({ value });
    expect(component.registry.fireDataItemChanges).toHaveBeenCalled();
  });

  it('ngOnChanges should call fireDataItemChanges for data updates', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    const data = new SimpleChange(1, 2, false);
    component.ngOnChanges({ data });
    expect(component.registry.fireDataItemChanges).toHaveBeenCalled();
  });

  it('ngOnChanges should call fireDataItemChanges for label updates', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    const label = new SimpleChange(1, 2, false);
    component.ngOnChanges({ label });
    expect(component.registry.fireDataItemChanges).toHaveBeenCalled();
  });

  it('ngOnChanges should not call fireDataItemChanges for first update', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    const label = new SimpleChange(1, 2, true);
    component.ngOnChanges({ label });
    expect(component.registry.fireDataItemChanges).not.toHaveBeenCalled();
  });

  it('ngOnChanges should not call fireDataItemChanges without updates', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'fireDataItemChanges');
    component.ngOnChanges({ });
    expect(component.registry.fireDataItemChanges).not.toHaveBeenCalled();
  });
});
