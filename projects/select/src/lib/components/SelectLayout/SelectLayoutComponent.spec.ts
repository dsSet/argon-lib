import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { SelectLayoutComponent } from './SelectLayoutComponent';
import { PortalModule } from '@angular/cdk/portal';

describe('SelectLayoutComponent', () => {

  let component: SelectLayoutComponent;
  let fixture: ComponentFixture<SelectLayoutComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [PortalModule],
    declarations: [ SelectLayoutComponent ],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectLayoutComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-select-layout class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-layout');
  });

  it('should have role listbox', () => {
    HostBindingTestHelper.testRoleAttribute(component, fixture, 'listbox');
  });
});
