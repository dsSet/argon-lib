import { ChangeDetectionStrategy, Component, ElementRef, HostBinding } from '@angular/core';
import { DropdownLayoutInterface } from '@argon/dropdown';
import { Portal } from '@angular/cdk/portal';

@Component({
  selector: 'ar-select-layout',
  template: '<ng-container [cdkPortalOutlet]="content"></ng-container>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectLayoutComponent implements DropdownLayoutInterface {

  @HostBinding('class.ar-select-layout') className = true;

  @HostBinding('attr.role') role = 'listbox';

  public content: Portal<any>;

  constructor(
    public element: ElementRef<HTMLElement>
  ) { }


}
