import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectMultipleRegistryService } from '../../services/SelectMultipleRegistryService';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SelectCleanComponent } from './SelectCleanComponent';
import createSpyObj = jasmine.createSpyObj;

describe('SelectCleanComponent', () => {

  let component: SelectCleanComponent;
  let fixture: ComponentFixture<SelectCleanComponent>;
  let registry: AbstractRegistry;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [ SelectCleanComponent ],
    providers: [
      { provide: AbstractRegistry, useClass: SelectMultipleRegistryService }
    ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectCleanComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(AbstractRegistry) as any;
  });

  it('className should toggle .ar-select-clean class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-clean');
  });

  it('handleClick should call fireValueUpdate with null', () => {
    fixture.detectChanges();
    registry.setDisabled(false);
    spyOn(registry, 'fireValueUpdate');
    const event = createSpyObj(['stopPropagation']);
    const result = component.handleClick(event);
    expect(registry.fireValueUpdate).toHaveBeenCalledWith(null);
    expect(event.stopPropagation).toHaveBeenCalled();
    expect(result).toEqual(false);
  });

  it('handleClick should not call fireValueUpdate if disabled', () => {
    fixture.detectChanges();
    registry.setDisabled(true);
    spyOn(registry, 'fireValueUpdate');
    const event = createSpyObj(['stopPropagation']);
    component.handleClick(event);
    expect(registry.fireValueUpdate).not.toHaveBeenCalled();
    expect(event.stopPropagation).not.toHaveBeenCalled();
  });

});
