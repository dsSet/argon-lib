import { ChangeDetectionStrategy, Component, HostBinding, HostListener, Injector, OnDestroy, OnInit } from '@angular/core';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { BaseShape } from '@argon/tools';

@Component({
  selector: 'ar-select-clean',
  template: `
    <ar-close-icon class="ar-select-clean__content"></ar-close-icon>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectCleanComponent extends SelectRegistryMixin(BaseShape) implements OnInit, OnDestroy {

  @HostBinding('class.ar-select-clean') className = true;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  @HostListener('mousedown', ['$event'])
  @HostListener('click', ['$event'])
  public handleClick(event: Event) {
    if (this.registry.state.disabled) {
      return;
    }
    event.stopPropagation();
    this.registry.fireValueUpdate(null);
    return false;
  }

}
