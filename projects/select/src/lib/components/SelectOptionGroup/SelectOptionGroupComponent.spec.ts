import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { SelectOptionGroupComponent } from './SelectOptionGroupComponent';
import { AbstractRegistry } from '../../services/AbstractRegistry';
import { SelectRegistryService } from '../../services/SelectRegistryService';
import { Subject } from 'rxjs';

describe('SelectOptionGroupComponent', () => {

  let component: SelectOptionGroupComponent;
  let fixture: ComponentFixture<SelectOptionGroupComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    declarations: [ SelectOptionGroupComponent ],
    providers: [
      { provide: AbstractRegistry, useClass: SelectRegistryService }
    ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectOptionGroupComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-select-option-group class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-select-option-group');
  });

  it('bordered should toggle .ar-select-option-group--bordered class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'bordered', 'ar-select-option-group--bordered');
  });

  it('hidden should toggle .ar-select-option-group--hidden class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'hidden', 'ar-select-option-group--hidden');
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(component.className).toBeTruthy();
    expect(component.bordered).toBeTruthy();
    expect(component.hidden).toBeFalsy();
    expect(component.hideEmpty).toBeFalsy();
  });

  it('should toggle hidden on options changes', fakeAsync(() => {
    fixture.detectChanges();
    component.hidden = false;
    component.hideEmpty = true;
    component.options = {
      changes: new Subject<any>(),
      length: 0
    } as any;
    component.ngAfterContentInit();
    (component.options as any).changes.next(0);
    expect(component.hidden).toEqual(true);
    tick();
    (component.options as any).length = 12;
    (component.options as any).changes.next(1);
    expect(component.hidden).toEqual(false);
    (component.options as any).changes.complete();
  }));

  it('should not toggle hidden with hideEmpty false flag', fakeAsync(() => {
    fixture.detectChanges();
    component.hidden = false;
    component.hideEmpty = false;
    component.options = {
      changes: new Subject<any>(),
      length: 0
    } as any;
    component.ngAfterContentInit();
    (component.options as any).changes.next(0);
    expect(component.hidden).toEqual(false);
  }));

  it('should toggle hidden on options changes', fakeAsync(() => {
    fixture.detectChanges();
    component.hidden = false;
    component.hideEmpty = true;
    component.asyncOptions = {
      change: new Subject<any>(),
      options: []
    } as any;
    component.ngAfterContentInit();
    (component.asyncOptions as any).change.next(0);
    expect(component.hidden).toEqual(true);
    tick();
    (component.asyncOptions as any).options = [1, 2, 3];
    (component.asyncOptions as any).change.next(1);
    expect(component.hidden).toEqual(false);
    (component.asyncOptions as any).change.complete();
  }));

});
