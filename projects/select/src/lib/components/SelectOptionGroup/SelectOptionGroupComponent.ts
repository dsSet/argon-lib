import size from 'lodash/size';
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component, ContentChild, ContentChildren,
  HostBinding, Injector,
  Input, OnDestroy, OnInit, QueryList
} from '@angular/core';
import { BaseShape, Unsubscribable } from '@argon/tools';
import { SelectRegistryMixin } from '../../mixins/SelectRegistryMixin';
import { SelectOptionAsyncComponent } from '../SelectOptionAsync/SelectOptionAsyncComponent';
import { SelectOptionComponent } from '../SelectOption/SelectOptionComponent';
import { merge, Subscription } from 'rxjs';

@Component({
  selector: 'ar-select-option-group',
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectOptionGroupComponent extends SelectRegistryMixin(BaseShape) implements AfterContentInit, OnInit, OnDestroy {

  @HostBinding('class.ar-select-option-group') className = true;

  @HostBinding('class.ar-select-option-group--bordered') @Input() bordered = true;

  @HostBinding('class.ar-select-option-group--hidden') public hidden: boolean;

  @Input() hideEmpty: boolean;

  @ContentChild(SelectOptionAsyncComponent, { read: SelectOptionAsyncComponent }) asyncOptions: SelectOptionAsyncComponent;

  @ContentChildren(SelectOptionComponent, { descendants: true }) options: QueryList<SelectOptionComponent>;

  private optionsSubscription: Subscription;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngAfterContentInit(): void {
    const source = this.asyncOptions ? merge(
      this.asyncOptions.change,
      this.options.changes
    ) : this.options.changes;

    this.optionsSubscription = source.subscribe(this.handleOptionsChange);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  handleOptionsChange = () => {
    let hidden = this.hidden;
    if (this.hideEmpty) {
      const hasAsyncOptions = this.asyncOptions ? size(this.asyncOptions.options) > 0 : false;
      const hasSyncOptions = this.options.length > 0;
      hidden = !hasAsyncOptions && !hasSyncOptions;
    } else {
      hidden = false;
    }

    if (hidden !== this.hidden) {
      this.hidden = hidden;
      this.changeDetectorRef.markForCheck();
    }
  }

}
