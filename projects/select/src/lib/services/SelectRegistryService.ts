import filter from 'lodash/filter';
import { Injectable, OnDestroy } from '@angular/core';
import { AbstractRegistry } from './AbstractRegistry';
import { SelectDataItemInterface } from '../interfaces/SelectDataItemInterface';

@Injectable()
export class SelectRegistryService extends AbstractRegistry implements OnDestroy {

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public handleOptionToggle(value: any) {
    if (!this.isEqual(value, this.state.value)) {
      this.fireValueUpdate(value);
    }
  }

  public isOptionSelected(value: any): boolean {
    return this.isEqual(this.state.value, value);
  }

  protected findDataItems(value: any): Array<SelectDataItemInterface> {
    return filter(this.dataItems, (item: SelectDataItemInterface) => this.isEqual(item.value, value));
  }

}
