import { AbstractRegistry } from './AbstractRegistry';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { ElementRef, EventEmitter, Injectable, Injector } from '@angular/core';
import createSpy = jasmine.createSpy;
import { SelectOptionInterface } from '../interfaces/SelectOptionInterface';
import { SelectDataItemInterface } from '../interfaces/SelectDataItemInterface';
import { UIClickableInterface } from '@argon/tools';

describe('AbstractRegistry', () => {

  @Injectable()
  class MockRegistry extends AbstractRegistry {
    constructor(injector: Injector) {
      super(injector);
    }

    getOnChange(): any {
      return this.onChange;
    }

    getOnTouch(): any {
      return this.onTouch;
    }

    mockFindDataItems(value: any): any {
      return this.findDataItems(value);
    }
  }

  const createMockOption = (value: any, position: any, data = null, disabled = false): SelectOptionInterface & UIClickableInterface => ({
    value,
    label: `Mock ${value}`,
    disabled,
    data,
    element: new ElementRef({
      compareDocumentPosition: createSpy('compareDocumentPosition').and.returnValue(position)
    }) as ElementRef<any>,
    getLabel(): string { return ''; },
    setActiveStyles(): void { },
    setInactiveStyles(): void { },
    click: new EventEmitter<void>()
  });

  let registry: MockRegistry;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    providers: [
      MockRegistry
    ]
  })));

  beforeEach(() => {
    registry = TestBed.inject(MockRegistry) as any;
  });

  afterEach(() => {
    registry.ngOnDestroy();
  });

  it('getInitialState should have defaults', () => {
    expect(AbstractRegistry.getInitialState()).toEqual({
      isOpened: false,
      value: null,
      disabled: false,
      query: '',
      dataItemsChanged: false
    });
  });

  it('should create selectState with initial state', () => {
    expect(registry.selectState.value).toEqual({ state: AbstractRegistry.getInitialState(), changes: { } });
  });

  it('state property should return state from select state', () => {
    expect(registry.state).toEqual(AbstractRegistry.getInitialState());
  });

  it('hasValue property should check value in the state', () => {
    registry.setValue(123);
    expect(registry.hasValue).toEqual(true);
    registry.setValue(null);
    expect(registry.hasValue).toEqual(false);
    registry.setValue(0);
    expect(registry.hasValue).toEqual(true);
    registry.setValue(undefined);
    expect(registry.hasValue).toEqual(false);
    registry.setValue('');
    expect(registry.hasValue).toEqual(true);
  });

  it('ngOnDestroy should complete stateState subject', () => {
    spyOn(registry.selectState, 'complete');
    registry.ngOnDestroy();
    expect(registry.selectState.complete).toHaveBeenCalled();
  });

  it('setIsOpened should set isOpened state value', () => {
    registry.setIsOpened(true);
    expect(registry.state.isOpened).toEqual(true);
    registry.setIsOpened(false);
    expect(registry.state.isOpened).toEqual(false);
  });

  it('setValue should set value state value', () => {
    registry.setValue(321);
    expect(registry.state.value).toEqual(321);
  });

  it('setDisabled should set disabled state value', () => {
    registry.setDisabled(true);
    expect(registry.state.disabled).toEqual(true);
    registry.setDisabled(false);
    expect(registry.state.disabled).toEqual(false);
  });

  it('setQuery should set query state value', () => {
    registry.setQuery('mock');
    expect(registry.state.query).toEqual('mock');
  });

  it('registerOnTouched should update onTouch fn', () => {
    const onTouch = () => 'Mock Fn';
    registry.registerOnTouched(onTouch);
    expect(registry.getOnTouch()).toEqual(onTouch);
  });

  it('handleValueChanges should call onChange fn', () => {
    const onChange = createSpy('onChange');
    registry.setValue(8);
    registry.registerOnChange(onChange);
    registry.handleValueChanges(123);
    expect(onChange).toHaveBeenCalledWith(123);
    expect(registry.state.value).toEqual(8);
  });

  it('isOptionSelected should return false', () => {
    expect(registry.isOptionSelected(3)).toEqual(false);
  });

  it('fireValueUpdate should call onChange and update state', () => {
    const onChange = createSpy('onChange');
    registry.setValue(8);
    registry.registerOnChange(onChange);
    registry.fireValueUpdate(123);
    expect(onChange).toHaveBeenCalledWith(123);
    expect(registry.state.value).toEqual(123);
  });

  it('findDataItems should return empty array', () => {
    expect(registry.mockFindDataItems(123)).toEqual([]);
  });

  it('registerOption should reset optionItems with new option', () => {
    spyOn(registry.optionItems, 'reset');
    const option = createMockOption(1, Node.DOCUMENT_POSITION_CONTAINED_BY);
    registry.registerOption(option);
    expect(registry.optionItems.reset).toHaveBeenCalledWith([option]);
  });

  it('unregisterOption should reset optionItems without option', () => {
    const option1 = createMockOption(1, Node.DOCUMENT_POSITION_CONTAINED_BY);
    const option2 = createMockOption(2, Node.DOCUMENT_POSITION_CONTAINED_BY);
    registry.registerOption(option1);
    registry.registerOption(option2);

    expect(registry.optionItems.toArray()).toEqual([option2, option1]);
    spyOn(registry.optionItems, 'reset');
    registry.unregisterOption(option1);
    expect(registry.optionItems.reset).toHaveBeenCalledWith([option2]);
  });

  it('registerDataItem should add data item', () => {
    const dataItem: SelectDataItemInterface = {
      data: null,
      disabled: false,
      label: 'mock',
      value: 1
    };
    expect(registry.dataItems).toEqual([]);
    registry.registerDataItem(dataItem);
    expect(registry.dataItems).toEqual([dataItem]);
  });

  it('unregisterDataItem should ermove data item', () => {
    const dataItem: SelectDataItemInterface = {
      data: null,
      disabled: false,
      label: 'mock',
      value: 1
    };
    registry.dataItems = [dataItem];
    registry.unregisterDataItem(dataItem);
    expect(registry.dataItems).toEqual([]);
  });
});
