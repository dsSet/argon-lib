import uniqWith from 'lodash/uniqWith';
import without from 'lodash/without';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { SelectStateInterface } from '../interfaces/SelectStateInterface';
import { BaseShape, CompareValuesMixin, StateSubject, StateInterface, UIClickableInterface } from '@argon/tools';
import { Injectable, Injector, OnDestroy, QueryList } from '@angular/core';
import { SelectDataItemInterface } from '../interfaces/SelectDataItemInterface';
import { SelectOptionInterface } from '../interfaces/SelectOptionInterface';

@Injectable()
export class AbstractRegistry extends CompareValuesMixin(BaseShape) implements OnDestroy {

  static getInitialState(): SelectStateInterface {
    return {
      isOpened: false,
      value: null,
      disabled: false,
      query: '',
      dataItemsChanged: false
    };
  }

  public readonly selectState = new StateSubject<SelectStateInterface>(AbstractRegistry.getInitialState());

  public get state(): SelectStateInterface { return this.selectState.state; }

  public get hasValue(): boolean { return this.state.value !== null && this.state.value !== undefined; }

  public get activeDataItems(): Array<SelectDataItemInterface> {
    return uniqWith(this.findDataItems(this.state.value),
      (itemA: SelectDataItemInterface, itemB: SelectDataItemInterface) => this.isEqual(itemA.value, itemB.value)
    );
  }

  public readonly optionItems: QueryList<SelectOptionInterface & UIClickableInterface>
    = new QueryList<SelectOptionInterface & UIClickableInterface>();

  public dataItems: Array<SelectDataItemInterface> = [];

  public detector = new Subject<void>();

  constructor(injector: Injector) {
    super(injector);
    this.selectState.subscribe(this.handleSelectOpen);

    this.detector.pipe(
      debounceTime(250)
    ).subscribe(this.fireDataItemChanges);
  }

  ngOnDestroy(): void {
    this.selectState.complete();
    this.detector.complete();
  }

  /** Update isOpened state data */
  public setIsOpened(isOpened: boolean) {
    if (this.state.isOpened !== isOpened) {
      this.onTouch();
      this.selectState.setState({ isOpened });
    }
  }

  /** Update value state data */
  public setValue(value: any) {
    if (!this.isEqual(value, this.state.value)) {
      this.selectState.setState({ value });
    }
  }

  /** Update disabled state data */
  public setDisabled(disabled: boolean) {
    if (this.state.disabled !== disabled) {
      this.selectState.setState({ disabled });
    }
  }

  public setQuery(query: string) {
    if (this.state.query !== query) {
      this.selectState.setState({ query });
    }
  }

  /** Register onChange fn for ValueAccessor compatibility */
  public registerOnChange(fn: any) {
    this.onChange = fn;
  }

  /** Register onTouch fn for ValueAccessor compatibility */
  public registerOnTouched(fn: any) {
    this.onTouch = fn;
  }

  /** Call onChange function with next value. Should not based on state. */
  public handleValueChanges(value: any) {
    this.onChange(value);
  }

  /** Toggle single option in value */
  public handleOptionToggle(value: any) {

  }

  /** determinate is value selected */
  public isOptionSelected(value: any): boolean {
    return false;
  }

  /** change value inside state and emit changes with changeFn */
  public fireValueUpdate(value: any) {
    this.handleValueChanges(value);
    this.setValue(value);
  }

  public registerOption(option: SelectOptionInterface & UIClickableInterface) {
    const nextCollection = this.reorderItems(this.push(this.optionItems.toArray(), option));
    this.optionItems.reset(nextCollection);
  }

  public unregisterOption(option: SelectOptionInterface & UIClickableInterface) {
    const nextCollection = this.reorderItems(without(this.optionItems.toArray(), option));
    this.optionItems.reset(nextCollection);
  }

  public registerDataItem(item: SelectDataItemInterface) {
    this.detector.next();
    this.dataItems = this.push(this.dataItems, item);
  }

  public unregisterDataItem(item: SelectDataItemInterface) {
    this.detector.next();
    this.dataItems = without(this.dataItems, item);
  }

  public fireDataItemChanges = () => {
    this.selectState.setState({ dataItemsChanged: true });
  }

  protected onChange = (value: any) => { };

  protected onTouch = () => { };

  protected reorderItems(items: Array<SelectOptionInterface & UIClickableInterface>): Array<SelectOptionInterface & UIClickableInterface> {
    return items.sort(this.optionsOrderFunction);
  }

  /** Find data item */
  protected findDataItems(value: any): Array<SelectDataItemInterface> {
    return [];
  }

  private optionsOrderFunction = (optionA: SelectOptionInterface, optionB: SelectOptionInterface): number => {
    const position = optionA.element.nativeElement.compareDocumentPosition(optionB.element.nativeElement);
    return position === Node.DOCUMENT_POSITION_PRECEDING ? 1 : -1;
  }

  private handleSelectOpen = (state: StateInterface<SelectStateInterface>) => {
    if (state.changes.isOpened && state.state.isOpened) {
      const items = this.reorderItems(this.optionItems.toArray());
      this.optionItems.reset(items);
    }
  }
}
