import { TestBed, waitForAsync } from '@angular/core/testing';
import { SelectMultipleRegistryService } from './SelectMultipleRegistryService';


describe('SelectMultipleRegistryService', () => {

  let registry: SelectMultipleRegistryService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    providers: [
      SelectMultipleRegistryService
    ]
  })));

  beforeEach(() => {
    registry = TestBed.inject(SelectMultipleRegistryService) as any;
  });

  afterEach(() => {
    registry.ngOnDestroy();
  });

  it('currentValue should return empty array or current value', () => {
    registry.setValue(null);
    expect(registry.currentValue).toEqual([]);
    registry.setValue([1, 2]);
    expect(registry.currentValue).toEqual([1, 2]);
  });

  it('hasValue should based on value array size', () => {
    registry.setValue(null);
    expect(registry.hasValue).toEqual(false);
    registry.setValue([1, 2]);
    expect(registry.hasValue).toEqual(true);
    registry.setValue([]);
    expect(registry.hasValue).toEqual(false);
  });

  it('handleOptionToggle should call fireValueUpdate without value if selected', () => {
    registry.setValue([1, 2, 3]);
    spyOn(registry, 'fireValueUpdate');
    registry.handleOptionToggle(2);
    expect(registry.fireValueUpdate).toHaveBeenCalledWith([1, 3]);
  });

  it('handleOptionToggle should call fireValueUpdate with value if not selected', () => {
    registry.setValue([1, 2, 3]);
    spyOn(registry, 'fireValueUpdate');
    registry.handleOptionToggle(5);
    expect(registry.fireValueUpdate).toHaveBeenCalledWith([1, 2, 3, 5]);
  });

  it('isOptionSelected should check value inside array', () => {
    registry.setValue([5, 8, 22]);
    expect(registry.isOptionSelected(5)).toEqual(true);
    expect(registry.isOptionSelected(1)).toEqual(false);
  });
});
