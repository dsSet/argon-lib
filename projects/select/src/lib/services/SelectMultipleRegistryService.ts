import size from 'lodash/size';
import find from 'lodash/find';
import map from 'lodash/map';
import compact from 'lodash/compact';
import { Injectable, OnDestroy } from '@angular/core';
import { AbstractRegistry } from './AbstractRegistry';
import { SelectDataItemInterface } from '../interfaces/SelectDataItemInterface';

@Injectable()
export class SelectMultipleRegistryService extends AbstractRegistry implements OnDestroy {

  public get currentValue(): Array<any> {
    return this.state.value || [];
  }

  public get hasValue(): boolean { return size(this.state.value) > 0; }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public handleOptionToggle(value: any) {
    if (this.find(this.currentValue, value)) {
      this.fireValueUpdate(this.remove(this.currentValue, value));
    } else {
      this.fireValueUpdate(this.push(this.currentValue, value));
    }
  }

  public isOptionSelected(value: any): boolean {
    return this.findIndex(this.currentValue, value) >= 0;
  }

  protected findDataItems(value: any = []): Array<SelectDataItemInterface> {
    return compact(map(value, (item: any) => {
      return find(this.dataItems, (dataItem: SelectDataItemInterface) => this.isEqual(item, dataItem.value));
    }));
  }

}
