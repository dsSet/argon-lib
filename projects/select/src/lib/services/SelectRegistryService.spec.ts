import { TestBed, waitForAsync } from '@angular/core/testing';
import { SelectRegistryService } from './SelectRegistryService';


describe('SelectRegistryService', () => {

  let registry: SelectRegistryService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    providers: [
      SelectRegistryService
    ]
  })));

  beforeEach(() => {
    registry = TestBed.inject(SelectRegistryService) as any;
  });

  afterEach(() => {
    registry.ngOnDestroy();
  });

  it('handleOptionToggle should call fireValueUpdate if value not selected', () => {
    registry.setValue(5);
    spyOn(registry, 'fireValueUpdate');
    registry.handleOptionToggle(2);
    expect(registry.fireValueUpdate).toHaveBeenCalledWith(2);
  });

  it('handleOptionToggle should not call fireValueUpdate if value selected', () => {
    registry.setValue(5);
    spyOn(registry, 'fireValueUpdate');
    registry.handleOptionToggle(5);
    expect(registry.fireValueUpdate).not.toHaveBeenCalledWith(2);
  });

  it('isOptionSelected should compare value with state', () => {
    registry.setValue(5);
    expect(registry.isOptionSelected(5)).toEqual(true);
    expect(registry.isOptionSelected(1)).toEqual(false);
  });
});
