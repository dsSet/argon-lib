# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@2.0.3...@argon/select@2.0.4) (2023-12-12)


### Bug Fixes

* update control value accessors to handle disabled state correctly ([b9d29c5](https://bitbucket.org/dsSet/argon-lib/commits/b9d29c56fb1ba76738ce5fbf2824ede9b736af82))





## [2.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@2.0.2...@argon/select@2.0.3) (2023-10-18)


### Bug Fixes

* **select:** fix scss imports ([36f5dfd](https://bitbucket.org/dsSet/argon-lib/commits/36f5dfd4cbd298b5d94ab3fbc8c7033947b83e10))






## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@2.0.1...@argon/select@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/select





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@1.0.5...@argon/select@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/select






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@1.0.5...@argon/select@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/select






## [1.0.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@1.0.4...@argon/select@1.0.5) (2020-10-02)

**Note:** Version bump only for package @argon/select





## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@1.0.3...@argon/select@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/select





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@1.0.2...@argon/select@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/select






## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@1.0.1...@argon/select@1.0.2) (2020-09-29)


### Bug Fixes

* **select:** fix incorrect interface implementation ([586255b](https://bitbucket.org/dsSet/argon-lib/commits/586255b77b7641ac2ddd3a6e8974f31ef2e0fba8))





## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.11...@argon/select@1.0.1) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.11...@argon/select@1.0.0) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))






## [0.5.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.10...@argon/select@0.5.11) (2020-08-04)

**Note:** Version bump only for package @argon/select






## [0.5.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.9...@argon/select@0.5.10) (2020-07-07)


### Bug Fixes

* **select:** label component updates, state update on dataitem changes ([69db269](https://bitbucket.org/dsSet/argon-lib/commits/69db269))






## [0.5.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.8...@argon/select@0.5.9) (2020-06-10)

**Note:** Version bump only for package @argon/select





## [0.5.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.7...@argon/select@0.5.8) (2020-06-04)

**Note:** Version bump only for package @argon/select






## [0.5.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.6...@argon/select@0.5.7) (2020-05-21)

**Note:** Version bump only for package @argon/select






## [0.5.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.5...@argon/select@0.5.6) (2020-04-21)


### Bug Fixes

* **select:** add misset output for select option ([40b65de](https://bitbucket.org/dsSet/argon-lib/commits/40b65de))





## [0.5.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.4...@argon/select@0.5.5) (2020-04-18)

**Note:** Version bump only for package @argon/select





## [0.5.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.3...@argon/select@0.5.4) (2020-04-18)

**Note:** Version bump only for package @argon/select






## [0.5.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.2...@argon/select@0.5.3) (2020-03-24)

**Note:** Version bump only for package @argon/select





## [0.5.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.1...@argon/select@0.5.2) (2020-03-24)

**Note:** Version bump only for package @argon/select






## [0.5.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.5.0...@argon/select@0.5.1) (2020-03-18)

**Note:** Version bump only for package @argon/select





# [0.5.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.9...@argon/select@0.5.0) (2020-02-25)


### Bug Fixes

* **select:** fixdefault option ctor for aot mode ([3f275bb](https://bitbucket.org/dsSet/argon-lib/commits/3f275bb))


### Features

* **select:** implement component for static options filtering ([7257c3b](https://bitbucket.org/dsSet/argon-lib/commits/7257c3b))





## [0.4.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.8...@argon/select@0.4.9) (2020-02-21)


### Bug Fixes

* **select:** add missed default annotation for the style variables ([5698d6c](https://bitbucket.org/dsSet/argon-lib/commits/5698d6c))
* **select:** fix select view updates for async and dynamic options ([95b34f8](https://bitbucket.org/dsSet/argon-lib/commits/95b34f8))






## [0.4.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.7...@argon/select@0.4.8) (2020-02-20)


### Bug Fixes

* **select:** fix incorrect import ([218ae31](https://bitbucket.org/dsSet/argon-lib/commits/218ae31))





## [0.4.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.6...@argon/select@0.4.7) (2020-02-14)

**Note:** Version bump only for package @argon/select






## [0.4.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.5...@argon/select@0.4.6) (2019-11-29)


### Bug Fixes

* **select:** hooks calls ([4839fa2](https://bitbucket.org/dsSet/argon-lib/commits/4839fa2))





## [0.4.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.4...@argon/select@0.4.5) (2019-11-22)

**Note:** Version bump only for package @argon/select





## [0.4.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.3...@argon/select@0.4.4) (2019-11-22)

**Note:** Version bump only for package @argon/select





## [0.4.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.2...@argon/select@0.4.3) (2019-11-22)

**Note:** Version bump only for package @argon/select





## [0.4.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.1...@argon/select@0.4.2) (2019-11-22)

**Note:** Version bump only for package @argon/select






## [0.4.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.4.0...@argon/select@0.4.1) (2019-10-31)

**Note:** Version bump only for package @argon/select





# [0.4.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.3.0...@argon/select@0.4.0) (2019-10-28)


### Bug Fixes

* **select:** fix clean button prevent defaults actions ([1362123](https://bitbucket.org/dsSet/argon-lib/commits/1362123))
* **select:** fix multiselect disabled class name ([1a57e7a](https://bitbucket.org/dsSet/argon-lib/commits/1a57e7a))
* **select:** fix search component styles and init action ([555ff38](https://bitbucket.org/dsSet/argon-lib/commits/555ff38))
* **select:** fix select badge click stopPropagation ([ec50d8e](https://bitbucket.org/dsSet/argon-lib/commits/ec50d8e))
* **select:** fix typings ([97e10c9](https://bitbucket.org/dsSet/argon-lib/commits/97e10c9))
* **select:** minor fixes ([470ccb9](https://bitbucket.org/dsSet/argon-lib/commits/470ccb9))
* **select:** unregister option and data item methods ([d2a4c8d](https://bitbucket.org/dsSet/argon-lib/commits/d2a4c8d))


### Features

* **select:** add arTextCut directive and fix styles for text overflow ([6485c63](https://bitbucket.org/dsSet/argon-lib/commits/6485c63))
* **select:** add Autosuggest component ([7eb300a](https://bitbucket.org/dsSet/argon-lib/commits/7eb300a))
* **select:** add select-data-itens component ([5da39db](https://bitbucket.org/dsSet/argon-lib/commits/5da39db))
* **select:** implement multiselect component ([73eb8c4](https://bitbucket.org/dsSet/argon-lib/commits/73eb8c4))





# [0.3.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.2.0...@argon/select@0.3.0) (2019-10-11)


### Features

* **select:** move data items to the label component ([413e1b2](https://bitbucket.org/dsSet/argon-lib/commits/413e1b2))





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/select@0.1.0...@argon/select@0.2.0) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))
* **select:** fix peer dependencies ([09c315f](https://bitbucket.org/dsSet/argon-lib/commits/09c315f))


### Features

* **select:** implement multiselect component ([ab87418](https://bitbucket.org/dsSet/argon-lib/commits/ab87418))





# 0.1.0 (2019-10-08)


### Bug Fixes

* 🐛 fix select build ([1bdf71e](https://bitbucket.org/dsSet/argon-lib/commits/1bdf71e))


### Features

* **select:** add a11y iterations ([3578da4](https://bitbucket.org/dsSet/argon-lib/commits/3578da4))
* **select:** add base select functionality and styles ([700ab60](https://bitbucket.org/dsSet/argon-lib/commits/700ab60))
* **select:** add select elements, add styles ([ac94810](https://bitbucket.org/dsSet/argon-lib/commits/ac94810))
* **select:** add select module ([80f55ac](https://bitbucket.org/dsSet/argon-lib/commits/80f55ac))
* **select:** implement async options component, implement options data source ([8abc625](https://bitbucket.org/dsSet/argon-lib/commits/8abc625))
