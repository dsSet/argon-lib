import { storiesOf, moduleMetadata } from '@storybook/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { DropdownModule } from './index';
import { DropdownManagementComponent } from './components/DropdownManagementComponent';
import { DropdownCustomUIComponent } from './components/DropdownCustomUIComponent';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/Service.md').default + require('../docs/index.md').default
};

storiesOf('Argon|dropdown/Use Dropdown Service', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        CommonModule,
        DropdownModule
      ],
      declarations: [
        DropdownManagementComponent,
        DropdownCustomUIComponent
      ]
    })
  )
  .add('Manage dropdowns from template', () => ({
    template: `
      <h4>Manage from template</h4>
      <div class="btn-group">
        <button class="btn btn-sm btn-info" (click)="dropdown.open()">
          Open
        </button>
        <button class="btn btn-sm btn-info" (click)="dropdown.close()">
          Close
        </button>
      </div>
      <p>{{ dropdown.opened ? 'Dropdown opened' : 'Dropdown closed' }}</p>
      <ar-dropdown-focusable #dropdown>
        <div  class="dropdown-content">Dropdown 1</div>
        <ng-template>Dropdown content</ng-template>
      </ar-dropdown-focusable>
    `
  }), { notes })
  .add('Manage dropdowns with service', () => ({
    component: DropdownManagementComponent,
    moduleMetadata: { }
  }), { notes })
  .add('Custom Dropdown UI', () => ({
    template: `
      <ar-dropdown-focusable>
        <div class="btn btn-primary">Dropdown Click trigger</div>
        <ng-template>
          <div>
            Dropdown content
            <story-dropdown-custom-ui>Click to change color</story-dropdown-custom-ui>
          </div>
        </ng-template>
      </ar-dropdown-focusable>
      <ar-dropdown-focusable>
        <story-dropdown-custom-ui>Click to change color</story-dropdown-custom-ui>
        <ng-template>
          <div>
            Another dropdown
          </div>
        </ng-template>
      </ar-dropdown-focusable>
    `
  }), { notes });
