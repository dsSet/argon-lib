import { storiesOf, moduleMetadata } from '@storybook/angular';
import { DropdownModule } from './index';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { StyleKitModule } from '@argon/style-kit';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/Menu.md').default + require('../docs/index.md').default
};

storiesOf('Argon|dropdown/Dropdown Menu', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        CommonModule,
        DropdownModule,
        StyleKitModule
      ]
    })
  ).add('Base (clickable)', () => ({
    props: {
      items: Array.from({ length: 30 }).map((item: any, index: number) => index + 4),
      selectedItem: null
    },
    template: `
        <ar-dropdown layoutType="argon-menu">
          <div class="btn btn-primary">Click to open</div>
          <ng-template>
            <ar-dropdown-menu>
              <ar-dropdown-menu-item [disabled]="true">Menu Item 1 (disabled)</ar-dropdown-menu-item>
              <ar-dropdown-menu-item [closeOnSelect]="false">Menu Item 2 (should not be closed on click)</ar-dropdown-menu-item>
              <ar-dropdown-menu-item [selected]="true">Menu Item 3 (should be selected)</ar-dropdown-menu-item>
              <ar-dropdown-menu-item>Menu Item 4</ar-dropdown-menu-item>
              <ar-dropdown-menu-item>Menu Item 5</ar-dropdown-menu-item>
            </ar-dropdown-menu>
          </ng-template>
        </ar-dropdown>
    `
  }), { notes })
  .add('Overlap test (hoverable)', () => ({
    props: {
      items: Array.from({ length: 50 }).map((item: any, index: number) => index),
      selectedItem: null
    },
    template: `
        <ar-dropdown-hover layoutType="argon-menu">
          <div class="btn btn-primary">Hover to open</div>
          <ng-template>
            <ar-dropdown-menu>
              <ar-dropdown-menu-item
                *ngFor="let item of items"
                (click)="selectedItem = item"
                [selected]="selectedItem === item"
              >Menu Item {{ item }}</ar-dropdown-menu-item>
            </ar-dropdown-menu>
          </ng-template>
        </ar-dropdown-hover>
    `
  }), { notes })
  .add('Menu item with long text', () => ({
    props: {
      message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    },
    template: `
        <ar-dropdown layoutType="argon-menu">
          <div class="btn btn-primary">Focus to open</div>
          <ng-template>
            <ar-dropdown-menu>
              <ar-dropdown-menu-item [limited]="true" [arTextCut]="message">{{ message }}</ar-dropdown-menu-item>
            </ar-dropdown-menu>
          </ng-template>
        </ar-dropdown>
    `
  }), { notes });
