import { storiesOf, moduleMetadata } from '@storybook/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { DropdownModule } from './index';


import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/BuiltInUI.md').default + require('../docs/index.md').default
};

storiesOf('Argon|dropdown/Other UI', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        CommonModule,
        DropdownModule
      ]
    })
  )
  .add('[arDropdownClose]', () => ({
    template: `
        <ar-dropdown-focusable>
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content
              <div arDropdownClose>Click to close dropdown</div>
            </div>
          </ng-template>
        </ar-dropdown-focusable>
    `
  }), { notes })
  .add('*arDropdownMobile', () => ({
    template: `
        <ar-dropdown>
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Resize screen to mobile to see hidden content
              <div *arDropdownMobile>This content will be visible for mobile mode only</div>
            </div>
          </ng-template>
        </ar-dropdown>
    `
  }), { notes });
