import { storiesOf, moduleMetadata } from '@storybook/angular';
import { DropdownModule } from './index';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownContentComponent } from './components/DropdownContentComponent';
import { ComponentPortal } from '@angular/cdk/portal';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/BaseUsage.md').default + require('../docs/index.md').default
};


storiesOf('Argon|dropdown/Base Dropdown', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        DropdownModule
      ],
      declarations: [
        DropdownContentComponent
      ],
      entryComponents: [
        DropdownContentComponent
      ]
    })
  )
  .add('Clickable dropdown (from template)', () => ({
      template: `
        <ar-dropdown>
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content from template
            </div>
          </ng-template>
        </ar-dropdown>
      `
  }), { notes })
  .add('Clickable dropdown (from *cdkPortal)', () => ({
    template: `
        <ar-dropdown>
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <div *cdkPortal>
            Dropdown content from cdkPortal
          </div>
        </ar-dropdown>`
  }), { notes })
  .add('Clickable dropdown (from Portal)', () => ({
    props: {
      content: new ComponentPortal(DropdownContentComponent)
    },
    template: `
        <ar-dropdown [portal]="content">
          <div class="btn btn-primary">Dropdown Click trigger</div>
        </ar-dropdown>`
  }), { notes })
  .add('Focusable dropdown', () => ({
    template: `
        <ar-dropdown-focusable>
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content
            </div>
          </ng-template>
        </ar-dropdown-focusable>
    `
  }), { notes })
  .add('Hoverable dropdown', () => ({
    template: `
        <ar-dropdown-hover>
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content
            </div>
          </ng-template>
        </ar-dropdown-hover>
    `
  }), { notes })
  .add('Hoverable dropdown (set up debounce time)', () => ({
    template: `
        <ar-dropdown-hover [enterDebounce]="500" [leaveDebounce]="500">
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content
            </div>
          </ng-template>
        </ar-dropdown-hover>
    `
  }), { notes })
  .add('Disabled', () => ({
    template: `
        <ar-dropdown-hover [disabled]="true">
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content
            </div>
          </ng-template>
        </ar-dropdown-hover>
    `
  }), { notes })
  .add('Without focus', () => ({
    template: `
        <ar-dropdown-focusable [tabIndex]="-1">
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content
            </div>
          </ng-template>
        </ar-dropdown-focusable>
    `
  }), { notes })
  .add('Autosized', () => ({
    template: `
        <ar-dropdown-focusable [autosize]="true">
          <div style="width: 800px;" class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content
            </div>
          </ng-template>
        </ar-dropdown-focusable>
    `
  }), { notes })
  .add('(changeState) usage', () => ({
    props: {
      isOpen1: false,
      isOpen2: false
    },
    template: `
        <ar-dropdown-hover (changeState)="isOpen1 = $event">
          <pre>State: {{ isOpen1 | json }}</pre>
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content.
            </div>
          </ng-template>
        </ar-dropdown-hover>
        <ar-dropdown-hover (changeState)="isOpen2 = $event">
          <pre>State: {{ isOpen2 | json }}</pre>
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content 2.
            </div>
          </ng-template>
        </ar-dropdown-hover>
    `
  }), { notes })
  .add('Prevent mobile mode for dropdown', () => ({
      template: `
        <ar-dropdown-hover [preventMobileOverlay]="true">
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content
            </div>
          </ng-template>
        </ar-dropdown-hover>
      `
  }))
  .add('Difference between dropdown types', () => ({
    template: `
        <p>Use tab key to navigate (focusable)</p>
        <ar-dropdown-focusable>
          <div class="btn btn-primary">Dropdown 1</div>
          <ng-template>
            <div>Content 1</div>
          </ng-template>
        </ar-dropdown-focusable>
        <ar-dropdown-focusable>
          <div class="btn btn-primary">Dropdown 2</div>
          <ng-template>
            <div>Content 2</div>
          </ng-template>
        </ar-dropdown-focusable>
        <ar-dropdown-focusable>
          <div class="btn btn-primary">Dropdown 3</div>
          <ng-template>
            <div>Content 3</div>
          </ng-template>
        </ar-dropdown-focusable>
        <hr />
        <p>Use click to open</p>
        <ar-dropdown>
          <div class="btn btn-primary">Dropdown 1</div>
          <ng-template>
            <div>Content 1</div>
          </ng-template>
        </ar-dropdown>
        <ar-dropdown>
          <div class="btn btn-primary">Dropdown 2</div>
          <ng-template>
            <div>Content 2</div>
          </ng-template>
        </ar-dropdown>
        <ar-dropdown>
          <div class="btn btn-primary">Dropdown 3</div>
          <ng-template>
            <div>Content 3</div>
          </ng-template>
        </ar-dropdown>
        <hr />
        <p>Use hover to open</p>
        <ar-dropdown-hover>
          <div class="btn btn-primary">Dropdown 1</div>
          <ng-template>
            <div>Content 1</div>
          </ng-template>
        </ar-dropdown-hover>
        <ar-dropdown-hover>
          <div class="btn btn-primary">Dropdown 2</div>
          <ng-template>
            <div>Content 2</div>
          </ng-template>
        </ar-dropdown-hover>
        <ar-dropdown-hover>
          <div class="btn btn-primary">Dropdown 3</div>
          <ng-template>
            <div>Content 3</div>
          </ng-template>
        </ar-dropdown-hover>
    `
  }), { notes });
