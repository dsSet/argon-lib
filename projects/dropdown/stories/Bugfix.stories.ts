import { moduleMetadata, storiesOf } from '@storybook/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownModule } from './index';
import { PreventFocusComponent } from './bugfix/PreventFocusComponent';
import { A11yModule } from '@angular/cdk/a11y';

const notes = {
  markdown: require('../docs/Bugfix.md').default
};


storiesOf('Argon|dropdown/Bugfix', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        DropdownModule
      ]
    })
  )
  .add('Focusable dropdown should not open on element click', () => ({
    component: PreventFocusComponent,
    moduleMetadata: {
      imports: [A11yModule],
      declarations: [ PreventFocusComponent ]
    }
  }), { notes })
  .add('Should correct close dropdown on page scroll', () => ({
    template: `
      <div style="height: 200vh; display: flex; align-items: center; justify-content: center">
        <ar-dropdown>
          <div class="btn btn-primary">Dropdown Click trigger</div>
          <ng-template>
            <div>
              Dropdown content from template
            </div>
          </ng-template>
        </ar-dropdown>
      </div>
    `
  }));
