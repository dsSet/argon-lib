import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { DropdownDirective, DropdownStateService } from '../index';

@Component({
  selector: 'story-dropdown-manage',
  template: `
    <h4>Open from service</h4>
    <div class="btn-group">
      <button class="btn btn-sm btn-info" (click)="openDropdown(dropdown1)">
        Open dropdown 1 {{ isOpened(dropdown1) ? 'Opened' : 'Closed' }}
      </button>
      <button class="btn btn-sm btn-info" (click)="openDropdown(dropdown2)">
        Open dropdown 2 {{ isOpened(dropdown2) ? 'Opened' : 'Closed' }}
      </button>
      <button class="btn btn-sm btn-info" (click)="openDropdown(dropdown3)">
        Open dropdown 3 {{ isOpened(dropdown3) ? 'Opened' : 'Closed' }}
      </button>
      <button class="btn btn-sm btn-info" (click)="closeDropdown()">
        Close dropdown
      </button>
    </div>
    <hr />
    <h4>Open from template</h4>
    <div class="btn-group">
      <button class="btn btn-sm btn-info" (click)="dropdown1.open()">
        Open dropdown 1 {{ dropdown1.opened ? 'Opened' : 'Closed' }}
      </button>
      <button class="btn btn-sm btn-info" (click)="dropdown2.open()">
        Open dropdown 2 {{ dropdown2.opened ? 'Opened' : 'Closed' }}
      </button>
      <button class="btn btn-sm btn-info" (click)="dropdown3.open()">
        Open dropdown 3 {{ dropdown3.opened ? 'Opened' : 'Closed' }}
      </button>
    </div>
    <hr />
    <p>{{ hasDropdown() ? 'Has opened dropdown' : 'Has no opened dropdown' }}</p>
    <hr />
    <div>
      <ar-dropdown-focusable #dropdown1>
        <div  class="dropdown-content">Dropdown 1</div>
        <ng-template>Dropdown content</ng-template>
      </ar-dropdown-focusable>
      <ar-dropdown-focusable #dropdown2>
        <div class="dropdown-content">Dropdown 2</div>
        <ng-template>Dropdown 2 content</ng-template>
      </ar-dropdown-focusable>
      <ar-dropdown-hover #dropdown3>
        <div class="dropdown-content">Dropdown 3</div>
        <ng-template>Dropdown 3 content</ng-template>
      </ar-dropdown-hover>
    </div>
  `,
  styles: [
    `.dropdown-content { width: 250px; }`
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownManagementComponent {

  @ViewChild('dropdown1', { static: true, read: DropdownDirective }) dropdown1: DropdownDirective;
  @ViewChild('dropdown2', { static: true, read: DropdownDirective }) dropdown2: DropdownDirective;
  @ViewChild('dropdown3', { static: true, read: DropdownDirective }) dropdown3: DropdownDirective;

  constructor(
    private stateService: DropdownStateService
  ) { }

  public openDropdown(dropdown: DropdownDirective) {
    this.stateService.openDropdown(dropdown);
  }

  public closeDropdown() {
    this.stateService.closeDropdown();
  }

  public isOpened(dropdown: DropdownDirective): boolean {
    return this.stateService.isOpened(dropdown);
  }

  public hasDropdown(): boolean {
    return this.stateService.hasDropdown;
  }
}
