import { ChangeDetectionStrategy, Component } from '@angular/core';


@Component({
  selector: 'story-dropdown-content',
  template: `<span>I'm content from the Portal</span>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownContentComponent {

}
