import { ChangeDetectionStrategy, Component, Injector, Renderer2 } from '@angular/core';
import { DropdownDirective, DropdownLayoutInterface, DropdownStateInterface, DropdownStateMixin } from '../index';
import { BaseShape, StateInterface } from '@argon/tools';

@Component({
  selector: 'story-dropdown-custom-ui',
  template: '<button class="btn btn-sm" (click)="setNextBackground(layout)"><ng-content></ng-content></button>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownCustomUIComponent extends DropdownStateMixin(BaseShape) {

  private counter = 0;

  private backgroundColors = ['purple', 'magenta', 'skyblue'];

  public get layout(): DropdownLayoutInterface { return this.dropdownStateService.state.layout.instance; }

  constructor(
    private renderer: Renderer2,
    private dropdown: DropdownDirective,
    injector: Injector
  ) {
    super(injector);
  }

  handleStateChange(nextState: StateInterface<DropdownStateInterface>): void {
    if (!this.isOpened(this.dropdown)) {
      return;
    }

    const { changes } = nextState;
    const { layout } = changes;

    if (layout && layout.currentValue) {
      const layoutInstance: DropdownLayoutInterface = layout.currentValue.instance;
      this.renderer.setStyle(layoutInstance.element.nativeElement, 'color', 'white');
      this.setNextBackground(layoutInstance);
    }
    super.handleStateChange(nextState);
  }

  public setNextBackground(layout: DropdownLayoutInterface) {
    if (this.isOpened(this.dropdown)) {
      const colorIndex = this.counter % 3;
      const color = this.backgroundColors[colorIndex];
      this.renderer.setStyle(layout.element.nativeElement, 'background-color', color);
      this.counter += 1;
    }
  }
}
