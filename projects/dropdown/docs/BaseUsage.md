#Dropdown component usage


## Provide dropdown content

`<ar-dropdown>`, `<ar-dropdown-hover>` and `<ar-dropdown-focusable>` are using next ways to add content.

### Dropdown content with `<ng-template>`

````html
<ar-dropdown>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
    </div>
  </ng-template>
</ar-dropdown>
````


### Dropdown content with `*cdkPortal`

````html
<ar-dropdown>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <div *cdkPortal>
    Dropdown content from cdkPortal
  </div>
</ar-dropdown>
````

### Dropdown content with custom portal

````typescript
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';

@Component({
  selector: 'story-dropdown-content',
  template: `<span>I'm content from the Portal</span>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownContentComponent {

}

const content = new ComponentPortal(DropdownContentComponent)
````

````html
<ar-dropdown [portal]="content">
  <div class="btn btn-primary">Dropdown Click trigger</div>
</ar-dropdown>
````

## Dropdown types

### Clickable dropdown

````html
<ar-dropdown>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
    </div>
  </ng-template>
</ar-dropdown>
````

### Focusable dropdown

````html
<ar-dropdown-focusable>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
    </div>
  </ng-template>
</ar-dropdown-focusable>
````

### Hoverable dropdown

````html
<ar-dropdown-hover>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
    </div>
  </ng-template>
</ar-dropdown-hover>
````

#### Set up debounce time

````html
<ar-dropdown-hover [enterDebounce]="500" [leaveDebounce]="500">
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
    </div>
  </ng-template>
</ar-dropdown-hover>
````

## Dropdown set up

### Disable dropdown

````html
<ar-dropdown-hover [disabled]="true">
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
    </div>
  </ng-template>
</ar-dropdown-hover>
````

### Remove focus (will not trigger dropdown open action on focusable dropdown)

````html
<ar-dropdown-focusable [tabIndex]="-1">
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
    </div>
  </ng-template>
</ar-dropdown-focusable>
````

### Autosize

````html
<ar-dropdown-focusable [autosize]="true">
  <div style="width: 800px;" class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
    </div>
  </ng-template>
</ar-dropdown-focusable>
````

### (changeState) usage

````html
<ar-dropdown-hover (changeState)="isOpen1 = $event">
  <pre>State: {{ isOpen1 | json }}</pre>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content.
    </div>
  </ng-template>
</ar-dropdown-hover>

<ar-dropdown-hover (changeState)="isOpen2 = $event">
  <pre>State: {{ isOpen2 | json }}</pre>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content 2.
    </div>
  </ng-template>
</ar-dropdown-hover>
````
