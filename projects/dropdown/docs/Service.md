#Using `DropdownStateService`

Dropdowns could be managed programmatically both from template and component code.

##Manage from template

````html
<h4>Manage from template</h4>

<div class="btn-group">
  <button class="btn btn-sm btn-info" (click)="dropdown.open()">
    Open
  </button>
  <button class="btn btn-sm btn-info" (click)="dropdown.close()">
    Close
  </button>
</div>

<p>{{ dropdown.opened ? 'Dropdown opened' : 'Dropdown closed' }}</p>

<ar-dropdown-focusable #dropdown>
  <div  class="dropdown-content">Dropdown 1</div>
  <ng-template>Dropdown content</ng-template>
</ar-dropdown-focusable>
````

To manage dropdowns from template you can use `DropdownDirective` public api:

|Method/property |Description
|--- |---
|`public open(): void` | Open dropdown
|`public close(): void` | Close dropdown
|`public readonly opened: boolean` | Getter for determinate dropdown state


##Manage from code

### Component template

````html
<div class="btn-group">
  <button class="btn btn-sm btn-info" (click)="openDropdown(dropdown1)">
    Open dropdown 1 {{ isOpened(dropdown1) ? 'Opened' : 'Closed' }}
  </button>
  <button class="btn btn-sm btn-info" (click)="openDropdown(dropdown2)">
    Open dropdown 2 {{ isOpened(dropdown2) ? 'Opened' : 'Closed' }}
  </button>
  <button class="btn btn-sm btn-info" (click)="openDropdown(dropdown3)">
    Open dropdown 3 {{ isOpened(dropdown3) ? 'Opened' : 'Closed' }}
  </button>
  <button class="btn btn-sm btn-info" (click)="closeDropdown()">
    Close dropdown
  </button>
</div>

<div>
  <ar-dropdown-focusable #dropdown1>
    <div  class="dropdown-content">Dropdown 1</div>
    <ng-template>Dropdown content</ng-template>
  </ar-dropdown-focusable>
  <ar-dropdown-focusable #dropdown2>
    <div class="dropdown-content">Dropdown 2</div>
    <ng-template>Dropdown 2 content</ng-template>
  </ar-dropdown-focusable>
  <ar-dropdown-hover #dropdown3>
    <div class="dropdown-content">Dropdown 3</div>
    <ng-template>Dropdown 3 content</ng-template>
  </ar-dropdown-hover>
</div>
````

### Component class

````typescript
import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { DropdownDirective, DropdownStateService } from '..';

export class DropdownManagementComponent {

  @ViewChild('dropdown1', { static: true, read: DropdownDirective }) dropdown1: DropdownDirective;
  @ViewChild('dropdown2', { static: true, read: DropdownDirective }) dropdown2: DropdownDirective;
  @ViewChild('dropdown3', { static: true, read: DropdownDirective }) dropdown3: DropdownDirective;

  constructor(
    private stateService: DropdownStateService
  ) { }

  public openDropdown(dropdown: DropdownDirective) {
    this.stateService.openDropdown(dropdown);
  }

  public closeDropdown() {
    this.stateService.closeDropdown();
  }

  public isOpened(dropdown: DropdownDirective): boolean {
    return this.stateService.isOpened(dropdown);
  }

  public hasDropdown(): boolean {
    return this.stateService.hasDropdown;
  }
}
````

> All dropdowns provided as `DropdownDirective` use it to pick up any dropdown. `DropdownDirective` also implements `DropdownInterface`.
Keep in mind that `DropdownStateService` is working with `DropdownInterface` only.

##`DropdownStateService`

````typescript
import { ComponentRef, Injectable, OnDestroy } from '@angular/core';
import { StateSubject, Unsubscribable } from '@argon/tools';
import { DropdownStateInterface } from '../interfaces/DropdownStateInterface';
import { BreakpointService } from '@argon/media';
import { Subscription } from 'rxjs';
import { DropdownInterface } from '../interfaces/DropdownInterface';
import { DropdownLayoutInterface } from '../interfaces/DropdownLayoutInterface';

class DropdownStateService {

  // State subject from `tools` package
  public readonly stateSubject: StateSubject<DropdownStateInterface>;

  // Alias for state only
  public readonly state: DropdownStateInterface;

  // Getter to check opened dropdowns
  public readonly hasDropdown: boolean;

  // Check is `dropdown` opened 
  public isOpened(dropdown: DropdownInterface): boolean;

  // Close any opened dropdown
  public closeDropdown(): void;

  // Open `dropdown`
  public openDropdown(dropdown: DropdownInterface): void;

  // Update opened state. DO NOT CALL THIS METHOD DIRECTLY!
  public setIsOpened(isOpened: boolean): void;

  // set dropdown layout. DO NOT CALL THIS METHOD DIRECTLY!
  public setLayout(layout: ComponentRef<DropdownLayoutInterface>): void;

}
````

Explanation:
 
 - Method `setIsOpened` using by internal services to set correct dropdown state. For some reasons dropdown will not appear at the DOM immediately.


Base mounting flow:

  1. Set `dropdown` to the state (just notify engine about new dropdown)
  2. Set `isOpened` flag to the state (notify about DOM mount)
  3. Set `layout` to the state (Notify about layout initial render)


Base unmount flow: 

  1. Set `dropdown` and `layout` to null
  2. Set `isOpened` flag to the state (notify about DOM unmount)


 - Method `setLayout` will call with internal engine to set correct layout from providers.
 
 
##Access to the service (Custom Dropdown UI)
 
 At the previous example `DropdownStateService` was injected with constructor.
 If you want to implement custom dropdown UI for dropdown you can use `DropdownStateMixin` instead.
 
````typescript
 function DropdownStateMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
      BaseClass: T
    ): T & Constructor<DropdownStateExtensionInterface>
```` 
  
See `DropdownStateExtensionInterface` for details.
  
Example:

This component will change dropdown color on click. Set layout color to `white` and background color to the first color from list on open.

````typescript
import { ChangeDetectionStrategy, Component, Injector, Renderer2 } from '@angular/core';
import { BaseShape, StateInterface } from '@argon/tools';
import { DropdownStateMixin, DropdownStateInterface, DropdownDirective, DropdownLayoutInterface } from '..';

@Component({
  selector: 'story-dropdown-custom-ui',
  template: '<button class="btn btn-sm" (click)="setNextBackground(layout)"><ng-content></ng-content></button>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownCustomUIComponent extends DropdownStateMixin(BaseShape) {

  private counter = 0;

  private backgroundColors = ['purple', 'magenta', 'skyblue'];

  // dropdown layout alias
  public get layout(): DropdownLayoutInterface { return this.dropdownStateService.state.layout.instance; }

  constructor(
    private renderer: Renderer2,
    private dropdown: DropdownDirective, // Parent dropdown directive
    injector: Injector
  ) {
    super(injector);
  }

  // Check dropdown state changes
  handleStateChange(nextState: StateInterface<DropdownStateInterface>): void {
    if (!this.isOpened(this.dropdown)) {
      return;
    }

    const { changes } = nextState;
    const { layout } = changes;

    if (layout && layout.currentValue) {
      const layoutInstance: DropdownLayoutInterface = layout.currentValue.instance;
      this.renderer.setStyle(layoutInstance.element.nativeElement, 'color', 'white');
      this.setNextBackground(layoutInstance);
    }
    super.handleStateChange(nextState);
  }

  // Update layout background
  public setNextBackground(layout: DropdownLayoutInterface) {
    if (this.isOpened(this.dropdown)) {
      const colorIndex = this.counter % 3;
      const color = this.backgroundColors[colorIndex];
      this.renderer.setStyle(layout.element.nativeElement, 'background-color', color);
      this.counter += 1;
    }
  }
}


````

> Tip: Use `BaseShape` for prototyping. This class already implements `HasInjectorInterface & HasLifecircleHooksInterface` interfaces


Notes:
 
  - `handleStateChange` method will be called for each dropdown state changes.
  - `nextState: StateInterface<DropdownStateInterface>` contains current dropdown state and latest state changes.
  See `StateInterface` for details.
  - `this.isOpened(this.dropdown)` checks that current dropdown is open.
  - `if (layout && layout.currentValue)` checks layout mounted changes. On this step component has access to current mounted layout and can populate it with specific styles.
  - `super.handleStateChange(nextState);` call `changeDetector.markForCheck();` so you don't need additional checks with `ChangeDetectionStrategy.OnPush`.
  - `public get layout(): DropdownLayoutInterface` alias for current layout from state
  
  
````typescript

import { SimpleChanges } from '@angular/core';

export interface StateInterface<TModel> {

  state: TModel;

  changes: SimpleChanges;

}
````

Usage:

Inside dropdown:


````html
<ar-dropdown-focusable>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
      <story-dropdown-custom-ui>Click to change color</story-dropdown-custom-ui>
    </div>
  </ng-template>
</ar-dropdown-focusable>
````

Inside content:


````html
<ar-dropdown-focusable>
  <story-dropdown-custom-ui>Click to change color</story-dropdown-custom-ui>
  <ng-template>
    <div>
      Another dropdown
    </div>
  </ng-template>
</ar-dropdown-focusable>
````


