# Bugfix

## Dropdown Focusable

### Prevent dropdown open on child click 

````typescript
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'story-prevent-focus',
  template: `
    <ar-dropdown-focusable>
      <input placeloder="Input inside content" />
      <span>Text inside content</span>
      <button (click)="handleFocus($event)"
              (mousedown)="handleFocus($event)"
      >Should prevent open events!</button>
      <ng-template>
        Dropdown content
      </ng-template>
    </ar-dropdown-focusable>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PreventFocusComponent {

  handleFocus(event: Event) {
    event.stopPropagation();
    return false;
  }

}
````

To prevent dropdown open handle `click` and `mousedown` events for needed element. 
