#@argon/dropdown

 - [Base Usage](./BaseUsage.md)
 - [Menu](./Menu.md)
 
 
##Components
 
###`<ar-dropdown>` `<ar-dropdown-focusable>` `DropdownDirective`
|Property |Default value|Description
|---      |---          |---
|`@Input() portal: Portal<any>`| `undefined`| Custom portal component to render inside dropdown
|`@Input() layoutType: string`| `undefined`| Layout type. Key to pick up one of layout from `ARGON_DROPDOWN_LAYOUT_PROVIDER`
|`@Input() disabled: boolean`|`undefined`| Disable dropdown flag
|`@Input() tabIndex`|`0`| Html tabindex attribute value
|`@Input() autosize: boolean`|`false`| Flag for sizing dropdown layout. Will update layout width dropdown width.
|`@Output() changeState = new EventEmitter<boolean>()`|  | Notify about dropdown open/close event. Will trigger after dropdown render.   
  
### `<ar-dropdown-hover>`

Has the same basic props as `DropdownDirective`

|Property |Default value|Description
|---      |---          |---
|`@Input() enterDebounce: number`| `250`| Timeout to show dropdown
|`@Input() leaveDebounce: number`| `100`| Timeout to hide dropdown

### `<ar-dropdown-menu>`

Has no properties

### `<ar-dropdown-menu-item>`

|Property |Default value|Description
|---      |---          |---
|`@Input() disabled: boolean`| `false`| Disabled flag
|`@Input() selected: boolean`| `false`| Selected flag
|`@Input() closeOnSelect: boolean`| `false`| Will close dropdown on item click if `true`
|`@Input() limited: boolean`| `false`| Limit menu item width
|`@Output() click = new EventEmitter<void>()` | | Click handler. Will be triggered with keyboard `Enter` and `Space` keypress too. 

## Directives

###`[arDropdownClose]` `[arDropdownMobile]`

Have no active properties

## Interfaces

### `DropdownInterface`

Internal interface to determinate dropdown UI

````typescript
import { ElementRef, ViewContainerRef } from '@angular/core';
import { DropdownConfigModel } from '..';
import { Portal } from '@angular/cdk/portal';

export interface DropdownInterface {
  
  // HTML element link
  element: ElementRef<HTMLElement>;
  
  // Dropdown view container 
  viewContainerRef: ViewContainerRef;

  // Dropdown config
  config: DropdownConfigModel;

  // Dropdown content to render
  content: Portal<any>;

  // Type of dropdown layout
  layoutType: string;

  // Backdrop usage flag
  hasBackdrop: boolean;

}
````


### `DropdownLayoutInterface`

Interface to determinate dropdown layout

````typescript
import { Portal } from '@angular/cdk/portal';
import { ElementRef } from '@angular/core';


export interface DropdownLayoutInterface {

  // Dropdown content
  content: Portal<any>;

  // Layout HTML element
  element: ElementRef<HTMLElement>;

}
````


### `DropdownStateExtensionInterface`

Result of `DropdownStateMixin`

````typescript
import { ChangeDetectorRef } from '@angular/core';
import { StateInterface } from '@argon/tools';
import { DropdownStateService } from '..';
import { DropdownStateInterface } from '..';
import { DropdownInterface } from '..';


export interface DropdownStateExtensionInterface {

  dropdownStateService: DropdownStateService;

  changeDetector: ChangeDetectorRef;

  ngOnInit(): void;

  ngOnDestroy(): void;

  // check is dropdown opened
  isOpened(dropdown: DropdownInterface): boolean;

  // handle dropdown state changes
  handleStateChange(nextState: StateInterface<DropdownStateInterface>): void;

}

````


### `DropdownStateInterface`

Dropdown internal state

````typescript
import { ComponentRef } from '@angular/core';
import { DropdownInterface } from '..';
import { DropdownLayoutInterface } from '..';

export interface DropdownStateInterface {
  
  // Current view type (true for mobile device view)
  isFullScreen: boolean;

  // Current dropdown
  dropdown: DropdownInterface;

  // Current dropdown layout
  layout: ComponentRef<DropdownLayoutInterface>;

  // Notify about dropdown present at the real DOM. Will trigger with portal attach(detach) events.
  isOpened: boolean;

}

````


## Models

### `DropdownConfigModel`

Model with dropdown config

````typescript
import { ConnectedPosition } from '@angular/cdk/overlay';

export class DropdownConfigModel {
  
  // Overlay `withPush` flag
  public withPush = true;
  
  // Overlay `growAfterOpen`
  public growAfterOpen = false;
  
  // Overlay `viewPortMargin`
  public viewPortMargin = 10;

  // Positions for set up dropdown inside overlay
  public elementPositions: Array<ConnectedPosition>

}

````


### `DropdownLayoutModel`

Model for specify dropdown layout. This model can be provided with `ARGON_DROPDOWN_LAYOUT_PROVIDER`. Package accept multiple layouts. 

````typescript
import { DropdownLayoutInterface } from '..';
import { Type } from '@angular/core';

export class DropdownLayoutModel {

  // Layout uniq key
  public type: string;
  
  // Layout component. Don't forget include this component with `entryComponents` property.
  public component: Type<DropdownLayoutInterface>;

}
````

## Providers

### `ARGON_DROPDOWN_CONFIG_PROVIDER`

> ``new InjectionToken<DropdownConfigModel>('ARGON_DROPDOWN_CONFIG_PROVIDER')``

Token for dropdown config.

### `ARGON_DROPDOWN_LAYOUT_PROVIDER`

> ``new InjectionToken<DropdownLayoutModel>('ARGON_DROPDOWN_LAYOUT_PROVIDER')``

Token for provide dropdown layout. Important: this token must be provided with `multi: true` flag. `argon-default` and `argon-menu` types are provide by default.

### `ARGON_DROPDOWN_DEFAULT_LAYOUT_PROVIDER`

> ``new InjectionToken<string>('ARGON_DROPDOWN_DEFAULT_LAYOUT_PROVIDER')``

Token for default layout type. Will provide `type` property from `DropdownLayoutModel`. Dropdown will pick up layout by this key if `layoutType` will not defined. `argon-default` provided by default.


