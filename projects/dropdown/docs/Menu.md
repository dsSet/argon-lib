#Dropdown Menu usage

##Base example

````html
<ar-dropdown layoutType="argon-menu">
  <div class="btn btn-primary">Click to open</div>
  <ng-template>
    <ar-dropdown-menu>
      <ar-dropdown-menu-item [disabled]="true">Menu Item 1 (disabled)</ar-dropdown-menu-item>
      <ar-dropdown-menu-item [closeOnSelect]="false">Menu Item 2 (should not be closed on click)</ar-dropdown-menu-item>
      <ar-dropdown-menu-item [selected]="true">Menu Item 3 (should be selected)</ar-dropdown-menu-item>
      <ar-dropdown-menu-item>Menu Item 4</ar-dropdown-menu-item>
      <ar-dropdown-menu-item>Menu Item 5</ar-dropdown-menu-item>
    </ar-dropdown-menu>
  </ng-template>
</ar-dropdown>
````

##limit menu item width

````html
<ar-dropdown-focusable layoutType="argon-menu">
  <div class="btn btn-primary">Focus to open</div>
  <ng-template>
    <ar-dropdown-menu>
      <ar-dropdown-menu-item [limited]="true" [arTextCut]="message">{{ message }}</ar-dropdown-menu-item>
    </ar-dropdown-menu>
  </ng-template>
</ar-dropdown-focusable>
````

Use `limited` property to set menu item `max-width` and `[arTextCut]` directive from `@argon/style-kit` package to add text overflow and title.



