#Additional UI

##`[arDropdownClose]` directive

Bind dropdown close action on click to any html element inside dropdown.

````html
<ar-dropdown-focusable>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Dropdown content
      <div arDropdownClose>Click to close dropdown</div>
    </div>
  </ng-template>
</ar-dropdown-focusable>
````

##`*arDropdownMobile` directive

Toggle content visibility for the full screen mode.

````html
<ar-dropdown>
  <div class="btn btn-primary">Dropdown Click trigger</div>
  <ng-template>
    <div>
      Resize screen to mobile to see hidden content
      <div *arDropdownMobile>This content will be visible for mobile mode only</div>
    </div>
  </ng-template>
</ar-dropdown>
````



