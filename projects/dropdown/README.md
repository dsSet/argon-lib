# @argon/dropdown package

## Versions

- `Angular`: ^15.2.9
- `Rxjs`: ~6.6.3  
- `Cdk`: ^15.2.9

## Install

> npm i --save @argon/dropdown 

## Documentation

[@argon/dropdown documentation](./docs/index.md)

## Package content

| Source | Content |
|--------|---------|
|`@argon/dropdown/assets` | static assets folder |
|`@argon/dropdown/style` | styles folder |
|`@argon/dropdown/stories` | storybook folder |
|`@argon/dropdown/docs` | documentation folder |

## Styling components

Package component are supporting Bootstrap 4 styles. Package also contains custom styles according by BEM technology.

> Custom styles should be included globally.

Custom styles can be applied in 3 ways.

### Include styles as is

Include `index.scss` file to your global app styles

```scss
@import "~@argon/dropdown/style/index.scss";
```  

### Override default variables

```scss
@import "~@argon/dropdown/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/dropdown/style/index.scss";
```

### Use mixins optional

```scss
@import "~@argon/dropdown/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/dropdown/style/[package-mixin].scss";

.ar-component-to-styled {
  @include any-package-mixin();
  color: red;
}

```  

Otherwise you can skip components default styles and implement new ones by yourself. 
