# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@2.0.2...@argon/dropdown@2.0.3) (2023-10-18)


### Bug Fixes

* **dropdown:** fix scss imports ([e7e9394](https://bitbucket.org/dsSet/argon-lib/commits/e7e939429f9c37c97b97ff810d5d20ec33a9e496))






## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@2.0.1...@argon/dropdown@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/dropdown





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@1.0.4...@argon/dropdown@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/dropdown






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@1.0.4...@argon/dropdown@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/dropdown






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@1.0.3...@argon/dropdown@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/dropdown





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@1.0.2...@argon/dropdown@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/dropdown





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@1.0.1...@argon/dropdown@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/dropdown






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.7.1...@argon/dropdown@1.0.1) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.7.1...@argon/dropdown@1.0.0) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))






## [0.7.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.7.0...@argon/dropdown@0.7.1) (2020-08-04)

**Note:** Version bump only for package @argon/dropdown






# [0.7.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.14...@argon/dropdown@0.7.0) (2020-06-10)


### Bug Fixes

* **dropdown:** fix hover events for mobile mode ([d1935dc](https://bitbucket.org/dsSet/argon-lib/commits/d1935dc))
* **dropdown:** fix overlay error on toggle between several dropdowns ([cd6731f](https://bitbucket.org/dsSet/argon-lib/commits/cd6731f))


### Features

* **dropdown:** add param to prevent mobile mode for dropdown ([f401b8d](https://bitbucket.org/dsSet/argon-lib/commits/f401b8d))





## [0.6.14](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.13...@argon/dropdown@0.6.14) (2020-06-04)


### Bug Fixes

* **dropdown:** fix dropdown close event on parent container scroll ([9cf0254](https://bitbucket.org/dsSet/argon-lib/commits/9cf0254))
* **dropdown:** update remove dropdown function, fix unit test ([b5c6f29](https://bitbucket.org/dsSet/argon-lib/commits/b5c6f29))






## [0.6.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.12...@argon/dropdown@0.6.13) (2020-05-21)

**Note:** Version bump only for package @argon/dropdown






## [0.6.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.11...@argon/dropdown@0.6.12) (2020-04-18)

**Note:** Version bump only for package @argon/dropdown





## [0.6.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.10...@argon/dropdown@0.6.11) (2020-04-18)


### Bug Fixes

* **dropdown:** fix host binding props ([20ec760](https://bitbucket.org/dsSet/argon-lib/commits/20ec760))






## [0.6.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.9...@argon/dropdown@0.6.10) (2020-03-24)

**Note:** Version bump only for package @argon/dropdown





## [0.6.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.8...@argon/dropdown@0.6.9) (2020-03-24)

**Note:** Version bump only for package @argon/dropdown






## [0.6.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.7...@argon/dropdown@0.6.8) (2020-03-18)

**Note:** Version bump only for package @argon/dropdown





## [0.6.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.6...@argon/dropdown@0.6.7) (2020-02-14)

**Note:** Version bump only for package @argon/dropdown






## [0.6.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.5...@argon/dropdown@0.6.6) (2019-11-29)


### Bug Fixes

* **dropdown:** fix hooks call ([d0afca2](https://bitbucket.org/dsSet/argon-lib/commits/d0afca2))





## [0.6.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.4...@argon/dropdown@0.6.5) (2019-11-22)


### Bug Fixes

* **dropdown:** fix lifecircle hooks call ([715a615](https://bitbucket.org/dsSet/argon-lib/commits/715a615))





## [0.6.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.3...@argon/dropdown@0.6.4) (2019-11-22)

**Note:** Version bump only for package @argon/dropdown





## [0.6.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.2...@argon/dropdown@0.6.3) (2019-11-22)


### Bug Fixes

* **dropdown:** fix focusable dropdown destory ([28f7188](https://bitbucket.org/dsSet/argon-lib/commits/28f7188))





## [0.6.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.1...@argon/dropdown@0.6.2) (2019-11-22)


### Bug Fixes

* **dropdown:** fix hover dropdown destroy ([c5488e0](https://bitbucket.org/dsSet/argon-lib/commits/c5488e0))






## [0.6.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.6.0...@argon/dropdown@0.6.1) (2019-10-31)


### Bug Fixes

* **dropdown:** fix stories import ([a519849](https://bitbucket.org/dsSet/argon-lib/commits/a519849))





# [0.6.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.5.7...@argon/dropdown@0.6.0) (2019-10-28)


### Bug Fixes

* **dropdown:** add exports, fix build ([3a25267](https://bitbucket.org/dsSet/argon-lib/commits/3a25267))
* **dropdown:** minor fixes ([f44d851](https://bitbucket.org/dsSet/argon-lib/commits/f44d851))
* **dropdown:** prevent focusable dropdown open ([2d66f39](https://bitbucket.org/dsSet/argon-lib/commits/2d66f39))


### Features

* **dropdown:** refactoring dropdown engine ([ff9fd7f](https://bitbucket.org/dsSet/argon-lib/commits/ff9fd7f))
* **dropdown:** update dropdown menu with a11yList mixin ([9b4a2a3](https://bitbucket.org/dsSet/argon-lib/commits/9b4a2a3))





## [0.5.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.5.6...@argon/dropdown@0.5.7) (2019-10-11)

**Note:** Version bump only for package @argon/dropdown





## [0.5.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.5.5...@argon/dropdown@0.5.6) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





## [0.5.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.5.4...@argon/dropdown@0.5.5) (2019-10-08)


### Bug Fixes

* 🐛 fix droipdown disabled property binding ([1e838ad](https://bitbucket.org/dsSet/argon-lib/commits/1e838ad))
* **dropdown:** add exports, fix dropdown state change event emitter ([cd8f103](https://bitbucket.org/dsSet/argon-lib/commits/cd8f103))





## [0.5.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.5.3...@argon/dropdown@0.5.4) (2019-09-21)

**Note:** Version bump only for package @argon/dropdown





## [0.5.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.5.2...@argon/dropdown@0.5.3) (2019-09-17)

**Note:** Version bump only for package @argon/dropdown





## [0.5.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.5.1...@argon/dropdown@0.5.2) (2019-09-13)

**Note:** Version bump only for package @argon/dropdown






## [0.5.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.5.0...@argon/dropdown@0.5.1) (2019-09-07)

**Note:** Version bump only for package @argon/dropdown





# [0.5.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.4.6...@argon/dropdown@0.5.0) (2019-09-06)


### Features

* **dropdown:** replace dropdown tabindex with mixin ([3871b20](https://bitbucket.org/dsSet/argon-lib/commits/3871b20))





## [0.4.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.4.5...@argon/dropdown@0.4.6) (2019-08-11)


### Bug Fixes

* **dropdown:** fix package json ([c00509c](https://bitbucket.org/dsSet/argon-lib/commits/c00509c))





## [0.4.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.4.4...@argon/dropdown@0.4.5) (2019-08-11)

**Note:** Version bump only for package @argon/dropdown





## [0.4.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.4.3...@argon/dropdown@0.4.4) (2019-08-10)

**Note:** Version bump only for package @argon/dropdown





## [0.4.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.4.2...@argon/dropdown@0.4.3) (2019-08-10)

**Note:** Version bump only for package @argon/dropdown





## [0.4.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.4.1...@argon/dropdown@0.4.2) (2019-07-28)

**Note:** Version bump only for package @argon/dropdown





## [0.4.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.4.0...@argon/dropdown@0.4.1) (2019-07-24)

**Note:** Version bump only for package @argon/dropdown





# [0.4.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.3.4...@argon/dropdown@0.4.0) (2019-07-21)


### Features

* **dropdown:** add @Unsubscribeable decorators ([ecb720e](https://bitbucket.org/dsSet/argon-lib/commits/ecb720e))





## [0.3.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.3.3...@argon/dropdown@0.3.4) (2019-07-15)


### Bug Fixes

* **main:** revert incorrect imports ([d30bb89](https://bitbucket.org/dsSet/argon-lib/commits/d30bb89))





## [0.3.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.3.2...@argon/dropdown@0.3.3) (2019-07-14)

**Note:** Version bump only for package @argon/dropdown





## [0.3.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.3.1...@argon/dropdown@0.3.2) (2019-07-14)


### Bug Fixes

* **dropdown:** mock import ([884e049](https://bitbucket.org/dsSet/argon-lib/commits/884e049))





## [0.3.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.3.0...@argon/dropdown@0.3.1) (2019-07-14)

**Note:** Version bump only for package @argon/dropdown





# [0.3.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.2.0...@argon/dropdown@0.3.0) (2019-07-14)


### Features

* **@argon/dropdown:** add keyboard events for dropdown menu ([6ae0df5](https://bitbucket.org/dsSet/argon-lib/commits/6ae0df5))
* **dropdown:** add close, mobile directives. add actions container ([519810f](https://bitbucket.org/dsSet/argon-lib/commits/519810f))
* **dropdown:** add dropdown service story. add reposition on change screen size ([07c34ca](https://bitbucket.org/dsSet/argon-lib/commits/07c34ca))
* **dropdown:** add keyboard search ([33a03c7](https://bitbucket.org/dsSet/argon-lib/commits/33a03c7))
* **dropdown:** add scrollbar directive to dropdown menu ([0b8752a](https://bitbucket.org/dsSet/argon-lib/commits/0b8752a))
* **dropdown:** improve menu rendering ([3b35799](https://bitbucket.org/dsSet/argon-lib/commits/3b35799))
* **dropdown:** move dropdown close keyboard event to service ([54fc117](https://bitbucket.org/dsSet/argon-lib/commits/54fc117))






# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.1.2...@argon/dropdown@0.2.0) (2019-06-30)


### Features

* **dropdown:** add keyboard support ([bae71d3](https://bitbucket.org/dsSet/argon-lib/commits/bae71d3))
* **dropdown:** add menu item component ([3b35fee](https://bitbucket.org/dsSet/argon-lib/commits/3b35fee))
* **dropdown:** add positions for mobile and tablet view ([17dba27](https://bitbucket.org/dsSet/argon-lib/commits/17dba27))
* **dropdown:** change dropdown animation, add dropdown menu component ([c14cf7f](https://bitbucket.org/dsSet/argon-lib/commits/c14cf7f))
* **media:** add media size module ([9ed7d3b](https://bitbucket.org/dsSet/argon-lib/commits/9ed7d3b))





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.1.1...@argon/dropdown@0.1.2) (2019-06-26)

**Note:** Version bump only for package @argon/dropdown





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/dropdown@0.1.0...@argon/dropdown@0.1.1) (2019-06-26)

**Note:** Version bump only for package @argon/dropdown





# 0.1.0 (2019-06-26)


### Features

* **dropdown:** add dropdown module ([ea0da19](https://bitbucket.org/dsSet/argon-lib/commits/ea0da19))
