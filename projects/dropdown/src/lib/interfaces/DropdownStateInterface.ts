import { DropdownInterface } from './DropdownInterface';
import { ComponentRef } from '@angular/core';
import { DropdownLayoutInterface } from './DropdownLayoutInterface';


export interface DropdownStateInterface {

  isFullScreen: boolean;

  dropdown: DropdownInterface;

  layout: ComponentRef<DropdownLayoutInterface>;

  isOpened: boolean;

}
