import { ChangeDetectorRef } from '@angular/core';
import { StateInterface } from '@argon/tools';
import { DropdownStateService } from '../services/DropdownStateService';
import { DropdownStateInterface } from './DropdownStateInterface';
import { DropdownInterface } from './DropdownInterface';


export interface DropdownStateExtensionInterface {

  dropdownStateService: DropdownStateService;

  changeDetector: ChangeDetectorRef;

  ngOnInit(): void;

  ngOnDestroy(): void;

  isOpened(dropdown: DropdownInterface): boolean;

  handleStateChange(nextState: StateInterface<DropdownStateInterface>): void;

}
