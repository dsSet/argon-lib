import { ElementRef, ViewContainerRef } from '@angular/core';
import { DropdownConfigModel } from '../models/DropdownConfigModel';
import { Portal } from '@angular/cdk/portal';

export interface DropdownInterface {

  element: ElementRef<HTMLElement>;

  viewContainerRef: ViewContainerRef;

  config: DropdownConfigModel;

  content: Portal<any>;

  layoutType: string;

  hasBackdrop: boolean;

  preventMobileOverlay?: boolean;

}
