import { Portal } from '@angular/cdk/portal';
import { ElementRef } from '@angular/core';


export interface DropdownLayoutInterface {

  content: Portal<any>;

  element: ElementRef<HTMLElement>;

}
