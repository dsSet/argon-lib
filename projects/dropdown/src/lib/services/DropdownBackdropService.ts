import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { DropdownInterface } from '../interfaces/DropdownInterface';

@Injectable({
  providedIn: 'root'
})
export class DropdownBackdropService {

  static BACKDROP_CLASS = 'ar-dropdown-backdrop';

  static BACKDROP_HIDDEN_CLASS = 'ar-dropdown-backdrop--hidden';

  public readonly renderer: Renderer2;

  constructor(
    protected rendererFactory: RendererFactory2
  ) {
    this.renderer = this.rendererFactory.createRenderer(null, null);
  }

  public toggleBackdropVisibility(isVisible: boolean, backdrop: HTMLElement | null, dropdown: DropdownInterface | null) {
    if (!backdrop || !dropdown) {
      return;
    }

    if ((isVisible && !dropdown.preventMobileOverlay) || dropdown.hasBackdrop) {
      this.renderer.removeClass(backdrop, DropdownBackdropService.BACKDROP_HIDDEN_CLASS);
    } else {
      this.renderer.addClass(backdrop, DropdownBackdropService.BACKDROP_HIDDEN_CLASS);
    }
  }

}
