import { ComponentRef, Injectable, OnDestroy } from '@angular/core';
import { StateSubject, Unsubscribable } from '@argon/tools';
import { DropdownStateInterface } from '../interfaces/DropdownStateInterface';
import { BreakpointService } from '@argon/media';
import { Subscription } from 'rxjs';
import { DropdownInterface } from '../interfaces/DropdownInterface';
import { DropdownLayoutInterface } from '../interfaces/DropdownLayoutInterface';


@Injectable({
  providedIn: 'root'
})
export class DropdownStateService implements OnDestroy {

  static getInitialState(): DropdownStateInterface {
    return {
      dropdown: null,
      layout: null,
      isFullScreen: false,
      isOpened: false
    };
  }

  public readonly stateSubject: StateSubject<DropdownStateInterface>;

  public get state(): DropdownStateInterface { return this.stateSubject.state; }

  public get hasDropdown(): boolean { return Boolean(this.state.dropdown); }

  private breakpointSubscription: Subscription;

  constructor(
    protected breakpointsService: BreakpointService,
  ) {
    this.stateSubject = new StateSubject(DropdownStateService.getInitialState());
    this.breakpointSubscription = this.breakpointsService.isPhone.subscribe(this.handleScreenSizeChange);
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  public isOpened(dropdown: DropdownInterface): boolean {
    return this.state.dropdown === dropdown;
  }

  public closeDropdown() {
    this.stateSubject.setState({ dropdown: null, layout: null });
  }

  public openDropdown(dropdown: DropdownInterface) {
    this.stateSubject.setState({ dropdown });
  }

  public setIsOpened(isOpened: boolean) {
    if (!isOpened && this.hasDropdown) {
      this.stateSubject.setState({ isOpened, dropdown: null });
    } else {
      this.stateSubject.setState({ isOpened });
    }
  }

  public setLayout(layout: ComponentRef<DropdownLayoutInterface>) {
    this.stateSubject.setState({ layout });
  }

  private handleScreenSizeChange = (isFullScreen: boolean) => {
    this.stateSubject.setState({ isFullScreen });
  }

}
