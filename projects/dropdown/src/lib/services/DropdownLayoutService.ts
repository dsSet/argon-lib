import find from 'lodash/find';
import first from 'lodash/first';
import { Inject, Injectable } from '@angular/core';
import { ARGON_DROPDOWN_DEFAULT_LAYOUT_PROVIDER, ARGON_DROPDOWN_LAYOUT_PROVIDER } from '../providers/DropdownLayoutProvider';
import { DropdownLayoutModel } from '../models/DropdownLayoutModel';

@Injectable({
  providedIn: 'root'
})
export class DropdownLayoutService {

  public get defaultLayoutModel(): DropdownLayoutModel | null { return this.findLayout(this.defaultLayout); }

  public get firstLayoutModel(): DropdownLayoutModel | null { return first(this.layouts) || null; }

  constructor(
    @Inject(ARGON_DROPDOWN_LAYOUT_PROVIDER) private layouts: Array<DropdownLayoutModel>,
    @Inject(ARGON_DROPDOWN_DEFAULT_LAYOUT_PROVIDER) private defaultLayout: string
  ) { }

  public getDropdownLayout(type: string): DropdownLayoutModel | null {
    return this.findLayout(type) || this.defaultLayoutModel || this.firstLayoutModel;
  }

  public findLayout(type: string): DropdownLayoutModel | null {
    return find(this.layouts, { type }) || null;
  }

}
