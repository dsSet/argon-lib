import { DropdownStateService } from './DropdownStateService';
import { TestBed } from '@angular/core/testing';
import { BreakpointService } from '@argon/media';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownModule } from '../DropdownModule';
import { SimpleChange } from '@angular/core';


describe('DropdownStateService', () => {

  let service: DropdownStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        DropdownModule
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(DropdownStateService) as any;
  });

  it('getInitialState should return initial state', () => {
    expect(DropdownStateService.getInitialState()).toEqual({
      dropdown: null,
      layout: null,
      isFullScreen: false,
      isOpened: false
    });
  });

  it('should have defaults', () => {
    expect(service.stateSubject.value).toEqual({
      state: DropdownStateService.getInitialState(),
      changes: { isFullScreen: new SimpleChange(false, false, false) }
    });
    expect(service.state).toEqual(DropdownStateService.getInitialState());
  });

  it('hasDropdown should check dropdown at the state', () => {
    expect(service.hasDropdown).toBeFalsy();
    service.openDropdown({ data: 123 } as any);
    expect(service.hasDropdown).toBeTruthy();
  });

  it('isOpened should compare dropdown', () => {
    const dropdown1 = { data: 1 } as any;
    const dropdown2 = { data: 2 } as any;
    service.openDropdown(dropdown1);
    expect(service.isOpened(dropdown1)).toEqual(true);
    expect(service.isOpened(dropdown2)).toEqual(false);
  });

  it('closeDropdown should remove dropdown and layout from state', () => {
    const dropdown = { data: 1 } as any;
    const layout = { data: 'layout' } as any;
    service.stateSubject.setState({ dropdown, layout });
    expect(service.stateSubject.value.state.dropdown).toEqual(dropdown);
    expect(service.stateSubject.value.state.layout).toEqual(layout);
    service.closeDropdown();
    expect(service.stateSubject.value.state.dropdown).toEqual(null);
    expect(service.stateSubject.value.state.layout).toEqual(null);
  });

  it('openDropdown should set dropdown to the state', () => {
    const dropdown = { data: 1 } as any;
    service.openDropdown(dropdown);
    expect(service.stateSubject.value.state.dropdown).toEqual(dropdown);
  });

  it('setIsOpened should update isOpened state', () => {
    service.setIsOpened(true);
    expect(service.stateSubject.value.state.isOpened).toEqual(true);
    service.setIsOpened(false);
    expect(service.stateSubject.value.state.isOpened).toEqual(false);
  });

  it('setLayout should update layout state', () => {
    const layout = { data: 'layout' } as any;
    service.setLayout(layout);
    expect(service.stateSubject.value.state.layout).toEqual(layout);
    service.setLayout(null);
    expect(service.stateSubject.value.state.layout).toEqual(null);
  });

  it('breakpointsService isPhone subscription should update isFullScreen state', () => {
    const breakpointsService: BreakpointService = TestBed.inject(BreakpointService) as any;
    breakpointsService.isPhone.next(true);
    expect(service.stateSubject.value.state.isFullScreen).toEqual(true);
    breakpointsService.isPhone.next(false);
    expect(service.stateSubject.value.state.isFullScreen).toEqual(false);
  });
});
