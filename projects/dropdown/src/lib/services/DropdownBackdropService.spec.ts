import { DropdownBackdropService } from './DropdownBackdropService';
import { TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownModule } from '../DropdownModule';
import { DropdownInterface } from '../interfaces/DropdownInterface';


describe('DropdownBackdropService', () => {

  let service: DropdownBackdropService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        DropdownModule
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(DropdownBackdropService) as any;
  });

  it('toggleBackdropVisibility should remove BACKDROP_HIDDEN_CLASS to backdrop if visible', () => {
    const backdrop = document.createElement('div');
    spyOn(service.renderer, 'removeClass');
    const dropdownLike: Partial<DropdownInterface> = {
      hasBackdrop: true
    };
    service.toggleBackdropVisibility(true, backdrop, dropdownLike as DropdownInterface);
    expect(service.renderer.removeClass).toHaveBeenCalledWith(backdrop, DropdownBackdropService.BACKDROP_HIDDEN_CLASS);
  });

  it('toggleBackdropVisibility should add BACKDROP_HIDDEN_CLASS to backdrop if hidden', () => {
    const backdrop = document.createElement('div');
    const dropdownLike: Partial<DropdownInterface> = {
      hasBackdrop: false
    };
    spyOn(service.renderer, 'addClass');
    service.toggleBackdropVisibility(false, backdrop, dropdownLike as DropdownInterface);
    expect(service.renderer.addClass).toHaveBeenCalledWith(backdrop, DropdownBackdropService.BACKDROP_HIDDEN_CLASS);
  });

  it('toggleBackdropVisibility should not call updates without backdrop', () => {
    spyOn(service.renderer, 'addClass');
    spyOn(service.renderer, 'removeClass');
    service.toggleBackdropVisibility(false, null, null);
    service.toggleBackdropVisibility(true, null, null);
    expect(service.renderer.addClass).not.toHaveBeenCalled();
    expect(service.renderer.removeClass).not.toHaveBeenCalled();
  });
});
