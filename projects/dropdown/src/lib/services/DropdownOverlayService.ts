import { Injectable, OnDestroy } from '@angular/core';
import { Overlay, OverlayConfig, OverlayRef, PositionStrategy } from '@angular/cdk/overlay';
import { Subscription, merge } from 'rxjs';
import { map } from 'rxjs/operators';
import { Unsubscribable, StateInterface } from '@argon/tools';
import { KeyboardService, KeyCodesEnum } from '@argon/keyboard';
import { DropdownStateInterface } from '../interfaces/DropdownStateInterface';
import { DropdownInterface } from '../interfaces/DropdownInterface';
import { DropdownStateService } from './DropdownStateService';
import { DropdownLayoutService } from './DropdownLayoutService';
import { DropdownBackdropService } from './DropdownBackdropService';
import { ComponentPortal } from '@angular/cdk/portal';


@Injectable({
  providedIn: 'root'
})
export class DropdownOverlayService implements OnDestroy {

  public readonly overlayRef: OverlayRef;

  private backdropSubscription: Subscription;

  private keyboardCloseSubscription: Subscription;

  private stateSubscription: Subscription;

  private stateServiceSubscription: Subscription;

  constructor(
    protected overlay: Overlay,
    protected dropdownLayoutService: DropdownLayoutService,
    protected keyboardService: KeyboardService,
    protected dropdownStateService: DropdownStateService,
    protected dropdownBackdropService: DropdownBackdropService
  ) {
    const config = new OverlayConfig({
      hasBackdrop: true,
      backdropClass: DropdownBackdropService.BACKDROP_CLASS,
      panelClass: 'ar-dropdown-pane',
      scrollStrategy: this.overlay.scrollStrategies.close()
    });
    this.overlayRef = this.overlay.create(config);

    this.backdropSubscription = this.overlayRef.backdropClick().subscribe(() => { this.dropdownStateService.closeDropdown(); });
    this.keyboardCloseSubscription = this.keyboardService
      .createFromSource([KeyCodesEnum.Esc], this.overlayRef.keydownEvents())
      .subscribe(() => { this.dropdownStateService.closeDropdown(); });
    this.stateSubscription = merge(
      this.overlayRef.attachments().pipe(map(() => true )),
      this.overlayRef.detachments().pipe(map(() => false ))
    ).subscribe((isOpened: boolean) => { this.dropdownStateService.setIsOpened(isOpened); });
    this.stateServiceSubscription = this.dropdownStateService.stateSubject.subscribe(this.handleStateChange);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    this.overlayRef.dispose();
  }

  public handleStateChange = (nextState: StateInterface<DropdownStateInterface>): void => {
    const { changes, state } = nextState;
    const { isFullScreen, dropdown, isOpened } = changes;

    if (dropdown && !dropdown.currentValue) {
      this.removeDropdown();
    } else if (dropdown && dropdown.currentValue !== dropdown.previousValue) {
      this.createDropdown(dropdown.currentValue);
    } else if (
      isFullScreen && isFullScreen.previousValue !== isFullScreen.currentValue && !(state.dropdown && state.dropdown.preventMobileOverlay)
    ) {
      this.handleChangeScreenSize();
    }

    if (isOpened && isOpened.currentValue && !isOpened.previousValue) {
      this.dropdownBackdropService.toggleBackdropVisibility(
        state.isFullScreen,
        this.overlayRef.backdropElement,
        state.dropdown
      );
    }
  }

  public removeDropdown() {
    if (this.overlayRef.hasAttached()) {
      this.overlayRef.detach();
    }
  }

  public createDropdown(dropdown: DropdownInterface) {
    this.removeDropdown();
    const layout = this.dropdownLayoutService.getDropdownLayout(dropdown.layoutType);

    if (!layout) {
      return;
    }

    const positionStrategy = this.createPositionStrategy(dropdown);
    this.overlayRef.updatePositionStrategy(positionStrategy);

    const portal = new ComponentPortal(layout.component, dropdown.viewContainerRef);

    const mountedLayout = this.overlayRef.attach(portal);
    mountedLayout.instance.content = dropdown.content;
    mountedLayout.changeDetectorRef.markForCheck();
    this.dropdownStateService.setLayout(mountedLayout);
  }

  private handleChangeScreenSize() {
    if (this.overlayRef && this.overlayRef.hasAttached() && this.dropdownStateService.hasDropdown) {
      const newPosition = this.createPositionStrategy(this.dropdownStateService.state.dropdown);
      this.overlayRef.updatePositionStrategy(newPosition);
      this.overlayRef.updatePosition();
    }
  }

  private createPositionStrategy(dropdown: DropdownInterface): PositionStrategy {
    return this.dropdownStateService.state.isFullScreen && !dropdown.preventMobileOverlay
      ? this.createGlobalPosition()
      : this.createElementPosition(dropdown);
  }

  private createElementPosition(dropdown: DropdownInterface): PositionStrategy {
    const strategy = this.overlay.position().flexibleConnectedTo(dropdown.element);
    strategy.withPositions(dropdown.config.elementPositions);
    strategy.withPush(dropdown.config.withPush);
    strategy.withGrowAfterOpen(dropdown.config.growAfterOpen);
    strategy.withViewportMargin(dropdown.config.viewPortMargin);
    return strategy;
  }

  private createGlobalPosition(): PositionStrategy {
    const strategy = this.overlay.position().global();
    strategy.centerHorizontally();
    strategy.centerVertically();
    return strategy;
  }

}
