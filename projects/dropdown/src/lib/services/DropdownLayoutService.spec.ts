import { DropdownLayoutService } from './DropdownLayoutService';
import { TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownModule } from '../DropdownModule';
import { argonDefaultLayout, argonMenuLayout } from '../providers/DropdownLayoutProvider';


describe('DropdownLayoutService', () => {

  let service: DropdownLayoutService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        DropdownModule
      ]
    });
  });

  beforeEach(() => {
    service = TestBed.inject(DropdownLayoutService) as any;
  });

  it('findLayout should return layout from providers', () => {
    expect(service.findLayout('mock-layout')).toEqual(null);
    expect(service.findLayout('argon-menu')).toEqual(argonMenuLayout);
  });

  it('defaultLayoutModel should equal default layout from providers', () => {
    expect(service.defaultLayoutModel).toEqual(argonDefaultLayout);
  });

  it('firstLayoutModel should be the any first layout from providers', () => {
    expect(service.firstLayoutModel).toBeDefined();
  });

  it('getDropdownLayout should return layout by key', () => {
    expect(service.getDropdownLayout('argon-menu')).toEqual(argonMenuLayout);
    expect(service.getDropdownLayout('mock-layout')).toEqual(service.defaultLayoutModel);
  });
});
