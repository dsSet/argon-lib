import { TestBed, waitForAsync } from '@angular/core/testing';
import { DropdownOverlayService } from './DropdownOverlayService';
import { BreakpointService } from '@argon/media';
import { KeyboardService, KeyCodesEnum } from '@argon/keyboard';
import { CloseScrollStrategy, FlexibleConnectedPositionStrategy, GlobalPositionStrategy, OverlayModule } from '@angular/cdk/overlay';
import { ComponentRef, ElementRef } from '@angular/core';
import { KeyboardServiceMock } from '../../../../../test/KeyboardServiceMock';
import { BreakpointServiceMock } from '../../../../../test/BreakpointServiceMock';
import { DropdownInterface } from '../interfaces/DropdownInterface';
import { ComponentPortal } from '@angular/cdk/portal';
import { defaultConfig } from '../providers/DropdownConfigProvider';
import { DropdownModule } from '../DropdownModule';
import { DropdownLayoutInterface } from '../interfaces/DropdownLayoutInterface';
import createSpy = jasmine.createSpy;
import createSpyObj = jasmine.createSpyObj;
import { DropdownStateService } from './DropdownStateService';

describe('DropdownOverlayService', () => {
  let service: DropdownOverlayService;
  let breakpointService: BreakpointService;
  let keyboardService: KeyboardServiceMock;
  let attachedLayout: ComponentRef<DropdownLayoutInterface>;
  let dropdownStateService: DropdownStateService;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      imports: [
        OverlayModule,
        DropdownModule
      ],
      providers: [
        DropdownOverlayService,
        { provide: KeyboardService, useClass: KeyboardServiceMock },
      ]
    }).compileComponents()
  ));

  beforeEach(() => {
    service = TestBed.inject(DropdownOverlayService) as any;
    breakpointService = TestBed.inject(BreakpointService) as any;
    keyboardService = TestBed.inject(KeyboardService) as any;
    dropdownStateService = TestBed.inject(DropdownStateService) as any;
    attachedLayout = {
      instance: {
        content: null,
        element: new ElementRef<HTMLElement>(document.createElement('div'))
      },
      changeDetectorRef: createSpyObj(['markForCheck'])
    } as any;
  });

  afterEach(() => {
    keyboardService.dispose();
  });

  it('should observer KeyCodesEnum.Esc event', () => {
    expect(keyboardService.createFromSource).toHaveBeenCalledWith([KeyCodesEnum.Esc], service.overlayRef.keydownEvents());
  });

  it('should create and config overlay', () => {
    expect(service.overlayRef).toBeDefined();
    const config = service.overlayRef.getConfig();
    expect(config.backdropClass).toEqual('ar-dropdown-backdrop');
    expect(config.panelClass).toEqual('ar-dropdown-pane');
    expect(config.hasBackdrop).toEqual(true);
    expect(config.scrollStrategy).toEqual(jasmine.any(CloseScrollStrategy));
  });

  it('removeDropdown should close dropdown', () => {
    spyOn(service.overlayRef, 'hasAttached').and.returnValue(true);
    spyOn(service.overlayRef, 'detach');
    service.removeDropdown();
    expect(service.overlayRef.hasAttached).toHaveBeenCalled();
    expect(service.overlayRef.detach).toHaveBeenCalled();
  });

  it('createDropdown should create new dropdown', () => {
    spyOn(service, 'removeDropdown');
    spyOn(service.overlayRef, 'updatePositionStrategy');
    spyOn(service.overlayRef, 'attach').and.returnValue(attachedLayout);

    const dropdown: DropdownInterface = {
      viewContainerRef: null,
      hasBackdrop: false,
      layoutType: 'argon-menu',
      content: new ComponentPortal(null),
      config: defaultConfig,
      element: new ElementRef(document.createElement('div'))
    };

    service.createDropdown(dropdown);

    expect(service.removeDropdown).toHaveBeenCalled();
    expect(service.overlayRef.updatePositionStrategy).toHaveBeenCalled();
    expect(service.overlayRef.attach).toHaveBeenCalled();
  });

  it('keyboardService keydown esc event should close dropdown', () => {
    spyOn(service.overlayRef, 'hasAttached').and.returnValue(true);
    spyOn(service.overlayRef, 'detach');
    keyboardService.createFromSourceSubject.next(new KeyboardEvent('keydown'));

    expect(service.overlayRef.hasAttached).toHaveBeenCalled();
    expect(service.overlayRef.detach).toHaveBeenCalled();
  });

  it('should create dropdown with flexible position for tablet and desktop', () => {
    dropdownStateService.stateSubject.setState({ isFullScreen: false });
    spyOn(service.overlayRef, 'updatePositionStrategy');
    spyOn(service.overlayRef, 'attach').and.returnValue(attachedLayout);
    const dropdown: DropdownInterface = {
      viewContainerRef: null,
      hasBackdrop: false,
      layoutType: 'argon-menu',
      content: new ComponentPortal(null),
      config: defaultConfig,
      element: new ElementRef(document.createElement('div'))
    };

    service.createDropdown(dropdown);
    expect(service.overlayRef.updatePositionStrategy).toHaveBeenCalledWith(jasmine.any(FlexibleConnectedPositionStrategy));

    dropdownStateService.stateSubject.setState({ isFullScreen: true });
    service.createDropdown(dropdown);

    expect(service.overlayRef.updatePositionStrategy).toHaveBeenCalledWith(jasmine.any(GlobalPositionStrategy));
  });

  it('should update position strategy on change screen resolution', () => {
    dropdownStateService.stateSubject.setState({ isFullScreen: false });
    const spy = spyOn(service.overlayRef, 'hasAttached').and.returnValue(false);
    spyOn(service.overlayRef, 'updatePositionStrategy');
    spyOn(service.overlayRef, 'updatePosition');
    spyOn(service.overlayRef, 'attach').and.returnValue(attachedLayout);

    const dropdown: DropdownInterface = {
      viewContainerRef: null,
      hasBackdrop: false,
      layoutType: 'argon-menu',
      content: new ComponentPortal(null),
      config: defaultConfig,
      element: new ElementRef(document.createElement('div'))
    };

    service.createDropdown(dropdown);
    expect(service.overlayRef.updatePositionStrategy).toHaveBeenCalledWith(jasmine.any(FlexibleConnectedPositionStrategy));

    dropdownStateService.stateSubject.setState({ dropdown });
    expect(service.overlayRef.updatePositionStrategy).toHaveBeenCalledWith(jasmine.any(FlexibleConnectedPositionStrategy));
    spy.and.returnValue(true);
    dropdownStateService.stateSubject.setState({ isFullScreen: true });
    expect(service.overlayRef.updatePositionStrategy).toHaveBeenCalledWith(jasmine.any(GlobalPositionStrategy));
    expect(service.overlayRef.updatePosition).toHaveBeenCalled();
  });

  it('should not update position strategy if breakpoint didn\'t changed', () => {
    dropdownStateService.stateSubject.setState({ isFullScreen: false });
    spyOn(service.overlayRef, 'hasAttached').and.returnValue(true);
    const updatePositionSpy = spyOn(service.overlayRef, 'updatePositionStrategy');
    spyOn(service.overlayRef, 'updatePosition');
    spyOn(service.overlayRef, 'attach').and.returnValue(attachedLayout);

    const dropdown: DropdownInterface = {
      viewContainerRef: null,
      hasBackdrop: false,
      layoutType: 'argon-menu',
      content: new ComponentPortal(null),
      config: defaultConfig,
      element: new ElementRef(document.createElement('div'))
    };

    service.createDropdown(dropdown);
    expect(service.overlayRef.updatePositionStrategy).toHaveBeenCalledWith(jasmine.any(FlexibleConnectedPositionStrategy));

    updatePositionSpy.calls.reset();

    dropdownStateService.stateSubject.setState({ isFullScreen: false });
    expect(service.overlayRef.updatePositionStrategy).not.toHaveBeenCalled();
    expect(service.overlayRef.updatePosition).not.toHaveBeenCalled();
  });

  it('should not update position strategy if have no dropdowns', () => {
    dropdownStateService.stateSubject.setState({ isFullScreen: false });
    spyOn(service.overlayRef, 'hasAttached').and.returnValue(false);
    spyOn(service.overlayRef, 'updatePositionStrategy');
    spyOn(service.overlayRef, 'updatePosition');

    dropdownStateService.stateSubject.setState({ isFullScreen: true });
    expect(service.overlayRef.updatePositionStrategy).not.toHaveBeenCalled();
    expect(service.overlayRef.updatePosition).not.toHaveBeenCalled();
  });

  it('removeDropdown should not detach overlayRef if have no dropdown', () => {
    spyOn(service.overlayRef, 'hasAttached').and.returnValue(false);
    spyOn(service.overlayRef, 'detach');
    service.removeDropdown();
    expect(service.overlayRef.detach).not.toHaveBeenCalled();
  });
});
