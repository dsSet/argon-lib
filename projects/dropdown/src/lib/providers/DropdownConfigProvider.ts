import { InjectionToken, Provider } from '@angular/core';
import { DropdownConfigModel } from '../models/DropdownConfigModel';

export const ARGON_DROPDOWN_CONFIG_PROVIDER = new InjectionToken<DropdownConfigModel>('ARGON_DROPDOWN_CONFIG_PROVIDER');

export const defaultConfig: DropdownConfigModel = {
  viewPortMargin: 10,
  growAfterOpen: true,
  withPush: false,
  elementPositions: DropdownConfigModel.positions
};

export const CONFIG_PROVIDERS: Array<Provider> = [
  { provide: ARGON_DROPDOWN_CONFIG_PROVIDER, useValue: defaultConfig }
];
