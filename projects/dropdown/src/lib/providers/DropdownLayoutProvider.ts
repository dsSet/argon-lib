import { InjectionToken, Provider } from '@angular/core';
import { DropdownLayoutModel } from '../models/DropdownLayoutModel';
import { DropdownLayoutComponent } from '../components/DropdownLayout/DropdownLayoutComponent';
import { DropdownMenuLayoutComponent } from '../components/DropdownMenuLayout/DropdownMenuLayoutComponent';

export const ARGON_DROPDOWN_LAYOUT_PROVIDER = new InjectionToken<DropdownLayoutModel>('ARGON_DROPDOWN_LAYOUT_PROVIDER');

export const ARGON_DROPDOWN_DEFAULT_LAYOUT_PROVIDER = new InjectionToken<string>('ARGON_DROPDOWN_DEFAULT_LAYOUT_PROVIDER');

export const argonDefaultLayout = { component: DropdownLayoutComponent, type: 'argon-default' } as DropdownLayoutModel;
export const argonMenuLayout = { component: DropdownMenuLayoutComponent, type: 'argon-menu' } as DropdownLayoutModel;

export const defaultLayoutKey = 'argon-default';

export const LAYOUT_PROVIDERS: Array<Provider> = [
  {
    provide: ARGON_DROPDOWN_LAYOUT_PROVIDER,
    useValue: argonDefaultLayout,
    multi: true
  },
  {
    provide: ARGON_DROPDOWN_LAYOUT_PROVIDER,
    useValue: argonMenuLayout,
    multi: true
  },
  { provide: ARGON_DROPDOWN_DEFAULT_LAYOUT_PROVIDER, useValue: defaultLayoutKey }
];
