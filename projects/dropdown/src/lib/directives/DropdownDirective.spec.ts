import { DropdownDirective } from './DropdownDirective';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DropdownModule } from '../DropdownModule';
import { Component, ElementRef, SimpleChange, TemplateRef } from '@angular/core';
import { HostBindingTestHelper } from '../../../../../test/helpers/HostBindingTestHelper';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CdkPortal, ComponentPortal, TemplatePortal } from '@angular/cdk/portal';
import { DropdownStateInterface } from '../interfaces/DropdownStateInterface';
import { StateInterface } from '@argon/tools';

describe('DropdownDirective', () => {
  let fixture: ComponentFixture<MockComponent>;
  let directive: DropdownDirective;

  @Component({ selector: 'spec-component', template: '' })
  class MockComponent extends DropdownDirective { }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MockComponent
      ],
      imports: [
        NoopAnimationsModule,
        DropdownModule
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    directive = fixture.componentInstance;
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(directive.className).toBeTruthy();
    expect(directive.bsClass).toBeTruthy();
    expect(directive.opened).toBeFalsy();
    expect(directive.autosize).toBeFalsy();
    expect(directive.tabIndex).toEqual(0);
    expect(directive.element).toBeDefined();
    expect(directive.config).toBeDefined();
    expect(directive.viewContainerRef).toBeDefined();
    expect(directive.renderer).toBeDefined();
  });

  it('className should toggle ar-dropdown class', () => {
    HostBindingTestHelper.testToggleClass(directive, fixture, 'className', 'ar-dropdown');
  });

  it('bsClass should toggle dropdown class', () => {
    HostBindingTestHelper.testToggleClass(directive, fixture, 'bsClass', 'dropdown');
  });

  it('disabled should toggle ar-dropdown--disabled class', () => {
    HostBindingTestHelper.testToggleClass(directive, fixture, 'disabled', 'ar-dropdown--disabled');
  });

  it('should have ar-dropdown--focused in opened state', () => {
    fixture.detectChanges();
    directive.open();
    fixture.detectChanges();
    expect(directive.opened).toEqual(true);
    HostBindingTestHelper.testHasClassName(fixture, 'ar-dropdown--focused').toBeTruthy();
  });

  it('content property should return null if content not defined', () => {
    fixture.detectChanges();
    expect(directive.content).toEqual(null);
  });

  it('content property should return TemplatePortal if contentTemplate defined', () => {
    fixture.detectChanges();
    directive.contentTemplate = { elementRef: new ElementRef(null) } as TemplateRef<any>;
    expect(directive.content).toEqual(jasmine.any(TemplatePortal));
  });

  it('content property should return CdkPortal if contentPortal defined', () => {
    fixture.detectChanges();
    directive.contentPortal = new CdkPortal(null, null);
    expect(directive.content).toEqual(directive.contentPortal);
  });

  it('content property should return Portal if portal defined', () => {
    fixture.detectChanges();
    directive.portal = new ComponentPortal(null, null);
    expect(directive.content).toEqual(directive.portal);
  });

  it('open should call openDropdown if not disabled and not opened', () => {
    fixture.detectChanges();
    spyOn(directive.dropdownStateService, 'openDropdown');
    expect(directive.disabled).toBeFalsy();
    expect(directive.opened).toBeFalsy();
    directive.open();
    expect(directive.dropdownStateService.openDropdown).toHaveBeenCalledWith(directive);
  });

  it('open should not call openDropdown if disabled', () => {
    fixture.detectChanges();
    spyOn(directive.dropdownStateService, 'openDropdown');
    directive.disabled = true;
    expect(directive.opened).toBeFalsy();
    directive.open();
    expect(directive.dropdownStateService.openDropdown).not.toHaveBeenCalled();
  });

  it('open should not call openDropdown if opened', () => {
    fixture.detectChanges();
    directive.open();
    spyOn(directive.dropdownStateService, 'openDropdown');
    expect(directive.disabled).toBeFalsy();
    expect(directive.opened).toBeTruthy();
    directive.open();
    expect(directive.dropdownStateService.openDropdown).not.toHaveBeenCalled();
  });

  it('close should not call closeDropdown if not opened', () => {
    fixture.detectChanges();
    spyOn(directive.dropdownStateService, 'closeDropdown');
    expect(directive.opened).toBeFalsy();
    directive.close();
    expect(directive.dropdownStateService.closeDropdown).not.toHaveBeenCalled();
  });

  it('close should call closeDropdown if opened', () => {
    fixture.detectChanges();
    directive.open();
    spyOn(directive.dropdownStateService, 'closeDropdown');
    expect(directive.opened).toBeTruthy();
    directive.close();
    expect(directive.dropdownStateService.closeDropdown).toHaveBeenCalled();
  });

  it('handleStateChange should emit changeState with `false` if dropdown was closed', () => {
    fixture.detectChanges();
    directive.open();
    spyOn(directive.changeState, 'emit');
    directive.close();
    expect(directive.changeState.emit).toHaveBeenCalledWith(false);
  });

  it('handleStateChange should not emit changeState with `false` dropdown not changed', () => {
    fixture.detectChanges();
    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: null, isOpened: false },
      changes: { }
    };
    spyOn(directive.changeState, 'emit');
    directive.handleStateChange(state);
    expect(directive.changeState.emit).not.toHaveBeenCalled();
  });

  it('handleStateChange should not emit changeState if dropdown not changed', () => {
    fixture.detectChanges();
    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: null, isOpened: false },
      changes: { dropdown: new SimpleChange(directive, directive, false) }
    };
    spyOn(directive.changeState, 'emit');
    directive.handleStateChange(state);
    expect(directive.changeState.emit).not.toHaveBeenCalled();
  });

  it('handleStateChange should not emit changeState if dropdown prev value is not equal current directive', () => {
    fixture.detectChanges();
    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: null, isOpened: false },
      changes: { dropdown: new SimpleChange({ }, directive, false) }
    };
    spyOn(directive.changeState, 'emit');
    directive.handleStateChange(state);
    expect(directive.changeState.emit).not.toHaveBeenCalled();
  });

  it('should update layout size on initial mount if autosize is true', () => {
    fixture.detectChanges();
    directive.element.nativeElement.style.width = '100px';
    const layoutElement = document.createElement('div');
    const layout = new ElementRef(layoutElement);
    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: directive, isOpened: false },
      changes: { layout: new SimpleChange(null, { instance: { element: layout } }, false) }
    };
    directive.autosize = true;

    spyOn(directive.renderer, 'setStyle');

    directive.handleStateChange(state);
    expect(directive.renderer.setStyle).toHaveBeenCalledWith(layoutElement, 'width', '100px');
  });

  it('should not update layout size on initial mount if autosize is false', () => {
    fixture.detectChanges();
    directive.element.nativeElement.style.width = '100px';
    const layoutElement = document.createElement('div');
    const layout = new ElementRef(layoutElement);
    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: directive, isOpened: false },
      changes: { layout: new SimpleChange(null, { instance: { element: layout } }, false) }
    };
    directive.autosize = false;

    spyOn(directive.renderer, 'setStyle');

    directive.handleStateChange(state);
    expect(directive.renderer.setStyle).not.toHaveBeenCalled();
  });

  it('should not update layout size on initial mount no layout changes', () => {
    fixture.detectChanges();
    directive.element.nativeElement.style.width = '100px';
    const layoutElement = document.createElement('div');
    const layout = new ElementRef(layoutElement);
    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: directive, isOpened: false },
      changes: { }
    };
    directive.autosize = true;

    spyOn(directive.renderer, 'setStyle');

    directive.handleStateChange(state);
    expect(directive.renderer.setStyle).not.toHaveBeenCalled();
  });

  it('should not update layout size on initial mount if has no current layout', () => {
    fixture.detectChanges();
    directive.element.nativeElement.style.width = '100px';
    const layoutElement = document.createElement('div');
    const layout = new ElementRef(layoutElement);
    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: directive, isOpened: false },
      changes: { layout: new SimpleChange({ instance: { element: layout } }, null, false) }
    };
    directive.autosize = true;

    spyOn(directive.renderer, 'setStyle');

    directive.handleStateChange(state);
    expect(directive.renderer.setStyle).not.toHaveBeenCalled();
  });

  it('should not update layout size on initial mount if not current dropdown', () => {
    fixture.detectChanges();
    directive.element.nativeElement.style.width = '100px';
    const layoutElement = document.createElement('div');
    const layout = new ElementRef(layoutElement);
    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: null, isOpened: false },
      changes: { layout: new SimpleChange(null, { instance: { element: layout } }, false) }
    };
    directive.autosize = true;

    spyOn(directive.renderer, 'setStyle');

    directive.handleStateChange(state);
    expect(directive.renderer.setStyle).not.toHaveBeenCalled();
  });

  it('should emit changeState with true if initial layout mount', () => {
    fixture.detectChanges();

    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: directive, isOpened: false },
      changes: { layout: new SimpleChange(null, null, false) }
    };

    spyOn(directive.changeState, 'emit');

    directive.handleStateChange(state);
    expect(directive.changeState.emit).toHaveBeenCalledWith(true);
  });

  it('should not emit changeState for different dropdown', () => {
    fixture.detectChanges();

    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: null, isOpened: false },
      changes: { layout: new SimpleChange(null, null, false) }
    };

    spyOn(directive.changeState, 'emit');

    directive.handleStateChange(state);
    expect(directive.changeState.emit).not.toHaveBeenCalled();
  });

  it('should not emit changeState if has no layout changes', () => {
    fixture.detectChanges();

    const state: StateInterface<DropdownStateInterface> = {
      state: { layout: null, isFullScreen: false, dropdown: directive, isOpened: false },
      changes: { }
    };

    spyOn(directive.changeState, 'emit');

    directive.handleStateChange(state);
    expect(directive.changeState.emit).not.toHaveBeenCalled();
  });
});
