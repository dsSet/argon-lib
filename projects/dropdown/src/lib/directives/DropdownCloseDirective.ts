import { Directive, HostListener, Injector, OnDestroy, OnInit } from '@angular/core';
import { DropdownStateMixin } from '../mixins/DropdownStateMixin';
import { BaseShape } from '@argon/tools';

@Directive({
  selector: '[arDropdownClose]'
})
export class DropdownCloseDirective extends DropdownStateMixin(BaseShape) implements OnInit, OnDestroy {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  @HostListener('click')
  public click() {
    this.dropdownStateService.closeDropdown();
  }

}
