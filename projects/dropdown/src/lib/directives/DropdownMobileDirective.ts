import { Directive, EmbeddedViewRef, Injector, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { DropdownStateMixin } from '../mixins/DropdownStateMixin';
import { BaseShape, StateInterface } from '@argon/tools';
import { DropdownStateInterface } from '../interfaces/DropdownStateInterface';

@Directive({
  selector: '[arDropdownMobile]'
})
export class DropdownMobileDirective extends DropdownStateMixin(BaseShape) implements OnInit, OnDestroy {

  private content: EmbeddedViewRef<any>;

  constructor(
    private templateRef: TemplateRef<any>,
    private container: ViewContainerRef,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  handleStateChange(nextState: StateInterface<DropdownStateInterface>): void {
    const { changes, state } = nextState;
    const { isFullScreen, layout } = changes;
    if (isFullScreen) {
      this.handleViewUpdate(isFullScreen.currentValue);
    } else if (layout && layout.currentValue) {
      this.handleViewUpdate(state.isFullScreen);
    }

    super.handleStateChange(nextState);
  }

  private handleViewUpdate = (isMobile: boolean) => {
    if (isMobile && !this.content) {
      this.content = this.container.createEmbeddedView(this.templateRef);
    } else if (!isMobile && this.content) {
      this.content = null;
      this.container.clear();
    }
  }
}
