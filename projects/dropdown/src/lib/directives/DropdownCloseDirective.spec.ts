import { DropdownCloseDirective } from './DropdownCloseDirective';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DropdownModule } from '../DropdownModule';
import { Component } from '@angular/core';

describe('DropdownCloseDirective', () => {
  let fixture: ComponentFixture<MockComponent>;
  let directive: DropdownCloseDirective;

  @Component({ selector: 'spec-component', template: '' })
  class MockComponent extends DropdownCloseDirective { }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MockComponent
      ],
      imports: [
        DropdownModule
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    directive = fixture.componentInstance;
  });

  it('click should close dropdown', () => {
    fixture.detectChanges();
    spyOn(directive.dropdownStateService, 'closeDropdown');
    directive.click();
    expect(directive.dropdownStateService.closeDropdown).toHaveBeenCalled();
  });
});
