import {
  ContentChild,
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Injector,
  Input,
  OnDestroy, OnInit,
  Output, Renderer2, TemplateRef, ViewContainerRef
} from '@angular/core';
import { UIShape, UITabindexMixin, StateInterface } from '@argon/tools';
import { DropdownStateMixin } from '../mixins/DropdownStateMixin';
import { DropdownInterface } from '../interfaces/DropdownInterface';
import { DropdownStateInterface } from '../interfaces/DropdownStateInterface';
import { DropdownConfigModel } from '../models/DropdownConfigModel';
import { ARGON_DROPDOWN_CONFIG_PROVIDER } from '../providers/DropdownConfigProvider';
import { CdkPortal, Portal, TemplatePortal } from '@angular/cdk/portal';
import { DropdownOverlayService } from '../services/DropdownOverlayService';

@Directive({
  selector: '[arDropdownDirective]'
})
export class DropdownDirective extends DropdownStateMixin(UITabindexMixin(UIShape)) implements DropdownInterface, OnDestroy, OnInit {

  @ContentChild(TemplateRef, { static: true }) contentTemplate: TemplateRef<any>;

  @ContentChild(CdkPortal, { static: true }) contentPortal: CdkPortal;

  @HostBinding('class.ar-dropdown') className = true;

  @HostBinding('class.dropdown') bsClass = true;

  @HostBinding('class.ar-dropdown--focused') public get opened(): boolean { return this.isOpened(this); }

  @HostBinding('class.ar-dropdown--disabled') @Input() disabled: boolean;

  @Input() portal: Portal<any>;

  @Input() layoutType: string;

  @Input() tabIndex = 0;

  @Input() autosize = false;

  @Input() preventMobileOverlay = false;

  @Output() click = new EventEmitter<void>();

  @Output() changeState = new EventEmitter<boolean>();

  public element: ElementRef<HTMLElement>;

  public config: DropdownConfigModel;

  public viewContainerRef: ViewContainerRef;

  public renderer: Renderer2;

  public get content(): Portal<any> {
    if (this.portal) {
      return this.portal;
    }

    if (this.contentPortal) {
      return this.contentPortal;
    }

    if (this.contentTemplate) {
      return new TemplatePortal(this.contentTemplate, this.viewContainerRef);
    }

    return null;
  }

  public hasBackdrop = true;

  private overlayService: DropdownOverlayService;

  constructor(injector: Injector) {
    super(injector);
    this.element = this.injector.get(ElementRef);
    this.config = this.injector.get(ARGON_DROPDOWN_CONFIG_PROVIDER);
    this.viewContainerRef = this.injector.get(ViewContainerRef);
    this.overlayService = this.injector.get(DropdownOverlayService);
    this.renderer = this.injector.get(Renderer2);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.close();
  }

  @HostListener('click')
  public open() {
    if (!this.opened && !this.disabled) {
      this.dropdownStateService.openDropdown(this);
    }
  }

  public close() {
    if (this.opened) {
      this.dropdownStateService.closeDropdown();
    }
  }

  handleStateChange(nextState: StateInterface<DropdownStateInterface>): void {
    const { changes } = nextState;
    const { isOpened, dropdown, layout } = changes;

    if (dropdown && dropdown.previousValue !== dropdown.currentValue && dropdown.previousValue === this) {
      this.changeState.emit(false);
    }

    if (this.autosize && layout && nextState.state.dropdown === this && layout.currentValue) {
      this.setLayoutSize(layout.currentValue.instance.element);
    }

    if (layout && nextState.state.dropdown === this) {
      this.changeState.emit(true);
    }

    super.handleStateChange(nextState);
  }

  private setLayoutSize(layout: ElementRef<HTMLElement>) {
    const clientRect = this.element.nativeElement.getBoundingClientRect();
    this.renderer.setStyle(layout.nativeElement, 'width', `${clientRect.width}px`);
  }
}
