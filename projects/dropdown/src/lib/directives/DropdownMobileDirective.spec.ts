import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DropdownMobileDirective } from './DropdownMobileDirective';
import { DropdownModule } from '../DropdownModule';
import { DropdownStateService } from '../services/DropdownStateService';

@Component({
  selector: 'spec-fake-component',
  template: '<div class="mobile-content" *arDropdownMobile></div>'
})
class FakeComponent { }

describe('DropdownMobileDirective', () => {
  let fixture: ComponentFixture<FakeComponent>;
  let element: HTMLElement;
  let dropdownService: DropdownStateService;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      declarations: [
        FakeComponent
      ],
      imports: [
        DropdownModule
      ]
    }).compileComponents()
  ));

  it('should render content depends on dropdownOverlayService.isMobile property', () => {
    fixture = TestBed.createComponent(FakeComponent);
    element = fixture.nativeElement;
    dropdownService = TestBed.inject(DropdownStateService) as any;
    dropdownService.stateSubject.setState({ isFullScreen: true });
    fixture.detectChanges();
    expect(element.getElementsByClassName('mobile-content').length).toEqual(1);

    dropdownService.stateSubject.setState({ isFullScreen: false });
    fixture.detectChanges();
    expect(element.getElementsByClassName('mobile-content').length).toEqual(0);
  });
});
