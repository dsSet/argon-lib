import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { DropdownMenuComponent } from './DropdownMenuComponent';
import { NO_ERRORS_SCHEMA, QueryList } from '@angular/core';
import { KeyboardModule } from '@argon/keyboard';

describe('DropdownMenuComponent', () => {

  let component: DropdownMenuComponent;
  let fixture: ComponentFixture<DropdownMenuComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [KeyboardModule],
    declarations: [ DropdownMenuComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownMenuComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-dropdown-menu class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-dropdown-menu');
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(component.className).toEqual(true);
    expect(component.items).toEqual(jasmine.any(QueryList));
  });

  it('should connect items', () => {
    spyOn(component, 'connect');
    fixture.detectChanges();
    expect(component.connect).toHaveBeenCalledWith(component.items);
  });
});
