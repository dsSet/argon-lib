import {
  AfterContentInit,
  ChangeDetectionStrategy,
  Component,
  ContentChildren,
  HostBinding, Injector, OnDestroy, OnInit,
  QueryList
} from '@angular/core';
import { DropdownMenuA11yService, A11yListMixin } from '@argon/a11y-list';
import { BaseShape } from '@argon/tools';
import { DropdownMenuItemComponent } from '../DropdownMenuItem/DropdownMenuItemComponent';

@Component({
  selector: 'ar-dropdown-menu',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [DropdownMenuA11yService]
})
export class DropdownMenuComponent extends A11yListMixin(BaseShape) implements AfterContentInit, OnInit, OnDestroy {

  @ContentChildren(DropdownMenuItemComponent, { descendants: true }) items: QueryList<DropdownMenuItemComponent>;

  @HostBinding('class.ar-dropdown-menu') className = true;

  constructor(
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  ngAfterContentInit(): void {
    this.connect(this.items);
  }

}
