import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { DropdownMenuLayoutComponent } from './DropdownMenuLayoutComponent';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('DropdownMenuLayoutComponent', () => {

  let component: DropdownMenuLayoutComponent;
  let fixture: ComponentFixture<DropdownMenuLayoutComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [NoopAnimationsModule],
    declarations: [ DropdownMenuLayoutComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownMenuLayoutComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-dropdown-menu-layout class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-dropdown-menu-layout');
  });

  it('should have defaults', () => {
    expect(component.className).toEqual(true);
    expect(component.animation).toEqual(true);
  });
});
