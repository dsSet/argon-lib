import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DropdownMenuComponent } from '../DropdownMenu/DropdownMenuComponent';
import { DropdownMenuItemComponent } from './DropdownMenuItemComponent';
import { DropdownStateService } from '../../services/DropdownStateService';
import { DropdownMenuA11yService } from '@argon/a11y-list';
import { EventEmitter } from '@angular/core';
import { DropdownModule } from '../../DropdownModule';

describe('DropdownMenuItemComponent', () => {
  let component: DropdownMenuItemComponent;
  let fixture: ComponentFixture<DropdownMenuItemComponent>;
  let dropdownStateService: DropdownStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        DropdownModule
      ]
    }).overrideComponent(DropdownMenuComponent, {
      remove: {
        providers: [DropdownMenuA11yService]
      }
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownMenuItemComponent);
    component = fixture.componentInstance;
    dropdownStateService = TestBed.inject(DropdownStateService);
  });

  it('should have default properties', () => {
    expect(component.bsClass).toBeTruthy();
    expect(component.className).toBeTruthy();
    expect(component.selected).toBeFalsy();
    expect(component.role).toEqual('menuitem');
    expect(component.click).toEqual(jasmine.any(EventEmitter));
    expect(component.disabled).toBeFalsy();
    expect(component.active).toBeFalsy();
    expect(component.closeOnSelect).toBeTruthy();
  });

  it('setActiveStyles should call scrollIntoView element action if true value passed', () => {
    const element = fixture.nativeElement;
    spyOn(element, 'scrollIntoView');
    component.setActiveStyles();
    expect(element.scrollIntoView).toHaveBeenCalledWith({ behavior: 'smooth', inline: 'nearest', block: 'nearest' });
    expect(component.active).toEqual(true);
  });

  it('handleClick should prevent default event', () => {
    const event = new MouseEvent('click');
    spyOn(event, 'stopPropagation');
    component.handleClick(event);
    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('handleClick should close dropdown if !disabled and closeOnSelect === true', () => {
    component.disabled = false;
    component.closeOnSelect = true;
    spyOn(component.dropdownStateService, 'closeDropdown');
    component.handleClick();
    expect(component.dropdownStateService.closeDropdown).toHaveBeenCalled();
  });

  it('handleClick not should close dropdown if disabled', () => {
    component.disabled = true;
    component.closeOnSelect = true;
    spyOn(component.dropdownStateService, 'closeDropdown');
    const event = new MouseEvent('click');
    component.handleClick(null);
    expect(component.dropdownStateService.closeDropdown).not.toHaveBeenCalled();
  });

  it('handleClick should not close dropdown if closeOnSelect === false', () => {
    component.disabled = false;
    component.closeOnSelect = false;
    spyOn(component.dropdownStateService, 'closeDropdown');
    const event = new MouseEvent('click');
    component.handleClick(event);
    expect(component.dropdownStateService.closeDropdown).not.toHaveBeenCalled();
  });

  it('bsClass should bind .dropdown-item class', () => {
    component.bsClass = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('dropdown-item')).toEqual(true);
    component.bsClass = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('dropdown-item')).toEqual(false);
  });

  it('className should bind .ar-dropdown-menu-item', () => {
    component.className = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-dropdown-menu-item')).toEqual(true);
    component.className = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-dropdown-menu-item')).toEqual(false);
  });

  it('selected should bind .selected', () => {
    component.selected = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('selected')).toEqual(true);
    component.selected = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('selected')).toEqual(false);
  });

  it('disabled should bind .disabled', () => {
    component.disabled = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('disabled')).toEqual(true);
    component.disabled = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('disabled')).toEqual(false);
  });

  it('active should bind .active', () => {
    component.active = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('active')).toEqual(true);
    component.active = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('active')).toEqual(false);
  });

  it('role should bind "role" attribute', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('role')).toEqual('menuitem');
    component.role = 'menu';
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('role')).toEqual('menu');
  });
});
