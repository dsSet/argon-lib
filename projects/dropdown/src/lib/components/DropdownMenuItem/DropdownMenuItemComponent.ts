import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, ElementRef,
  EventEmitter,
  HostBinding,
  HostListener, Injector,
  Input,
  Output
} from '@angular/core';
import { A11yListItem } from '@argon/a11y-list';
import { DropdownStateService } from '../../services/DropdownStateService';

@Component({
  selector: 'ar-dropdown-menu-item',
  template: `
    <ng-content></ng-content>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DropdownMenuItemComponent extends A11yListItem {

  @HostBinding('class.dropdown-item') bsClass = true;

  @HostBinding('class.ar-dropdown-menu-item') className = true;

  @HostBinding('class.active') active = false;

  @HostBinding('attr.role') role = 'menuitem';

  @HostBinding('class.disabled') @Input() disabled = false;

  @HostBinding('class.selected') @Input() selected = false;

  @HostBinding('class.ar-dropdown-menu-item--limited') @Input() limited = false;

  @Input() closeOnSelect = true;

  @Output() click = new EventEmitter<void>();

  public changeDetector: ChangeDetectorRef;

  public dropdownStateService: DropdownStateService;

  public element: ElementRef<HTMLElement>;

  constructor(
    injector: Injector
  ) {
    super(injector);
    this.changeDetector = this.injector.get(ChangeDetectorRef);
    this.dropdownStateService = this.injector.get(DropdownStateService);
    this.element = this.injector.get(ElementRef);
  }

  @HostListener('click', ['$event'])
  public handleClick(e?: MouseEvent) {
    if (e) {
      e.stopPropagation();
    }

    if (this.disabled) {
      return;
    }

    if (this.closeOnSelect) {
      this.dropdownStateService.closeDropdown();
    }
  }

  setActiveStyles(): void {
    super.setActiveStyles();
    this.element.nativeElement.scrollIntoView({ behavior: 'smooth', inline: 'nearest', block: 'nearest' });
  }

  handleStateChange(): void {
    super.handleStateChange();
    this.changeDetector.markForCheck();
  }

}
