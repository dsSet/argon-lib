import { Component, ViewChild } from '@angular/core';
import { DropdownDirective } from '../../directives/DropdownDirective';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DropdownModule } from '../../DropdownModule';
import { DropdownFocusableComponent } from './DropdownFocusableComponent';
import { DropdownStateService } from '../../services/DropdownStateService';
import { Subject } from 'rxjs';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('DropdownFocusableComponent', () => {
  @Component({ selector: 'spec-component', template: '<ar-dropdown-focusable></ar-dropdown-focusable>' })
  class MockComponent {
    @ViewChild(DropdownFocusableComponent, { static: true }) dropdown: DropdownFocusableComponent;
  }


  let componentFixture: ComponentFixture<DropdownFocusableComponent>;
  let component: DropdownFocusableComponent;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      declarations: [
        MockComponent
      ],
      providers: [
        DropdownStateService
      ],
      imports: [
        NoopAnimationsModule,
        DropdownModule
      ],
    }).compileComponents()
  ));

  beforeEach(() => {
    componentFixture = TestBed.createComponent(DropdownFocusableComponent);
    component = componentFixture.componentInstance;
  });

  afterEach(() => {
    componentFixture.detectChanges();
  });

  it('should extend DropdownDirective', () => {
    const fixture = TestBed.createComponent(DropdownFocusableComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance instanceof DropdownDirective).toBeTruthy();
  });

  it('should be provided as DropdownDirective', () => {
    const fixture = TestBed.createComponent(MockComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance.dropdown).toBeDefined();
  });

  it('should add focus monitor listener', () => {
    const focusSubject = new Subject<any>();
    spyOn(component.focusMonitor, 'monitor').and.returnValue(focusSubject);
    componentFixture.detectChanges();
    expect(component.focusMonitor.monitor).toHaveBeenCalledWith(component.element, true);
    focusSubject.complete();
  });

});
