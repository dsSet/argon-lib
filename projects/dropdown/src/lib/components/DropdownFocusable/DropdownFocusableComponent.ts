import { DropdownDirective } from '../../directives/DropdownDirective';
import { ChangeDetectionStrategy, Component, forwardRef, Injector, NgZone, OnDestroy, OnInit } from '@angular/core';
import { FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import { animationFrameScheduler, scheduled, Subject, Subscription, timer } from 'rxjs';
import { StateInterface, Unsubscribable } from '@argon/tools';
import { DropdownStateInterface } from '../../interfaces/DropdownStateInterface';
import { DropdownLayoutInterface } from '../../interfaces/DropdownLayoutInterface';
import { debounce, distinctUntilChanged, map, mergeAll } from 'rxjs/operators';


@Component({
  selector: 'ar-dropdown-focusable',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: DropdownDirective, useExisting: forwardRef(() => DropdownFocusableComponent) }
  ],
  host: {
    '[attr.tabindex]': 'attrTabIndex',
    '[attr.disabled]': 'attrDisabled'
  }
})
export class DropdownFocusableComponent extends DropdownDirective implements OnInit, OnDestroy {

  public hasBackdrop = false;

  private focusMonitorSubscription: Subscription;

  private layoutFocusMonitorSubscription: Subscription;

  private layoutFocus = new Subject<FocusOrigin>();

  constructor(
    public focusMonitor: FocusMonitor,
    public ngZone: NgZone,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.focusMonitorSubscription = scheduled([
      this.focusMonitor.monitor(this.element, true),
      this.layoutFocus.asObservable()
    ], animationFrameScheduler).pipe(
      mergeAll(),
      map((origin: FocusOrigin) => Boolean(origin)),
      debounce(this.getFocusDebounce),
      distinctUntilChanged()
    ).subscribe(this.handleFocus);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  handleStateChange(nextState: StateInterface<DropdownStateInterface>): void {
    const { changes, state } = nextState;
    const { layout } = changes;

    if (layout && layout.currentValue && state.dropdown === this) {
      this.addLayoutMonitor(layout.currentValue.instance);
    } else if (layout && !layout.currentValue && layout.previousValue && state.dropdown !== this) {
      this.removeLayoutMonitor(layout.previousValue.instance);
    }

    super.handleStateChange(nextState);
  }

  private handleFocus = (focused: boolean) => {
    this.ngZone.run(() => { this.toggleDropdown(focused); });
  }

  private toggleDropdown = (isOpened: boolean) => {
    if (isOpened && !this.opened) {
      this.open();
    } else if (!isOpened && this.opened) {
      this.close();
    }
  }

  private addLayoutMonitor(layout: DropdownLayoutInterface) {
    this.renderer.setAttribute(layout.element.nativeElement, 'tabindex', '0');
    this.layoutFocusMonitorSubscription = this.focusMonitor.monitor(layout.element, true).subscribe(this.layoutFocus);
  }

  private removeLayoutMonitor(layout: DropdownLayoutInterface) {
    this.renderer.removeAttribute(layout.element.nativeElement, 'tabindex');
    if (this.layoutFocusMonitorSubscription) {
      this.layoutFocusMonitorSubscription.unsubscribe();
      this.layoutFocusMonitorSubscription = null;
    }
  }

  private getFocusDebounce = (focused: boolean) => {
    return timer(focused ? 0 : 100);
  }
}
