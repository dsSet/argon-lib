import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { DropdownLayoutComponent } from './DropdownLayoutComponent';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('DropdownLayoutComponent', () => {

  let component: DropdownLayoutComponent;
  let fixture: ComponentFixture<DropdownLayoutComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [NoopAnimationsModule],
    declarations: [ DropdownLayoutComponent ],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownLayoutComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-dropdown-layout class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-dropdown-layout');
  });

  it('should have defaults', () => {
    expect(component.className).toEqual(true);
    expect(component.animation).toEqual(true);
  });
});
