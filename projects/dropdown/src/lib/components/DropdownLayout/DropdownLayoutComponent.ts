import { ChangeDetectionStrategy, Component, ElementRef, HostBinding } from '@angular/core';
import { Portal } from '@angular/cdk/portal';
import { trigger } from '@angular/animations';
import { ScrollbarDirective } from '@argon/style-kit';
import { DropdownLayoutInterface } from '../../interfaces/DropdownLayoutInterface';
import { LayoutAnimations } from '../../animations/DropdownLayoutAnimation';

@Component({
  selector: 'ar-dropdown-layout',
  template: `<ng-container [cdkPortalOutlet]="content"></ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('inOutAnimation', LayoutAnimations)
  ]
})
export class DropdownLayoutComponent extends ScrollbarDirective implements DropdownLayoutInterface {

  @HostBinding('class.ar-dropdown-layout') className = true;

  @HostBinding('@inOutAnimation') animation = true;

  content: Portal<any>;

  constructor(
    public element: ElementRef<HTMLElement>
  ) {
    super();
  }

}
