import {
  ChangeDetectionStrategy,
  Component,
  forwardRef,
  Injector,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { DropdownDirective } from '../../directives/DropdownDirective';

@Component({
  selector: 'ar-dropdown',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: DropdownDirective, useExisting: forwardRef(() => DropdownComponent) }
  ],
  host: {
    '[attr.tabindex]': 'attrTabIndex',
    '[attr.disabled]': 'attrDisabled'
  }
})
export class DropdownComponent extends DropdownDirective implements OnInit, OnDestroy {

  constructor(
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

}
