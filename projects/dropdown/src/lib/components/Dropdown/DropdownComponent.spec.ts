import { TestBed, waitForAsync } from '@angular/core/testing';
import { DropdownComponent } from './DropdownComponent';
import { Component, ViewChild } from '@angular/core';
import { DropdownModule } from '../../DropdownModule';
import { DropdownDirective } from '../../directives/DropdownDirective';
import { DropdownStateService } from '../../services/DropdownStateService';

describe('DropdownComponent', () => {
  @Component({ selector: 'spec-component', template: '<ar-dropdown></ar-dropdown>' })
  class MockComponent {
    @ViewChild(DropdownDirective, { static: true }) dropdown: DropdownDirective;
  }

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      declarations: [
        MockComponent
      ],
      providers: [
        DropdownStateService
      ],
      imports: [
        DropdownModule
      ],
    }).compileComponents()
  ));

  it('should extend DropdownDirective', () => {
    const fixture = TestBed.createComponent(DropdownComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance instanceof DropdownDirective).toBeTruthy();
  });

  it('should be provided as DropdownDirective', () => {
    const fixture = TestBed.createComponent(MockComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance.dropdown).toBeDefined();
  });
});
