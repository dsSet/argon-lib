import { ChangeDetectionStrategy, Component, ElementRef, forwardRef, Injector, Input, OnDestroy, OnInit } from '@angular/core';
import { DropdownDirective } from '../../directives/DropdownDirective';
import { fromEvent, Observable, scheduled, Subject, Subscription, animationFrameScheduler, timer } from 'rxjs';
import { debounce, filter, map, mergeAll } from 'rxjs/operators';
import { DropdownStateInterface } from '../../interfaces/DropdownStateInterface';
import { StateInterface, Unsubscribable } from '@argon/tools';
import { DropdownLayoutInterface } from '../../interfaces/DropdownLayoutInterface';


@Component({
  selector: 'ar-dropdown-hover',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: DropdownDirective, useExisting: forwardRef(() => DropdownHoverComponent) }
  ],
  host: {
    '[attr.tabindex]': 'attrTabIndex',
    '[attr.disabled]': 'attrDisabled'
  }
})
export class DropdownHoverComponent extends DropdownDirective implements OnInit, OnDestroy {

  public hasBackdrop = false;

  @Input() enterDebounce = 250;

  @Input() leaveDebounce = 100;

  private layoutMouseEvents = new Subject<Event>();

  private mouseSubscription: Subscription;

  private layoutMouseEventsSubscription: Subscription;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.mouseSubscription = scheduled([
        this.createHoverSource(this.element).pipe(filter(this.hoverFilter)),
        this.layoutMouseEvents.asObservable()
    ], animationFrameScheduler).pipe(
      mergeAll(),
      map((event: Event) => event.type === 'mouseenter'),
      debounce(this.getDebounceTime)
    ).subscribe(this.handleMouseEvents);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  handleStateChange(nextState: StateInterface<DropdownStateInterface>): void {
    const { changes, state } = nextState;
    const { layout } = changes;
    if (layout && layout.currentValue && state.dropdown === this) {
      this.addHoverMonitor(layout.currentValue.instance);
    } else if (layout && !layout.currentValue && layout.previousValue && state.dropdown !== this) {
      this.removeHoverMonitor(layout.previousValue.instance);
    }

    super.handleStateChange(nextState);
  }

  private createHoverSource(element: ElementRef<HTMLElement>): Observable<Event> {
    return scheduled([
      fromEvent(element.nativeElement, 'mouseenter'),
      fromEvent(element.nativeElement, 'mouseleave').pipe(
        filter(() => !this.dropdownStateService.state.isFullScreen || this.preventMobileOverlay)
      )
    ], animationFrameScheduler).pipe(mergeAll());
  }

  private handleMouseEvents = (isOpened: boolean) => {
    if (isOpened && !this.opened) {
      this.open();
    } else if (!isOpened && this.opened) {
      this.close();
    }
  }

  private addHoverMonitor(layout: DropdownLayoutInterface) {
    this.layoutMouseEventsSubscription = this.createHoverSource(layout.element).subscribe(this.layoutMouseEvents);
  }

  private removeHoverMonitor(layout: DropdownLayoutInterface) {
    if (this.layoutMouseEventsSubscription) {
      this.layoutMouseEventsSubscription.unsubscribe();
      this.layoutMouseEventsSubscription = null;
    }
  }

  private getDebounceTime = (isMouseIn: boolean): Observable<number> => {
    return timer(isMouseIn ? this.enterDebounce : this.leaveDebounce);
  }

  private hoverFilter = (event: MouseEvent): boolean => {
    const layout = this.dropdownStateService.state.layout;
    if (!layout || !event.relatedTarget) {
      return true;
    }

    const layoutElement = layout.instance.element.nativeElement;

    return !(layoutElement.contains(event.relatedTarget as Node) || event.relatedTarget === layoutElement);
  }
}
