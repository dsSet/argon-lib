import { Component, ViewChild } from '@angular/core';
import { DropdownDirective } from '../../directives/DropdownDirective';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { DropdownModule } from '../../DropdownModule';
import { DropdownHoverComponent } from './DropdownHoverComponent';
import { DropdownStateService } from '../../services/DropdownStateService';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('DropdownHoverComponent', () => {
  @Component({ selector: 'spec-component', template: '<ar-dropdown-hover></ar-dropdown-hover>' })
  class MockComponent {
    @ViewChild(DropdownHoverComponent, { static: true }) dropdown: DropdownHoverComponent;
  }

  const mouseEnter = new MouseEvent('mouseenter');
  const mouseLeave = new MouseEvent('mouseleave');

  let componentFixture: ComponentFixture<DropdownHoverComponent>;
  let component: DropdownHoverComponent;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      declarations: [
        MockComponent
      ],
      providers: [
        DropdownStateService
      ],
      imports: [
        NoopAnimationsModule,
        DropdownModule
      ],
    }).compileComponents()
  ));

  beforeEach(() => {
    componentFixture = TestBed.createComponent(DropdownHoverComponent);
    component = componentFixture.componentInstance;
  });

  afterEach(() => {
    componentFixture.detectChanges();
  });

  it('should extend DropdownDirective', () => {
    const fixture = TestBed.createComponent(DropdownHoverComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance instanceof DropdownDirective).toBeTruthy();
  });

  it('should be provided as DropdownDirective', () => {
    const fixture = TestBed.createComponent(MockComponent);
    fixture.detectChanges();
    expect(fixture.componentInstance.dropdown).toBeDefined();
  });

  it('should have defaults', () => {
    expect(component.enterDebounce).toEqual(250);
    expect(component.leaveDebounce).toEqual(100);
  });
});
