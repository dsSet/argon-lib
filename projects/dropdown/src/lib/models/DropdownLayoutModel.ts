import { DropdownLayoutInterface } from '../interfaces/DropdownLayoutInterface';
import { Type } from '@angular/core';

export class DropdownLayoutModel {

  constructor(
    public type: string,
    public component: Type<DropdownLayoutInterface>
  ) { }

}
