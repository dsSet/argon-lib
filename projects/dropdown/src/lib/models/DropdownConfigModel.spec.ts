import { DropdownConfigModel } from './DropdownConfigModel';

describe('DropdownConfigModel', () => {

  it('should have defaults', () => {
    const model = new DropdownConfigModel();
    expect(model.elementPositions).toEqual(DropdownConfigModel.positions);
    expect(model.withPush).toEqual(true);
    expect(model.growAfterOpen).toEqual(false);
    expect(model.viewPortMargin).toEqual(10);
  });
});
