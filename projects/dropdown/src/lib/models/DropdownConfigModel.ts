import { ConnectedPosition } from '@angular/cdk/overlay';

export class DropdownConfigModel {

  public static positions: Array<ConnectedPosition> = [
    { weight: 100, overlayY: 'top', overlayX: 'start', originY: 'bottom', originX: 'start', offsetY: 5 },
    { weight: 90, overlayY: 'top', overlayX: 'end', originY: 'bottom', originX: 'end', offsetY: 5 },
    { weight: 80, overlayY: 'top', overlayX: 'center', originY: 'bottom', originX: 'center', offsetY: 5 },
    { weight: 70, overlayY: 'bottom', overlayX: 'start', originY: 'top', originX: 'start', offsetY: -5 },
    { weight: 60, overlayY: 'bottom', overlayX: 'end', originY: 'top', originX: 'end', offsetY: -5 },
    { weight: 50, overlayY: 'bottom', overlayX: 'center', originY: 'top', originX: 'center', offsetY: -5 },
  ];

  public withPush = true;

  public growAfterOpen = false;

  public viewPortMargin = 10;

  constructor(
    public elementPositions: Array<ConnectedPosition> = DropdownConfigModel.positions
  ) { }

}
