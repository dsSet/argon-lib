import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { PortalModule } from '@angular/cdk/portal';
import { KeyboardModule } from '@argon/keyboard';
import { MediaModule } from '@argon/media';
import { StyleKitModule } from '@argon/style-kit';
import { DropdownLayoutComponent } from './components/DropdownLayout/DropdownLayoutComponent';
import { DropdownComponent } from './components/Dropdown/DropdownComponent';
import { DropdownMenuComponent } from './components/DropdownMenu/DropdownMenuComponent';
import { DropdownMenuLayoutComponent } from './components/DropdownMenuLayout/DropdownMenuLayoutComponent';
import { DropdownMenuItemComponent } from './components/DropdownMenuItem/DropdownMenuItemComponent';
import { DropdownCloseDirective } from './directives/DropdownCloseDirective';
import { DropdownMobileDirective } from './directives/DropdownMobileDirective';
import { CONFIG_PROVIDERS } from './providers/DropdownConfigProvider';
import {
  LAYOUT_PROVIDERS
} from './providers/DropdownLayoutProvider';
import { DropdownDirective } from './directives/DropdownDirective';
import { DropdownFocusableComponent } from './components/DropdownFocusable/DropdownFocusableComponent';
import { DropdownHoverComponent } from './components/DropdownHover/DropdownHoverComponent';

@NgModule({
    imports: [
        MediaModule,
        KeyboardModule,
        StyleKitModule,
        OverlayModule,
        PortalModule,
        CommonModule
    ],
    declarations: [
        DropdownComponent,
        DropdownFocusableComponent,
        DropdownHoverComponent,
        DropdownLayoutComponent,
        DropdownMenuComponent,
        DropdownMenuLayoutComponent,
        DropdownMenuItemComponent,
        DropdownCloseDirective,
        DropdownMobileDirective,
        DropdownDirective
    ],
    exports: [
        DropdownComponent,
        DropdownFocusableComponent,
        DropdownHoverComponent,
        DropdownLayoutComponent,
        DropdownMenuComponent,
        DropdownMenuLayoutComponent,
        DropdownMenuItemComponent,
        DropdownCloseDirective,
        DropdownMobileDirective,
    ],
    providers: [
        CONFIG_PROVIDERS,
        LAYOUT_PROVIDERS
    ]
})
export class DropdownModule { }
