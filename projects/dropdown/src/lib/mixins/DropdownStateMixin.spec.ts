import { ChangeDetectorRef, Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownModule } from '../DropdownModule';
import { DropdownStateMixin } from './DropdownStateMixin';
import { BaseShape, StateInterface } from '@argon/tools';
import { DropdownStateService } from '../services/DropdownStateService';
import { DropdownStateInterface } from '../interfaces/DropdownStateInterface';

describe('DropdownStateMixin', () => {

  @Component({ selector: 'spec-component', template: '' })
  class MockComponent extends DropdownStateMixin(BaseShape) { }

  let fixture: ComponentFixture<MockComponent>;
  let component: MockComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        MockComponent
      ],
      imports: [
        NoopAnimationsModule,
        DropdownModule
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(component.dropdownStateService).toBeDefined();
    expect(component.changeDetector).toBeDefined();
  });

  it('isOpened should call dropdownStateService.isOpened', () => {
    fixture.detectChanges();
    spyOn(component.dropdownStateService, 'isOpened');
    const directive = { data: 123 } as any;
    component.isOpened(directive);
    expect(component.dropdownStateService.isOpened).toHaveBeenCalledWith(directive);
  });

  it('handleStateChange should call changeDetector.markForCheck', () => {
    fixture.detectChanges();
    spyOn(component.changeDetector, 'markForCheck');
    const state: StateInterface<DropdownStateInterface> = {
      changes: { },
      state: DropdownStateService.getInitialState()
    };
    component.handleStateChange(state);
    expect(component.changeDetector.markForCheck).toHaveBeenCalled();
  });

});
