import { ChangeDetectorRef, Injectable, OnDestroy, OnInit } from '@angular/core';
import { Constructor, HasInjectorInterface, HasLifecircleHooksInterface, Unsubscribable, StateInterface } from '@argon/tools';
import { DropdownStateExtensionInterface } from '../interfaces/DropdownStateExtensionInterface';
import { DropdownStateService } from '../services/DropdownStateService';
import { DropdownStateInterface } from '../interfaces/DropdownStateInterface';
import { Subscription } from 'rxjs';
import { DropdownInterface } from '../interfaces/DropdownInterface';


export function DropdownStateMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<DropdownStateExtensionInterface> {

  @Injectable()
  class DropdownStateExtension extends BaseClass implements DropdownStateExtensionInterface, OnInit, OnDestroy {

    public dropdownStateService: DropdownStateService;

    public changeDetector: ChangeDetectorRef;

    private stateSubscription: Subscription;

    ngOnInit(): void {
      if (super.ngOnInit) {
        super.ngOnInit();
      }

      this.dropdownStateService = this.injector.get(DropdownStateService);
      this.changeDetector = this.injector.get(ChangeDetectorRef);
      this.stateSubscription = this.dropdownStateService.stateSubject.subscribe(
        (nextState: StateInterface<DropdownStateInterface>) => { this.handleStateChange(nextState); }
        );
    }

    @Unsubscribable
    ngOnDestroy(): void {
      if (super.ngOnDestroy) {
        super.ngOnDestroy();
      }
    }

    public isOpened(dropdown: DropdownInterface): boolean {
      return this.dropdownStateService.isOpened(dropdown);
    }

    public handleStateChange(nextState: StateInterface<DropdownStateInterface>) {
      this.changeDetector.markForCheck();
    }

  }

  return DropdownStateExtension;
}
