import { animate, AnimationMetadata, style, transition } from '@angular/animations';


export const LayoutAnimations: Array<AnimationMetadata> = [
  transition(':enter', [
    style({ opacity: 0, transform: 'translateY(10px)' }),
    animate('0.1s', style({ opacity: 1, transform: 'translateY(0)' })),
  ]),
  transition(':leave', [
    style({ opacity: 0 })
  ]),
];
