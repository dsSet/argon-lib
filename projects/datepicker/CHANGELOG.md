# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@2.0.1...@argon/datepicker@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/datepicker





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@1.0.4...@argon/datepicker@2.0.1) (2023-10-12)


### Bug Fixes

* tsfix ([6b405d8](https://bitbucket.org/dsSet/argon-lib/commits/6b405d8621988dd8b6fa34479bdb573e8bac2cc5))






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@1.0.4...@argon/datepicker@2.0.0) (2023-10-12)


### Bug Fixes

* tsfix ([6b405d8](https://bitbucket.org/dsSet/argon-lib/commits/6b405d8621988dd8b6fa34479bdb573e8bac2cc5))






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@1.0.3...@argon/datepicker@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/datepicker





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@1.0.2...@argon/datepicker@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/datepicker





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@1.0.1...@argon/datepicker@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/datepicker






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@0.1.6...@argon/datepicker@1.0.1) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@0.1.6...@argon/datepicker@1.0.0) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))






## [0.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@0.1.5...@argon/datepicker@0.1.6) (2020-08-07)

**Note:** Version bump only for package @argon/datepicker





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@0.1.4...@argon/datepicker@0.1.5) (2020-08-06)


### Bug Fixes

* **datepicker:** add changeComplete event for partial selection ([2f3c671](https://bitbucket.org/dsSet/argon-lib/commits/2f3c671))





## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@0.1.3...@argon/datepicker@0.1.4) (2020-08-05)


### Bug Fixes

* **datepicker:** update dates comparsion ([4c5d775](https://bitbucket.org/dsSet/argon-lib/commits/4c5d775))





## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@0.1.2...@argon/datepicker@0.1.3) (2020-08-05)


### Bug Fixes

* **datepicker:** fix plugin date compartion ([10b5304](https://bitbucket.org/dsSet/argon-lib/commits/10b5304))





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@0.1.1...@argon/datepicker@0.1.2) (2020-08-05)


### Bug Fixes

* **datepicker:** fix current view value comparsion ([e0fad87](https://bitbucket.org/dsSet/argon-lib/commits/e0fad87))





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/datepicker@0.1.0...@argon/datepicker@0.1.1) (2020-08-05)


### Bug Fixes

* **datepicker:** fix assets link ([c4726fe](https://bitbucket.org/dsSet/argon-lib/commits/c4726fe))





# 0.1.0 (2020-08-04)


### Features

* **date:** add base services ([5b5c81c](https://bitbucket.org/dsSet/argon-lib/commits/5b5c81c))
* **date-moment:** add package ([ce1ba01](https://bitbucket.org/dsSet/argon-lib/commits/ce1ba01))
