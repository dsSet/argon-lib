/*
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { DateModule } from './index';

import '!!style-loader!css-loader!sass-loader!./styles.scss';
import { DatePickerStateInterface } from '../src/lib/interfaces/DatePickerStateInterface';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { DateServicesProvider } from '@argon/date-moment-bridge';

storiesOf('Argon|datepicker', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        DateModule,
        BrowserAnimationsModule,
        CommonModule
      ],
      providers: [
        DateServicesProvider
      ]
    })
  )
  .add('Base Datepicker', () => ({
    template: `
        <ar-date-picker>
          <ar-date-picker-header></ar-date-picker-header>
        </ar-date-picker>
      `
  }))
  .add('Datepicker with custom config', () => ({
    props: {
      config: {
        viewCount: 3,
        viewDisplayStrategy: 'middle',
        displayMonthLabel: true,
        displayWeekdays: true,
        displayWeeks: true
      } as Partial<DatePickerStateInterface>,
      date: new Date()
    },
    template: `
        <ar-date-picker [config]="config">
          <ar-date-picker-header></ar-date-picker-header>
        </ar-date-picker>
      `
  }));
*/
