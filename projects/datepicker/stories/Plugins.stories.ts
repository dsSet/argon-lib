/*
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { DateModule } from './index';

import '!!style-loader!css-loader!sass-loader!./styles.scss';
import { DatePickerStateInterface } from '../src/lib/interfaces/DatePickerStateInterface';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { DateServicesProvider } from '@argon/date-moment-bridge';

storiesOf('Argon|datepicker/plugins', module)
  .addDecorator(
      moduleMetadata({
        imports: [
          DateModule,
          BrowserAnimationsModule,
          CommonModule
        ],
        providers: [
          DateServicesProvider
        ]
      })
    )
    .add('vacations', () => ({
      props: {
        config: {
          viewDateType: 'argon-month-view-full',
          displayWeekdays: true,
        } as Partial<DatePickerStateInterface>,
      },
      template: `
        <ar-date-picker [config]="config">
          <ar-date-picker-header></ar-date-picker-header>
          <ar-date-plugin-vacations></ar-date-plugin-vacations>
        </ar-date-picker>
      `
    }))
    .add('single selection', () => ({
      props: {
        config: {
          viewDateType: 'argon-month-view-full'
        } as Partial<DatePickerStateInterface>,
        date: new Date(2020, 3, 24),
        handleSelectDate(date) {
          this.date = date;
          console.log(`${date} was selected`);
        }
      },
      template: `
        <ar-date-picker [config]="config">
          <ar-date-picker-header></ar-date-picker-header>
          <ar-date-plugin-select [date]="date" (change)="handleSelectDate($event)"></ar-date-plugin-select>
        </ar-date-picker>
      `
    }))
    .add('range selection', () => ({
      props: {
        config: {
          viewCount: 2,
          displayMonthLabel: true
        } as Partial<DatePickerStateInterface>,
        range: [
          new Date(2020, 3, 3, 5, 24),
          new Date(2020, 4, 12, 8, 11)
        ],
        handleSelectDate(range: [Date, Date]) {
          this.range = range;
          console.log(`range ${range[0]} - ${range[1]} was selected`);
        }
      },
      template: `
          <ar-date-picker [config]="config">
            <ar-date-picker-header></ar-date-picker-header>
            <ar-date-plugin-select-range [range]="range" (change)="handleSelectDate($event)"></ar-date-plugin-select-range>
          </ar-date-picker>
        `
    }))
    .add('restrict date range', () => ({
      props: {
        config: {
          viewCount: 2,
          viewDate: new Date(2020, 2, 25),
        } as Partial<DatePickerStateInterface>,
        min: new Date(2020, 2, 18),
        max: new Date(2020, 6, 3),
        handleSelectDate(date: Date) {
          console.log(`${date} was selected`);
        }
      },
      template: `
        <ar-date-picker [config]="config">
          <ar-date-picker-header></ar-date-picker-header>
          <ar-date-plugin-restriction [min]="min" [max]="max"></ar-date-plugin-restriction>
          <ar-date-plugin-select [date]="date" (change)="handleSelectDate($event)"></ar-date-plugin-select>
        </ar-date-picker>
      `
    }))
    .add('date list selection', () => ({
      props: {
        config: {
          viewCount: 3,
          viewDisplayStrategy: 'middle',
          viewDate: new Date(),
          isWeekdayInteractive: true,
          isWeekInteractive: true,
          displayWeeks: true,
          displayWeekdays: true
        } as Partial<DatePickerStateInterface>,
        list: [],
        handleSelectDate(dates: Array<Date>) {
          this.list = dates;
          console.log('selected list', dates);
        },
        clear() {
          this.list = [];
        }
      },
      template: `
        <button (click)="clear()">Clear</button>
        <ar-date-picker [config]="config">
          <ar-date-picker-header></ar-date-picker-header>
          <ar-date-plugin-select-list [dates]="list" (change)="handleSelectDate($event)"></ar-date-plugin-select-list>
        </ar-date-picker>
      `
    }))
    .add('partial selection (select start date)', () => ({
      props: {
        config: {
          viewCount: 3,
          viewDisplayStrategy: 'middle'
        } as Partial<DatePickerStateInterface>,
        start: null,
        end: new Date(2020, 6, 16),
        handleSelectStart(date: Date) {
          this.start = date;
        },
        handleSelectTo(date: Date) {
          this.end = date;
        }
      },
      template: `
        <div>{{ start | json }} - {{ end | json }}</div>
        <ar-date-picker [config]="config">
          <ar-date-picker-header></ar-date-picker-header>
          <ar-date-plugin-select-partial-range
              (changeFrom)="handleSelectStart($event)"
              (changeTo)="handleSelectTo($event)"
              mode="start"
              [dateTo]="end"
              [dateFrom]="start"
              ></ar-date-plugin-select-partial-range>
        </ar-date-picker>
      `
    }))
    .add('partial selection (select end date)', () => ({
      props: {
        config: {
          viewCount: 3,
          viewDisplayStrategy: 'middle'
        } as Partial<DatePickerStateInterface>,
        start: new Date(),
        end: null,
        handleSelectStart(date: Date) {
          this.start = date;
        },
        handleSelectTo(date: Date) {
          this.end = date;
        }
      },
      template: `
        <div>{{ start | json }} - {{ end | json }}</div>
        <ar-date-picker [config]="config">
          <ar-date-picker-header></ar-date-picker-header>
          <ar-date-plugin-select-partial-range
              (changeFrom)="handleSelectStart($event)"
              (changeTo)="handleSelectTo($event)"
              mode="end"
              [dateTo]="end"
              [dateFrom]="start"
              ></ar-date-plugin-select-partial-range>
        </ar-date-picker>
      `
    }));
*/
