import { ChangeDetectorRef, Directive, HostBinding, Input } from '@angular/core';
import { DateTransformService, DateIntervalEnum } from '@argon/date';
import { MonthViewInterface } from '../interfaces/MonthViewInterface';
import { DatePickerStateService } from '../services/DatePickerStateService';
import { DatePickerService } from '../services/DatePickerService';

@Directive({
  selector: '[arMonth]'
})
export class MonthDirective implements MonthViewInterface {

  @Input() set date(date: Date) {
    if (!this.isSameMonth(date, this.viewDate)) {
      this.days = this.createDaysRange(date);
    }

    this.viewDate = date;
  }

  @HostBinding('class.ar-month-view') className = true;

  public days: Array<Date>;

  protected viewDate: Date;

  constructor(
    protected datePickerService: DatePickerService,
    protected dateTransformService: DateTransformService,
    public readonly stateService: DatePickerStateService,
    public readonly changeDetectorRef: ChangeDetectorRef
  ) { }

  protected isSameMonth(date1: Date, date2: Date): boolean {
    if (!date1 && !date2) {
      return true;
    }

    if (date1 && date2) {
      return date1.getMonth() === date2.getMonth();
    }

    return false;
  }

  protected createDaysRange(date: Date): Array<Date> {
    if (!date) {
      return [];
    }

    const [start, end] = this.datePickerService.getMonthRange(date);
    return Array.from(this.dateTransformService.getDateIterator(start, end, DateIntervalEnum.day));
  }

}
