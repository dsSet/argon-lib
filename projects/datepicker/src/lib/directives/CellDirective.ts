import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive, ElementRef,
  HostBinding, HostListener,
  Injector,
  Input, OnDestroy, OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { CdkPortalOutlet, ComponentPortal } from '@angular/cdk/portal';
import { ViewCellInterface } from '../interfaces/ViewCellInterface';
import { DateViewFactoryService } from '../services/DateViewFactoryService';
import { CellInterface } from '../interfaces/CellInterface';
import { CellEventModel } from '../models/CellEventModel';
import { DateCellEnum } from '../types/DateCellEnum';
import { DateEventService } from '../services/DateEventService';
import { Subscription } from 'rxjs';
import { Unsubscribable } from '@argon/tools';

@Directive({
  selector: '[arDatepickerCell]'
})
export class CellDirective implements CellInterface, OnInit, OnDestroy {

  @HostBinding('class.ar-datepicker-cell') className = true;

  @Input() type: string;

  @HostBinding('class.visible') @Input() date: Date;

  public cellType: DateCellEnum;

  @HostBinding('class.interactive') @Input() interactive: boolean;

  public element: ElementRef<HTMLElement>;

  @ViewChild(CdkPortalOutlet, { read: CdkPortalOutlet, static: true }) portal: CdkPortalOutlet;

  protected view: ComponentPortal<ViewCellInterface>;

  protected viewRef: ComponentRef<ViewCellInterface>;

  protected dateViewFactoryService: DateViewFactoryService;

  protected viewContainerRef: ViewContainerRef;

  protected componentFactoryResolver: ComponentFactoryResolver;

  protected eventService: DateEventService;

  private pluginSubscription: Subscription;

  constructor(
    protected injector: Injector
  ) {
    this.dateViewFactoryService = this.injector.get(DateViewFactoryService);
    this.viewContainerRef = this.injector.get(ViewContainerRef);
    this.componentFactoryResolver = this.injector.get(ComponentFactoryResolver);
    this.eventService = this.injector.get(DateEventService);
    this.element = this.injector.get(ElementRef);
  }

  ngOnInit(): void {
    this.pluginSubscription = this.eventService.pluginEventSubject.subscribe(() => this.eventService.applyPlugins(this));
    this.eventService.applyPlugins(this);
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  @HostListener('mouseenter', ['$event'])
  public handleMouseEnter(event: MouseEvent) {
    if (!this.interactive) {
      return;
    }
    this.eventService.dispatchEvent(new CellEventModel(event, this.cellType, this.date, 'mouseenter'));
  }

  @HostListener('mouseleave', ['$event'])
  public handleMouseleave(event: MouseEvent) {
    if (!this.interactive) {
      return;
    }
    this.eventService.dispatchEvent(new CellEventModel(event, this.cellType, this.date, 'mouseleave'));
  }

  @HostListener('click', ['$event'])
  public handleClick(event: MouseEvent) {
    if (!this.interactive) {
      return;
    }
    this.eventService.dispatchEvent(new CellEventModel(event, this.cellType, this.date, 'click'));
  }

  @HostListener('contextmenu', ['$event'])
  public handleContextmenu(event: MouseEvent) {
    if (!this.interactive) {
      return;
    }
    this.eventService.dispatchEvent(new CellEventModel(event, this.cellType, this.date, 'click'));
  }

  protected attachView(view: ComponentPortal<ViewCellInterface>) {
    this.view = view;
    this.viewRef = this.portal.attachComponentPortal(this.view);
    this.viewRef.instance.date = this.date;
    this.viewRef.changeDetectorRef.markForCheck();
  }

}
