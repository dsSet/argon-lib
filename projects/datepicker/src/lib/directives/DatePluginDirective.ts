import { DateEventService } from '../services/DateEventService';
import { Directive, HostBinding, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { CellEventInterface } from '../interfaces/CellEventInterface';
import { Unsubscribable } from '@argon/tools';
import { Subscription } from 'rxjs';
import { CellInterface } from '../interfaces/CellInterface';
import { PluginInterface } from '../interfaces/PluginInterface';
import { DateTransformService } from '@argon/date';

@Directive({
  selector: '[arDatePlugin]'
})
export class DatePluginDirective implements OnInit, OnDestroy, PluginInterface {

  @HostBinding('class.ar-date-plugin') className = true;

  private cellEventSubscription: Subscription;

  constructor(
    public dateEventService: DateEventService,
    public dateTransformService: DateTransformService,
    public renderer: Renderer2
  ) { }

  ngOnInit(): void {
    this.cellEventSubscription = this.dateEventService.cellEventSubject.subscribe((event: CellEventInterface) => this.handleEvent(event));
    this.dateEventService.registerPlugin(this);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    this.dateEventService.unregisterPlugin(this);
  }

  public applyPlugin(cell: CellInterface): void {

  }

  protected handleEvent(event: CellEventInterface) {

  }

}
