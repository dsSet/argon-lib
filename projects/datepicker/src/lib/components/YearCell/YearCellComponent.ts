import { ChangeDetectionStrategy, Component, HostBinding, Injector, Input, OnInit } from '@angular/core';
import { CellDirective } from '../../directives/CellDirective';
import { CellInterface } from '../../interfaces/CellInterface';
import { DateCellEnum } from '../../types/DateCellEnum';

@Component({
  selector: 'ar-year-cell',
  template: '<ng-container *cdkPortalOutlet></ng-container>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class YearCellComponent extends CellDirective implements CellInterface, OnInit {

  @HostBinding('class.ar-year-cell') className = true;

  @Input() type: 'argon-cell-year' | string;

  public cellType = DateCellEnum.year;

  constructor(injector: Injector) {
    super(injector);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    const view = this.dateViewFactoryService.createYearCellView(
      this.viewContainerRef, this.injector, this.componentFactoryResolver, this.type
    );
    this.attachView(view);
  }

}
