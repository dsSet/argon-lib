import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  Renderer2,
  SimpleChanges
} from '@angular/core';
import { DatePluginDirective } from '../../directives/DatePluginDirective';
import { DateEventService } from '../../services/DateEventService';
import { CellInterface } from '../../interfaces/CellInterface';
import { DateTransformService, DatePeriodEnum } from '@argon/date';
import { DateCellEnum } from '../../types/DateCellEnum';
import { CellEventInterface } from '../../interfaces/CellEventInterface';
import { DatePickerStateService } from '../../services/DatePickerStateService';
import { DatePickerModeEnum } from '../../types/DatePickerModeEnum';
import { DatePluginClassEnum } from '../../types/DatePluginClassEnum';

@Component({
  selector: 'ar-date-plugin-select',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateSelectPluginComponent extends DatePluginDirective implements OnInit, OnChanges {

  @Input() date: Date;

  @Input() mode: 'start-of-day' | 'end-of-day' = 'start-of-day';

  @Output() change = new EventEmitter<Date>();

  constructor(
    dateEventService: DateEventService,
    dateTransformService: DateTransformService,
    renderer: Renderer2,
    private datePickerStateService: DatePickerStateService
  ) {
    super(dateEventService, dateTransformService, renderer);
  }

  ngOnInit(): void {
    super.ngOnInit();
    if (this.date) {
      this.datePickerStateService.setViewDate(this.date);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { date } = changes;
    if (!date || date.isFirstChange() || this.dateTransformService.isSameDay(date.currentValue, date.previousValue)) {
      return;
    }

    this.dateEventService.pluginEventSubject.next();
    this.datePickerStateService.setViewDate(this.date);
  }

  public applyPlugin(cell: CellInterface): void {
    super.applyPlugin(cell);
    if (!this.isAcceptedCell(cell)) {
      return;
    }

    if (this.shouldApplyStyle(cell.date, this.date)) {
      this.renderer.addClass(cell.element.nativeElement, DatePluginClassEnum.primary);
    } else {
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.primary);
    }
  }

  protected handleEvent(event: CellEventInterface) {
    super.handleEvent(event);
    if (!event.date || event.source !== DateCellEnum.day || event.eventType !== 'click'
      || this.dateTransformService.isSameDay(this.date, event.date)
    ) {
      return;
    }

    this.date = event.date;
    if (this.mode === 'end-of-day') {
      this.change.emit(this.dateTransformService.endOf(event.date, DatePeriodEnum.day));
    } else {
      this.change.emit(event.date);
    }

    this.dateEventService.pluginEventSubject.next();
    this.datePickerStateService.setViewDate(this.date);
  }

  private isAcceptedCell(cell: CellInterface): boolean {
    if (!cell.date) {
      return false;
    }

    return cell.cellType === DateCellEnum.day
      || cell.cellType === DateCellEnum.month
      || cell.cellType === DateCellEnum.year;
  }

  private shouldApplyStyle(date1: Date, date2: Date): boolean {
    return (this.datePickerStateService.viewMode === DatePickerModeEnum.day && this.dateTransformService.isSameDay(date1, date2))
      || (this.datePickerStateService.viewMode === DatePickerModeEnum.month && this.dateTransformService.isSameMonth(date1, date2))
      || (this.datePickerStateService.viewMode === DatePickerModeEnum.year && this.dateTransformService.isSameYear(date1, date2));
  }
}
