import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  SimpleChanges
} from '@angular/core';
import { DatePluginDirective } from '../../directives/DatePluginDirective';
import { DateEventService } from '../../services/DateEventService';
import { CellInterface } from '../../interfaces/CellInterface';
import { DateTransformService, DatePeriodEnum } from '@argon/date';
import { DateCellEnum } from '../../types/DateCellEnum';
import { CellEventInterface } from '../../interfaces/CellEventInterface';
import { DatePickerStateService } from '../../services/DatePickerStateService';
import { RangeSelectionModel } from '../../models/RangeSelectionModel';
import { Unsubscribable } from '@argon/tools';
import { KeyboardService, KeyCodesEnum } from '@argon/keyboard';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { DatePluginClassEnum } from '../../types/DatePluginClassEnum';

@Component({
  selector: 'ar-date-plugin-select-range',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateSelectRangePluginComponent extends DatePluginDirective implements OnInit, OnChanges, OnDestroy {

  @Input() range: [Date, Date];

  @Output() change = new EventEmitter<[Date, Date]>();

  private selection = new RangeSelectionModel();

  private keyboardSubscription: Subscription;

  constructor(
    dateEventService: DateEventService,
    dateTransformService: DateTransformService,
    renderer: Renderer2,
    private datePickerStateService: DatePickerStateService,
    private keyboardService: KeyboardService
  ) {
    super(dateEventService, dateTransformService, renderer);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.keyboardSubscription = this.keyboardService.listenKeyDownEvent([KeyCodesEnum.Esc])
      .pipe(filter(() => this.selection.hasStart))
      .subscribe(this.selection.clear);
    this.selection.complete.subscribe(this.handleSelectionComplete);
    this.selection.changes.subscribe(() => this.dateEventService.pluginEventSubject.next());
    const viewDate = this.getViewDateFromRange(this.range);
    if (viewDate) {
      this.datePickerStateService.setViewDate(viewDate);
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { range } = changes;
    if (!range || range.isFirstChange() || this.isRangesEqual(range.currentValue, range.previousValue)) {
      return;
    }

    this.dateEventService.pluginEventSubject.next();
    const viewDate = this.getViewDateFromRange(this.range);
    this.datePickerStateService.setViewDate(viewDate);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.selection.dispose();
  }

  public applyPlugin(cell: CellInterface): void {
    super.applyPlugin(cell);
    if (!this.isAcceptedCell(cell)) {
      return;
    }

    if (this.selection.inSelectionRange(cell.date) || (!this.selection.hasStart && this.shouldApplyStyle(cell.date, this.range))) {
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.secondary);
      this.renderer.addClass(cell.element.nativeElement, DatePluginClassEnum.primary);
    } else if (this.shouldApplyStyle(cell.date, this.range)) {
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.primary);
      this.renderer.addClass(cell.element.nativeElement, DatePluginClassEnum.secondary);
    } else {
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.primary);
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.secondary);
    }
  }

  protected handleEvent(event: CellEventInterface) {
    super.handleEvent(event);
    if (event.source !== DateCellEnum.day) {
      return;
    }

    if (event.eventType === 'click') {
      this.handleClickEvent(event);
    } else {
      this.handleMouseMoveEvent(event);
    }
  }

  private handleSelectionComplete = ([start, end]: [Date, Date]) => {
    this.range = [start, this.dateTransformService.endOf(end, DatePeriodEnum.day)];
    this.change.emit(this.range);
    this.selection.clear();
  }

  private handleClickEvent(event: CellEventInterface) {
    if (event.event.button === 2 && this.selection.hasStart) {
      event.event.preventDefault();
      this.selection.clear();
      return;
    }

    if (event.event.button !== 0) {
      return;
    }

    if (!this.selection.hasStart || this.selection.start > event.date) {
      this.selection.setStart(event.date);
    } else {
      this.selection.setEnd(event.date);
    }
  }

  private handleMouseMoveEvent(event: CellEventInterface) {
    if (event.eventType === 'mouseenter') {
      this.selection.setHoverDate(event.date);
    } else if (event.eventType === 'mouseleave' && event.date === this.selection.hover) {
      this.selection.removeHoverDate();
    }
  }

  private getViewDateFromRange(range?: [Date, Date]): Date | null {
    const [ start, end ] = range || [null, null];
    return start || end || null;
  }

  private isRangesEqual(range1: [Date, Date], range2: [Date, Date]): boolean {
    if (!range1 || !range2) {
      return false;
    }

    return this.dateTransformService.isSameDay(range1[0], range2[0]) && this.dateTransformService.isSameDay(range1[1], range2[1]);
  }


  private isAcceptedCell(cell: CellInterface): boolean {
    return cell.date && cell.cellType === DateCellEnum.day;
  }

  private shouldApplyStyle(date: Date, range: [Date, Date]): boolean {
    const [start, end] = range || [null, null];
    if (!start && !end) {
      return false;
    }

    if (!start) {
      return end === date;
    }

    if (!end) {
      return start === date;
    }

    return date >= start && date <= end;
  }
}
