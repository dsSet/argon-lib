import { ChangeDetectionStrategy, Component, Input, OnChanges, Renderer2, SimpleChanges } from '@angular/core';
import { DatePluginDirective } from '../../directives/DatePluginDirective';
import { DatePickerStateInterface } from '../../interfaces/DatePickerStateInterface';
import { DateEventService } from '../../services/DateEventService';
import { DateTransformService } from '@argon/date';
import { DatePickerStateService } from '../../services/DatePickerStateService';
import { CellInterface } from '../../interfaces/CellInterface';
import { DateCellEnum } from '../../types/DateCellEnum';
import { DatePluginClassEnum } from '../../types/DatePluginClassEnum';
import size from 'lodash/size';

@Component({
  selector: 'ar-date-plugin-restriction',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateRestrictionPluginComponent extends DatePluginDirective implements OnChanges {

  @Input() min: Date;

  @Input() max: Date;

  constructor(
    dateEventService: DateEventService,
    dateTransformService: DateTransformService,
    renderer: Renderer2,
    private stateService: DatePickerStateService
  ) {
    super(dateEventService, dateTransformService, renderer);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { min, max } = changes;
    if (!min && !max) {
      return;
    }

    const nextState: Partial<DatePickerStateInterface> = { };

    if (min && !this.dateTransformService.isSameDay(min.currentValue, min.previousValue)) {
      nextState.minDate = min.currentValue || null;
    }

    if (max && !this.dateTransformService.isSameDay(max.currentValue, max.previousValue)) {
      nextState.maxDate = max.currentValue || null;
    }

    if (size(nextState) > 0) {
      this.stateService.setState(nextState);
    }
  }

  applyPlugin(cell: CellInterface): void {
    super.applyPlugin(cell);

    if (this.shouldApplyStyle(cell)) {
      this.renderer.addClass(cell.element.nativeElement, DatePluginClassEnum.disabled);
    } else {
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.disabled);
    }

  }

  private shouldApplyStyle(cell: CellInterface): boolean {
    if (!cell.date || cell.cellType === DateCellEnum.week || cell.cellType === DateCellEnum.weekday) {
      return false;
    }

    return this.min && this.min > cell.date
      || this.max && this.max < cell.date;
  }
}
