import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  SimpleChanges
} from '@angular/core';
import { DatePluginDirective } from '../../directives/DatePluginDirective';
import { DateEventService } from '../../services/DateEventService';
import { CellInterface } from '../../interfaces/CellInterface';
import { DatePeriodEnum, DateTransformService } from '@argon/date';
import { DateCellEnum } from '../../types/DateCellEnum';
import { CellEventInterface } from '../../interfaces/CellEventInterface';
import { RangeSelectionModel } from '../../models/RangeSelectionModel';
import { Unsubscribable } from '@argon/tools';
import { Subscription } from 'rxjs';
import { DatePluginClassEnum } from '../../types/DatePluginClassEnum';

@Component({
  selector: 'ar-date-plugin-select-partial-range',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateSelectPartialRangePluginComponent extends DatePluginDirective implements OnInit, OnChanges, OnDestroy {

  @Input() dateFrom: Date;

  @Input() dateTo: Date;

  @Input() mode: 'start' | 'end' = 'start';

  @Output() changeFrom = new EventEmitter<Date>();

  @Output() changeTo = new EventEmitter<Date>();

  @Output() changeComplete = new EventEmitter<void>();

  private selection = new RangeSelectionModel();

  private selectionSubscription: Subscription;

  constructor(
    dateEventService: DateEventService,
    dateTransformService: DateTransformService,
    renderer: Renderer2
  ) {
    super(dateEventService, dateTransformService, renderer);
  }

  ngOnInit(): void {
    super.ngOnInit();
    if (this.dateFrom) {
      this.selection.setStart(this.dateTransformService.startOf(this.dateFrom, DatePeriodEnum.day));
    }

    if (this.dateTo) {
      this.selection.setEnd(this.dateTransformService.startOf(this.dateTo, DatePeriodEnum.day));
    }

    this.selectionSubscription = this.selection.changes.subscribe(this.dateEventService.pluginEventSubject);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { dateFrom, dateTo } = changes;
    if (dateFrom && !dateFrom.isFirstChange() && !this.dateTransformService.isSameDay(dateFrom.currentValue, dateFrom.previousValue)) {
      this.selection.setStart(dateFrom.currentValue);
    }

    if (dateTo && !dateTo.isFirstChange() && !this.dateTransformService.isSameDay(dateTo.currentValue, dateTo.previousValue)) {
      this.selection.setEnd(dateTo.currentValue);
    }
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.selection.dispose();
  }

  public applyPlugin(cell: CellInterface): void {
    super.applyPlugin(cell);
    if (!this.isAcceptedCell(cell)) {
      return;
    }

    if (this.shouldApplyPrimaryStyle(cell.date)) {
      this.renderer.addClass(cell.element.nativeElement, DatePluginClassEnum.primary);
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.secondary);
    } else if (this.shouldApplySecondaryStyle(cell.date)) {
      this.renderer.addClass(cell.element.nativeElement, DatePluginClassEnum.secondary);
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.primary);
    } else {
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.primary);
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.secondary);
    }
  }

  protected handleEvent(event: CellEventInterface) {
    super.handleEvent(event);
    if (event.source !== DateCellEnum.day) {
      return;
    }

    if (this.mode === 'start' && event.eventType === 'click') {
      this.handleStartSelectionEvents(event);
    } else if (this.mode === 'end' && event.eventType === 'click') {
      this.handleEndSelectionEvents(event);
    }

    if (event.eventType === 'mouseenter') {
      this.selection.setHoverDate(event.date);
    } else if (event.eventType === 'mouseleave' && this.selection.hover === event.date) {
      this.selection.setHoverDate(null);
    }
  }

  private handleEndSelectionEvents(event: CellEventInterface) {
    if (this.selection.hasStart && event.date < this.selection.start) {
      this.setDateFrom(event.date);
      this.setDateTo(event.date);
    } else {
      this.setDateTo(event.date);
    }
    this.changeComplete.emit();
  }

  private handleStartSelectionEvents(event: CellEventInterface) {
    if (this.selection.hasEnd && event.date > this.selection.end) {
      this.setDateFrom(event.date);
      this.setDateTo(event.date);
    } else {
      this.setDateFrom(event.date);
    }
    this.changeComplete.emit();
  }

  private isAcceptedCell(cell: CellInterface): boolean {
    return cell.date && cell.cellType === DateCellEnum.day;
  }

  private shouldApplyPrimaryStyle(date: Date): boolean {
    const from = this.mode === 'start'
      ? this.selection.hover
      : this.selection.start;

    const to = this.mode === 'start'
      ? this.selection.end
      : this.selection.hover;

    if (from && to) {
      return date >= from && date <= to || Number(date) === Number(from);
    } else if (this.selection.hover) {
      return Number(this.selection.hover) === Number(date);
    } else if (this.selection.isComplete) {
      return date >= this.selection.start && date <= this.selection.end;
    }

    return Number(date) === Number(this.selection.end) || Number(date) === Number(this.selection.start);
  }

  private shouldApplySecondaryStyle(date: Date): boolean {
    if (!this.selection.hover) {
      return false;
    }

    return this.selection.isComplete
      ? date >= this.selection.start && date <= this.selection.end
      : Number(date) === Number(this.selection.end) || Number(date) === Number(this.selection.start);
  }

  private setDateFrom(date: Date) {
    this.dateFrom = date;
    this.changeFrom.next(date);
    this.selection.setStart(date);
  }

  private setDateTo(date: Date) {
    this.dateTo = date;
    this.changeTo.next(this.dateTransformService.endOf(date, DatePeriodEnum.day));
    this.selection.setEnd(date);
  }
}
