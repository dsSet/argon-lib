import { ChangeDetectionStrategy, Component, Renderer2 } from '@angular/core';
import { DatePluginDirective } from '../../directives/DatePluginDirective';
import { DateEventService } from '../../services/DateEventService';
import { CellInterface } from '../../interfaces/CellInterface';
import { DateTransformService } from '@argon/date';
import { DateCellEnum } from '../../types/DateCellEnum';
import { DatePluginClassEnum } from '../../types/DatePluginClassEnum';

@Component({
  selector: 'ar-date-plugin-today',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateTodayPluginComponent extends DatePluginDirective {

  private date = new Date();

  constructor(
    dateEventService: DateEventService,
    dateTransformService: DateTransformService,
    renderer: Renderer2
  ) {
    super(dateEventService, dateTransformService, renderer);
  }

  applyPlugin(cell: CellInterface): void {
    super.applyPlugin(cell);
    if (!cell.date || cell.cellType !== DateCellEnum.day || !this.dateTransformService.isSameDay(cell.date, this.date)) {
      return;
    }

    this.renderer.addClass(cell.element.nativeElement, DatePluginClassEnum.today);
  }
}
