import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  SimpleChanges
} from '@angular/core';
import { DatePluginDirective } from '../../directives/DatePluginDirective';
import { DateEventService } from '../../services/DateEventService';
import { CellInterface } from '../../interfaces/CellInterface';
import { DateIntervalEnum, DateTransformService, DatePeriodEnum } from '@argon/date';
import { DateCellEnum } from '../../types/DateCellEnum';
import { CellEventInterface } from '../../interfaces/CellEventInterface';
import { Unsubscribable } from '@argon/tools';
import find from 'lodash/find';
import filter from 'lodash/filter';
import differenceBy from 'lodash/differenceBy';
import size from 'lodash/size';
import { DatePluginClassEnum } from '../../types/DatePluginClassEnum';

@Component({
  selector: 'ar-date-plugin-select-list',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateSelectListPluginComponent extends DatePluginDirective implements OnInit, OnChanges, OnDestroy {

  @Input() dates: Array<Date>;

  @Output() change = new EventEmitter<Array<Date>>();

  private weekToSelect: Date;

  private weekdayToSelect: Date;

  constructor(
    dateEventService: DateEventService,
    dateTransformService: DateTransformService,
    renderer: Renderer2
  ) {
    super(dateEventService, dateTransformService, renderer);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { dates } = changes;
    if (!dates) {
      return;
    }

    this.dateEventService.pluginEventSubject.next();
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public applyPlugin(cell: CellInterface): void {
    super.applyPlugin(cell);
    if (this.shouldApplyStyle(cell)) {
      this.renderer.addClass(cell.element.nativeElement, DatePluginClassEnum.primary);
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.secondary);
    } else if (this.shouldHighLightCell(cell)) {
      this.renderer.addClass(cell.element.nativeElement, DatePluginClassEnum.secondary);
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.primary);
    } else {
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.secondary);
      this.renderer.removeClass(cell.element.nativeElement, DatePluginClassEnum.primary);
    }
  }

  protected handleEvent(event: CellEventInterface) {
    super.handleEvent(event);
    switch (event.source) {
      case DateCellEnum.week:
        this.handleWeekEvents(event);
        break;
      case DateCellEnum.weekday:
        this.handleWeekdayEvents(event);
        break;
      case DateCellEnum.day:
        this.handleDayEvents(event);
        break;
    }
  }

  private handleDayEvents(event: CellEventInterface) {
    if (event.eventType !== 'click') {
      return;
    }
    this.dates = this.isSelected(event.date)
      ? filter(this.dates, (item: Date) => !this.dateTransformService.isSameDay(item, event.date))
      : [...(this.dates || []), event.date];
    this.dateEventService.pluginEventSubject.next();
    this.change.emit(this.dates);
  }

  private handleWeekdayEvents(event: CellEventInterface) {
    switch (event.eventType) {
      case 'mouseenter':
        this.weekdayToSelect = event.date;
        break;
      case 'mouseleave':
        if (this.weekdayToSelect === event.date) {
          this.weekdayToSelect = null;
        }
        break;
      case 'click':
        this.handleSelectWeekday(event.date);
        break;
      default: return;
    }
    this.dateEventService.pluginEventSubject.next();
  }

  private handleSelectWeekday(date: Date) {
    const days = this.dateTransformService.getMonthWeekdays(date);
    this.toggleSelection(days);
  }

  private handleWeekEvents(event: CellEventInterface) {
    switch (event.eventType) {
      case 'mouseenter':
        this.weekToSelect = event.date;
        break;
      case 'mouseleave':
        if (this.weekToSelect === event.date) {
          this.weekToSelect = null;
        }
        break;
      case 'click':
        this.handleSelectWeek(event.date);
        break;
      default: return;
    }
    this.dateEventService.pluginEventSubject.next();
  }

  private handleSelectWeek(date: Date) {
    const days = Array.from(this.dateTransformService.getDateIterator(
      this.dateTransformService.startOf(date, DatePeriodEnum.isoWeek),
      this.dateTransformService.shiftDate(date, ['endOf', DatePeriodEnum.isoWeek], ['startOf', DatePeriodEnum.day]),
      DateIntervalEnum.day
    ));

    this.toggleSelection(days);
  }

  private toggleSelection(days: Array<Date>) {
    const unselectedDays = differenceBy(days, this.dates, Number);
    if (size(unselectedDays) > 0) {
      this.dates = [...(this.dates || []), ...unselectedDays];
    } else {
      this.dates = differenceBy(this.dates, days, Number);
    }
    this.change.emit(this.dates);
  }

  private shouldApplyStyle(cell: CellInterface): boolean {
    if (cell.cellType !== DateCellEnum.day) {
      return false;
    }

    return this.isSelected(cell.date);
  }

  private shouldHighLightCell(cell: CellInterface) {
    if (cell.cellType !== DateCellEnum.week && cell.date === this.weekToSelect) {
      return true;
    }

    if (cell.cellType === DateCellEnum.weekday && cell.date === this.weekdayToSelect) {
      return true;
    }

    if (cell.cellType !== DateCellEnum.day) {
      return false;
    }

    return (
      this.weekToSelect
      && this.dateTransformService.isSameWeek(this.weekToSelect, cell.date)
    ) || (
      this.weekdayToSelect
      && this.dateTransformService.isSameMonth(this.weekdayToSelect, cell.date)
      && this.dateTransformService.isSameWeekday(this.weekdayToSelect, cell.date)
    );
  }

  private isSelected(date: Date): boolean {
    return !!find(this.dates, (item: Date) => this.dateTransformService.isSameDay(item, date));
  }
}
