import { PluginInterface } from '../../interfaces/PluginInterface';
import { Type } from '@angular/core';
import { DateRestrictionPluginComponent } from './DateRestrictionPluginComponent';
import { DateSelectListPluginComponent } from './DateSelectListPluginComponent';
import { DateSelectPluginComponent } from './DateSelectPluginComponent';
import { DateSelectRangePluginComponent } from './DateSelectRangePluginComponent';
import { DateTodayPluginComponent } from './DateTodayPluginComponent';
import { DateVacationsPluginComponent } from './DateVacationsPluginComponent';
import { DateSelectPartialRangePluginComponent } from './DateSelectPartialRangePluginComponent';

export const PLUGINS: Array<Type<PluginInterface>> = [
  DateRestrictionPluginComponent,
  DateSelectListPluginComponent,
  DateSelectPluginComponent,
  DateSelectRangePluginComponent,
  DateTodayPluginComponent,
  DateVacationsPluginComponent,
  DateSelectPartialRangePluginComponent
];
