import {
  ChangeDetectionStrategy,
  Component,
  ElementRef, HostBinding,
  Input,
  OnChanges,
  OnInit,
  Renderer2,
  SimpleChanges,
  ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'ar-date-grid',
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class DateGridComponent implements OnInit, OnChanges {

  @HostBinding('class.ar-date-grid') className = true;

  @Input() mode: 'date' | 'month' | 'year' = 'date';

  constructor(
    private element: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {
  }

  public ngOnInit(): void {
    this.renderer.addClass(this.element.nativeElement, this.mode);
  }

  public ngOnChanges(changes: SimpleChanges): void {
    const { mode } = changes;

    if (!mode || mode.isFirstChange()) {
      return;
    }

    if (mode.previousValue) {
      this.renderer.removeClass(this.element.nativeElement, mode.previousValue);
    }

    if (mode.currentValue) {
      this.renderer.addClass(this.element.nativeElement, mode.currentValue);
    }
  }

}
