import { ChangeDetectionStrategy, Component, HostBinding, Injector, Input, OnInit } from '@angular/core';
import { CellDirective } from '../../directives/CellDirective';
import { CellInterface } from '../../interfaces/CellInterface';
import { DateCellEnum } from '../../types/DateCellEnum';

@Component({
  selector: 'ar-week-cell',
  template: `<ng-container *cdkPortalOutlet></ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeekCellComponent extends CellDirective implements OnInit, CellInterface {

  @HostBinding('class.ar-week-cell') className = true;

  @Input() type: 'argon-cell-week' | string;

  public cellType = DateCellEnum.week;

  constructor(injector: Injector) {
    super(injector);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    const view = this.dateViewFactoryService.createWeekCellView(
      this.viewContainerRef, this.injector, this.componentFactoryResolver, this.type
    );
    this.attachView(view);
  }

}
