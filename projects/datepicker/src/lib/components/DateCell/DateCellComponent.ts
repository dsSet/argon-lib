import { ChangeDetectionStrategy, Component, HostBinding, Injector, Input, OnInit } from '@angular/core';
import { CellDirective } from '../../directives/CellDirective';
import { CellInterface } from '../../interfaces/CellInterface';
import { DateCellEnum } from '../../types/DateCellEnum';

@Component({
  selector: 'ar-date-cell',
  template: `<ng-container *cdkPortalOutlet></ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateCellComponent extends CellDirective implements OnInit, CellInterface {

  @HostBinding('class.ar-date-cell') className = true;

  @Input() type: 'argon-cell-date' | string;

  public cellType = DateCellEnum.day;

  constructor(injector: Injector) {
    super(injector);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    const view = this.dateViewFactoryService.createDateCellView(
      this.viewContainerRef, this.injector, this.componentFactoryResolver, this.type
    );
    this.attachView(view);
  }

}
