import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component,
  HostBinding,
  Input, OnChanges,
  OnInit, SimpleChanges,
} from '@angular/core';
import { DatePickerStateService } from '../../services/DatePickerStateService';
import { DateEventService } from '../../services/DateEventService';
import { Subscription } from 'rxjs';
import { DatePickerStateInterface } from '../../interfaces/DatePickerStateInterface';
import { DateFormatService } from '@argon/date';
import isEqual from 'lodash/isEqual';
import { DatePickerModeEnum } from '../../types/DatePickerModeEnum';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'ar-date-picker',
  templateUrl: './DatePickerComponent.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    DatePickerStateService,
    DateEventService,
  ],
  animations: [
    trigger('toggleDayView', [
      transition('day => month, month => year', [
        style({ transform: 'scale(1.3)', opacity: 0 }),
        animate('0.2s', style({ opacity: 1, transform: 'scale(1)' })),
      ]),
      transition('month => day, year => month', [
        style({ transform: 'scale(0.7)', opacity: 0 }),
        animate('0.2s', style({ opacity: 1, transform: 'scale(1)' }))
      ])
    ])
  ]
})
export class DatePickerComponent implements OnInit, OnChanges {

  @HostBinding('class.ar-date-picker') className = true;

  @Input() config: Partial<DatePickerStateInterface>;

  public viewModes = DatePickerModeEnum;

  private stateSubscription: Subscription;

  constructor(
    public stateService: DatePickerStateService,
    public dateFormatService: DateFormatService,
    private changeDetectorRef: ChangeDetectorRef,
  ) { }

  ngOnInit(): void {
    this.stateSubscription = this.stateService.stateSubject.subscribe(this.handleStateChange);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { config } = changes;

    if (config && config.currentValue && !isEqual(config.previousValue, config.currentValue)) {
      this.stateService.setState(config.currentValue);
    }
  }

  private handleStateChange = () => {
    this.changeDetectorRef.markForCheck();
  }

}
