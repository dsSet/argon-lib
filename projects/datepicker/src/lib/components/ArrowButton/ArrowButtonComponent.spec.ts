import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ArrowButtonComponent } from './ArrowButtonComponent';
import { DateViewFactoryService } from '../../services/DateViewFactoryService';
import { ComponentFactoryResolver, Injector, ViewContainerRef } from '@angular/core';
import { CdkPortalOutlet, ComponentPortal, PortalModule } from '@angular/cdk/portal';
import { HostBindingTestHelper } from 'test/helpers/HostBindingTestHelper';

describe('ArrowButtonComponent', () => {

  let component: ArrowButtonComponent;
  let fixture: ComponentFixture<ArrowButtonComponent>;

  class DateViewFactoryServiceMock {
    public portal = new ComponentPortal(null);
    public createArrowButtonView(
      viewContainerRef: ViewContainerRef,
      injector: Injector,
      componentFactoryResolver: ComponentFactoryResolver
    ): ComponentPortal<any> {
      return this.portal;
    }
  }

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [PortalModule],
    declarations: [ ArrowButtonComponent ],
    providers: [
      { provide: DateViewFactoryService, useClass: DateViewFactoryServiceMock }
    ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrowButtonComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-date-arrow-button class', () => {
    spyOn(component.portalOutlet, 'attach');
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-date-arrow-button');
  });

  it('disabled should toggle .disabled class', () => {
    spyOn(component.portalOutlet, 'attach');
    HostBindingTestHelper.testToggleClass(component, fixture, 'disabled', 'disabled');
  });

  it('flip should toggle .disabled class', () => {
    spyOn(component.portalOutlet, 'attach');
    HostBindingTestHelper.testToggleClass(component, fixture, 'flip', 'flip');
  });

  it('should have initial values', () => {
    expect(component.className).toEqual(true);
    expect(component.disabled).toBeFalsy();
    expect(component.flip).toBeFalsy();
    expect(component.portalOutlet instanceof CdkPortalOutlet).toBeTruthy();
  });

  it('should attach view on init', () => {
    const service: DateViewFactoryServiceMock = TestBed.inject(DateViewFactoryService) as any;
    spyOn(component.portalOutlet, 'attach');
    spyOn(service, 'createArrowButtonView').and.returnValue(new ComponentPortal(null));

    fixture.detectChanges();

    expect(service.createArrowButtonView).toHaveBeenCalled();
    expect(component.portalOutlet.attach).toHaveBeenCalledWith(new ComponentPortal(null));
  });
});
