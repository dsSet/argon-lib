import {
  ChangeDetectionStrategy,
  Component,
  ComponentFactoryResolver, HostBinding,
  Injector, Input,
  OnInit,
  ViewChild
} from '@angular/core';
import { DateViewFactoryService } from '../../services/DateViewFactoryService';
import { CdkPortalOutlet } from '@angular/cdk/portal';

@Component({
  selector: 'ar-date-arrow-button',
  template: '<ng-container *cdkPortalOutlet></ng-container>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArrowButtonComponent implements OnInit {

  @HostBinding('class.ar-date-arrow-button') className = true;

  @HostBinding('class.disabled') @Input() disabled: boolean;

  @HostBinding('class.flip') @Input() flip: boolean;

  @ViewChild(CdkPortalOutlet, { read: CdkPortalOutlet, static: true }) portalOutlet: CdkPortalOutlet;

  constructor(
    private dateViewFactoryService: DateViewFactoryService,
    private injector: Injector,
    private componentFactoryResolver: ComponentFactoryResolver
  ) { }

  ngOnInit(): void {
    const view = this.dateViewFactoryService.createArrowButtonView(null, this.injector, this.componentFactoryResolver);
    this.portalOutlet.attach(view);
  }



}
