import { ChangeDetectionStrategy, Component, HostBinding, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DateIntervalEnum, DateTransformService, DatePeriodEnum } from '@argon/date';
import { DatePickerStateService } from '../../services/DatePickerStateService';
import { DatePickerModeEnum } from '../../types/DatePickerModeEnum';

@Component({
  selector: 'ar-date-picker-months-view',
  templateUrl: './DatePickerMonthsViewComponent.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatePickerMonthsViewComponent implements OnChanges {

  @HostBinding('class.ar-date-picker-months-view') className = true;

  @Input() date: Date;

  public months: Array<Date>;

  constructor(
    private dateTransformService: DateTransformService,
    public stateService: DatePickerStateService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    const { date } = changes;

    if (!date) {
      return;
    }

    if (date.currentValue) {
      const start = this.dateTransformService.startOf(date.currentValue, DatePeriodEnum.year);
      const end = this.dateTransformService.shiftDate(date.currentValue, ['endOf', DatePeriodEnum.year], ['startOf', DatePeriodEnum.month]);
      this.months = Array.from(this.dateTransformService.getDateIterator(start, end, DateIntervalEnum.month));
    } else {
      this.months = [];
    }
  }

  public handleMonthSelect(date: Date) {
    this.stateService.setView(date, DatePickerModeEnum.day);
  }

}
