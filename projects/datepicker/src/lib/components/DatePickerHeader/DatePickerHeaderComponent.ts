import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostBinding,
  Input,
  OnDestroy,
  OnInit
} from '@angular/core';
import { DatePickerStateService } from '../../services/DatePickerStateService';
import { DateFormatService, DateTransformService, DateIntervalEnum } from '@argon/date';
import { Unsubscribable } from '@argon/tools';
import { Subscription } from 'rxjs';
import { DatePickerModeEnum } from '../../types/DatePickerModeEnum';
import { DatePickerService } from '../../services/DatePickerService';

@Component({
  selector: 'ar-date-picker-header',
  templateUrl: './DatePickerHeaderComponent.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatePickerHeaderComponent implements OnInit, OnDestroy {

  @Input() enableViewToggle = true;

  @HostBinding('class.ar-date-picker-header') className = true;

  public get displayMonth(): string {
    if (this.datePickerStateService.viewMode === DatePickerModeEnum.year) {
      const [start, end] = this.datePickerService.getYearsDisplayRange(this.datePickerStateService.endViewDate);
      return `${this.dateFormatService.formatYear(start)} - ${this.dateFormatService.formatYear(end)}`;
    }

    if (this.datePickerStateService.viewMode === DatePickerModeEnum.month) {
      return this.dateFormatService.formatYear(this.datePickerStateService.endViewDate);
    }

    if (!this.datePickerStateService.hasMultipleViews) {
      return this.dateFormatService.formatMonthYear(this.datePickerStateService.startViewDate);
    }

    if (this.datePickerStateService.startViewDate.getFullYear() === this.datePickerStateService.endViewDate.getFullYear()) {
      const startMonth = this.dateFormatService.formatMonth(this.datePickerStateService.startViewDate);
      const endMonth = this.dateFormatService.formatMonth(this.datePickerStateService.endViewDate);
      const year = this.dateFormatService.formatYear(this.datePickerStateService.endViewDate);
      return `${startMonth} - ${endMonth} ${year}`;
    }

    const startDate = this.dateFormatService.formatMonthYear(this.datePickerStateService.startViewDate);
    const endDate = this.dateFormatService.formatMonthYear(this.datePickerStateService.endViewDate);
    return `${startDate} - ${endDate}`;
  }

  private stateSubscription: Subscription;

  constructor(
    public datePickerStateService: DatePickerStateService,
    public dateFormatService: DateFormatService,
    public dateTransformService: DateTransformService,
    private datePickerService: DatePickerService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.stateSubscription = this.datePickerStateService.stateSubject.subscribe(this.handleStateChange);
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  public handleLabelClick() {
    if (this.enableViewToggle) {
      if (this.datePickerStateService.viewMode === DatePickerModeEnum.day) {
        this.datePickerStateService.setViewMode(DatePickerModeEnum.month);
      } else if (this.datePickerStateService.viewMode === DatePickerModeEnum.month) {
        this.datePickerStateService.setViewMode(DatePickerModeEnum.year);
      }
    }
  }

  public handlePrevMonth() {
    this.datePickerStateService.setViewDate(
      this.dateTransformService.decrement(this.datePickerStateService.viewDate, this.getShiftAmount(), this.getShiftRange())
    );
  }

  public handleNextMonth() {
    this.datePickerStateService.setViewDate(
      this.dateTransformService.increment(this.datePickerStateService.viewDate, this.getShiftAmount(), this.getShiftRange())
    );
  }

  private handleStateChange = () => {
    this.changeDetectorRef.markForCheck();
  }

  private getShiftAmount(): number {
    return this.datePickerStateService.viewMode === DatePickerModeEnum.year
      ? DatePickerService.VIEW_YEARS_COUNT
      : 1;
  }

  private getShiftRange(): DateIntervalEnum {
    switch (this.datePickerStateService.viewMode) {
      case DatePickerModeEnum.day: return DateIntervalEnum.month;
      case DatePickerModeEnum.month: return DateIntervalEnum.year;
      case DatePickerModeEnum.year: return DateIntervalEnum.year;
      default: return DateIntervalEnum.month;
    }
  }


}
