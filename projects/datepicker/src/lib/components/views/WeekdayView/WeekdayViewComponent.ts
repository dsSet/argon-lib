import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ViewCellInterface } from '../../../interfaces/ViewCellInterface';
import { DateFormatService } from '@argon/date';

@Component({
  selector: 'ar-weekday-view',
  template: `{{ dateFormatService.formatWeekday(date) }}`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeekdayViewComponent implements ViewCellInterface {

  public date: Date;

  constructor(
    public dateFormatService: DateFormatService
  ) { }

}
