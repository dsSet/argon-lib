import { Type } from '@angular/core';
import { ArrowButtonViewComponent } from './ArrowButtonView/ArrowButtonViewComponent';
import { DateViewCellComponent } from './DateViewCell/DateViewCellComponent';
import { MonthViewComponent } from './MonthView/MonthViewComponent';
import { MonthViewCellComponent } from './MonthViewCell/MonthViewCellComponent';
import { MonthViewFullComponent } from './MonthViewFull/MonthViewFullComponent';
import { WeekdayViewComponent } from './WeekdayView/WeekdayViewComponent';
import { WeekViewComponent } from './WeekView/WeekViewComponent';
import { YearViewCellComponent } from './YearViewCell/YearViewCellComponent';

export const VIEWS: Array<Type<any>> = [
  ArrowButtonViewComponent,
  DateViewCellComponent,
  MonthViewComponent,
  MonthViewCellComponent,
  MonthViewFullComponent,
  WeekdayViewComponent,
  WeekViewComponent,
  YearViewCellComponent
];
