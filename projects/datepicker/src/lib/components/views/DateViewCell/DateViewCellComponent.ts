import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ViewCellInterface } from '../../../interfaces/ViewCellInterface';
import { DateFormatService } from '@argon/date';

@Component({
  selector: 'ar-date-view',
  template: `{{ dateFormatService.formatDay(date) }}`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateViewCellComponent implements ViewCellInterface {

  public date: Date;

  constructor(
    public dateFormatService: DateFormatService
  ) { }

}
