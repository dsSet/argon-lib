import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ViewCellInterface } from '../../../interfaces/ViewCellInterface';
import { DateFormatService } from '@argon/date';

@Component({
  selector: 'ar-month-view-cell',
  template: `{{ dateFormatService.formatMonth(date) }}`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MonthViewCellComponent implements ViewCellInterface {

  @Input() public date: Date;

  constructor(
    public dateFormatService: DateFormatService
  ) { }

}
