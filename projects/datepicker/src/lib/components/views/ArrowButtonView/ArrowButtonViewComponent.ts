/* tslint:disable:max-line-length */
import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ar-date-arrow-button-view',
  template: `
    <svg viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
      <path d="M4.70456 13.2946C4.31529 12.9053 4.31495 12.2743 4.7038 11.8846L7.87515 8.70633C8.26465 8.31598 8.26465 7.68401 7.87515 7.29366L4.70379 4.11538C4.31495 3.72568 4.31529 3.09465 4.70456 2.70538C5.09414 2.31581 5.72576 2.31581 6.11533 2.70538L10.7028 7.29289C11.0934 7.68341 11.0934 8.31658 10.7028 8.7071L6.11533 13.2946C5.72576 13.6842 5.09414 13.6842 4.70456 13.2946Z" fill="currentColor" />
    </svg>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArrowButtonViewComponent {

  @HostBinding('class.ar-date-arrow-button-view') className = true;

}
