import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ViewCellInterface } from '../../../interfaces/ViewCellInterface';
import { DateFormatService } from '@argon/date';

@Component({
  selector: 'ar-year-view-cell',
  template: `{{ dateFormatService.formatYear(date) }}`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class YearViewCellComponent implements ViewCellInterface {

  @Input() public date: Date;

  constructor(
    public dateFormatService: DateFormatService
  ) { }

}
