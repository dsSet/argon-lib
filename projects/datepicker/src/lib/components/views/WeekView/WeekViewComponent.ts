import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DateFormatService } from '@argon/date';
import { ViewCellInterface } from '../../../interfaces/ViewCellInterface';

@Component({
  selector: 'ar-week-view',
  template: `{{ dateFormatService.formatWeek(date) }}`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeekViewComponent implements ViewCellInterface {

  public date: Date;

  constructor(
    public dateFormatService: DateFormatService
  ) { }

}
