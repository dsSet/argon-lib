import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { DateTransformService } from '@argon/date';
import { MonthDirective } from '../../../directives/MonthDirective';
import { MonthViewInterface } from '../../../interfaces/MonthViewInterface';
import { DatePickerStateService } from '../../../services/DatePickerStateService';
import { DatePickerService } from '../../../services/DatePickerService';

@Component({
  selector: 'ar-month-view-full',
  template: `
    <ar-date-grid mode="date">
      <ar-date-cell
        *ngFor="let date of days"
        [date]="date"
        [interactive]="stateService.isDayInteractive"
        [class.ar-month-view__mute]="isMuted(date)"
      ></ar-date-cell>
    </ar-date-grid>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MonthViewFullComponent extends MonthDirective implements MonthViewInterface {

  constructor(
    protected datePickerService: DatePickerService,
    protected dateTransformService: DateTransformService,
    public readonly stateService: DatePickerStateService,
    public readonly changeDetectorRef: ChangeDetectorRef
  ) {
    super(datePickerService, dateTransformService, stateService, changeDetectorRef);
  }

  public isMuted(date: Date): boolean {
    return date.getMonth() !== this.viewDate.getMonth();
  }

}
