import { ChangeDetectionStrategy, Component, HostBinding, Injector, Input, OnInit } from '@angular/core';
import { CellDirective } from '../../directives/CellDirective';
import { CellInterface } from '../../interfaces/CellInterface';
import { DateCellEnum } from '../../types/DateCellEnum';

@Component({
  selector: 'ar-month-cell',
  template: '<ng-container *cdkPortalOutlet></ng-container>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MonthCellComponent extends CellDirective implements CellInterface, OnInit {

  @HostBinding('class.ar-month-cell') className = true;

  @Input() type: 'argon-cell-month' | string;

  public cellType = DateCellEnum.month;

  constructor(injector: Injector) {
    super(injector);
  }

  public ngOnInit(): void {
    super.ngOnInit();
    const view = this.dateViewFactoryService.createMonthCellView(
      this.viewContainerRef, this.injector, this.componentFactoryResolver, this.type
    );
    this.attachView(view);
  }

}
