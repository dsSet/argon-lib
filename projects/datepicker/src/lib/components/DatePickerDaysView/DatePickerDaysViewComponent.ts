import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component, ComponentFactoryResolver,
  ComponentRef, HostBinding, Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { DateViewFactoryService } from '../../services/DateViewFactoryService';
import { CdkPortalOutlet } from '@angular/cdk/portal';
import { MonthViewInterface } from '../../interfaces/MonthViewInterface';
import { DatePickerStateService } from '../../services/DatePickerStateService';
import { StateInterface } from '@argon/tools';
import { DatePickerStateInterface } from '../../interfaces/DatePickerStateInterface';
import { DatePickerService } from '../../services/DatePickerService';

@Component({
  selector: 'ar-date-picker-days-view',
  templateUrl: './DatePickerDaysViewComponent.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatePickerDaysViewComponent implements OnInit, OnChanges {

  @HostBinding('class.ar-date-picker-days-view') className = true;

  @Input() date: Date;

  @ViewChild(CdkPortalOutlet, { read: CdkPortalOutlet, static: true }) portalOutlet: CdkPortalOutlet;

  public weeks: Array<Date>;

  public weekdays: Array<Date>;

  private viewRef: ComponentRef<MonthViewInterface>;

  constructor(
    private injector: Injector,
    private componentFactoryResolver: ComponentFactoryResolver,
    private dateViewFactoryService: DateViewFactoryService,
    public stateService: DatePickerStateService,
    public datePickerService: DatePickerService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.stateService.stateSubject.subscribe(this.handleStateChange);
    const view = this.dateViewFactoryService.createMonthView(
      null, this.injector, this.componentFactoryResolver, this.stateService.viewDateType
    );
    this.viewRef = this.portalOutlet.attachComponentPortal(view);
    this.setViewDate(this.date, this.stateService.displayWeeks, this.stateService.displayWeekdays);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { date } = changes;

    if (!date || date.isFirstChange()) {
      return;
    }

    this.setViewDate(date.currentValue, this.stateService.displayWeeks, this.stateService.displayWeekdays);
  }

  private setViewDate(date: Date, displayWeeks: boolean, displayWeekdays: boolean) {
    this.viewRef.instance.date = date;
    this.viewRef.instance.changeDetectorRef.markForCheck();
    this.setWeeks(displayWeeks);
    this.setWeekdays(displayWeekdays);
  }

  private setWeeks(displayWeeks: boolean) {
    this.weeks = displayWeeks
      ? this.datePickerService.getMonthWeeks(this.date)
      : [];
  }

  private setWeekdays(displayWeekdays: boolean) {
    this.weekdays = displayWeekdays
      ? this.datePickerService.getWeekdays(this.date)
      : [];
  }

  private handleStateChange = ({ changes }: StateInterface<DatePickerStateInterface>) => {
    const { displayWeeks, displayWeekdays } = changes;
    if (displayWeeks) {
      this.setWeeks(displayWeeks.currentValue);
    }

    if (displayWeekdays) {
      this.setWeeks(displayWeekdays.currentValue);
    }

    if (displayWeekdays || displayWeeks) {
      this.changeDetectorRef.markForCheck();
    }
  }

}
