import { ChangeDetectionStrategy, Component, HostBinding, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DateTransformService, DateIntervalEnum } from '@argon/date';
import { DatePickerStateService } from '../../services/DatePickerStateService';
import { DatePickerModeEnum } from '../../types/DatePickerModeEnum';
import { DatePickerService } from '../../services/DatePickerService';

@Component({
  selector: 'ar-date-picker-years-view',
  templateUrl: './DatePickerYearsViewComponent.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatePickerYearsViewComponent implements OnChanges {

  @HostBinding('class.ar-date-picker-months-view') className = true;

  @Input() date: Date;

  public years: Array<Date>;

  constructor(
    private dateTransformService: DateTransformService,
    private datePickerService: DatePickerService,
    public stateService: DatePickerStateService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    const { date } = changes;

    if (!date) {
      return;
    }

    if (date.currentValue) {
      const [start, end] = this.datePickerService.getYearsDisplayRange(date.currentValue);
      this.years = Array.from(this.dateTransformService.getDateIterator(start, end, DateIntervalEnum.year));
    } else {
      this.years = [];
    }
  }

  public handleMonthSelect(date: Date) {
    this.stateService.setView(date, DatePickerModeEnum.month);
  }

}
