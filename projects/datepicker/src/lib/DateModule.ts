import { NgModule } from '@angular/core';
import { DateCellComponent } from './components/DateCell/DateCellComponent';
import { defaultProviders } from './providers/config';
import { PortalModule } from '@angular/cdk/portal';
import { CellDirective } from './directives/CellDirective';
import { DateGridComponent } from './components/DateGrid/DateGridComponent';
import { CommonModule } from '@angular/common';
import { MonthDirective } from './directives/MonthDirective';
import { WeekdayCellComponent } from './components/WeekdayCell/WeekdayCellComponent';
import { DatePickerComponent } from './components/DatePicker/DatePickerComponent';
import { WeekCellComponent } from './components/WeekCell/WeekCellComponent';
import { DatePluginDirective } from './directives/DatePluginDirective';
import { DatePickerDaysViewComponent } from './components/DatePickerDaysView/DatePickerDaysViewComponent';
import { DatePickerHeaderComponent } from './components/DatePickerHeader/DatePickerHeaderComponent';
import { ArrowButtonComponent } from './components/ArrowButton/ArrowButtonComponent';
import { MonthCellComponent } from './components/MonthCell/MonthCellComponent';
import { DatePickerMonthsViewComponent } from './components/DatePickerMonthsView/DatePickerMonthsViewComponent';
import { YearCellComponent } from './components/YearCell/YearCellComponent';
import { DatePickerYearsViewComponent } from './components/DatePickerYearsView/DatePickerYearsViewComponent';
import { PLUGINS } from './components/plugins/plugins';
import { VIEWS } from './components/views/views';

@NgModule({
    imports: [
        PortalModule,
        CommonModule
    ],
    declarations: [
        CellDirective,
        DateCellComponent,
        DateGridComponent,
        MonthDirective,
        WeekdayCellComponent,
        DatePickerComponent,
        WeekCellComponent,
        DatePluginDirective,
        DatePickerDaysViewComponent,
        DatePickerHeaderComponent,
        ArrowButtonComponent,
        MonthCellComponent,
        DatePickerMonthsViewComponent,
        YearCellComponent,
        DatePickerYearsViewComponent,
        PLUGINS,
        VIEWS
    ],
    exports: [
        DatePickerComponent,
        DatePickerHeaderComponent,
        PLUGINS
    ],
    providers: [
        defaultProviders
    ]
})
export class DateModule {

}
