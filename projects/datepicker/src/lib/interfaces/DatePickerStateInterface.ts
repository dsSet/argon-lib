import { DatePickerModeEnum } from '../types/DatePickerModeEnum';

export class DatePickerStateInterface {

  viewDate: Date;

  viewRange: Array<Date>;

  viewMode: DatePickerModeEnum;

  viewCount: number;

  viewDisplayStrategy: 'left' | 'middle' | 'right';

  viewDateType: 'argon-month-view' | 'argon-month-view-full' | string;

  viewMonthType: string;

  viewYearType: string;

  displayWeekdays: boolean;

  displayWeeks: boolean;

  displayMonthLabel: boolean;

  minDate: Date;

  maxDate: Date;

  isDayInteractive: boolean;

  isWeekInteractive: boolean;

  isWeekdayInteractive: boolean;

  isMonthInteractive: boolean;

  isYearInteractive: boolean;

}
