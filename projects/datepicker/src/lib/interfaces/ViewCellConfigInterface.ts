import { ViewCellInterface } from './ViewCellInterface';
import { ViewConfigInterface } from './ViewConfigInterface';

export interface ViewCellConfigInterface extends ViewConfigInterface<ViewCellInterface> {

}
