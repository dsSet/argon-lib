import { MonthViewInterface } from './MonthViewInterface';
import { ViewConfigInterface } from './ViewConfigInterface';

export interface MonthViewConfigInterface extends ViewConfigInterface<MonthViewInterface> {

}
