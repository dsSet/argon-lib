import { CellInterface } from './CellInterface';

export interface PluginInterface {

  applyPlugin(cell: CellInterface): void;

}
