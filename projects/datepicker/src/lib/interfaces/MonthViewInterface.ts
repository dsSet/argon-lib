import { ChangeDetectorRef } from '@angular/core';

export interface MonthViewInterface {

  date: Date;

  changeDetectorRef: ChangeDetectorRef;

}
