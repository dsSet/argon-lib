import { DateCellEnum } from '../types/DateCellEnum';

export interface CellEventInterface {

  event: MouseEvent;

  source: DateCellEnum;

  date: Date;

  eventType: 'click' | 'mouseenter' | 'mouseleave';

}
