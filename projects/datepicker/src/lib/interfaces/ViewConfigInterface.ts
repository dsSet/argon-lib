import { Type } from '@angular/core';

export interface ViewConfigInterface<TComponent> {

  type: string;

  component: Type<TComponent>;

}
