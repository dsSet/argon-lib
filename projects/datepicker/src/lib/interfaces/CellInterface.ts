import { DateCellEnum } from '../types/DateCellEnum';
import { ElementRef } from '@angular/core';

export interface CellInterface {

  type: string;

  cellType: DateCellEnum;

  date: Date;

  element: ElementRef<HTMLElement>;

  interactive: boolean;

}
