import { Injectable, OnDestroy } from '@angular/core';
import { StateSubject } from '@argon/tools';
import { DatePickerStateInterface } from '../interfaces/DatePickerStateInterface';
import { DatePickerModeEnum } from '../types/DatePickerModeEnum';
import { DatePickerService } from './DatePickerService';
import first from 'lodash/first';
import last from 'lodash/last';
import { Unsubscribable } from '@argon/tools';

@Injectable()
export class DatePickerStateService implements OnDestroy {

  static getDefaultState(): DatePickerStateInterface {
    return {
      viewDate: new Date(),
      viewRange: [new Date()],
      viewMode: DatePickerModeEnum.day,
      viewCount: 1,
      viewDateType: null,
      viewMonthType: null,
      viewYearType: null,
      viewDisplayStrategy: 'left',
      displayWeekdays: true,
      displayWeeks: false,
      displayMonthLabel: false,
      maxDate: null,
      minDate: null,
      isDayInteractive: true,
      isMonthInteractive: true,
      isWeekdayInteractive: false,
      isWeekInteractive: false,
      isYearInteractive: true
    };
  }

  public stateSubject = new StateSubject<DatePickerStateInterface>(DatePickerStateService.getDefaultState());

  public get state(): DatePickerStateInterface {
    return this.stateSubject.state;
  }

  public get viewDate(): Date { return this.state.viewDate; }

  public get viewRange(): Array<Date> { return this.state.viewRange; }

  public get hasMultipleViews(): boolean { return this.state.viewCount > 1; }

  public get startViewDate(): Date { return first(this.viewRange); }

  public get endViewDate(): Date { return last(this.viewRange); }

  public get displayMonthLabel(): boolean { return this.state.displayMonthLabel; }

  public get displayWeekdays(): boolean { return this.state.displayWeekdays; }

  public get displayWeeks(): boolean { return this.state.displayWeeks; }

  public get viewMode(): DatePickerModeEnum { return this.state.viewMode; }

  public get isDayInteractive(): boolean { return this.state.isDayInteractive; }
  public get isMonthInteractive(): boolean { return this.state.isMonthInteractive; }
  public get isWeekdayInteractive(): boolean { return this.state.isWeekdayInteractive; }
  public get isWeekInteractive(): boolean { return this.state.isWeekInteractive; }
  public get isYearInteractive(): boolean { return this.state.isYearInteractive; }

  public get viewDateType(): string { return this.state.viewDateType; }

  constructor(
    private datePickerService: DatePickerService
  ) { }

  @Unsubscribable
  ngOnDestroy(): void { }

  public setViewMode(viewMode: DatePickerModeEnum) {
    this.stateSubject.setState({ viewMode });
  }

  public setViewDate(viewDate: Date) {
    if (Number(viewDate) === Number(this.viewDate)) {
      return;
    }
    const nextViewDate = this.resolveViewDate({ ...this.state, viewDate });
    const viewRange = this.datePickerService.calculateViewDateRange(this.state.viewCount, this.state.viewDisplayStrategy, nextViewDate);
    this.stateSubject.setState({ viewDate: nextViewDate, viewRange });
  }

  public setView(viewDate: Date, viewMode: DatePickerModeEnum) {
    const nextViewDate = this.resolveViewDate({ ...this.state, viewDate });
    const viewRange = this.datePickerService.calculateViewDateRange(this.state.viewCount, this.state.viewDisplayStrategy, nextViewDate);
    this.stateSubject.setState({ viewDate: nextViewDate, viewRange, viewMode });
  }

  public setState(state: Partial<DatePickerStateInterface>) {
    const { viewDate, minDate, maxDate, viewCount, viewDisplayStrategy } = state;
    const nextState = { ...this.state, ...state };
    const stateUpdates = { ...state };

    if (viewDate || minDate || maxDate) {
      nextState.viewDate = this.resolveViewDate(nextState);
      stateUpdates.viewDate = nextState.viewDate;
    }

    if (nextState.viewDate !== this.viewDate || viewCount || viewDisplayStrategy) {
      stateUpdates.viewRange = this.datePickerService.calculateViewDateRange(
        nextState.viewCount, nextState.viewDisplayStrategy, nextState.viewDate
      );
    }

    this.stateSubject.setState(stateUpdates);
  }

  private resolveViewDate(state: DatePickerStateInterface): Date {
    const { viewDate, minDate, maxDate } = state;
    if (minDate && minDate > viewDate) {
      return minDate;
    }

    return maxDate && maxDate < viewDate ? maxDate : viewDate;
  }

}
