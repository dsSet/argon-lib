import { Injectable } from '@angular/core';
import { DateTransformService, DateIntervalEnum, DatePeriodEnum } from '@argon/date';

@Injectable({ providedIn: 'root' })
export class DatePickerService {

  static VIEW_YEARS_COUNT = 25;

  static DAYS_COUNT_IN_MONTH_VIEW = 41;

  constructor(
    private dateTransformService: DateTransformService
  ) { }

  public getWeekdays(date: Date): Array<Date> {
    return Array.from(this.dateTransformService.getDateIterator(
      this.dateTransformService.startOf(date, DatePeriodEnum.isoWeek),
      this.dateTransformService.shiftDate(date, ['endOf', DatePeriodEnum.isoWeek], ['startOf', DatePeriodEnum.day]),
      DateIntervalEnum.day
    ));
  }

  public calculateViewDateRange(viewCount: number, viewDisplayStrategy: 'left' | 'middle' | 'right', viewDate: Date): Array<Date> {
    if (viewCount === 1) {
      return [viewDate];
    }

    const [start, end] = this.getDateViewRange(viewCount, viewDisplayStrategy, viewDate);
    return Array.from(this.dateTransformService.getDateIterator(start, end, DateIntervalEnum.month));
  }

  public getDateViewRange(viewCount: number, viewDisplayStrategy: 'left' | 'middle' | 'right', viewDate: Date): [Date, Date] {
    let start = viewDate;
    let end = viewDate;
    if (viewDisplayStrategy === 'left') {
      start = viewDate;
      end = this.dateTransformService.increment(start, viewCount - 1, DateIntervalEnum.month);
    }

    if (viewDisplayStrategy === 'middle') {
      const decrementDelta = viewCount % 2
        ? Math.ceil(viewCount / 2) - 1
        : Math.ceil(viewCount / 2) - 1;

      const incrementDelta = viewCount % 2
        ? Math.ceil(viewCount / 2) - 1
        : Math.ceil(viewCount / 2);

      start = this.dateTransformService.decrement(viewDate, decrementDelta, DateIntervalEnum.month);
      end = this.dateTransformService.increment(viewDate, incrementDelta, DateIntervalEnum.month);
    }

    if (viewDisplayStrategy === 'right') {
      end = viewDate;
      start = this.dateTransformService.decrement(end, viewCount - 1, DateIntervalEnum.month);
    }

    return [start, end];
  }

  public getMonthWeeks(date: Date): Array<Date> {
    const [start, end] = this.getMonthRange(date);
    return Array.from(this.dateTransformService.getDateIterator(
      start, this.dateTransformService.startOf(end, DatePeriodEnum.isoWeek), DateIntervalEnum.week
    ));
  }

  public getMonthRange(date: Date): [Date, Date] {
    const startDate = this.dateTransformService.startOf(date, DatePeriodEnum.month, DatePeriodEnum.isoWeek);
    const endDate = this.dateTransformService.increment(startDate, DatePickerService.DAYS_COUNT_IN_MONTH_VIEW, DateIntervalEnum.day);
    return [startDate, endDate];
  }

  public getYearsDisplayRange(date: Date): [Date, Date] {
    const delta = Math.ceil(DatePickerService.VIEW_YEARS_COUNT / 2) - 1;
    const start = this.dateTransformService.decrement(date, delta, DateIntervalEnum.year);
    const end = this.dateTransformService.increment(date, delta, DateIntervalEnum.year);
    return [start, end];
  }

}
