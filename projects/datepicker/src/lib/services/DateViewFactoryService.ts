import { ComponentFactoryResolver, Inject, Injectable, Injector, Type, ViewContainerRef } from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';
import { ViewCellInterface } from '../interfaces/ViewCellInterface';
import {
  ARGON_DATE_ARROW_VIEW,
  ARGON_DATE_VIEW_CELL,
  ARGON_DATE_VIEW_CELL_DEFAULT,
  ARGON_MONTH_VIEW,
  ARGON_MONTH_VIEW_CELL,
  ARGON_MONTH_VIEW_CELL_DEFAULT,
  ARGON_MONTH_VIEW_DEFAULT,
  ARGON_WEEK_VIEW_CELL,
  ARGON_WEEK_VIEW_CELL_DEFAULT,
  ARGON_WEEKDAY_VIEW_CELL,
  ARGON_WEEKDAY_VIEW_CELL_DEFAULT,
  ARGON_YEAR_VIEW_CELL,
  ARGON_YEAR_VIEW_CELL_DEFAULT
} from '../providers/config';
import { ViewCellConfigInterface } from '../interfaces/ViewCellConfigInterface';
import find from 'lodash/find';
import { MonthViewInterface } from '../interfaces/MonthViewInterface';
import { MonthViewConfigInterface } from '../interfaces/MonthViewConfigInterface';
import { ViewConfigInterface } from '../interfaces/ViewConfigInterface';


@Injectable({ providedIn: 'root' })
export class DateViewFactoryService {

  constructor(
    @Inject(ARGON_DATE_VIEW_CELL) private dateCellView: Array<ViewCellConfigInterface>,
    @Inject(ARGON_DATE_VIEW_CELL_DEFAULT) private dateCellViewDefaultType: string,
    @Inject(ARGON_MONTH_VIEW_CELL) private monthCellView: Array<ViewCellConfigInterface>,
    @Inject(ARGON_MONTH_VIEW_CELL_DEFAULT) private monthCellViewDefaultType: string,
    @Inject(ARGON_YEAR_VIEW_CELL) private yearCellView: Array<ViewCellConfigInterface>,
    @Inject(ARGON_YEAR_VIEW_CELL_DEFAULT) private yearCellViewDefaultType: string,
    @Inject(ARGON_WEEK_VIEW_CELL) private weekCellView: Array<ViewCellConfigInterface>,
    @Inject(ARGON_WEEK_VIEW_CELL_DEFAULT) private weekCellViewDefaultType: string,
    @Inject(ARGON_WEEKDAY_VIEW_CELL) private weekdayCellView: Array<ViewCellConfigInterface>,
    @Inject(ARGON_WEEKDAY_VIEW_CELL_DEFAULT) private weekdayCellViewDefaultType: string,
    @Inject(ARGON_MONTH_VIEW) private monthView: Array<MonthViewConfigInterface>,
    @Inject(ARGON_MONTH_VIEW_DEFAULT) private monthViewDefault: string,
    @Inject(ARGON_DATE_ARROW_VIEW) private arrowButtonView: Type<any>
  ) { }

  public createArrowButtonView(
    viewContainerRef: ViewContainerRef,
    injector: Injector,
    componentFactoryResolver: ComponentFactoryResolver
  ): ComponentPortal<any> {
    return new ComponentPortal(this.arrowButtonView, viewContainerRef, injector, componentFactoryResolver);
  }

  /**
   * Create view for displaying days of month
   */
  public createMonthView(
    viewContainerRef: ViewContainerRef,
    injector: Injector,
    componentFactoryResolver: ComponentFactoryResolver,
    type?: string,
  ): ComponentPortal<MonthViewInterface> {
    const config = this.getViewConfig(this.monthView, type || this.monthViewDefault);
    return this.createPortal(viewContainerRef, injector, componentFactoryResolver, config);
  }

  /**
   * Display week number
   */
  public createWeekCellView(
    viewContainerRef: ViewContainerRef,
    injector: Injector,
    componentFactoryResolver: ComponentFactoryResolver,
    type?: string,
  ): ComponentPortal<ViewCellInterface> {
    const config = this.getViewConfig(this.weekCellView, type || this.weekCellViewDefaultType);
    return this.createPortal(viewContainerRef, injector, componentFactoryResolver, config);
  }

  /**
   * Display day of week (su, mo and ect.)
   */
  public createWeekdayCellView(
    viewContainerRef: ViewContainerRef,
    injector: Injector,
    componentFactoryResolver: ComponentFactoryResolver,
    type?: string,
  ): ComponentPortal<ViewCellInterface> {
    const config = this.getViewConfig(this.weekdayCellView, type || this.weekdayCellViewDefaultType);
    return this.createPortal(viewContainerRef, injector, componentFactoryResolver, config);
  }

  /**
   * Display day of month
   */
  public createDateCellView(
    viewContainerRef: ViewContainerRef,
    injector: Injector,
    componentFactoryResolver: ComponentFactoryResolver,
    type?: string,
  ): ComponentPortal<ViewCellInterface> {
    const config = this.getViewConfig(this.dateCellView, type || this.dateCellViewDefaultType);
    return this.createPortal(viewContainerRef, injector, componentFactoryResolver, config);
  }

  /**
   * Display month
   */
  public createMonthCellView(
    viewContainerRef: ViewContainerRef,
    injector: Injector,
    componentFactoryResolver: ComponentFactoryResolver,
    type?: string,
  ): ComponentPortal<ViewCellInterface> {
    const config = this.getViewConfig(this.monthCellView, type || this.monthCellViewDefaultType);
    return this.createPortal(viewContainerRef, injector, componentFactoryResolver, config);
  }

  /**
   * Display year
   */
  public createYearCellView(
    viewContainerRef: ViewContainerRef,
    injector: Injector,
    componentFactoryResolver: ComponentFactoryResolver,
    type?: string,
  ): ComponentPortal<ViewCellInterface> {
    const config = this.getViewConfig(this.yearCellView, type || this.yearCellViewDefaultType);
    return this.createPortal(viewContainerRef, injector, componentFactoryResolver, config);
  }

  private createPortal<TComponent>(
    viewContainerRef: ViewContainerRef,
    injector: Injector,
    componentFactoryResolver: ComponentFactoryResolver,
    config: ViewConfigInterface<TComponent>
  ) {
    return new ComponentPortal<TComponent>(config.component, viewContainerRef, injector, componentFactoryResolver);
  }

  private getViewConfig<TComponent>(
    views: Array<ViewConfigInterface<TComponent>>, type: string
  ): ViewConfigInterface<TComponent> {
    const config = find(views, { type });

    if (!config) {
      throw new Error(`Argon: could not find ${type}. Allowed types ${views.map(view => view.type)}`);
    }

    return config;
  }

}
