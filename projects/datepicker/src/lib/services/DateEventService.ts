import { Injectable, OnDestroy } from '@angular/core';
import { CellEventInterface } from '../interfaces/CellEventInterface';
import { Subject } from 'rxjs';
import { PluginInterface } from '../interfaces/PluginInterface';
import without from 'lodash/without';
import each from 'lodash/each';
import { CellInterface } from '../interfaces/CellInterface';
import { Unsubscribable } from '@argon/tools';

@Injectable()
export class DateEventService implements OnDestroy {

  public cellEventSubject = new Subject<CellEventInterface>();

  public pluginEventSubject = new Subject<void>();

  public plugins: Array<PluginInterface> = [];

  @Unsubscribable
  ngOnDestroy(): void { }

  public dispatchEvent(event: CellEventInterface) {
    this.cellEventSubject.next(event);
  }

  public registerPlugin(plugin: PluginInterface) {
    this.plugins = [...this.plugins, plugin];
    // this.pluginEventSubject.next();
  }

  public unregisterPlugin(plugin: PluginInterface) {
    this.plugins = without(this.plugins, plugin);
  }

  public applyPlugins(cell: CellInterface) {
    each(this.plugins, (plugin: PluginInterface) => plugin.applyPlugin(cell));
  }

}
