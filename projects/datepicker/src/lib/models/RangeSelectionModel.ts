import { animationFrameScheduler, BehaviorSubject, Observable, scheduled } from 'rxjs';
import { debounceTime, filter, map, mergeAll } from 'rxjs/operators';

export class RangeSelectionModel {

  public readonly selection = new BehaviorSubject<[Date, Date]>([null, null]);

  public readonly complete: Observable<[Date, Date]>;

  public readonly changes: Observable<void>;

  public readonly hoverDate = new BehaviorSubject<Date>(null);

  public get start(): Date | null { return this.selection.value[0]; }

  public get hover(): Date | null { return this.hoverDate.value; }

  public get end(): Date | null { return this.selection.value[1]; }

  public get isComplete(): boolean | null { return this.hasStart && this.hasEnd; }

  public get hasStart(): boolean {
    return Boolean(this.start);
  }

  public get hasEnd(): boolean {
    return Boolean(this.end);
  }

  constructor() {
    this.complete = this.selection.pipe(
      filter(([start, end]) => Boolean(start) && Boolean(end))
    );
    this.changes = scheduled([
      this.selection.pipe(map(() => null)),
      this.hoverDate.pipe(map(() => null))
    ], animationFrameScheduler).pipe(mergeAll(), debounceTime(50));
  }

  public setStart(date: Date) {
    this.selection.next([date, this.end]);
  }

  public setEnd(date: Date) {
    this.selection.next([this.start, date]);
  }

  public setHoverDate(date: Date) {
    if (this.hover !== date) {
      this.hoverDate.next(date);
    }
  }

  public removeHoverDate() {
    this.hoverDate.next(null);
  }

  public dispose() {
    this.selection.complete();
    this.hoverDate.complete();
  }

  public clear = () => {
    this.selection.next([null, null]);
  }

  public inSelectionRange(date: Date): boolean {
    if (!this.hasStart) {
      return false;
    }

    if (this.hasStart && Number(date) === Number(this.start)) {
      return true;
    }

    return this.hover && date >= this.start && date <= this.hover;
  }
}
