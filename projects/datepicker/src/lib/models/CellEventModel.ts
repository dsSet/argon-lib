import { CellEventInterface } from '../interfaces/CellEventInterface';
import { DateCellEnum } from '../types/DateCellEnum';

export class CellEventModel implements CellEventInterface {

  constructor(
    public event: MouseEvent,
    public source: DateCellEnum,
    public date: Date,
    public eventType: 'click' | 'mouseenter' | 'mouseleave'
  ) { }

}
