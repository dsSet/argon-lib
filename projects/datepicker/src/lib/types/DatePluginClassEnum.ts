
export enum DatePluginClassEnum {

  primary = 'ar-date-selected',

  secondary = 'ar-date-selected-secondary',

  disabled = 'ar-date-disabled',

  vacation = 'ar-date-vacation',

  today = 'ar-date-today'

}
