
export enum DateCellEnum {

  day = 'day',

  week = 'week',

  weekday = 'weekday',

  month = 'month',

  year = 'year'

}
