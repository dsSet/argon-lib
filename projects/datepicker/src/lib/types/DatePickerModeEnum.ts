
export enum DatePickerModeEnum {

  day = 'day',

  month = 'month',

  year = 'year'

}
