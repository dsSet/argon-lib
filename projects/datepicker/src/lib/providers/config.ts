import { InjectionToken, Provider, Type } from '@angular/core';
import { ViewCellConfigInterface } from '../interfaces/ViewCellConfigInterface';
import { DateViewCellComponent } from '../components/views/DateViewCell/DateViewCellComponent';
import { MonthViewConfigInterface } from '../interfaces/MonthViewConfigInterface';
import { MonthViewComponent } from '../components/views/MonthView/MonthViewComponent';
import { WeekdayViewComponent } from '../components/views/WeekdayView/WeekdayViewComponent';
import { WeekViewComponent } from '../components/views/WeekView/WeekViewComponent';
import { MonthViewFullComponent } from '../components/views/MonthViewFull/MonthViewFullComponent';
import { ArrowButtonViewComponent } from '../components/views/ArrowButtonView/ArrowButtonViewComponent';
import { MonthViewCellComponent } from '../components/views/MonthViewCell/MonthViewCellComponent';
import { YearViewCellComponent } from '../components/views/YearViewCell/YearViewCellComponent';

export const ARGON_DATE_VIEW_CELL = new InjectionToken<ViewCellConfigInterface>('ARGON_DATE_VIEW_CELL');

export const ARGON_MONTH_VIEW_CELL = new InjectionToken<ViewCellConfigInterface>('ARGON_MONTH_VIEW_CELL');

export const ARGON_YEAR_VIEW_CELL = new InjectionToken<ViewCellConfigInterface>('ARGON_YEAR_VIEW_CELL');

export const ARGON_WEEK_VIEW_CELL = new InjectionToken<ViewCellConfigInterface>('ARGON_WEEK_VIEW_CELL');

export const ARGON_WEEKDAY_VIEW_CELL = new InjectionToken<ViewCellConfigInterface>('ARGON_WEEKDAY_VIEW_CELL');

export const ARGON_DATE_VIEW_CELL_DEFAULT = new InjectionToken<string>('ARGON_DATE_VIEW_CELL_DEFAULT');

export const ARGON_MONTH_VIEW_CELL_DEFAULT = new InjectionToken<string>('ARGON_MONTH_VIEW_CELL_DEFAULT');

export const ARGON_YEAR_VIEW_CELL_DEFAULT = new InjectionToken<string>('ARGON_YEAR_VIEW_CELL_DEFAULT');

export const ARGON_WEEK_VIEW_CELL_DEFAULT = new InjectionToken<string>('ARGON_YEAR_VIEW_CELL_DEFAULT');

export const ARGON_WEEKDAY_VIEW_CELL_DEFAULT = new InjectionToken<string>('ARGON_WEEKDAY_VIEW_CELL_DEFAULT');

export const ARGON_MONTH_VIEW = new InjectionToken<MonthViewConfigInterface>('ARGON_MONTH_VIEW');

export const ARGON_MONTH_VIEW_DEFAULT = new InjectionToken<string>('ARGON_YEAR_VIEW_CELL_DEFAULT');

export const ARGON_DATE_ARROW_VIEW = new InjectionToken<Type<any>>('ARGON_DATE_ARROW_VIEW');


export const defaultProviders: Array<Provider> = [
  {
    provide: ARGON_DATE_VIEW_CELL,
    useValue: { component: DateViewCellComponent, type: 'argon-cell-date' } as ViewCellConfigInterface,
    multi: true
  },
  {
    provide: ARGON_MONTH_VIEW_CELL,
    useValue: { component: MonthViewCellComponent, type: 'argon-cell-month' } as ViewCellConfigInterface,
    multi: true
  },
  {
    provide: ARGON_YEAR_VIEW_CELL,
    useValue: { component: YearViewCellComponent, type: 'argon-cell-year' } as ViewCellConfigInterface,
    multi: true
  },
  {
    provide: ARGON_WEEK_VIEW_CELL,
    useValue: { component: WeekViewComponent, type: 'argon-cell-week' } as ViewCellConfigInterface,
    multi: true
  },
  {
    provide: ARGON_WEEKDAY_VIEW_CELL,
    useValue: { component: WeekdayViewComponent, type: 'argon-cell-weekday' } as ViewCellConfigInterface,
    multi: true
  },
  {
    provide: ARGON_DATE_VIEW_CELL_DEFAULT,
    useValue: 'argon-cell-date'
  },
  {
    provide: ARGON_MONTH_VIEW_CELL_DEFAULT,
    useValue: 'argon-cell-month'
  },
  {
    provide: ARGON_YEAR_VIEW_CELL_DEFAULT,
    useValue: 'argon-cell-year'
  },
  {
    provide: ARGON_WEEK_VIEW_CELL_DEFAULT,
    useValue: 'argon-cell-week'
  },
  {
    provide: ARGON_WEEKDAY_VIEW_CELL_DEFAULT,
    useValue: 'argon-cell-weekday'
  },
  {
    provide: ARGON_MONTH_VIEW,
    useValue: { component: MonthViewComponent, type: 'argon-month-view' },
    multi: true
  },
  {
    provide: ARGON_MONTH_VIEW,
    useValue: { component: MonthViewFullComponent, type: 'argon-month-view-full' },
    multi: true
  },
  {
    provide: ARGON_MONTH_VIEW_DEFAULT,
    useValue: 'argon-month-view'
  },
  {
    provide: ARGON_DATE_ARROW_VIEW,
    useValue: ArrowButtonViewComponent
  }
];
