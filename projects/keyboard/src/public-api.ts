/*
 * Public API Surface of keyboard
 */

export { KeyboardModule } from './lib/KeyboardModule';
export { KeyboardService } from './lib/services/KeyboardService';
export { HotKeyModel } from './lib/models/HotKeyModel';
export { KeyCodesEnum, ARROW_KEYS, ALPHABETIC_KEYS, NUMERIC_KEYS, ALPHANUMERIC_KEYS, NAVIGATION_KEYS } from './lib/types/KeyCodesEnum';

