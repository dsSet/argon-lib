import { KeyCodesEnum } from '../types/KeyCodesEnum';

export class HotKeyModel {

  static getEventCode(event: KeyboardEvent): KeyCodesEnum {
    return event.code as KeyCodesEnum;
  }

  constructor(
    public key: KeyCodesEnum,
    public altKey: boolean = false,
    public ctrlKey: boolean = false,
    public shiftKey: boolean = false,
  ) { }

  public match(event: KeyboardEvent): boolean {
    return HotKeyModel.getEventCode(event) === this.key
      && this.altKey === event.altKey
      && this.ctrlKey === event.ctrlKey
      && this.shiftKey === event.shiftKey;
  }
}
