import { KeyCodesEnum } from '../types/KeyCodesEnum';
import { HotKeyModel } from './HotKeyModel';

describe('HotKeyModel', () => {
  it('match should resolve keyboard event', () => {
    const event = new KeyboardEvent('keyup', { code: KeyCodesEnum.keyB });
    const event2 = new KeyboardEvent('keyup', { code: KeyCodesEnum.keyA });
    const model = new HotKeyModel(KeyCodesEnum.keyB);
    expect(model.match(event)).toBeTruthy();
    expect(model.match(event2)).toBeFalsy();
  });

  it('match should resolve keyboard event with modifiers', () => {
    const code = KeyCodesEnum.keyA;
    const events = [
      new KeyboardEvent('keyup', { code, altKey: true, shiftKey: true, ctrlKey: true }),
      new KeyboardEvent('keyup', { code, altKey: true, shiftKey: true, ctrlKey: false }),
      new KeyboardEvent('keyup', { code, altKey: true, shiftKey: false, ctrlKey: true }),
      new KeyboardEvent('keyup', { code, altKey: true, shiftKey: false, ctrlKey: false }),
      new KeyboardEvent('keyup', { code, altKey: false, shiftKey: true, ctrlKey: true }),
      new KeyboardEvent('keyup', { code, altKey: false, shiftKey: true, ctrlKey: false }),
      new KeyboardEvent('keyup', { code, altKey: false, shiftKey: false, ctrlKey: true }),
      new KeyboardEvent('keyup', { code, altKey: false, shiftKey: false, ctrlKey: false }),
    ];

    for (let i = 0; i < events.length; i++) {
      const event = events[i];
      const model = new HotKeyModel(code, event.altKey, event.ctrlKey, event.shiftKey);

      expect(model.match(event)).toBeTruthy();

      for (let j = 0; j < events.length; j++) {
        if (i !== j) {
          const invalidEvent = events[j];
          expect(model.match(invalidEvent)).toBeFalsy();
        }
      }
    }
  });
});
