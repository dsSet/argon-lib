import indexOf from 'lodash/indexOf';
import { Injectable } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/operators';
import { KeyCodesEnum } from '../types/KeyCodesEnum';
import { HotKeyModel } from '../models/HotKeyModel';

@Injectable({ providedIn: 'root' })
export class KeyboardService {

  static KEY_UP_DEBOUNCE = 40;

  public createFromSource(codes: Array<KeyCodesEnum>, source: Observable<KeyboardEvent>): Observable<KeyboardEvent> {
    return source.pipe(
      filter(this.createKeyCodeFilter(codes)),
      debounceTime(KeyboardService.KEY_UP_DEBOUNCE),
      distinctUntilChanged()
    );
  }

  public listenKeyDownEvent(codes: Array<KeyCodesEnum>, target?: HTMLElement): Observable<KeyboardEvent> {
    const eventListener = this.createKeyboardEventListener('keydown', target);
    return eventListener.pipe(
      filter(this.createKeyCodeFilter(codes)),
      debounceTime(KeyboardService.KEY_UP_DEBOUNCE),
      distinctUntilChanged()
    );
  }

  public listenHotKey(hotKey: HotKeyModel): Observable<KeyboardEvent> {
    const eventListener = this.createKeyboardEventListener('keyup');
    return eventListener.pipe(
      filter((event: KeyboardEvent) => hotKey.match(event)),
      debounceTime(KeyboardService.KEY_UP_DEBOUNCE),
      distinctUntilChanged()
    );
  }

  private getEventCode(event: KeyboardEvent): KeyCodesEnum {
    return event.code as KeyCodesEnum;
  }

  private createKeyboardEventListener(type: string, target?: HTMLElement): Observable<KeyboardEvent> {
    return fromEvent<KeyboardEvent>(target || document, type);
  }

  private createKeyCodeFilter(codes: Array<KeyCodesEnum>): (event: KeyboardEvent) => boolean {
    return (event: KeyboardEvent) => indexOf(codes, this.getEventCode(event)) >= 0;
  }
}
