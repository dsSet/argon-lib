import { KeyboardService } from './KeyboardService';
import { Subscription } from 'rxjs';
import { KeyCodesEnum } from '../types/KeyCodesEnum';
import { fakeAsync, tick } from '@angular/core/testing';
import { HotKeyModel } from '../models/HotKeyModel';

describe('KeyboardService', () => {
  let service: KeyboardService;
  let subscription: Subscription;
  beforeEach(() => {
    service = new KeyboardService();
  });

  afterEach(() => {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  });

  it('listenKeyDownEvent should handle keydown events', () => {
    const event = new KeyboardEvent('keydown', { code: KeyCodesEnum.Enter });
    subscription = service.listenKeyDownEvent([KeyCodesEnum.Enter]).subscribe((result: KeyboardEvent) => {
      expect(result).toEqual(event);
    });
    document.dispatchEvent(event);
  });

  it('listenKeyDownEvent should filter keydown', fakeAsync(() => {
    const actualResult = [];
    const eventEnter = new KeyboardEvent('keydown', { code: KeyCodesEnum.Enter });
    const eventQ = new KeyboardEvent('keydown', { code: KeyCodesEnum.keyQ });
    subscription = service.listenKeyDownEvent([KeyCodesEnum.Enter]).subscribe((result: KeyboardEvent) => {
      actualResult.push(result);
    });
    document.dispatchEvent(eventQ);
    tick(KeyboardService.KEY_UP_DEBOUNCE);
    expect(actualResult).toEqual([]);

    document.dispatchEvent(eventEnter);
    tick(KeyboardService.KEY_UP_DEBOUNCE);
    expect(actualResult).toEqual([eventEnter]);
  }));

  it('listenKeyDownEvent should distinct event', fakeAsync(() => {
    const actualResult = [];
    const eventEnter = new KeyboardEvent('keydown', { code: KeyCodesEnum.Enter });
    const eventEnter2 = new KeyboardEvent('keydown', { code: KeyCodesEnum.Enter });
    const eventEnter3 = new KeyboardEvent('keydown', { code: KeyCodesEnum.Enter });
    subscription = service.listenKeyDownEvent([KeyCodesEnum.Enter]).subscribe((result: KeyboardEvent) => {
      actualResult.push(result);
    });
    document.dispatchEvent(eventEnter);
    tick(KeyboardService.KEY_UP_DEBOUNCE / 2);
    expect(actualResult).toEqual([]);

    document.dispatchEvent(eventEnter2);
    tick(KeyboardService.KEY_UP_DEBOUNCE);
    expect(actualResult).toEqual([eventEnter2]);

    document.dispatchEvent(eventEnter3);
    tick(KeyboardService.KEY_UP_DEBOUNCE);
    expect(actualResult).toEqual([eventEnter2, eventEnter3]);
  }));

  it('listenHotKey should handle events by HotKeyModel model', () => {
    const hotKey = new HotKeyModel(KeyCodesEnum.keyQ, true);
    const event = new KeyboardEvent('keyup', { code: KeyCodesEnum.keyQ, altKey: true });
    subscription = service.listenHotKey(hotKey).subscribe((result: KeyboardEvent) => {
      expect(result).toEqual(event);
    });
    document.dispatchEvent(event);
  });

  it('listenHotKey should filter events', fakeAsync(() => {
    const actualResult = [];
    const hotKey = new HotKeyModel(KeyCodesEnum.keyQ, true, true);
    const event = new KeyboardEvent('keyup', { code: KeyCodesEnum.keyQ, altKey: true });
    subscription = service.listenHotKey(hotKey).subscribe((result: KeyboardEvent) => {
      actualResult.push(result);
    });
    document.dispatchEvent(event);
    tick(KeyboardService.KEY_UP_DEBOUNCE);
    expect(actualResult).toEqual([]);
  }));

  it('listenHotKey should distinct events', fakeAsync(() => {
    const actualResult = [];
    const hotKey = new HotKeyModel(KeyCodesEnum.keyQ, true);
    const event = new KeyboardEvent('keyup', { code: KeyCodesEnum.keyQ, altKey: true });
    const event2 = new KeyboardEvent('keyup', { code: KeyCodesEnum.keyQ, altKey: true });
    const event3 = new KeyboardEvent('keyup', { code: KeyCodesEnum.keyQ, altKey: true });
    subscription = service.listenHotKey(hotKey).subscribe((result: KeyboardEvent) => {
      actualResult.push(result);
    });

    document.dispatchEvent(event);
    tick(KeyboardService.KEY_UP_DEBOUNCE);
    expect(actualResult).toEqual([event]);

    document.dispatchEvent(event2);
    tick(KeyboardService.KEY_UP_DEBOUNCE / 2);
    expect(actualResult).toEqual([event]);

    document.dispatchEvent(event3);
    tick(KeyboardService.KEY_UP_DEBOUNCE);
    expect(actualResult).toEqual([event, event3]);
  }));
});
