
export enum KeyCodesEnum {
  Esc = 'Escape',
  Enter = 'Enter',
  Space = 'Space',

  Backspace = 'Backspace',
  Delete = 'Delete',

  keyQ = 'KeyQ',
  keyW = 'KeyW',
  keyE = 'KeyE',
  keyR = 'KeyR',
  keyT = 'KeyT',
  keyY = 'KeyY',
  keyU = 'KeyU',
  keyI = 'KeyI',
  keyO = 'KeyO',
  keyP = 'KeyP',
  keyA = 'KeyA',
  keyS = 'KeyS',
  keyD = 'KeyD',
  keyF = 'KeyF',
  keyG = 'KeyG',
  keyH = 'KeyH',
  keyJ = 'KeyJ',
  keyK = 'KeyK',
  keyL = 'KeyL',
  keyZ = 'KeyZ',
  keyX = 'KeyX',
  keyC = 'KeyC',
  keyV = 'KeyV',
  keyB = 'KeyB',
  keyN = 'KeyN',
  keyM = 'KeyM',

  keyBracketLeft = 'BracketLeft',
  keyBracketRight = 'BracketRight',
  keyBackquote = 'Backquote',
  keyMinus = 'Minus',
  keyEqual = 'Equal',
  keySemicolon = 'Semicolon',
  keyQuote = 'Quote',
  keyBackslash = 'Backslash',
  keyComma = 'Comma',
  keyPeriod = 'Period',
  keySlash = 'Slash',

  digit0 = 'Digit0',
  digit1 = 'Digit1',
  digit2 = 'Digit2',
  digit3 = 'Digit3',
  digit4 = 'Digit4',
  digit5 = 'Digit5',
  digit6 = 'Digit6',
  digit7 = 'Digit7',
  digit8 = 'Digit8',
  digit9 = 'Digit9',

  arrowUp = 'ArrowUp',
  arrowDown = 'ArrowDown',
  arrowLeft = 'ArrowLeft',
  arrowRight = 'ArrowRight',

  home = 'Home',
  end = 'End',
  pageUp = 'PageUp',
  pageDown = 'PageDown'
}

export const ARROW_KEYS = [
  KeyCodesEnum.arrowUp,
  KeyCodesEnum.arrowDown,
  KeyCodesEnum.arrowLeft,
  KeyCodesEnum.arrowRight
];

export const NAVIGATION_KEYS = [
  ...ARROW_KEYS,
  KeyCodesEnum.home,
  KeyCodesEnum.end,
  KeyCodesEnum.pageUp,
  KeyCodesEnum.pageDown
];

export const NUMERIC_KEYS = [
  KeyCodesEnum.digit0,
  KeyCodesEnum.digit1,
  KeyCodesEnum.digit2,
  KeyCodesEnum.digit3,
  KeyCodesEnum.digit4,
  KeyCodesEnum.digit5,
  KeyCodesEnum.digit6,
  KeyCodesEnum.digit7,
  KeyCodesEnum.digit8,
  KeyCodesEnum.digit9
];

export const ALPHABETIC_KEYS = [
  KeyCodesEnum.keyQ,
  KeyCodesEnum.keyW,
  KeyCodesEnum.keyE,
  KeyCodesEnum.keyR,
  KeyCodesEnum.keyT,
  KeyCodesEnum.keyY,
  KeyCodesEnum.keyU,
  KeyCodesEnum.keyI,
  KeyCodesEnum.keyO,
  KeyCodesEnum.keyP,
  KeyCodesEnum.keyA,
  KeyCodesEnum.keyS,
  KeyCodesEnum.keyD,
  KeyCodesEnum.keyF,
  KeyCodesEnum.keyG,
  KeyCodesEnum.keyH,
  KeyCodesEnum.keyJ,
  KeyCodesEnum.keyK,
  KeyCodesEnum.keyL,
  KeyCodesEnum.keyZ,
  KeyCodesEnum.keyX,
  KeyCodesEnum.keyC,
  KeyCodesEnum.keyV,
  KeyCodesEnum.keyB,
  KeyCodesEnum.keyN,
  KeyCodesEnum.keyM,
  KeyCodesEnum.keyBracketLeft,
  KeyCodesEnum.keyBracketRight,
  KeyCodesEnum.keyBackquote,
  KeyCodesEnum.keyMinus,
  KeyCodesEnum.keyEqual,
  KeyCodesEnum.keySemicolon,
  KeyCodesEnum.keyQuote,
  KeyCodesEnum.keyBackslash,
  KeyCodesEnum.keyComma,
  KeyCodesEnum.keyPeriod,
  KeyCodesEnum.keySlash
];

export const ALPHANUMERIC_KEYS = [...NUMERIC_KEYS, ...ALPHABETIC_KEYS];
