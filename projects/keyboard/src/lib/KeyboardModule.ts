import { NgModule } from '@angular/core';
import { KeyboardService } from './services/KeyboardService';

@NgModule({
  providers: [
    KeyboardService
  ]
})
export class KeyboardModule { }
