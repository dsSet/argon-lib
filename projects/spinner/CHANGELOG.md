# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@2.0.1...@argon/spinner@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/spinner





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@1.0.4...@argon/spinner@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/spinner






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@1.0.4...@argon/spinner@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/spinner






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@1.0.3...@argon/spinner@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/spinner





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@1.0.2...@argon/spinner@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/spinner





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@1.0.1...@argon/spinner@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/spinner






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.11...@argon/spinner@1.0.1) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.11...@argon/spinner@1.0.0) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))






## [0.1.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.10...@argon/spinner@0.1.11) (2020-03-24)

**Note:** Version bump only for package @argon/spinner





## [0.1.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.9...@argon/spinner@0.1.10) (2020-03-24)

**Note:** Version bump only for package @argon/spinner






## [0.1.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.8...@argon/spinner@0.1.9) (2019-11-29)

**Note:** Version bump only for package @argon/spinner





## [0.1.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.7...@argon/spinner@0.1.8) (2019-11-22)

**Note:** Version bump only for package @argon/spinner





## [0.1.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.6...@argon/spinner@0.1.7) (2019-10-11)

**Note:** Version bump only for package @argon/spinner





## [0.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.5...@argon/spinner@0.1.6) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.4...@argon/spinner@0.1.5) (2019-10-08)

**Note:** Version bump only for package @argon/spinner





## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.3...@argon/spinner@0.1.4) (2019-09-21)

**Note:** Version bump only for package @argon/spinner






## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.2...@argon/spinner@0.1.3) (2019-09-07)

**Note:** Version bump only for package @argon/spinner





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.1...@argon/spinner@0.1.2) (2019-09-06)


### Bug Fixes

* **spinner:** fix spinner container styles ([d262fc5](https://bitbucket.org/dsSet/argon-lib/commits/d262fc5))





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/spinner@0.1.0...@argon/spinner@0.1.1) (2019-08-10)


### Bug Fixes

* **spinner:** remove cyclic dependency ([b16d8ed](https://bitbucket.org/dsSet/argon-lib/commits/b16d8ed))





# 0.1.0 (2019-08-10)


### Features

* **spinner:** add spinner module ([a0c8b56](https://bitbucket.org/dsSet/argon-lib/commits/a0c8b56))
* **spinner:** implement spinner module ([09958e6](https://bitbucket.org/dsSet/argon-lib/commits/09958e6))
