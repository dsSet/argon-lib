import { moduleMetadata, storiesOf } from '@storybook/angular';
import { SpinnerModule, SPINNER_COMPONENT_PROVIDER, SpinnerProviderInterface } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';
import { CustomSpinnerComponent } from './components/CustomSpinnerComponent';

storiesOf('Argon|spinner/Spinner Container', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        SpinnerModule
      ],
      declarations: [
        CustomSpinnerComponent
      ],
      providers: [
        {
          provide: SPINNER_COMPONENT_PROVIDER,
          useValue: { key: 'custom', component: CustomSpinnerComponent } as SpinnerProviderInterface,
          multi: true
        },
      ],
      entryComponents: [
        CustomSpinnerComponent
      ]
    })
  ).add('Use spinner container', () => ({
  props: {
    active: false
  },
  template: `
    <ar-spinner-container [isSpinnerActive]="active">
      <div style="width: 20rem; height: 20rem; border: 1px solid purple">
        <button class="btn btn-primary" (click)="active = !active">Action inside container</button>
      </div>
    </ar-spinner-container>
    <button class="btn btn-primary" (click)="active = !active">Toggle</button>
  `
})).add('spinnerType property', () => ({
  template: `
    <ar-spinner-container [isSpinnerActive]="true" spinnerType="default">
      <div style="width: 20rem; height: 20rem; border: 1px solid purple"></div>
    </ar-spinner-container>
  `
}));
