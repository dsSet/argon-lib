import { SpinableStrategyInterface } from '../index';
import { Observable, Subject, timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

export class CustomSpinableStrategy implements SpinableStrategyInterface {

  private readonly subject = new Subject<boolean>();

  private readonly spinBus: Observable<any>;

  constructor() {
    this.spinBus = this.subject.pipe(
      switchMap((state: boolean) => timer(500).pipe(map(() => state))),
    );
  }

  setSpinnerState(state: boolean): void {
    this.subject.next(state);
  }

  connect(): Observable<any> {
    return this.spinBus;
  }

  dispose(): void {
    this.subject.complete();
  }

}
