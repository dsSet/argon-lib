import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'story-custom-spinner',
  template: 'Custom spinner...',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomSpinnerComponent { }
