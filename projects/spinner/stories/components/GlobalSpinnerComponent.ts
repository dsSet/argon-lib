import { ChangeDetectionStrategy, Component } from '@angular/core';
import { SpinnerOverlayService } from '../index';
import { timer } from 'rxjs';

@Component({
  selector: 'story-global-spinner',
  template: `
    <button class="btn btn-secondary" (click)="runSpinner(200)">Click to spin (200ms)</button>
    <button class="btn btn-secondary" (click)="runSpinner(500)">Click to spin (500ms)</button>
    <button class="btn btn-secondary" (click)="runSpinner(1000)">Click to spin (1000ms)</button>
    <button class="btn btn-secondary" (click)="runSpinner(5000)">Click to spin (5000ms)</button>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GlobalSpinnerComponent {

  constructor(
    private spinnerOverlayService: SpinnerOverlayService
  ) { }

  public runSpinner(timeout: number) {
    this.spinnerOverlayService.setSpinnerState(true);
    timer(timeout).subscribe(() => {
      this.spinnerOverlayService.setSpinnerState(false);
    });
  }
}
