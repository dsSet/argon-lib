import { DebounceSpinableStrategy, SpinableStrategyInterface } from '../index';

export class CustomDebounceSpinableStrategy extends DebounceSpinableStrategy implements SpinableStrategyInterface {

  debounceTime = 1000;

}
