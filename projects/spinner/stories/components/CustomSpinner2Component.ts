import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'story-custom-spinner-2',
  template: 'Custom spinner 2...',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomSpinner2Component { }
