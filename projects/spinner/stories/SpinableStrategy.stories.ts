import { moduleMetadata, storiesOf } from '@storybook/angular';
import {
  DebounceSpinableStrategy,
  IconSpinnerComponent,
  SPINNER_COMPONENT_PROVIDER,
  SpinnerModule,
  SpinnerProviderInterface,
  SpinableStrategyInterface
} from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';
import { CustomDebounceSpinableStrategy } from './components/CustomDebounceSpinableStrategy';
import { CustomSpinableStrategy } from './components/CustomSpinableStrategy';


storiesOf('Argon|spinner/Spinable strategy', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        SpinnerModule
      ],
      providers: [
        {
          provide: SPINNER_COMPONENT_PROVIDER,
          useValue: {
            key: 'custom-debounce',
            component: IconSpinnerComponent,
            getSpinableStrategy(): SpinableStrategyInterface {
              return new DebounceSpinableStrategy();
            }
          } as SpinnerProviderInterface,
          multi: true
        },
        {
          provide: SPINNER_COMPONENT_PROVIDER,
          useValue: {
            key: 'custom-debounce-override',
            component: IconSpinnerComponent,
            getSpinableStrategy(): SpinableStrategyInterface {
              return new CustomDebounceSpinableStrategy();
            }
          } as SpinnerProviderInterface,
          multi: true
        },
        {
          provide: SPINNER_COMPONENT_PROVIDER,
          useValue: {
            key: 'custom-strategy',
            component: IconSpinnerComponent,
            getSpinableStrategy(): SpinableStrategyInterface {
              return new CustomSpinableStrategy();
            }
          } as SpinnerProviderInterface,
          multi: true
        }
      ],
    })
  ).add('Using DebounceSpinableStrategy', () => ({
  props: {
    active: false
  },
  template: `
    <div *arSpinner="active; type: 'custom-debounce'" style="width: 20rem; height: 20rem; border: 1px solid purple">
      <button class="btn btn-primary" (click)="active = !active">Action inside container</button>
      <p>Spinner will appear with 250ms debounce.</p>
    </div>
    <button class="btn btn-primary" (click)="active = !active">Toggle</button>
  `
})).add('Using NoopSpinableStrategy', () => ({
  props: {
    active: false
  },
  template: `
    <div *arSpinner="active; type: 'default'" style="width: 20rem; height: 20rem; border: 1px solid purple">
      <button class="btn btn-primary" (click)="active = !active">Action inside container</button>
      <p>Spinner will toggle immediately. Using by default.</p>
    </div>
    <button class="btn btn-primary" (click)="active = !active">Toggle</button>
  `
})).add('Override debounce strategy', () => ({
  props: {
    active: false
  },
  template: `
    <div *arSpinner="active; type: 'custom-debounce-override'" style="width: 20rem; height: 20rem; border: 1px solid purple">
      <button class="btn btn-primary" (click)="active = !active">Action inside container</button>
      <p>Spinner will appear with 1000ms debounce.</p>
    </div>
    <button class="btn btn-primary" (click)="active = !active">Toggle</button>
  `
})).add('Use custom strategy', () => ({
  props: {
    active: false
  },
  template: `
    <div *arSpinner="active; type: 'custom-strategy'" style="width: 20rem; height: 20rem; border: 1px solid purple">
      <button class="btn btn-primary" (click)="active = !active">Action inside container</button>
      <p>Spinner will toggle with 500ms debounce.</p>
    </div>
    <button class="btn btn-primary" (click)="active = !active">Toggle</button>
  `
}));
