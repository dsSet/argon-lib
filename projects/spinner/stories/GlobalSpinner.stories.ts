import { moduleMetadata, storiesOf } from '@storybook/angular';
import {
  DebounceSpinableStrategy, GLOBAL_SPINNER_PROVIDER,
  IconSpinnerComponent,
  SpinableStrategyInterface,
  SPINNER_COMPONENT_PROVIDER,
  SpinnerModule, SpinnerProviderInterface
} from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';
import { GlobalSpinnerComponent } from './components/GlobalSpinnerComponent';


storiesOf('Argon|spinner/Global spinner ()', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        SpinnerModule
      ],
      declarations: [
        GlobalSpinnerComponent
      ]
    })
  )
  .add('SpinnerOverlayService usage', () => ({ component: GlobalSpinnerComponent, moduleMetadata: { } }))
  .add('Set up global spinner', () => ({ component: GlobalSpinnerComponent, moduleMetadata: {
    providers: [
      {
        provide: SPINNER_COMPONENT_PROVIDER,
        useValue: {
          key: 'global',
          component: IconSpinnerComponent,
          getSpinableStrategy(): SpinableStrategyInterface {
            return new DebounceSpinableStrategy();
          }
        } as SpinnerProviderInterface,
        multi: true
      },
      { provide: GLOBAL_SPINNER_PROVIDER, useValue: 'global' }
    ]
  }
}));
