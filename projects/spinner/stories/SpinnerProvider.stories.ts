import { moduleMetadata, storiesOf } from '@storybook/angular';
import { SpinnerModule, SPINNER_COMPONENT_PROVIDER, SpinnerProviderInterface } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';
import { CustomSpinnerComponent } from './components/CustomSpinnerComponent';
import { CustomSpinner2Component } from './components/CustomSpinner2Component';


storiesOf('Argon|spinner/Spinner Provider', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        SpinnerModule
      ],
      declarations: [
        CustomSpinnerComponent,
        CustomSpinner2Component
      ],
      providers: [
        {
          provide: SPINNER_COMPONENT_PROVIDER,
          useValue: { key: 'custom', component: CustomSpinnerComponent } as SpinnerProviderInterface,
          multi: true
        },
        {
          provide: SPINNER_COMPONENT_PROVIDER,
          useValue: { key: 'custom2', component: CustomSpinner2Component } as SpinnerProviderInterface,
          multi: true
        }
      ],
      entryComponents: [
        CustomSpinnerComponent,
        CustomSpinner2Component
      ]
    })
  ).add('Use spinner by type', () => ({
  template: `
    <div *arSpinner="true; type: 'custom'" style="width: 20rem; height: 20rem; border: 1px solid purple"></div>
    <div *arSpinner="true; type: 'custom2'" style="width: 20rem; height: 20rem; border: 1px solid purple"></div>
    <div *arSpinner="true; type: 'default'" style="width: 20rem; height: 20rem; border: 1px solid purple"></div>
  `
})).add('Spinner by default (last)', () => ({
  template: `
    <div *arSpinner="true;" style="width: 20rem; height: 20rem; border: 1px solid purple"></div>
  `
}));
