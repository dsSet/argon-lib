import { moduleMetadata, storiesOf } from '@storybook/angular';
import { SpinnerModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

storiesOf('Argon|spinner/Spinner Directive', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        SpinnerModule
      ]
    })
  ).add('Spin container with directive', () => ({
  props: {
    active: false
  },
  template: `
    <div *arSpinner="active" style="width: 20rem; height: 20rem; border: 1px solid purple">
      <button class="btn btn-primary" (click)="active = !active">Action inside container</button>
    </div>
    <button class="btn btn-primary" (click)="active = !active">Toggle</button>
  `
}));
