import {
  ChangeDetectorRef,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  Input,
  OnChanges,
  OnDestroy,
  OnInit, SimpleChanges,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { CdkPortalOutlet } from '@angular/cdk/portal';
import { SpinnerService } from '../services/SpinnerService';
import { SpinnerLayoutComponent } from '../components/SpinnerLayout/SpinnerLayoutComponent';
import { SpinableStrategyInterface } from '../interfaces/SpinableStrategyInterface';
import { SpinnerLayoutService } from '../services/SpinnerLayoutService';

@Directive({
  selector: '[arSpinner]'
})
export class SpinnerDirective extends CdkPortalOutlet implements OnInit, OnDestroy, OnChanges {

  @Input() arSpinnerType: string;

  @Input() arSpinner: boolean;

  public layout: ComponentRef<SpinnerLayoutComponent>;

  protected changeDetector: ChangeDetectorRef;

  protected spinableStrategy: SpinableStrategyInterface;

  constructor(
    componentFactoryResolver: ComponentFactoryResolver,
    viewContainerRef: ViewContainerRef,
    protected templateRef: TemplateRef<any>,
    protected spinnerService: SpinnerService,
    protected spinnerLayoutService: SpinnerLayoutService
  ) {
    super(componentFactoryResolver, viewContainerRef);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.spinableStrategy = this.spinnerService.getSpinableStrategy(this.arSpinnerType);
    this.layout = this.attach(this.spinnerLayoutService.createSpinnerLayoutPortal());
    this.layout.instance.spinnerType = this.arSpinnerType;
    this.layout.instance.template = this.templateRef;
    this.layout.instance.isSpinnerActive = this.arSpinner;
    this.changeDetector = this.layout.injector.get(ChangeDetectorRef);
    this.spinableStrategy.connect().subscribe(this.updateView);
  }

  ngOnDestroy(): void {
    this.dispose();
    this.spinableStrategy.dispose();
    super.ngOnDestroy();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { arSpinner } = changes;
    if (!arSpinner || arSpinner.isFirstChange()) {
      return;
    }

    if (arSpinner.currentValue !== arSpinner.previousValue) {
      this.spinableStrategy.setSpinnerState(arSpinner.currentValue);
    }
  }

  protected updateView = (state: boolean) => {
    this.layout.instance.isSpinnerActive = state;
    this.changeDetector.markForCheck();
  }
}
