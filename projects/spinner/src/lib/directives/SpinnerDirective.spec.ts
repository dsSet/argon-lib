import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SpinnerDirective } from './SpinnerDirective';
import { SpinnerService } from '../services/SpinnerService';
import { SpinnerServiceMock } from '../../test/SpinnerServiceMock';
import { Component, Input, SimpleChange, ViewChild } from '@angular/core';
import { MockSpinnerComponent } from '../../test/MockSpinnerComponent';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { MockSpinnerLayoutComponent } from '../../test/MockSpinnerLayoutComponent';
import { SpinnerLayoutService } from '../services/SpinnerLayoutService';
import { SpinnerLayoutServiceMock } from '../../test/SpinnerLayoutServiceMock';

@Component({
  selector: 'spec-component',
  template: '<div *arSpinner="state; type: type"></div>'
})
class MockComponent {
  @Input() state: boolean;
  @Input() type: string;
  @ViewChild(SpinnerDirective, { static: true }) spinner: SpinnerDirective;
}

describe('SpinnerDirective', () => {
  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;
  let spinnerService: SpinnerServiceMock;
  let spinnerLayoutService: SpinnerLayoutServiceMock;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      declarations: [ SpinnerDirective, MockComponent, MockSpinnerComponent, MockSpinnerLayoutComponent ],
      providers: [
        { provide: SpinnerService, useClass: SpinnerServiceMock },
        { provide: SpinnerLayoutService, useClass: SpinnerLayoutServiceMock }
      ]
    })
    .overrideModule(BrowserDynamicTestingModule, {
      set: {
        entryComponents: [MockSpinnerComponent, MockSpinnerLayoutComponent]
      }
    })
    .compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
    spinnerService = TestBed.inject(SpinnerService) as any;
    spinnerLayoutService = TestBed.inject(SpinnerLayoutService) as any;
  });

  it('ngOnInit should get and connect getSpinableStrategy', () => {
    component.type = 'MockSpinner';
    fixture.detectChanges();
    expect(spinnerService.getSpinableStrategy).toHaveBeenCalledWith(component.type);
    expect(spinnerService.spinableStrategy.connect).toHaveBeenCalled();
  });

  it('ngOnInit should create and configure spinner layout', () => {
    fixture.detectChanges();
    const directive = component.spinner;
    expect(spinnerLayoutService.createSpinnerLayoutPortal).toHaveBeenCalled();
    expect(directive.hasAttached()).toBeTruthy();
  });

  it('ngOnInit should set values to spinner layout', () => {
    component.type = 'MockSpinner';
    component.state = true;
    fixture.detectChanges();
    const directive = component.spinner;
    expect(directive.layout.instance.isSpinnerActive).toEqual(component.state);
    expect(directive.layout.instance.spinnerType).toEqual(component.type);
  });

  it('ngOnDestroy should dispose outlet and strategy', () => {
    fixture.detectChanges();
    const directive = component.spinner;
    spyOn(directive, 'dispose');
    directive.ngOnDestroy();
    expect(spinnerService.spinableStrategy.dispose).toHaveBeenCalled();
    expect(directive.dispose).toHaveBeenCalled();
  });

  it('ngOnChange should not trigger state changes if arSpinner property was not changed', () => {
    fixture.detectChanges();
    const directive = component.spinner;
    directive.ngOnChanges({ });
    expect(spinnerService.spinableStrategy.setSpinnerState).not.toHaveBeenCalled();
  });

  it('ngOnChange should not trigger state changes if first change', () => {
    const arSpinner = new SimpleChange(false, true, true);
    fixture.detectChanges();
    const directive = component.spinner;
    directive.ngOnChanges({ arSpinner });
    expect(spinnerService.spinableStrategy.setSpinnerState).not.toHaveBeenCalled();
  });

  it('ngOnChange should not trigger state changes if arSpinner value not changed', () => {
    const arSpinner = new SimpleChange(true, true, false);
    fixture.detectChanges();
    const directive = component.spinner;
    directive.ngOnChanges({ arSpinner });
    expect(spinnerService.spinableStrategy.setSpinnerState).not.toHaveBeenCalled();
  });

  it('ngOnChange should trigger state changes if arSpinner', () => {
    const arSpinner = new SimpleChange(true, false, false);
    fixture.detectChanges();
    const directive = component.spinner;
    directive.ngOnChanges({ arSpinner });
    expect(spinnerService.spinableStrategy.setSpinnerState).toHaveBeenCalledWith(arSpinner.currentValue);
  });

  it('ngOnChange should update layout spinner state', () => {
    component.state = true;
    const arSpinner = new SimpleChange(component.state, false, false);
    fixture.detectChanges();
    const directive = component.spinner;
    expect(directive.layout.instance.isSpinnerActive).toEqual(arSpinner.previousValue);
    spinnerService.spinableStrategy.stateSubject.next(arSpinner.currentValue);
    expect(directive.layout.instance.isSpinnerActive).toEqual(arSpinner.currentValue);
  });
});
