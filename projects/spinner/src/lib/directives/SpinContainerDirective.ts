import { Directive, HostBinding } from '@angular/core';
import { SpinableSubjectInterface } from '../interfaces/SpinableSubjectInterface';

@Directive({
  selector: '[arSpinContainer]'
})
export class SpinContainerDirective implements SpinableSubjectInterface {

  @HostBinding('class.ar-spinner-layout') className = true;

  @HostBinding('class.ar-spinner-layout--active') isSpinnerActive: boolean;

}
