import { Component } from '@angular/core';
import { SpinContainerDirective } from './SpinContainerDirective';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

@Component({
  selector: 'spec-component',
  template: ''
})
class MockComponent extends SpinContainerDirective { }


describe('SpinContainerDirective', () => {

  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      declarations: [ MockComponent ]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
  });

  it('should have default properties', () => {
    expect(component.className).toBeTruthy();
    expect(component.isSpinnerActive).toBeFalsy();
  });

  it('className should bind .ar-spinner-layout class', () => {
    component.className = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-spinner-layout')).toEqual(true);
    component.className = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-spinner-layout')).toEqual(false);
  });

  it('isSpinnerActive should bind .ar-spinner-layout--active class', () => {
    component.isSpinnerActive = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-spinner-layout--active')).toEqual(true);
    component.isSpinnerActive = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-spinner-layout--active')).toEqual(false);
  });
});
