import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SpinnerServiceMock } from '../../../test/SpinnerServiceMock';
import { SpinnerService } from '../../services/SpinnerService';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SpinnerLayoutComponent } from './SpinnerLayoutComponent';
import { Portal } from '@angular/cdk/portal';


describe('SpinnerLayoutComponent', () => {
  let component: SpinnerLayoutComponent;
  let fixture: ComponentFixture<SpinnerLayoutComponent>;
  let spinnerService: SpinnerServiceMock;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      declarations: [
        SpinnerLayoutComponent
      ],
      providers: [
        { provide: SpinnerService, useClass: SpinnerServiceMock }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerLayoutComponent);
    component = fixture.componentInstance;
    spinnerService = TestBed.inject(SpinnerService) as any;
  });

  it('ngOnInit should create spinner portal', () => {
    component.spinnerType = 'MockSpinner';
    component.ngOnInit();
    expect(spinnerService.createSpinnerPortal).toHaveBeenCalledWith(component.spinnerType);
    expect(component.spinner instanceof Portal).toBeTruthy();
  });
});
