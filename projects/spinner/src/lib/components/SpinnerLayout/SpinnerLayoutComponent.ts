import { ChangeDetectionStrategy, Component, Injector, Input, OnInit, TemplateRef } from '@angular/core';
import { Portal } from '@angular/cdk/portal';
import { SpinnerService } from '../../services/SpinnerService';
import { SpinContainerDirective } from '../../directives/SpinContainerDirective';

@Component({
  selector: 'ar-spinner-layout',
  template: `
    <ng-container [ngTemplateOutlet]="template"></ng-container>
    <ar-spinner-shape *ngIf="isSpinnerActive" [spinner]="spinner"></ar-spinner-shape>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerLayoutComponent extends SpinContainerDirective implements OnInit {

  @Input() isSpinnerActive: boolean;

  @Input() template: TemplateRef<any>;

  @Input() spinnerType: string;

  public spinner: Portal<any>;

  protected spinnerService: SpinnerService;

  constructor(
    private injector: Injector
  ) {
    super();
  }

  ngOnInit(): void {
    this.spinnerService = this.injector.get(SpinnerService);
    this.spinner = this.spinnerService.createSpinnerPortal(this.spinnerType);
  }

}
