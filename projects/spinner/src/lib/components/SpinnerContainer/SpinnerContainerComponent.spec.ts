import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SpinnerService } from '../../services/SpinnerService';
import { SpinnerServiceMock } from '../../../test/SpinnerServiceMock';
import { SpinnerContainerComponent } from './SpinnerContainerComponent';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';


describe('SpinnerContainerComponent', () => {
  let component: SpinnerContainerComponent;
  let fixture: ComponentFixture<SpinnerContainerComponent>;
  let spinnerService: SpinnerServiceMock;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      declarations: [
        SpinnerContainerComponent
      ],
      providers: [
        { provide: SpinnerService, useClass: SpinnerServiceMock }
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerContainerComponent);
    component = fixture.componentInstance;
    spinnerService = TestBed.inject(SpinnerService) as any;
  });

  it('ngOnInit should create spinableStrategy', () => {
    component.spinnerType = 'MockSpinnerType';
    component.ngOnInit();
    expect(spinnerService.getSpinableStrategy).toHaveBeenCalledWith(component.spinnerType);
    expect(spinnerService.spinableStrategy.connect).toHaveBeenCalled();
  });

  it('ngOnDestroy should dispose spinableStrategy', () => {
    component.ngOnInit();
    component.ngOnDestroy();
    expect(spinnerService.spinableStrategy.dispose).toHaveBeenCalled();
  });

  it('ngOnChanges should skip actions if active not changed', () => {
    component.ngOnInit();
    component.ngOnChanges({ });
    expect(spinnerService.spinableStrategy.setSpinnerState).not.toHaveBeenCalled();
  });

  it('ngOnChanges should skip actions if active is first change', () => {
    component.ngOnInit();
    const active = new SimpleChange(true, false, true);
    component.ngOnChanges({ active });
    expect(spinnerService.spinableStrategy.setSpinnerState).not.toHaveBeenCalled();
  });

  it('ngOnChanges should call setSpinnerState', () => {
    component.ngOnInit();
    const active = new SimpleChange(true, false, false);
    component.ngOnChanges({ active });
    expect(spinnerService.spinableStrategy.setSpinnerState).toHaveBeenCalledWith(active.currentValue);
  });

  it('spinableStrategy changes should update component state', () => {
    const state = true;
    expect(component.isSpinnerActive).toBeFalsy();
    component.ngOnInit();
    spinnerService.spinableStrategy.stateSubject.next(state);
    fixture.detectChanges();
    expect(component.isSpinnerActive).toBeTruthy();
  });
});
