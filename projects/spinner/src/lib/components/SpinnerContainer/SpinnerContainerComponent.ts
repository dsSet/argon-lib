import { SpinnerLayoutComponent } from '../SpinnerLayout/SpinnerLayoutComponent';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { SpinableSubjectInterface } from '../../interfaces/SpinableSubjectInterface';
import { SpinableStrategyInterface } from '../../interfaces/SpinableStrategyInterface';


@Component({
  selector: 'ar-spinner-container',
  template: `
    <ng-content></ng-content>
    <ar-spinner-shape *ngIf="isSpinnerActive" [spinner]="spinner"></ar-spinner-shape>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerContainerComponent extends SpinnerLayoutComponent implements SpinableSubjectInterface, OnInit, OnChanges, OnDestroy {

  @Input() isSpinnerActive: boolean;

  @Input() spinnerType: string;

  private spinableStrategy: SpinableStrategyInterface;

  constructor(
    injector: Injector,
    private changeDetector: ChangeDetectorRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.spinableStrategy = this.spinnerService.getSpinableStrategy(this.spinnerType);
    this.spinableStrategy.connect().subscribe(this.updateSpinnerState);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { active } = changes;
    if (!active || active.isFirstChange()) {
      return;
    }

    this.spinableStrategy.setSpinnerState(active.currentValue);
  }

  ngOnDestroy(): void {
    this.spinableStrategy.dispose();
  }

  private updateSpinnerState = (state: boolean) => {
    this.isSpinnerActive = state;
    this.changeDetector.markForCheck();
  }

}
