import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IconSpinnerComponent } from './IconSpinnerComponent';

describe('DropdownContainerComponent', () => {
  let component: IconSpinnerComponent;
  let fixture: ComponentFixture<IconSpinnerComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      declarations: [ IconSpinnerComponent ]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconSpinnerComponent);
    component = fixture.componentInstance;
  });

  it('should have default properties', () => {
    expect(component.className).toBeTruthy();
  });

  it('className should bind .ar-icon-spinner class', () => {
    component.className = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-icon-spinner')).toEqual(true);
    component.className = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-icon-spinner')).toEqual(false);
  });
});
