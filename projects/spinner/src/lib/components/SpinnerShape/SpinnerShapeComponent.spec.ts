import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { SpinnerShapeComponent } from './SpinnerShapeComponent';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('SpinnerShapeComponent', () => {
  let component: SpinnerShapeComponent;
  let fixture: ComponentFixture<SpinnerShapeComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      declarations: [ SpinnerShapeComponent ],
      schemas: [ NO_ERRORS_SCHEMA ]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpinnerShapeComponent);
    component = fixture.componentInstance;
  });

  it('should have default properties', () => {
    expect(component.className).toBeTruthy();
  });

  it('className should bind .ar-spinner-shape class', () => {
    component.className = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-spinner-shape')).toEqual(true);
    component.className = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-spinner-shape')).toEqual(false);
  });
});
