import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';
import { Portal } from '@angular/cdk/portal';

@Component({
  selector: 'ar-spinner-shape',
  template: `
    <ng-container [cdkPortalOutlet]="spinner"></ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerShapeComponent {

  @HostBinding('class.ar-spinner-shape') className = true;

  @Input() spinner: Portal<any>;

}
