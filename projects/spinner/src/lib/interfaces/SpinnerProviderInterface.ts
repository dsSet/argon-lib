import { Type } from '@angular/core';
import { Portal } from '@angular/cdk/portal';
import { SpinableStrategyInterface } from './SpinableStrategyInterface';

export interface SpinnerProviderInterface {

  key: string;

  component: Type<any> | Portal<any>;

  getSpinableStrategy?(): SpinableStrategyInterface;

}
