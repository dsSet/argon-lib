import { Observable } from 'rxjs';


export interface SpinableStrategyInterface {

  setSpinnerState(state: boolean): void;

  connect(): Observable<any>;

  dispose(): void;

}
