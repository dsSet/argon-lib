

export interface SpinableSubjectInterface {

  isSpinnerActive: boolean;

}
