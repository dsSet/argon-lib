import { InjectionToken } from '@angular/core';

export const GLOBAL_SPINNER_PROVIDER = new InjectionToken<string>('GLOBAL_SPINNER_PROVIDER');

