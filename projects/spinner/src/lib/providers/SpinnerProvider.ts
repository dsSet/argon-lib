import { InjectionToken } from '@angular/core';
import { SpinnerProviderInterface } from '../interfaces/SpinnerProviderInterface';
import { IconSpinnerComponent } from '../components/IconSpinner/IconSpinnerComponent';

export const SPINNER_COMPONENT_PROVIDER = new InjectionToken<SpinnerProviderInterface>('SPINNER_COMPONENT_PROVIDER');

export const defaultSpinner: SpinnerProviderInterface = {

  key: 'default',

  component: IconSpinnerComponent

};
