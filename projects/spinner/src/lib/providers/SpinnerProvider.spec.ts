import { defaultSpinner } from './SpinnerProvider';
import { IconSpinnerComponent } from '../components/IconSpinner/IconSpinnerComponent';


describe('SpinnerProvider', () => {

  it('should provide default value', () => {
    expect(defaultSpinner.key).toEqual('default');
    expect(defaultSpinner.component).toEqual(IconSpinnerComponent);
    expect(defaultSpinner.getSpinableStrategy).not.toBeDefined();
  });
});
