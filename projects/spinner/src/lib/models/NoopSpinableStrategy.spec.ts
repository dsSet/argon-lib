import { fakeAsync, tick } from '@angular/core/testing';
import { NoopSpinableStrategy } from './NoopSpinableStrategy';


describe('NoopSpinableStrategy', () => {

  it('should trigger true state immediately', fakeAsync(() => {
    const strategy = new NoopSpinableStrategy();
    let expectedState = false;
    strategy.connect().subscribe((state: boolean) => { expectedState = state; });
    strategy.setSpinnerState(true);
    tick(0);
    expect(expectedState).toEqual(true);
    strategy.dispose();
  }));

  it('should trigger false state immediately', fakeAsync(() => {
    const strategy = new NoopSpinableStrategy();
    let expectedState = true;
    strategy.connect().subscribe((state: boolean) => { expectedState = state; });
    strategy.setSpinnerState(false);
    tick(0);
    expect(expectedState).toEqual(false);
    strategy.dispose();
  }));
});
