import { SpinableStrategyInterface } from '../interfaces/SpinableStrategyInterface';
import { Observable, Subject } from 'rxjs';

export class NoopSpinableStrategy implements SpinableStrategyInterface {

  private spinnerState = new Subject<boolean>();

  public setSpinnerState(state: boolean) {
    this.spinnerState.next(state);
  }

  connect(): Observable<any> {
    return this.spinnerState.asObservable();
  }

  public dispose(): void {
    this.spinnerState.complete();
  }

}
