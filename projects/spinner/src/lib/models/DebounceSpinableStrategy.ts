import { SpinableStrategyInterface } from '../interfaces/SpinableStrategyInterface';
import { Observable, Subject, timer } from 'rxjs';
import { debounce, distinctUntilChanged } from 'rxjs/operators';

export class DebounceSpinableStrategy implements SpinableStrategyInterface {

  public debounceTime = 250;

  private readonly actions = new Subject<boolean>();

  private readonly actionsBus: Observable<any>;

  constructor() {
    this.actionsBus = this.actions.pipe(
      debounce(this.getDebounceTimer),
      distinctUntilChanged()
    );
  }

  public setSpinnerState(state: boolean) {
    this.actions.next(state);
  }

  connect(): Observable<any> {
    return this.actionsBus;
  }

  dispose(): void {
    this.actions.complete();
  }

  private getDebounceTimer = (state: boolean) => {
    return state ? timer(this.debounceTime) : timer(0);
  }

}
