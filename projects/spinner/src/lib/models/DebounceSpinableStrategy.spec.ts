import { DebounceSpinableStrategy } from './DebounceSpinableStrategy';
import { fakeAsync, tick } from '@angular/core/testing';


describe('DebounceSpinableStrategy', () => {

  it('should have default debounceTime value equal 250', () => {
    const strategy = new DebounceSpinableStrategy();
    expect(strategy.debounceTime).toEqual(250);
  });

  it('should trigger true state after debounce timeout', fakeAsync(() => {
    const strategy = new DebounceSpinableStrategy();
    let expectedState = false;
    strategy.connect().subscribe((state: boolean) => { expectedState = state; });
    strategy.setSpinnerState(true);
    expect(expectedState).toEqual(false);
    tick(strategy.debounceTime);
    expect(expectedState).toEqual(true);
    strategy.dispose();
  }));

  it('should trigger false state immediately', fakeAsync(() => {
    const strategy = new DebounceSpinableStrategy();
    let expectedState = true;
    strategy.connect().subscribe((state: boolean) => { expectedState = state; });
    strategy.setSpinnerState(false);
    tick(0);
    expect(expectedState).toEqual(false);
    strategy.dispose();
  }));
});
