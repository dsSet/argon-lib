import { Inject, NgModule, Optional } from '@angular/core';
import { IconSpinnerComponent } from './components/IconSpinner/IconSpinnerComponent';
import { defaultSpinner, SPINNER_COMPONENT_PROVIDER } from './providers/SpinnerProvider';
import { SpinnerService } from './services/SpinnerService';
import { SpinnerDirective } from './directives/SpinnerDirective';
import { PortalModule } from '@angular/cdk/portal';
import { SpinnerLayoutComponent } from './components/SpinnerLayout/SpinnerLayoutComponent';
import { CommonModule } from '@angular/common';
import { SpinnerContainerComponent } from './components/SpinnerContainer/SpinnerContainerComponent';
import { SpinnerShapeComponent } from './components/SpinnerShape/SpinnerShapeComponent';
import { SpinContainerDirective } from './directives/SpinContainerDirective';
import { Overlay, OverlayModule } from '@angular/cdk/overlay';
import { SpinnerOverlayService } from './services/SpinnerOverlayService';
import { spinnerOverlayFactory } from './factories/spinnerOverlayFactory';
import { GLOBAL_SPINNER_PROVIDER } from './providers/GlobalSpinnerProvider';
import { SpinnerLayoutService } from './services/SpinnerLayoutService';

@NgModule({
    imports: [
        CommonModule,
        PortalModule,
        OverlayModule
    ],
    declarations: [
        IconSpinnerComponent,
        SpinnerDirective,
        SpinnerLayoutComponent,
        SpinnerContainerComponent,
        SpinnerShapeComponent,
        SpinContainerDirective
    ],
    exports: [
        SpinnerDirective,
        SpinnerContainerComponent
    ],
    providers: [
        SpinnerLayoutService,
        SpinnerService,
        { provide: SPINNER_COMPONENT_PROVIDER, useValue: defaultSpinner, multi: true },
        {
            provide: SpinnerOverlayService,
            useFactory: spinnerOverlayFactory,
            deps: [SpinnerService, Overlay, [new Optional(), new Inject(GLOBAL_SPINNER_PROVIDER)]]
        }
    ]
})
export class SpinnerModule { }
