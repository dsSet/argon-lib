import { SpinnerService } from '../services/SpinnerService';
import { Overlay } from '@angular/cdk/overlay';
import { SpinnerOverlayService } from '../services/SpinnerOverlayService';

let spinnerOverlayService: SpinnerOverlayService;

export function spinnerOverlayFactory(spinnerService: SpinnerService, overlay: Overlay, spinnerType: string) {
  if (!spinnerOverlayService || spinnerOverlayService.isDisposed) {
    spinnerOverlayService = new SpinnerOverlayService(spinnerService, overlay, spinnerType);
  }

  return spinnerOverlayService;
}
