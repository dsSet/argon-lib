import last from 'lodash/last';
import find from 'lodash/find';
import { Inject, Injectable, Type } from '@angular/core';
import { ComponentPortal, Portal } from '@angular/cdk/portal';
import { SPINNER_COMPONENT_PROVIDER } from '../providers/SpinnerProvider';
import { SpinnerProviderInterface } from '../interfaces/SpinnerProviderInterface';
import { SpinableStrategyInterface } from '../interfaces/SpinableStrategyInterface';
import { NoopSpinableStrategy } from '../models/NoopSpinableStrategy';

@Injectable()
export class SpinnerService {

  constructor(
    @Inject(SPINNER_COMPONENT_PROVIDER) private spinnerProvider: Array<SpinnerProviderInterface>
  ) { }

  public createSpinnerPortal(key?: string): Portal<any> {
    const spinner = this.getSpinner(key);

    if (spinner instanceof Portal) {
      return spinner;
    }

    return new ComponentPortal(spinner);
  }

  public getSpinableStrategy(key?: string): SpinableStrategyInterface {
    const provider = this.getSpinnerProvider(key);
    if (provider.getSpinableStrategy) {
      return provider.getSpinableStrategy();
    }

    return new NoopSpinableStrategy();
  }

  private getSpinner(key?: string): Portal<any> | Type<any> {
    const provider = this.getSpinnerProvider(key);
    return provider.component;
  }

  private getSpinnerProvider(key?: string): SpinnerProviderInterface {
    const provider: SpinnerProviderInterface = key
      ? find(this.spinnerProvider, { key })
      : last(this.spinnerProvider);

    if (!provider) {
      throw new Error(`SpinnerService: no provider for spinner type ${key}`);
    }

    return provider;
  }
}
