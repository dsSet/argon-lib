import { Injectable } from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';
import { SpinnerLayoutComponent } from '../components/SpinnerLayout/SpinnerLayoutComponent';

@Injectable()
export class SpinnerLayoutService {

  public createSpinnerLayoutPortal(): ComponentPortal<SpinnerLayoutComponent> {
    return new ComponentPortal<SpinnerLayoutComponent>(SpinnerLayoutComponent);
  }

}
