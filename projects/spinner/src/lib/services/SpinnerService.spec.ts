import { TestBed, waitForAsync } from '@angular/core/testing';
import { MockSpinnerComponent } from '../../test/MockSpinnerComponent';
import { SpinnerService } from './SpinnerService';
import { SpinnerProviderInterface } from '../interfaces/SpinnerProviderInterface';
import { SpinableStrategyInterface } from '../interfaces/SpinableStrategyInterface';
import { DebounceSpinableStrategy } from '../models/DebounceSpinableStrategy';
import { defaultSpinner, SPINNER_COMPONENT_PROVIDER } from '../providers/SpinnerProvider';
import { ComponentPortal, Portal } from '@angular/cdk/portal';
import { IconSpinnerComponent } from '../components/IconSpinner/IconSpinnerComponent';
import { Component } from '@angular/core';
import { NoopSpinableStrategy } from '../models/NoopSpinableStrategy';

describe('SpinnerService', () => {

  @Component({ selector: 'spec-spinner-2-component', template: '' })
  class MockSpinner2Component { }

  const spinner1: SpinnerProviderInterface = {
    key: 'mockSpinner1',
    component: new ComponentPortal(MockSpinner2Component),
    getSpinableStrategy(): SpinableStrategyInterface {
      return new DebounceSpinableStrategy();
    }
  };

  const spinner2: SpinnerProviderInterface = {
    key: 'mockSpinner2',
    component: MockSpinnerComponent
  };

  let service: SpinnerService;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      providers: [
        SpinnerService,
        { provide: SPINNER_COMPONENT_PROVIDER, useValue: defaultSpinner, multi: true },
        { provide: SPINNER_COMPONENT_PROVIDER, useValue: spinner1, multi: true },
        { provide: SPINNER_COMPONENT_PROVIDER, useValue: spinner2, multi: true },
      ]
    }).compileComponents()
  ));

  beforeEach(() => {
    service = TestBed.inject(SpinnerService) as any;
  });

  it('createSpinnerPortal should return spinner portal from last provided spinner if has no key', () => {
    const portal = service.createSpinnerPortal() as ComponentPortal<any>;
    expect(portal instanceof ComponentPortal).toBeTruthy();
    expect(portal.component).toEqual(MockSpinnerComponent);
  });

  it('createSpinnerPortal should return spinner portal for key', () => {
    const portal = service.createSpinnerPortal('default') as ComponentPortal<any>;
    expect(portal instanceof ComponentPortal).toBeTruthy();
    expect(portal.component).toEqual(IconSpinnerComponent);
  });

  it('createSpinnerPortal should return spinner portal from last provided spinner if has no key', () => {
    const portal = service.createSpinnerPortal('mockSpinner1') as ComponentPortal<any>;
    expect(portal instanceof ComponentPortal).toBeTruthy();
    expect(portal.component).toEqual(MockSpinner2Component);
  });

  it('createSpinnerPortal should throw Error for unknown spinner type', () => {
    const fakeSpinner = 'fakeSpinner';
    expect(() => { service.createSpinnerPortal(fakeSpinner); }).toThrow(
      new Error(`SpinnerService: no provider for spinner type ${fakeSpinner}`)
    );
  });

  it('getSpinableStrategy should return provided strategy', () => {
    const strategy = service.getSpinableStrategy(spinner1.key);
    expect(strategy instanceof DebounceSpinableStrategy).toBeTruthy();
  });

  it('getSpinableStrategy should return NoopSpinable strategy by default', () => {
    const strategy = service.getSpinableStrategy(spinner2.key);
    expect(strategy instanceof NoopSpinableStrategy).toBeTruthy();
  });

  it('getSpinableStrategy should throw Error for unknown spinner type', () => {
    const fakeSpinner = 'fakeSpinner';
    expect(() => { service.getSpinableStrategy(fakeSpinner); }).toThrow(
      new Error(`SpinnerService: no provider for spinner type ${fakeSpinner}`)
    );
  });
});
