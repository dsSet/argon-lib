import { TestBed, waitForAsync } from '@angular/core/testing';
import { ComponentPortal, Portal } from '@angular/cdk/portal';
import { SpinnerLayoutComponent } from '../components/SpinnerLayout/SpinnerLayoutComponent';
import { SpinnerLayoutService } from './SpinnerLayoutService';

describe('SpinnerLayoutService', () => {

  let service: SpinnerLayoutService;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      providers: [
        SpinnerLayoutService
      ]
    }).compileComponents()
  ));

  beforeEach(() => {
    service = TestBed.inject(SpinnerLayoutService) as any;
  });

  it('createSpinnerLayoutPortal should create portal for SpinnerLayoutComponent', () => {
    const portal = service.createSpinnerLayoutPortal();
    expect(portal instanceof ComponentPortal).toBeTruthy();
    expect(portal.component).toEqual(SpinnerLayoutComponent);
  });
});
