import { Inject, Injectable, OnDestroy, Optional } from '@angular/core';
import { SpinnerService } from './SpinnerService';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { SpinableSubjectInterface } from '../interfaces/SpinableSubjectInterface';
import { GLOBAL_SPINNER_PROVIDER } from '../providers/GlobalSpinnerProvider';
import { Portal } from '@angular/cdk/portal';
import { SpinableStrategyInterface } from '../interfaces/SpinableStrategyInterface';

@Injectable()
export class SpinnerOverlayService implements OnDestroy, SpinableSubjectInterface {

  public set isSpinnerActive(state: boolean) {
    if (state) {
      this.overlayRef.attach(this.spinner);
    } else {
      this.overlayRef.detach();
    }
  }

  public isDisposed: boolean;

  public readonly overlayRef: OverlayRef;

  public readonly spinner: Portal<any>;

  public readonly spinableStrategy: SpinableStrategyInterface;

  constructor(
    private spinnerService: SpinnerService,
    private overlay: Overlay,
    @Optional() @Inject(GLOBAL_SPINNER_PROVIDER) spinnerType: string
  ) {
    this.overlayRef = this.createOverlay();
    this.spinableStrategy = this.spinnerService.getSpinableStrategy(spinnerType);
    this.spinner = this.spinnerService.createSpinnerPortal(spinnerType);
    this.spinableStrategy.connect().subscribe((state: boolean) => { this.isSpinnerActive = state; });
  }

  public setSpinnerState(state: boolean) {
    this.spinableStrategy.setSpinnerState(state);
  }

  ngOnDestroy(): void {
    this.spinableStrategy.dispose();
    this.overlayRef.dispose();
    this.isDisposed = true;
  }

  private createOverlay(): OverlayRef {
    const position = this.overlay.position().global();
    position.centerVertically();
    position.centerHorizontally();

    const scroll = this.overlay.scrollStrategies.block();

    return this.overlay.create({
      positionStrategy: position,
      scrollStrategy: scroll,
      hasBackdrop: true,
      backdropClass: 'ar-spinner-backdrop'
    });
  }
}
