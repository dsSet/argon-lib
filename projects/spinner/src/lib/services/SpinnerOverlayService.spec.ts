import { TestBed, waitForAsync } from '@angular/core/testing';
import { OverlayModule, OverlayRef } from '@angular/cdk/overlay';
import { SpinnerOverlayService } from './SpinnerOverlayService';
import { GLOBAL_SPINNER_PROVIDER } from '../providers/GlobalSpinnerProvider';
import { SpinnerService } from './SpinnerService';
import { SpinnerServiceMock } from '../../test/SpinnerServiceMock';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { MockSpinnerComponent } from '../../test/MockSpinnerComponent';
import { MockSpinnerLayoutComponent } from '../../test/MockSpinnerLayoutComponent';

describe('SpinnerOverlayService', () => {
  const globalSpinnerType = 'globalSpinnerType';
  let service: SpinnerOverlayService;
  let spinnerService: SpinnerServiceMock;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      imports: [
        OverlayModule
      ],
      declarations: [
        MockSpinnerComponent
      ],
      providers: [
        SpinnerOverlayService,
        { provide: GLOBAL_SPINNER_PROVIDER, useValue: globalSpinnerType },
        { provide: SpinnerService, useClass: SpinnerServiceMock }
      ]
    })
      .overrideModule(BrowserDynamicTestingModule, {
        set: {
          entryComponents: [MockSpinnerComponent]
        }
      })
      .compileComponents()
  ));

  beforeEach(() => {
    service = TestBed.inject(SpinnerOverlayService) as any;
    spinnerService = TestBed.inject(SpinnerService) as any;
  });

  it('ctor should create overlayRef, spinner, spinableStrategy', () => {
    expect(service.overlayRef instanceof OverlayRef).toBeTruthy();
    expect(service.spinner).toEqual(spinnerService.spinnerPortal);
    expect(service.spinableStrategy).toEqual(spinnerService.spinableStrategy);
    expect(spinnerService.getSpinableStrategy).toHaveBeenCalledWith(globalSpinnerType);
    expect(spinnerService.spinableStrategy.connect).toHaveBeenCalled();
  });

  it('spinableStrategy should change isSpinnerActive property', () => {
    const spy = spyOnProperty(service, 'isSpinnerActive', 'set');
    spinnerService.spinableStrategy.stateSubject.next(true);
    expect(spy).toHaveBeenCalledWith(true);
  });

  it('setSpinnerState should call spinableStrategy.setSpinnerState', () => {
    service.setSpinnerState(true);
    expect(spinnerService.spinableStrategy.setSpinnerState).toHaveBeenCalledWith(true);
  });

  it('isSpinnerActive should attach spinner for true value', () => {
    spyOn(service.overlayRef, 'attach');
    service.isSpinnerActive = true;
    expect(service.overlayRef.attach).toHaveBeenCalledWith(service.spinner);
  });

  it('isSpinnerActive should detach spinner for false value', () => {
    spyOn(service.overlayRef, 'detach');
    service.isSpinnerActive = false;
    expect(service.overlayRef.detach).toHaveBeenCalled();
  });

  it('overlayRef should have backdrop class .ar-spinner-backdrop', () => {
    service.isSpinnerActive = true;
    expect(service.overlayRef.backdropElement.classList.contains('ar-spinner-backdrop')).toBeTruthy();
  });

  it('ngOnDestroy should dispose spinableStrategy and overlayRef', () => {
    spyOn(service.overlayRef, 'dispose');
    expect(service.isDisposed).toBeFalsy();
    service.ngOnDestroy();
    expect(service.isDisposed).toBeTruthy();
    expect(service.overlayRef.dispose).toHaveBeenCalled();
    expect(service.spinableStrategy.dispose).toHaveBeenCalled();
  });
});
