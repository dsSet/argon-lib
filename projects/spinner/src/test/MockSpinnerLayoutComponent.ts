import { Component, TemplateRef } from '@angular/core';

@Component({
  selector: 'spec-spinner-layout',
  template: ''
})
export class MockSpinnerLayoutComponent {

  public spinnerType: string;

  public template: TemplateRef<any>;

  public isSpinnerActive: boolean;

}
