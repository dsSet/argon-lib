import createSpy = jasmine.createSpy;
import { ComponentPortal } from '@angular/cdk/portal';
import { SpinableStrategyMock } from './SpinableStrategyMock';
import { MockSpinnerComponent } from './MockSpinnerComponent';

export class SpinnerServiceMock {

  public spinableStrategy = new SpinableStrategyMock();

  public spinnerPortal = new ComponentPortal(MockSpinnerComponent);

  createSpinnerPortal = createSpy('createSpinnerPortal').and.returnValue(this.spinnerPortal);

  getSpinableStrategy = createSpy('getSpinableStrategy').and.returnValue(this.spinableStrategy);

}
