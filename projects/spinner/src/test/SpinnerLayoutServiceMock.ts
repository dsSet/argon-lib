import { ComponentPortal } from '@angular/cdk/portal';
import { MockSpinnerLayoutComponent } from './MockSpinnerLayoutComponent';
import createSpy = jasmine.createSpy;

export class SpinnerLayoutServiceMock {

  public layoutPortal = new ComponentPortal(MockSpinnerLayoutComponent);

  createSpinnerLayoutPortal = createSpy('createSpinnerLayoutPortal').and.returnValue(this.layoutPortal);

}
