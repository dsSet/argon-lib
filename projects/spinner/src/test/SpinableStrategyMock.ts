import { SpinableStrategyInterface } from '../lib/interfaces/SpinableStrategyInterface';
import { Subject } from 'rxjs';
import createSpy = jasmine.createSpy;

export class SpinableStrategyMock implements SpinableStrategyInterface {

  public stateSubject = new Subject<boolean>();

  connect = createSpy('connect').and.returnValue(this.stateSubject.asObservable());

  dispose = createSpy('dispose');

  setSpinnerState = createSpy('setSpinnerState');

}
