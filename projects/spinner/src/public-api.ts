/*
 * Public API Surface of spinner
 */

export { SpinnerModule } from './lib/SpinnerModule';
export { SPINNER_COMPONENT_PROVIDER } from './lib/providers/SpinnerProvider';
export { GLOBAL_SPINNER_PROVIDER } from './lib/providers/GlobalSpinnerProvider';

export { SpinnerProviderInterface } from './lib/interfaces/SpinnerProviderInterface';
export { SpinableStrategyInterface } from './lib/interfaces/SpinableStrategyInterface';
export { SpinableSubjectInterface } from './lib/interfaces/SpinableSubjectInterface';

export { DebounceSpinableStrategy } from './lib/models/DebounceSpinableStrategy';
export { NoopSpinableStrategy } from './lib/models/NoopSpinableStrategy';

export { IconSpinnerComponent } from './lib/components/IconSpinner/IconSpinnerComponent';
export { SpinnerContainerComponent } from './lib/components/SpinnerContainer/SpinnerContainerComponent';

export { SpinnerDirective } from './lib/directives/SpinnerDirective';

export { SpinnerService } from './lib/services/SpinnerService';
export { SpinnerOverlayService } from './lib/services/SpinnerOverlayService';
