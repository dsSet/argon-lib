import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ButtonsModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

storiesOf('Argon|buttons/Button component', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        ButtonsModule
      ]
    })
  ).add('render custom button', () => ({
      props: {
        handleClick: () => { alert('Click handled!'); }
      },
      template: `
        <ar-button arBsContext="success" arBsSize="sm" (click)="handleClick()">Button Text</ar-button>
        <ar-button arBsContext="success" arBsSize="sm" (click)="handleClick()" [disabled]="true">Button Text</ar-button>
      `
    })
  ).add('access key test', () => ({
    props: {
      handleClick: () => { alert('Click handled!'); }
    },
    template: `
        <ar-button accesskey="Q" arBsContext="success" arBsSize="sm" (click)="handleClick()">Button Text (Alt + Q)</ar-button>
        <ar-button
          accesskey="W"
          arBsContext="success"
          arBsSize="sm"
          (click)="handleClick()"
          [disabled]="true"
        >
          Button Text (Alt + W)
        </ar-button>
      `
  })
);
