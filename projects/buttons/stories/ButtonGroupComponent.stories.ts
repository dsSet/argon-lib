import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ButtonsModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

storiesOf('Argon|buttons/Button group', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        ButtonsModule
      ]
    })
  ).add('Button group', () => ({
      template: `
        <ar-button-group>
          <button arButton arBsContext="info">Button text</button>
          <button arButton arBsContext="primary">Button text</button>
          <button arButton arBsContext="secondary">Button text</button>
        </ar-button-group>
      `
    })
  ).add('Button group vertical', () => ({
    template: `
        <ar-button-group [vertical]="true">
          <button arButton arBsContext="info">Button text</button>
          <button arButton arBsContext="primary">Button text</button>
          <button arButton arBsContext="secondary">Button text</button>
        </ar-button-group>
      `
  })
);
