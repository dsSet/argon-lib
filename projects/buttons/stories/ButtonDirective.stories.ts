import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ButtonsModule } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

storiesOf('Argon|buttons/Button directive', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        ButtonsModule
      ]
    })
  ).add('decorate <button/> with directive', () => ({
      template: `
        <div>
          <button arButton arBsContext="info">Button text (info)</button>
          <button arButton arBsContext="primary">Button text (primary)</button>
          <button arButton arBsContext="secondary">Button text (secondary)</button>
          <button arButton arBsContext="success">Button text (success)</button>
          <button arButton arBsContext="warning">Button text (warning)</button>
          <button arButton arBsContext="danger">Button text (danger)</button>
          <button arButton arBsContext="default">Button text (default)</button>
          <button arButton arBsContext="light">Button text (light)</button>
          <button arButton arBsContext="dark">Button text (dark)</button>
          <button arButton arBsContext="link">Button text (link)</button>
          <button arButton arBsContext="muted">Button text (muted)</button>
          <button arButton arBsContext="white">Button text (white)</button>
        </div>
      `
    })
  ).add('disabled state, click action', () => ({
      props: {
        handleClick: () => { alert('CLICKED!'); }
      },
      template: `
        <button arButton arBsContext="info" (click)="handleClick()">Button Text</button>
        <button arButton arBsContext="info" disabled (click)="handleClick()">Button Text</button>
      `
})).add('button link', () => ({
  template: `
        <a arButton arBsContext="info" href="http://angular.io" target="_blank">Angular Link</a>
        <a arButton arBsContext="link" href="http://angular.io" target="_blank">Angular Link</a>
      `
})).add('button styles', () => ({
  template: `
    <div style="width: 300px">
       <button arButton="block" arBsContext="secondary">Button text (block)</button>
    </div>
  `
})).add('button sizes', () => ({
  template: `
    <div>
      <button arButton arBsContext="secondary" arBsSize="xss">Button Text (xss)</button>
      <button arButton arBsContext="secondary" arBsSize="xs">Button Text (xs)</button>
      <button arButton arBsContext="secondary" arBsSize="sm">Button Text (sm)</button>
      <button arButton arBsContext="secondary" arBsSize="md">Button Text (md)</button>
      <button arButton arBsContext="secondary" arBsSize="lg">Button Text (lg)</button>
      <button arButton arBsContext="secondary" arBsSize="xl">Button Text (xl)</button>
    </div>
  `
}));
