# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@2.0.1...@argon/buttons@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/buttons





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@1.0.4...@argon/buttons@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/buttons






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@1.0.4...@argon/buttons@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/buttons






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@1.0.3...@argon/buttons@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/buttons





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@1.0.2...@argon/buttons@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/buttons





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@1.0.1...@argon/buttons@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/buttons






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.21...@argon/buttons@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/buttons





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.21...@argon/buttons@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/buttons






## [0.2.21](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.20...@argon/buttons@0.2.21) (2020-08-04)

**Note:** Version bump only for package @argon/buttons






## [0.2.20](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.19...@argon/buttons@0.2.20) (2020-05-21)

**Note:** Version bump only for package @argon/buttons






## [0.2.19](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.18...@argon/buttons@0.2.19) (2020-04-18)


### Bug Fixes

* **buttons:** add missd export ([01a6ad7](https://bitbucket.org/dsSet/argon-lib/commits/01a6ad7))





## [0.2.18](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.17...@argon/buttons@0.2.18) (2020-04-18)

**Note:** Version bump only for package @argon/buttons





## [0.2.17](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.16...@argon/buttons@0.2.17) (2020-04-18)


### Bug Fixes

* **buttons:** fix buttons module for aot build ([a04477a](https://bitbucket.org/dsSet/argon-lib/commits/a04477a))
* **buttons:** fix host binding props ([3c9524d](https://bitbucket.org/dsSet/argon-lib/commits/3c9524d))
* **buttons:** remove incorrect export ([328f2c7](https://bitbucket.org/dsSet/argon-lib/commits/328f2c7))






## [0.2.16](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.15...@argon/buttons@0.2.16) (2020-03-29)

**Note:** Version bump only for package @argon/buttons





## [0.2.15](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.14...@argon/buttons@0.2.15) (2020-03-24)

**Note:** Version bump only for package @argon/buttons





## [0.2.14](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.13...@argon/buttons@0.2.14) (2020-03-24)

**Note:** Version bump only for package @argon/buttons






## [0.2.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.12...@argon/buttons@0.2.13) (2020-03-18)

**Note:** Version bump only for package @argon/buttons





## [0.2.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.11...@argon/buttons@0.2.12) (2020-02-14)

**Note:** Version bump only for package @argon/buttons






## [0.2.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.10...@argon/buttons@0.2.11) (2019-11-29)


### Bug Fixes

* **button:** fix lifecircle hooks calls ([2462b4d](https://bitbucket.org/dsSet/argon-lib/commits/2462b4d))





## [0.2.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.9...@argon/buttons@0.2.10) (2019-11-22)

**Note:** Version bump only for package @argon/buttons





## [0.2.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.8...@argon/buttons@0.2.9) (2019-11-22)


### Bug Fixes

* **buttons:** temp disable unit test ([c4fab7e](https://bitbucket.org/dsSet/argon-lib/commits/c4fab7e))





## [0.2.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.7...@argon/buttons@0.2.8) (2019-10-28)

**Note:** Version bump only for package @argon/buttons





## [0.2.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.6...@argon/buttons@0.2.7) (2019-10-11)

**Note:** Version bump only for package @argon/buttons





## [0.2.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.5...@argon/buttons@0.2.6) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





## [0.2.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.4...@argon/buttons@0.2.5) (2019-10-08)

**Note:** Version bump only for package @argon/buttons





## [0.2.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.3...@argon/buttons@0.2.4) (2019-09-21)

**Note:** Version bump only for package @argon/buttons





## [0.2.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.2...@argon/buttons@0.2.3) (2019-09-17)

**Note:** Version bump only for package @argon/buttons





## [0.2.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.1...@argon/buttons@0.2.2) (2019-09-13)

**Note:** Version bump only for package @argon/buttons






## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.2.0...@argon/buttons@0.2.1) (2019-09-07)

**Note:** Version bump only for package @argon/buttons





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.1.2...@argon/buttons@0.2.0) (2019-09-06)


### Bug Fixes

* **buttons:** fix dependencies ([eb0bf8b](https://bitbucket.org/dsSet/argon-lib/commits/eb0bf8b))


### Features

* **buttons:** update button implementation with common mixins ([279ab07](https://bitbucket.org/dsSet/argon-lib/commits/279ab07))





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.1.1...@argon/buttons@0.1.2) (2019-08-10)

**Note:** Version bump only for package @argon/buttons





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/buttons@0.1.0...@argon/buttons@0.1.1) (2019-08-10)

**Note:** Version bump only for package @argon/buttons





# 0.1.0 (2019-07-28)


### Features

* **buttons:** add button component ([f825b4b](https://bitbucket.org/dsSet/argon-lib/commits/f825b4b))
* **buttons:** add buttons module ([16924d2](https://bitbucket.org/dsSet/argon-lib/commits/16924d2))
* **buttons:** implement button directive and button group component ([0a645d0](https://bitbucket.org/dsSet/argon-lib/commits/0a645d0))
