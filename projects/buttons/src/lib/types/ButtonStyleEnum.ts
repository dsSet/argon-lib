
export enum ButtonStyleEnum {

  inline = 'inline',

  block = 'block'

}

export type ButtonStyleType = 'inline' | 'block';
