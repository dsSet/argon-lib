import { NgModule } from '@angular/core';
import { ButtonDirective } from './directives/ButtonDirective';
import { ButtonGroupComponent } from './components/ButtonGroup/ButtonGroupComponent';
import { ButtonComponent } from './components/Button/ButtonComponent';
import { ToolsModule } from '@argon/tools';

@NgModule({
  imports: [
    ToolsModule
  ],
  declarations: [
    ButtonDirective,
    ButtonComponent,
    ButtonGroupComponent
  ],
  exports: [
    ButtonDirective,
    ButtonComponent,
    ButtonGroupComponent
  ]
})
export class ButtonsModule { }
