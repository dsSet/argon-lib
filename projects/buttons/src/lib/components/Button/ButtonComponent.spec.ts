import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ButtonComponent } from './ButtonComponent';
import { ButtonDirective } from '../../directives/ButtonDirective';
import { ToolsModule } from '@argon/tools';


describe('ButtonComponent', () => {
  let component: ButtonComponent;
  let fixture: ComponentFixture<ButtonComponent>;
  let element: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      /*imports: [
        ToolsModule,
      ],*/
      declarations: [
        ButtonComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
  });

  it('should extend button directive', () => {
    expect(component instanceof ButtonDirective).toBeTruthy();
  });

  // it('disabled should toggle .disabled class', () => {
  //   component.disabled = false;
  //   fixture.detectChanges();
  //   expect(fixture.nativeElement.classList.contains('disabled')).toBeFalsy();
  //
  //   component.disabled = true;
  //   fixture.detectChanges();
  //   expect(fixture.nativeElement.classList.contains('disabled')).toBeTruthy();
  // });
});
