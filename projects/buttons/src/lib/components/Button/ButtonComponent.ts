import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { ButtonDirective } from '../../directives/ButtonDirective';

@Component({
  selector: 'ar-button',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    '[attr.tabindex]': 'attrTabIndex',
    '[attr.accesskey]': 'attrAccessKey',
    '[attr.disabled]': 'attrDisabled',
    '[class.disabled]': 'disabled',
    role: 'button'
  }
})
export class ButtonComponent extends ButtonDirective implements OnInit, OnDestroy, OnChanges {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }
}
