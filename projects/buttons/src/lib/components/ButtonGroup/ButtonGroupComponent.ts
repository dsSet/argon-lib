import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'ar-button-group',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonGroupComponent {

  @Input() vertical: boolean;

  @HostBinding('class.btn-group') get bsClass(): boolean {
    return !this.vertical;
  }

  @HostBinding('class.btn-group-vertical') get bsClassVertical(): boolean {
    return this.vertical;
  }

  @HostBinding('class.ar-button-group') className = true;

  @HostBinding('attr.role') role = 'group';

}
