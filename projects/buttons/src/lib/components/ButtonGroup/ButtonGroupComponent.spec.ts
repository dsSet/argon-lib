import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ButtonGroupComponent } from './ButtonGroupComponent';

describe('ButtonGroupComponent', () => {
  let component: ButtonGroupComponent;
  let fixture: ComponentFixture<ButtonGroupComponent>;
  let element: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ButtonGroupComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonGroupComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
  });

  it('should have className === true by default', () => {
    expect(component.className).toEqual(true);
  });

  it('className should toggle .ar-button-group class', () => {
    component.className = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-button-group')).toBeTruthy();

    component.className = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-button-group')).toBeFalsy();
  });

  it('vertical input should toggle .btn-group and .btn-group-vertical classes', () => {
    component.vertical = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('btn-group')).toBeTruthy();
    expect(fixture.nativeElement.classList.contains('btn-group-vertical')).toBeFalsy();

    component.vertical = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('btn-group')).toBeFalsy();
    expect(fixture.nativeElement.classList.contains('btn-group-vertical')).toBeTruthy();
  });

  it('should append role attribute', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('role')).toEqual('group');
  });
});
