import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ButtonDirective } from './ButtonDirective';
import { ButtonStyleEnum } from '../types/ButtonStyleEnum';
import { KeyboardService } from '@argon/keyboard';

describe('ButtonDirective', () => {
  @Component({
    selector: 'spec-component',
    template: '<div [arButton]="style"></div>'
  })
  class FakeContainerComponent {
    @Input() style: ButtonStyleEnum;
  }

  let component: FakeContainerComponent;
  let fixture: ComponentFixture<FakeContainerComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ FakeContainerComponent, ButtonDirective ],
      providers: [
        KeyboardService
      ]
    });
    fixture = TestBed.createComponent(FakeContainerComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement.querySelector('div');
  });

  it('should bind .ar-button class', () => {
    fixture.detectChanges();
    expect(element.classList.contains('ar-button')).toBeTruthy();
  });

  it('should bind .btn class', () => {
    fixture.detectChanges();
    expect(element.classList.contains('btn')).toBeTruthy();
  });

  it('should bind role attribute', () => {
    fixture.detectChanges();
    expect(element.getAttribute('role')).toEqual('button');
  });

  it('should bind .btn-block class for arButton style block', () => {
    component.style = ButtonStyleEnum.block;
    fixture.detectChanges();
    expect(element.classList.contains('btn-block')).toBeTruthy();
  });

  it('should not bind .btn-block class for arButton style inline', () => {
    component.style = ButtonStyleEnum.inline;
    fixture.detectChanges();
    expect(element.classList.contains('btn-block')).toBeFalsy();
  });
});
