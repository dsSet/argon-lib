import {
  Directive,
  EventEmitter,
  HostBinding,
  Injector,
  Input,
  OnChanges, OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { ButtonStyleEnum, ButtonStyleType } from '../types/ButtonStyleEnum';
import { ContextMixin, BsContextType, BsSizeType } from '@argon/bs-context';
import { UIShape, UITabindexMixin, UIAccessKeyMixin } from '@argon/tools';

@Directive({
  selector: '[arButton]',
  host: {
    '[attr.tabindex]': 'attrTabIndex',
    '[attr.accesskey]': 'attrAccessKey',
    '[attr.disabled]': 'attrDisabled',
    '[class.disabled]': 'disabled',
    role: 'button'
  }
})
export class ButtonDirective extends UIAccessKeyMixin(UITabindexMixin(ContextMixin(UIShape))) implements OnInit, OnChanges, OnDestroy {

  @Input() arBsSize: BsSizeType;

  @Input() arBsContext: BsContextType;

  @Input() arButton: ButtonStyleType;

  @Input() tabIndex = 0;

  @Input() accesskey: string;

  @Input() disabled: boolean;

  @Output() click = new EventEmitter<void>();

  @HostBinding('class.ar-button') className = true;

  @HostBinding('class.btn-block') get styleBlock(): boolean {
    return this.arButton === ButtonStyleEnum.block;
  }

  bsClassName = 'btn';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

}
