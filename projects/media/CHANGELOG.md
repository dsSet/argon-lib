# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@2.0.1...@argon/media@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/media





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@1.0.4...@argon/media@2.0.1) (2023-10-12)


### Bug Fixes

* tsfix ([6b405d8](https://bitbucket.org/dsSet/argon-lib/commits/6b405d8621988dd8b6fa34479bdb573e8bac2cc5))






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@1.0.4...@argon/media@2.0.0) (2023-10-12)


### Bug Fixes

* tsfix ([6b405d8](https://bitbucket.org/dsSet/argon-lib/commits/6b405d8621988dd8b6fa34479bdb573e8bac2cc5))






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@1.0.3...@argon/media@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/media





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@1.0.2...@argon/media@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/media





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@1.0.1...@argon/media@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/media






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.3.7...@argon/media@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/media





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.3.7...@argon/media@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/media






## [0.3.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.3.6...@argon/media@0.3.7) (2020-08-04)

**Note:** Version bump only for package @argon/media






## [0.3.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.3.5...@argon/media@0.3.6) (2020-05-21)

**Note:** Version bump only for package @argon/media






## [0.3.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.3.4...@argon/media@0.3.5) (2020-04-18)

**Note:** Version bump only for package @argon/media






## [0.3.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.3.3...@argon/media@0.3.4) (2020-03-24)

**Note:** Version bump only for package @argon/media





## [0.3.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.3.2...@argon/media@0.3.3) (2020-03-24)

**Note:** Version bump only for package @argon/media






## [0.3.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.3.1...@argon/media@0.3.2) (2020-03-18)

**Note:** Version bump only for package @argon/media





## [0.3.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.3.0...@argon/media@0.3.1) (2020-02-14)

**Note:** Version bump only for package @argon/media






# [0.3.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.15...@argon/media@0.3.0) (2019-11-29)


### Bug Fixes

* **media:** fix build ([2fcec64](https://bitbucket.org/dsSet/argon-lib/commits/2fcec64))


### Features

* **media:** add media directive ([3381dcb](https://bitbucket.org/dsSet/argon-lib/commits/3381dcb))
* **media:** add media toggle directive and global breakpoint service ([288b89d](https://bitbucket.org/dsSet/argon-lib/commits/288b89d))





## [0.2.15](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.14...@argon/media@0.2.15) (2019-11-22)

**Note:** Version bump only for package @argon/media





## [0.2.14](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.13...@argon/media@0.2.14) (2019-11-22)

**Note:** Version bump only for package @argon/media





## [0.2.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.12...@argon/media@0.2.13) (2019-10-28)

**Note:** Version bump only for package @argon/media





## [0.2.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.11...@argon/media@0.2.12) (2019-10-11)

**Note:** Version bump only for package @argon/media





## [0.2.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.10...@argon/media@0.2.11) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





## [0.2.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.9...@argon/media@0.2.10) (2019-10-08)

**Note:** Version bump only for package @argon/media





## [0.2.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.8...@argon/media@0.2.9) (2019-09-21)

**Note:** Version bump only for package @argon/media





## [0.2.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.7...@argon/media@0.2.8) (2019-09-17)

**Note:** Version bump only for package @argon/media





## [0.2.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.6...@argon/media@0.2.7) (2019-09-13)

**Note:** Version bump only for package @argon/media






## [0.2.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.5...@argon/media@0.2.6) (2019-09-07)

**Note:** Version bump only for package @argon/media





## [0.2.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.4...@argon/media@0.2.5) (2019-09-06)

**Note:** Version bump only for package @argon/media





## [0.2.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.3...@argon/media@0.2.4) (2019-08-10)

**Note:** Version bump only for package @argon/media





## [0.2.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.2...@argon/media@0.2.3) (2019-08-10)

**Note:** Version bump only for package @argon/media





## [0.2.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.1...@argon/media@0.2.2) (2019-07-28)

**Note:** Version bump only for package @argon/media





## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.2.0...@argon/media@0.2.1) (2019-07-24)

**Note:** Version bump only for package @argon/media





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.1.5...@argon/media@0.2.0) (2019-07-21)


### Features

* **media:** add @Unsubscribeable decorator ([8335b1a](https://bitbucket.org/dsSet/argon-lib/commits/8335b1a))





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.1.4...@argon/media@0.1.5) (2019-07-15)


### Bug Fixes

* **main:** revert incorrect imports ([d30bb89](https://bitbucket.org/dsSet/argon-lib/commits/d30bb89))





## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.1.3...@argon/media@0.1.4) (2019-07-14)

**Note:** Version bump only for package @argon/media





## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.1.2...@argon/media@0.1.3) (2019-07-14)


### Bug Fixes

* **dropdown:** mock import ([884e049](https://bitbucket.org/dsSet/argon-lib/commits/884e049))





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.1.1...@argon/media@0.1.2) (2019-07-14)

**Note:** Version bump only for package @argon/media





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/media@0.1.0...@argon/media@0.1.1) (2019-07-14)

**Note:** Version bump only for package @argon/media






# 0.1.0 (2019-06-30)


### Features

* **media:** add media size module ([9ed7d3b](https://bitbucket.org/dsSet/argon-lib/commits/9ed7d3b))
