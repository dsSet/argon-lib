import isArray from 'lodash/isArray';
import each from 'lodash/each';
import { Directive, HostBinding, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MediaType } from '../types/MediaType';
import { GlobalMediaService } from '../services/GlobalMediaService';

@Directive({
  selector: '[arMediaToggle]'
})
export class MediaToggleDirective implements OnChanges {

  @Input() arMediaToggle: MediaType | Array<MediaType>;

  @HostBinding('class.ar-media') className = true;

  @HostBinding('class.phone') phone: boolean;

  @HostBinding('class.tablet') tablet: boolean;

  @HostBinding('class.desktop') desktop: boolean;

  constructor(
    private service: GlobalMediaService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    const { arMediaToggle } = changes;
    if (!arMediaToggle) {
      return;
    }

    const currentValue = arMediaToggle.currentValue;

    if (isArray(currentValue)) {
      this.phone = false;
      this.desktop = false;
      this.tablet = false;
      each(currentValue, (point: MediaType) => {
        if (point === 'phone') {
          this.phone = true;
        } else if (point === 'tablet') {
          this.tablet = true;
        } else if (point === 'desktop') {
          this.desktop = true;
        }
      });
    } else {
      this.phone = currentValue === 'phone';
      this.desktop = currentValue === 'desktop';
      this.tablet = currentValue === 'tablet';
    }

  }

}
