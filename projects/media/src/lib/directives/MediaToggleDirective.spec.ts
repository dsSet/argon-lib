import { MediaToggleDirective } from './MediaToggleDirective';
import { GlobalMediaService } from '../services/GlobalMediaService';
import { Component, NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { BreakpointService } from '../services/BreakpointService';
import { MediaModule } from '../MediaModule';
import { HostBindingTestHelper } from '../../../../../test/helpers/HostBindingTestHelper';


describe('MediaToggleDirective', () => {

  @Component({ selector: 'spec-component', template: '' })
  class MockComponent extends MediaToggleDirective {
    constructor(service: GlobalMediaService) {
      super(service);
    }
  }

  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;
  let breakpointService: BreakpointService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ MediaModule ],
    declarations: [ MockComponent],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
    breakpointService = TestBed.inject(BreakpointService) as any;
  });

  it('should have default values', () => {
    fixture.detectChanges();
    expect(component.arMediaToggle).not.toBeDefined();
    expect(component.className).toBeTruthy();
    expect(component.phone).toBeFalsy();
    expect(component.desktop).toBeFalsy();
    expect(component.tablet).toBeFalsy();
  });

  it('className should toggle .ar-media class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-media');
  });

  it('phone should toggle .phone class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'phone', 'phone');
  });

  it('tablet should toggle .tablet class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'tablet', 'tablet');
  });

  it('desktop should toggle .desktop class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'desktop', 'desktop');
  });

  it('arMediaToggle should toggle phone, desktop and tablet properties', () => {
    let arMediaToggle = new SimpleChange(null, 'phone', false);
    component.ngOnChanges({ arMediaToggle });
    fixture.detectChanges();
    expect(component.phone).toBeTruthy();
    expect(component.tablet).toBeFalsy();
    expect(component.desktop).toBeFalsy();

    arMediaToggle = new SimpleChange(null, 'tablet', false);
    component.ngOnChanges({ arMediaToggle });
    fixture.detectChanges();
    expect(component.phone).toBeFalsy();
    expect(component.tablet).toBeTruthy();
    expect(component.desktop).toBeFalsy();

    arMediaToggle = new SimpleChange(null, 'desktop', false);
    component.ngOnChanges({ arMediaToggle });
    fixture.detectChanges();
    expect(component.phone).toBeFalsy();
    expect(component.tablet).toBeFalsy();
    expect(component.desktop).toBeTruthy();

    arMediaToggle = new SimpleChange(null, ['tablet', 'phone'], false);
    component.ngOnChanges({ arMediaToggle });
    fixture.detectChanges();
    expect(component.phone).toBeTruthy();
    expect(component.tablet).toBeTruthy();
    expect(component.desktop).toBeFalsy();

    component.ngOnChanges({ });
    fixture.detectChanges();
    expect(component.phone).toBeTruthy();
    expect(component.tablet).toBeTruthy();
    expect(component.desktop).toBeFalsy();

    arMediaToggle = new SimpleChange(null, [], false);
    component.ngOnChanges({ arMediaToggle });
    fixture.detectChanges();
    expect(component.phone).toBeFalsy();
    expect(component.tablet).toBeFalsy();
    expect(component.desktop).toBeFalsy();

    arMediaToggle = new SimpleChange(null, ['desktop'], false);
    component.ngOnChanges({ arMediaToggle });
    fixture.detectChanges();
    expect(component.phone).toBeFalsy();
    expect(component.tablet).toBeFalsy();
    expect(component.desktop).toBeTruthy();

    arMediaToggle = new SimpleChange(null, null, false);
    component.ngOnChanges({ arMediaToggle });
    fixture.detectChanges();
    expect(component.phone).toBeFalsy();
    expect(component.tablet).toBeFalsy();
    expect(component.desktop).toBeFalsy();
  });

});
