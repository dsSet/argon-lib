import { Component, ElementRef, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { BreakpointService } from '../services/BreakpointService';
import { MediaDirective } from './MediaDirective';
import { MediaModule } from '../MediaModule';

describe('MediaDirective', () => {

  @Component({
    selector: 'spec-component',
    template: `
      <div #phone *arMedia="'phone'">Phone</div>
      <div #tablet *arMedia="'tablet'">Tablet</div>
      <div #desktop *arMedia="'desktop'">Desktop</div>
      <div #phoneAndTablet *arMedia="['phone', 'tablet']">Phone and Tablet</div>
      <div #none *arMedia>None</div>
    `
  })
  class MockComponent {

    @ViewChild('phone', { read: ElementRef }) phone: ElementRef;
    @ViewChild('tablet', { read: ElementRef }) tablet: ElementRef;
    @ViewChild('desktop', { read: ElementRef }) desktop: ElementRef;
    @ViewChild('phoneAndTablet', { read: ElementRef }) phoneAndTablet: ElementRef;
    @ViewChild('none', { read: ElementRef }) none: ElementRef;

  }

  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;
  let breakpointService: BreakpointService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ MediaModule ],
    declarations: [ MockComponent],
    schemas: [ NO_ERRORS_SCHEMA ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
    breakpointService = TestBed.inject(BreakpointService) as any;
  });

  it('should correct render for desktop breakpoint', fakeAsync(() => {
    fixture.detectChanges();
    breakpointService.isDesktop.next(true);
    breakpointService.isTablet.next(false);
    breakpointService.isPhone.next(false);
    tick();
    fixture.detectChanges();
    expect(component.desktop).toBeTruthy();
    expect(component.tablet).toBeFalsy();
    expect(component.phone).toBeFalsy();
    expect(component.phoneAndTablet).toBeFalsy();
    expect(component.none).toBeFalsy();
  }));

  it('should correct render for tablet breakpoint', fakeAsync(() => {
    fixture.detectChanges();
    breakpointService.isDesktop.next(false);
    breakpointService.isTablet.next(true);
    breakpointService.isPhone.next(false);
    tick();
    fixture.detectChanges();
    expect(component.desktop).toBeFalsy();
    expect(component.tablet).toBeTruthy();
    expect(component.phone).toBeFalsy();
    expect(component.phoneAndTablet).toBeTruthy();
    expect(component.none).toBeFalsy();
  }));

  it('should correct render for phone breakpoint', fakeAsync(() => {
    fixture.detectChanges();
    breakpointService.isDesktop.next(false);
    breakpointService.isTablet.next(false);
    breakpointService.isPhone.next(true);
    tick();
    fixture.detectChanges();
    expect(component.desktop).toBeFalsy();
    expect(component.tablet).toBeFalsy();
    expect(component.phone).toBeTruthy();
    expect(component.phoneAndTablet).toBeTruthy();
    expect(component.none).toBeFalsy();
  }));

});
