import isArray from 'lodash/isArray';
import map from 'lodash/map';
import sum from 'lodash/sum';
import compact from 'lodash/compact';
import { Directive, EmbeddedViewRef, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { Unsubscribable } from '@argon/tools';
import { BreakpointService } from '../services/BreakpointService';
import { Observable, scheduled, Subscription, asyncScheduler } from 'rxjs';
import { combineAll } from 'rxjs/operators';
import { MediaType } from '../types/MediaType';


@Directive({
  selector: '[arMedia]'
})
export class MediaDirective implements OnInit, OnDestroy {

  @Input() arMedia: MediaType | Array<MediaType>;

  private breakpointSubscription: Subscription;

  private currentView: EmbeddedViewRef<any>;

  constructor(
    public breakpointService: BreakpointService,
    public templateRef: TemplateRef<any>,
    public viewContainer: ViewContainerRef
  ) { }

  ngOnInit(): void {
    let observer: Observable<boolean>;
    if (isArray(this.arMedia)) {
      const observers = compact(map(this.arMedia, this.getObserver));
      observer = scheduled(observers, asyncScheduler).pipe(
        combineAll((...values: Array<any>) => sum(values) > 0)
      );
    } else {
      observer = this.getObserver(this.arMedia as MediaType);
    }

    if (observer) {
      this.breakpointSubscription = observer.subscribe(this.handleBreakpointChange);
    }
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  private getObserver = (value: MediaType): Observable<boolean> => {
    switch (value) {
      case 'phone':
        return this.breakpointService.isPhone;
      case 'tablet':
        return this.breakpointService.isTablet;
      case 'desktop':
        return this.breakpointService.isDesktop;
      default:
        return null;
    }
  }

  private handleBreakpointChange = (isVisible: boolean) => {
    if (isVisible && !this.currentView) {
      this.currentView = this.viewContainer.createEmbeddedView(this.templateRef);
    } else if (!isVisible && this.currentView) {
      this.viewContainer.clear();
      this.currentView = null;
    }
  }

}
