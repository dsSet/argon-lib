import { NgModule } from '@angular/core';
import { ARGON_BREAKPOINTS, argonDefaultBreakpoints } from './providers/argonBreakpointsProvider';
import { LayoutModule } from '@angular/cdk/layout';
import { MediaDirective } from './directives/MediaDirective';
import { MediaToggleDirective } from './directives/MediaToggleDirective';

@NgModule({
  imports: [
    LayoutModule
  ],
  declarations: [
    MediaDirective,
    MediaToggleDirective
  ],
  exports: [
    MediaDirective,
    MediaToggleDirective
  ],
  providers: [
    { provide: ARGON_BREAKPOINTS, useValue: argonDefaultBreakpoints }
  ]
})
export class MediaModule { }
