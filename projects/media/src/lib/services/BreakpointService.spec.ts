import { BreakpointService } from './BreakpointService';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { argonDefaultBreakpoints } from '../providers/argonBreakpointsProvider';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

describe('BreakpointService', () => {
  let breakpointService: BreakpointService;
  let breakpointObserverSpy: jasmine.SpyObj<BreakpointObserver>;
  let observer: Subject<string>;

  beforeEach(() => {
    observer = new Subject();
    breakpointObserverSpy = jasmine.createSpyObj('BreakpointObserver', ['observe']);
    breakpointObserverSpy.observe.and.callFake((point: string) => {
      return observer.pipe(
        map((nextPoint: string) => ({
          matches: point === nextPoint
        } as BreakpointState))
      );
    });
    breakpointService = new BreakpointService(breakpointObserverSpy, argonDefaultBreakpoints);
  });

  afterEach(() => {
    observer.complete();
  });

  it('should observe breakpoints', () => {
    expect(breakpointObserverSpy.observe).toHaveBeenCalledWith(argonDefaultBreakpoints.phone);
    expect(breakpointObserverSpy.observe).toHaveBeenCalledWith(argonDefaultBreakpoints.tablet);
    expect(breakpointObserverSpy.observe).toHaveBeenCalledWith(argonDefaultBreakpoints.desktop);
  });

  it('should have default states for isPhone, isTablet, isDesktop values', () => {
    expect(breakpointService.isPhone.value).toEqual(false);
    expect(breakpointService.isTablet.value).toEqual(false);
    expect(breakpointService.isDesktop.value).toEqual(false);
  });

  it('should change state on resolution changes', () => {
    observer.next(argonDefaultBreakpoints.phone);
    expect(breakpointService.isPhone.value).toEqual(true);
    expect(breakpointService.isTablet.value).toEqual(false);
    expect(breakpointService.isDesktop.value).toEqual(false);
    observer.next(argonDefaultBreakpoints.tablet);
    expect(breakpointService.isPhone.value).toEqual(false);
    expect(breakpointService.isTablet.value).toEqual(true);
    expect(breakpointService.isDesktop.value).toEqual(false);
    observer.next(argonDefaultBreakpoints.desktop);
    expect(breakpointService.isPhone.value).toEqual(false);
    expect(breakpointService.isTablet.value).toEqual(false);
    expect(breakpointService.isDesktop.value).toEqual(true);
  });

  it('should complete all observers on destroy', () => {
    expect(observer.observers.length).toEqual(3);
    breakpointService.ngOnDestroy();
    expect(breakpointService.isPhone.isStopped).toEqual(true);
    expect(breakpointService.isTablet.isStopped).toEqual(true);
    expect(breakpointService.isDesktop.isStopped).toEqual(true);
    expect(observer.observers.length).toEqual(0);
  });
});
