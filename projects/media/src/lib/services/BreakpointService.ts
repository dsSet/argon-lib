import { Inject, Injectable, OnDestroy } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { ARGON_BREAKPOINTS } from '../providers/argonBreakpointsProvider';
import { BreakpointsInterface } from '../interfaces/BreakpointsInterface';
import { BehaviorSubject, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Unsubscribable } from '@argon/tools';

@Injectable({
  providedIn: 'root'
})
export class BreakpointService implements OnDestroy {

  public readonly isPhone = new BehaviorSubject<boolean>(false);

  public readonly isTablet = new BehaviorSubject<boolean>(false);

  public readonly isDesktop = new BehaviorSubject<boolean>(false);

  private phoneSubscription: Subscription;
  private tabletSubscription: Subscription;
  private desktopSubscription: Subscription;

  constructor(
    private breakpointObserver: BreakpointObserver,
    @Inject(ARGON_BREAKPOINTS) breakpoints: BreakpointsInterface
  ) {
    this.phoneSubscription = breakpointObserver.observe(breakpoints.phone).pipe(map(this.mapState)).subscribe(this.isPhone);
    this.tabletSubscription = breakpointObserver.observe(breakpoints.tablet).pipe(map(this.mapState)).subscribe(this.isTablet);
    this.desktopSubscription = breakpointObserver.observe(breakpoints.desktop).pipe(map(this.mapState)).subscribe(this.isDesktop);
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  private mapState = (state: BreakpointState): boolean => {
    return state.matches;
  }

}
