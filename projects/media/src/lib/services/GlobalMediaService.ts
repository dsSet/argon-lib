import { Inject, Injectable, OnDestroy, Renderer2, RendererFactory2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BreakpointService } from './BreakpointService';
import { asyncScheduler, scheduled, Subscription } from 'rxjs';
import { combineAll } from 'rxjs/operators';
import { Unsubscribable } from '@argon/tools';

@Injectable({ providedIn: 'root' })
export class GlobalMediaService implements OnDestroy {

  static GLOBAL_PHONE = 'ar-media-phone';
  static GLOBAL_TABLET = 'ar-media-tablet';
  static GLOBAL_DESKTOP = 'ar-media-desktop';

  private renderer: Renderer2;

  private documentSubscription: Subscription;

  constructor(
    @Inject(DOCUMENT) public document: any,
    rendererFactory: RendererFactory2,
    public breakpointService: BreakpointService
  ) {
    this.renderer = rendererFactory.createRenderer(null, null);

    this.documentSubscription = scheduled([
      this.breakpointService.isPhone,
      this.breakpointService.isTablet,
      this.breakpointService.isDesktop
    ], asyncScheduler).pipe(combineAll()).subscribe(this.setDocumentClassName);
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  private setDocumentClassName = ([isPhone, isTablet, isDesktop]: [boolean, boolean, boolean]) => {
    this.toggleClass(isPhone, GlobalMediaService.GLOBAL_PHONE);
    this.toggleClass(isTablet, GlobalMediaService.GLOBAL_TABLET);
    this.toggleClass(isDesktop, GlobalMediaService.GLOBAL_DESKTOP);
  }

  private toggleClass = (display: boolean, name: string) => {
    const body = this.document.body;
    const hasMounted = body.classList.contains(name);
    if (display && !hasMounted) {
      this.renderer.addClass(body, name);
    } else if (!display && hasMounted) {
      this.renderer.removeClass(body, name);
    }
  }

}
