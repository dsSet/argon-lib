
export type MediaType = 'desktop' | 'tablet' | 'phone';
