
export interface BreakpointsInterface {

  phone: string;

  tablet: string;

  desktop: string;

}
