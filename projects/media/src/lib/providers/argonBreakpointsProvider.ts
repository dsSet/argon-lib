import { InjectionToken } from '@angular/core';
import { BreakpointsInterface } from '../interfaces/BreakpointsInterface';

export const ARGON_BREAKPOINTS = new InjectionToken<BreakpointsInterface>('ARGON_BREAKPOINTS');

export const argonDefaultBreakpoints: BreakpointsInterface = {

  phone: '(max-width: 767px)',

  tablet: '(min-width: 768px) and (max-width: 1023px)',

  desktop: '(min-width: 1024px)'

};
