import { moduleMetadata, storiesOf } from '@storybook/angular';
import { MediaModule } from './index';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/MediaToggleDirective.md').default + require('../docs/index.md').default
};


storiesOf('Argon|media/Media toggle directive', module)
  .addDecorator(
      moduleMetadata({
        imports: [MediaModule]
      })
    )
  .add('Display content by media query', () => ({
    template: `
      <div arMediaToggle="desktop">Desktop value</div>
      <div arMediaToggle="tablet">Tablet value</div>
      <div arMediaToggle="phone">Phone value</div>
    `
  }), { notes })
  .add('Pass list of breakpoints', () => ({
    template: `
        <div [arMediaToggle]="['tablet', 'phone']">Visible for the tablet and phone breakpoints</div>
        <div [arMediaToggle]="['tablet', 'desktop']">Visible for the tablet and desktop breakpoints</div>
      `
  }), { notes });
