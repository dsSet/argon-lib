import { moduleMetadata, storiesOf } from '@storybook/angular';
import { MediaModule } from './index';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/MediaDirective.md').default + require('../docs/index.md').default
};


storiesOf('Argon|media/Media directive', module)
  .addDecorator(
      moduleMetadata({
        imports: [MediaModule]
      })
    )
  .add('Display content by media query', () => ({
    template: `
      <div *arMedia="'desktop'">Desktop value</div>
      <div *arMedia="'tablet'">Tablet value</div>
      <div *arMedia="'phone'">Phone value</div>
    `
  }), { notes })
  .add('Pass list of breakpoints', () => ({
    template: `
        <div *arMedia="['tablet', 'phone']">Visible for the tablet and phone breakpoints</div>
        <div *arMedia="['tablet', 'desktop']">Visible for the tablet and desktop breakpoints</div>
      `
  }), { notes });
