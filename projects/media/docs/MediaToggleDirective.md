# Media toggle mixin `[arMediaToggle]`


Allow to determinate view rendering basing on current breakpoint value. Display will determinate basing on class and css only. 


## Display for breakpoint

````html
<div arMediaToggle="desktop">Desktop value</div>
<div arMediaToggle="tablet">Tablet value</div>
<div arMediaToggle="phone">Phone value</div>
```` 

## Use a list of breakpoints to display

````html
<div [arMediaToggle]="['tablet', 'phone']">Visible for the tablet and phone breakpoints</div>
<div [arMediaToggle]="['tablet', 'desktop']">Visible for the tablet and desktop breakpoints</div>
````


