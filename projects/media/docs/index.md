# @argon/media

## Directives

### `[arMedia]`

|Property |Default value|Description
|---      |---          |---
|`@Input() arMedia: MediaType` `Array<MediaType>` | `undefined`|Breakpoint to show


### `[arMediaToggle]`


|Property |Default value|Description
|---      |---          |---
|`@Input() arMediaToggle: MediaType` `Array<MediaType>` | `undefined`|Breakpoint to show


## Types

### `MediaType`

````
MediaType = 'phone' | 'tablet' | 'desktop'
````
