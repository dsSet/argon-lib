# Media mixin `[arMedia]`


Allow to determinate view rendering basing on current breakpoint value. Will works as *ngIf toggle. 


## Display for breakpoint

````html
<div *arMedia="'desktop'">Desktop value</div>
<div *arMedia="'tablet'">Tablet value</div>
<div *arMedia="'phone'">Phone value</div>
```` 

## Use a list of breakpoints to display

````html
<div *arMedia="['tablet', 'phone']">Visible for the tablet and phone breakpoints</div>
<div *arMedia="['tablet', 'desktop']">Visible for the tablet and desktop breakpoints</div>
````


