import { storiesOf, moduleMetadata } from '@storybook/angular';
import { IconsModule, AR_ICONS_SPRITE_PATH_TOKEN } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

// @ts-ignore
import spriteUrl from '../assets/iconmoon.svg';
import { names } from './icon-names';

storiesOf('Argon|icons/Inline Icon', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        IconsModule
      ],
      providers: [
        { provide: AR_ICONS_SPRITE_PATH_TOKEN, useValue: spriteUrl }
      ]
    })
  ).add('Icon base output', () => ({
      template: `
        <ar-icon name="icon-home"></ar-icon>
      `
    })
  ).add('Icon inside paragraph', () => ({
      template: `
        <p>
          This text should contains
          <ar-icon name="icon-home"></ar-icon>
          icon
        </p>
      `
    })
  ).add('Icon color test', () => ({
    template: `
        <p style="color: blueviolet">
          Icon <ar-icon name="icon-blog"></ar-icon> should inherit text color
        </p>
      `
    })
  ).add('Icon should accept arBsSize property', () => ({
    template: `
      <div style="background-color: #d1d4d6; padding: 10px;">
        <ar-icon-container>
          <ar-icon name="icon-cancel-circle" arBsSize="xss"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsSize="xs"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsSize="sm"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsSize="md"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsSize="lg"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsSize="xl"></ar-icon>
        </ar-icon-container>
      </div>
    `
  })
).add('Icon should accept arBsContext property', () => ({
    template: `
      <div style="background-color: #d1d4d6; padding: 10px;">
        <ar-icon-container>
          <ar-icon name="icon-cancel-circle" arBsContext="info"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="primary"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="secondary"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="success"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="warning"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="danger"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="default"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="light"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="dark"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="link"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="muted"></ar-icon>
          <ar-icon name="icon-cancel-circle" arBsContext="white"></ar-icon>
        </ar-icon-container>
      </div>
      `
  })
).add('Build in icons', () => ({
  props: {
    icons: names
  },
  template: `
    <ar-icon-container *ngFor="let name of icons" style="display: block;">
      <ar-icon [name]="name"></ar-icon> {{ name }}
    </ar-icon-container>
  `
}));
