import { storiesOf, moduleMetadata } from '@storybook/angular';
import { IconsModule, AR_ICONS_SPRITE_PATH_TOKEN } from './index';

import '!!style-loader!css-loader!sass-loader!./style.scss';

// @ts-ignore
import spriteUrl from '../assets/iconmoon.svg';

storiesOf('Argon|icons/Icon container', module)
.addDecorator(
    moduleMetadata({
      imports: [
        IconsModule
      ],
      providers: [
        { provide: AR_ICONS_SPRITE_PATH_TOKEN, useValue: spriteUrl }
      ]
    })
).add('Centered icon', () => ({
    template: `
      <ar-icon-container>
        <ar-icon name="icon-home"></ar-icon>
        This icon should be centered
      </ar-icon-container>
      `
  })
).add('Icon container inline', () => ({
    template: `
    <div>
      <span>Icon container should be inline-block</span>
      <ar-icon-container>
        <ar-icon name="icon-cross"></ar-icon>
        This icon should be centered
      </ar-icon-container>
    </div>
      `
  })
).add('Icon inside context button', () => ({
    template: `
      <div style="text-align: center; margin: 1rem">
        <div>Icon should not have left margin if first-child in container</div>
        <button class="btn btn-success btn-sm">
          <ar-icon-container>
            <ar-icon name="icon-plus"></ar-icon>
            <span>Button text</span>
          </ar-icon-container>
        </button>
      </div>
      <div style="text-align: center; margin: 1rem">
        <div>Icon inside text</div>
        <button class="btn btn-success">
          <ar-icon-container>
            <span>Button</span>
            <ar-icon name="icon-plus"></ar-icon>
            <span>text</span>
          </ar-icon-container>
        </button>
      </div>
      <div style="text-align: center; margin: 1rem">
        <div>Icon should not have right margin if last-child in container</div>
        <button class="btn btn-primary btn-lg">
          <ar-icon-container>
            <span>Button text</span>
            <ar-icon name="icon-plus"></ar-icon>
          </ar-icon-container>
        </button>
      </div>
      `
  })
);
