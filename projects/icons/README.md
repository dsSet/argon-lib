# @argon/icons package

## Versions

- `Angular`: ^15.2.9
- `Rxjs`: ~6.6.3  
- `Cdk`: ^15.2.9

## Install

> npm i --save @argon/icons 

## Documentation

[@argon/icons documentation](./docs/index.md)

## Package content

| Source | Content |
|--------|---------|
|`@argon/icons/assets` | static assets folder |
|`@argon/icons/style` | styles folder |
|`@argon/icons/stories` | storybook folder |
|`@argon/icons/docs` | documentation folder |

## Styling components

Package component are supporting Bootstrap 4 styles. Package also contains custom styles according by BEM technology.

> Custom styles should be included globally.

Custom styles can be applied in 3 ways.

### Include styles as is

Include `index.scss` file to your global app styles

```scss
@import "~@argon/icons/style/index.scss";
```  

### Override default variables

```scss
@import "~@argon/icons/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/icons/style/index.scss";
```

### Use mixins optional

```scss
@import "~@argon/icons/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/icons/style/[package-mixin].scss";

.ar-component-to-styled {
  @include any-package-mixin();
  color: red;
}

```  

Otherwise you can skip components default styles and implement new ones by yourself. 
