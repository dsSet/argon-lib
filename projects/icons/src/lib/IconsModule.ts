import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AR_ICONS_SPRITE_PATH_TOKEN } from './tokens/iconsSpritePathToken';
import { IconComponent } from './components/Icon/IconComponent';
import { IconContainerComponent } from './components/IconContainer/IconContainerComponent';
import { IconContainerContentComponent } from './components/IconContainer/IconContainerContentComponent';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    IconComponent,
    IconContainerComponent,
    IconContainerContentComponent,
  ],
  exports: [
    IconComponent,
    IconContainerComponent,
  ],
  providers: [
    { provide: AR_ICONS_SPRITE_PATH_TOKEN, useValue: '' }
  ]
})
export class IconsModule { }
