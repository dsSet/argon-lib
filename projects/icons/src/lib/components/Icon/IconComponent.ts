import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Inject,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { AR_ICONS_SPRITE_PATH_TOKEN } from '../../tokens/iconsSpritePathToken';
import { ContextMixin, BsSizeType, BsContextType } from '@argon/bs-context';
import { BaseShape } from '@argon/tools';

@Component({
  selector: 'ar-icon',
  template: `
    <svg xmlns="http://www.w3.org/2000/svg" class="ar-icon__sprite">
      <use [attr.xlink:href]="link" *ngIf="name" />
    </svg>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent extends ContextMixin(BaseShape) implements OnInit, OnChanges {

  @Input() name: string;

  @Input() arBsSize: BsSizeType;

  @Input() arBsContext: BsContextType;

  @HostBinding('class.ar-icon') className = true;

  public get link(): string { return `${this.spriteUrl}#${this.name}`; }

  bsClassName = 'text';

  constructor(
    @Inject(AR_ICONS_SPRITE_PATH_TOKEN) protected spriteUrl: string,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }
}
