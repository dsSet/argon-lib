import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IconComponent } from './IconComponent';
import { AR_ICONS_SPRITE_PATH_TOKEN } from '../../tokens/iconsSpritePathToken';
import { CommonModule } from '@angular/common';
import { DebugElement } from '@angular/core';

describe('IconComponent', () => {
  let component: IconComponent;
  let fixture: ComponentFixture<IconComponent>;
  let element: DebugElement;

  const SPRITE_URL = '/fake-sprite.svg';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule
      ],
      providers: [
        { provide: AR_ICONS_SPRITE_PATH_TOKEN, useValue: SPRITE_URL }
      ],
      declarations: [
        IconComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
  });

  it('className should bind .ar-icon class', () => {
    component.className = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-icon')).toEqual(true);
    component.className = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-icon')).toEqual(false);
  });

  it('link should return url link to svg sprite', () => {
    const name = 'fake-icon';
    const expectedResult = `${SPRITE_URL}#${name}`;
    component.name = name;
    fixture.detectChanges();
    expect(component.link).toEqual(expectedResult);
  });

});
