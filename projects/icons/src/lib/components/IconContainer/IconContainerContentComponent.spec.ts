import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IconContainerContentComponent } from './IconContainerContentComponent';

describe('IconContainerContentComponent', () => {
  let component: IconContainerContentComponent;
  let fixture: ComponentFixture<IconContainerContentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        IconContainerContentComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconContainerContentComponent);
    component = fixture.componentInstance;
  });

  it('className should bind .ar-icon-container-content', () => {
    component.className = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-icon-container-content')).toEqual(true);
    component.className = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-icon-container-content')).toEqual(false);
  });

});
