import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ar-icon-container-content',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconContainerContentComponent {

  @HostBinding('class.ar-icon-container-content') className = true;

}
