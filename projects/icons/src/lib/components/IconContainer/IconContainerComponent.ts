import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

@Component({
  selector: 'ar-icon-container',
  template: `
    <ar-icon-container-content>
      <ng-content></ng-content>
    </ar-icon-container-content>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconContainerComponent {

  @HostBinding('class.ar-icon-container') className = true;

}
