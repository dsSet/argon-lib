import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IconContainerComponent } from './IconContainerComponent';
import { IconContainerContentComponent } from './IconContainerContentComponent';

describe('IconContainerComponent', () => {
  let component: IconContainerComponent;
  let fixture: ComponentFixture<IconContainerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        IconContainerComponent,
        IconContainerContentComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IconContainerComponent);
    component = fixture.componentInstance;
  });

  it('className should bind .ar-icon-container', () => {
    component.className = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-icon-container')).toEqual(true);
    component.className = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-icon-container')).toEqual(false);
  });

});
