import { InjectionToken } from '@angular/core';

export const AR_ICONS_SPRITE_PATH_TOKEN = new InjectionToken<string>('AR_ICONS_SPRITE_PATH_TOKEN');
