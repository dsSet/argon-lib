/*
 * Public API Surface of icons
 */

export { IconComponent } from './lib/components/Icon/IconComponent';
export { IconContainerComponent } from './lib/components/IconContainer/IconContainerComponent';

export { IconsModule } from './lib/IconsModule';

export { AR_ICONS_SPRITE_PATH_TOKEN } from './lib/tokens/iconsSpritePathToken';
