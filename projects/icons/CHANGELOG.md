# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@2.0.1...@argon/icons@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/icons





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@1.0.4...@argon/icons@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/icons






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@1.0.4...@argon/icons@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/icons






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@1.0.3...@argon/icons@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/icons





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@1.0.2...@argon/icons@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/icons





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@1.0.1...@argon/icons@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/icons






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.22...@argon/icons@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/icons





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.22...@argon/icons@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/icons






## [0.1.22](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.21...@argon/icons@0.1.22) (2020-08-04)

**Note:** Version bump only for package @argon/icons






## [0.1.21](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.20...@argon/icons@0.1.21) (2020-06-04)


### Bug Fixes

* **icons:** add missed dependency ([d6b6b56](https://bitbucket.org/dsSet/argon-lib/commits/d6b6b56))






## [0.1.20](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.19...@argon/icons@0.1.20) (2020-05-21)

**Note:** Version bump only for package @argon/icons






## [0.1.19](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.18...@argon/icons@0.1.19) (2020-04-18)

**Note:** Version bump only for package @argon/icons





## [0.1.18](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.17...@argon/icons@0.1.18) (2020-04-18)


### Bug Fixes

* **icons:** fix build ([00e94e7](https://bitbucket.org/dsSet/argon-lib/commits/00e94e7))






## [0.1.17](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.16...@argon/icons@0.1.17) (2020-03-24)

**Note:** Version bump only for package @argon/icons





## [0.1.16](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.15...@argon/icons@0.1.16) (2020-03-24)

**Note:** Version bump only for package @argon/icons






## [0.1.15](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.14...@argon/icons@0.1.15) (2020-03-18)

**Note:** Version bump only for package @argon/icons





## [0.1.14](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.13...@argon/icons@0.1.14) (2020-02-14)

**Note:** Version bump only for package @argon/icons






## [0.1.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.12...@argon/icons@0.1.13) (2019-11-29)

**Note:** Version bump only for package @argon/icons





## [0.1.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.11...@argon/icons@0.1.12) (2019-11-22)

**Note:** Version bump only for package @argon/icons





## [0.1.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.10...@argon/icons@0.1.11) (2019-11-22)

**Note:** Version bump only for package @argon/icons





## [0.1.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.9...@argon/icons@0.1.10) (2019-10-28)

**Note:** Version bump only for package @argon/icons





## [0.1.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.8...@argon/icons@0.1.9) (2019-10-11)

**Note:** Version bump only for package @argon/icons





## [0.1.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.7...@argon/icons@0.1.8) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





## [0.1.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.6...@argon/icons@0.1.7) (2019-10-08)

**Note:** Version bump only for package @argon/icons





## [0.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.5...@argon/icons@0.1.6) (2019-09-21)

**Note:** Version bump only for package @argon/icons





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.4...@argon/icons@0.1.5) (2019-09-17)

**Note:** Version bump only for package @argon/icons






## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.3...@argon/icons@0.1.4) (2019-09-07)

**Note:** Version bump only for package @argon/icons





## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.2...@argon/icons@0.1.3) (2019-09-06)

**Note:** Version bump only for package @argon/icons





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.1...@argon/icons@0.1.2) (2019-08-10)

**Note:** Version bump only for package @argon/icons





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/icons@0.1.0...@argon/icons@0.1.1) (2019-07-28)

**Note:** Version bump only for package @argon/icons





# 0.1.0 (2019-07-17)


### Features

* **icons:** add ions module ([53434a7](https://bitbucket.org/dsSet/argon-lib/commits/53434a7))
* **icons:** add path token to public api ([92c09b7](https://bitbucket.org/dsSet/argon-lib/commits/92c09b7))
* **icons:** add styles and stories ([75e2023](https://bitbucket.org/dsSet/argon-lib/commits/75e2023))
