# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@2.0.1...@argon/tools@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/tools





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@1.0.4...@argon/tools@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/tools






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@1.0.4...@argon/tools@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/tools






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@1.0.3...@argon/tools@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/tools





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@1.0.2...@argon/tools@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/tools





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@1.0.1...@argon/tools@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/tools






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.9.1...@argon/tools@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/tools





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.9.1...@argon/tools@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/tools






## [0.9.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.9.0...@argon/tools@0.9.1) (2020-08-04)

**Note:** Version bump only for package @argon/tools






# [0.9.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.8.4...@argon/tools@0.9.0) (2020-05-21)


### Features

* **tools:** implement fullscreen service ([6d1e643](https://bitbucket.org/dsSet/argon-lib/commits/6d1e643))






## [0.8.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.8.3...@argon/tools@0.8.4) (2020-04-18)


### Bug Fixes

* **tools:** fix ui mixins ([5a55179](https://bitbucket.org/dsSet/argon-lib/commits/5a55179))






## [0.8.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.8.2...@argon/tools@0.8.3) (2020-03-24)

**Note:** Version bump only for package @argon/tools





## [0.8.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.8.1...@argon/tools@0.8.2) (2020-03-24)

**Note:** Version bump only for package @argon/tools






## [0.8.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.8.0...@argon/tools@0.8.1) (2020-03-18)


### Bug Fixes

* **tools:** fix scroll shapes definition ([699f3f8](https://bitbucket.org/dsSet/argon-lib/commits/699f3f8))





# [0.8.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.7.2...@argon/tools@0.8.0) (2020-02-14)


### Features

* **tools:** add   case Insensitive comparator ([f4ad64f](https://bitbucket.org/dsSet/argon-lib/commits/f4ad64f))






## [0.7.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.7.1...@argon/tools@0.7.2) (2019-11-29)


### Bug Fixes

* **tools:** fix interface ([dd56c3d](https://bitbucket.org/dsSet/argon-lib/commits/dd56c3d))





## [0.7.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.7.0...@argon/tools@0.7.1) (2019-11-22)

**Note:** Version bump only for package @argon/tools





# [0.7.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.6.3...@argon/tools@0.7.0) (2019-11-22)


### Features

* **table:** base table structure + styles ([b82fa0a](https://bitbucket.org/dsSet/argon-lib/commits/b82fa0a))
* **tools:** implement scroll pattern ([2d3bc14](https://bitbucket.org/dsSet/argon-lib/commits/2d3bc14))





## [0.6.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.6.2...@argon/tools@0.6.3) (2019-10-28)


### Bug Fixes

* **tools:** fix prev state getter for State pattern ([afedd3a](https://bitbucket.org/dsSet/argon-lib/commits/afedd3a))
* **tools:** fix typings inside storyboook ([b2a6693](https://bitbucket.org/dsSet/argon-lib/commits/b2a6693))





## [0.6.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.6.1...@argon/tools@0.6.2) (2019-10-11)

**Note:** Version bump only for package @argon/tools





## [0.6.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.6.0...@argon/tools@0.6.1) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





# [0.6.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.5.1...@argon/tools@0.6.0) (2019-10-08)


### Bug Fixes

* **tools:** add missed files ([084f5ec](https://bitbucket.org/dsSet/argon-lib/commits/084f5ec))
* **tools:** fix compareFn type definition ([e93e4c1](https://bitbucket.org/dsSet/argon-lib/commits/e93e4c1))
* **tools:** fix dependencies ([3b4ba97](https://bitbucket.org/dsSet/argon-lib/commits/3b4ba97))
* **tools:** fix dependencies ([cb938a1](https://bitbucket.org/dsSet/argon-lib/commits/cb938a1))
* **tools:** update HasLifecircleHooks interface ([3441244](https://bitbucket.org/dsSet/argon-lib/commits/3441244))


### Features

* **message-bus:** add message-bus package ([6accdd7](https://bitbucket.org/dsSet/argon-lib/commits/6accdd7))
* **tools:** add UIClickableMixin ([94d3602](https://bitbucket.org/dsSet/argon-lib/commits/94d3602))
* **tools:** extend tabindex mixin with focus method ([fdcb26d](https://bitbucket.org/dsSet/argon-lib/commits/fdcb26d))
* **tools:** restructoring folders, implement state subject ([079cefb](https://bitbucket.org/dsSet/argon-lib/commits/079cefb))





## [0.5.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.5.0...@argon/tools@0.5.1) (2019-09-21)

**Note:** Version bump only for package @argon/tools





# [0.5.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.4.0...@argon/tools@0.5.0) (2019-09-17)


### Features

* **tools:** add CompareValuesMixin ([b11c379](https://bitbucket.org/dsSet/argon-lib/commits/b11c379))





# [0.4.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.3.1...@argon/tools@0.4.0) (2019-09-13)


### Features

* **tools:** implement message bus ([cd58edf](https://bitbucket.org/dsSet/argon-lib/commits/cd58edf))






## [0.3.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.3.0...@argon/tools@0.3.1) (2019-09-07)

**Note:** Version bump only for package @argon/tools





# [0.3.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.2.3...@argon/tools@0.3.0) (2019-09-06)


### Bug Fixes

* **tools:** change tabindex interface ([8bd7d44](https://bitbucket.org/dsSet/argon-lib/commits/8bd7d44))
* **tools:** rename UIClickable to UIShape ([8e90176](https://bitbucket.org/dsSet/argon-lib/commits/8e90176))


### Features

* **tools:** add changeable shape ([4b218c8](https://bitbucket.org/dsSet/argon-lib/commits/4b218c8))
* **tools:** add mixins for tabindex and accesskey functionality ([f86b151](https://bitbucket.org/dsSet/argon-lib/commits/f86b151))
* **tools:** add UI Interactive directive ([88b88e3](https://bitbucket.org/dsSet/argon-lib/commits/88b88e3))
* **tools:** add UIClickable model ([b9ae2d7](https://bitbucket.org/dsSet/argon-lib/commits/b9ae2d7))





## [0.2.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.2.2...@argon/tools@0.2.3) (2019-08-10)

**Note:** Version bump only for package @argon/tools





## [0.2.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.2.1...@argon/tools@0.2.2) (2019-08-10)


### Bug Fixes

* **tools:** add unsubscribable rules to getters and proto properties ([b21933a](https://bitbucket.org/dsSet/argon-lib/commits/b21933a))





## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.2.0...@argon/tools@0.2.1) (2019-07-28)

**Note:** Version bump only for package @argon/tools





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tools@0.1.0...@argon/tools@0.2.0) (2019-07-24)


### Features

* **tools:** add @MapProperty decorator ([eef08d1](https://bitbucket.org/dsSet/argon-lib/commits/eef08d1))
* **tools:** extend @PropertyMap decorator ([5375954](https://bitbucket.org/dsSet/argon-lib/commits/5375954))





# 0.1.0 (2019-07-21)


### Features

* **tools:** add @argon/tools module ([0aef26b](https://bitbucket.org/dsSet/argon-lib/commits/0aef26b))
