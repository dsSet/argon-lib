import 'reflect-metadata';
import { PropertyMapService } from './PropertyMapService';

export const metadataService = new PropertyMapService();

export const PropertyMap = (
  key?: string,
  transformFn?: (value: any, context: object) => any,
  options?: { [key: string]: any }
) => {
  return (target: any, propertyKey: string) => {
    const metadata = metadataService.createPropertyMetadata(propertyKey, key, options, transformFn);
    metadataService.addMetadata(metadata, target);
  };
};
