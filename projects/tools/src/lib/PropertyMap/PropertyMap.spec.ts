import { PropertyMap, metadataService } from './PropertyMap';

describe('PropertyMap', () => {
  it('should be decorator factory', () => {
    const result = PropertyMap();
    expect(typeof result).toEqual('function');
  });

  it('should create and set metadata to target object', () => {
    const metadata = { data: 'fake metadata' } as never;
    const createPropertyMetadataSpy = spyOn(metadataService, 'createPropertyMetadata').and.returnValue(metadata);
    const addMetadataSpy = spyOn(metadataService, 'addMetadata');

    const key = 'FakeKey';
    const transformFn = () => null;
    const options = { data: 'mock options' };
    const target = { data: 'Fake property' };
    const propertyKey = 'MockProperty';

    const decorator = PropertyMap(key, transformFn, options);
    decorator(target, propertyKey);

    expect(createPropertyMetadataSpy).toHaveBeenCalledWith(propertyKey, key, options, transformFn);
    expect(addMetadataSpy).toHaveBeenCalledWith(metadata, target);
  });
});
