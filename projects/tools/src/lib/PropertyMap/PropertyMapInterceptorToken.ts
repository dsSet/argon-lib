import { InjectionToken } from '@angular/core';
import { PropertyMapInterceptorInterface } from './PropertyMapInterceptorInterface';

export const PROPERTY_MAP_INTERCEPTOR_TOKEN = new InjectionToken<PropertyMapInterceptorInterface>('PROPERTY_MAP_INTERCEPTOR_TOKEN');
