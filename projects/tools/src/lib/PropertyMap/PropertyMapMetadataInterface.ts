
export interface PropertyMapMetadataInterface {

  originalKey: string;

  targetKey: string;

  options?: { [key: string]: any };

  transformFn?: (value: any, context: object) => any;

}
