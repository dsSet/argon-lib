import 'reflect-metadata';
import size from 'lodash/size';
import orderBy from 'lodash/orderBy';
import each from 'lodash/each';
import get from 'lodash/get';
import set from 'lodash/set';
import isObject from 'lodash/isObject';
import { Inject, Injectable, Optional } from '@angular/core';
import { PropertyMapMetadataInterface } from './PropertyMapMetadataInterface';
import { AbstractControl } from '@angular/forms';
import { PROPERTY_MAP_INTERCEPTOR_TOKEN } from './PropertyMapInterceptorToken';
import { PropertyMapInterceptorInterface } from './PropertyMapInterceptorInterface';

@Injectable()
export class PropertyMapService {

  static PROPERTY_METADATA_KEY = 'argon:design:property';

  private readonly orderedInterceptors: Array<PropertyMapInterceptorInterface>;

  constructor(
    @Optional() @Inject(PROPERTY_MAP_INTERCEPTOR_TOKEN) private mapInterceptors: Array<PropertyMapInterceptorInterface> = []
  ) {
    this.orderedInterceptors = orderBy(this.mapInterceptors, ['weight'], ['desc']);
  }

  public createPropertyMetadata(
    originalKey: string,
    targetKey?: string,
    options?: { [key: string]: any },
    transformFn?: (value: any, context: object) => any
    ): PropertyMapMetadataInterface {
    return {
      originalKey,
      targetKey: targetKey || originalKey,
      options,
      transformFn
    };
  }

  public addMetadata(metadataValue: PropertyMapMetadataInterface, target: object) {
    const metadata = this.getMetadata(target);
    metadata.push(metadataValue);

    Reflect.defineMetadata(
      PropertyMapService.PROPERTY_METADATA_KEY,
      metadata,
      target
    );
  }

  public getMetadata(target: object): Array<PropertyMapMetadataInterface> {
    if (!Reflect.hasMetadata(PropertyMapService.PROPERTY_METADATA_KEY, target)) {
      return [];
    }

    return Reflect.getMetadata(PropertyMapService.PROPERTY_METADATA_KEY, target);
  }

  public mapProperties(target: object): object {
    const targetMetadata = this.getMetadata(target);
    if (!size(targetMetadata)) {
      return target;
    }
    const result = { };
    each(targetMetadata, (metadata: PropertyMapMetadataInterface) => {
      const value = get(target, metadata.originalKey, null);
      set(result, metadata.targetKey, this.getTargetValue(value, metadata, target));
    });

    return result;
  }

  protected getTargetValue(originalValue: any, metadata: PropertyMapMetadataInterface, context: object): any {
    const value = originalValue instanceof AbstractControl
      ? originalValue.value
      : originalValue;

    if (isObject(value)) {
      return this.mapProperties(value);
    }

    if (!size(this.orderedInterceptors)) {
      return value;
    }

    return this.orderedInterceptors.reduce((accumulator: any, iterceptor: PropertyMapInterceptorInterface) => {
      return iterceptor.mapValue(originalValue, accumulator, metadata, context);
    }, value);
  }

}
