import isNumber from 'lodash/isNumber';
import isString from 'lodash/isString';
import isArray from 'lodash/isArray';
import isBoolean from 'lodash/isBoolean';
import map from 'lodash/map';
import { PropertyMapInterceptorInterface } from './PropertyMapInterceptorInterface';
import { PropertyMapMetadataInterface } from './PropertyMapMetadataInterface';
import { Injectable } from "@angular/core";

@Injectable()
export class PropertyMapDefaultInterceptor implements PropertyMapInterceptorInterface {

  weight = 10;

  public mapValue(originalValue: any, value: any, metadata: PropertyMapMetadataInterface, context: object): any {
    if (isArray(value)) {
      return map(value, (item: any) => this.mapValue(item, item, metadata, context));
    }

    if (metadata.transformFn) {
      return metadata.transformFn(value, context);
    }

    if (isString(value) || isNumber(value) || isBoolean(value)) {
      return value;
    }

    if (!value) {
      return null;
    }

    return value;
  }

}
