import { PropertyMapService } from './PropertyMapService';
import { PropertyMap } from './PropertyMap';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { PropertyMapInterceptorInterface } from './PropertyMapInterceptorInterface';

describe('PropertyMapService', () => {
  let service: PropertyMapService;

  beforeEach(() => {
     service = new PropertyMapService();
  });

  it('PROPERTY_METADATA_KEY should be "argon:design:property"', () => {
    expect(PropertyMapService.PROPERTY_METADATA_KEY).toEqual('argon:design:property');
  });

  it('createPropertyMetadata should create PropertyMapMetadataInterface', () => {
    const originalKey = 'originalKey';
    const targetKey = 'targetKey';
    const options = { data: 'fake options' };
    const transformFn = jasmine.createSpy('transformFn');
    expect(service.createPropertyMetadata(originalKey, targetKey, options, transformFn)).toEqual({
      originalKey, targetKey, options, transformFn
    });

    expect(service.createPropertyMetadata(originalKey, null, options, transformFn)).toEqual({
      originalKey, targetKey: originalKey, options, transformFn
    });
  });

  it('addMetadata should push metadata reflection', () => {
    const mockMetadata = { data: 'fake metadata' } as any;
    const mockObject = { data: 'mock object' };
    const expectedResult = [mockMetadata];
    service.addMetadata(mockMetadata, mockObject);
    expect(service.getMetadata(mockObject)).toEqual(expectedResult);
  });

  it('getMetadata should return [] if object has no metadata', () => {
    const mockObject = { data: 'mock object' };
    expect(service.getMetadata(mockObject)).toEqual([]);
  });

  it('getMetadata should return metadata array', () => {
    const mockMetadata1 = { data: 'fake metadata 1' } as any;
    const mockMetadata2 = { data: 'fake metadata 2' } as any;
    const mockMetadata3 = { data: 'fake metadata 3' } as any;
    const mockObject = { data: 'mock object' };

    service.addMetadata(mockMetadata1, mockObject);
    service.addMetadata(mockMetadata2, mockObject);
    service.addMetadata(mockMetadata3, mockObject);

    expect(service.getMetadata(mockObject)).toEqual([mockMetadata1, mockMetadata2, mockMetadata3]);
  });

  describe('mapProperties', () => {
    const expectedResult = {
      key1: 1,
      key2: true,
      propertyGetter: 18,
      control: 'Form Control',
      group: {
        groupProperty1: 1,
        groupProperty2: 2
      },
      array: [true, 1, 'string'],
      nestedObject: {
        key2_1: 'nested string'
      }
    };

    class MockClass1 {

      @PropertyMap('key1')
      property1 = 1;

      @PropertyMap('key2')
      property2 = true;

      @PropertyMap()
      nestedObject = new MockClass2();

      property3 = 'test';

      @PropertyMap()
      get propertyGetter() { return 18; }

      @PropertyMap()
      control = new UntypedFormControl('Form Control');

      @PropertyMap()
      group = new UntypedFormGroup({
        groupProperty1: new UntypedFormControl(1),
        groupProperty2: new UntypedFormControl(2)
      });

      @PropertyMap()
      array = new UntypedFormArray([
        new UntypedFormControl(true),
        new UntypedFormControl(1),
        new UntypedFormControl('string'),
      ]);
    }

    class MockClass2 {

      @PropertyMap('key2_1')
      nested1 = 'nested string';

    }

    it('mapProperties should return raw object if interceptors not defined', () => {
      const mockObject = new MockClass1();
      expect(service.mapProperties(mockObject)).toEqual(expectedResult);
    });

    it('should use interceptors for mapping', () => {
      const resultValue = 152;
      const interceptor = {
        weight: 5,
        mapValue: jasmine.createSpy('mapValue').and.returnValue(resultValue)
      } as PropertyMapInterceptorInterface;
      const mockService = new PropertyMapService([interceptor]);
      const mockObject = new MockClass2();
      const expectedValue = {
        key2_1: resultValue
      };

      expect(mockService.mapProperties(mockObject)).toEqual(expectedValue);
      expect(interceptor.mapValue)
        .toHaveBeenCalledWith(mockObject.nested1, mockObject.nested1, mockService.getMetadata(mockObject)[0], mockObject);
    });
  });

});
