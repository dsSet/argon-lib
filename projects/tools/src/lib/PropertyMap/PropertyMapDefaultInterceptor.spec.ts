import { PropertyMapDefaultInterceptor } from './PropertyMapDefaultInterceptor';
import { PropertyMapMetadataInterface } from './PropertyMapMetadataInterface';


describe('PropertyMapDefaultInterceptor', () => {
  let interceptor: PropertyMapDefaultInterceptor;
  beforeEach(() => {
    interceptor = new PropertyMapDefaultInterceptor();
  });

  it('should have weight 10', () => {
    expect(interceptor.weight).toEqual(10);
  });

  describe('mapValue', () => {
    let metadata: PropertyMapMetadataInterface;
    const rawMetadata: PropertyMapMetadataInterface = { targetKey: 'targetKey', originalKey: 'originalKey' };
    const originalValue = 'Fake Original Value';
    const value = 'Fake Value';
    const context = { data: 'fake context' };

    beforeEach(() => {
      metadata = jasmine.createSpyObj('PropertyMapMetadataInterface', ['transformFn']);
    });

    it('should apply transformFn if exist', () => {
      interceptor.mapValue(originalValue, value, metadata, context);
      expect(metadata.transformFn).toHaveBeenCalledWith(value, context);
    });

    it('should return null for null and undefined values', () => {
      expect(interceptor.mapValue(1, undefined, rawMetadata, context)).toEqual(null);
      expect(interceptor.mapValue(1, null, rawMetadata, context)).toEqual(null);
    });

    it('should return value for empty string, 0, and false', () => {
      expect(interceptor.mapValue(1, '', rawMetadata, context)).toEqual('');
      expect(interceptor.mapValue(1, 0, rawMetadata, context)).toEqual(0);
      expect(interceptor.mapValue(1, false, rawMetadata, context)).toEqual(false);
    });

    it('should return value for primitive types', () => {
      const fakeObject = { data: 'object property' };
      expect(interceptor.mapValue(1, 'String', rawMetadata, context)).toEqual('String');
      expect(interceptor.mapValue(1, 88, rawMetadata, context)).toEqual(88);
      expect(interceptor.mapValue(1, true, rawMetadata, context)).toEqual(true);
      expect(interceptor.mapValue(1, fakeObject, rawMetadata, context)).toEqual(fakeObject);
    });

    it('should call recursively for arrays', () => {
      spyOn(interceptor, 'mapValue').and.callThrough();
      const arrayValue = [1, 2, 3];
      expect(interceptor.mapValue(123, arrayValue, rawMetadata, context)).toEqual(arrayValue);
      expect(interceptor.mapValue).toHaveBeenCalledWith(123, arrayValue, rawMetadata, context);
      expect(interceptor.mapValue).toHaveBeenCalledWith(1, 1, rawMetadata, context);
      expect(interceptor.mapValue).toHaveBeenCalledWith(2, 2, rawMetadata, context);
      expect(interceptor.mapValue).toHaveBeenCalledWith(3, 3, rawMetadata, context);
    });
  });


});
