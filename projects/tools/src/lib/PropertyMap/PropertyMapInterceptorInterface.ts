import { PropertyMapMetadataInterface } from './PropertyMapMetadataInterface';

export interface PropertyMapInterceptorInterface {

  weight: number;

  mapValue(originalValue: any, value: any, metadata: PropertyMapMetadataInterface, context: object): any;

}
