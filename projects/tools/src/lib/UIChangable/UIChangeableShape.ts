/* tslint:disable:directive-class-suffix */
import { UIShape } from '../Shapes/UIShape';
import { Directive, EventEmitter, Injectable, Output } from '@angular/core';
import { UIChangeableInterface } from './UIChangeableInterface';

@Directive({ selector: '[arUIChangeableShape]' })
export class UIChangeableShape extends UIShape implements UIChangeableInterface {

  @Output() change = new EventEmitter<any>();

}
