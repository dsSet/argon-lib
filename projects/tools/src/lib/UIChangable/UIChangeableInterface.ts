import { EventEmitter } from '@angular/core';

export interface UIChangeableInterface {

  change: EventEmitter<any>;

}
