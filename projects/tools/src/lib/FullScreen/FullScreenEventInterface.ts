
export interface FullScreenEventInterface {

  event: Event;

  element: Element | null;

}
