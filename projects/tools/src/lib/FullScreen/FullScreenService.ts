import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { fromEvent, Observable, of, queueScheduler, scheduled } from 'rxjs';
import { filter, map, mergeAll } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';
import { FullScreenSourceLikeInterface } from './FullScreenSourceLikeInterface';
import { DocumentFullScreenLikeInterface } from './DocumentFullScreenLikeInterface';
import { FullScreenEventInterface } from './FullScreenEventInterface';

@Injectable({ providedIn: 'root' })
export class FullScreenService {

  public get documentOriginal(): Document {
    return this.document as Document;
  }

  public get fullScreenTarget(): Element | null {
    return this.document.fullscreenElement
      || this.document.webkitFullscreenElement
      || this.document.msFullscreenElement
      || this.document.mozFullscreenElement
      || null;
  }

  constructor(
    @Inject(DOCUMENT) public readonly document: DocumentFullScreenLikeInterface
  ) { }

  public createFullScreenListener(element?: Element): Observable<FullScreenEventInterface> {
    return scheduled([
      fromEvent(this.documentOriginal, 'fullscreenchange'),
      fromEvent(this.documentOriginal, 'msfullscreenchange'),
      fromEvent(this.documentOriginal, 'webkitfullscreenchange'),
      fromEvent(this.documentOriginal, 'mozfullscreenchange'),
    ], queueScheduler).pipe(
      mergeAll(),
      filter(() => element && this.fullScreenTarget ? this.fullScreenTarget === element : true),
      map((event: Event) => ({ event, element: this.fullScreenTarget }))
    );
  }

  public isFullScreenAvailable(element: Element): boolean {
    return Boolean(
      this.document.fullscreenEnabled
      || this.document.msFullScreenEnabled
      || this.document.webkitFullscreenEnabled
      || this.document.mozFullscreenEnabled
    ) && Boolean(this.getFullScreenRequest(element));
  }

  public isCloseFullScreenAvailable(): boolean {
    return Boolean(this.getCloseFullScreenSource());
  }

  public getFullScreenRequest(element: Element): () => (Promise<void> | void) {
    if (!element || this.fullScreenTarget) {
      return null;
    }
    const source = element as FullScreenSourceLikeInterface;
    return source.requestFullscreen
      || source.mozRequestFullScreen
      || source.webkitRequestFullscreen
      || source.msRequestFullscreen;
  }

  public getCloseFullScreenSource(): () => (Promise<void> | void) {
    return this.document.exitFullscreen
    || this.document.mozCancelFullScreen
    || this.document.webkitExitFullscreen
    || this.document.msExitFullscreen;
  }

  public requestFullScreen(element: Element): Observable<boolean> {
    let source = this.getFullScreenRequest(element);
    if (source) {
      source = source.bind(element);
      const result: Promise<void> | void = source();
      return result ? fromPromise(result).pipe(map(() => true)) : of(true);
    }

    return of(false);
  }

  public exitFullScreen(): Observable<boolean> {
    let source = this.getCloseFullScreenSource();
    if (source) {
      source = source.bind(this.document);
      const result: Promise<void> | void = source();
      return result ? fromPromise(result).pipe(map(() => true)) : of(true);
    }
    return of(false);
  }

}
