import { FullScreenService } from './FullScreenService';
import { DocumentFullScreenLikeInterface } from './DocumentFullScreenLikeInterface';
import createSpy = jasmine.createSpy;
import { FullScreenSourceLikeInterface } from './FullScreenSourceLikeInterface';
import { fakeAsync, flush, tick } from '@angular/core/testing';
import { FullScreenEventInterface } from './FullScreenEventInterface';
import { AsyncScheduler } from 'rxjs/internal/scheduler/AsyncScheduler';

class DocumentMock implements DocumentFullScreenLikeInterface {

  public readonly mockExitFullScreen: () => (Promise<void> | void);
  public mockFullScreenElement: Element;
  public mockFullscreenEnabled: boolean;

  get fullscreenElement(): Element {
    return this.browserPrefix === null ? this.mockFullScreenElement : null;
  }

  get mozFullscreenElement(): Element {
    return this.browserPrefix === 'moz' ? this.mockFullScreenElement : null;
  }

  get webkitFullscreenElement(): Element {
    return this.browserPrefix === 'webkit' ? this.mockFullScreenElement : null;
  }

  get msFullscreenElement(): Element {
    return this.browserPrefix === 'ms' ? this.mockFullScreenElement : null;
  }

  get fullscreenEnabled(): boolean {
    return this.browserPrefix === null ? this.mockFullscreenEnabled : null;
  }

  get mozFullscreenEnabled(): boolean {
    return this.browserPrefix === 'moz' ? this.mockFullscreenEnabled : null;
  }

  get webkitFullscreenEnabled(): boolean {
    return this.browserPrefix === 'webkit' ? this.mockFullscreenEnabled : null;
  }

  get msFullScreenEnabled(): boolean {
    return this.browserPrefix === 'ms' ? this.mockFullscreenEnabled : null;
  }

  get exitFullscreen(): () => (Promise<void> | void) {
    return this.browserPrefix === null ? this.mockExitFullScreen : null;
  }

  get mozCancelFullScreen(): () => (Promise<void> | void) {
    return this.browserPrefix === 'moz' ? this.mockExitFullScreen : null;
  }

  get webkitExitFullscreen(): () => (Promise<void> | void) {
    return this.browserPrefix === 'webkit' ? this.mockExitFullScreen : null;
  }

  get msExitFullscreen(): () => (Promise<void> | void) {
    return this.browserPrefix === 'ms' ? this.mockExitFullScreen : null;
  }

  constructor(
    private browserPrefix: null | 'moz' | 'webkit' | 'ms'
  ) {
    if (browserPrefix === 'webkit' || browserPrefix === 'ms') {
      this.mockExitFullScreen = createSpy('mockExitFullScreen');
    } else {
      this.mockExitFullScreen = createSpy('mockExitFullScreen').and.returnValue(Promise.resolve());
    }
  }

  public setFullScreenElement(element: Element) {
    this.mockFullScreenElement = element;
  }

  public setFullscreenEnabled(isEnabled: boolean) {
    this.mockFullscreenEnabled = isEnabled;
  }

}

class ElementMock implements FullScreenSourceLikeInterface {

  public readonly mockRequestFullScreen: () => (Promise<void> | void);

  get requestFullscreen(): () => (Promise<void> | void) {
    return this.browserPrefix === null ? this.mockRequestFullScreen : null;
  }

  get mozRequestFullScreen(): () => (Promise<void> | void) {
    return this.browserPrefix === 'moz' ? this.mockRequestFullScreen : null;
  }

  get webkitRequestFullscreen(): () => (Promise<void> | void) {
    return this.browserPrefix === 'webkit' ? this.mockRequestFullScreen : null;
  }

  get msRequestFullscreen(): () => (Promise<void> | void) {
    return this.browserPrefix === 'ms' ? this.mockRequestFullScreen : null;
  }

  constructor(
    private browserPrefix: null | 'moz' | 'webkit' | 'ms'
  ) {
    if (browserPrefix === 'webkit' || browserPrefix === 'ms') {
      this.mockRequestFullScreen = createSpy('mockRequestFullScreen');
    } else {
      this.mockRequestFullScreen = createSpy('mockRequestFullScreen').and.returnValue(Promise.resolve());
    }
  }

}


describe('FullScreenService', () => {
  let service: FullScreenService;
  let documentMock: DocumentMock;
  const env = [null, 'ms', 'moz', 'webkit'];
  // tslint:disable-next-line:prefer-for-of
  for (let index = 0; index < env.length; index++) {
    const prefix: null | 'moz' | 'webkit' | 'ms' = env[index] as any;
    const title = prefix ? `Run for ${prefix} env` : 'Run for the standard API';
    describe(title, () => {
      beforeEach(() => {
        documentMock = new DocumentMock(prefix);
        service = new FullScreenService(documentMock);
      });

      it('documentOriginal should returns document element', () => {
        expect(service.documentOriginal).toEqual(service.document as any);
      });

      describe('getFullScreenRequest', () => {
        it('should return null if has no requested element', () => {
          expect(service.getFullScreenRequest(null)).toEqual(null);
        });

        it('should return null if document already have fullscreen target', () => {
          const element = new ElementMock(prefix);
          documentMock.setFullScreenElement(document.createElement('div'));
          expect(service.getFullScreenRequest(element as any)).toEqual(null);
        });

        it('should return fullscreen source fn', () => {
          const element = new ElementMock(prefix);
          expect(service.getFullScreenRequest(element as any)).toEqual(element.mockRequestFullScreen);
        });
      });

      it('getCloseFullScreenSource should return close screen', () => {
        expect(service.getCloseFullScreenSource()).toEqual(documentMock.mockExitFullScreen);
      });

      describe('requestFullScreen', () => {
        it('should return observable with true', fakeAsync(() => {
          let result: boolean;
          const element = new ElementMock(prefix);
          service.requestFullScreen(element as any).subscribe((response: boolean) => {
            result = response;
          });
          tick(50);
          expect(result).toEqual(true);
        }));

        it('should return false if fullscreen was not opened', fakeAsync(() => {
          let result: boolean;
          const element = new ElementMock(prefix);
          documentMock.setFullScreenElement(document.createElement('div'));
          service.requestFullScreen(element as any).subscribe((response: boolean) => {
            result = response;
          });
          tick(50);
          expect(result).toEqual(false);
        }));
      });

      describe('exitFullScreen', () => {
        it('should return observable with true', fakeAsync(() => {
          let result: boolean;
          service.exitFullScreen().subscribe((response: boolean) => {
            result = response;
          });
          tick(50);
          expect(result).toEqual(true);
        }));

        it('should return false if has no close source', fakeAsync(() => {
          let result: boolean;
          spyOn(service, 'getCloseFullScreenSource').and.returnValue(null);
          const element = new ElementMock(prefix);
          service.exitFullScreen().subscribe((response: boolean) => {
            result = response;
          });
          tick(50);
          expect(result).toEqual(false);
        }));
      });

      it('isCloseFullScreenAvailable should check close source', () => {
        const spy = spyOn(service, 'getCloseFullScreenSource');
        spy.and.returnValue(() => null);
        expect(service.isCloseFullScreenAvailable()).toEqual(true);
        spy.and.returnValue(null);
        expect(service.isCloseFullScreenAvailable()).toEqual(false);
      });

      describe('isFullScreenAvailable', () => {
        it('should check with document.fullscreenEnabled', () => {
          const element = new ElementMock(prefix);
          documentMock.setFullscreenEnabled(true);
          expect(service.isFullScreenAvailable(element as any)).toEqual(true);
          documentMock.setFullscreenEnabled(false);
          expect(service.isFullScreenAvailable(element as any)).toEqual(false);
        });

        it('should check with element full screen source', () => {
          const element = new ElementMock(prefix);
          documentMock.setFullscreenEnabled(true);
          spyOn(service, 'getFullScreenRequest').and.returnValue(null);
          expect(service.isFullScreenAvailable(element as any)).toEqual(false);
        });
      });

      it('fullScreenTarget should returns target element from document', () => {
        documentMock.setFullScreenElement(document.createElement('div'));
        expect(service.fullScreenTarget).toEqual(documentMock.mockFullScreenElement);
        documentMock.setFullScreenElement(null);
        expect(service.fullScreenTarget).toEqual(null);
      });

      describe('createFullScreenListener', () => {
        class ServiceExt extends FullScreenService {
          public mockFullScreenTarget: Element;
          public get fullScreenTarget(): Element | null {
            return this.mockFullScreenTarget;
          }

          constructor() {
            super(document);
          }
        }

        it('should create full screen event filter', fakeAsync(() => {
          let result;
          const mockService = new ServiceExt();
          mockService.mockFullScreenTarget = document.createElement('div');
          const eventName = prefix ? `${prefix}fullscreenchange` : 'fullscreenchange';
          const event = new Event(eventName);
          mockService.createFullScreenListener().subscribe((data) => {
            result = data;
          });
          document.dispatchEvent(event);
          tick(50);
          expect(result).toBeDefined();
          expect(result.event).toEqual(event);
          expect(result.element).toEqual(mockService.mockFullScreenTarget);
        }));

        it('should use filter by element', fakeAsync(() => {
          let result;
          const element = document.createElement('span');
          const mockService = new ServiceExt();
          mockService.mockFullScreenTarget = document.createElement('div');
          const eventName = prefix ? `${prefix}fullscreenchange` : 'fullscreenchange';
          const event = new Event(eventName);
          mockService.createFullScreenListener(element).subscribe((data) => {
            result = data;
          });
          document.dispatchEvent(event);
          tick(50);
          expect(result).not.toBeDefined();

          mockService.mockFullScreenTarget = element;
          document.dispatchEvent(event);
          tick(50);
          expect(result).toBeDefined();
        }));
      });
    });
  }
});
