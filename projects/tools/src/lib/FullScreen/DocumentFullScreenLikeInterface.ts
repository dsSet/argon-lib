

export interface DocumentFullScreenLikeInterface {

  exitFullscreen?: () => (Promise<void> | void);

  mozCancelFullScreen?: () => (Promise<void> | void);

  webkitExitFullscreen?: () => (Promise<void> | void);

  msExitFullscreen?: () => (Promise<void> | void);

  readonly fullscreenEnabled?: boolean;

  readonly msFullScreenEnabled?: boolean;

  readonly webkitFullscreenEnabled?: boolean;

  readonly mozFullscreenEnabled?: boolean;

  readonly fullscreenElement?: Element;

  readonly mozFullscreenElement?: Element;

  readonly webkitFullscreenElement?: Element;

  readonly msFullscreenElement?: Element;

}
