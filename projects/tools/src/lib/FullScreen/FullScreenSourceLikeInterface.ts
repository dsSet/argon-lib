

export interface FullScreenSourceLikeInterface {

  requestFullscreen?: () => (Promise<void> | void);

  mozRequestFullScreen?: () => (Promise<void> | void);

  webkitRequestFullscreen?: () => (Promise<void> | void);

  msRequestFullscreen?: () => (Promise<void> | void);

}

