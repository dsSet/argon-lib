import { CompareFn } from './CompareFn';

export interface CompareValuesInterface {

  compareFn: CompareFn;

  isEqual<TValue>(itemA: TValue, itemB: TValue): boolean;

  find<TValue>(collection: Array<TValue>, itemB: TValue): TValue | null;

  findIndex<TValue>(collection: Array<TValue>, itemB: TValue): number;

  diff<TValue>(collectionA: Array<TValue>, collectionB: Array<TValue>): Array<TValue>;

  push<TValue>(collection: Array<TValue>, item: TValue): Array<TValue>;

  remove<TValue>(collection: Array<TValue>, item: TValue): Array<TValue>;

}
