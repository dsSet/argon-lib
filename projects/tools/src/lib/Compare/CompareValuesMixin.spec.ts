import { CompareValuesMixin } from './CompareValuesMixin';
import { Comparators } from './Comparators';


describe('CompareValuesMixin', () => {

  class BaseClass { }

  class MockComponent extends CompareValuesMixin(BaseClass) { }

  let object: MockComponent;

  beforeEach(() => {
    object = new MockComponent();
  });

  it('should provide default compareFn', () => {
    expect(object.compareFn).toEqual(Comparators.defaultComparator);
  });

  it('isEqual should return return false if one of values is not defined', () => {
    expect(object.isEqual(1, null)).toEqual(false);
    expect(object.isEqual(null, 'Test')).toEqual(false);
  });

  it('isEqual should call compareFn to compare items', () => {
    spyOn(object, 'compareFn');
    const itemA = 90;
    const itemB = 22;
    object.isEqual(itemA, itemB);
    expect(object.compareFn).toHaveBeenCalledWith(itemA, itemB);
  });

  it('findIndex should call isEqual to compare items', () => {
    spyOn(object, 'isEqual').and.returnValue(false);
    const collection = [1, 2];
    const item = 3;
    object.findIndex(collection, item);
    expect(object.isEqual).toHaveBeenCalledWith(1, 3);
    expect(object.isEqual).toHaveBeenCalledWith(2, 3);
  });

  it('findIndex should return item index', () => {
    const collection = [1, 2];
    const item = 2;
    expect(object.findIndex(collection, item)).toEqual(1);
  });

  it('findIndex should return -1 if item not found', () => {
    const collection = [1, 2];
    const item = 3;
    expect(object.findIndex(collection, item)).toEqual(-1);
  });

  it('find should call isEqual to compare items', () => {
    spyOn(object, 'isEqual').and.returnValue(false);
    const collection = [1, 2];
    const item = 3;
    object.find(collection, item);
    expect(object.isEqual).toHaveBeenCalledWith(1, 3);
    expect(object.isEqual).toHaveBeenCalledWith(2, 3);
  });

  it('find should return item', () => {
    const collection = [1, 2];
    const item = 2;
    expect(object.find(collection, item)).toEqual(2);
  });

  it('find should return null if item not found', () => {
    const collection = [1, 2];
    const item = 3;
    expect(object.find(collection, item)).toEqual(null);
  });

  it('diff should call isEqual', () => {
    spyOn(object, 'compareFn').and.returnValue(false);
    const collection1 = [1, 2];
    const collection2 = [1, 2];
    object.diff(collection1, collection2);
    expect(object.compareFn).toHaveBeenCalled();
  });

  it('diff return collections difference', () => {
    expect(object.diff([1, 2, 3], [1, 2])).toEqual([3]);
    expect(object.diff([1, 2], [1, 2, 3])).toEqual([]);
    expect(object.diff([1, 2, 1], [1, 2])).toEqual([]);
  });

  it('push should add item to collection', () => {
    expect(object.push([1, 2, 3], 3)).toEqual([1, 2, 3, 3]);
    expect(object.push([], 3)).toEqual([3]);
  });

  it('remove should remove item from collection', () => {
    expect(object.remove([1, 2, 3], 2)).toEqual([1, 3]);
    expect(object.remove([1, 2, 3], 8)).toEqual([1, 2, 3]);
    expect(object.remove([1, 2, 3, 1, 1], 1)).toEqual([2, 3]);
  });
});
