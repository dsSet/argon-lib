
export type CompareFn = (itemA: any, itemB: any) => boolean;
