import findIndex from 'lodash/findIndex';
import differenceWith from 'lodash/differenceWith';
import find from 'lodash/find';
import { Constructor } from '../types/Constructor';
import { CompareValuesInterface } from './CompareValuesInterface';
import { Injectable, Input, Directive } from '@angular/core';
import { Comparators } from './Comparators';
import { CompareFn } from './CompareFn';

export function CompareValuesMixin<T extends Constructor<any>>(
  BaseClass: T
): T & Constructor<CompareValuesInterface> {

  @Directive()
@Injectable()
  class CompareValuesExtension extends BaseClass implements CompareValuesInterface {

    @Input() compareFn: CompareFn = Comparators.defaultComparator;

    isEqual<TValue>(itemA: TValue, itemB: TValue): boolean {
      if ((!itemA && itemB) || (itemA && !itemB)) {
        return false;
      }
      return this.compareFn(itemA, itemB);
    }

    findIndex<TValue>(collection: Array<TValue>, item: TValue): number {
      return findIndex(collection, (collectionItem: TValue) => this.isEqual(collectionItem, item));
    }

    find<TValue>(collection: Array<TValue>, item: TValue): TValue | null {
      return find(collection, (collectionItem: TValue) => this.isEqual(collectionItem, item)) || null;
    }

    diff<TValue>(collectionA: Array<TValue>, collectionB: Array<TValue>): Array<TValue> {
      return differenceWith(collectionA, collectionB, this.compareFn);
    }

    push<TValue>(collection: Array<TValue>, item: TValue): Array<TValue> {
      return [...collection, item];
    }

    remove<TValue>(collection: Array<TValue>, item: TValue): Array<TValue> {
      return this.diff(collection, [item]);
    }


  }

  return CompareValuesExtension;
}
