import isEqual from 'lodash/isEqual';

export class Comparators {

  public static defaultComparator<TValue>(valueA: TValue, valueB: TValue): boolean {
    return valueA === valueB;
  }

  public static stringComparator<TValue>(valueA: TValue, valueB: TValue): boolean {
    return Comparators.defaultComparator(JSON.stringify(valueA), JSON.stringify(valueB));
  }

  public static caseInsensitiveComparator<TValue>(valueA: TValue, valueB: TValue): boolean {
    return Comparators.defaultComparator(String(valueA).toLowerCase(), String(valueB).toLowerCase());
  }

  public static deepEqualComparator<TValue>(valueA: TValue, valueB: TValue) {
    return isEqual(valueA, valueB);
  }

}

