import { SimpleChanges } from '@angular/core';

export interface HasLifecircleHooksInterface {

  ngOnChanges?(changes: SimpleChanges): void;

  ngOnInit?(): void;

  ngDoCheck?(): void;

  ngAfterContentInit?(): void;

  ngAfterContentChecked?(): void;

  ngAfterViewInit?(): void;

  ngAfterViewChecked?(): void;

  ngOnDestroy?(): void;

}
