/* tslint:disable:directive-class-suffix */
import { UIClickableInterface } from '../UIClickable/UIClickableInterface';
import {
  Directive,
  EventEmitter,
  Injector,
  Input,
  Output
} from '@angular/core';
import { BaseShape } from './BaseShape';

@Directive({ selector: '[arUiShape]' })
export class UIShape extends BaseShape implements UIClickableInterface {

  @Output() click = new EventEmitter<void>();

  @Input() disabled: boolean;

  get attrDisabled(): string {
    return this.disabled ? '' : null;
  }

  constructor(injector: Injector) {
    super(injector);
  }
}
