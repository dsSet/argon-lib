import { Injectable, Injector } from '@angular/core';
import { HasInjectorInterface } from './HasInjectorInterface';

@Injectable()
export class BaseShape implements HasInjectorInterface {

  constructor(public injector: Injector) { }

}
