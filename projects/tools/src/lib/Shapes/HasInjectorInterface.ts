import { Injector } from '@angular/core';

export interface HasInjectorInterface {
  injector: Injector;
}
