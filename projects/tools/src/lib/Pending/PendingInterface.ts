import { PendingSubject } from './PendingSubject';

export interface PendingInterface {

  pending: PendingSubject;

}
