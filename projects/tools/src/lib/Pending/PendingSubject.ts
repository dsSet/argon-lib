import { BehaviorSubject } from 'rxjs';


export class PendingSubject extends BehaviorSubject<boolean> {

  public get count(): number { return this.accumulator; }

  private accumulator = 0;

  constructor(value: boolean) {
    super(value);
  }

  next(value: boolean): void {
    if (value) {
      this.accumulator++;
    } else if (this.accumulator > 0) {
      this.accumulator--;
    }

    if (value && this.accumulator === 1) {
      super.next(true);
    } else if (!value && this.accumulator === 0) {
      super.next(false);
    }
  }
}
