import 'reflect-metadata';
import { Subscription } from 'rxjs';
import { PendingInterface } from './PendingInterface';

export function Pending(target: PendingInterface, propertyKey: string | symbol): any {

  const propertyType: any = Reflect.getMetadata('design:type', target, propertyKey);

  if (propertyType !== Subscription) {
    throw new Error('@Pending can be applied to Subscription type only');
  }

  const descriptor = Object.getOwnPropertyDescriptor(target, propertyKey) || { enumerable: true, configurable: true };

  let value: Subscription;

  const originalGet = descriptor.get || (() => value);
  const originalSet = descriptor.set || ((val: Subscription) => { value = val; });

  descriptor.get = originalGet;

  descriptor.set = function setProperty(val: Subscription) {
    originalSet(val);
    value = val;
    this.pending.next(true);
    value.add(() => {
      this.pending.next(false);
    });
  };

  return descriptor;

}
