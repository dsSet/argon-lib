import { PendingSubject } from './PendingSubject';
import { Subscription } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';


describe('PendingSubject', () => {

  let pendingSubject: PendingSubject;

  const defaultValue = false;

  let subscription: Subscription;

  beforeEach(() => {
    pendingSubject = new PendingSubject(defaultValue);
  });

  afterEach(() => {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  });

  it('should have 0 count by default', () => {
    expect(pendingSubject.count).toEqual(0);
  });

  it('should have default value in state', () => {
    expect(pendingSubject.value).toEqual(defaultValue);
  });

  it('should increase accumulator for true values', () => {
    pendingSubject.next(true);
    expect(pendingSubject.count).toEqual(1);
    pendingSubject.next(true);
    expect(pendingSubject.count).toEqual(2);
  });

  it('should decrease accumulator for true values', () => {
    pendingSubject.next(true);
    pendingSubject.next(true);
    pendingSubject.next(true);
    expect(pendingSubject.count).toEqual(3);
    pendingSubject.next(false);
    expect(pendingSubject.count).toEqual(2);
    pendingSubject.next(false);
    expect(pendingSubject.count).toEqual(1);
    pendingSubject.next(false);
    expect(pendingSubject.count).toEqual(0);
  });

  it('should emit true value if accumulator is 1 and next is true or acc is 0 and next is false', fakeAsync(() => {
    const calls = [];
    subscription = pendingSubject.subscribe((value) => { calls.push(value); });
    pendingSubject.next(true);
    pendingSubject.next(true);
    pendingSubject.next(true);
    tick();
    expect(calls).toEqual([defaultValue, true]);

    pendingSubject.next(false);
    tick();
    expect(calls).toEqual([defaultValue, true]);

    pendingSubject.next(false);
    tick();
    expect(calls).toEqual([defaultValue, true]);

    pendingSubject.next(false);
    tick();
    expect(calls).toEqual([defaultValue, true, false]);
  }));

});
