import { PendingInterface } from './PendingInterface';
import createSpyObj = jasmine.createSpyObj;
import { Subject, Subscription } from 'rxjs';
import { Pending } from './Pending';

describe('@Pending', () => {

  class MockClass implements PendingInterface {
    pending = createSpyObj(['next']);

    public get next(): any { return this.pending.next; }

    @Pending
    public subscription: Subscription;

  }

  let mock: MockClass;

  beforeEach(() => {
    mock = new MockClass();
  });

  it('should call next state with true on add subscription', () => {
    const subject = new Subject();
    mock.subscription = subject.subscribe();
    expect(mock.next).toHaveBeenCalledWith(true);
    subject.complete();
    expect(mock.next).toHaveBeenCalledWith(false);
  });

  it('should call next state with true change subscription', () => {
    const subject = new Subject();
    mock.subscription = subject.subscribe();
    expect(mock.next).toHaveBeenCalledWith(true);
    mock.subscription = subject.subscribe();
    expect(mock.next).toHaveBeenCalledWith(true);
    subject.complete();
    expect(mock.next).toHaveBeenCalledWith(false);
    expect(mock.next).toHaveBeenCalledWith(false);
  });

  it('should throw error for non subscription fields', () => {
    expect(() => {
      class MockErrorClass implements PendingInterface {
        pending = createSpyObj(['next']);

        @Pending
        public subscription: number;
      }
      const error = new MockErrorClass();
    }).toThrow(new Error('@Pending can be applied to Subscription type only'));
  });
});
