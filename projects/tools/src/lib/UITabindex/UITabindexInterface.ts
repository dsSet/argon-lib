import { ElementRef, OnDestroy, OnInit } from '@angular/core';
import { KeyboardService } from '@argon/keyboard';

export interface UITabindexInterface extends OnInit, OnDestroy {

  tabIndex: number;

  readonly attrTabIndex: number;

  keyboardService: KeyboardService;

  element: ElementRef<HTMLElement>;

  focus(): void;

}
