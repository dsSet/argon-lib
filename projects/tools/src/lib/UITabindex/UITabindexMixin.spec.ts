import { UIClickableInterface } from '../UIClickable/UIClickableInterface';
import { Component, DebugElement, EventEmitter, Injector, Input, OnDestroy, OnInit, Output, Directive } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UITabindexMixin } from './UITabindexMixin';
import { HasInjectorInterface } from '../Shapes/HasInjectorInterface';
import { KeyboardService, KeyCodesEnum } from '@argon/keyboard';
import { KeyboardServiceMock } from '../../../../../test/KeyboardServiceMock';
import { HasLifecircleHooksInterface } from '../Shapes/HasLifecircleHooksInterface';

describe('UITabindexMixin', () => {

  @Directive()
class ClickableComponent implements UIClickableInterface, HasInjectorInterface, HasLifecircleHooksInterface, OnInit, OnDestroy {
    @Output() click = new EventEmitter<void>();
    @Input() disabled: boolean;

    constructor(public injector: Injector) { }

    ngOnInit(): void { }

    ngOnDestroy(): void { }
  }

  @Component({
    selector: 'spec-fake-component', template: '',
    host: {
      '[attr.tabindex]': 'attrTabIndex'
    }
  })
  class FakeComponent extends UITabindexMixin(ClickableComponent) {
    constructor(public injector: Injector) {
      super(injector);
    }
  }

  let component: FakeComponent;
  let fixture: ComponentFixture<FakeComponent>;
  let element: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        FakeComponent
      ],
      providers: [
        { provide: KeyboardService, useClass: KeyboardServiceMock }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FakeComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
  });


  it('should have default values', () => {
    expect(component.tabIndex).toEqual(0);
  });

  it('tabIndex should bind attr tabindex', () => {
    component.tabIndex = 5;
    component.disabled = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('tabindex')).toEqual('5');
  });

  it('disabled component should have tabindex = -1', () => {
    component.tabIndex = 5;
    component.disabled = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('tabindex')).toEqual(null);
  });

  it('Space and Enter should trigger click event', () => {
    const keyboardService: KeyboardServiceMock = TestBed.inject(KeyboardService) as any;
    spyOn(component.click, 'emit');
    component.ngOnInit();
    const event = new KeyboardEvent('keydown', { code: KeyCodesEnum.Enter });
    keyboardService.listenKeyDownEventSubject.next(event);
    expect(component.click.emit).toHaveBeenCalled();
  });

  it('Space and Enter should not trigger click event if disabled', () => {
    const keyboardService: KeyboardServiceMock = TestBed.inject(KeyboardService) as any;
    component.disabled = true;
    spyOn(component.click, 'emit');
    component.ngOnInit();
    const event = new KeyboardEvent('keydown', { code: KeyCodesEnum.Enter });
    keyboardService.listenKeyDownEventSubject.next(event);
    expect(component.click.emit).not.toHaveBeenCalled();
  });

  it('ngOnInit should add keyboard listeners', () => {
    const keyboardService: KeyboardServiceMock = TestBed.inject(KeyboardService) as any;
    component.ngOnInit();
    expect(keyboardService.listenKeyDownEvent).toHaveBeenCalledWith([KeyCodesEnum.Space, KeyCodesEnum.Enter], element.nativeElement);
  });

  it('ngOnDestroy should remove keyboard subscriptions', () => {
    const keyboardService: KeyboardServiceMock = TestBed.inject(KeyboardService) as any;
    component.ngOnInit();
    expect(keyboardService.listenKeyDownEventSubject.observers.length).toEqual(1);
    component.ngOnDestroy();
    expect(keyboardService.listenKeyDownEventSubject.observers.length).toEqual(0);
  });
});
