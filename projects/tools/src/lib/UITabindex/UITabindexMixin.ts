import { ElementRef, Injectable, Input, OnDestroy, OnInit, Directive } from '@angular/core';
import { Constructor } from '../types/Constructor';
import { HasInjectorInterface } from '../Shapes/HasInjectorInterface';
import { KeyboardService, KeyCodesEnum } from '@argon/keyboard';
import { Unsubscribable } from '../Unsubscribable/Unsubscribable';
import { Subscription } from 'rxjs';
import { HasLifecircleHooksInterface } from '../Shapes/HasLifecircleHooksInterface';
import { UITabindexInterface } from './UITabindexInterface';
import { UIClickableInterface } from '../UIClickable/UIClickableInterface';


export function UITabindexMixin<T extends Constructor<UIClickableInterface & HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<UITabindexInterface> {

  @Directive()
@Injectable()
  class UITabindexExtended extends BaseClass implements UITabindexInterface, OnInit, OnDestroy {

    @Input() tabIndex = 0;

    get attrTabIndex(): number {
      return this.disabled ? null : this.tabIndex;
    }

    public keyboardService: KeyboardService;

    public element: ElementRef<HTMLElement>;

    private keyboardSubscription: Subscription;

    ngOnInit(): void {
      if (super.ngOnInit) {
        super.ngOnInit();
      }

      this.keyboardService = this.injector.get(KeyboardService);
      this.element = this.injector.get(ElementRef);

      this.keyboardSubscription = this.keyboardService
        .listenKeyDownEvent([KeyCodesEnum.Space, KeyCodesEnum.Enter], this.element.nativeElement)
        .subscribe(this.handleKeypress);
    }

    @Unsubscribable
    ngOnDestroy(): void {
      if (super.ngOnDestroy) {
        super.ngOnDestroy();
      }
    }

    public focus() {
      if (this.disabled || this.attrTabIndex < 0) {
        return;
      }

      this.element.nativeElement.focus();
    }

    private handleKeypress = () => {
      if (!this.disabled) {
        this.click.emit();
      }
    }
  }

  return UITabindexExtended;
}
