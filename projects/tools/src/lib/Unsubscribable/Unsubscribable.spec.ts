import { Unsubscribable } from './Unsubscribable';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { EventEmitter } from '@angular/core';
import { Pending } from '../Pending/Pending';

const createMockDescriptor = (value = () => { }): PropertyDescriptor => ({
  value,
  configurable: true,
  enumerable: true,
  writable: true
});

const runDecorator = (context: any, ngOnDestroy?: any) => {
  const descriptor = createMockDescriptor(ngOnDestroy);
  Unsubscribable({ }, 'ngOnDestroy', descriptor);
  descriptor.value.call(context);
};

describe('Unsubscribable', () => {
  it('should throw error when call without descriptor', () => {
    expect(() => { Unsubscribable({ }, 'FakeProperty', null); })
      .toThrow(new Error('@Unsubscribable: look like your script target is less than ES5. decorator was not applied'));
  });

  it('should throw error if propertyKey !== ngOnDestroy', () => {
    expect(() => { Unsubscribable(null, 'FakeProperty', createMockDescriptor()); })
      .toThrow(new Error('@Unsubscribable should be applied to ngOnDestroy property'));
  });

  it('should call original ngOnDestroy method', () => {
    const ngOnDestroy = jasmine.createSpy();
    const descriptor = createMockDescriptor(ngOnDestroy);
    Unsubscribable({ }, 'ngOnDestroy', descriptor);
    descriptor.value();
    expect(ngOnDestroy).toHaveBeenCalled();
  });

  it('should call original ngOnDestroy only if called without context', () => {
    const contextSpy = jasmine.createSpy();
    const ngOnDestroy = function() {
      contextSpy(this);
    };
    runDecorator(null, ngOnDestroy);
    expect(contextSpy).toHaveBeenCalledWith(null);
  });

  it('should call original ngOnDestroy method (call with fake context)', () => {
    const contextSpy = jasmine.createSpy();
    const ngOnDestroy = function() {
      contextSpy(this);
    };
    const context = { data: 'fake context' };
    runDecorator(context, ngOnDestroy);
    expect(contextSpy).toHaveBeenCalledWith(context);
  });

  it('should call .unsubscribe() for Subscription if subscription is ton closed', () => {
    const unsubscribeSpy = jasmine.createSpy();
    const subscription = new Subscription(unsubscribeSpy);
    expect(subscription.closed).toEqual(false);

    runDecorator({ subscription });

    expect(unsubscribeSpy).toHaveBeenCalled();
    expect(subscription.closed).toEqual(true);
  });

  it('should not call .unsubscribe() for Closed subscriptions', () => {
    const subscription = {
      unsubscribe: jasmine.createSpy('unsubscribe'),
      closed: true
    };
    runDecorator({ subscription });
    expect(subscription.unsubscribe).not.toHaveBeenCalled();
  });

  it('should not call .complete() for EventEmitter', () => {
    const eventEmitter = new EventEmitter();
    spyOn(eventEmitter, 'complete');
    expect(eventEmitter.isStopped).toEqual(false);
    runDecorator({ eventEmitter });
    expect(eventEmitter.complete).not.toHaveBeenCalled();
  });

  it('should call .complete() for Subject if subject not stopped', () => {
    const subject = new Subject();
    spyOn(subject, 'complete');
    expect(subject.isStopped).toEqual(false);
    runDecorator({ subject });
    expect(subject.complete).toHaveBeenCalled();
  });

  it('should not call .complete() for stopped Subject', () => {
    const subject = new Subject();
    subject.complete();
    spyOn(subject, 'complete');
    expect(subject.isStopped).toEqual(true);
    runDecorator({ subject });
    expect(subject.complete).not.toHaveBeenCalled();
  });

  it('should call .complete() for BehaviorSubject if subject not stopped', () => {
    const subject = new BehaviorSubject(null);
    spyOn(subject, 'complete');
    expect(subject.isStopped).toEqual(false);
    runDecorator({ subject });
    expect(subject.complete).toHaveBeenCalled();
  });

  it('should not call .complete() for stopped BehaviorSubject', () => {
    const subject = new BehaviorSubject(null);
    subject.complete();
    spyOn(subject, 'complete');
    expect(subject.isStopped).toEqual(true);
    runDecorator({ subject });
    expect(subject.complete).not.toHaveBeenCalled();
  });

  it('should clean up descriptors', () => {
    class Target {
      get subject(): Subject<any> {
        return this.innerSubject;
      }
      set subject(val: Subject<any>) {
        this.innerSubject = val;
      }
      private innerSubject = new Subject();
    }

    const target = new Target();
    // spyOn(target.subject, 'complete');

    const descriptor = createMockDescriptor();
    expect(target.subject.isStopped).toEqual(false);
    Unsubscribable(Target, 'ngOnDestroy', descriptor);
    descriptor.value.call(target);
    expect(target.subject.isStopped).toEqual(true);
  });
});
