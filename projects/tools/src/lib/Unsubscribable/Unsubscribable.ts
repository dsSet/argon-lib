import each from 'lodash/each';
import { Subject, Subscription } from 'rxjs';
import { EventEmitter } from '@angular/core';

const cleanUpProperty = (property: any, key: string) => {
  if (property instanceof Subscription && !property.closed) {
    property.unsubscribe();
  }

  if (property instanceof EventEmitter) {
    return;
  }

  if (property instanceof Subject && !property.isStopped) {
    property.complete();
  }
}

export function Unsubscribable(target: any, propertyKey: string, descriptor: PropertyDescriptor) {

  if (!descriptor) {
    throw new Error('@Unsubscribable: look like your script target is less than ES5. decorator was not applied');
  }

  if (propertyKey !== 'ngOnDestroy') {
    throw new Error('@Unsubscribable should be applied to ngOnDestroy property');
  }

  const ngOnDestroyOriginal = descriptor.value;

  descriptor.value = function ngOnDestroy() {
    if (!this) {
      return ngOnDestroyOriginal.call(this);
    }

    each(this, cleanUpProperty);
    each(Object.getOwnPropertyDescriptors(target), (propDescriptor: PropertyDescriptor, key: string) => {
      cleanUpProperty(this[key], key);
    });

    return ngOnDestroyOriginal.call(this);
  };
}
