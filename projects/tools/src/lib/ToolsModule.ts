import { NgModule } from '@angular/core';
import { PropertyMapService } from './PropertyMap/PropertyMapService';
import { PROPERTY_MAP_INTERCEPTOR_TOKEN } from './PropertyMap/PropertyMapInterceptorToken';
import { PropertyMapDefaultInterceptor } from './PropertyMap/PropertyMapDefaultInterceptor';
import { KeyboardModule } from '@argon/keyboard';
import { ScrollGroupDirective } from './Scroll/ScrollGroupDirective';
import { UIScrollShape } from './Scroll/UIScrollShape';
import { UIVirtualScrollShape } from './Scroll/UIVirtualScrollShape';
import { UIShape } from './Shapes/UIShape';
import { UIChangeableShape } from './UIChangable/UIChangeableShape';

@NgModule({
  imports: [
    KeyboardModule
  ],
  declarations: [
    ScrollGroupDirective,
    UIScrollShape,
    UIVirtualScrollShape,
    UIShape,
    UIChangeableShape
  ],
  exports: [
    ScrollGroupDirective
  ],
  providers: [
    PropertyMapService,
    { provide: PROPERTY_MAP_INTERCEPTOR_TOKEN, useClass: PropertyMapDefaultInterceptor, multi: true }
  ]
})
export class ToolsModule {

}
