import { Constructor } from '../types/Constructor';
import { UIClickableInterface } from './UIClickableInterface';
import { EventEmitter, Injectable, Input, Output, Directive } from '@angular/core';

export function UIClickableMixin<T extends Constructor<any>>(
  BaseClass: T
): T & Constructor<UIClickableInterface> {

  @Directive()
@Injectable()
  class UIClickableExtension extends BaseClass implements UIClickableInterface {

    @Input() disabled: boolean;

    @Output() click = new EventEmitter<void>();

  }

  return UIClickableExtension;

}
