import { UIClickableMixin } from './UIClickableMixin';
import { EventEmitter } from '@angular/core';

describe('UIClickableMixin', () => {

  class BaseClass {

  }

  class ExtendedClass extends UIClickableMixin(BaseClass) {

  }


  it('should have default values', () => {
    const object = new ExtendedClass();
    expect(object.click instanceof EventEmitter).toBeTruthy();
    expect(object.disabled).toBeFalsy();
  });

});
