import { EventEmitter } from '@angular/core';

export interface UIClickableInterface {

  disabled: boolean;

  click: EventEmitter<void>;

}
