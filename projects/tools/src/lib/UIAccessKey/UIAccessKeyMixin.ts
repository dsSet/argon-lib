import { Constructor } from '../types/Constructor';
import { HostBinding, Injectable, Input, Directive } from '@angular/core';
import { UIAccessKeyInterface } from './UIAccessKeyInterface';
import { UIClickableInterface } from '../UIClickable/UIClickableInterface';

export function UIAccessKeyMixin<T extends Constructor<UIClickableInterface>>(BaseClass: T): T & Constructor<UIAccessKeyInterface> {

  @Directive()
@Injectable()
  class UIAccessKeyExtended extends BaseClass implements UIAccessKeyInterface {

    @Input() accesskey: string;

    public get attrAccessKey(): string {
      return this.disabled ? null : this.accesskey;
    }

  }

  return UIAccessKeyExtended;
}

