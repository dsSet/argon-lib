
export interface UIAccessKeyInterface {

  accesskey: string;

  readonly attrAccessKey: string;

}
