import { UIClickableInterface } from '../UIClickable/UIClickableInterface';
import { Component, DebugElement, EventEmitter, Input, Output, Directive } from '@angular/core';
import { UIAccessKeyMixin } from './UIAccessKeyMixin';
import { ComponentFixture, TestBed } from '@angular/core/testing';

describe('UIAccessKeyMixin', () => {

  @Directive()
class ClickableComponent implements UIClickableInterface {
    @Output() click = new EventEmitter<void>();
    @Input() disabled: boolean;
  }

  @Component({ selector: 'spec-fake-component', template: '', host: {
      '[attr.accesskey]': 'attrAccessKey'
    }
  })
  class FakeComponent extends UIAccessKeyMixin(ClickableComponent) { }

  let component: FakeComponent;
  let fixture: ComponentFixture<FakeComponent>;
  let element: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        FakeComponent
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FakeComponent);
    component = fixture.componentInstance;
    element = fixture.debugElement;
  });

  it('accesskey should bind attr accesskey', () => {
    component.accesskey = 'P';
    component.disabled = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('accesskey')).toEqual('P');
  });

  it('disabled component should not have accesskey', () => {
    component.accesskey = 'A';
    component.disabled = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('accesskey')).toEqual(null);
  });

});
