import { Directive, ElementRef, Injector, Input, NgZone, OnDestroy, OnInit, Optional } from '@angular/core';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import { ScrollGroupInterface } from './ScrollGroupInterface';
import { UIScrollShape } from './UIScrollShape';
import { ScrollGroupMixin } from './ScrollGroupMixin';

@Directive({
  selector: '[arScrollGroup]'
})
export class ScrollGroupDirective extends ScrollGroupMixin(UIScrollShape) implements OnInit, OnDestroy {

  @Input() arScrollGroup: Array<ScrollGroupInterface>;

  constructor(
    injector: Injector,
    elementRef: ElementRef<HTMLElement>,
    scrollDispatcher: ScrollDispatcher,
    ngZone: NgZone,
  ) {
    super(injector, elementRef, scrollDispatcher, ngZone);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
