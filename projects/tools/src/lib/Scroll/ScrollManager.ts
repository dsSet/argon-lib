import has from 'lodash/has';
import each from 'lodash/each';
import get from 'lodash/get';
import without from 'lodash/without';
import { Injectable, OnDestroy } from '@angular/core';
import { CdkScrollable, ScrollDispatcher } from '@angular/cdk/scrolling';
import { debounceTime, filter, map, tap } from 'rxjs/operators';
import { ScrollGroupInterface } from './ScrollGroupInterface';
import { Subscription } from 'rxjs';
import { Unsubscribable } from '../Unsubscribable/Unsubscribable';
import { UIScrollGroupInterface } from './UIScrollGroupInterface';

@Injectable()
export class ScrollManager implements OnDestroy {

  public readonly groups: { [name: string]: Array<UIScrollGroupInterface> } = { };

  public containers: Array<UIScrollGroupInterface> = [];

  public isBusy: boolean;

  private scrollSubscription: Subscription;

  constructor(
    public scrollDispatcher: ScrollDispatcher
  ) {
    this.scrollSubscription = this.scrollDispatcher.scrolled().pipe(
      map((item: CdkScrollable) => item as any),
      filter(this.isRegistered),
      tap(() => { this.isBusy = true; }),
      tap( this.syncContainers)
    ).subscribe(() => { this.isBusy = false; });
  }

  public register(container: UIScrollGroupInterface) {
    each(container.arScrollGroup, ({ name }: ScrollGroupInterface) => {
      if (!has(this.groups, name)) {
        this.groups[name] = [container];
      } else {
        this.groups[name].push(container);
      }
    });
    this.containers.push(container);
  }

  public unregister(container: UIScrollGroupInterface) {
    each(container.arScrollGroup, ({ name }: ScrollGroupInterface) => {
      if (has(this.groups, name)) {
        this.groups[name] = without(this.groups[name], container);
      }
    });
    this.containers = without(this.containers, container);
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  private isRegistered = (sourceContainer: UIScrollGroupInterface) => {
    return this.containers.indexOf(sourceContainer) >= 0 && !this.isBusy;
  }

  private syncContainers = (sourceContainer: UIScrollGroupInterface) => {
    each(sourceContainer.arScrollGroup, ({ name, axis }: ScrollGroupInterface) => {
      each(get(this.groups, name, []), (target: UIScrollGroupInterface) => {
        if (sourceContainer !== target) {
          this.sync(sourceContainer, target, axis);
        }
      });
    });
  }

  private sync(source: UIScrollGroupInterface, target: UIScrollGroupInterface, axis?: 'x' | 'y') {
    switch (axis) {
      case 'x':
        target.scrollTo({ left: source.measureScrollOffset('left' ) });
        return;
      case 'y':
        target.scrollTo({ top: source.measureScrollOffset('top' ) });
        return;
      default:
        target.scrollTo({ top: source.measureScrollOffset('top' ), left: source.measureScrollOffset('left' ) });
    }
  }
}
