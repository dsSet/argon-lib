
export interface ScrollGroupInterface {

  name: string;

  axis?: 'x' | 'y';

}
