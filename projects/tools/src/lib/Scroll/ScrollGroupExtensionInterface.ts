import { ScrollManager } from './ScrollManager';
import { OnDestroy, OnInit } from '@angular/core';

export interface ScrollGroupExtensionInterface extends OnInit, OnDestroy {

  scrollManager: ScrollManager;

  ngOnInit(): void;

  ngOnDestroy(): void;

}
