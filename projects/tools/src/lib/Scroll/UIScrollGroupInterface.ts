import { Injector } from '@angular/core';
import { ExtendedScrollToOptions } from '@angular/cdk/scrolling';
import { HasInjectorInterface } from '../Shapes/HasInjectorInterface';
import { ScrollGroupInterface } from './ScrollGroupInterface';
import { HasLifecircleHooksInterface } from '../Shapes/HasLifecircleHooksInterface';


export interface UIScrollGroupInterface extends HasInjectorInterface, HasLifecircleHooksInterface {

  injector: Injector;

  arScrollGroup: Array<ScrollGroupInterface>;

  scrollTo(options: ExtendedScrollToOptions): void;

  measureScrollOffset(from: 'top' | 'left' | 'right' | 'bottom' | 'start' | 'end'): number;

}
