import { ScrollGroupDirective } from './ScrollGroupDirective';
import { Component, ElementRef, Injector, NgZone } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ScrollDispatcher, ScrollingModule } from '@angular/cdk/scrolling';
import { ScrollManager } from './ScrollManager';


describe('ScrollGroupDirective', () => {

  @Component({
    selector: 'spec-component',
    template: ''
  })
  class MockComponent extends ScrollGroupDirective {

    constructor(
      injector: Injector,
      elementRef: ElementRef<HTMLElement>,
      scrollDispatcher: ScrollDispatcher,
      ngZone: NgZone
    ) {
      super(injector, elementRef, scrollDispatcher, ngZone);
    }
  }

  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ ScrollingModule ],
    declarations: [ MockComponent ],
    providers: [
      ScrollManager
    ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
  });

  it('ngOnInit should register component', () => {
    fixture.detectChanges();
    spyOn(component.scrollManager, 'register');
    component.ngOnInit();
    expect(component.scrollManager.register).toHaveBeenCalledWith(component);
  });

  it('ngOnDestroy should unregister component', () => {
    fixture.detectChanges();
    spyOn(component.scrollManager, 'unregister');
    component.ngOnDestroy();
    expect(component.scrollManager.unregister).toHaveBeenCalledWith(component);
  });
});
