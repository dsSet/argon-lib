import { UIScrollGroupInterface } from './UIScrollGroupInterface';
import {
  ChangeDetectorRef, Component,
  ElementRef,
  Inject,
  Injector,
  Input,
  NgZone,
  Optional
} from '@angular/core';
import { ScrollDispatcher } from '@angular/cdk/overlay';
import { ScrollGroupInterface } from './ScrollGroupInterface';
import { Directionality } from '@angular/cdk/bidi';
import {
  CdkVirtualScrollable,
  CdkVirtualScrollViewport,
  ViewportRuler,
  VIRTUAL_SCROLL_STRATEGY,
  VirtualScrollStrategy
} from '@angular/cdk/scrolling';

// tslint:disable-next-line:component-selector
@Component({ selector: '[arUIVirtualScrollShape]', template: `<ng-content></ng-content>` })
// tslint:disable-next-line:component-class-suffix
export class UIVirtualScrollShape extends CdkVirtualScrollViewport implements UIScrollGroupInterface {

  @Input() arScrollGroup: Array<ScrollGroupInterface>;

  constructor(
    public injector: Injector,
    elementRef: ElementRef<HTMLElement>,
    changeDetectorRef: ChangeDetectorRef,
    ngZone: NgZone,
    @Inject(VIRTUAL_SCROLL_STRATEGY) scrollStrategy: VirtualScrollStrategy,
    @Optional() dir: Directionality,
    scrollDispatcher: ScrollDispatcher,
    viewportRuler: ViewportRuler,
    scrollable: CdkVirtualScrollable
  ) {
    super(elementRef, changeDetectorRef, ngZone, scrollStrategy, dir, scrollDispatcher, viewportRuler, scrollable);
  }
}
