import { TestBed, waitForAsync } from '@angular/core/testing';
import { ScrollDispatcher, ScrollingModule } from '@angular/cdk/scrolling';
import { ScrollManager } from './ScrollManager';
import { Subject } from 'rxjs';
import { ScrollGroupInterface } from './ScrollGroupInterface';
import { UIScrollGroupInterface } from './UIScrollGroupInterface';
import Spy = jasmine.Spy;


describe('ScrollManager', () => {

  class ScrollDispatcherMock {
    scrolledSubject = new Subject<any>();
    scrolled = jasmine.createSpy('scrolled').and.returnValue(this.scrolledSubject);
  }

  const createMockContainer: (groups?: Array<ScrollGroupInterface>) => Partial<UIScrollGroupInterface>
    = (groups: Array<ScrollGroupInterface> = [{ name: 'mockGroup' }]) => ({
    arScrollGroup: groups,
    scrollTo: jasmine.createSpy('scrollTo'),
    measureScrollOffset: jasmine.createSpy('measureScrollOffset').and.returnValue(66)
  });

  let service: ScrollManager;
  let scrollDispatcher: ScrollDispatcherMock;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ ScrollingModule ],
    providers: [
      ScrollManager,
      { provide: ScrollDispatcher, useClass: ScrollDispatcherMock }
    ]
  })));

  beforeEach(() => {
    service = TestBed.inject(ScrollManager) as any;
    scrollDispatcher = TestBed.inject(ScrollDispatcher) as any;
  });

  afterEach(() => {
    scrollDispatcher.scrolledSubject.complete();
  });

  it('should listen scroll dispatcher', () => {
    expect(scrollDispatcher.scrolled).toHaveBeenCalledWith();
  });

  it('register should add container to containers list', () => {
    const container = createMockContainer() as UIScrollGroupInterface;
    expect(service.containers).toEqual([]);
    service.register(container);
    expect(service.containers).toEqual([container]);
  });

  it('register should create groups', () => {
    const container = createMockContainer([{ name: 'g1' }]) as UIScrollGroupInterface;
    expect(service.groups.g1).not.toBeDefined();
    service.register(container);
    expect(service.groups.g1).toEqual([container]);
  });

  it('register should register container in several groups', () => {
    const container = createMockContainer([{ name: 'g1' }, { name: 'g2' }]) as UIScrollGroupInterface;
    service.register(container);
    expect(service.groups.g1).toEqual([container]);
    expect(service.groups.g2).toEqual([container]);
  });

  it('register should push container to the defined group', () => {
    const container = createMockContainer([{ name: 'g1' }]) as UIScrollGroupInterface;
    const container2 = createMockContainer([{ name: 'g1' }]) as UIScrollGroupInterface;
    service.register(container);
    service.register(container2);
    expect(service.groups.g1).toEqual([container, container2]);
  });

  it('unregister should remove container from containers list', () => {
    const container = createMockContainer([{ name: 'g1' }]) as UIScrollGroupInterface;
    service.containers.push(container);
    service.unregister(container);
    expect(service.containers).toEqual([]);
  });

  it('unregister should remove container from groups', () => {
    const container = createMockContainer([{ name: 'g1' }, { name: 'g2' }]) as UIScrollGroupInterface;
    service.groups.g1 = [container];
    service.groups.g2 = [container];
    service.unregister(container);
    expect(service.groups.g1).toEqual([]);
    expect(service.groups.g2).toEqual([]);
  });

  it('scroll update dependencies by group', () => {
    const c1 = createMockContainer([{ name: 'g1' }, { name: 'g2' }]) as UIScrollGroupInterface;
    const c2 = createMockContainer([{ name: 'g1' }]) as UIScrollGroupInterface;
    const c3 = createMockContainer([{ name: 'g2' }]) as UIScrollGroupInterface;
    const c4 = createMockContainer([{ name: 'g3' }]) as UIScrollGroupInterface;
    service.register(c1);
    service.register(c2);
    service.register(c3);
    service.register(c4);

    (c1.measureScrollOffset as Spy<any>).and.returnValue(123);

    scrollDispatcher.scrolledSubject.next(c1);
    expect(c2.scrollTo).toHaveBeenCalledWith({ top: 123, left: 123 });
    expect(c3.scrollTo).toHaveBeenCalledWith({ top: 123, left: 123 });
    expect(c4.scrollTo).not.toHaveBeenCalled();
    expect(c1.scrollTo).not.toHaveBeenCalled();
  });

  it('should update scroll partially (x)', () => {
    const c1 = createMockContainer([{ name: 'g1', axis: 'x' }]) as UIScrollGroupInterface;
    const c2 = createMockContainer([{ name: 'g1' }]) as UIScrollGroupInterface;
    service.register(c1);
    service.register(c2);
    (c1.measureScrollOffset as Spy<any>).and.returnValue(45);
    scrollDispatcher.scrolledSubject.next(c1);
    expect(c2.scrollTo).toHaveBeenCalledWith({ left: 45 });
    expect(c1.scrollTo).not.toHaveBeenCalled();
  });

  it('should update scroll partially (y)', () => {
    const c1 = createMockContainer([{ name: 'g1', axis: 'y' }]) as UIScrollGroupInterface;
    const c2 = createMockContainer([{ name: 'g1' }]) as UIScrollGroupInterface;
    service.register(c1);
    service.register(c2);
    (c1.measureScrollOffset as Spy<any>).and.returnValue(87);
    scrollDispatcher.scrolledSubject.next(c1);
    expect(c2.scrollTo).toHaveBeenCalledWith({ top: 87 });
    expect(c1.scrollTo).not.toHaveBeenCalled();
  });
});
