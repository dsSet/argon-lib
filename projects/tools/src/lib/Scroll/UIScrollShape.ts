import { UIScrollGroupInterface } from './UIScrollGroupInterface';
import { Directive, ElementRef, Injector, Input, NgZone } from '@angular/core';
import { CdkScrollable, ScrollDispatcher } from '@angular/cdk/overlay';
import { ScrollGroupInterface } from './ScrollGroupInterface';

@Directive({ selector: '[arUIScrollShape]' })
// tslint:disable-next-line:directive-class-suffix
export class UIScrollShape extends CdkScrollable implements UIScrollGroupInterface {

  @Input() arScrollGroup: Array<ScrollGroupInterface>;

  constructor(
    public injector: Injector,
    elementRef: ElementRef<HTMLElement>,
    scrollDispatcher: ScrollDispatcher,
    ngZone: NgZone
  ) {
    super(elementRef, scrollDispatcher, ngZone);
  }

}
