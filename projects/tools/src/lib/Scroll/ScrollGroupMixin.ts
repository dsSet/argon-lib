import 'reflect-metadata';
import { Constructor } from '../types/Constructor';
import { ScrollGroupExtensionInterface } from './ScrollGroupExtensionInterface';
import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { ScrollManager } from './ScrollManager';
import { UIScrollGroupInterface } from './UIScrollGroupInterface';


export function ScrollGroupMixin<T extends Constructor<UIScrollGroupInterface>>(
  BaseClass: T
): T & Constructor<ScrollGroupExtensionInterface> {

  @Injectable()
  class ScrollGroupExtension extends BaseClass implements OnInit, OnDestroy, ScrollGroupExtensionInterface {

    public scrollManager: ScrollManager;

    ngOnInit(): void {
      if (super.ngOnInit) {
        super.ngOnInit();
      }
      this.scrollManager = this.injector.get(ScrollManager);
      this.scrollManager.register(this);
    }

    ngOnDestroy(): void {
      if (super.ngOnDestroy) {
        super.ngOnDestroy();
      }
      this.scrollManager.unregister(this);
    }

  }

  return ScrollGroupExtension;
}
