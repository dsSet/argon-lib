import { SimpleChanges } from '@angular/core';

export interface StateInterface<TModel> {

  state: TModel;

  changes: SimpleChanges;

}
