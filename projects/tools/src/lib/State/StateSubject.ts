import each from 'lodash/each';
import get from 'lodash/get';
import { BehaviorSubject } from 'rxjs';
import { StateInterface } from './StateInterface';
import { SimpleChange, SimpleChanges } from '@angular/core';

export class StateSubject<TModel> extends BehaviorSubject<StateInterface<TModel>> {

  public get state(): TModel {
    return this.value.state;
  }

  public get changes(): SimpleChanges {
    return this.value.changes;
  }

  constructor(initialState: TModel) {
    super({ state: initialState, changes: { } });
  }

  public setState(state: Partial<TModel>) {
    this.next({
      state: {
        ...this.state,
        ...state
      },
      changes: this.calculateChanges(state)
    });
  }

  private calculateChanges(state: Partial<TModel>): SimpleChanges {
    const changes = { };
    each(state, (currentValue: any, key: string) => {
      const prevValue = get(this.state, key);
      changes[key] = new SimpleChange(prevValue, currentValue, false);
    });

    return changes;
  }

}
