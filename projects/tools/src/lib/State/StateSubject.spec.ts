import { StateSubject } from './StateSubject';
import { SimpleChange } from '@angular/core';


describe('StateSubject', () => {

  class MockState {
    constructor(
      public property1: boolean,
      public property2: string,
      public property3: object
    ) { }
  }

  let stateSubject: StateSubject<MockState>;
  let defaultState: MockState;

  beforeEach(() => {
    defaultState = new MockState(true, 'DefaultMock', { data: 'mockData' });
    stateSubject = new StateSubject(defaultState);
  });

  it('should have default state by default', () => {
    expect(stateSubject.state).toEqual(defaultState);
  });

  it('should have no changes by default', () => {
    expect(stateSubject.changes).toEqual({ });
  });

  it('setState should update state', () => {
    const nextState: Partial<MockState> = { property1: false, property2: 'New Mock Data' };
    stateSubject.setState(nextState);
    expect(stateSubject.state).toEqual({ ...defaultState, ...nextState });
  });

  it('setState should calculate changes', () => {
    const nextState: Partial<MockState> = { property1: false, property2: 'New Mock Data' };
    stateSubject.setState(nextState);
    expect(stateSubject.changes.property1).toEqual(new SimpleChange(defaultState.property1, nextState.property1, false));
    expect(stateSubject.changes.property2).toEqual(new SimpleChange(defaultState.property2, nextState.property2, false));
  });

});
