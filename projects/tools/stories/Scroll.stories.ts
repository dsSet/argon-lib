import { moduleMetadata, storiesOf } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ScrollManager, ToolsModule } from './index';
import { ScrollingModule } from '@angular/cdk/scrolling';

const notes = {
  markdown: require('../docs/Scroll.md').default
};


@Component({
  selector: 'story-scroll-manager',
  template: '<ng-content></ng-content>',
  providers: [ ScrollManager ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class ScrollManagerComponent { }


@Component({
  selector: 'story-scroll-container',
  template: `
    <svg width="300%" height="300%" viewBox="0 0 200 200">
      <defs>
        <symbol id="s-rect">
          <rect width="200" height="200"/>
        </symbol>
        <linearGradient id="g-green"
                        x1="0%" y1="0%"
                        x2="90%" y2="90%">
          <stop offset="0%" stop-color="white" />
          <stop offset="90%" stop-color="black" />
        </linearGradient>
      </defs>
      <rect width="200" height="200" fill="url(#g-green)" />
    </svg>
  `,
  styles: [
    `:host {
      display: inline-block;
      width: 15rem;
      height: 15rem;
      overflow: auto;
      border: 1px solid #007C7C;
      margin: 1rem;
    }`
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class ScrollContainerComponent { }

storiesOf('Argon|tools/ScrollManager', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        ScrollingModule,
        ToolsModule,
        CommonModule
      ],
      declarations: [
        ScrollManagerComponent,
        ScrollContainerComponent
      ]
    })
  )
  .add('Manage several containers', () => ({
      props: {
        group: { name: 'group1' }
      },
      template: `
        <story-scroll-manager>
          <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
        </story-scroll-manager>
      `
    }), { notes }
  )
  .add('Sync partial', () => ({
      props: {
        groupX: { name: 'groupX', axis: 'x' },
        groupY: { name: 'groupY', axis: 'y' },
        group: { name: 'group' },
      },
      template: `
      <story-scroll-manager>
        <h3>X-scroll sync</h3>
        <story-scroll-container [arScrollGroup]="[groupX]"></story-scroll-container>
        <story-scroll-container [arScrollGroup]="[groupX]"></story-scroll-container>
      </story-scroll-manager>
      <story-scroll-manager>
        <h3>Y-scroll sync</h3>
        <story-scroll-container [arScrollGroup]="[groupY]"></story-scroll-container>
        <story-scroll-container [arScrollGroup]="[groupY]"></story-scroll-container>
      </story-scroll-manager>
    `
    }), { notes }
  )
  .add('Several groups with one container', () => ({
      props: {
        groupX: { name: 'groupX', axis: 'x' },
        groupY: { name: 'groupY', axis: 'y' },
      },
      template: `
        <story-scroll-manager>
          <story-scroll-container [arScrollGroup]="[groupX]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[groupY]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[groupX, groupY]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[groupX, groupY]"></story-scroll-container>
        </story-scroll-manager>
      `
    }), { notes }
  )
  .add('Put several managers', () => ({
      props: {
        group: { name: 'group1' }
      },
      template: `
        <story-scroll-manager>
          <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
        </story-scroll-manager>
        <story-scroll-manager>
          <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
        </story-scroll-manager>
      `
    }), { notes }
  )
  .add('Nested Managers case', () => ({
      props: {
        group: { name: 'group1' }
      },
      template: `
        <story-scroll-manager>
          <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
          <story-scroll-manager>
            <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
            <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
          </story-scroll-manager>
        </story-scroll-manager>
      `
    }), { notes }
  )
  .add('Chain dependency', () => ({
      props: {
        group1: { name: 'group1' },
        group2: { name: 'group2' },
        group3: { name: 'group3' }
      },
      template: `
        <story-scroll-manager>
          <story-scroll-container [arScrollGroup]="[group1]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[group1, group2]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[group2, group3]"></story-scroll-container>
          <story-scroll-container [arScrollGroup]="[group3]"></story-scroll-container>
        </story-scroll-manager>
      `
    }), { notes }
  );
