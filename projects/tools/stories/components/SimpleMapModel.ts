import { trim } from 'lodash';
import { PropertyMap } from '../index';
import { UntypedFormControl } from '@angular/forms';

class SimpleNestedModel {

  @PropertyMap(null, Boolean)
  public nestedBool;

  @PropertyMap(null, Math.ceil)
  public nestedFormControl = new UntypedFormControl(123.33);

}


export class SimpleMapModel {

  public fieldA = 5;

  @PropertyMap('fieldAPlusFieldB', (val: number, context: SimpleMapModel) => val + context.fieldA)
  public fieldB = 25;

  @PropertyMap()
  public get avgAB(): number {
    return (this.fieldA + this.fieldB) / 2;
  }

  @PropertyMap('originalKeyWasReplacedByThisString', trim as any)
  public stringProperty = '    string Property data   ';

  @PropertyMap(null, Math.round)
  public numberProperty = 12.123;

  @PropertyMap(null, Math.abs)
  public arrayProperty = [-1, 23, -4.3];

  @PropertyMap()
  public nestedObject = {
    a: 'test',
    b: -123
  };

  public skipProperty = 'I should not be mapped';

  @PropertyMap()
  public nestedMap = new SimpleNestedModel();

}
