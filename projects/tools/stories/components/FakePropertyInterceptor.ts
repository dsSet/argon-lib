import { isString } from 'lodash';
import { PropertyMapInterceptorInterface, PropertyMapMetadataInterface } from '../index';

/**
 * Custom interceptor for PropertyMap decorator.
 * PropertyMapService accept array of interceptors provided by PROPERTY_MAP_INTERCEPTOR_TOKEN
 * Interceptors will be order by "weight" property
 */
export class FakePropertyInterceptor implements PropertyMapInterceptorInterface {

  public weight = 9;

  mapValue(originalValue: any, value: any, metadata: PropertyMapMetadataInterface, context: object): any {
    if (isString(value)) {
      return `+++${value}+++`;
    }

    return value;
  }


}
