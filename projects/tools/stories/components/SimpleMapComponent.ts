import { Component } from '@angular/core';
import { SimpleMapModel } from './SimpleMapModel';
import { PropertyMapService } from '../index';

@Component({
  selector: 'story-simple-map',
  template: `
    <div>
      <div>Source:</div>
      <pre>{{ source | json }}</pre>
    </div>
    <div>
      <div>Target:</div>
      <pre>{{ service.mapProperties(source) | json }}</pre>
    </div>
  `,
  styles: [':host { display: flex; }']
})
export class SimpleMapComponent {

  source = new SimpleMapModel();

  constructor(
    public service: PropertyMapService
  ) { }

}
