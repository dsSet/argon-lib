import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FullScreenEventInterface, FullScreenService, Unsubscribable } from '../index';


@Component({
  selector: 'story-full-screen-component',
  template: `
    <h1>Could have full screen mode</h1>
    <button class="btn btn-sm btn-primary" (click)="toggleFullScreen()">
      {{ isOpened ? 'Close Full Screen' : 'Open Full Screen' }}
    </button>
  `,
  styles: [`
    :host {
      display: block;
      padding: 8rem 3rem 3rem 3rem;
      background-color: white;
      text-align: center;
    }
  `]
})
export class FullScreenExampleComponent implements OnInit, OnDestroy {

  public isOpened: boolean;

  private fullScreenEventSubscription: Subscription;

  constructor(
    public element: ElementRef<Element>,
    public fullScreenService: FullScreenService
  ) { }

  ngOnInit(): void {
    if (!this.fullScreenService.isFullScreenAvailable(this.element.nativeElement)) {
      alert('Fullscreen mode is not available!');
    } else {
      this.fullScreenEventSubscription = this.fullScreenService.createFullScreenListener(this.element.nativeElement)
        .subscribe(this.handleFullScreenChanged);
    }
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  public toggleFullScreen() {
    if (this.isOpened) {
      this.fullScreenService.exitFullScreen().subscribe();
    } else {
      this.fullScreenService.requestFullScreen(this.element.nativeElement).subscribe();
    }
  }

  private handleFullScreenChanged = (event: FullScreenEventInterface) => {
    this.isOpened = Boolean(event.element);
  }
}
