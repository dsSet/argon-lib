import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Component, EventEmitter, HostBinding, Injector, Input, Output, ViewEncapsulation } from '@angular/core';
import { UIAccessKeyMixin, UITabindexMixin, HasInjectorInterface, UIClickableInterface, ToolsModule } from './index';

@Component({
  selector: 'story-fake-clickable',
  template: '<ng-content></ng-content>',
  encapsulation: ViewEncapsulation.None,
  styles: ['.disabled { pointer-events: none; }']
})
class FakeClickableComponent implements HasInjectorInterface, UIClickableInterface {

  @HostBinding('class.disabled') @Input() disabled: boolean;

  @Output() click = new EventEmitter<void>();

  @HostBinding('class.btn') btn = true;

  @HostBinding('class.btn-primary') primary = true;

  constructor(
    public injector: Injector
  ) {
    console.log(this.injector);
  }
}

@Component({
  selector: 'story-fake-access-key',
  template: '<ng-content></ng-content>',
  encapsulation: ViewEncapsulation.None,
  styles: ['.disabled { pointer-events: none; }']
})
class FakeAccessKeyComponent extends UIAccessKeyMixin(FakeClickableComponent) {
  accesskey = 'Q';
}

@Component({
  selector: 'story-fake-tab-index',
  template: '<ng-content></ng-content>',
  encapsulation: ViewEncapsulation.None,
  styles: ['.disabled { pointer-events: none; }']
})
class FakeTabIndexComponent extends UITabindexMixin(FakeClickableComponent) { }

@Component({
  selector: 'story-fake-mix',
  template: '<ng-content></ng-content>',
  encapsulation: ViewEncapsulation.None,
  styles: ['.disabled { pointer-events: none; }']
})
class FakeMixComponent extends UITabindexMixin(UIAccessKeyMixin(FakeClickableComponent)) { }


storiesOf('Argon|tools/Mixins', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        ToolsModule
      ],
      declarations: [
        FakeClickableComponent,
        FakeAccessKeyComponent,
        FakeTabIndexComponent,
        FakeMixComponent
      ]
    })
  ).add('UIAccessKeyMixin', () => ({
    props: {
      handleClick() { alert('clicked'); }
    },
    template: `
      <story-fake-access-key (click)="handleClick()">Click me (Alt + Q)</story-fake-access-key>
      <story-fake-access-key accesskey="W" (click)="handleClick()">Click me (Alt + W)</story-fake-access-key>
      <story-fake-access-key accesskey="A" [disabled]="true" (click)="handleClick()">Click me (Alt + A)</story-fake-access-key>
    `
  })
).add('UITabindexMixin', () => ({
    props: {
      handleClick() { alert('clicked'); }
    },
    template: `
      <story-fake-tab-index (click)="handleClick()">Click me</story-fake-tab-index>
      <story-fake-tab-index (click)="handleClick()">Click me</story-fake-tab-index>
      <story-fake-tab-index [disabled]="true" (click)="handleClick()">Click me</story-fake-tab-index>
    `
  })
).add('Apply several mixins', () => ({
    props: {
      handleClick() { alert('clicked'); }
    },
    template: `
      <story-fake-mix accesskey="Q" (click)="handleClick()">Click me (Alt + Q)</story-fake-mix>
      <story-fake-mix accesskey="W" (click)="handleClick()">Click me (Alt + W)</story-fake-mix>
      <story-fake-mix accesskey="A" [disabled]="true" (click)="handleClick()">Click me (Alt + A)</story-fake-mix>
    `
  })
);
