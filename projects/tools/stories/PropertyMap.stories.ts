import { moduleMetadata, storiesOf } from '@storybook/angular';
import { SimpleMapComponent } from './components/SimpleMapComponent';
import { CommonModule } from '@angular/common';
import { ToolsModule, PROPERTY_MAP_INTERCEPTOR_TOKEN } from './index';
import { FakePropertyInterceptor } from './components/FakePropertyInterceptor';


storiesOf('Argon|tools/@PropertyMap', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        ToolsModule
      ],
      declarations: [
        SimpleMapComponent
      ],
      providers: [
        { provide: PROPERTY_MAP_INTERCEPTOR_TOKEN, useClass: FakePropertyInterceptor, multi: true }
      ]
    })
  ).add('Simple map', () => ({
    component: SimpleMapComponent,
    moduleMetadata: { }
  })
);
