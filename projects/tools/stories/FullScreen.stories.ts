import { moduleMetadata, storiesOf } from '@storybook/angular';
import { FullScreenExampleComponent } from './components/FullScreenExampleComponent';

const notes = {
  markdown: require('../docs/FullScreen.md').default
};

storiesOf('Argon|tools/FullScreen', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        FullScreenExampleComponent
      ]
    })
  ).add('Full Screen example', () => ({
    component: FullScreenExampleComponent,
    moduleMetadata: { }
  }),
  { notes }
);
