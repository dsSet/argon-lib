import { storiesOf, moduleMetadata } from '@storybook/angular';
import { Component, EventEmitter, Injectable, OnDestroy, OnInit } from '@angular/core';
import { Unsubscribable } from './index';
import { BehaviorSubject, Subject, Subscription, timer } from 'rxjs';


@Injectable()
class Service implements OnDestroy {

  public subscription1: Subscription;
  public observer1 = new EventEmitter<any>();
  public observer2 = new BehaviorSubject<any>(false);
  public observer3 = new Subject<any>();
  public observer4 = timer(0, 1000);

  protected subscription3: Subscription;
  private subscription2: Subscription;


  constructor() {
    this.subscription1 = this.observer4.subscribe((t: number) => { console.log('service subscription1: ', t); });
    this.subscription2 = this.observer4.subscribe((t: number) => { console.log('service subscription2: ', t); });
    this.subscription3 = this.observer4.subscribe((t: number) => { console.log('service subscription3: ', t); });
  }

  @Unsubscribable
  ngOnDestroy(): void {
    console.log('Destroy service');
  }
}

@Component({
  selector: 'story-root-component',
  template: '<story-child-component></story-child-component>',
  providers: [
    Service
  ]
})
class FakeComponent {

  constructor(
    private service: Service
  ) { }

}


@Component({
  selector: 'story-extended-component',
  template: ''
})
class ExtendedComponent implements OnInit, OnDestroy {

  public inheritedSubscription1: Subscription;
  public inheritedObserver4 = timer(0, 1000);

  protected inheritedSubscription2: Subscription;

  private inheritedSubscription3: Subscription;

  ngOnInit(): void {
    this.inheritedSubscription1 = this.inheritedObserver4.subscribe((t: number) => { console.log('inheritedSubscription1: ', t); });
    this.inheritedSubscription2 = this.inheritedObserver4.subscribe((t: number) => { console.log('inheritedSubscription2: ', t); });
    this.inheritedSubscription3 = this.inheritedObserver4.subscribe((t: number) => { console.log('inheritedSubscription3: ', t); });
  }

  @Unsubscribable
  ngOnDestroy(): void {
    console.log('Parent component destroyed');
  }

}

@Component({
  selector: 'story-child-component',
  template: 'See console output for details'
})
class ChildComponent extends ExtendedComponent implements OnInit, OnDestroy {

  public subscription1: Subscription;
  public observer1 = new EventEmitter<any>();
  public observer2 = new BehaviorSubject<any>(false);
  public observer3 = new Subject<any>();
  public observer4 = timer(0, 1000);

  protected subscription3: Subscription;
  private subscription2: Subscription;


  ngOnInit(): void {
    super.ngOnInit();
    this.subscription1 = this.observer4.subscribe((t: number) => { console.log('subscription1: ', t); });
    this.subscription2 = this.observer4.subscribe((t: number) => { console.log('subscription2: ', t); });
    this.subscription3 = this.observer4.subscribe((t: number) => { console.log('subscription3: ', t); });
  }


  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
    console.log('component destroyed');
  }
}

storiesOf('Argon|tools/@Unsubscribable', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        FakeComponent,
        ChildComponent
      ]
    })
  ).add('@Unsubscribable test', () => ({
    component: FakeComponent,
    moduleMetadata: { }
  })
);
