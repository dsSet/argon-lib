import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Pending, PendingInterface, PendingSubject, Unsubscribable } from './index';
import { Subscription, timer } from 'rxjs';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'story-spinable-component',
  template: `
    <div>
      <div *ngFor="let item of log">{{ item }}</div>
    </div>
  `
})
class StorySpinableComponent implements OnInit, OnDestroy, PendingInterface {

  pending = new PendingSubject(false);

  @Pending
  public prop1: Subscription;

  @Pending
  public prop2: Subscription;

  public log = [];

  ngOnInit(): void {

    this.prop1 = timer(3000).subscribe(() => { console.warn('Finished1'); });

    this.prop2 = timer(2000).subscribe(() => { console.warn('Finished3'); });

    this.pending.subscribe((state: boolean) => {
      console.log('Pending', state);
      this.log.push(`Pending state change: ${state}`);
    });

  }

  @Unsubscribable
  ngOnDestroy(): void { }

}

storiesOf('Argon|tools/@Pending', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule
      ],
      declarations: [
        StorySpinableComponent
      ]
    })
  ).add('@Pending test', () => ({
    component: StorySpinableComponent,
    moduleMetadata: { }
  })
);
