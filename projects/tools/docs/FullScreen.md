# FullScreenService

Allow to open `Element` in full screen mode 

## Properties

|Property | Description
|---      |---
|`documentOriginal: Document`| link to the Document
|`fullScreenTarget: Element`| link to the element which is opening with at the full screen


## Methods

|Property | Description
|---      |---
|`createFullScreenListener(element?: Element): Observable<FullScreenEventInterface>`| create stream for handling fullscreen toggling events. `element` could be used as filter.  
|`isFullScreenAvailable(element: Element): boolean`| check full screen functionality for the `element`
|`isCloseFullScreenAvailable(): boolean`| check document close full screen API
|`requestFullScreen(element: Element): Observable<boolean>`| open element in fullscreen mode
|`exitFullScreen(): Observable<boolean>`| close fullscreen if available

## Interfaces

````typescript
export interface FullScreenEventInterface {

  event: Event;

  element: Element | null;

}
````

## Example

````typescript

import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FullScreenEventInterface, FullScreenService, Unsubscribable } from '..';


@Component({
  selector: 'story-full-screen-component',
  template: `
    <h1>Could have full screen mode</h1>
    <button class="btn btn-sm btn-primary" (click)="toggleFullScreen()">
      {{ isOpened ? 'Close Full Screen' : 'Open Full Screen' }}
    </button>
  `,
  styles: [`
    :host {
      display: block;
      padding: 8rem 3rem 3rem 3rem;
      background-color: white;
      text-align: center;
    }
  `]
})
export class FullScreenExampleComponent implements OnInit, OnDestroy {

  public isOpened: boolean;

  private fullScreenEventSubscription: Subscription;

  constructor(
    public element: ElementRef<Element>,
    public fullScreenService: FullScreenService
  ) { }

  ngOnInit(): void {
    if (!this.fullScreenService.isFullScreenAvailable(this.element.nativeElement)) {
      alert('Fullscreen mode is not available!');
    } else {
      this.fullScreenEventSubscription = this.fullScreenService.createFullScreenListener(this.element.nativeElement)
        .subscribe(this.handleFullScreenChanged);
    }
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  public toggleFullScreen() {
    if (this.isOpened) {
      this.fullScreenService.exitFullScreen().subscribe();
    } else {
      this.fullScreenService.requestFullScreen(this.element.nativeElement).subscribe();
    }
  }

  private handleFullScreenChanged = (event: FullScreenEventInterface) => {
    this.isOpened = Boolean(event.element);
  }
}


````

