#@Pending decorator

Allow manage pending state

##Usage
````typescript
import { Pending, PendingInterface, PendingSubject } from '../dist'
import { Subscription } from 'rxjs';

export class PendingExample implements PendingInterface {
  
  public pending = new PendingSubject(false);
  
  @Pending
  private subscription1: Subscription;
  
  @Pending
  private subscription2: Subscription;
  
}

````

``@Pending`` decorator will add watchers on subscription.
Each time on set new value to ``Subscription`` property ``PendingSubject`` will receive ``true`` value to emit.
Each time on ``Subscription`` complete ``PendingSubject`` will receive ``false`` value to emit.
``PendingSubject`` is ``BehaviourSubject`` extension. It emit only first ``true`` and last ``false`` values on call queue.

````typescript
import { PendingSubject } from '../dist'

export interface PendingInterface {

  pending: PendingSubject;

}
````

##Explanation:
Code:
````typescript
import { Pending, PendingInterface, PendingSubject } from '../dist'
import { Subscription, timer } from 'rxjs';

export class PendingExample implements PendingInterface {
  
  public pending = new PendingSubject(false);
  
  @Pending
  private subscription: Subscription;
  
  @Pending
  private subscription2: Subscription;
    
  constructor() {
    this.subscription = timer(3000).subscribe(() => { console.log('Done' ); });
    this.subscription2 = timer(1000).subscribe(() => { console.log('Done' ); });
  }
  
}
````

Will be equal to: 
````typescript
import { BehaviourSubject, Subscription, timer, finalize } from 'rxjs';

export class PendingExample {
  
  public pending = new BehaviourSubject(false);
  
  private subscription: Subscription;
  
  private subscription2: Subscription;
    
  constructor() {
    this.pending.next(true);
    
    this.subscription = timer(3000)
      .pipe(finalize(() => { if (this.pending.value) this.pending.next(false); }))
      .subscribe(() => { console.log('Done' ); });
    
    this.subscription2 = timer(1000)
      .pipe(finalize(() => { if (this.pending.value) this.pending.next(false); }))
      .subscribe(() => { console.log('Done' ); });
  }
  
}
````

##Full example:

````typescript
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { CommonModule } from '@angular/common';
import { Pending, PendingInterface, PendingSubject, Unsubscribable } from '../dist';

@Component({
  selector: 'story-spinable-component',
  template: `
    <div>
      <div *ngFor="let item of log">{{ item }}</div>
    </div>
  `
})
class StorySpinableComponent implements OnInit, OnDestroy, PendingInterface {

  pending = new PendingSubject(false);

  @Pending
  public prop1: Subscription;

  @Pending
  public prop2: Subscription;

  public log = [];

  ngOnInit(): void {

    this.prop1 = timer(3000).subscribe(() => { console.warn('Finished1'); });

    this.prop2 = timer(2000).subscribe(() => { console.warn('Finished3'); });

    this.pending.subscribe((state: boolean) => {
      console.log('Pending', state);
      this.log.push(`Pending state change: ${state}`);
    });

  }

  @Unsubscribable
  ngOnDestroy(): void { }

}
````


| execution second | log content 
| ---------------- | ----------- 
| before ngOnInit call | ``[]``
| 0s | ``['Pending state change: true']``
| 1s | ``['Pending state change: true']``
| 2s | ``['Pending state change: true']``
| 3s | ``['Pending state change: true','Pending state change: false']``
 
