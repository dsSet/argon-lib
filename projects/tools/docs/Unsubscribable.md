#@Unsubscribable decorator

Decorator can be use for automatically call .unsubscribe() and .complete() methods for **Subscription** and **Observable** class properties.
This decorator must be applied to **ngOnDestroy** method.

> Note: Decorator will skip any **EventEmitter** property

Usage:

````typescript
import { Component, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { timer } from 'rxjs/operators';
import { Unsubscribable } from '../dist';

@Component({
  selector: 'fake-component',
  template: ''
})
class FakeComponent implements OnDestroy {
  
  public subject = new Subject();
  
  public subscription: Subscription;
  
  constructor() {
    this.subscription = timer(0, 100).subscribe(() => { console.log(1); })
  }
  
  @Unsubscribable
  ngOnDestroy() {
    // some code there
  }
  
}
````
This component will call ``this.subscription.unsubscribe()`` and ``this.subject.complete()`` on component **ngOnDestroy** hook call.     

> Note: decorator can be applied to services too. 
