# Scroll Pattern

Allow to link several containers for common scroll behaviour

> Scroll manager works with absolute scroll in `px`. You need to take care of containers and content size by yourself.

## `ScrollManager`

The first step to linking containers provide mutual `ScrollManager` with DI.

> `ToolsModule` does not provide scroll manager on global level to avoid unexpected links between different container.
> You should provide this service by yourself.

Parent scroll container example:

````typescript
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ScrollManager } from '@argon/tools';

@Component({
  selector: 'story-scroll-manager',
  template: '<ng-content></ng-content>',
  providers: [ ScrollManager ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class ScrollManagerComponent { }

````

## `[arScrollGroup]`

Register container at the `ScrollManager`. Each container can be linked to several scroll groups.

|Property |Default value|Description
|---      |---          |---
|`@Input() arScrollGroup: Array<ScrollGroupInterface>`| `undefined` | List of the scroll groups.

````typescript

export interface ScrollGroupInterface {
  
  // The uniq (inside current manager) scroll group
  name: string;

  // Axis to manage. Will manage both x and y axis if not set
  axis?: 'x' | 'y';

}

````

## Examples

`<story-scroll-container>` could be any html element with scrollbars. All examples are using `<story-scroll-manager>` declared above.

### Two containers full sync

````typescript
const group = { name: 'group1' };
````

````html
<story-scroll-manager>
  <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
  <story-scroll-container [arScrollGroup]="[group]"></story-scroll-container>
</story-scroll-manager>
````

### Sync only one axis

````typescript
const groupX = { name: 'groupX', axis: 'x' };
````

````html
<story-scroll-manager>
  <h3>X-scroll sync</h3>
  <story-scroll-container [arScrollGroup]="[groupX]"></story-scroll-container>
  <story-scroll-container [arScrollGroup]="[groupX]"></story-scroll-container>
</story-scroll-manager>
````

### Sync several groups

````typescript
const groupX = { name: 'groupX', axis: 'x' };
const groupY = { name: 'groupY', axis: 'y' };
````

````html
<story-scroll-manager>
  <story-scroll-container [arScrollGroup]="[groupX]"></story-scroll-container>
  <story-scroll-container [arScrollGroup]="[groupY]"></story-scroll-container>
  <story-scroll-container [arScrollGroup]="[groupX, groupY]"></story-scroll-container>
  <story-scroll-container [arScrollGroup]="[groupX, groupY]"></story-scroll-container>
</story-scroll-manager>
````


### Chaining containers

````typescript
const group1 = { name: 'group1' };
const group2 = { name: 'group2' };
const group3 = { name: 'group3' };
````

````html
<story-scroll-manager>
  <story-scroll-container [arScrollGroup]="[group1]"></story-scroll-container>
  <story-scroll-container [arScrollGroup]="[group1, group2]"></story-scroll-container>
  <story-scroll-container [arScrollGroup]="[group2, group3]"></story-scroll-container>
  <story-scroll-container [arScrollGroup]="[group3]"></story-scroll-container>
</story-scroll-manager>
````

> This is extraordinary case just for demonstrate how scroll events managing by the `ScrollManager`.
> Try to avoid this situation for performance reasons.


