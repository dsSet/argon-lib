#@PropertyMap() decorator

This decorator allow to save metadata for object serialization.

##Usage:
````typescript
import { PropertyMap } from '../dist';

export class FakeModel {
  
  @PropertyMap('newKey', Math.round, { customOption: 123, anotherCustomOption: 'test' })
  public someProperty = 1.5;
  
  @PropertyMap(null, Boolean)
  public anotherProperty: boolean;
  
  public propertyWithoutDecorator = 'test';
  
}

````

Arguments:

> Signature: ``@PropertyMap(key?: string, transformFn?: (value: any) => any, options?: { [key:string]: any })``

- ``key`` - property key in serialized object, if not defined or null original property name will be used
- ``transformFn`` - function will be applied to property value on serialization
- ``options`` - custom options set. Can be used for manage serialization process inside custom interceptor.  

##Serialization process:

Model from previous section can be serialized with **PropertyMapService**.

````typescript
import { Injectable } from '@angular/core';
import { PropertyMapService } from '../dist';
import { FakeModel } from './';

@Injectable()
class SomeService {
  
  private model = new FakeModel();
  
  constructor(
    private serializer: PropertyMapService
  ) { }
  
  public serializeModel(): object {
    return this.serializer.mapProperties(this.model);
  }
  
}
````

Method ``public serializeModel(): object`` will return:

````json
{
  "newKey": 2,
  "anotherProperty": false
}
````

##Extend default serialization process:
Serialization process can be extended with **interceptors**.

Interceptor should implement **PropertyMapInterceptorInterface** interface:

````typescript
import { PropertyMapMetadataInterface } from '../dist';

export interface PropertyMapInterceptorInterface {

  weight: number;

  mapValue(originalValue: any, value: any, metadata: PropertyMapMetadataInterface, context: object): any;

}
````
- ``mapValue`` - method can transform property value
  - ``originalValue`` - original value inside source object.
  - ``value`` - current value (originalValue parsed with another interceptors)
  - ``metadata`` - property metadata
  - ``context`` - object instance
- ``weight`` - property for sorting transformation queue (package contains default interceptor with weight === 10)

> Note: Default interceptor will map angular ``AbstractControl`` to value 

Interceptor example:
````typescript
import { isString } from 'lodash';
import { PropertyMapInterceptorInterface, PropertyMapMetadataInterface } from '../dist';


export class FakePropertyInterceptor implements PropertyMapInterceptorInterface {

  public weight = 9;

  mapValue(originalValue: any, value: any, metadata: PropertyMapMetadataInterface, context: object): any {
    if (isString(value)) {
      return `+++${value}+++`;
    }

    return value;
  }

}
````

Provide interceptor:
````typescript
import { NgModule } from '@angular/core';
import { PROPERTY_MAP_INTERCEPTOR_TOKEN } from '../dist';
import { FakePropertyInterceptor } from './';

@NgModule({
  providers: [
    { provide: PROPERTY_MAP_INTERCEPTOR_TOKEN, useClass: FakePropertyInterceptor, multi: true }
  ]
})
class FakeModule { }

````

> Note: ``PROPERTY_MAP_INTERCEPTOR_TOKEN`` must be provided with ``multi: true``

##PropertyMapMetadataInterface

````typescript
export interface PropertyMapMetadataInterface {

  originalKey: string;

  targetKey: string;

  options?: { [key: string]: any };

  transformFn?: (value: any, context: object) => any;

}
````
