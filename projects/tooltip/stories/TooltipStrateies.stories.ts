import { moduleMetadata, storiesOf } from '@storybook/angular';
import { TooltipElementPositionStrategy, TooltipModule, TooltipPositionStrategy } from './index';
import { PortalModule } from '@angular/cdk/portal';
import { WithPositionComponent } from './components/WithPositionComponent';
import { ExtendedTooltipElementPositionStrategy } from './components/ExtendedTooltipElementPositionStrategy';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/Positions.md').default + require('../docs/index.md').default
};


storiesOf('Argon|tooltip/Position strategies', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        PortalModule,
        TooltipModule
      ]
    })
  )
  .add('TooltipElementPositionStrategy', () => ({
    template: `
      <div arTooltip>
        This block should have tooltip
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `,
    moduleMetadata: {
      providers: [
        { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy }
      ]
    }
  }), { notes })
  .add('Use Built in strategies at the template level', () => ({
    template: `
      <ar-point-tooltip class="example-content">
        This block should have tooltip with point strategy
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </ar-point-tooltip>

      <ar-element-tooltip class="example-content">
        This block should have tooltip with element strategy
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </ar-element-tooltip>
    `
  }), { notes })
  .add('Provide position strategy on component level', () => ({
    template: `
      <story-with-position-component>
        <div class="example-content" arTooltip>
          This block should have tooltip
          <ng-template>
            <small>I am tooltip!</small>
          </ng-template>
        </div>
      </story-with-position-component>
    `,
    moduleMetadata: {
      declarations: [
        WithPositionComponent
      ]
    }
  }), { notes })
  .add('Override debounce time for the TooltipElementPositionStrategy', () => ({
    template: `
      <div class="example-content" arTooltip>
        This block should have tooltip (show - 0.5s, hide - 1s)
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `,
    moduleMetadata: {
      providers: [
        { provide: TooltipPositionStrategy, useClass: ExtendedTooltipElementPositionStrategy }
      ]
    }
  }), { notes });
