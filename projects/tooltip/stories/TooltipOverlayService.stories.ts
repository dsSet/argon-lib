import { moduleMetadata, storiesOf } from '@storybook/angular';
import { TooltipElementPositionStrategy, TooltipModule, TooltipPositionStrategy } from './index';
import { PortalModule } from '@angular/cdk/portal';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DraggableComponent } from './components/DraggableComponent';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/Service.md').default + require('../docs/index.md').default
};

storiesOf('Argon|tooltip/Tooltip Overlay Service', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        PortalModule,
        TooltipModule,
        DragDropModule,
      ],
      declarations: [
        DraggableComponent,
      ]
    })
  )
  .add('Update tooltip position on element drag', () => ({
    component: DraggableComponent,
    moduleMetadata: { },
  }), { notes })
  .add('Update tooltip position on element drag (Element position strategy)', () => ({
    component: DraggableComponent,
    moduleMetadata: {
      providers: [
        { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy }
      ]
    }
  }), { notes });
