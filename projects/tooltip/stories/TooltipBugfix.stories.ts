import { CommonModule } from '@angular/common';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { TooltipElementPositionStrategy, TooltipModule, TooltipPointPositionStrategy, TooltipPositionStrategy } from './index';
import { Component, ViewChild } from '@angular/core';
import { CdkPortal } from '@angular/cdk/portal';

@Component({
  selector: 'story-tooltip-with-portal',
  template: `
    <span [arTooltip]>
      This block should have tooltip
      <div *cdkPortal>
        <small>I am tooltip!</small>
      </div>
    </span>
    <span [arTooltip]>
      This ism another tooltip
      <ng-template>
        <h4>Huge content!</h4>
      </ng-template>
    </span>
  `
})
class TooltipWithPortalComponent {
  @ViewChild(CdkPortal, { static: true }) tooltip: CdkPortal;
}


storiesOf('Argon|tooltip/Bugfix', module)
  .addDecorator(moduleMetadata({
    imports: [
      TooltipModule,
      CommonModule,
      ReactiveFormsModule
    ],
    declarations: [
      TooltipWithPortalComponent,
    ]
  }))
  .add('Point position strategy', () => ({
    component: TooltipWithPortalComponent,
    moduleMetadata: {
      providers: [
        { provide: TooltipPositionStrategy, useClass: TooltipPointPositionStrategy }
      ]
    }
  }))
  .add('Element position strategy', () => ({
    component: TooltipWithPortalComponent,
    moduleMetadata: {
      providers: [
        { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy }
      ]
    }
  }))
  .add('Dynamic tooltip', () => ({
    props: {
      control: new UntypedFormControl()
    },
    template: `
        <div [arTooltip] [disabled]="!control.value" >
            <input [formControl]="control" />
            This block should have tooltip
            <ng-template>
              <small>I am tooltip with data: {{ control.value }}</small>
            </ng-template>
        </div>
      `,
    moduleMetadata: {
      providers: [
        { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy }
      ]
    }
  }))
  .add('Update tooltip on click content', () => ({
    props: {
      counter: 0,
      content: null,
      handleClick() {
        this.counter += 1;
        this.content = `Counter value ${this.counter}`;
      },
      handleRemove() { this.content = null; }
    },
    template: `
        <div [arTooltip] [disabled]="!content" type="argon-tooltip">
           <div (click)="handleClick()">Click me</div>
           <div (click)="handleRemove()">Click to remove</div>
           <ng-template>{{ content }}</ng-template>
        </div>
      `
  }));
