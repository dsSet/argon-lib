import { moduleMetadata, storiesOf } from '@storybook/angular';
import { TooltipModule, TooltipService } from './index';
import { CdkPortal, PortalModule } from '@angular/cdk/portal';
import { Component, ViewChild } from '@angular/core';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/Base.md').default + require('../docs/index.md').default
};


@Component({
  selector: 'story-tooltip-with-portal',
  template: `
    <div *cdkPortal>
      <small>I am tooltip!</small>
    </div>
    <div [arTooltip]="tooltip">This block should have tooltip</div>
  `
})
class TooltipWithPortalComponent {
  @ViewChild(CdkPortal, { static: true }) tooltip: CdkPortal;
}

@Component({
  selector: 'story-tooltip-from-component',
  template: `<small>I am tooltip!</small>`
})
class TooltipFromComponent { }

storiesOf('Argon|tooltip/Base usage', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        PortalModule,
        TooltipModule
      ],
      declarations: [
        TooltipWithPortalComponent,
        TooltipFromComponent
      ],
      entryComponents: [
        TooltipFromComponent
      ]
    })
  )
  .add('Create tooltip from template', () => ({
    template: `
      <div arTooltip>
        This block should have tooltip
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `
  }), { notes })
  .add('Create tooltip from cdkPortal', () => ({
    template: `
      <div arTooltip>
        This block should have tooltip
        <small *cdkPortal>I am tooltip!</small>
      </div>
    `
  }), { notes })
  .add('Use the same tooltip with different UI', () => ({
    template: `
      <ng-template #tooltip>
        <small>I am tooltip!</small>
      </ng-template>

      <div class="btn btn-sm btn-success" [arTooltip]="tooltip">
        Button 1
      </div>

      <div class="btn btn-sm btn-error" [arTooltip]="tooltip">
        Button 2
      </div>
    `
  }), { notes })
  .add('Resolve tooltip when several templates provided as content', () => ({
    template: `
      <div [arTooltip]="tooltip">
        This block should have tooltip
        <ng-template>Another template. this is not tooltip</ng-template>
        <ng-template #tooltip>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `
  }), { notes })
  .add('Create tooltip from portal', () => ({
    component: TooltipWithPortalComponent,
    moduleMetadata: { }
  }), { notes })
  .add('Create tooltip from component', () => ({
    props: {
      tooltip: TooltipFromComponent
    },
    template: `
      <div [arTooltip]="tooltip">This block should have tooltip</div>
    `
  }), { notes })
  .add('Disable tooltip', () => ({
    template: `
      <div arTooltip [disabled]="true">
        This block should have tooltip
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `
  }), { notes })
  .add('Use custom tooltip positions', () => ({
    props: {
      positions: TooltipService.createTooltipPositions(25, 'left', 'right')
    },
    template: `
      <div arTooltip [positions]="positions">
        This block should have tooltip
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `
  }), { notes });
