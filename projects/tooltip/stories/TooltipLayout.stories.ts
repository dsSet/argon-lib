import { moduleMetadata, storiesOf } from '@storybook/angular';
import {
  ARGON_TOOLTIP_LAYOUT,
  ARGON_TOOLTIP_DEFAULT_LAYOUT,
  TooltipLayoutConfigModel,
  TooltipModule,
  ARGON_TOOLTIP_POSITION_PROVIDER
} from './index';
import { PortalModule } from '@angular/cdk/portal';
import { CustomTooltipLayoutComponent } from './components/CustomTooltipLayoutComponent';

import '!!style-loader!css-loader!sass-loader!./style.scss';

const notes = {
  markdown: require('../docs/Layout.md').default + require('../docs/index.md').default
};

storiesOf('Argon|tooltip/Tooltip layout', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        PortalModule,
        TooltipModule
      ]
    })
  )
  .add('Select Layout', () => ({
    template: `
      <div arTooltip type="argon-tooltip">
        This block should have tooltip
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `,
  }), { notes })
  .add('Provide layout as default', () => ({
    template: `
      <div arTooltip type="argon-tooltip">
        This block should have tooltip
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `,
    moduleMetadata: {
      providers: [
        { provide: ARGON_TOOLTIP_DEFAULT_LAYOUT, useValue: 'argon-tooltip' }
      ]
    }
  }), { notes })
  .add('Tooltip layout size', () => ({
    props: {
      loremIpsum: `
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      `
    },
    template: `
      <div>
        <ng-template #tooltip><small>{{loremIpsum}}</small></ng-template>
        <div [arTooltip]="tooltip" arTooltipSize="xxs" type="argon-tooltip">xxs tooltip size</div>
        <div [arTooltip]="tooltip" arTooltipSize="xs" type="argon-tooltip">xs tooltip size</div>
        <div [arTooltip]="tooltip" arTooltipSize="sm" type="argon-tooltip">sm tooltip size</div>
        <div [arTooltip]="tooltip" arTooltipSize="md" type="argon-tooltip">md tooltip size</div>
        <div [arTooltip]="tooltip" arTooltipSize="lg" type="argon-tooltip">lg tooltip size</div>
        <div [arTooltip]="tooltip" arTooltipSize="xl" type="argon-tooltip">xl tooltip size</div>
        <div [arTooltip]="tooltip" type="argon-tooltip">default size</div>
      </div>
    `
  }), { notes })
  .add('Implement custom layout', () => ({
    template: `
      <div arTooltip type="custom-tooltip">
        This block should have tooltip
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `,
    moduleMetadata: {
      declarations: [
        CustomTooltipLayoutComponent
      ],
      entryComponents: [
        CustomTooltipLayoutComponent
      ],
      providers: [
        {
          provide: ARGON_TOOLTIP_LAYOUT,
          useValue: new TooltipLayoutConfigModel(CustomTooltipLayoutComponent, 'custom-tooltip'),
          multi: true
        }
      ]
    }
  }), { notes })
  .add('Provide default tooltip positions', () => ({
    template: `
      <div arTooltip>
        This block should have tooltip
        <ng-template>
          <small>I am tooltip!</small>
        </ng-template>
      </div>
    `,
    moduleMetadata: {
      providers: [
        {
          provide: ARGON_TOOLTIP_POSITION_PROVIDER,
          useValue: [
            { originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 0, weight: 200 },
            { originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: 0, weight: 100 },
            { originX: 'start', overlayX: 'end', originY: 'center', overlayY: 'center', offsetX: 0, weight: 80 },
            { originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 0, weight: 70 },
          ]
        }
      ]
    }
  }), { notes });
