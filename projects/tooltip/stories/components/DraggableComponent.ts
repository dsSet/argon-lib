import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TooltipOverlayService, TooltipService } from '../index';

@Component({
  selector: 'story-draggable-component',
  template: `
    <div class="dnd-element" [arTooltip] [positions]="positions" cdkDrag (cdkDragMoved)="handleMove()">
      Drag me
      <ng-template>
        <small>I should be moved with parent!</small>
      </ng-template>
    </div>
  `,
  styles: [`
    .dnd-element {
      display: flex;
      align-items: center;
      justify-content: center;
      cursor: grab;
      width: 100px;
      height: 100px;
      border: 1px solid #440044;
      background-color: #f9cd0b;
    }

    .dnd-element:active {
      cursor: grabbing;
      background-color: rgba(255, 204, 17, 0.43);
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DraggableComponent {

  public positions = TooltipService.createTooltipPositions(25, 'top', 'right', 'left', 'bottom');

  constructor(
    private tooltipOverlayService: TooltipOverlayService
  ) { }

  public handleMove() {
    this.tooltipOverlayService.updatePosition(10);
  }

}
