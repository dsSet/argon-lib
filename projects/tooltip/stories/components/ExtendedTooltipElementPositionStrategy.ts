import { Injectable } from '@angular/core';
import { TooltipElementPositionStrategy } from '../index';

@Injectable()
export class ExtendedTooltipElementPositionStrategy extends TooltipElementPositionStrategy {

  public showDebounceTime = 500;

  public hideDebounceTime = 1000;

}
