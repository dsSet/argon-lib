import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TooltipElementPositionStrategy, TooltipPositionStrategy } from '../index';

@Component({
  selector: 'story-with-position-component',
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy }
  ]
})
export class WithPositionComponent {

}
