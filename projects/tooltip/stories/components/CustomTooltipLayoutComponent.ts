import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TooltipLayoutModel } from '../index';


@Component({
  selector: `story-custom-tooltip-layout`,
  template: `
    <div>Tooltip layout fixed text:</div>
    <ng-container [cdkPortalOutlet]="tooltip"></ng-container>
  `,
  styles: [`
    :host {
      background-color: rgba(0, 105, 209, 0.51);
      color: white;
      border: 1px solid #007bff;
      padding: 0.5rem;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomTooltipLayoutComponent extends TooltipLayoutModel {

}
