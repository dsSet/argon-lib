# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@2.0.1...@argon/tooltip@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/tooltip





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@1.0.4...@argon/tooltip@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/tooltip






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@1.0.4...@argon/tooltip@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/tooltip






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@1.0.3...@argon/tooltip@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/tooltip





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@1.0.2...@argon/tooltip@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/tooltip





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@1.0.1...@argon/tooltip@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/tooltip






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.12...@argon/tooltip@1.0.1) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.12...@argon/tooltip@1.0.0) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))






## [0.2.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.11...@argon/tooltip@0.2.12) (2020-08-04)

**Note:** Version bump only for package @argon/tooltip






## [0.2.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.10...@argon/tooltip@0.2.11) (2020-07-08)


### Bug Fixes

* **tooltip:** add missed ctor for layout base model ([091c689](https://bitbucket.org/dsSet/argon-lib/commits/091c689))






## [0.2.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.9...@argon/tooltip@0.2.10) (2020-05-21)

**Note:** Version bump only for package @argon/tooltip






## [0.2.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.8...@argon/tooltip@0.2.9) (2020-04-18)

**Note:** Version bump only for package @argon/tooltip





## [0.2.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.7...@argon/tooltip@0.2.8) (2020-04-18)


### Bug Fixes

* **tooltip:** fix layout model annotation ([2d557de](https://bitbucket.org/dsSet/argon-lib/commits/2d557de))
* **tooltip:** update typings ([a60b662](https://bitbucket.org/dsSet/argon-lib/commits/a60b662))






## [0.2.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.6...@argon/tooltip@0.2.7) (2020-03-24)

**Note:** Version bump only for package @argon/tooltip





## [0.2.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.5...@argon/tooltip@0.2.6) (2020-03-24)

**Note:** Version bump only for package @argon/tooltip






## [0.2.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.4...@argon/tooltip@0.2.5) (2020-03-18)

**Note:** Version bump only for package @argon/tooltip





## [0.2.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.3...@argon/tooltip@0.2.4) (2020-02-14)

**Note:** Version bump only for package @argon/tooltip






## [0.2.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.2...@argon/tooltip@0.2.3) (2019-11-29)

**Note:** Version bump only for package @argon/tooltip





## [0.2.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.1...@argon/tooltip@0.2.2) (2019-11-22)

**Note:** Version bump only for package @argon/tooltip





## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.2.0...@argon/tooltip@0.2.1) (2019-11-22)

**Note:** Version bump only for package @argon/tooltip





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.1.8...@argon/tooltip@0.2.0) (2019-10-28)


### Bug Fixes

* **tooltip:** tooltip fixes ([b7b3646](https://bitbucket.org/dsSet/argon-lib/commits/b7b3646))
* **tooltip:** unsubscribe from prev position subscription ([19d632d](https://bitbucket.org/dsSet/argon-lib/commits/19d632d))


### Features

* **tooltip:** add position extension components for tooltip directive ([2f2e692](https://bitbucket.org/dsSet/argon-lib/commits/2f2e692))
* **tooltip:** add tooltip configurations ([d3f586e](https://bitbucket.org/dsSet/argon-lib/commits/d3f586e))





## [0.1.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.1.7...@argon/tooltip@0.1.8) (2019-10-11)

**Note:** Version bump only for package @argon/tooltip





## [0.1.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.1.6...@argon/tooltip@0.1.7) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))
* **tooltip:** fix tooltip rendering, fix tooltip destroy on component destroy ([3a20f71](https://bitbucket.org/dsSet/argon-lib/commits/3a20f71))





## [0.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.1.5...@argon/tooltip@0.1.6) (2019-10-08)


### Bug Fixes

* **tooltip:** fix tooltip update ([b5a0109](https://bitbucket.org/dsSet/argon-lib/commits/b5a0109))





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.1.4...@argon/tooltip@0.1.5) (2019-09-21)

**Note:** Version bump only for package @argon/tooltip





## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.1.3...@argon/tooltip@0.1.4) (2019-09-17)

**Note:** Version bump only for package @argon/tooltip





## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.1.2...@argon/tooltip@0.1.3) (2019-09-13)

**Note:** Version bump only for package @argon/tooltip






## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.1.1...@argon/tooltip@0.1.2) (2019-09-07)

**Note:** Version bump only for package @argon/tooltip





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tooltip@0.1.0...@argon/tooltip@0.1.1) (2019-09-06)

**Note:** Version bump only for package @argon/tooltip





# 0.1.0 (2019-08-14)


### Features

* **tooltip:** add element position strategy ([3148c53](https://bitbucket.org/dsSet/argon-lib/commits/3148c53))
* **tooltip:** add role attribute for tooltip layout ([e8e4e7c](https://bitbucket.org/dsSet/argon-lib/commits/e8e4e7c))
* **tooltip:** add tooltip layout, update tooltip position strategies ([39c13b7](https://bitbucket.org/dsSet/argon-lib/commits/39c13b7))
* **tooltip:** add tooltip module ([9d70907](https://bitbucket.org/dsSet/argon-lib/commits/9d70907))
* **tooltip:** add tooltip service factory, add public exports ([5121015](https://bitbucket.org/dsSet/argon-lib/commits/5121015))
* **tooltip:** implament update tooltip position method ([3c77c53](https://bitbucket.org/dsSet/argon-lib/commits/3c77c53))
* **tooltip:** implement base tooltip ([f27f83b](https://bitbucket.org/dsSet/argon-lib/commits/f27f83b))
* **tooltip:** split debounce time for show and hide tooltip events ([e0a415f](https://bitbucket.org/dsSet/argon-lib/commits/e0a415f))
