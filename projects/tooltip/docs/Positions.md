# Tooltip Positions

Tooltip displaying is basing on abstract class `TooltipPositionStrategy`.

````typescript
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { fromEvent, merge, Observable } from 'rxjs';
import { TooltipEventInterface } from '../interfaces/TooltipEventInterface';
import { TooltipParamsInterface } from '../interfaces/TooltipParamsInterface';
import { map } from 'rxjs/operators';

export abstract class TooltipPositionStrategy {

  public overlay: Overlay;

  public abstract getConfig(): OverlayConfig;

  public abstract connect(overlayRef: OverlayRef, overlay: Overlay): Observable<TooltipEventInterface | null>;

  public abstract dispose();

  public abstract show(params: TooltipParamsInterface);

  public abstract hide(params: TooltipParamsInterface);

  public abstract update(params: TooltipParamsInterface);

  public abstract isConnected(): boolean;

}

````

Module has two implementations of this class `TooltipElementPositionStrategy` (based on html element)
and `TooltipPointPositionStrategy` (based on mouse pointer). `TooltipPointPositionStrategy` provided by default.

## Change position strategy

````typescript
import { NgModule } from '@angular/core';
import { TooltipPositionStrategy, TooltipElementPositionStrategy } from '@argon/tooltip';

@NgModule({
  providers: [
    { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy }
  ]
})
class CustomModule { }
````

## Use Built in strategies at the template level

````html
<ar-point-tooltip class="example-content">
  This block should have tooltip with point strategy
  <ng-template>
    <small>I am tooltip!</small>
  </ng-template>
</ar-point-tooltip>

<ar-element-tooltip class="example-content">
  This block should have tooltip with element strategy
  <ng-template>
    <small>I am tooltip!</small>
  </ng-template>
</ar-element-tooltip>
````

`<ar-point-tooltip>` and `<ar-element-tooltip>` simple extensions of `arTooltip` directive with provided strategies.

## Provide position strategy on component level

````typescript
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TooltipElementPositionStrategy, TooltipPositionStrategy } from '../index';

@Component({
  selector: 'story-with-position-component',
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy }
  ]
})
export class WithPositionComponent {

}
````

````html
<story-with-position-component>
  <div class="example-content" arTooltip>
    This block should have tooltip
    <ng-template>
      <small>I am tooltip!</small>
    </ng-template>
  </div>
</story-with-position-component>
````

## Override debounce time for the TooltipElementPositionStrategy

````typescript
import { Injectable } from '@angular/core';
import { TooltipElementPositionStrategy } from '@argon/tooltip';

@Injectable()
export class ExtendedTooltipElementPositionStrategy extends TooltipElementPositionStrategy {

  public showDebounceTime = 500;

  public hideDebounceTime = 1000;

}
````
