# Use tooltip layout

## Select layout

> `argon-tooltip` layout provided by default.

````html
<div arTooltip type="argon-tooltip">
  This block should have tooltip
  <ng-template>
    <small>I am tooltip!</small>
  </ng-template>
</div>
````

## Provide layout as default

````typescript
import { NgModule } from '@angular/core';
import { ARGON_TOOLTIP_DEFAULT_LAYOUT } from '@argon/tooltip';

@NgModule({
  providers: [
    { provide: ARGON_TOOLTIP_DEFAULT_LAYOUT, useValue: 'argon-tooltip' }
  ]
})
class CustomModule { }
````

````html
<div arTooltip>
  This block should have tooltip
  <ng-template>
    <small>I am tooltip!</small>
  </ng-template>
</div>
````

## Sizing layout

````html
<div>
  <ng-template #tooltip>
    <small>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
      Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
      Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  
    </small>
  </ng-template>
  
  <div [arTooltip]="tooltip" arTooltipSize="xxs" type="argon-tooltip">xxs tooltip size</div>
  <div [arTooltip]="tooltip" arTooltipSize="xs" type="argon-tooltip">xs tooltip size</div>
  <div [arTooltip]="tooltip" arTooltipSize="sm" type="argon-tooltip">sm tooltip size</div>
  <div [arTooltip]="tooltip" arTooltipSize="md" type="argon-tooltip">md tooltip size</div>
  <div [arTooltip]="tooltip" arTooltipSize="lg" type="argon-tooltip">lg tooltip size</div>
  <div [arTooltip]="tooltip" arTooltipSize="xl" type="argon-tooltip">xl tooltip size</div>
  <div [arTooltip]="tooltip" type="argon-tooltip">default size</div>
</div>
````

## Implement custom layout

### 1. Create new component

````typescript
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TooltipLayoutModel } from '@argon/tooltip';

@Component({
  selector: `story-custom-tooltip-layout`,
  template: `
    <div>Tooltip layout fixed text:</div>
    <ng-container [cdkPortalOutlet]="tooltip"></ng-container>
  `,
  styles: [`
    :host {
      background-color: rgba(0, 105, 209, 0.51);
      color: white;
      border: 1px solid #007bff;
      padding: 0.5rem;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomTooltipLayoutComponent extends TooltipLayoutModel {

}
````

Custom layout should implements `TooltipLayoutModel`.

### 2. Register layout

````typescript
import { NgModule } from '@angular/core';
import { ARGON_TOOLTIP_DEFAULT_LAYOUT, ARGON_TOOLTIP_LAYOUT, TooltipLayoutConfigModel } from '@argon/tooltip';
import { CustomTooltipLayoutComponent } from './CustomTooltipLayoutComponent';

@NgModule({
  declarations: [
    CustomTooltipLayoutComponent
  ],
  entryComponents: [
    CustomTooltipLayoutComponent
  ],
  providers: [
    {
      provide: ARGON_TOOLTIP_LAYOUT,
      useValue: new TooltipLayoutConfigModel(CustomTooltipLayoutComponent, 'custom-tooltip'),
      multi: true
    },
    // Optionally you can set this layout as default tooltip layout
    // { provide: ARGON_TOOLTIP_DEFAULT_LAYOUT, useValue: 'custom-tooltip' }
  ]
})
class CustomModule { }
````

### 3. Usage

````html
<div arTooltip type="custom-tooltip">
  This block should have tooltip
  <ng-template>
    <small>I am tooltip!</small>
  </ng-template>
</div>
````

## Provide default tooltip positions

````typescript
import { NgModule } from '@angular/core';
import { ARGON_TOOLTIP_POSITION_PROVIDER } from '@argon/tooltip';

@NgModule({
  providers: [
    {
      provide: ARGON_TOOLTIP_POSITION_PROVIDER,
      useValue: [
        { originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 0, weight: 200 },
        { originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: 0, weight: 100 },
        { originX: 'start', overlayX: 'end', originY: 'center', overlayY: 'center', offsetX: 0, weight: 80 },
        { originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 0, weight: 70 },
      ]
    }
  ]
})
class CustomModule { }
````
