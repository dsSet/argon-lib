# Tooltip base usage

## Display tooltip

- From template

````html
<div arTooltip>
  This block should have tooltip
  <ng-template>
    <small>I am tooltip!</small>
  </ng-template>
</div>
````

- From CdkPortal

````html
<div arTooltip>
  This block should have tooltip
  <small *cdkPortal>I am tooltip!</small>
</div>
````

- Apply the same tooltip to several components

````html
<ng-template #tooltip>
  <small>I am tooltip!</small>
</ng-template>

<div class="btn btn-sm btn-success" [arTooltip]="tooltip">
  Button 1
</div>

<div class="btn btn-sm btn-error" [arTooltip]="tooltip">
  Button 2
</div>
````

- Resolve tooltip when several templates provided as content
 
````html
<div [arTooltip]="tooltip">
  This block should have tooltip
  <ng-template>Another template. this is not tooltip</ng-template>
  <ng-template #tooltip>
    <small>I am tooltip!</small>
  </ng-template>
</div>
````

- Provide tooltip component

````typescript

@Component({
  selector: 'story-tooltip-from-component',
  template: `<small>I am tooltip!</small>`
})
class TooltipFromComponent { }

const tooltip = TooltipFromComponent;

````

````html
  <div [arTooltip]="tooltip">This block should have tooltip</div>
````

- Provide tooltip portal

````typescript

const tooltip = new ComponentPortal(TooltipFromComponent);

````

````html
  <div [arTooltip]="tooltip">This block should have tooltip</div>
````

- Disabled tooltip

````html
<div arTooltip [disabled]="true">
  This block should have tooltip
  <ng-template>
    <small>I am tooltip!</small>
  </ng-template>
</div>
````

- Use custom tooltip positions

Use `TooltipService.createTooltipPositions` for create custom position.

`TooltipService.createTooltipPositions(offset: number, ...args: Array<'left' | 'right' | 'top' | 'bottom'>): Array<ConnectedPosition>`

`offest` - offset property for positions
`...args` - aliases collection for positions. Will keep positions order and weight.

> You can implement positions without `TooltipService`.

````typescript
 
const positions = TooltipService.createTooltipPositions(25, 'left', 'right');

````

````html
<div arTooltip [positions]="positions">
  This block should have tooltip
  <ng-template>
    <small>I am tooltip!</small>
  </ng-template>
</div>
````
