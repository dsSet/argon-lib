# @argon/tooltip

 
 
## Components

### `<ar-tooltip-layout>`
 
 Default tooltip layout.
 
## Directives

### `[arTooltip]`

|Property |Default value|Description
|---      |---          |---
|`@Input() arTooltip: Portal<any>  Type<any>  TemplateRef<any>` | `undefined` | Tooltip content
|`@Input() arTooltipSize: BsSizeEnum` | `undefined` | Layout css size (`sm`, `md` ...)
|`@Input() type: string;` | `undefined` | Uniq key to determinate tooltip layout
|`@Input() positions: Array<ConnectedPosition>` | `undefined` | List of custom tooltip positions
|`@Input() disabled: boolean` | `undefined` | Disabled flag

## Interfaces

### `TooltipEventInterface`

````typescript
import { PositionStrategy } from '@angular/cdk/overlay';
import { TooltipParamsInterface } from '..';

export interface TooltipEventInterface {

  position: PositionStrategy;

  params: TooltipParamsInterface;

  fromOverlay?: boolean;

}
````

### `TooltipParamsInterface`

````typescript
import { Portal } from '@angular/cdk/portal';
import { ElementRef, Type } from '@angular/core';
import { BsSizeEnum } from '@argon/bs-context';
import { TooltipLayoutModel } from '..';
import { ConnectedPosition } from '@angular/cdk/overlay';

export interface TooltipParamsInterface {

  tooltip?: Portal<any>;

  target?: ElementRef<any>;

  event?: MouseEvent;

  layout?: Type<TooltipLayoutModel>;

  size?: BsSizeEnum;

  positions?: Array<ConnectedPosition>;

}
````

## Models

### `TooltipLayoutModel`

Abstract model for tooltip layout.

````typescript
import { Portal } from '@angular/cdk/portal';
import { ContextDirective, BsSizeEnum } from '@argon/bs-context';
import { HostBinding } from '@angular/core';

export class TooltipLayoutModel extends ContextDirective {

  public tooltip: Portal<any>;

  public arBsSize: BsSizeEnum;

  @HostBinding('attr.role') role = 'tooltip';

  protected bsClassName = 'ar-tooltip';

}
````

### `TooltipLayoutConfigModel`

````typescript
import { Type } from '@angular/core';
import { TooltipLayoutModel } from './TooltipLayoutModel';

export class TooltipLayoutConfigModel {

  // Layout component
  public component: Type<TooltipLayoutModel>;
  
  // Layout key
  public type: string;

}
````

### Position strategies

`TooltipPositionStrategy` - base abstract class which can be extended for custom tooltip position implementation.

`TooltipPointPositionStrategy` - implementation for bind tooltip to the mouse cursor.

`TooltipElementPositionStrategy` - implementation for bind tooltip to the html element.

## Providers

### Tooltip layout configuration

`ARGON_TOOLTIP_LAYOUT` token to register tooltip layout, should be provided with `multi` property. `ar-tooltip-layout` provided by default.

> `export const ARGON_TOOLTIP_LAYOUT = new InjectionToken<TooltipLayoutConfigModel>('ARGON_TOOLTIP_LAYOUT');`

`ARGON_TOOLTIP_DEFAULT_LAYOUT` token for provide default tooltip layout (`null` value provided by default)

> `export const ARGON_TOOLTIP_DEFAULT_LAYOUT = new InjectionToken<string>('ARGON_TOOLTIP_DEFAULT_LAYOUT');`

### Tooltip position

`ARGON_TOOLTIP_POSITION_PROVIDER` token to provide default list of `ConnectedPosition` to display tooltip

> `export const ARGON_TOOLTIP_POSITION_PROVIDER = new InjectionToken<Array<ConnectedPosition>>('ARGON_TOOLTIP_POSITION_PROVIDER');`
