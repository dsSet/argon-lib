# Overlay service

Overlay service is most for the internal usage only, but sometimes you will need to update tooltip position dynamically.
You can use `updatePosition` method for this purpose. Here is implementation of draggable component with tooltip.
Component will update tooltip position on drag.

````typescript
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TooltipOverlayService, TooltipService } from '@argon/tooltip';

@Component({
  selector: 'story-draggable-component',
  template: `
    <div class="dnd-element" [arTooltip] [positions]="positions" cdkDrag (cdkDragMoved)="handleMove()">
      Drag me
      <ng-template>
        <small>I should be moved with parent!</small>
      </ng-template>
    </div>
  `,
  styles: [`
    .dnd-element {
      display: flex;
      align-items: center;
      justify-content: center;
      cursor: grab;
      width: 100px;
      height: 100px;
      border: 1px solid #440044;
      background-color: #f9cd0b;
    }

    .dnd-element:active {
      cursor: grabbing;
      background-color: rgba(255, 204, 17, 0.43);
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DraggableComponent {

  public positions = TooltipService.createTooltipPositions(25, 'top', 'right', 'left', 'bottom');

  constructor(
    private tooltipOverlayService: TooltipOverlayService
  ) { }

  public handleMove() {
    this.tooltipOverlayService.updatePosition(10);
  }

}

````

> Signature: `public updatePosition(throttleTimeout: number = 0): void`

`throttleTimeout` is throttle time for reduce number of tooltip updates.


