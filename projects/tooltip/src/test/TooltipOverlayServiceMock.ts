import createSpy = jasmine.createSpy;

export class TooltipOverlayServiceMock {

  displayTooltip = createSpy('displayTooltip');

  updateTooltipPosition = createSpy('updateTooltipPosition');

  hideTooltip = createSpy('hideTooltip');

  updatePosition = createSpy('updatePosition');

  removeTooltip = createSpy('removeTooltip');

  overlayElement = document.createElement('div');

  isVisible = createSpy('isVisible');

}
