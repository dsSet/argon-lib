import { TooltipPositionStrategy } from '../lib/models/TooltipPositionStrategy';
import createSpy = jasmine.createSpy;
import { Subject } from 'rxjs';
import { TooltipEventInterface } from '../lib/interfaces/TooltipEventInterface';
import { OverlayConfig } from '@angular/cdk/overlay';

export class TooltipPositionStrategyMock extends TooltipPositionStrategy {

  public tooltipSubject = new Subject<TooltipEventInterface>();

  public overlayConfig = new OverlayConfig();

  public isConnectedState = true;

  connect = createSpy('connect').and.returnValue(this.tooltipSubject);

  dispose = createSpy('dispose');

  getConfig = createSpy('getConfig').and.returnValue(this.overlayConfig);

  hide = createSpy('hide');

  show = createSpy('show');

  update = createSpy('update');

  isConnected = createSpy('isConnected').and.returnValue(this.isConnectedState);

}
