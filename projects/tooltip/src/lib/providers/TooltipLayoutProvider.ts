import { InjectionToken, Provider } from '@angular/core';
import { TooltipLayoutConfigModel } from '../models/TooltipLayoutConfigModel';
import { TooltipLayoutComponent } from '../components/TooltipLayout/TooltipLayoutComponent';

export const ARGON_TOOLTIP_LAYOUT = new InjectionToken<TooltipLayoutConfigModel>('ARGON_TOOLTIP_LAYOUT');

export const ARGON_TOOLTIP_DEFAULT_LAYOUT = new InjectionToken<string>('ARGON_TOOLTIP_DEFAULT_LAYOUT');

export const defaultLayout: TooltipLayoutConfigModel = {
  component: TooltipLayoutComponent,
  type: 'argon-tooltip'
};

export const layoutConfig: Array<Provider> = [
  {
    provide: ARGON_TOOLTIP_LAYOUT,
    useValue: defaultLayout,
    multi: true
  },
  {
    provide: ARGON_TOOLTIP_DEFAULT_LAYOUT,
    useValue: null
  }
];
