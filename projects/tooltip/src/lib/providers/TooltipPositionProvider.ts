import { InjectionToken, Provider } from '@angular/core';
import { ConnectedPosition } from '@angular/cdk/overlay';

export const ARGON_TOOLTIP_POSITION_PROVIDER = new InjectionToken<Array<ConnectedPosition>>('ARGON_TOOLTIP_POSITION_PROVIDER');

export const defaultTooltipPositions: Array<ConnectedPosition> = [
  { originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -10, weight: 100 },
  { originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 10, weight: 90 },
  { originX: 'start', overlayX: 'end', originY: 'center', overlayY: 'center', offsetX: -10, weight: 80 },
  { originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 10, weight: 70 },
];

export const positionStrategyConfig: Array<Provider> = [
  {
    provide: ARGON_TOOLTIP_POSITION_PROVIDER,
    useValue: defaultTooltipPositions
  }
];
