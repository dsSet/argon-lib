import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ComponentPortal } from '@angular/cdk/portal';
import { Component, SimpleChange, TemplateRef, ViewChild } from '@angular/core';
import { TooltipDirective } from './TooltipDirective';
import { TooltipOverlayServiceMock } from '../../test/TooltipOverlayServiceMock';
import { TooltipOverlayService } from '../services/TooltipOverlayService';
import { TooltipLayoutModel } from '../models/TooltipLayoutModel';
import { BsSizeEnum } from '@argon/bs-context';
import { ARGON_TOOLTIP_DEFAULT_LAYOUT, ARGON_TOOLTIP_LAYOUT } from '../providers/TooltipLayoutProvider';
import { defaultTooltipPositions, positionStrategyConfig } from '../providers/TooltipPositionProvider';
import { TooltipPositionStrategy } from '../models/TooltipPositionStrategy';
import { TooltipElementPositionStrategy } from '../models/TooltipElementPositionStrategy';
import { TooltipLayoutConfigModel } from '../models/TooltipLayoutConfigModel';

describe('TooltipDirective', () => {
  @Component({ selector: 'spec-fake-component', template: '<ng-template></ng-template>' })
  class FakeComponent extends TooltipDirective {
    @ViewChild(TemplateRef, { static: true }) template: TemplateRef<any>;

    get nativeElement() { return this.elementRef.nativeElement; }

    get element() { return this.elementRef; }
  }

  @Component({ selector: 'spec-tooltip-layout', template: '' })
  class FakeTooltipLayoutComponent extends TooltipLayoutModel { }

  @Component({ selector: 'spec-tooltip', template: '' })
  class FakeTooltip { }

  let component: TooltipDirective;
  let fixture: ComponentFixture<FakeComponent>;
  let overlayService: TooltipOverlayServiceMock;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      declarations: [ FakeComponent ],
      providers: [
        { provide: TooltipOverlayService, useValue: new TooltipOverlayServiceMock() },
        { provide: ARGON_TOOLTIP_LAYOUT, useValue: new TooltipLayoutConfigModel(FakeTooltipLayoutComponent, 'spec-layout'), multi: true },
        { provide: ARGON_TOOLTIP_DEFAULT_LAYOUT, useValue: 'spec-layout' },
        { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy },
        positionStrategyConfig
      ]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(FakeComponent);
    component = fixture.componentInstance;
    overlayService = TestBed.inject(TooltipOverlayService) as any;
  });

  afterEach(() => {
    fixture.detectChanges();
  });

  it('ngOnChanges should create tooltipPortal from arTooltip', () => {
    const arTooltip = new SimpleChange(null, FakeTooltip, false);
    component.ngOnChanges({ arTooltip });

    expect(component.tooltipPortal).toBeDefined();
  });

  it('ngOnChanges should not create tooltipPortal if arTooltip not changed', () => {
    const arTooltip = new SimpleChange(FakeTooltip, FakeTooltip, false);
    component.ngOnChanges({ arTooltip });
    expect(component.tooltipPortal).not.toBeDefined();
  });

  it('ngOnChanges should not create tooltipPortal if arTooltip value not changed', () => {
    component.ngOnChanges({ });

    expect(component.tooltipPortal).not.toBeDefined();
  });

  it('element mouseenter should call displayTooltip action', () => {
    fixture.detectChanges();

    const tooltipPortal = new ComponentPortal(FakeTooltip);
    const arTooltipSize = BsSizeEnum.SM;
    component.tooltipPortal = tooltipPortal;
    component.arTooltipSize = arTooltipSize;

    const event = new Event('mouseenter');

    const expectedValue = {
      tooltip: tooltipPortal,
      event,
      layout: FakeTooltipLayoutComponent,
      target: fixture.componentInstance.element,
      size: arTooltipSize,
      positions: defaultTooltipPositions
    };

    fixture.nativeElement.dispatchEvent(event);
    expect(overlayService.displayTooltip).toHaveBeenCalledWith(expectedValue, component.positionStrategy);
  });

  it('element mouseleave should call hideTooltip action', () => {
    fixture.detectChanges();

    const tooltipPortal = new ComponentPortal(FakeTooltip);
    const arTooltipSize = BsSizeEnum.SM;
    component.tooltipPortal = tooltipPortal;
    component.arTooltipSize = arTooltipSize;

    const event = new Event('mouseleave');

    const expectedValue = {
      tooltip: tooltipPortal,
      event,
      layout: FakeTooltipLayoutComponent,
      target: fixture.componentInstance.element,
      size: arTooltipSize,
      positions: defaultTooltipPositions
    };

    fixture.nativeElement.dispatchEvent(event);
    expect(overlayService.hideTooltip).toHaveBeenCalledWith(expectedValue, component.positionStrategy);
  });

  it('element mousemove should call updateTooltipPosition action', () => {
    fixture.detectChanges();

    const tooltipPortal = new ComponentPortal(FakeTooltip);
    const arTooltipSize = BsSizeEnum.SM;
    component.tooltipPortal = tooltipPortal;
    component.arTooltipSize = arTooltipSize;

    const event = new Event('mousemove');

    const expectedValue = {
      tooltip: tooltipPortal,
      event,
      layout: FakeTooltipLayoutComponent,
      target: fixture.componentInstance.element,
      size: arTooltipSize,
      positions: defaultTooltipPositions
    };

    fixture.nativeElement.dispatchEvent(event);
    expect(overlayService.updateTooltipPosition).toHaveBeenCalledWith(expectedValue, component.positionStrategy);
  });

  it('element mousemove should not call updateTooltipPosition action if disabled', () => {
    fixture.detectChanges();

    const tooltipPortal = new ComponentPortal(FakeTooltip);
    const arTooltipSize = BsSizeEnum.SM;
    component.tooltipPortal = tooltipPortal;
    component.arTooltipSize = arTooltipSize;
    component.disabled = true;

    const event = new Event('mousemove');

    fixture.nativeElement.dispatchEvent(event);
    expect(overlayService.updateTooltipPosition).not.toHaveBeenCalled();
  });

  it('element mouseenter should not call displayTooltip action id disabled', () => {
    fixture.detectChanges();

    const tooltipPortal = new ComponentPortal(FakeTooltip);
    const arTooltipSize = BsSizeEnum.SM;
    component.tooltipPortal = tooltipPortal;
    component.arTooltipSize = arTooltipSize;
    component.disabled = true;

    const event = new Event('mouseenter');

    fixture.nativeElement.dispatchEvent(event);
    expect(overlayService.displayTooltip).not.toHaveBeenCalled();
  });

  it('element mouseleave should not call hideTooltip action if has no tooltip portal', () => {
    fixture.detectChanges();

    const tooltipPortal = new ComponentPortal(FakeTooltip);
    const arTooltipSize = BsSizeEnum.SM;
    component.tooltipPortal = null;
    component.arTooltipSize = arTooltipSize;

    const event = new Event('mouseleave');

    fixture.nativeElement.dispatchEvent(event);
    expect(overlayService.hideTooltip).not.toHaveBeenCalled();
  });

  it('disabled change should not call overlayService.hideTooltip action if tooltip is not displaying', () => {
    const disabled = new SimpleChange(false, true, false);
    overlayService.isVisible.and.returnValue(false);
    component.ngOnChanges({ disabled });
    expect(overlayService.hideTooltip).not.toHaveBeenCalled();
  });

  it('disabled change should call overlayService.hideTooltip action if tooltip is displaying', () => {
    const disabled = new SimpleChange(false, true, false);
    overlayService.isVisible.and.returnValue(true);
    component.tooltipPortal = new ComponentPortal(FakeTooltip);
    component.ngOnChanges({ disabled });
    expect(overlayService.isVisible).toHaveBeenCalledWith(component.tooltipPortal);
    expect(overlayService.hideTooltip).toHaveBeenCalled();
  });
});
