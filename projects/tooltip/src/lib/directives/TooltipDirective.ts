import {
  AfterContentInit,
  ContentChild,
  Directive,
  ElementRef,
  HostBinding, Injector,
  Input, OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  TemplateRef,
  Type,
  ViewContainerRef
} from '@angular/core';
import { Unsubscribable, BaseShape } from '@argon/tools';
import { BsSizeType } from '@argon/bs-context';
import { fromEvent, Subscription } from 'rxjs';
import { CdkPortal, Portal } from '@angular/cdk/portal';
import { filter } from 'rxjs/operators';
import { TooltipOverlayService } from '../services/TooltipOverlayService';
import { TooltipLayoutModel } from '../models/TooltipLayoutModel';
import { TooltipParamsInterface } from '../interfaces/TooltipParamsInterface';
import { TooltipLayoutService } from '../services/TooltipLayoutService';
import { TooltipService } from '../services/TooltipService';
import { ConnectedPosition } from '@angular/cdk/overlay';
import { TooltipPositionStrategy } from '../models/TooltipPositionStrategy';

@Directive({
  selector: '[arTooltip]'
})
export class TooltipDirective extends BaseShape implements OnInit, OnDestroy, OnChanges, AfterContentInit {

  @Input() arTooltip: Portal<any> | Type<any> | TemplateRef<any>;

  @Input() arTooltipSize: BsSizeType;

  @Input() type: string;

  @Input() positions: Array<ConnectedPosition>;

  @Input() disabled: boolean;

  @ContentChild(TemplateRef, { read: TemplateRef, static: true }) templateContent: TemplateRef<any>;

  @ContentChild(CdkPortal, { read: CdkPortal, static: true }) portalContent: CdkPortal;

  @HostBinding('class.ar-tooltip-source') get className(): boolean {
    return Boolean(this.tooltipPortal) && !this.disabled;
  }

  public tooltipPortal: Portal<any>;

  public layout: Type<TooltipLayoutModel>;

  public readonly positionStrategy: TooltipPositionStrategy;

  protected elementRef: ElementRef<HTMLElement>;

  protected tooltipOverlayService: TooltipOverlayService;

  protected tooltipLayoutService: TooltipLayoutService;

  protected tooltipService: TooltipService;

  private mouseEnterSubscription: Subscription;

  private mouseLeaveSubscription: Subscription;

  private mouseOverSubscription: Subscription;

  constructor(
    protected viewContainer: ViewContainerRef,
    injector: Injector
  ) {
    super(injector);
    this.elementRef = this.injector.get(ElementRef);
    this.tooltipOverlayService = this.injector.get(TooltipOverlayService);
    this.tooltipLayoutService = this.injector.get(TooltipLayoutService);
    this.tooltipService = this.injector.get(TooltipService);
    this.positionStrategy = this.injector.get(TooltipPositionStrategy);
  }

  ngOnInit(): void {
    this.layout = this.tooltipLayoutService.getLayout(this.type);
    this.mouseEnterSubscription = fromEvent(this.elementRef.nativeElement, 'mouseenter').pipe(
      filter((event: MouseEvent) => this.showFilter(event))
    ).subscribe(this.handleMouseEnter);
    this.mouseLeaveSubscription = fromEvent(this.elementRef.nativeElement, 'mouseleave').pipe(
      filter((event: MouseEvent) => this.hideFilter(event))
    ).subscribe(this.handleMouseLeave);
    this.mouseOverSubscription = fromEvent(this.elementRef.nativeElement, 'mousemove').pipe(
      filter((event: MouseEvent) => this.showFilter(event))
    ).subscribe(this.handleMouseOver);
  }

  ngAfterContentInit(): void {
    const source = this.arTooltip || this.templateContent || this.portalContent;
    this.tooltipPortal = this.tooltipService.createTooltipPortal(source, this.viewContainer);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { arTooltip, disabled } = changes;
    if (arTooltip && arTooltip.previousValue !== arTooltip.currentValue) {
      this.tooltipPortal = this.tooltipService.createTooltipPortal(arTooltip.currentValue, this.viewContainer);
    }

    if (disabled && disabled.currentValue && this.tooltipOverlayService.isVisible(this.tooltipPortal)) {
      this.handleMouseLeave(null);
    }
  }

  @Unsubscribable
  ngOnDestroy(): void {
    this.tooltipOverlayService.removeTooltip(this.tooltipPortal);
    this.positionStrategy.dispose();
  }

  protected showFilter(event: MouseEvent): boolean {
    return Boolean(this.tooltipPortal) && !this.disabled;
  }

  protected hideFilter(event: MouseEvent): boolean {
    if (!this.tooltipPortal) {
      return false;
    }
    const overlayElement = this.tooltipOverlayService.overlayElement;
    return !(overlayElement.contains(event.relatedTarget as Node) || event.relatedTarget === overlayElement);
  }

  protected getTooltipParams(event: MouseEvent): TooltipParamsInterface {
    return {
      tooltip: this.tooltipPortal,
      event,
      layout: this.layout,
      target: this.elementRef,
      size: this.arTooltipSize,
      positions: this.positions || this.tooltipLayoutService.defaultTooltipPositions
    };
  }

  private handleMouseEnter = (event: MouseEvent) => {
    this.tooltipOverlayService.displayTooltip(this.getTooltipParams(event), this.positionStrategy);
  }

  private handleMouseLeave = (event: MouseEvent) => {
    this.tooltipOverlayService.hideTooltip(this.getTooltipParams(event), this.positionStrategy);
  }

  private handleMouseOver = (event: MouseEvent) => {
    this.tooltipOverlayService.updateTooltipPosition(this.getTooltipParams(event), this.positionStrategy);
  }
}
