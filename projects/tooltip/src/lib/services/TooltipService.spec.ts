import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TooltipService } from './TooltipService';
import { Component, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { ComponentPortal, TemplatePortal } from '@angular/cdk/portal';


describe('TooltipService', () => {

  @Component({ selector: 'spec-component', template: '<ng-template></ng-template>' })
  class MockComponent {
    @ViewChild(TemplateRef, { static: true, read: TemplateRef }) template: TemplateRef<any>;
    constructor(
      public viewContainer: ViewContainerRef
    ) { }
  }

  let service: TooltipService;
  let fixture: ComponentFixture<MockComponent>;
  let component: MockComponent;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      declarations: [
        MockComponent
      ],
      providers: [
        TooltipService,
      ]
    }).compileComponents()
  ));

  beforeEach(() => {
    service = TestBed.inject(TooltipService) as any;
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
  });

  it('createTooltipPortal should create portal', () => {
    fixture.detectChanges();

    const portal = new ComponentPortal(null);
    expect(service.createTooltipPortal(portal, component.viewContainer)).toEqual(portal);

    expect(service.createTooltipPortal(component.template, component.viewContainer))
      .toEqual(new TemplatePortal(component.template, component.viewContainer));

    expect(service.createTooltipPortal(MockComponent, component.viewContainer))
      .toEqual(new ComponentPortal(MockComponent));

    expect(service.createTooltipPortal(null, component.viewContainer)).toEqual(null);
  });

  it('getPositionLeft should create position', () => {
    expect(TooltipService.getPositionLeft(100, 25))
      .toEqual({ originX: 'start', overlayX: 'end', originY: 'center', overlayY: 'center', offsetX: -25, weight: 100 });
    expect(TooltipService.getPositionLeft(100))
      .toEqual({ originX: 'start', overlayX: 'end', originY: 'center', overlayY: 'center', offsetX: -10, weight: 100 });
  });

  it('getPositionRight should create position', () => {
    expect(TooltipService.getPositionRight(100, 25))
      .toEqual({ originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 25, weight: 100 });
    expect(TooltipService.getPositionRight(100))
      .toEqual({ originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: 10, weight: 100 });
  });

  it('getPositionTop should create position', () => {
    expect(TooltipService.getPositionTop(100, 25))
      .toEqual({ originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -25, weight: 100 });
    expect(TooltipService.getPositionTop(100))
      .toEqual({ originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -10, weight: 100 });
  });

  it('getPositionBottom should create position', () => {
    expect(TooltipService.getPositionBottom(100, 25))
      .toEqual({ originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 25, weight: 100 });
    expect(TooltipService.getPositionBottom(100))
      .toEqual({ originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: 10, weight: 100 });
  });

  it('createTooltipPositions should create a list of positions', () => {
    expect(TooltipService.createTooltipPositions(88, 'bottom', 'right', 'top', 'left'))
      .toEqual([
        TooltipService.getPositionBottom(100, 88),
        TooltipService.getPositionRight(99, 88),
        TooltipService.getPositionTop(98, 88),
        TooltipService.getPositionLeft(97, 88)
      ]);
  });

  it('createPosition should create position by key', () => {
    expect(TooltipService.createPosition('left', 5, 48))
      .toEqual(TooltipService.getPositionLeft(95, 48));

    expect(TooltipService.createPosition('right', 5, 48))
      .toEqual(TooltipService.getPositionRight(95, 48));

    expect(TooltipService.createPosition('top', 5, 48))
      .toEqual(TooltipService.getPositionTop(95, 48));

    expect(TooltipService.createPosition('bottom', 5, 48))
      .toEqual(TooltipService.getPositionBottom(95, 48));

    expect(TooltipService.createPosition(null, 5, 48))
      .toEqual(TooltipService.getPositionTop(95, 48));
  });
});
