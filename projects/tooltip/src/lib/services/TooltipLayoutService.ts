import find from 'lodash/find';
import { Inject, Injectable, Type } from '@angular/core';
import { ARGON_TOOLTIP_DEFAULT_LAYOUT, ARGON_TOOLTIP_LAYOUT } from '../providers/TooltipLayoutProvider';
import { TooltipLayoutModel } from '../models/TooltipLayoutModel';
import { TooltipLayoutConfigModel } from '../models/TooltipLayoutConfigModel';
import { ARGON_TOOLTIP_POSITION_PROVIDER } from '../providers/TooltipPositionProvider';
import { ConnectedPosition } from '@angular/cdk/overlay';


@Injectable({
  providedIn: 'root'
})
export class TooltipLayoutService {

  constructor(
    @Inject(ARGON_TOOLTIP_LAYOUT) public layout: Array<TooltipLayoutConfigModel>,
    @Inject(ARGON_TOOLTIP_DEFAULT_LAYOUT) public defaultType: string,
    @Inject(ARGON_TOOLTIP_POSITION_PROVIDER) public defaultTooltipPositions: Array<ConnectedPosition>
  ) { }

  public getLayout(key: string): Type<TooltipLayoutModel> | null {
    const type = key || this.defaultType;
    const layoutConfig: TooltipLayoutConfigModel = find(this.layout, { type });
    return layoutConfig ? layoutConfig.component : null;
  }

}
