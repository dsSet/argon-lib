import map from 'lodash/map';
import { Injectable, TemplateRef, Type, ViewContainerRef } from '@angular/core';
import { ComponentPortal, Portal, TemplatePortal } from '@angular/cdk/portal';
import { ConnectedPosition } from '@angular/cdk/overlay';

@Injectable({
  providedIn: 'root'
})
export class TooltipService {

  static getPositionLeft(weight: number, offset: number = 10): ConnectedPosition {
    return { originX: 'start', overlayX: 'end', originY: 'center', overlayY: 'center', offsetX: -offset, weight };
  }

  static getPositionRight(weight: number, offset: number = 10): ConnectedPosition {
    return { originX: 'end', overlayX: 'start', originY: 'center', overlayY: 'center', offsetX: offset, weight };
  }

  static getPositionTop(weight: number, offset: number = 10): ConnectedPosition {
    return { originX: 'center', overlayX: 'center', originY: 'top', overlayY: 'bottom', offsetY: -offset, weight };
  }

  static getPositionBottom(weight: number, offset: number = 10): ConnectedPosition {
    return { originX: 'center', overlayX: 'center', originY: 'bottom', overlayY: 'top', offsetY: offset, weight };
  }

  static createTooltipPositions(offset: number, ...args: Array<'left' | 'right' | 'top' | 'bottom'>): Array<ConnectedPosition> {
    const positions = map(args,
      function mapArgs(position: 'left' | 'right' | 'top' | 'bottom', index: number) {
        return TooltipService.createPosition(position, index, offset);
      }
    );
    return positions;
  }

  static createPosition(position: 'left' | 'right' | 'top' | 'bottom', index: number, offset: number): ConnectedPosition {
    const weight = 100 - index;
    switch (position) {
      case 'left': return TooltipService.getPositionLeft(weight, offset);
      case 'right': return TooltipService.getPositionRight(weight, offset);
      case 'top': return TooltipService.getPositionTop(weight, offset);
      case 'bottom': return TooltipService.getPositionBottom(weight, offset);
      default: return TooltipService.getPositionTop(weight, offset);
    }
  }

  public createTooltipPortal(tooltip: Portal<any> | Type<any> | TemplateRef<any>, viewContainer: ViewContainerRef): Portal<any> {
    if (tooltip instanceof Portal) {
      return tooltip;
    }

    if (tooltip instanceof TemplateRef) {
      return new TemplatePortal(tooltip, viewContainer);
    }

    if (tooltip instanceof Type) {
      return new ComponentPortal(tooltip);
    }

    return null;
  }

}
