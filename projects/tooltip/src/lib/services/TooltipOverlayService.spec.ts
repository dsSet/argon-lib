import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { TooltipOverlayService } from './TooltipOverlayService';
import { TooltipPositionStrategy } from '../models/TooltipPositionStrategy';
import { TooltipPositionStrategyMock } from '../../test/TooltipPositionStrategyMock';
import { FlexibleConnectedPositionStrategy, Overlay, OverlayModule, PositionStrategy } from '@angular/cdk/overlay';
import { TooltipParamsInterface } from '../interfaces/TooltipParamsInterface';
import { ComponentPortal, Portal } from '@angular/cdk/portal';
import { Component, ComponentRef, Type } from '@angular/core';
import { BsSizeEnum } from '@argon/bs-context';
import { TooltipEventInterface } from '../interfaces/TooltipEventInterface';
import { TooltipLayoutModel } from '../models/TooltipLayoutModel';

describe('TooltipOverlayService', () => {

  @Component({ selector: 'spec-fake-tooltip' })
  class FakeTooltipComponent { }

  @Component({ selector: 'spec-fake-layout' })
  class FakeLayoutComponent {
    tooltip: Portal<any>;
    arBsSize: BsSizeEnum;
  }

  let overlayService: TooltipOverlayService;
  let positionStrategy: TooltipPositionStrategyMock;
  let overlay: Overlay;

  const createPosition = (point: any, overlayFactory: Overlay): FlexibleConnectedPositionStrategy => {
    const position: FlexibleConnectedPositionStrategy = overlayFactory.position().flexibleConnectedTo({ x: 0, y: 0 });
    position.withPositions([
      { originX: 'end', originY: 'bottom', overlayX: 'end', overlayY: 'top', weight: 1 }
    ]);
    return position;
  };

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      imports: [ OverlayModule ],
      providers: [
        TooltipOverlayService,
        { provide: TooltipPositionStrategy, useClass: TooltipPositionStrategyMock }
      ]
    }).compileComponents()
  ));

  beforeEach(() => {
    overlayService = TestBed.inject(TooltipOverlayService) as any;
    positionStrategy = TestBed.inject(TooltipPositionStrategy) as any;
    overlay = TestBed.inject(Overlay) as any;
  });

  afterEach(() => {
    overlayService.updatePositionSubject.complete();
  });

  it('ctor should create overlayRef', () => {
    expect(overlayService.overlayRef).toBeDefined();
  });

  it('ctor should subscribe to updatePositionSubject', () => {
    expect(overlayService.updatePositionSubject.observers.length).toEqual(1);
  });

  it('displayTooltip should call show method with params', () => {
    const params: TooltipParamsInterface = {
      tooltip: new ComponentPortal(FakeTooltipComponent),
      size: BsSizeEnum.LG
    };
    overlayService.displayTooltip(params, positionStrategy);
    expect(positionStrategy.show).toHaveBeenCalledWith(params);
  });

  it('displayTooltip should not call show method without tooltip', () => {
    const params: TooltipParamsInterface = {
      size: BsSizeEnum.LG
    };
    overlayService.displayTooltip(params, positionStrategy);
    expect(positionStrategy.show).not.toHaveBeenCalled();
  });

  it('updateTooltipPosition should call update method', () => {
    const params: TooltipParamsInterface = {
      size: BsSizeEnum.LG
    };
    overlayService.updateTooltipPosition(params, positionStrategy);
    expect(positionStrategy.update).toHaveBeenCalledWith(params);
  });

  it('hideTooltip should call hide method', () => {
    const params: TooltipParamsInterface = {
      size: BsSizeEnum.LG
    };
    overlayService.hideTooltip(params, positionStrategy);
    expect(positionStrategy.hide).toHaveBeenCalledWith(params);
  });

  it('updatePosition should emit value with updatePositionSubject', () => {
    const time = 150;
    spyOn(overlayService.updatePositionSubject, 'next');
    spyOn(overlayService.overlayRef, 'updatePosition');
    overlayService.updatePosition(time);
    expect(overlayService.updatePositionSubject.next).toHaveBeenCalledWith(time);
  });

  it('updatePosition should emit default value with updatePositionSubject', () => {
    spyOn(overlayService.updatePositionSubject, 'next');
    overlayService.updatePosition();
    expect(overlayService.updatePositionSubject.next).toHaveBeenCalledWith(0);
  });

  it('ngOnDestroy should dispose service', () => {
    spyOn(overlayService.overlayRef, 'dispose');
    overlayService.ngOnDestroy();
    expect(overlayService.overlayRef.dispose).toHaveBeenCalled();
  });

  it('updatePositionSubject events should call overlay ref updatePosition', fakeAsync(() => {
    spyOn(overlayService.overlayRef, 'hasAttached').and.returnValue(true);
    spyOn(overlayService.overlayRef, 'updatePosition');
    overlayService.updatePositionSubject.next(50);
    tick(50);
    expect(overlayService.overlayRef.hasAttached).toHaveBeenCalled();
    expect(overlayService.overlayRef.updatePosition).toHaveBeenCalled();
  }));

  it('updatePositionSubject events should not call overlay ref updatePosition if has not attachments', fakeAsync(() => {
    spyOn(overlayService.overlayRef, 'hasAttached').and.returnValue(false);
    spyOn(overlayService.overlayRef, 'updatePosition');
    overlayService.updatePositionSubject.next(50);
    tick(50);
    expect(overlayService.overlayRef.hasAttached).toHaveBeenCalled();
    expect(overlayService.overlayRef.updatePosition).not.toHaveBeenCalled();
  }));

  it('should detach tooltip for null events', () => {
    const params: TooltipParamsInterface = {
      tooltip: new ComponentPortal(FakeTooltipComponent),
      size: BsSizeEnum.LG
    };
    spyOn(overlayService.overlayRef, 'detach');

    overlayService.displayTooltip(params, positionStrategy);
    positionStrategy.tooltipSubject.next(null);
    expect(overlayService.overlayRef.detach).toHaveBeenCalled();
  });

  it('should update position from event', () => {
    const params: TooltipParamsInterface = {
      tooltip: new ComponentPortal(FakeTooltipComponent),
      size: BsSizeEnum.LG
    };
    spyOn(overlayService.overlayRef, 'updatePositionStrategy');
    const position: PositionStrategy = createPosition({ x: 0, y: 0 }, overlay);
    const event: TooltipEventInterface = { position, params: { } };
    overlayService.displayTooltip(params, positionStrategy);
    positionStrategy.tooltipSubject.next(event);
    expect(overlayService.overlayRef.updatePositionStrategy).toHaveBeenCalledWith(position);
  });

  it('should skip update position if no positions in event', () => {
    const params: TooltipParamsInterface = {
      tooltip: new ComponentPortal(FakeTooltipComponent),
      size: BsSizeEnum.LG
    };
    spyOn(overlayService.overlayRef, 'updatePositionStrategy');
    const event: TooltipEventInterface = { position: null, params: { } };
    overlayService.displayTooltip(params, positionStrategy);
    positionStrategy.tooltipSubject.next(event);
    expect(overlayService.overlayRef.updatePositionStrategy).not.toHaveBeenCalled();
  });

  it('should skip update position from event if position is null', () => {
    spyOn(overlayService.overlayRef, 'updatePositionStrategy');
    const event: TooltipEventInterface = { position: null, params: { } };
    positionStrategy.tooltipSubject.next(event);
    expect(overlayService.overlayRef.updatePositionStrategy).not.toHaveBeenCalled();
  });

  it('should call updatePosition from event', () => {
    const params: TooltipParamsInterface = {
      tooltip: new ComponentPortal(FakeTooltipComponent),
      size: BsSizeEnum.LG
    };
    spyOn(overlayService, 'updatePosition');
    const position: PositionStrategy = createPosition({ x: 0, y: 0 }, overlay);
    const event: TooltipEventInterface = { position, params: { } };
    overlayService.displayTooltip(params, positionStrategy);
    positionStrategy.tooltipSubject.next(event);
    expect(overlayService.updatePosition).toHaveBeenCalled();
  });

  it('should not attach tooltip if has attachments', () => {
    spyOn(overlayService.overlayRef, 'hasAttached').and.returnValue(true);
    spyOn(overlayService.overlayRef, 'attach');
    const position: PositionStrategy = createPosition({ x: 0, y: 0 }, overlay);
    const event: TooltipEventInterface = { position, params: { tooltip: new ComponentPortal(FakeTooltipComponent) } };
    positionStrategy.tooltipSubject.next(event);
    expect(overlayService.overlayRef.attach).not.toHaveBeenCalled();
  });

  it('should reattach tooltip if has attachments and got another tooltip', () => {
    let isAttached = false;
    spyOn(overlayService.overlayRef, 'hasAttached').and.callFake(() => isAttached);
    const attachSpy = spyOn(overlayService.overlayRef, 'attach');
    spyOn(overlayService.overlayRef, 'detach');
    const position: PositionStrategy = createPosition({ x: 0, y: 0 }, overlay);
    const event1: TooltipEventInterface = { position, params: { tooltip: new ComponentPortal(FakeTooltipComponent) } };
    const event2: TooltipEventInterface = { position, params: { tooltip: new ComponentPortal(FakeTooltipComponent) } };
    overlayService.displayTooltip(event1.params, positionStrategy);
    positionStrategy.tooltipSubject.next(event1);
    isAttached = true;
    expect(overlayService.overlayRef.attach).toHaveBeenCalled();
    attachSpy.calls.reset();
    positionStrategy.tooltipSubject.next(event2);
    expect(overlayService.overlayRef.detach).toHaveBeenCalled();
    expect(overlayService.overlayRef.attach).toHaveBeenCalled();
  });

  it('should not reattach tooltip if has attachments and got the same tooltip', () => {
    let isAttached = false;
    spyOn(overlayService.overlayRef, 'hasAttached').and.callFake(() => isAttached);
    const attachSpy = spyOn(overlayService.overlayRef, 'attach');
    spyOn(overlayService.overlayRef, 'detach');
    const position: PositionStrategy = createPosition({ x: 0, y: 0 }, overlay);
    const event: TooltipEventInterface = { position, params: { tooltip: new ComponentPortal(FakeTooltipComponent) } };
    overlayService.displayTooltip(event.params, positionStrategy);
    positionStrategy.tooltipSubject.next(event);
    isAttached = true;
    expect(overlayService.overlayRef.attach).toHaveBeenCalled();
    attachSpy.calls.reset();
    positionStrategy.tooltipSubject.next(event);
    expect(overlayService.overlayRef.detach).not.toHaveBeenCalled();
    expect(overlayService.overlayRef.attach).not.toHaveBeenCalled();
  });

  it('should attach tooltip if has no attachments', () => {
    spyOn(overlayService.overlayRef, 'hasAttached').and.returnValue(false);
    spyOn(overlayService.overlayRef, 'attach');
    const position: PositionStrategy = createPosition({ x: 0, y: 0 }, overlay);
    const event: TooltipEventInterface = { position, params: { tooltip: new ComponentPortal(FakeTooltipComponent) } };
    overlayService.displayTooltip(event.params, positionStrategy);
    positionStrategy.tooltipSubject.next(event);
    expect(overlayService.overlayRef.attach).toHaveBeenCalledWith(event.params.tooltip);
  });

  it('should not attach tooltip if has no tooltip', () => {
    spyOn(overlayService.overlayRef, 'hasAttached').and.returnValue(false);
    spyOn(overlayService.overlayRef, 'attach');
    const position: PositionStrategy = createPosition({ x: 0, y: 0 }, overlay);
    const event: TooltipEventInterface = { position, params: { tooltip: null } };
    positionStrategy.tooltipSubject.next(event);
    expect(overlayService.overlayRef.attach).not.toHaveBeenCalled();
  });

  it('should attach layout if exist', () => {
    spyOn(overlayService.overlayRef, 'hasAttached').and.returnValue(false);
    const layoutInstance: ComponentRef<FakeLayoutComponent> = {
      instance: new FakeLayoutComponent(),
      changeDetectorRef: null,
      injector: null,
      hostView: null,
      componentType: FakeLayoutComponent,
      location: null,
      destroy(): void { },
      onDestroy(): void { },
      setInput(name: string, value: unknown): void { }
    };
    const position: PositionStrategy = createPosition({ x: 0, y: 0 }, overlay);
    const event: TooltipEventInterface = { position, params: {
        tooltip: new ComponentPortal(FakeTooltipComponent),
        layout: FakeLayoutComponent as Type<TooltipLayoutModel>,
        size: BsSizeEnum.MD
    } };
    spyOn(overlayService.overlayRef, 'attach').and.returnValue(layoutInstance);
    overlayService.displayTooltip(event.params, positionStrategy);
    positionStrategy.tooltipSubject.next(event);

    expect(overlayService.overlayRef.attach).toHaveBeenCalled();
    expect(layoutInstance.instance.tooltip).toEqual(event.params.tooltip);
    expect(layoutInstance.instance.arBsSize).toEqual(event.params.size);
  });

  it('isVisible should check tooltip', () => {
    const tooltip = new ComponentPortal(null);
    overlayService.currentEvent = null;
    expect(overlayService.isVisible(tooltip)).toBeFalsy();

    overlayService.currentEvent = { } as any;
    expect(overlayService.isVisible(tooltip)).toBeFalsy();

    overlayService.currentEvent = {
      params: { }
    } as any;
    expect(overlayService.isVisible(tooltip)).toBeFalsy();

    overlayService.currentEvent = {
      params: {
        tooltip: null
      }
    } as any;
    expect(overlayService.isVisible(tooltip)).toBeFalsy();

    overlayService.currentEvent = {
      params: {
        tooltip
      }
    } as any;
    expect(overlayService.isVisible(tooltip)).toBeTruthy();
  });

  it('remove event should not call overlay detach if has no current event', () => {
    spyOn(overlayService.overlayRef, 'detach');
    const tooltip = new ComponentPortal(null);
    overlayService.removeTooltip(tooltip);
    expect(overlayService.overlayRef.detach).not.toHaveBeenCalled();
  });

  it('remove event should not call overlay detach if current event tooltip not equal tooltip', () => {
    spyOn(overlayService.overlayRef, 'detach');
    overlayService.currentEvent = {
      params: {
        tooltip: new ComponentPortal(null)
      }
    } as any;
    overlayService.removeTooltip(null);
    expect(overlayService.overlayRef.detach).not.toHaveBeenCalled();
  });

  it('remove event should not call overlay detach if overlay has no attachments', () => {
    spyOn(overlayService.overlayRef, 'detach');
    spyOn(overlayService.overlayRef, 'hasAttached').and.returnValue(false);
    const tooltip = new ComponentPortal(null);
    overlayService.currentEvent = {
      params: {
        tooltip
      }
    } as any;
    overlayService.removeTooltip(null);
    expect(overlayService.overlayRef.detach).not.toHaveBeenCalled();
  });

  it('remove event should call overlay detach', () => {
    spyOn(overlayService.overlayRef, 'detach');
    spyOn(overlayService.overlayRef, 'hasAttached').and.returnValue(true);
    const tooltip = new ComponentPortal(null);
    overlayService.currentEvent = {
      params: {
        tooltip
      }
    } as any;
    overlayService.removeTooltip(tooltip);
    expect(overlayService.overlayRef.detach).toHaveBeenCalled();
  });

  it('updateTooltipPosition should connect position strategy if not connected', () => {
    const params: TooltipParamsInterface = {
      tooltip: new ComponentPortal(FakeTooltipComponent),
      size: BsSizeEnum.LG
    };
    positionStrategy.isConnected.and.returnValue(false);
    overlayService.updateTooltipPosition(params, positionStrategy);
    expect(positionStrategy.connect).toHaveBeenCalledWith(overlayService.overlayRef, overlayService.overlay);
  });
});
