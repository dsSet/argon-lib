import { Injectable, OnDestroy } from '@angular/core';
import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, Portal } from '@angular/cdk/portal';
import { Subject, Subscription, timer } from 'rxjs';
import { throttle } from 'rxjs/operators';
import { Unsubscribable } from '@argon/tools';
import { TooltipPositionStrategy } from '../models/TooltipPositionStrategy';
import { TooltipEventInterface } from '../interfaces/TooltipEventInterface';
import { TooltipParamsInterface } from '../interfaces/TooltipParamsInterface';

@Injectable({
  providedIn: 'root'
})
export class TooltipOverlayService implements OnDestroy {

  public readonly overlayRef: OverlayRef;

  public readonly updatePositionSubject = new Subject<number>();

  public get overlayElement(): HTMLElement { return this.overlayRef.overlayElement; }

  public currentEvent: TooltipEventInterface | null;

  private positionSubscription: Subscription;

  constructor(
    public overlay: Overlay
  ) {
    this.overlayRef = this.overlay.create({
      panelClass: 'ar-tooltip-overlay'
    });
    this.updatePositionSubject.pipe(
      throttle((throttleTimeout: number) => timer(throttleTimeout))
    ).subscribe(this.handleUpdateTooltipPosition);
  }

  public isVisible(tooltip: Portal<any>): boolean {
    return this.currentEvent && this.currentEvent.params && this.currentEvent.params.tooltip === tooltip;
  }

  public displayTooltip(params: TooltipParamsInterface, positionStrategy: TooltipPositionStrategy) {
    if (!params.tooltip) {
      return;
    }
    this.removePositionSubscription();
    this.positionSubscription = positionStrategy.connect(this.overlayRef, this.overlay).subscribe(this.handleUpdateTooltip);
    positionStrategy.show(params);
  }

  public updateTooltipPosition(params: TooltipParamsInterface, positionStrategy: TooltipPositionStrategy) {
    if (!positionStrategy.isConnected()) {
      this.removePositionSubscription();
      this.positionSubscription = positionStrategy.connect(this.overlayRef, this.overlay).subscribe(this.handleUpdateTooltip);
    }
    positionStrategy.update(params);
  }

  public hideTooltip(params: TooltipParamsInterface, positionStrategy: TooltipPositionStrategy) {
    positionStrategy.hide(params);
    positionStrategy.dispose();
  }

  public updatePosition(throttleTimeout: number = 0) {
    this.updatePositionSubject.next(throttleTimeout);
  }

  public removeTooltip(tooltip?: Portal<any>) {
    if (this.isVisible(tooltip) && this.overlayRef.hasAttached()) {
      this.overlayRef.detach();
    }
  }

  @Unsubscribable
  ngOnDestroy(): void {
    this.overlayRef.dispose();
  }

  private removePositionSubscription() {
    if (this.positionSubscription) {
      this.positionSubscription.unsubscribe();
      this.positionSubscription = null;
    }
  }

  private handleUpdateTooltip = (event: TooltipEventInterface | null) => {
    if (!event) {
      this.currentEvent = event;
      this.overlayRef.detach();
      return;
    }

    if (event.position) {
      this.overlayRef.updatePositionStrategy(event.position);
    }

    if (this.shouldReattach(event)) {
      this.overlayRef.detach();
      this.attachTooltip(event);
    } else if (!this.overlayRef.hasAttached()) {
      this.attachTooltip(event);
    }

    this.currentEvent = event;
    this.updatePosition();
  }

  private handleUpdateTooltipPosition = () => {
    if (this.overlayRef.hasAttached()) {
      this.overlayRef.updatePosition();
    }
  }

  private shouldReattach(event: TooltipEventInterface | null): boolean {
    if (!event || !this.currentEvent || event.fromOverlay) {
      return false;
    }

    return this.overlayRef.hasAttached() && event.params.tooltip !== this.currentEvent.params.tooltip;
  }

  private attachTooltip(event: TooltipEventInterface) {
    const { tooltip, layout } = event.params;

    if (!tooltip) {
      return;
    }

    if (layout) {
      const layoutPortal = new ComponentPortal(layout);
      const layoutRef = this.overlayRef.attach(layoutPortal);
      layoutRef.instance.tooltip = tooltip;
      layoutRef.instance.arBsSize = event.params.size;
    } else {
      this.overlayRef.attach(tooltip);
    }
  }

}
