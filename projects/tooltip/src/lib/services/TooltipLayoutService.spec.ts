import { TestBed, waitForAsync } from '@angular/core/testing';
import { TooltipLayoutService } from './TooltipLayoutService';
import { defaultTooltipPositions, positionStrategyConfig } from '../providers/TooltipPositionProvider';
import { defaultLayout, layoutConfig } from '../providers/TooltipLayoutProvider';


describe('TooltipLayoutService', () => {

  let service: TooltipLayoutService;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      providers: [
        TooltipLayoutService,
        positionStrategyConfig,
        layoutConfig,
      ]
    })
  ));

  beforeEach(() => {
    service = TestBed.inject(TooltipLayoutService) as any;
  });

  it('should have defaults', () => {
    expect(service.defaultTooltipPositions).toEqual(defaultTooltipPositions);
    expect(service.defaultType).toEqual(null);
    expect(service.layout).toEqual([defaultLayout]);
  });

  it('getLayout should return component from layout config', () => {
    expect(service.getLayout(defaultLayout.type)).toEqual(defaultLayout.component);
    expect(service.getLayout('fake-layout')).toEqual(null);
  });
});
