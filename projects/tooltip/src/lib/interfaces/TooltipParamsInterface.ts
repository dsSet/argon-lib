import { Portal } from '@angular/cdk/portal';
import { ElementRef, Type } from '@angular/core';
import { BsSizeType } from '@argon/bs-context';
import { TooltipLayoutModel } from '../models/TooltipLayoutModel';
import { ConnectedPosition } from '@angular/cdk/overlay';

export interface TooltipParamsInterface {

  tooltip?: Portal<any>;

  target?: ElementRef<any>;

  event?: MouseEvent;

  layout?: Type<TooltipLayoutModel>;

  size?: BsSizeType;

  positions?: Array<ConnectedPosition>;

}
