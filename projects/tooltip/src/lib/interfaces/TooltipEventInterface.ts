import { PositionStrategy } from '@angular/cdk/overlay';
import { TooltipParamsInterface } from './TooltipParamsInterface';

export interface TooltipEventInterface {

  position: PositionStrategy;

  params: TooltipParamsInterface;

  fromOverlay?: boolean;

}
