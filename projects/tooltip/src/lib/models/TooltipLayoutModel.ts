import { Portal } from '@angular/cdk/portal';
import { ContextMixin, BsSizeType, BsContextType } from '@argon/bs-context';
import { Directive, HostBinding, Injector, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UIShape } from '@argon/tools';

@Directive({
  selector: '[arTooltipLayoutModel]'
})
export class TooltipLayoutModel extends ContextMixin(UIShape) implements OnInit, OnChanges {

  public tooltip: Portal<any>;

  @Input() arBsSize: BsSizeType;

  @Input() arBsContext: BsContextType;

  @HostBinding('attr.role') role = 'tooltip';

  bsClassName = 'ar-tooltip';

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

}
