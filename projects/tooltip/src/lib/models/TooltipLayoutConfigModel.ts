import { Type } from '@angular/core';
import { TooltipLayoutModel } from './TooltipLayoutModel';

export class TooltipLayoutConfigModel {

  constructor(
    public component: Type<TooltipLayoutModel>,
    public type: string
  ) { }

}
