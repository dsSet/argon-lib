import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Injector } from '@angular/core';
import { TooltipLayoutModel } from './TooltipLayoutModel';


describe('TooltipLayoutModel', () => {

  @Component({ selector: 'spec-fake-layout', template: '' })
  class FakeTooltipLayoutComponent extends TooltipLayoutModel {

    constructor(injector: Injector) {
      super(injector);
    }
  }

  let component: FakeTooltipLayoutComponent;
  let fixture: ComponentFixture<FakeTooltipLayoutComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      declarations: [ FakeTooltipLayoutComponent ]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(FakeTooltipLayoutComponent);
    component = fixture.componentInstance;
  });

  it('should have role attribute', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('role')).toEqual('tooltip');
  });

  it('should have class name .ar-tooltip', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-tooltip')).toBeTruthy();
  });
});
