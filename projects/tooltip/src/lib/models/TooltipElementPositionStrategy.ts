import { TooltipPositionStrategy } from './TooltipPositionStrategy';
import { Overlay, OverlayConfig, OverlayRef, PositionStrategy } from '@angular/cdk/overlay';
import { merge, Observable, Subject, timer } from 'rxjs';
import { TooltipEventInterface } from '../interfaces/TooltipEventInterface';
import { Injectable } from '@angular/core';
import { debounce, distinctUntilChanged } from 'rxjs/operators';
import { TooltipParamsInterface } from '../interfaces/TooltipParamsInterface';

@Injectable()
export class TooltipElementPositionStrategy extends TooltipPositionStrategy {

  public eventBus: Subject<TooltipEventInterface>;

  public overlayRef: OverlayRef;

  public showDebounceTime = 150;

  public hideDebounceTime = 250;

  connect(overlayRef: OverlayRef, overlay: Overlay): Observable<TooltipEventInterface | null> {
    this.eventBus = new Subject<TooltipEventInterface>();
    this.overlay = overlay;
    return merge(
      this.eventBus,
      this.createOverlayObserver(overlayRef)
    ).pipe(
      debounce(this.getDebounceTime),
      distinctUntilChanged()
    );
  }

  dispose() {
    this.overlay = null;
    if (this.eventBus) {
      this.eventBus.complete();
      this.eventBus = null;
    }
  }

  getConfig(): OverlayConfig {
    return new OverlayConfig({
      scrollStrategy: this.overlay.scrollStrategies.close(),
      hasBackdrop: false
    });
  }

  show(params: TooltipParamsInterface) {
    this.eventBus.next({
      position: this.createPosition(params),
      params
    });
  }

  hide(params: TooltipParamsInterface) {
    if (this.eventBus) {
      this.eventBus.next(null);
    }
  }

  update(params: TooltipParamsInterface) { }

  isConnected(): boolean {
    return Boolean(this.eventBus);
  }

  private createPosition(params: TooltipParamsInterface): PositionStrategy {
    const position = this.overlay.position().flexibleConnectedTo(params.target);
    position.withPositions(params.positions);
    position.withGrowAfterOpen(true);
    position.withPush(true);
    return position;
  }

  private getDebounceTime = (event: TooltipEventInterface | null): Observable<any> => {
    return event ? timer(this.showDebounceTime) : timer(this.hideDebounceTime);
  }

}
