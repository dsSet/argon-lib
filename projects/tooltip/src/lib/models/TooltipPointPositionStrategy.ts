import { Injectable } from '@angular/core';
import { merge, Observable, Subject } from 'rxjs';
import { TooltipEventInterface } from '../interfaces/TooltipEventInterface';
import {
  FlexibleConnectedPositionStrategy,
  Overlay,
  OverlayConfig,
  OverlayRef
} from '@angular/cdk/overlay';
import { TooltipPositionStrategy } from './TooltipPositionStrategy';
import { TooltipParamsInterface } from '../interfaces/TooltipParamsInterface';

@Injectable()
export class TooltipPointPositionStrategy extends TooltipPositionStrategy {

  public eventBus: Subject<TooltipEventInterface>;

  public position: FlexibleConnectedPositionStrategy;

  public connect(overlayRef: OverlayRef, overlay: Overlay): Observable<TooltipEventInterface | null> {
    this.eventBus = new Subject<TooltipEventInterface>();
    this.overlay = overlay;
    return merge(
      this.eventBus,
      this.createOverlayObserver(overlayRef)
    );
  }

  public dispose() {
    this.overlay = null;
    if (this.eventBus) {
      this.eventBus.complete();
      this.eventBus = null;
    }
  }

  public getConfig(): OverlayConfig {
    const config = new OverlayConfig({
      scrollStrategy: this.overlay.scrollStrategies.close(),
      hasBackdrop: false,
    });

    return config;
  }

  public show(params: TooltipParamsInterface) {
    const position = this.getPosition(params);
    this.eventBus.next({ position, params });
  }

  public hide(params: TooltipParamsInterface) {
    this.position = null;
    if (this.eventBus) {
      this.eventBus.next(null);
    }
  }

  public update(params: TooltipParamsInterface) {
    const position = this.getPosition(params);
    this.eventBus.next({ position, params });
  }

  public isConnected(): boolean {
    return Boolean(this.eventBus);
  }

  private createPosition(params: TooltipParamsInterface): FlexibleConnectedPositionStrategy {
    const event = params.event;
    const position = this.overlay.position().flexibleConnectedTo({ x: event.clientX, y: event.clientY });
    position.withPositions(params.positions);
    position.withGrowAfterOpen(true);
    position.withPush(true);
    return position;
  }

  private getPosition(params: TooltipParamsInterface): FlexibleConnectedPositionStrategy {
    const event = params.event;
    if (!this.position) {
      this.position = this.createPosition(params);
    } else {
      this.position.setOrigin({ x: event.clientX, y: event.clientY });
    }
    return this.position;
  }

}
