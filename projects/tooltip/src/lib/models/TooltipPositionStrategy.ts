import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { fromEvent, merge, Observable } from 'rxjs';
import { TooltipEventInterface } from '../interfaces/TooltipEventInterface';
import { TooltipParamsInterface } from '../interfaces/TooltipParamsInterface';
import { map } from 'rxjs/operators';

export abstract class TooltipPositionStrategy {

  public overlay: Overlay;

  public abstract getConfig(): OverlayConfig;

  public abstract connect(overlayRef: OverlayRef, overlay: Overlay): Observable<TooltipEventInterface | null>;

  public abstract dispose();

  public abstract show(params: TooltipParamsInterface);

  public abstract hide(params: TooltipParamsInterface);

  public abstract update(params: TooltipParamsInterface);

  public abstract isConnected(): boolean;

  protected createOverlayObserver(overlayRef: OverlayRef): Observable<TooltipEventInterface | null> {
    return merge(
      fromEvent(overlayRef.overlayElement, 'mouseenter'),
      fromEvent(overlayRef.overlayElement, 'mouseleave').pipe(map(() => null)),
    ).pipe(
      map(this.createTooltipEvent)
    );
  }

  protected createTooltipEvent = (event: MouseEvent): TooltipEventInterface => {
    if (!event) {
      return null;
    }

    return {
      position: null,
      params: { event },
      fromOverlay: true
    };
  }
}
