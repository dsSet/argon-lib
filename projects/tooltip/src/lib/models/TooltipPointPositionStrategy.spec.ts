import { CloseScrollStrategy, Overlay, OverlayConfig, OverlayModule, PositionStrategy } from '@angular/cdk/overlay';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { TooltipParamsInterface } from '../interfaces/TooltipParamsInterface';
import createSpy = jasmine.createSpy;
import { Observable, Subject } from 'rxjs';
import { TooltipPointPositionStrategy } from './TooltipPointPositionStrategy';
import { TooltipEventInterface } from '../interfaces/TooltipEventInterface';
import { TooltipService } from '../services/TooltipService';

describe('TooltipPointPositionStrategy', () => {

  let positionStrategy: TooltipPointPositionStrategy;
  let overlay: Overlay;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      imports: [ OverlayModule ],
      providers: [
        TooltipPointPositionStrategy
      ]
    }).compileComponents()
  ));

  beforeEach(() => {
    positionStrategy = TestBed.inject(TooltipPointPositionStrategy) as any;
    overlay = TestBed.inject(Overlay) as any;
  });

  it('dispose should complete event bus', () => {
    const eventBus = new Subject<any>();
    positionStrategy.eventBus = eventBus;
    spyOn(positionStrategy.eventBus, 'complete');
    positionStrategy.dispose();
    expect(eventBus.complete).toHaveBeenCalled();
    expect(positionStrategy.eventBus).toEqual(null);
  });

  it('dispose should remove overlay', () => {
    positionStrategy.overlay = overlay;
    positionStrategy.dispose();
    expect(positionStrategy.overlay).toEqual(null);
  });

  it('getConfig should create new config', () => {
    positionStrategy.overlay = overlay;
    const result = positionStrategy.getConfig();
    expect(result instanceof OverlayConfig).toBeTruthy();
    expect(result.hasBackdrop).toBeFalsy();
    expect(result.scrollStrategy instanceof CloseScrollStrategy).toBeTruthy();
  });

  it('show should emit tooltip event', () => {
    const point = { x: 180, y: 85 };
    positionStrategy.connect(overlay.create({
      positionStrategy: overlay.position().flexibleConnectedTo(point)
    }), overlay);
    const params: TooltipParamsInterface = {
      event: new MouseEvent('mouseenter', { clientX: point.x, clientY: point.y }),
      positions: TooltipService.createTooltipPositions(10, 'right')
    };

    const mockPosition = {
      withPositions: createSpy('withPositions'),
      withGrowAfterOpen: createSpy('withGrowAfterOpen'),
      withPush: createSpy('withPush')
    } as any;

    const mockPositionFactory = {
      flexibleConnectedTo: createSpy('flexibleConnectedTo').and.returnValue(mockPosition)
    };

    spyOn(overlay, 'position').and.returnValue(mockPositionFactory as any);
    spyOn(positionStrategy.eventBus, 'next');

    positionStrategy.show(params);

    expect(mockPositionFactory.flexibleConnectedTo).toHaveBeenCalledWith(point);
    expect(mockPosition.withPositions).toHaveBeenCalledWith(params.positions);
    expect(mockPosition.withGrowAfterOpen).toHaveBeenCalledWith(true);
    expect(mockPosition.withPush).toHaveBeenCalledWith(true);
    expect(positionStrategy.eventBus.next).toHaveBeenCalledWith({
      position: mockPosition,
      params
    });
  });

  it('hide should emit null', () => {
    positionStrategy.eventBus = new Subject<any>();
    positionStrategy.position = { data: 'Fake Position' } as any;
    spyOn(positionStrategy.eventBus, 'next');
    positionStrategy.hide({ });
    expect(positionStrategy.eventBus.next).toHaveBeenCalledWith(null);
    expect(positionStrategy.position).toBeNull();
  });

  it('connect should return observable', () => {
    const overlayRef = overlay.create();
    const result = positionStrategy.connect(overlayRef, overlay);
    expect(result instanceof Observable).toBeTruthy();
    positionStrategy.dispose();
  });

  it('update should emit next event with new origin', () => {
    positionStrategy.connect(overlay.create({
      positionStrategy: overlay.position().flexibleConnectedTo({ x: 150, y: 11 })
    }), overlay);
    const point = { x: 180, y: 85 };
    const params: TooltipParamsInterface = {
      event: new MouseEvent('mouseenter', { clientX: point.x, clientY: point.y }),
      positions: TooltipService.createTooltipPositions(10, 'right')
    };
    let result: TooltipEventInterface = null;
    positionStrategy.connect(overlay.create(), overlay).subscribe((res: any) => { result = res; });
    positionStrategy.update(params);

    expect(result).toBeTruthy();
  });

  it('update should update position origin if position exist', () => {
    positionStrategy.connect(overlay.create({
      positionStrategy: overlay.position().flexibleConnectedTo({ x: 150, y: 11 })
    }), overlay);
    const position = overlay.position().flexibleConnectedTo({ x: 18, y: 250 });
    positionStrategy.position = position;
    spyOn(positionStrategy.position, 'setOrigin');
    const point = { x: 180, y: 85 };
    const params: TooltipParamsInterface = {
      event: new MouseEvent('mouseenter', { clientX: point.x, clientY: point.y })
    };
    positionStrategy.update(params);
    expect(positionStrategy.position.setOrigin).toHaveBeenCalledWith(point);
  });

  it('isConnected should based on event bus', () => {
    expect(positionStrategy.isConnected()).toBeFalsy();
    positionStrategy.eventBus = new Subject<any>();
    expect(positionStrategy.isConnected()).toBeTruthy();
  });

  it('hide should remove position', () => {
    positionStrategy.position = { } as any;
    positionStrategy.hide({ });
    expect(positionStrategy.position).toEqual(null);
  });
});
