import { CloseScrollStrategy, Overlay, OverlayConfig, OverlayModule } from '@angular/cdk/overlay';
import { fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { TooltipElementPositionStrategy } from './TooltipElementPositionStrategy';
import { TooltipParamsInterface } from '../interfaces/TooltipParamsInterface';
import { ElementRef } from '@angular/core';
import createSpy = jasmine.createSpy;
import { Observable, Subject } from 'rxjs';
import { TooltipService } from '../services/TooltipService';

describe('TooltipElementPositionStrategy', () => {

  let positionStrategy: TooltipElementPositionStrategy;
  let overlay: Overlay;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      imports: [ OverlayModule ],
      providers: [
        TooltipElementPositionStrategy
      ]
    }).compileComponents()
  ));

  beforeEach(() => {
    positionStrategy = TestBed.inject(TooltipElementPositionStrategy) as any;
    overlay = TestBed.inject(Overlay) as any;
  });

  it('should have default values for show and hide timeouts', () => {
    expect(positionStrategy.showDebounceTime).toEqual(150);
    expect(positionStrategy.hideDebounceTime).toEqual(250);
  });

  it('dispose should complete event bus', () => {
    const eventBus = new Subject<any>();
    positionStrategy.eventBus = eventBus;
    positionStrategy.overlay = overlay;
    spyOn(eventBus, 'complete');
    positionStrategy.dispose();
    expect(eventBus.complete).toHaveBeenCalled();
    expect(positionStrategy.eventBus).toEqual(null);
    expect(positionStrategy.overlay).toEqual(null);
  });

  it('getConfig should create new config', () => {
    positionStrategy.overlay = overlay;
    const result = positionStrategy.getConfig();
    expect(result instanceof OverlayConfig).toBeTruthy();
    expect(result.hasBackdrop).toBeFalsy();
    expect(result.scrollStrategy instanceof CloseScrollStrategy).toBeTruthy();
  });

  it('show should emit tooltip event', () => {
    positionStrategy.connect(overlay.create({
      positionStrategy: overlay.position().flexibleConnectedTo(document.createElement('div'))
    }), overlay);
    const fakeTarget = { data: 'Fake target' };
    const params: TooltipParamsInterface = {
      target: new ElementRef(fakeTarget),
      positions: TooltipService.createTooltipPositions(10, 'left')
    };

    const mockPosition = {
      withPositions: createSpy('withPositions'),
      withGrowAfterOpen: createSpy('withGrowAfterOpen'),
      withPush: createSpy('withPush')
    } as never as any;

    const mockPositionFactory = {
      flexibleConnectedTo: createSpy('flexibleConnectedTo').and.returnValue(mockPosition)
    };

    spyOn(overlay, 'position').and.returnValue(mockPositionFactory as any);
    spyOn(positionStrategy.eventBus, 'next');

    positionStrategy.show(params);

    expect(mockPositionFactory.flexibleConnectedTo).toHaveBeenCalledWith(params.target);
    expect(mockPosition.withPositions).toHaveBeenCalledWith(params.positions);
    expect(mockPosition.withGrowAfterOpen).toHaveBeenCalledWith(true);
    expect(mockPosition.withPush).toHaveBeenCalledWith(true);
    expect(positionStrategy.eventBus.next).toHaveBeenCalledWith({
      position: mockPosition,
      params
    });
  });

  it('hide should emit null', () => {
    positionStrategy.eventBus = new Subject<any>();
    spyOn(positionStrategy.eventBus, 'next');
    positionStrategy.hide({ });
    expect(positionStrategy.eventBus.next).toHaveBeenCalledWith(null);
  });

  it('connect should return observable', () => {
    const overlayRef = overlay.create();
    const result = positionStrategy.connect(overlayRef, overlay);
    expect(result instanceof Observable).toBeTruthy();
    positionStrategy.dispose();
  });

  it('connected observer should emit show event with showDebounceTime', fakeAsync(() => {
    const overlayRef = overlay.create();
    let result: any = null;
    positionStrategy.connect(overlayRef, overlay).subscribe((res: any) => { result = res; });
    const event = { data: 'fake event' } as any;
    positionStrategy.eventBus.next(event);
    tick(positionStrategy.showDebounceTime / 2);
    expect(result).toBeNull();
    tick(positionStrategy.showDebounceTime / 2);
    expect(result).toEqual(event);
    positionStrategy.dispose();
  }));

  it('connected observer should emit hide event with hideDebounceTime', fakeAsync(() => {
    const overlayRef = overlay.create();
    const prevValue = { data: 'mock result' };
    let result: any = prevValue;
    positionStrategy.connect(overlayRef, overlay).subscribe((res: any) => { result = res; });
    positionStrategy.eventBus.next(null);
    tick(positionStrategy.hideDebounceTime / 2);
    expect(result).toEqual(prevValue);
    tick(positionStrategy.hideDebounceTime / 2);
    expect(result).toEqual(null);
    positionStrategy.dispose();
  }));

  it('should dispatch null for overlay mouseleave', fakeAsync(() => {
    const overlayRef = overlay.create();
    let result: any = { data: 'mock result' };
    positionStrategy.connect(overlayRef, overlay).subscribe((res: any) => { result = res; });
    const event = new MouseEvent('mouseleave');
    overlayRef.overlayElement.dispatchEvent(event);
    tick(positionStrategy.hideDebounceTime);
    expect(result).toBeNull();
  }));

  it('should dispatch event for overlay mouseenter', fakeAsync(() => {
    const overlayRef = overlay.create();
    let result: any = null;
    positionStrategy.connect(overlayRef, overlay).subscribe((res: any) => { result = res; });
    const event = new MouseEvent('mouseenter');
    overlayRef.overlayElement.dispatchEvent(event);
    tick(positionStrategy.hideDebounceTime);
    expect(result).toEqual({
      position: null,
      params: { event },
      fromOverlay: true
    });
  }));

  it('update should not emit tooltip event', () => {
    positionStrategy.connect(overlay.create({
      positionStrategy: overlay.position().flexibleConnectedTo(document.createElement('div'))
    }), overlay);

    spyOn(positionStrategy.eventBus, 'next');
    positionStrategy.update({ });
    expect(positionStrategy.eventBus.next).not.toHaveBeenCalled();
  });

  it('isConnected should based on event bus', () => {
    expect(positionStrategy.isConnected()).toBeFalsy();
    positionStrategy.eventBus = new Subject<any>();
    expect(positionStrategy.isConnected()).toBeTruthy();
  });
});
