import { ChangeDetectionStrategy, Component, Injector, ViewContainerRef } from '@angular/core';
import { TooltipDirective } from '../../directives/TooltipDirective';
import { TooltipPositionStrategy } from '../../models/TooltipPositionStrategy';
import { TooltipElementPositionStrategy } from '../../models/TooltipElementPositionStrategy';

@Component({
  selector: 'ar-element-tooltip',
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy }
  ]
})
export class ElementTooltipComponent extends TooltipDirective {

  constructor(viewContainer: ViewContainerRef, injector: Injector) {
    super(viewContainer, injector);
  }

}
