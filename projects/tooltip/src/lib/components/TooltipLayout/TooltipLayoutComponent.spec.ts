import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TooltipLayoutComponent } from './TooltipLayoutComponent';
import { TooltipLayoutModel } from '../../models/TooltipLayoutModel';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('TooltipLayoutComponent', () => {
  let component: TooltipLayoutComponent;
  let fixture: ComponentFixture<TooltipLayoutComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      declarations: [ TooltipLayoutComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(TooltipLayoutComponent);
    component = fixture.componentInstance;
  });

  it('should have default properties', () => {
    expect(component.className).toBeTruthy();
  });

  it('should extend TooltipLayoutModel', () => {
    expect(component instanceof TooltipLayoutModel).toBeTruthy();
  });

  it('className should bind .ar-tooltip-layout class', () => {
    component.className = true;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-tooltip-layout')).toEqual(true);
    component.className = false;
    fixture.detectChanges();
    expect(fixture.nativeElement.classList.contains('ar-tooltip-layout')).toEqual(false);
  });
});
