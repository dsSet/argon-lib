import { TooltipLayoutModel } from '../../models/TooltipLayoutModel';
import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Injector, Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { BsContextType, BsSizeType } from '@argon/bs-context';

@Component({
  selector: 'ar-tooltip-layout',
  template: `
    <ng-container [cdkPortalOutlet]="tooltip"></ng-container>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TooltipLayoutComponent extends TooltipLayoutModel implements OnInit, OnChanges {

  @HostBinding('class.ar-tooltip-layout') className = true;

  @Input() arBsSize: BsSizeType;

  @Input() arBsContext: BsContextType;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

}
