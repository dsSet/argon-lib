import { ChangeDetectionStrategy, Component, Injector, ViewContainerRef } from '@angular/core';
import { TooltipDirective } from '../../directives/TooltipDirective';
import { TooltipPointPositionStrategy } from '../../models/TooltipPointPositionStrategy';
import { TooltipPositionStrategy } from '../../models/TooltipPositionStrategy';

@Component({
  selector: 'ar-point-tooltip',
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: TooltipPositionStrategy, useClass: TooltipPointPositionStrategy }
  ]
})
export class PointTooltipComponent extends TooltipDirective {

  constructor(viewContainer: ViewContainerRef, injector: Injector) {
    super(viewContainer, injector);
  }

}
