import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { PointTooltipComponent } from './PointTooltipComponent';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TooltipDirective } from '../../directives/TooltipDirective';
import { TooltipOverlayService } from '../../services/TooltipOverlayService';
import { TooltipOverlayServiceMock } from '../../../test/TooltipOverlayServiceMock';
import { TooltipPositionStrategy } from '../../models/TooltipPositionStrategy';
import { TooltipElementPositionStrategy } from '../../models/TooltipElementPositionStrategy';
import { positionStrategyConfig } from '../../providers/TooltipPositionProvider';
import { layoutConfig } from '../../providers/TooltipLayoutProvider';

describe('PointTooltipComponent', () => {
  let component: PointTooltipComponent;
  let fixture: ComponentFixture<PointTooltipComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
      declarations: [ PointTooltipComponent ],
      providers: [
        { provide: TooltipOverlayService, useValue: new TooltipOverlayServiceMock() },
        { provide: TooltipPositionStrategy, useClass: TooltipElementPositionStrategy },
        positionStrategyConfig,
        layoutConfig
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointTooltipComponent);
    component = fixture.componentInstance;
  });

  it('should extends TooltipDirective', () => {
    expect(component).toEqual(jasmine.any(TooltipDirective));
  });
});
