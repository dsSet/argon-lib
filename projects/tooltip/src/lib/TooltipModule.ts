import { NgModule } from '@angular/core';
import { TooltipPositionStrategy } from './models/TooltipPositionStrategy';
import { TooltipPointPositionStrategy } from './models/TooltipPointPositionStrategy';
import { TooltipDirective } from './directives/TooltipDirective';
import { OverlayModule } from '@angular/cdk/overlay';
import { TooltipLayoutComponent } from './components/TooltipLayout/TooltipLayoutComponent';
import { PortalModule } from '@angular/cdk/portal';
import { TooltipLayoutModel } from './models/TooltipLayoutModel';
import { layoutConfig } from './providers/TooltipLayoutProvider';
import { positionStrategyConfig } from './providers/TooltipPositionProvider';
import { ElementTooltipComponent } from './components/ElementTooltip/ElementTooltipComponent';
import { PointTooltipComponent } from './components/PointTooltip/PointTooltipComponent';

@NgModule({
    imports: [
        OverlayModule,
        PortalModule
    ],
    declarations: [
        TooltipDirective,
        TooltipLayoutComponent,
        TooltipLayoutModel,
        ElementTooltipComponent,
        PointTooltipComponent
    ],
    exports: [
        TooltipDirective,
        ElementTooltipComponent,
        PointTooltipComponent
    ],
    providers: [
        { provide: TooltipPositionStrategy, useClass: TooltipPointPositionStrategy },
        layoutConfig,
        positionStrategyConfig
    ]
})
export class TooltipModule { }
