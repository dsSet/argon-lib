import { DropdownMenuA11yService } from './DropdownMenuA11yService';
import { ALPHANUMERIC_KEYS, KeyboardService, KeyCodesEnum, NAVIGATION_KEYS } from '@argon/keyboard';
import { SelectableItemInterface } from '../interfaces/SelectableItemInterface';
import { ChangeDetectorRef, ElementRef, EventEmitter, QueryList } from '@angular/core';
import { Subject } from 'rxjs';
import { fakeAsync, tick } from '@angular/core/testing';


const createItem = (disabled = false, selected = false): SelectableItemInterface => ({
  click: jasmine.createSpyObj('EventEmitter', ['emit']),
  changeDetector: jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']),
  disabled,
  selected,
  search: jasmine.createSpy('search', () => false)
});

const createKeyboardEvent = (code: KeyCodesEnum, key?: string): KeyboardEvent => {
  return new KeyboardEvent('keydown', { code, key });
};

const navigationKeys = [...NAVIGATION_KEYS, KeyCodesEnum.Enter, KeyCodesEnum.Space];

describe('DropdownMenuA11yService', () => {
  let service: DropdownMenuA11yService;
  let keyboardServiceSpy: jasmine.SpyObj<KeyboardService>;
  let items: Array<SelectableItemInterface>;
  let keySubject: Subject<KeyboardEvent>;
  let itemsList: QueryList<SelectableItemInterface>;

  beforeEach(() => {
    keySubject = new Subject();
    items = [
      createItem(),
      createItem(),
      createItem(),
      createItem()
    ];
    itemsList = new QueryList<SelectableItemInterface>();
    itemsList.reset(items);
    keyboardServiceSpy = jasmine.createSpyObj('KeyboardService', ['listenKeyDownEvent']);
    keyboardServiceSpy.listenKeyDownEvent.and.returnValue(keySubject);
    service = new DropdownMenuA11yService(keyboardServiceSpy);
  });

  afterEach(() => {
    keySubject.complete();
  });

  it('should have navigation constants', () => {
    expect(DropdownMenuA11yService.pageOffset).toEqual(10);
    expect(DropdownMenuA11yService.searchDebounceTime).toEqual(250);
  });

  it('registerItems should call selection.registerList', () => {
    spyOn(service.selection, 'registerList');
    service.registerItems(itemsList);
    expect(service.selection.registerList).toHaveBeenCalledWith(itemsList);
  });

  it('registerItems should subscribe to keyboard navigation', () => {
    service.registerItems(itemsList);
    expect(keyboardServiceSpy.listenKeyDownEvent).toHaveBeenCalledWith(navigationKeys, null);
  });

  it('registerItems should subscribe to keyboard navigation with HtmlElement', () => {
    const element = document.createElement('div');
    service.registerItems(itemsList, new ElementRef(element));
    expect(keyboardServiceSpy.listenKeyDownEvent).toHaveBeenCalledWith(navigationKeys, element);
  });

  it('registerItems should subscribe alphaNumeric keydown events', () => {
    service.registerItems(itemsList);
    expect(keyboardServiceSpy.listenKeyDownEvent).toHaveBeenCalledWith(ALPHANUMERIC_KEYS, null);
  });

  it('registerItems should subscribe alphaNumeric keydown events with HtmlElement', () => {
    const element = document.createElement('div');
    service.registerItems(itemsList, new ElementRef(element));
    expect(keyboardServiceSpy.listenKeyDownEvent).toHaveBeenCalledWith(ALPHANUMERIC_KEYS, element);
  });

  it('selectItem should call selection.select method', () => {
    const itemToSelect = items[2];
    service.registerItems(itemsList);
    spyOn(service.selection, 'select');
    service.selectItem(itemToSelect);
    expect(service.selection.select).toHaveBeenCalledWith(itemToSelect);
  });

  it('KeyCodesEnum.arrowUp event should be handled by selection.selectPrev', () => {
    service.registerItems(itemsList);
    spyOn(service.selection, 'selectPrev');
    keySubject.next(createKeyboardEvent(KeyCodesEnum.arrowUp));
    expect(service.selection.selectPrev).toHaveBeenCalled();
  });

  it('KeyCodesEnum.arrowDown event should be handled by selection.selectNext', () => {
    service.registerItems(itemsList);
    spyOn(service.selection, 'selectNext');
    keySubject.next(createKeyboardEvent(KeyCodesEnum.arrowDown));
    expect(service.selection.selectNext).toHaveBeenCalled();
  });

  it('KeyCodesEnum.home event should be handled by selection.selectFirst', () => {
    service.registerItems(itemsList);
    spyOn(service.selection, 'selectFirst');
    keySubject.next(createKeyboardEvent(KeyCodesEnum.home));
    expect(service.selection.selectFirst).toHaveBeenCalled();
  });

  it('KeyCodesEnum.end event should be handled by selection.selectLast', () => {
    service.registerItems(itemsList);
    spyOn(service.selection, 'selectLast');
    keySubject.next(createKeyboardEvent(KeyCodesEnum.end));
    expect(service.selection.selectLast).toHaveBeenCalled();
  });

  it('KeyCodesEnum.pageUp event should be handled by selection.selectPrev', () => {
    service.registerItems(itemsList);
    spyOn(service.selection, 'selectPrev');
    keySubject.next(createKeyboardEvent(KeyCodesEnum.pageUp));
    expect(service.selection.selectPrev).toHaveBeenCalledWith(DropdownMenuA11yService.pageOffset);
  });

  it('KeyCodesEnum.pageDown event should be handled by selection.selectNext', () => {
    service.registerItems(itemsList);
    spyOn(service.selection, 'selectNext');
    keySubject.next(createKeyboardEvent(KeyCodesEnum.pageDown));
    expect(service.selection.selectNext).toHaveBeenCalledWith(DropdownMenuA11yService.pageOffset);
  });

  it('KeyCodesEnum.Enter event should emit click event on selected item', () => {
    const itemToSelect = items[1];
    const event = createKeyboardEvent(KeyCodesEnum.Enter);
    service.registerItems(itemsList);
    service.selectItem(itemToSelect);
    keySubject.next(event);
    expect(itemToSelect.click.emit).toHaveBeenCalledWith(event);
  });

  it('KeyCodesEnum.Space event should emit click event on selected item', () => {
    const itemToSelect = items[0];
    const event = createKeyboardEvent(KeyCodesEnum.Space);
    service.registerItems(itemsList);
    service.selectItem(itemToSelect);
    keySubject.next(event);
    expect(itemToSelect.click.emit).toHaveBeenCalledWith(event);
  });

  it('KeyCodesEnum.Enter event should not call emit click event on not selected items', () => {
    const event = createKeyboardEvent(KeyCodesEnum.Enter);
    service.registerItems(itemsList);
    keySubject.next(event);
    itemsList.forEach((item: SelectableItemInterface) => {
      expect(item.click.emit).not.toHaveBeenCalled();
    });
  });

  it('AlphaNumeric events should be buffered in selection.selectByQuery', fakeAsync(() => {
    const eventT = createKeyboardEvent(KeyCodesEnum.keyT, 't');
    const eventE = createKeyboardEvent(KeyCodesEnum.keyE, 'e');
    const eventS = createKeyboardEvent(KeyCodesEnum.keyS, 's');

    const expectedResult = 'test';

    service.registerItems(itemsList);
    spyOn(service.selection, 'selectByQuery');

    keySubject.next(eventT);
    tick(DropdownMenuA11yService.searchDebounceTime / 2);
    keySubject.next(eventE);
    tick(DropdownMenuA11yService.searchDebounceTime / 2);
    keySubject.next(eventS);
    tick(DropdownMenuA11yService.searchDebounceTime / 2);
    keySubject.next(eventT);
    tick(DropdownMenuA11yService.searchDebounceTime * 2);

    expect(service.selection.selectByQuery).toHaveBeenCalledWith(expectedResult);
  }));

  it('ngOnDestroy should remove keyboard subscriptions', () => {
    service.registerItems(itemsList);
    expect(keySubject.observers.length).toEqual(3);
    service.ngOnDestroy();
    expect(keySubject.observers.length).toEqual(0);
  });

  it('ngOnDestroy should dispose selection model', () => {
    service.registerItems(itemsList);
    const selectionModel = service.selection;
    spyOn(selectionModel, 'dispose');
    service.ngOnDestroy();
    expect(selectionModel.dispose).toHaveBeenCalled();
  });
});
