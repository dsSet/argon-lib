import size from 'lodash/size';
import _map from 'lodash/map';
import join from 'lodash/join';
import { ElementRef, Injectable, OnDestroy, QueryList } from '@angular/core';
import { Subscription } from 'rxjs';
import { NAVIGATION_KEYS, ALPHANUMERIC_KEYS, KeyboardService, KeyCodesEnum } from '@argon/keyboard';
import { SelectableItemInterface } from '../interfaces/SelectableItemInterface';
import { MenuSelectionModel } from '../models/MenuSelectionModel';
import { buffer, debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Injectable()
export class DropdownMenuA11yService implements OnDestroy {

  static pageOffset = 10;

  static searchDebounceTime = 250;

  public selection = new MenuSelectionModel();

  private navigationSubscription: Subscription;

  private searchSubscription: Subscription;

  constructor(
    private keyboardService: KeyboardService
  ) { }

  ngOnDestroy(): void {
    this.removeKeyboardSubscriptions();
    this.selection.dispose();
  }

  public registerItems(items: QueryList<SelectableItemInterface>, element?: ElementRef<any>) {
    this.removeKeyboardSubscriptions();
    this.selection.registerList(items);
    this.registerEvents(element);
  }

  public selectItem(item: SelectableItemInterface) {
    this.selection.select(item);
  }

  private removeKeyboardSubscriptions() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
      this.navigationSubscription = null;
    }

    if (this.searchSubscription) {
      this.searchSubscription.unsubscribe();
      this.searchSubscription = null;
    }
  }

  private handleNavigation = (event: KeyboardEvent) => {
    switch (event.code) {
      case KeyCodesEnum.arrowUp:
        this.selection.selectPrev();
        break;
      case KeyCodesEnum.arrowDown:
        this.selection.selectNext();
        break;
      case KeyCodesEnum.home:
        this.selection.selectFirst();
        break;
      case KeyCodesEnum.end:
        this.selection.selectLast();
        break;
      case KeyCodesEnum.pageUp:
        this.selection.selectPrev(DropdownMenuA11yService.pageOffset);
        break;
      case KeyCodesEnum.pageDown:
        this.selection.selectNext(DropdownMenuA11yService.pageOffset);
        break;
      case KeyCodesEnum.Enter:
      case KeyCodesEnum.Space:
        if (this.selection.selected) {
          this.selection.selected.click.emit(event);
        }
        break;
    }
  }

  private handleSearch = (query: string) => {
    this.selection.selectByQuery(query);
  }

  private registerEvents(element?: ElementRef<any>) {
    const nativeElement = element ? element.nativeElement : null;
    this.navigationSubscription = this.keyboardService
      .listenKeyDownEvent([...NAVIGATION_KEYS, KeyCodesEnum.Enter, KeyCodesEnum.Space], nativeElement)
      .subscribe(this.handleNavigation);
    const searchObserver = this.keyboardService.listenKeyDownEvent(ALPHANUMERIC_KEYS, nativeElement);

    this.searchSubscription = searchObserver.pipe(
        buffer(
          searchObserver.pipe(
            debounceTime(DropdownMenuA11yService.searchDebounceTime),
            distinctUntilChanged()
          )
        ),
        map(this.createQueryString)
      ).subscribe(this.handleSearch);
  }

  private createQueryString = (events: Array<KeyboardEvent>): string => {
    return join(_map(events, 'key'), '');
  }
}
