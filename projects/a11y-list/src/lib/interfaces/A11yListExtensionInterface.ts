import { OnDestroy, OnInit, QueryList } from '@angular/core';
import { ActiveDescendantKeyManager, Highlightable } from '@angular/cdk/a11y';
import { KeyboardService } from '@argon/keyboard';
import { UIClickableInterface } from '@argon/tools';

export interface A11yListExtensionInterface extends OnInit, OnDestroy {

  keyManager: ActiveDescendantKeyManager<UIClickableInterface>;

  keyboardService: KeyboardService;

  connect(
    items: QueryList<Highlightable & UIClickableInterface> | Array<Highlightable & UIClickableInterface>, element?: HTMLElement
  ): void;

  filterEvent(event: KeyboardEvent): boolean;

  handleNavigate(event: KeyboardEvent): void;

  handleSelect(event: KeyboardEvent): void;

  removeSelection(): void;

}
