import { ChangeDetectorRef, EventEmitter } from '@angular/core';


export interface SelectableItemInterface {

  selected: boolean;

  disabled: boolean;

  changeDetector: ChangeDetectorRef;

  click: EventEmitter<Event>;

  search(query: string): boolean;

}
