import { Injectable, OnDestroy, OnInit, QueryList } from '@angular/core';
import { ActiveDescendantKeyManager, Highlightable } from '@angular/cdk/a11y';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Constructor, HasInjectorInterface, HasLifecircleHooksInterface, Unsubscribable, UIClickableInterface } from '@argon/tools';
import { KeyboardService, NAVIGATION_KEYS, KeyCodesEnum } from '@argon/keyboard';
import { A11yListExtensionInterface } from '../interfaces/A11yListExtensionInterface';


export function A11yListMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<A11yListExtensionInterface> {

  @Injectable()
  class A11yListExtension extends BaseClass implements A11yListExtensionInterface, OnInit, OnDestroy {

    public keyManager: ActiveDescendantKeyManager<UIClickableInterface>;

    public keyboardService: KeyboardService;

    private navigateSubscription: Subscription;

    private selectSubscription: Subscription;

    ngOnInit(): void {
      if (super.ngOnInit) {
        super.ngOnInit();
      }
      this.keyboardService = this.injector.get(KeyboardService);
    }

    @Unsubscribable
    ngOnDestroy(): void {
      if (super.ngOnDestroy) {
        super.ngOnDestroy();
      }
    }

    public connect(
      items: QueryList<Highlightable & UIClickableInterface> | Array<Highlightable & UIClickableInterface>, element?: HTMLElement
    ) {
      this.keyManager = new ActiveDescendantKeyManager(items).withWrap().withTypeAhead();
      this.registerEvents(element);
    }

    public filterEvent(event: KeyboardEvent): boolean {
      return true;
    }

    public handleNavigate(event: KeyboardEvent) {
      this.keyManager.onKeydown(event);
    }

    public handleSelect(event: KeyboardEvent) {
      if (this.keyManager.activeItem) {
        this.keyManager.activeItem.click.emit();
      }
    }

    public removeSelection() {
      this.keyManager.setActiveItem(-1);
    }

    private registerEvents(element?: HTMLElement) {
      this.navigateSubscription = this.keyboardService.listenKeyDownEvent(NAVIGATION_KEYS, element)
        .pipe(filter((event: KeyboardEvent) => this.filterEvent(event)))
        .subscribe((event: KeyboardEvent) => { this.handleNavigate(event); });

      this.selectSubscription = this.keyboardService.listenKeyDownEvent([KeyCodesEnum.Enter, KeyCodesEnum.Space], element)
        .pipe(filter((event: KeyboardEvent) => this.filterEvent(event)))
        .subscribe((event: KeyboardEvent) => { this.handleSelect(event); });
    }

  }

  return A11yListExtension;
}
