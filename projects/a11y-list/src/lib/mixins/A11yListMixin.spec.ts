import { A11yListMixin } from './A11yListMixin';
import { BaseShape } from '@argon/tools';
import { Component, EventEmitter, QueryList } from '@angular/core';
import { KeyboardServiceMock } from '../../../../../test/KeyboardServiceMock';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { KeyboardService, KeyCodesEnum, NAVIGATION_KEYS } from '@argon/keyboard';
import { ActiveDescendantKeyManager, Highlightable } from '@angular/cdk/a11y';
import { UIClickableInterface } from '@argon/tools';


describe('A11yListMixin', () => {

  @Component({ selector: 'spec-component', template: ''})
  class MockComponent extends A11yListMixin(BaseShape) {

  }

  class MockItem implements Highlightable, UIClickableInterface {
    click = new EventEmitter<void>();
    disabled: boolean;

    getLabel(): string {
      return '';
    }

    setActiveStyles(): void {
    }

    setInactiveStyles(): void {
    }
  }

  let keyboardService: KeyboardServiceMock;
  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      imports: [ ],
      declarations: [
        MockComponent
      ],
      providers: [
        { provide: KeyboardService, useClass: KeyboardServiceMock }
      ],
    }).compileComponents()
  ));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
    keyboardService = TestBed.inject(KeyboardService) as any;
  });

  it('should have keyboard service', () => {
    fixture.detectChanges();
    expect(component.keyboardService).toEqual(keyboardService as any);
  });

  it('connect should register navigation keyboard events', () => {
    fixture.detectChanges();
    const items: QueryList<Highlightable & UIClickableInterface> = new QueryList();
    items.reset([new MockItem()]);
    const element = document.createElement('div');
    component.connect(items, element);
    expect(keyboardService.listenKeyDownEvent).toHaveBeenCalledWith(NAVIGATION_KEYS, element);
    expect(keyboardService.listenKeyDownEvent).toHaveBeenCalledWith([KeyCodesEnum.Enter, KeyCodesEnum.Space], element);
  });

  it('connect should create keyManager', () => {
    fixture.detectChanges();
    const items: QueryList<Highlightable & UIClickableInterface> = new QueryList();
    items.reset([new MockItem()]);
    component.connect(items);
    expect(component.keyManager instanceof ActiveDescendantKeyManager).toBeTruthy();
  });

  it('filterEvent should return true by default', () => {
    const event = new KeyboardEvent('keydown');
    expect(component.filterEvent(event)).toEqual(true);
  });

  it('handleNavigate should call onKeydown event on key manager', () => {
    component.keyManager = new ActiveDescendantKeyManager([]);
    spyOn(component.keyManager, 'onKeydown');
    const event = new KeyboardEvent('keydown');
    component.handleNavigate(event);
    expect(component.keyManager.onKeydown).toHaveBeenCalledWith(event);
  });

  it('handleSelect should emit click on active item', () => {
    const item = new MockItem();
    const spy = spyOn(item.click, 'emit');
    component.keyManager = new ActiveDescendantKeyManager<UIClickableInterface>([item]);
    component.keyManager.setFirstItemActive();
    const event = new KeyboardEvent('keydown');
    component.handleSelect(event);
    expect(item.click.emit).toHaveBeenCalled();
    spy.calls.reset();
    component.keyManager.setActiveItem(-1);
    component.handleSelect(event);
    expect(item.click.emit).not.toHaveBeenCalled();
  });

  it('removeSelection should remove active item from keyManager', () => {
    const item = new MockItem();
    component.keyManager = new ActiveDescendantKeyManager<UIClickableInterface>([item]);
    component.keyManager.setFirstItemActive();
    expect(component.keyManager.activeItem).toEqual(item);
    component.removeSelection();
    expect(component.keyManager.activeItem).toEqual(null);
  });

  it('navigation keys should call handleNavigate method', () => {
    fixture.detectChanges();
    const event = new KeyboardEvent('keydown', { code: KeyCodesEnum.arrowDown });
    component.connect([]);
    spyOn(component, 'handleNavigate');
    spyOn(component, 'filterEvent').and.returnValue(true);
    keyboardService.listenKeyDownEventSubject.next(event);
    expect(component.handleNavigate).toHaveBeenCalledWith(event);
    expect(component.filterEvent).toHaveBeenCalledWith(event);
  });

  it('Space and Enter keys should call handleSelect method', () => {
    fixture.detectChanges();
    const event = new KeyboardEvent('keydown', { code: KeyCodesEnum.Enter });
    component.connect([]);
    spyOn(component, 'handleSelect');
    spyOn(component, 'filterEvent').and.returnValue(true);
    keyboardService.listenKeyDownEventSubject.next(event);
    expect(component.handleSelect).toHaveBeenCalledWith(event);
    expect(component.filterEvent).toHaveBeenCalledWith(event);
  });
});
