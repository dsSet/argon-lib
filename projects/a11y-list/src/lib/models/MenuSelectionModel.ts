import findIndex from 'lodash/findIndex';
import has from 'lodash/has';
import first from 'lodash/first';
import last from 'lodash/last';
import size from 'lodash/size';
import find from 'lodash/find';
import { QueryList } from '@angular/core';
import { SelectableItemInterface } from '../interfaces/SelectableItemInterface';
import { BehaviorSubject } from 'rxjs';

export class MenuSelectionModel {

  public get selected(): SelectableItemInterface {
    return this.selectedItem.value;
  }

  public get items(): Array<SelectableItemInterface> {
    return this.list.toArray();
  }

  public selectedItem = new BehaviorSubject<SelectableItemInterface>(null);

  constructor(
    private list: QueryList<SelectableItemInterface> = new QueryList<SelectableItemInterface>()
  ) {
    this.selectedItem.subscribe(this.handleUpdateIndex);
  }

  public registerList(list: QueryList<SelectableItemInterface>) {
    this.list = list;
    this.selectedItem.next(null);
  }

  public dispose() {
    this.selectedItem.complete();
  }

  public selectNext(offset = 1) {
    const selectableList = this.getSelectableItems();
    const selectedIndex = this.selectedItem.value ? findIndex(selectableList, this.selectedItem.value) + offset : 0;
    if (!has(selectableList, selectedIndex)) {
      this.selectedItem.next(first(selectableList) || null);
    } else {
      this.selectedItem.next(selectableList[selectedIndex]);
    }
  }

  public selectPrev(offset = 1) {
    const selectableList = this.getSelectableItems();
    const selectedIndex = this.selectedItem.value ? findIndex(selectableList, this.selectedItem.value) - offset : size(selectableList) - 1;
    if (!has(selectableList, selectedIndex)) {
      this.selectedItem.next(last(selectableList) || null);
    } else {
      this.selectedItem.next(selectableList[selectedIndex]);
    }
  }

  public selectFirst() {
    const selectableList = this.getSelectableItems();
    this.selectedItem.next(first(selectableList) || null);
  }

  public selectLast() {
    const selectableList = this.getSelectableItems();
    this.selectedItem.next(last(selectableList) || null);
  }

  public select(item: SelectableItemInterface) {
    this.selectedItem.next(item);
  }

  public selectByQuery(query: string) {
    const selectableList = this.getSelectableItems();
    const itemToSelect = find(selectableList, (item: SelectableItemInterface) => item.search(query));
    if (itemToSelect) {
      this.select(itemToSelect);
    }
  }

  public clear() {
    this.selectedItem.next(null);
  }

  private handleUpdateIndex = () => {
    this.list.forEach(this.updateSelection);
  }

  private updateSelection = (item: SelectableItemInterface) => {
    item.selected = item === this.selectedItem.value;
    item.changeDetector.markForCheck();
  }

  private filterSelectableItem = (item: SelectableItemInterface) => !item.disabled;

  private getSelectableItems = () => this.list.filter(this.filterSelectableItem);
}
