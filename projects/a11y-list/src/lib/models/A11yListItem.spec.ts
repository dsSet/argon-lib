import { A11yListItem } from './A11yListItem';
import { TestBed, waitForAsync } from '@angular/core/testing';

describe('A11yListItem', () => {

  let item: A11yListItem;

  beforeEach(waitForAsync(() =>
    TestBed.configureTestingModule({
      providers: [
        A11yListItem
      ],
    }).compileComponents()
  ));

  beforeEach(() => {
    item = TestBed.inject(A11yListItem) as any;
  });

  it('setActiveStyles should set active property to true', () => {
    spyOn(item, 'handleStateChange');
    item.active = false;
    item.setActiveStyles();
    expect(item.active).toEqual(true);
    expect(item.handleStateChange).toHaveBeenCalled();
  });

  it('setInactiveStyles should set active property to false', () => {
    spyOn(item, 'handleStateChange');
    item.active = true;
    item.setInactiveStyles();
    expect(item.active).toEqual(false);
    expect(item.handleStateChange).toHaveBeenCalled();
  });

  it('getLabel should return empty string by default', () => {
    expect(item.getLabel()).toEqual('');
  });

});
