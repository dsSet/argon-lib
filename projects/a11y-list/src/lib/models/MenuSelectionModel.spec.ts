import { SelectableItemInterface } from '../interfaces/SelectableItemInterface';
import { MenuSelectionModel } from './MenuSelectionModel';
import { QueryList } from '@angular/core';

const createItem = (
  id,
  disabled = false,
  selected = false,
  search = jasmine.createSpy('search').and.returnValue(false)
): SelectableItemInterface & { id: number } => ({
  id,
  click: jasmine.createSpyObj('EventEmitter', ['emit']),
  changeDetector: jasmine.createSpyObj('ChangeDetectorRef', ['markForCheck']),
  disabled,
  selected,
  search,
});

const createQueryList = (items: Array<SelectableItemInterface>): QueryList<SelectableItemInterface> => {
  const itemsList = new QueryList<SelectableItemInterface>();
  itemsList.reset(items);
  return itemsList;
}

describe('MenuSelectionModel', () => {
  let model: MenuSelectionModel;
  let items: Array<SelectableItemInterface>;
  let itemsList: QueryList<SelectableItemInterface>;

  beforeEach(() => {
    items = [
      createItem(0, true),
      createItem(1),
      createItem(2, true),
      createItem(3),
      createItem(4),
      createItem(5),
      createItem(6),
      createItem(7, true),
      createItem(8)
    ];
    itemsList = createQueryList(items);
    model = new MenuSelectionModel(itemsList);
  });

  it('should initialize selectedItem property in constructor', () => {
    expect(model.selectedItem.value).toEqual(null);
    expect(model.selectedItem.observers.length).toEqual(1);
  });

  it('should initialize items in ctor', () => {
    expect(model.items).toEqual(items);

    const items2 = [
      createItem(11),
      createItem(12)
    ];
    const model2 = new MenuSelectionModel(createQueryList(items2));
    expect(model2.items).toEqual(items2);

    const model3 = new MenuSelectionModel();
    expect(model3.items).toEqual([]);
  });

  it('registerList should update items list', () => {
    expect(model.items).toEqual(items);
    const nextItems = [createItem(11)];
    model.registerList(createQueryList(nextItems));
    expect(model.items).toEqual(nextItems);
  });

  it('registerList should remove selection', () => {
    model.selectedItem.next(createItem(11));
    model.registerList(itemsList);
    expect(model.selectedItem.value).toEqual(null);
  });

  it('selected property should return selected item', () => {
    const item = createItem(11, false, true);
    expect(model.selected).toEqual(null);
    model.selectedItem.next(item);
    expect(model.selected).toEqual(item);
  });

  it('dispose should complete selectedItem', () => {
    expect(model.selectedItem.isStopped).toEqual(false);
    model.dispose();
    expect(model.selectedItem.isStopped).toEqual(true);
  });

  it('selectNext should select first item if no selection', () => {
    model.selectNext();
    expect(model.selected).toEqual(items[1]);
  });

  it('selectNext should select next selectable item', () => {
    model.select(items[1]);
    model.selectNext();
    expect(model.selected).toEqual(items[3]);
  });

  it('selectNext should select first item if last item in list is selected', () => {
    model.select(items[8]);
    model.selectNext();
    expect(model.selected).toEqual(items[1]);
  });

  it('selectNext should move selection by offset', () => {
    model.select(items[1]);
    model.selectNext(3);
    expect(model.selected).toEqual(items[5]);
  });

  it('selectNext remove selection without selected items', () => {
    const nextItems = [
      createItem(11, true),
      createItem(12, true),
    ];
    model.selectedItem.next(nextItems[1]);
    model.registerList(createQueryList(nextItems));
    model.selectNext();
    expect(model.selected).toEqual(null);
  });

  it('selectPrev should select last item if no selection', () => {
    model.selectPrev();
    expect(model.selected).toEqual(items[8]);
  });

  it('selectPrev should select prev selectable item', () => {
    model.select(items[8]);
    model.selectPrev();
    expect(model.selected).toEqual(items[6]);
  });

  it('selectPrev should select last item if first item in list is selected', () => {
    model.select(items[1]);
    model.selectPrev();
    expect(model.selected).toEqual(items[8]);
  });

  it('selectPrev should move selection by offset', () => {
    model.select(items[5]);
    model.selectPrev(3);
    expect(model.selected).toEqual(items[1]);
  });

  it('selectPrev remove selection without selected items', () => {
    const nextItems = [
      createItem(11, true),
      createItem(12, true),
    ];
    model.selectedItem.next(nextItems[1]);
    model.registerList(createQueryList(nextItems));
    model.selectPrev();
    expect(model.selected).toEqual(null);
  });

  it('selectFirst should select first selectable item', () => {
    expect(model.selected).toEqual(null);
    model.selectFirst();
    expect(model.selected).toEqual(items[1]);
  });

  it('selectFirst should remove selection without selectable items', () => {
    const nextItems = [
      createItem(11, true),
      createItem(12, true),
    ];
    model.selectedItem.next(nextItems[1]);
    model.registerList(createQueryList(nextItems));
    model.selectFirst();
    expect(model.selected).toEqual(null);
  });

  it('selectLast should select last selectable item', () => {
    expect(model.selected).toEqual(null);
    model.selectLast();
    expect(model.selected).toEqual(items[8]);
  });

  it('selectLast should remove selection without selectable items', () => {
    const nextItems = [
      createItem(11, true),
      createItem(12, true),
    ];
    model.selectedItem.next(nextItems[1]);
    model.registerList(createQueryList(nextItems));
    model.selectLast();
    expect(model.selected).toEqual(null);
  });

  it('select should update selected item', () => {
    const item = createItem(11);
    model.select(item);
    expect(model.selected).toEqual(item);
  });

  it('selectByQuery should select item by search fn', () => {
    const item1 = createItem(11);
    const item2 = createItem(12, false, false, jasmine.createSpy('search').and.returnValue(true));
    const item3 = createItem(13);
    const queryList = createQueryList([item1, item2, item3]);
    const queryString = 'Fake Query';

    model.registerList(queryList);
    model.selectByQuery(queryString);
    expect(model.selected).toEqual(item2);
    expect(item1.search).toHaveBeenCalledWith(queryString);
    expect(item2.search).toHaveBeenCalledWith(queryString);
    expect(item3.search).not.toHaveBeenCalled();
  });

  it('clear should remove selection', () => {
    model.select(items[1]);
    expect(model.selected).toEqual(items[1]);
    model.clear();
    expect(model.selected).toEqual(null);
  });

  it('selection should update items state', () => {
    const item1 = createItem(11);
    const item2 = createItem(12, false, true);
    const item3 = createItem(13);
    const queryList = createQueryList([item1, item2, item3]);
    model.registerList(queryList);

    model.select(item1);
    expect(item1.selected).toEqual(true);
    expect(item2.selected).toEqual(false);
    expect(item3.selected).toEqual(false);
    expect(item1.changeDetector.markForCheck).toHaveBeenCalled();
    expect(item2.changeDetector.markForCheck).toHaveBeenCalled();
    expect(item3.changeDetector.markForCheck).toHaveBeenCalled();
  });
});
