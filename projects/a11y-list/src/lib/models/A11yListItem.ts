import { UIClickableInterface, BaseShape } from '@argon/tools';
import { Highlightable } from '@angular/cdk/a11y';
import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class A11yListItem extends BaseShape implements Highlightable, UIClickableInterface {

  public click: EventEmitter<void>;

  public disabled: boolean;

  public active: boolean;

  setActiveStyles() {
    this.active = true;
    this.handleStateChange();
  }

  setInactiveStyles() {
    this.active = false;
    this.handleStateChange();
  }

  getLabel(): string {
    return '';
  }

  public handleStateChange() {

  }

}
