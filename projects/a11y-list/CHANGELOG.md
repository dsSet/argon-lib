# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@2.0.1...@argon/a11y-list@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/a11y-list





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@1.0.4...@argon/a11y-list@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/a11y-list






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@1.0.4...@argon/a11y-list@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/a11y-list






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@1.0.3...@argon/a11y-list@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/a11y-list





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@1.0.2...@argon/a11y-list@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/a11y-list





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@1.0.1...@argon/a11y-list@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/a11y-list






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.13...@argon/a11y-list@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/a11y-list





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.13...@argon/a11y-list@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/a11y-list






## [0.2.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.12...@argon/a11y-list@0.2.13) (2020-08-04)

**Note:** Version bump only for package @argon/a11y-list






## [0.2.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.11...@argon/a11y-list@0.2.12) (2020-05-21)

**Note:** Version bump only for package @argon/a11y-list






## [0.2.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.10...@argon/a11y-list@0.2.11) (2020-04-18)

**Note:** Version bump only for package @argon/a11y-list






## [0.2.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.9...@argon/a11y-list@0.2.10) (2020-03-24)

**Note:** Version bump only for package @argon/a11y-list





## [0.2.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.8...@argon/a11y-list@0.2.9) (2020-03-24)

**Note:** Version bump only for package @argon/a11y-list






## [0.2.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.7...@argon/a11y-list@0.2.8) (2020-03-18)

**Note:** Version bump only for package @argon/a11y-list





## [0.2.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.6...@argon/a11y-list@0.2.7) (2020-02-14)

**Note:** Version bump only for package @argon/a11y-list






## [0.2.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.5...@argon/a11y-list@0.2.6) (2019-11-29)


### Bug Fixes

* **a11y-list:** fix mixin interface ([d19bbaa](https://bitbucket.org/dsSet/argon-lib/commits/d19bbaa))





## [0.2.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.4...@argon/a11y-list@0.2.5) (2019-11-22)

**Note:** Version bump only for package @argon/a11y-list





## [0.2.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.3...@argon/a11y-list@0.2.4) (2019-11-22)

**Note:** Version bump only for package @argon/a11y-list





## [0.2.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.2...@argon/a11y-list@0.2.3) (2019-10-28)


### Bug Fixes

* **a11y-list:** fix A11yList mixin type definitions ([8c97828](https://bitbucket.org/dsSet/argon-lib/commits/8c97828))





## [0.2.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.1...@argon/a11y-list@0.2.2) (2019-10-11)

**Note:** Version bump only for package @argon/a11y-list





## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.2.0...@argon/a11y-list@0.2.1) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.1.7...@argon/a11y-list@0.2.0) (2019-10-08)


### Features

* **a11y-list:** add mixin for item selection ([ec247e0](https://bitbucket.org/dsSet/argon-lib/commits/ec247e0))





## [0.1.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.1.6...@argon/a11y-list@0.1.7) (2019-09-21)

**Note:** Version bump only for package @argon/a11y-list






## [0.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.1.5...@argon/a11y-list@0.1.6) (2019-09-07)

**Note:** Version bump only for package @argon/a11y-list





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.1.4...@argon/a11y-list@0.1.5) (2019-07-28)

**Note:** Version bump only for package @argon/a11y-list





## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.1.3...@argon/a11y-list@0.1.4) (2019-07-15)


### Bug Fixes

* **main:** revert incorrect imports ([d30bb89](https://bitbucket.org/dsSet/argon-lib/commits/d30bb89))





## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.1.2...@argon/a11y-list@0.1.3) (2019-07-14)

**Note:** Version bump only for package @argon/a11y-list





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.1.1...@argon/a11y-list@0.1.2) (2019-07-14)


### Bug Fixes

* **dropdown:** mock import ([884e049](https://bitbucket.org/dsSet/argon-lib/commits/884e049))





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/a11y-list@0.1.0...@argon/a11y-list@0.1.1) (2019-07-14)

**Note:** Version bump only for package @argon/a11y-list





# 0.1.0 (2019-07-14)


### Features

* **@argon/a11y-list:** add a11y-list package ([cb26479](https://bitbucket.org/dsSet/argon-lib/commits/cb26479))
* **a11y-list:** add keyboard search ([9de262d](https://bitbucket.org/dsSet/argon-lib/commits/9de262d))
* **a11y-list:** add navigation events ([40d14f0](https://bitbucket.org/dsSet/argon-lib/commits/40d14f0))
