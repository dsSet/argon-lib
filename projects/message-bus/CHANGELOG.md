# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@2.0.1...@argon/message-bus@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/message-bus





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@1.0.4...@argon/message-bus@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/message-bus






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@1.0.4...@argon/message-bus@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/message-bus






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@1.0.3...@argon/message-bus@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/message-bus





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@1.0.2...@argon/message-bus@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/message-bus





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@1.0.1...@argon/message-bus@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/message-bus






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.13...@argon/message-bus@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/message-bus





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.13...@argon/message-bus@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/message-bus






## [0.1.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.12...@argon/message-bus@0.1.13) (2020-08-04)

**Note:** Version bump only for package @argon/message-bus






## [0.1.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.11...@argon/message-bus@0.1.12) (2020-05-21)

**Note:** Version bump only for package @argon/message-bus






## [0.1.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.10...@argon/message-bus@0.1.11) (2020-04-18)

**Note:** Version bump only for package @argon/message-bus






## [0.1.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.9...@argon/message-bus@0.1.10) (2020-03-24)

**Note:** Version bump only for package @argon/message-bus





## [0.1.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.8...@argon/message-bus@0.1.9) (2020-03-24)

**Note:** Version bump only for package @argon/message-bus






## [0.1.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.7...@argon/message-bus@0.1.8) (2020-03-18)

**Note:** Version bump only for package @argon/message-bus





## [0.1.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.6...@argon/message-bus@0.1.7) (2020-02-14)

**Note:** Version bump only for package @argon/message-bus






## [0.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.5...@argon/message-bus@0.1.6) (2019-11-29)


### Bug Fixes

* **message-bus:** fix extension interface ([79ff8c8](https://bitbucket.org/dsSet/argon-lib/commits/79ff8c8))





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.4...@argon/message-bus@0.1.5) (2019-11-22)

**Note:** Version bump only for package @argon/message-bus





## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.3...@argon/message-bus@0.1.4) (2019-11-22)

**Note:** Version bump only for package @argon/message-bus





## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.2...@argon/message-bus@0.1.3) (2019-10-28)

**Note:** Version bump only for package @argon/message-bus





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.1...@argon/message-bus@0.1.2) (2019-10-11)

**Note:** Version bump only for package @argon/message-bus





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/message-bus@0.1.0...@argon/message-bus@0.1.1) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





# 0.1.0 (2019-10-08)


### Bug Fixes

* **message-bus:** fix dependencies ([e5a03aa](https://bitbucket.org/dsSet/argon-lib/commits/e5a03aa))


### Features

* **message-bus:** add message-bus package ([6accdd7](https://bitbucket.org/dsSet/argon-lib/commits/6accdd7))
* **tools:** restructoring folders, implement state subject ([079cefb](https://bitbucket.org/dsSet/argon-lib/commits/079cefb))
