#Message Bus

##@Message decorator

This decorator can be used for define class as Message.

```typescript
import { Message } from '../dist';

@Message('MyMessage')
export class MyMessage {

  public customData = 'message staff';

  constructor(
    public data: string
  ) { }

}

```

Arguments:

>Signature: ``@Message(messageKey: string)``

``messageKey`` - uniq key for binding messages


##@MessageBinding decorator

Use to bind message to a class method.

```typescript
import { Injectable, OnDestroy } from '@angular/core';
import { MessageBinding, MessageBusService } from '../dist';

@Injectable()
export class MessageListenerService implements OnDestroy {

  constructor(
    private messageBusService: MessageBusService
  ) {
    this.messageBusService.connect(this);
  }

  ngOnDestroy(): void {
    this.messageBusService.disconnect(this);
  }

  @MessageBinding(MyMessage)
  public handleMessage(message: MyMessage) {
    alert(`MessageListenerService handle message with data ${message.data}`);
  }

}
```

Arguments:

>Signature: ``@MessageBinding(message: Constructor<any>, useHistory: boolean = false)``

``message`` - message constructor which was decorated with @Message.
``useHistory`` - allow to handle last ``message`` from the bus on method binding.

This service use ``MessageBusService`` to connect to the message bus.
``connect`` method is connect service instance to the message bus.
``disconnect`` method remove object bindings from the message bus

> ``disconnect`` method should be call mandatory to prevent memory leaks 

##MessageBusService

```typescript
import { OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Constructor } from '../dist';

export declare class MessageBusService implements OnDestroy {

    /**
     * Add new subscription to the message bus. Subscriptions will handle only messages by type MessageType.
     */
    of(MessageType: Constructor<any>): Observable<any>;

    /**
     * Send message to the message bus
     */
    send(message: any): void;

    /**
     * Get message from the message bus. Service will keep the lates message for each sent message type
     */
    getFromHistory(message: any): any;

    /**
     * Connect object to the message bus. This method will parse all bindings (see MessageBinding decorator)
     */
    connect(object: any): void;

    /**
     * Disconnect object from the message bus
     */
    disconnect(object: any): void;
}

```

##MessageBusMixin

Contains boilerplate code for working with MessageBusService.

Usage:

````typescript
import { Component } from '@angular/core';
import { BaseShape, MessageBinding, MessageBusMixin } from '../dist';

@Component({
  selector: 'listener-component',
  template: `Template))`
})
export class ListenerComponent extends MessageBusMixin(BaseShape) {
  
  @MessageBinding(MyMessage)
  handleMessage(message: MyMessage) {
      // Do something
  }
  
}

````

This component will be connected to the message bus. Method ``handleMessage`` will handle each ``MyMessage``.

> This mixin will ``connect`` and ``disconnect`` object automatically.
``connect`` will be called inside ``ngOnInit`` hook
``disconnect`` - inside ``ngOnDestroy``

``MessageBusMixin`` also populate base class with ``MessageBusExtensionInterface`` interface:

````typescript
import { MessageBusService } from '../dist';

export interface MessageBusExtensionInterface {

  messageBus: MessageBusService;

  sendMessage(message: any): void;

}
````

Mixin can wrap any class implemented ``HasInjectorInterface`` interface:

````typescript
import { Injector } from '@angular/core';

export interface HasInjectorInterface {
  
  injector: Injector;
  
}
````

Mixin Signature:

````typescript

function MessageBusMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<MessageBusExtensionInterface>;

````
