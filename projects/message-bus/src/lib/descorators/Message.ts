import 'reflect-metadata';
import { Constructor } from '@argon/tools';
import { MessageBindingDataInterface } from '../interfaces/MessageBindingDataInterface';
import { Subscription } from 'rxjs';

export const MESSAGE_METADATA_KEY = 'argon:design:message';
export const MESSAGE_BUS_SUBSCRIPTIONS_METADATA_KEY = 'argon:design:bus-subscriptions';

/**
 * Define message key for message bus.
 */
export function Message(messageKey: string) {
  return (target: Constructor<any>) => {
    Reflect.defineMetadata(MESSAGE_METADATA_KEY, messageKey, target);
  };
}

/**
 * Bind Message to method property.
 */
export function MessageBinding(message: Constructor<any>, useHistory: boolean = false) {
  return (target: any, propertyKey: string) => {
    const data: MessageBindingDataInterface = {
      message,
      useHistory,
      propertyKey
    };

    const bindingData = getMessageBindingData(target);

    Reflect.defineMetadata(MESSAGE_METADATA_KEY, [...bindingData, data], target);
  };
}

/**
 * Get messageKey from Message object
 */
export function getMessageKey(object: any): string {
  const key = Reflect.getMetadata(MESSAGE_METADATA_KEY, object);
  const ctorKey = Reflect.getMetadata(MESSAGE_METADATA_KEY, object.constructor);
  if (!key && !ctorKey) {
    throw new Error(`Message key not found. Did you forgot to apply @Message() decorator?`);
  }
  return key || ctorKey;
}

/**
 * Get Message Binding data from object
 */
export function getMessageBindingData(object: any): Array<MessageBindingDataInterface> {
  const data = Reflect.getMetadata(MESSAGE_METADATA_KEY, object);
  return data || [];
}

/**
 * Get set of message bus subscriptions
 */
export function getObjectSubscriptions(object: any): Set<Subscription> {
  if (!Reflect.hasMetadata(MESSAGE_BUS_SUBSCRIPTIONS_METADATA_KEY, object)) {
    throw new Error('Object has no message bus subscriptions');
  }

  return Reflect.getMetadata(MESSAGE_BUS_SUBSCRIPTIONS_METADATA_KEY, object);
}

/**
 * Store message bus subscriptions inside object metadata
 */
export function setObjectSubscriptions(object: any, subscriptions: Set<Subscription>) {
  if (Reflect.hasMetadata(MESSAGE_BUS_SUBSCRIPTIONS_METADATA_KEY, object)) {
    throw new Error('Object already connected to the message bus');
  }

  Reflect.defineMetadata(MESSAGE_BUS_SUBSCRIPTIONS_METADATA_KEY, subscriptions, object);
}

export function removeObjectSubscriptions(object: any): void {
  if (!Reflect.hasMetadata(MESSAGE_BUS_SUBSCRIPTIONS_METADATA_KEY, object)) {
    throw new Error('Object has no message bus subscriptions');
  }

  Reflect.deleteMetadata(MESSAGE_BUS_SUBSCRIPTIONS_METADATA_KEY, object);
}
