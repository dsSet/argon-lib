import {
  getMessageBindingData,
  getMessageKey,
  getObjectSubscriptions,
  Message,
  MessageBinding,
  removeObjectSubscriptions,
  setObjectSubscriptions
} from './Message';
import { MessageBindingDataInterface } from '../interfaces/MessageBindingDataInterface';
import { Subscription } from 'rxjs';

describe('Message', () => {

  it('@Message decorator should populate object with message key', () => {
    const key = 'FakeMessage';
    @Message(key)
    class MockClass { }
    expect(getMessageKey(MockClass)).toEqual(key);
  });

  it('getMessageKey should return message key from instance', () => {
    const key = 'FakeMessage';
    @Message(key)
    class MockClass { }

    const object = new MockClass();

    expect(getMessageKey(object)).toEqual(key);
  });

  it('getMessageKey should throw error if target has no messageKey', () => {
    const object = { data: 'MockObject' };
    expect(() => { getMessageKey(object); }).toThrow(new Error('Message key not found. Did you forgot to apply @Message() decorator?'));
  });

  it('MessageBinding should store message metadata', () => {
    @Message('MockMessage1') class MockMessage1 { }
    @Message('MockMessage2') class MockMessage2 { }

    class MockListener {
      @MessageBinding(MockMessage1) handleMessage1() { }
      @MessageBinding(MockMessage2, true) handleMessage2() { }
    }

    const expectedResult: Array<MessageBindingDataInterface> = [
      {
        message: MockMessage1,
        propertyKey: 'handleMessage1',
        useHistory: false
      },
      {
        message: MockMessage2,
        propertyKey: 'handleMessage2',
        useHistory: true
      }
    ];

    const instance = new MockListener();

    expect(getMessageBindingData(instance)).toEqual(expectedResult);

  });

  it('getMessageBindingData should return empty array if target has no message metadata', () => {
    expect(getMessageBindingData({ })).toEqual([]);
  });

  it('getObjectSubscriptions should throw error if target has no subscription metadata', () => {
    expect(() => { getObjectSubscriptions({ }); }).toThrow(new Error('Object has no message bus subscriptions'));
  });

  it('getObjectSubscriptions should return subscriptions Set from the target metadata', () => {
    const object = { data: 'Mock Object' };
    const subscriptions = new Set([new Subscription()]);
    setObjectSubscriptions(object, subscriptions);
    expect(getObjectSubscriptions(object)).toEqual(subscriptions);
  });

  it('setObjectSubscriptions should throw error if object already has subscriptions metadata', () => {
    const object = { data: 'Mock Object' };
    const subscriptions1 = new Set([new Subscription()]);
    const subscriptions2 = new Set([new Subscription()]);
    setObjectSubscriptions(object, subscriptions1);
    expect(() => { setObjectSubscriptions(object, subscriptions2); }).toThrow(new Error('Object already connected to the message bus'));
  });

  it('removeObjectSubscriptions should remove subscriptions from target', () => {
    const object = { data: 'Mock Object' };
    const subscriptions = new Set([new Subscription()]);
    setObjectSubscriptions(object, subscriptions);
    removeObjectSubscriptions(object);
    expect(() => { getObjectSubscriptions({ }); }).toThrow(new Error('Object has no message bus subscriptions'));
  });

  it('removeObjectSubscriptions should throw error if object has no subscriptions', () => {
    const object = { data: 'Mock Object' };
    expect(() => { removeObjectSubscriptions(object); }).toThrow(new Error('Object has no message bus subscriptions'));
  });
});
