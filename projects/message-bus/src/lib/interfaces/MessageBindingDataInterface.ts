import { Constructor } from '@argon/tools';

export interface MessageBindingDataInterface {

  message: Constructor<any>;

  useHistory?: boolean;

  propertyKey: string;

}
