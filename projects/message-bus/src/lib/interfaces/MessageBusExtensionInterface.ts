import { MessageBusService } from '../services/MessageBusService';
import { OnDestroy, OnInit } from '@angular/core';


export interface MessageBusExtensionInterface extends OnInit, OnDestroy {

  messageBus: MessageBusService;

  sendMessage(message: any): void;

}
