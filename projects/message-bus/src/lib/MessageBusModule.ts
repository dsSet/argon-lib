import { NgModule } from '@angular/core';
import { MessageBusService } from './services/MessageBusService';


@NgModule({
  providers: [
    MessageBusService
  ]
})
export class MessageBusModule { }
