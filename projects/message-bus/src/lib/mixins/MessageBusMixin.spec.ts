import { Component, Injectable, Injector, OnDestroy, OnInit, Directive } from '@angular/core';
import { HasInjectorInterface } from '../../../../tools/src/lib/Shapes/HasInjectorInterface';
import { MessageBusMixin } from './MessageBusMixin';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MessageBusService } from '../services/MessageBusService';
import { MessageBusServiceMock } from '../../test/MessageBusServiceMock';
import createSpy = jasmine.createSpy;

describe('MessageBusMixin', () => {

  @Injectable()
  class BaseClass implements HasInjectorInterface {
    constructor(public injector: Injector) { }
  }

  @Component({ selector: 'spec-component', template: '' })
  class FakeComponent extends MessageBusMixin(BaseClass) implements OnInit {
    constructor(injector: Injector) { super(injector); }
  }

  let component: FakeComponent;
  let fixture: ComponentFixture<FakeComponent>;
  let messageBusService: MessageBusServiceMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        FakeComponent
      ],
      providers: [
        { provide: MessageBusService, useClass: MessageBusServiceMock }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FakeComponent);
    component = fixture.componentInstance;
    messageBusService = TestBed.inject(MessageBusService) as any;
  });

  it('should create MessageBusService with ngOnInit', () => {
    component.ngOnInit();
    expect(component.messageBus).toEqual(messageBusService as any);
  });

  it('should connect object to the message bus on init', () => {
    component.ngOnInit();
    expect(messageBusService.connect).toHaveBeenCalledWith(component);
  });

  it('should disconnect object on destroy', () => {
    component.ngOnInit();
    component.ngOnDestroy();
    expect(messageBusService.disconnect).toHaveBeenCalledWith(component);
  });

  it('sendMessage should send message to the message bus', () => {
    component.ngOnInit();
    const message = { data: 'mock message' };
    component.sendMessage(message);
    expect(messageBusService.send).toHaveBeenCalledWith(message);
  });

  it('should call parent ngOnInit and ngOnDestroy', () => {
    const ngOnInit = createSpy('ngOnInit');
    const ngOnDestroy = createSpy('ngOnDestroy');
    @Directive()
class Base implements OnInit, OnDestroy {
      injector = {
        get: createSpy('get').and.returnValue(new MessageBusServiceMock())
      };
      ngOnInit() { ngOnInit(); }
      ngOnDestroy() { ngOnDestroy(); }
    }

    class Target extends MessageBusMixin(Base) { }

    const object = new Target();

    object.ngOnInit();
    expect(ngOnInit).toHaveBeenCalled();

    object.ngOnDestroy();
    expect(ngOnDestroy).toHaveBeenCalled();
  });
});
