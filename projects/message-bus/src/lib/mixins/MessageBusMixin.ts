import { Constructor, HasInjectorInterface, HasLifecircleHooksInterface } from '@argon/tools';
import { MessageBusExtensionInterface } from '../interfaces/MessageBusExtensionInterface';
import { OnDestroy, OnInit, Directive } from '@angular/core';
import { MessageBusService } from '../services/MessageBusService';

export function MessageBusMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<MessageBusExtensionInterface & OnInit & OnDestroy> {

  @Directive()
class MessageBusExtension extends BaseClass implements MessageBusExtensionInterface, OnInit, OnDestroy {

    public messageBus: MessageBusService;

    ngOnInit(): void {
      if (super.ngOnInit) {
        super.ngOnInit();
      }
      this.messageBus = this.injector.get(MessageBusService);
      this.messageBus.connect(this);
    }

    ngOnDestroy(): void {
      if (super.ngOnDestroy) {
        super.ngOnDestroy();
      }

      if (this.messageBus) {
        this.messageBus.disconnect(this);
      }
    }

    public sendMessage(message: any) {
      this.messageBus.send(message);
    }

  }

  return MessageBusExtension;
}
