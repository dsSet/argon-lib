import map from 'lodash/map';
import { Injectable, isDevMode, OnDestroy } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { Constructor, Unsubscribable } from '@argon/tools';
import { filter } from 'rxjs/operators';
import {
  getMessageBindingData,
  getMessageKey,
  getObjectSubscriptions,
  removeObjectSubscriptions,
  setObjectSubscriptions
} from '../descorators/Message';
import { MessageBindingDataInterface } from '../interfaces/MessageBindingDataInterface';

@Injectable()
export class MessageBusService implements OnDestroy {

  protected bus = new Subject<any>();

  protected history = new Map<string, any>();

  constructor() {
    this.bus.subscribe(this.updateHistory);
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  /**
   * Add new subscription to the message bus. Subscriptions will handle only messages by type MessageType.
   */
  public of(MessageType: Constructor<any>): Observable<any> {
    const messageKey = getMessageKey(MessageType);
    return this.bus.pipe(
      filter((message: any) => getMessageKey(message) === messageKey)
    );
  }

  /**
   * Send message to the message bus
   */
  public send(message: any): void {
    this.bus.next(message);
  }

  /**
   * Get message from the message bus. Service will keep the lates message for each sent message type
   */
  public getFromHistory(message: any): any {
    const key = getMessageKey(message);
    return this.history.get(key) || null;
  }

  /**
   * Connect object to the message bus. This method will parse all bindings (see MessageBinding decorator)
   */
  public connect(object: any): void {
    const metadata = getMessageBindingData(object);
    if (metadata.length === 0) {
      return;
    }
    const subscriptions = map(metadata, ({ useHistory, message, propertyKey }: MessageBindingDataInterface) => {
      const historyMessage = this.getFromHistory(message);
      if (historyMessage && useHistory) {
        object[propertyKey](historyMessage);
      }
      return this.of(message).subscribe((data: typeof message) => { object[propertyKey](data); });
    });
    setObjectSubscriptions(object, new Set(subscriptions));
  }

  /**
   * Disconnect object from the message bus
   */
  public disconnect(object: any): void {
    const subscriptions = getObjectSubscriptions(object);
    subscriptions.forEach((subscription: Subscription) => { subscription.unsubscribe(); });
    removeObjectSubscriptions(object);
  }

  private updateHistory = (message: any) => {
    if (isDevMode() && console.log) {
      console.log('MessageBus:', message);
    }
    this.history.set(getMessageKey(message), message);
  }
}
