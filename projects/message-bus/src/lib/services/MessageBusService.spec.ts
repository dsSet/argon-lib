import { MessageBusService } from './MessageBusService';
import { Subject } from 'rxjs';
import { getObjectSubscriptions, Message, MessageBinding } from '../descorators/Message';
import createSpy = jasmine.createSpy;

describe('MessageBusService', () => {

  @Message('Message1')
  class Message1 { data: any; }

  @Message('Message2')
  class Message2 { }

  class MockService extends MessageBusService {
    getBus(): Subject<any> { return this.bus; }
    getHistory(): Map<string, any> { return this.history; }
  }

  let service: MockService;

  beforeEach(() => {
    service = new MockService();
  });

  afterEach(() => {
    service.ngOnDestroy();
  });

  it('should have updateHistory subscription', () => {
    expect(service.getBus().observers.length).toEqual(1);
  });

  it('ngOnDestroy should complete bus', () => {
    const bus = service.getBus();
    expect(bus.isStopped).toBeFalsy();
    service.ngOnDestroy();
    expect(bus.isStopped).toBeTruthy();
  });

  it('of should return message stream and handle message', () => {
    const bus = service.getBus();
    const expectedResult = new Message1();
    const spy = createSpy('messageSpy');
    service.of(Message1).subscribe((data: Message1) => {
      spy(data);
    });
    bus.next(expectedResult);
    expect(spy).toHaveBeenCalledWith(expectedResult);
  });

  it('of should return message stream and filter message', () => {
    const bus = service.getBus();
    const expectedResult = new Message2();
    const spy = createSpy('messageSpy');
    service.of(Message1).subscribe((data: Message1) => {
      spy(data);
    });
    bus.next(expectedResult);
    expect(spy).not.toHaveBeenCalled();
  });

  it('send should emit bus events', () => {
    const bus = service.getBus();
    spyOn(bus, 'next');
    const message = new Message1();
    service.send(message);
    expect(bus.next).toHaveBeenCalledWith(message);
  });

  it('getFromHistory should return null if have no message history', () => {
    expect(service.getFromHistory(Message1)).toEqual(null);
  });

  it('getFromHistory should return last message from history', () => {
    const message1 = new Message1();
    message1.data = 1;
    const message2 = new Message1();
    message2.data = 1123;
    service.send(message1);
    service.send(message2);
    expect(service.getFromHistory(Message1)).toEqual(message2);
  });

  it('connect should bind object to the bus', () => {
    class MockObject {
      spy = createSpy('spy');
      @MessageBinding(Message1) handleMessage(data: Message1) { this.spy(data); }
    }

    const target = new MockObject();
    service.connect(target);
    const message = new Message1();
    service.send(message);

    expect(target.spy).toHaveBeenCalledWith(message);
  });

  it('connect should call message from the history', () => {
    const message = new Message1();
    service.send(message);

    class MockObject {
      spy = createSpy('spy');
      @MessageBinding(Message1, true) handleMessage(data: Message1) { this.spy(data); }
    }

    const target = new MockObject();
    service.connect(target);

    expect(target.spy).toHaveBeenCalledWith(message);
  });

  it('connect should not call message from the history if binding has flag false', () => {
    const message = new Message1();
    service.send(message);

    class MockObject {
      spy = createSpy('spy');
      @MessageBinding(Message1, false) handleMessage(data: Message1) { this.spy(data); }
    }

    const target = new MockObject();
    service.connect(target);

    expect(target.spy).not.toHaveBeenCalled();
  });

  it('connect should not add subscriptions if object has no metadata', () => {
    class MockObject {
      handleMessage(data: Message1) { }
    }
    const target = new MockObject();
    service.connect(target);
    expect(() => { getObjectSubscriptions(target); }).toThrow(new Error('Object has no message bus subscriptions'));
  });

  it('disconnect should remove subscriptions', () => {
    class MockObject {
      spy = createSpy('spy');
      @MessageBinding(Message1) handleMessage(data: Message1) { this.spy(data); }
    }
    const target = new MockObject();
    service.connect(target);

    const subscriptions = getObjectSubscriptions(target);
    expect(subscriptions.size).toEqual(1);
    const subscription = Array.from(subscriptions.values())[0];
    expect(subscription.closed).toBeFalsy();
    spyOn(subscription, 'unsubscribe');
    service.disconnect(target);
    expect(subscription.unsubscribe).toHaveBeenCalled();
  });

  it('disconnect should remove object subscriptions', () => {
    class MockObject {
      spy = createSpy('spy');
      @MessageBinding(Message1) handleMessage(data: Message1) { this.spy(data); }
    }
    const target = new MockObject();
    service.connect(target);

    expect(getObjectSubscriptions(target).size).toEqual(1);
    service.disconnect(target);

    expect(() => { getObjectSubscriptions(target); }).toThrow(new Error('Object has no message bus subscriptions'));
  });
});
