import { Subject } from 'rxjs';
import createSpy = jasmine.createSpy;


export class MessageBusServiceMock {

  getFromHistoryResult = { data: 'history result' };

  ofResult = new Subject<any>();

  of = createSpy('of').and.returnValue(this.ofResult);

  send = createSpy('send');

  getFromHistory = createSpy('getFromHistory').and.returnValue(this.getFromHistoryResult);

  connect = createSpy('connect');

  disconnect = createSpy('disconnect');

}
