/*
 * Public API Surface of message-bus
 */

export { MessageBusModule } from './lib/MessageBusModule';

// Message Bus
export { Message, MessageBinding } from './lib/descorators/Message';
export { MessageBusExtensionInterface } from './lib/interfaces/MessageBusExtensionInterface';
export { MessageBusMixin } from './lib/mixins/MessageBusMixin';
export { MessageBusService } from './lib/services/MessageBusService';
