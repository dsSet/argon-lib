import { Message } from '../index';

// Just for handling with single method inside service
export class TestMessageInterface {

  name: string;

  data: string;

}

@Message('Story:Message1')
export class Message1 implements TestMessageInterface {

  public customData = 'message staff';

  public name = 'Message1';

  constructor(
    public data: string
  ) { }

}

@Message('Story:Message2')
export class Message2 implements TestMessageInterface {

  public customData = 123;

  public name = 'Message2';

  constructor(
    public data: string
  ) { }

}


@Message('Story:Message3')
export class Message3 implements TestMessageInterface {

  public customData = true;

  public name = 'Message3';

  constructor(
    public data: string
  ) { }

}
