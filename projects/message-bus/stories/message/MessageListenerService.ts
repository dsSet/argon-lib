import { Injectable, OnDestroy } from '@angular/core';
import { MessageBusService, MessageBinding } from '../index';
import { Message1, Message2, Message3, TestMessageInterface } from './Messages';

@Injectable()
export class MessageListenerService implements OnDestroy {

  constructor(
    private messageBusService: MessageBusService
  ) {
    this.messageBusService.connect(this);
  }

  ngOnDestroy(): void {
    this.messageBusService.disconnect(this);
  }

  @MessageBinding(Message1)
  @MessageBinding(Message2)
  @MessageBinding(Message3)
  public handleMessage(message: TestMessageInterface) {
    alert(`MessageListenerService handle ${message.name} with data ${message.data}`);
  }

}
