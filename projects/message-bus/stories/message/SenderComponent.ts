import { Component } from '@angular/core';
import { MessageBusService } from '../index';
import { Message1, Message2, Message3 } from './Messages';


@Component({
  selector: 'story-sender-component',
  template: `
    <input class="input-control" [(ngModel)]="data" />
    <button class="btn btn-sm btn-primary" (click)="sendMessage(1)">Send Message 1</button>
    <button class="btn btn-sm btn-primary" (click)="sendMessage(2)">Send Message 2</button>
    <button class="btn btn-sm btn-primary" (click)="sendMessage(3)">Send Message 3</button>
  `
})
export class SenderComponent {

  public data: string;

  constructor(private messageBus: MessageBusService) { }

  public sendMessage(type: number) {
    let message;
    switch (type) {
      case 1: message = new Message1(this.data);
              break;
      case 2: message = new Message2(this.data);
              break;
      case 3: message = new Message3(this.data);
              break;
      default: return;
    }

    this.messageBus.send(message);
  }

}
