import { Component } from '@angular/core';
import { BaseShape } from '@argon/tools';
import { MessageBinding, MessageBusMixin } from '../index';
import { Message1, Message2, Message3 } from './Messages';

@Component({
  selector: 'story-listener-component',
  template: `
    <div *ngIf="message1">Message1: {{ message1 | json }}</div>
    <div *ngIf="message2">Message2: {{ message2 | json }}</div>
    <div *ngIf="message3">Message3 (with history): {{ message3 | json }}</div>
    <button class="btn btn-sm btn-secondary" (click)="sendMessage1()">Send message 1 from listener!</button>
  `
})
export class ListenerComponent extends MessageBusMixin(BaseShape) {

  public message1: Message1;

  public message2: Message2;

  public message3: Message3;

  @MessageBinding(Message1)
  handleMessage1(message: Message1) {
    this.message1 = message;
  }

  @MessageBinding(Message2)
  handleMessage2(message: Message2) {
    this.message2 = message;
  }

  @MessageBinding(Message3, true)
  handleMessage3(message: Message3) {
    this.message3 = message;
  }

  public sendMessage1() {
    this.sendMessage(new Message1('I was sent from Listener!'));
  }
}
