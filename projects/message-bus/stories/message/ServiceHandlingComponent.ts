import { Component } from '@angular/core';
import { MessageListenerService } from './MessageListenerService';

@Component({
  selector: 'story-service-handling',
  template: '<story-sender-component></story-sender-component>'
})
export class ServiceHandlingComponent {

  constructor(public service: MessageListenerService) { }

}
