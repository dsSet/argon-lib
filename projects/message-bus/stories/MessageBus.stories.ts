import { moduleMetadata, storiesOf } from '@storybook/angular';
import { MessageBusModule } from './index';
import { SenderComponent } from './message/SenderComponent';
import { ListenerComponent } from './message/ListenerComponent';
import { CommonModule } from '@angular/common';
import { MessageListenerService } from './message/MessageListenerService';
import { ServiceHandlingComponent } from './message/ServiceHandlingComponent';

const notes = { markdown: require('../docs/index.md').default };

storiesOf('Argon|message-bus/Message Bus', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        CommonModule,
        MessageBusModule
      ],
      declarations: [
        SenderComponent,
        ListenerComponent,
        ServiceHandlingComponent
      ],
      providers: [
        MessageListenerService
      ]
    })
  ).add('Test Message Binding', () => ({
    props: {
      listenersCount: [1]
    },
    template: `
      <story-sender-component></story-sender-component>
      <div style="display: flex">
        <pre *ngFor="let n of listenersCount; let i = index" style="padding: 0.2rem">
          <b>Listener {{ i }}</b>
          <small>
            <story-listener-component ></story-listener-component>
          </small>
        </pre>
        <hr>
      </div>
      <button class="btn btn-success" (click)="listenersCount.push(1)">Add listener</button>
    `
  }), { notes }
).add('Message binding inside service', () => ({
    component: ServiceHandlingComponent,
    moduleMetadata: { }
  }
), { notes });
