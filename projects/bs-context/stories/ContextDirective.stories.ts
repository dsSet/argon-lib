import { moduleMetadata, storiesOf } from '@storybook/angular';
import { ContextMixin, BsContextModule } from './index';
import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { UIShape } from '@argon/tools';

@Component({
  selector: 'story-test',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
class FakeComponent extends ContextMixin(UIShape) {

  @HostBinding('class.base-class') base = true;

  bsClassName = 'btn';
}

storiesOf('Argon|bs-context/Context Directive', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        BsContextModule
      ],
      declarations: [
        FakeComponent
      ]
    })
  )
  .add('Class name test', () => ({
    props: {
      context: 'primary'
    },
    template: `
      <story-test
        class="test-class"
        [class.from-pattern]="true"
        [arBsContext]="context"
        arBsSize="sm"
        (click)="context = 'secondary'"
      >Content</story-test>`,
  }))
  .add('Directives test', () => ({
    props: {
      context: 'primary'
    },
    template: `
      <div
        class="test-class"
        [class.from-pattern]="true"
        [arBsContext]="context"
        arBsSize="sm"
      >Content</div>`,
  }));
