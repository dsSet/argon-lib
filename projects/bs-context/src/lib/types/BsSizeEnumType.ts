/**
 * Context Size Type
 */
export enum BsSizeEnum {

  XL = 'xl',

  LG = 'lg',

  MD = 'md',

  SM = 'sm',

  XS = 'xs',

  XXS = 'xss'

}

export type BsSizeType = 'xl' | 'lg' | 'md' | 'sm' | 'xs' | 'xss';
