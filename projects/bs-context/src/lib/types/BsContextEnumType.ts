
/**
 * Context color type
 */
export enum BsContextEnum {

  INFO = 'info',

  PRIMARY = 'primary',

  SECONDARY = 'secondary',

  SUCCESS = 'success',

  WARNING = 'warning',

  DANGER = 'danger',

  DEFAULT = 'default',

  LIGHT = 'light',

  DARK = 'dark',

  LINK = 'link',

  MUTED = 'muted',

  WHITE = 'white',

}

// tslint:disable-next-line:max-line-length
export type BsContextType = 'info' | 'primary' | 'secondary' | 'success' | 'warning' | 'danger' | 'default' | 'light' | 'dark' | 'link' | 'muted' | 'white';
