import {
  Constructor,
  HasInjectorInterface,
  HasLifecircleHooksInterface,
} from '@argon/tools';
import { ElementRef, Injectable, Input, OnChanges, OnInit, Renderer2, SimpleChange, SimpleChanges, Type, Directive } from '@angular/core';
import { ContextExtensionInterface } from '../interfaces/ContextExtensionInterface';
import { BsSizeType } from '../types/BsSizeEnumType';
import { BsContextType } from '../types/BsContextEnumType';


export function ContextMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<ContextExtensionInterface> {

  @Directive()
@Injectable()
  class ContextExtension extends BaseClass implements ContextExtensionInterface, OnInit, OnChanges {

    @Input() arBsSize: BsSizeType;

    @Input() arBsContext: BsContextType;

    public bsClassName: string;

    protected element: ElementRef<HTMLElement>;

    protected renderer: Renderer2;

    ngOnInit(): void {
      if (super.ngOnInit) {
        super.ngOnInit();
      }

      this.renderer = this.injector.get<Renderer2>(Renderer2 as Type<Renderer2>);
      this.element = this.injector.get<ElementRef>(ElementRef);

      if (this.bsClassName) {
        this.renderer.addClass(this.element.nativeElement, this.bsClassName);
      } else if (this.element.nativeElement
        && this.element.nativeElement.classList
        && this.element.nativeElement.classList.length > 0
      ) {
        this.bsClassName = this.element.nativeElement.classList.item(0);
      }

      this.replaceItem(new SimpleChange(null, this.arBsContext, false));
      this.replaceItem(new SimpleChange(null, this.arBsSize, false));
    }

    ngOnChanges(changes: SimpleChanges): void {
      if (super.ngOnChanges) {
        super.ngOnChanges(changes);
      }

      const { arBsContext, arBsSize } = changes;
      this.replaceItem(arBsContext);
      this.replaceItem(arBsSize);
    }

    protected replaceItem(change: SimpleChange) {
      if (!change) {
        return;
      }

      const { currentValue, previousValue, firstChange } = change;
      if (firstChange) {
        return;
      }

      if (previousValue) {
        const value = this.getItemClass(previousValue);
        if (value) {
          this.renderer.removeClass(this.element.nativeElement, value);
        }
      }

      if (currentValue) {
        const value = this.getItemClass(currentValue);
        if (value) {
          this.renderer.addClass(this.element.nativeElement, value);
        }
      }
    }

    protected getItemClass(val: any): string {
      return this.bsClassName && val ? `${this.bsClassName}-${val}` : null;
    }

  }

  return ContextExtension;
}
