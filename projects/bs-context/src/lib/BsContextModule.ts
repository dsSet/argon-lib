import { NgModule } from '@angular/core';
import { ContextDirective } from './directives/ContextDirective';

@NgModule({
  declarations: [
    ContextDirective
  ],
  exports: [
    ContextDirective
  ]
})
export class BsContextModule { }
