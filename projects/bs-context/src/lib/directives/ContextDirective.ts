import {
  Directive,
  ElementRef,
  Injector,
  Input,
  OnChanges,
  OnInit,
  Renderer2,
  SimpleChange,
  SimpleChanges,
  Type
} from '@angular/core';
import { BsContextEnum } from '../types/BsContextEnumType';
import { BsSizeEnum } from '../types/BsSizeEnumType';
import { BaseShape } from '@argon/tools';

@Directive({
  selector: '[arBsContext], [arBsSize]'
})
export class ContextDirective extends BaseShape implements OnInit, OnChanges {

  @Input() arBsSize: BsSizeEnum;

  @Input() arBsContext: BsContextEnum;

  protected element: ElementRef<HTMLElement>;

  protected renderer: Renderer2;

  protected bsClassName: string;

  constructor(public injector: Injector) {
    super(injector);
    this.renderer = injector.get<Renderer2>(Renderer2 as Type<Renderer2>);
    this.element = injector.get<ElementRef>(ElementRef);
  }

  ngOnInit(): void {
    if (this.bsClassName) {
      this.renderer.addClass(this.element.nativeElement, this.bsClassName);
    } else if (this.element.nativeElement
      && this.element.nativeElement.classList
      && this.element.nativeElement.classList.length > 0
    ) {
      this.bsClassName = this.element.nativeElement.classList.item(0);
    }

    this.replaceItem(new SimpleChange(null, this.arBsContext, false));
    this.replaceItem(new SimpleChange(null, this.arBsSize, false));
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { arBsContext, arBsSize } = changes;
    this.replaceItem(arBsContext);
    this.replaceItem(arBsSize);
  }

  private replaceItem(change: SimpleChange) {
    if (!change) {
      return;
    }

    const { currentValue, previousValue, firstChange } = change;
    if (firstChange) {
      return;
    }

    if (previousValue) {
      const value = this.getItemClass(previousValue);
      if (value) {
        this.renderer.removeClass(this.element.nativeElement, value);
      }
    }

    if (currentValue) {
      const value = this.getItemClass(currentValue);
      if (value) {
        this.renderer.addClass(this.element.nativeElement, value);
      }
    }
  }

  private getItemClass(val: any): string {
    return this.bsClassName && val ? `${this.bsClassName}-${val}` : null;
  }
}
