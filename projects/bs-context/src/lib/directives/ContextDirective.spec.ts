import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ContextDirective } from './ContextDirective';
import { Component, SimpleChange } from '@angular/core';
import { BsContextEnum } from '../types/BsContextEnumType';
import { BsSizeEnum } from '../types/BsSizeEnumType';

describe('ContextDirective', () => {
  describe('should be used as inherited', () => {
    @Component({
      selector: 'spec-component',
      template: ''
    })
    class FakeComponent extends ContextDirective {

      bsClassName = 'fake-class';

    }

    let component: FakeComponent;
    let fixture: ComponentFixture<FakeComponent>;
    let element: HTMLElement;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ FakeComponent ],
      });
      fixture = TestBed.createComponent(FakeComponent);
      component = fixture.componentInstance;
      element = fixture.nativeElement;
    });

    it('should contain bsClassName in classList', () => {
      fixture.detectChanges();
      expect(element.classList.contains('fake-class')).toBeTruthy();
    });

    it('should have context class name', () => {
      component.arBsContext = BsContextEnum.DARK;
      fixture.detectChanges();
      expect(element.classList.contains('fake-class-dark')).toBeTruthy();
    });

    it('should have size class name', () => {
      component.arBsSize = BsSizeEnum.SM;
      fixture.detectChanges();
      expect(element.classList.contains('fake-class-sm')).toBeTruthy();
    });

    it('should use element class by default', () => {
      component.bsClassName = null;
      element.classList.add('outer-class');
      component.arBsSize = BsSizeEnum.LG;
      component.arBsContext = BsContextEnum.DANGER;
      fixture.detectChanges();
      expect(element.classList.contains('outer-class')).toBeTruthy();
      expect(element.classList.contains('outer-class-lg')).toBeTruthy();
      expect(element.classList.contains('outer-class-danger')).toBeTruthy();
    });

    it('should remove context and size onChanges', () => {
      component.arBsSize = BsSizeEnum.LG;
      component.arBsContext = BsContextEnum.DANGER;
      fixture.detectChanges();
      expect(element.classList.contains('fake-class-lg')).toBeTruthy();
      expect(element.classList.contains('fake-class-danger')).toBeTruthy();
      const arBsSize = new SimpleChange(component.arBsSize, null, false);
      const arBsContext = new SimpleChange(component.arBsContext, null, false);
      component.ngOnChanges({ arBsSize, arBsContext });
      fixture.detectChanges();
      expect(element.classList.contains('fake-class-lg')).toBeFalsy();
      expect(element.classList.contains('fake-class-danger')).toBeFalsy();
    });

    it('should change context and size', () => {
      component.arBsSize = BsSizeEnum.LG;
      component.arBsContext = BsContextEnum.DANGER;
      fixture.detectChanges();
      expect(element.classList.contains('fake-class-lg')).toBeTruthy();
      expect(element.classList.contains('fake-class-danger')).toBeTruthy();
      const arBsSize = new SimpleChange(component.arBsSize, BsSizeEnum.MD, false);
      const arBsContext = new SimpleChange(component.arBsContext, BsContextEnum.INFO, false);
      component.ngOnChanges({ arBsSize, arBsContext });
      fixture.detectChanges();
      expect(element.classList.contains('fake-class-md')).toBeTruthy();
      expect(element.classList.contains('fake-class-info')).toBeTruthy();
    });

    it('should not change classNames on first change', () => {
      component.arBsSize = BsSizeEnum.LG;
      fixture.detectChanges();
      expect(element.classList.contains('fake-class-lg')).toBeTruthy();
      const arBsSize = new SimpleChange(component.arBsSize, BsSizeEnum.MD, true);
      component.ngOnChanges({ arBsSize });
      expect(element.classList.contains('fake-class-lg')).toBeTruthy();
    });

    it('should not change classNames if no changes', () => {
      component.arBsSize = BsSizeEnum.LG;
      fixture.detectChanges();
      expect(element.classList.contains('fake-class-lg')).toBeTruthy();
      component.ngOnChanges({});
      expect(element.classList.contains('fake-class-lg')).toBeTruthy();
    });

    it('should not add context and size without base class', () => {
      component.arBsSize = BsSizeEnum.LG;
      component.arBsContext = BsContextEnum.DANGER;
      component.bsClassName = null;
      fixture.detectChanges();
      expect(element.classList.length).toEqual(0);
    });

    it('should not remove class if have no prev value on change', () => {
      component.arBsSize = BsSizeEnum.LG;
      fixture.detectChanges();
      expect(element.classList.contains('fake-class-lg')).toBeTruthy();
      component.bsClassName = null;
      const arBsSize = new SimpleChange(BsSizeEnum.LG, BsSizeEnum.MD, false);
      component.ngOnChanges({ arBsSize });
      expect(element.classList.contains('fake-class-lg')).toBeTruthy();
    });
  });

  describe('should bind as directive', () => {
    @Component({
      selector: 'spec-component',
      template: '<div class="test test2" arBsSize="sm" arBsContext="primary"></div>'
    })
    class FakeContainerComponent { }

    let component: FakeContainerComponent;
    let fixture: ComponentFixture<FakeContainerComponent>;
    let element: HTMLElement;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ FakeContainerComponent, ContextDirective ],
      });
      fixture = TestBed.createComponent(FakeContainerComponent);
      component = fixture.componentInstance;
      element = fixture.nativeElement.querySelector('div');
    });

    it('should use first class as base', () => {
      fixture.detectChanges();
      expect(element.classList.contains('test-sm')).toBeTruthy();
      expect(element.classList.contains('test-primary')).toBeTruthy();
    });
  });
});
