import { SimpleChanges } from '@angular/core';
import { BsContextType } from '../types/BsContextEnumType';
import { BsSizeType } from '../types/BsSizeEnumType';

export interface ContextExtensionInterface {

  arBsSize: BsSizeType;

  arBsContext: BsContextType;

  bsClassName: string;

  ngOnInit(): void;

  ngOnChanges(changes: SimpleChanges): void;

}
