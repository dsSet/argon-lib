# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@2.0.1...@argon/bs-context@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/bs-context





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@1.0.4...@argon/bs-context@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/bs-context






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@1.0.4...@argon/bs-context@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/bs-context






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@1.0.3...@argon/bs-context@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/bs-context





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@1.0.2...@argon/bs-context@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/bs-context





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@1.0.1...@argon/bs-context@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/bs-context






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.18...@argon/bs-context@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/bs-context





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.18...@argon/bs-context@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/bs-context






## [0.4.18](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.17...@argon/bs-context@0.4.18) (2020-08-04)

**Note:** Version bump only for package @argon/bs-context






## [0.4.17](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.16...@argon/bs-context@0.4.17) (2020-05-21)

**Note:** Version bump only for package @argon/bs-context






## [0.4.16](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.15...@argon/bs-context@0.4.16) (2020-04-18)


### Bug Fixes

* **bs-context:** add missed export ([66be556](https://bitbucket.org/dsSet/argon-lib/commits/66be556))





## [0.4.15](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.14...@argon/bs-context@0.4.15) (2020-04-18)


### Bug Fixes

* **bs-context:** fix imports ([a470b8a](https://bitbucket.org/dsSet/argon-lib/commits/a470b8a))
* 🐛 bs-context ([ded6a20](https://bitbucket.org/dsSet/argon-lib/commits/ded6a20))






## [0.4.14](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.13...@argon/bs-context@0.4.14) (2020-03-24)

**Note:** Version bump only for package @argon/bs-context





## [0.4.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.12...@argon/bs-context@0.4.13) (2020-03-24)

**Note:** Version bump only for package @argon/bs-context






## [0.4.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.11...@argon/bs-context@0.4.12) (2020-03-18)

**Note:** Version bump only for package @argon/bs-context





## [0.4.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.10...@argon/bs-context@0.4.11) (2020-02-14)

**Note:** Version bump only for package @argon/bs-context






## [0.4.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.9...@argon/bs-context@0.4.10) (2019-11-29)

**Note:** Version bump only for package @argon/bs-context





## [0.4.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.8...@argon/bs-context@0.4.9) (2019-11-22)

**Note:** Version bump only for package @argon/bs-context





## [0.4.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.7...@argon/bs-context@0.4.8) (2019-11-22)

**Note:** Version bump only for package @argon/bs-context





## [0.4.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.6...@argon/bs-context@0.4.7) (2019-10-28)

**Note:** Version bump only for package @argon/bs-context





## [0.4.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.5...@argon/bs-context@0.4.6) (2019-10-11)

**Note:** Version bump only for package @argon/bs-context





## [0.4.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.4...@argon/bs-context@0.4.5) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





## [0.4.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.3...@argon/bs-context@0.4.4) (2019-10-08)

**Note:** Version bump only for package @argon/bs-context





## [0.4.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.2...@argon/bs-context@0.4.3) (2019-09-21)

**Note:** Version bump only for package @argon/bs-context





## [0.4.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.1...@argon/bs-context@0.4.2) (2019-09-17)


### Bug Fixes

* **bs-context:** remove dependency ([4a2b675](https://bitbucket.org/dsSet/argon-lib/commits/4a2b675))






## [0.4.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.4.0...@argon/bs-context@0.4.1) (2019-09-07)

**Note:** Version bump only for package @argon/bs-context





# [0.4.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.15...@argon/bs-context@0.4.0) (2019-09-06)


### Features

* **bs-context:** change ContextDirective interface ([5586ce3](https://bitbucket.org/dsSet/argon-lib/commits/5586ce3))





## [0.3.15](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.14...@argon/bs-context@0.3.15) (2019-08-10)

**Note:** Version bump only for package @argon/bs-context





## [0.3.14](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.13...@argon/bs-context@0.3.14) (2019-07-28)

**Note:** Version bump only for package @argon/bs-context





## [0.3.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.12...@argon/bs-context@0.3.13) (2019-07-14)

**Note:** Version bump only for package @argon/bs-context





## [0.3.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.11...@argon/bs-context@0.3.12) (2019-07-14)


### Bug Fixes

* **dropdown:** mock import ([884e049](https://bitbucket.org/dsSet/argon-lib/commits/884e049))





## [0.3.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.10...@argon/bs-context@0.3.11) (2019-07-14)

**Note:** Version bump only for package @argon/bs-context





## [0.3.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.8...@argon/bs-context@0.3.10) (2019-07-14)

**Note:** Version bump only for package @argon/bs-context






## [0.3.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.8...@argon/bs-context@0.3.9) (2019-06-30)

**Note:** Version bump only for package @argon/bs-context





## [0.3.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.7...@argon/bs-context@0.3.8) (2019-06-23)


### Bug Fixes

* **bs-context,keyboard:** fix dependencies ([b0958fc](https://bitbucket.org/dsSet/argon-lib/commits/b0958fc))





## [0.3.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.6...@argon/bs-context@0.3.7) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.3.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.5...@argon/bs-context@0.3.6) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.3.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.4...@argon/bs-context@0.3.5) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.3.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.3...@argon/bs-context@0.3.4) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.3.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.2...@argon/bs-context@0.3.3) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.3.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.1...@argon/bs-context@0.3.2) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.3.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.3.0...@argon/bs-context@0.3.1) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





# [0.3.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.9...@argon/bs-context@0.3.0) (2019-06-23)


### Features

* **keyboard:** add keyboard package ([7b7934a](https://bitbucket.org/dsSet/argon-lib/commits/7b7934a))





## [0.2.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.8...@argon/bs-context@0.2.9) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.2.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.7...@argon/bs-context@0.2.8) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.2.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.6...@argon/bs-context@0.2.7) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.2.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.5...@argon/bs-context@0.2.6) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.2.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.4...@argon/bs-context@0.2.5) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.2.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.3...@argon/bs-context@0.2.4) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.2.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.2...@argon/bs-context@0.2.3) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.2.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.1...@argon/bs-context@0.2.2) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/bs-context@0.2.0...@argon/bs-context@0.2.1) (2019-06-23)

**Note:** Version bump only for package @argon/bs-context





# 0.2.0 (2019-06-22)


### Features

* **BsContextModule:** add context module ([0ee7b05](https://bitbucket.org/dsSet/argon-lib/commits/0ee7b05))
* **main:** add @argon/tslint ([ef2c799](https://bitbucket.org/dsSet/argon-lib/commits/ef2c799))





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/bs-context@0.1.0...bs-context@0.1.1) (2019-06-22)

**Note:** Version bump only for package bs-context





# 0.1.0 (2019-06-20)


### Features

* **BsContextModule:** add context module ([0ee7b05](https://bitbucket.org/dsSet/argon-lib/commits/0ee7b05))
