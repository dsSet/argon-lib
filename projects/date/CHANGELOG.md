# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/date@2.0.1...@argon/date@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/date





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/date@1.0.4...@argon/date@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/date






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/date@1.0.4...@argon/date@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/date






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/date@1.0.3...@argon/date@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/date





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/date@1.0.2...@argon/date@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/date





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/date@1.0.1...@argon/date@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/date






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/date@0.1.1...@argon/date@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/date





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/date@0.1.1...@argon/date@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/date






## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/date@0.1.0...@argon/date@0.1.1) (2020-08-07)


### Bug Fixes

* **date:** fix date transform decorator props value storing ([e5f8aa5](https://bitbucket.org/dsSet/argon-lib/commits/e5f8aa5))





# 0.1.0 (2020-08-04)


### Bug Fixes

* **date:** add missed method declaration ([6cf4940](https://bitbucket.org/dsSet/argon-lib/commits/6cf4940))
* **date:** date selection stuff ([79e98a7](https://bitbucket.org/dsSet/argon-lib/commits/79e98a7))
* **date:** set end date as end-of -day ([25c9167](https://bitbucket.org/dsSet/argon-lib/commits/25c9167))


### Features

* **data:** add date package ([3913c97](https://bitbucket.org/dsSet/argon-lib/commits/3913c97))
* **date:** add base services ([5b5c81c](https://bitbucket.org/dsSet/argon-lib/commits/5b5c81c))
* **date:** add date abstract services ([bac85fa](https://bitbucket.org/dsSet/argon-lib/commits/bac85fa))
* **date:** add date package ([c0d9e26](https://bitbucket.org/dsSet/argon-lib/commits/c0d9e26))
* **date:** implement base views ([ef5da99](https://bitbucket.org/dsSet/argon-lib/commits/ef5da99))
* **date:** implement date list selection plugin ([5cb3653](https://bitbucket.org/dsSet/argon-lib/commits/5cb3653))
* **date:** implement date picker header ([b3e2542](https://bitbucket.org/dsSet/argon-lib/commits/b3e2542))
* **date:** implement date picker state service ([7f7db38](https://bitbucket.org/dsSet/argon-lib/commits/7f7db38))
* **date:** implement date picker view ([061896c](https://bitbucket.org/dsSet/argon-lib/commits/061896c))
* **date:** implement date range selection plugin ([5bb59f1](https://bitbucket.org/dsSet/argon-lib/commits/5bb59f1))
* **date:** implement date restrictions plugin ([fddec36](https://bitbucket.org/dsSet/argon-lib/commits/fddec36))
* **date:** implement date transform decorators ([4a6dec1](https://bitbucket.org/dsSet/argon-lib/commits/4a6dec1))
* **date:** implement month view component ([86219e5](https://bitbucket.org/dsSet/argon-lib/commits/86219e5))
* **date:** implement months view ([5018024](https://bitbucket.org/dsSet/argon-lib/commits/5018024))
* **date:** implement partial range selection plugin ([5c20ba8](https://bitbucket.org/dsSet/argon-lib/commits/5c20ba8))
* **date:** implement plugins structure ([ed88a67](https://bitbucket.org/dsSet/argon-lib/commits/ed88a67))
* **date:** implement year view ([8c8d221](https://bitbucket.org/dsSet/argon-lib/commits/8c8d221))
* **date:** refactoring date picker ([a7f16ee](https://bitbucket.org/dsSet/argon-lib/commits/a7f16ee))
* **date:** update date header, and select plugin ([309b510](https://bitbucket.org/dsSet/argon-lib/commits/309b510))
