# @argon/date

Abstract representation of working with dates. You must provide implementation of that module. 
See one of the following packages for implementation:
 - @argon/date-moment-bridge
 

## Services

### DateTransformService 

Helps to transform native js dates

### DateFormatService

Contains methods to format native js dates

### DateAdapter

Service to convert dates between server, client and native js date formats.

## Providers

 - `ARGON_CLIENT_DATE_FORMAT` - list of supported client input formats. Using to parse user input.
 - `ARGON_CLIENT_DATE_DEFAULT_FORMAT` - default client date format. Using to format js date to client format string
 - `ARGON_SERVER_DATE_FORMAT` - list of possible server formats. Using to parse server data to js date
 - `ARGON_SERVER_DATE_DEFAULT_FORMAT` - default server date format. Using to convert js date to date string compatible with server side. 

## Decorators

### @DateTransform

Property decorator. Allows to transform property value to the target format.

````typescript

import { DateTransform, HasDateAdapterInterface, DateAdapter } from '@argon/date';

class Sample implements HasDateAdapterInterface {

  constructor(public dateAdapter: DateAdapter) { }

  @DateTransform('server')
  public inputClient: string | Date | null;

  @DateTransform('server')
  public inputDate: string | Date | null;
 
  @DateTransform('server')
  public inputServer: string | Date | null;

  @DateTransform('server')
  public inputInvalid: string | Date | null;

  @DateTransform('server')
  public inputNull: string | Date | null;

}

const obj = new Sample(Injector.get(DateAdapter));

obj.inputClient = '01.01.2020';
obj.inputDate = new Date(2020, 1, 1);
obj.inputServer = '2020-01-01T00:00:00Z';
obj.inputInvalid = 'Random Input';
obj.inputNull = null;

console.log('client:', obj.inputClient);
console.log('js date:', obj.inputDate);
console.log('server:', obj.inputServer);
console.log('invalid:', obj.inputInvalid);
console.log('null:', obj.inputNull);

// OUTPUT:
// client: '2020-01-01T00:00:00Z'
// js date: '2020-01-01T00:00:00Z'
// server: '2020-01-01T00:00:00Z'
// invalid: 'Random Input'
// null: null

````

### @DateTransformArgument

Allows to transform first argument of method from date input to target format. See @DateTransform for conversion example

````typescript

import { DateTransformArgument, HasDateAdapterInterface, DateAdapter } from '@argon/date';

class Sample implements HasDateAdapterInterface {
  
  constructor(public dateAdapter: DateAdapter) { }

  @DateTransformArgument('client')
  public setDate(date: 'string' | Date | null) {
    console.log(date);
  }

}

const obj = new Sample(Injector.get(DateAdapter));

obj.setDate(new Date(2020, 1, 1));

// OUTPUT
// '01.01.2020'

````
 
## Helpers

### transformAnyToTargetFormat

Allows to shortcut date transform to target format. Could be alternative of transform decorators.

````typescript
  function transformAnyToTargetFormat(value: Date | string | null, targetFormat: 'date' | 'server' | 'client', target: HasDateAdapterInterface): Date | string | null;
````
