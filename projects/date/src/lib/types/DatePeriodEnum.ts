
export enum DatePeriodEnum {

  day = 'day',

  week = 'week',

  isoWeek = 'isoWeek',

  month = 'month',

  year = 'year'

}
