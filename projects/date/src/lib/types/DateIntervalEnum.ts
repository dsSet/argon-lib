
export enum DateIntervalEnum {

  day = 'day',

  week = 'week',

  month = 'month',

  year = 'year'

}
