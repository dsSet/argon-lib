import { InjectionToken } from '@angular/core';

export const ARGON_CLIENT_DATE_FORMAT = new InjectionToken<Array<string>>('ARGON_CLIENT_DATE_FORMAT');
export const ARGON_CLIENT_DATE_DEFAULT_FORMAT = new InjectionToken<string>('ARGON_CLIENT_DATE_DEFAULT_FORMAT');

export const ARGON_SERVER_DATE_FORMAT = new InjectionToken<Array<string>>('ARGON_SERVER_DATE_FORMAT');
export const ARGON_SERVER_DATE_DEFAULT_FORMAT = new InjectionToken<string>('ARGON_SERVER_DATE_DEFAULT_FORMAT');
