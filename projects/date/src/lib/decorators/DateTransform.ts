import { HasDateAdapterInterface } from '../interfaces/HasDateAdapterInterface';
import isDate from 'lodash/isDate';

function parseValueToDate(value: Date | string | null, target: HasDateAdapterInterface): Date | string | null {
  return target.dateAdapter.anyToDate(value);
}

function convertValueToTargetFormat(
  value: Date | string | null, targetFormat: 'date' | 'server' | 'client', target: HasDateAdapterInterface
): Date | string | null {
  if (!isDate(value)) {
    return value;
  }
  switch (targetFormat) {
    case 'client': return target.dateAdapter.dateToClient(value as Date);
    case 'server': return target.dateAdapter.dateToServer(value as Date);
    case 'date': return value;
    default: return null;
  }
}

export function transformAnyToTargetFormat(
  value: Date | string | null, targetFormat: 'date' | 'server' | 'client', target: HasDateAdapterInterface
): Date | string | null {
  return convertValueToTargetFormat(parseValueToDate(value, target), targetFormat, target);
}

export function DateTransform(targetFormat: 'date' | 'server' | 'client') {
  return function DateTransformDecorator(target: HasDateAdapterInterface, propertyKey: string) {
    const originalDescriptor = Object.getOwnPropertyDescriptor(target, propertyKey);

    if (originalDescriptor) {
      throw new Error('@DateTransform does not supports descriptors');
    }

    const key = `_${propertyKey}`;

    Object.defineProperty(target, propertyKey, {
      configurable: true,
      enumerable: true,
      get() {
        if (!this.hasOwnProperty(key)) {
          Object.defineProperty(this, key, { enumerable: false, writable: true, configurable: false });
        }

        return transformAnyToTargetFormat(this[key], targetFormat, this);
      },
      set(value: any) {
        if (!this.hasOwnProperty(key)) {
          Object.defineProperty(this, key, { enumerable: false, writable: true, configurable: false });
        }

        this[key] = value;
      }
    });
  };
}

export function DateTransformArgument(targetFormat: 'date' | 'server' | 'client') {
  return function DateTransformArgumentsDecorator(
    // tslint:disable-next-line:ban-types
    target: HasDateAdapterInterface, propertyName: string, descriptor: TypedPropertyDescriptor<Function>
  ) {
    const originalMethod = descriptor.value;
    descriptor.value = function DecoratedFunction(value: Date | string, ...args: Array<any>) {
      originalMethod.apply(this, [transformAnyToTargetFormat(value, targetFormat, this), ...args]);
    };
  };
}
