import { DateTransform, DateTransformArgument } from './DateTransform';
import { HasDateAdapterInterface } from '../interfaces/HasDateAdapterInterface';
import { DateAdapter } from '../services/DateAdapter';

class MockDateAdapter extends DateAdapter {

  public date = new Date(2020, 1, 2, 0, 0, 0, 0);

  public client = '02.01.2020';

  public server = '2020-01-02T00:00:00Z';

  anyToDate(input: Date | string | null, formats?: Array<string>): Date | string | null {
    return this.date;
  }

  clientToDate(input: string | null, formats?: Array<string>): Date | string | null {
    return this.date;
  }

  clientToServer(input: string | null, formats?: Array<string>): string | null {
    return this.server;
  }

  dateToClient(date: Date | null, format?: string): string | null {
    return this.client;
  }

  dateToServer(date: Date | null): string | null {
    return this.server;
  }

  getClientFormats(): Array<string> {
    return [];
  }

  getDefaultClientFormat(): string {
    return '';
  }

  getDefaultServerFormat(): string {
    return '';
  }

  getServerFormats(): Array<string> {
    return [];
  }

  serverToClient(input: string | null, formats?: Array<string>): string | null {
    return this.client;
  }

  serverToDate(input: string | null, formats?: Array<string>): Date | string | null {
    return this.date;
  }

  setClientFormats(defaultFormat: string, additionalFormats: Array<string>) {
  }

}

describe('DateTransform', () => {
  describe('DateTransformDecorator', () => {
    class MockClass implements HasDateAdapterInterface {

      public dateAdapter = new MockDateAdapter();

      @DateTransform('date')
      public toDate: Date | string | null;

      @DateTransform('client')
      public toClient: Date | string | null;

      @DateTransform('server')
      public toServer: Date | string | null;

      @DateTransform('unsupported' as any)
      public unsupportedTarget: Date | string | null;

    }

    let mockClass: MockClass;

    beforeEach(() => {
      mockClass = new MockClass();
    });

    it('should throw error for the property with descriptor', () => {
      expect(() => {
        class SpyClass implements HasDateAdapterInterface {

          @DateTransform('server')
          public get property(): Date { return null; }

          public dateAdapter: DateAdapter;

        }
      }).toThrowError('@DateTransform does not supports descriptors');
    });

    it('should transform input to date', () => {
      mockClass.toDate = 'Input';
      expect(mockClass.toDate as any).toEqual(mockClass.dateAdapter.date);
    });

    it('should transform input to client', () => {
      mockClass.toClient = 'Input';
      expect(mockClass.toClient as any).toEqual(mockClass.dateAdapter.client);
    });

    it('should transform input to server', () => {
      mockClass.toServer = 'Input';
      expect(mockClass.toServer as any).toEqual(mockClass.dateAdapter.server);
    });

    it('should return null for unsupported target format', () => {
      mockClass.unsupportedTarget = 'Input';
      expect(mockClass.unsupportedTarget as any).toEqual(null);
    });

    it('should return plain value if date could not be parsed', () => {
      mockClass.dateAdapter.date = 'custom input' as any;
      mockClass.toClient = 'Input';
      expect(mockClass.toClient).toEqual('custom input');
    });
  });

  describe('@DateTransformArgument', () => {
    class MockClass implements HasDateAdapterInterface {

      public dateAdapter = new MockDateAdapter();

      public dateArgs: any;
      public clientArgs: any;
      public serverArgs: any;

      @DateTransformArgument('date')
      public handleDate(...args: Array<any>) {
        this.dateArgs = args;
      }

      @DateTransformArgument('client')
      public handleClient(...args: Array<any>) {
        this.clientArgs = args;
      }

      @DateTransformArgument('server')
      public handleServer(...args: Array<any>) {
        this.serverArgs = args;
      }
    }

    let mockClass: MockClass;
    const restArgs = [1, true, null, 'MockString'];

    beforeEach(() => {
      mockClass = new MockClass();
    });

    it('should transform first argument to date', () => {
      mockClass.handleDate('Input', ...restArgs);
      expect(mockClass.dateArgs).toEqual([
        mockClass.dateAdapter.date,
        ...restArgs
      ]);
    });

    it('should transform first argument to client', () => {
      mockClass.handleClient('Input', ...restArgs);
      expect(mockClass.clientArgs).toEqual([
        mockClass.dateAdapter.client,
        ...restArgs
      ]);
    });

    it('should transform first argument to server', () => {
      mockClass.handleServer('Input', ...restArgs);
      expect(mockClass.serverArgs).toEqual([
        mockClass.dateAdapter.server,
        ...restArgs
      ]);
    });
  });
});
