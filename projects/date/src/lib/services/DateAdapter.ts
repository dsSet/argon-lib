
export abstract class DateAdapter {

  public abstract dateToServer(date: Date | null): string | null;

  public abstract dateToClient(date: Date | null, format?: string): string | null;

  public abstract clientToServer(input: string | null, formats?: Array<string>): string | null;

  public abstract serverToClient(input: string | null, formats?: Array<string>): string | null;

  public abstract clientToDate(input: string | null, formats?: Array<string>): Date | null | string;

  public abstract serverToDate(input: string | null, formats?: Array<string>): Date | null | string;

  public abstract anyToDate(input: Date | string | null, formats?: Array<string>): Date | null | string;

  public abstract setClientFormats(defaultFormat: string, additionalFormats: Array<string>);

  public abstract getClientFormats(): Array<string>;

  public abstract getServerFormats(): Array<string>;

  public abstract getDefaultClientFormat(): string;

  public abstract getDefaultServerFormat(): string;

}
