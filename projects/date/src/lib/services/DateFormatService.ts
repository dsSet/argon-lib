

export abstract class DateFormatService {

  public abstract formatWeek(date: Date): string;

  public abstract formatWeekday(date: Date): string;

  public abstract formatDay(date: Date): string;

  public abstract formatMonth(date: Date): string;

  public abstract formatYear(date: Date): string;

  public abstract formatMonthYear(date: Date): string;

  public abstract formatMonthYearFull(date: Date): string;

}
