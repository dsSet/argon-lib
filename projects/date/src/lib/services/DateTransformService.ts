import { DatePeriodEnum } from '../types/DatePeriodEnum';
import { DateIntervalEnum } from '../types/DateIntervalEnum';

export abstract class DateTransformService {

  public abstract startOf(date: Date, ...periods: Array<DatePeriodEnum>): Date;

  public abstract endOf(date: Date, ...periods: Array<DatePeriodEnum>): Date;

  public abstract shiftDate(date: Date, ...commands: Array<['startOf' | 'endOf', DatePeriodEnum]>): Date;

  public abstract getDateIterator(start: Date, end: Date, period: DateIntervalEnum): IterableIterator<Date>;

  public abstract getMonthWeekdays(date: Date): Array<Date>;

  public abstract increment(date: Date, value: number, period: DateIntervalEnum): Date;

  public abstract decrement(date: Date, value: number, period: DateIntervalEnum): Date;

  public abstract isSameDay(date1: Date, date2: Date): boolean;

  public abstract isSameMonth(date1: Date, date2: Date): boolean;

  public abstract isSameYear(date1: Date, date2: Date): boolean;

  public abstract isSameWeek(date1: Date, date2: Date): boolean;

  public abstract isSameWeekday(date1: Date, date2: Date): boolean;

}
