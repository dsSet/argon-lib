import { DateAdapter } from '../services/DateAdapter';

export interface HasDateAdapterInterface {

  dateAdapter: DateAdapter;

}
