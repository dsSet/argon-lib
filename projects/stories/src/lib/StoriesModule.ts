import { NgModule } from '@angular/core';
import { ModalModule } from '@argon/modal';
import { DropdownModule } from '@argon/dropdown';
import { ModalWithDropdownComponent } from './components/ModalWithDropdown/ModalWithDropdownComponent';
import { ButtonsModule } from '@argon/buttons';
import { CheckboxModule } from '@argon/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { CheckboxFormComponent } from './components/CheckboxForm/CheckboxFormComponent';

@NgModule({
    imports: [
        ModalModule,
        DropdownModule,
        ButtonsModule,
        CheckboxModule,
        ReactiveFormsModule,
        FormsModule,
        PortalModule,
        CommonModule
    ],
    declarations: [
        ModalWithDropdownComponent,
        CheckboxFormComponent
    ],
    exports: [
        ModalModule,
        DropdownModule,
        ModalWithDropdownComponent,
        CheckboxFormComponent
    ]
})
export class StoriesModule { }
