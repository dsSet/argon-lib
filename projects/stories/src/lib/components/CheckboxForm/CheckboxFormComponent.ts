import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

@Component({
  selector: 'story-checkbox-form',
  templateUrl: './CheckboxFormComponent.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxFormComponent implements OnInit {

  public radioOptions = ['radio1', 'radio2', 'radio3'];

  public checkboxOptions = ['checkbox1', 'checkbox2', 'checkbox3'];

  form: UntypedFormGroup;

  i = 0;

  public checkbox: boolean;
  public radio: any;
  public list: Array<any>;

  ngOnInit(): void {
    this.createForm();
  }

  public createForm() {
    this.i++;
    this.form = new UntypedFormGroup({
      id: new UntypedFormControl(this.i),
      checkbox: new UntypedFormControl(),
      radio: new UntypedFormControl(),
      list: new UntypedFormControl(),
      group2: new UntypedFormGroup({
        radio: new UntypedFormControl(),
        list: new UntypedFormControl()
      })
    });
  }

  public toggleDisabled() {
    if (this.form.disabled) {
      this.form.enable();
    } else {
      this.form.disable();
    }
  }

}
