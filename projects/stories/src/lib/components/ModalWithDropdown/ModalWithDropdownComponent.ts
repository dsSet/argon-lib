import { Component, EventEmitter } from '@angular/core';
import { DialogInterface } from '@argon/modal';

@Component({
  selector: 'story-modal-with-dropdown',
  template: `
    <ar-dialog arBsSize="lg">
      <ar-dialog-header>
        <h5>Modal header</h5>
      </ar-dialog-header>
      <ar-dialog-body>
        <ar-dropdown-focusable>
          Click to open nested menu
          <ng-template>
            <ar-dropdown-menu>
              <ar-dropdown-menu-item>Menu Item 1</ar-dropdown-menu-item>
              <ar-dropdown-menu-item>Menu Item 2</ar-dropdown-menu-item>
              <ar-dropdown-menu-item>Menu Item 3</ar-dropdown-menu-item>
              <ar-dropdown-menu-item arCloseModal>Menu Item 4 (close modal)</ar-dropdown-menu-item>
            </ar-dropdown-menu>
          </ng-template>
        </ar-dropdown-focusable>
      </ar-dialog-body>
      <ar-dialog-footer>
        <button class="btn" (click)="close.emit()">Close</button>
      </ar-dialog-footer>
    </ar-dialog>
  `
})
export class ModalWithDropdownComponent implements DialogInterface {

  public close = new EventEmitter<void>();

}
