import { moduleMetadata, storiesOf } from '@storybook/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoriesModule } from './index';


storiesOf('Argon Mix|Modal/Modal & checkbox', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        StoriesModule
      ]
    })
  )
  .add('Checkboxes form inside modal', () => ({
    template: `
      <story-checkbox-form></story-checkbox-form>
    `
  }));
