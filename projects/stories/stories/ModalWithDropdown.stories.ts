import { moduleMetadata, storiesOf } from '@storybook/angular';
import { Component, OnInit } from '@angular/core';
import { ModalWithDropdownComponent, StoriesModule } from './index';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogProxy, DialogService } from '@argon/modal';

@Component({
  selector: 'story-modal-starter',
  template: `
    <ar-dropdown>
      Click me to open dropdown
      <ng-template>
        <a (click)="openModal()">Click to open modal</a>
      </ng-template>
    </ar-dropdown>
  `
})
class ModalStartComponent implements OnInit {

  public dialog: DialogProxy<ModalWithDropdownComponent>;

  constructor(
    private dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this.dialog = this.dialogService.createDialog(ModalWithDropdownComponent);
  }

  openModal() {
    this.dialog.open();
  }
}

storiesOf('Argon Mix|Modal/Modal & Dropdown mix', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        BrowserAnimationsModule,
        StoriesModule
      ],
      declarations: [
        ModalStartComponent
      ]
    })
  )
  .add('Dropdown inside and outside of modal', () => ({ component: ModalStartComponent, moduleMetadata: { } }));
