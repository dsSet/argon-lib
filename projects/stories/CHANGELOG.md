# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@2.0.3...@argon/stories@2.0.4) (2023-12-12)

**Note:** Version bump only for package @argon/stories





## [2.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@2.0.2...@argon/stories@2.0.3) (2023-10-18)

**Note:** Version bump only for package @argon/stories






## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@2.0.1...@argon/stories@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/stories





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@1.0.4...@argon/stories@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/stories






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@1.0.4...@argon/stories@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/stories






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@1.0.3...@argon/stories@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/stories





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@1.0.2...@argon/stories@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/stories





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@1.0.1...@argon/stories@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/stories






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.26...@argon/stories@1.0.1) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.26...@argon/stories@1.0.0) (2020-09-29)


### Reverts

* Revert "!refactor: entry components deprecation removal" ([b10eedb](https://bitbucket.org/dsSet/argon-lib/commits/b10eedb30526798c1173855fa1fc68db53d2679d))






## [0.2.26](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.25...@argon/stories@0.2.26) (2020-08-04)

**Note:** Version bump only for package @argon/stories






## [0.2.25](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.24...@argon/stories@0.2.25) (2020-06-24)

**Note:** Version bump only for package @argon/stories





## [0.2.24](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.23...@argon/stories@0.2.24) (2020-06-10)

**Note:** Version bump only for package @argon/stories





## [0.2.23](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.22...@argon/stories@0.2.23) (2020-06-04)

**Note:** Version bump only for package @argon/stories






## [0.2.22](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.21...@argon/stories@0.2.22) (2020-05-21)

**Note:** Version bump only for package @argon/stories






## [0.2.21](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.20...@argon/stories@0.2.21) (2020-04-18)

**Note:** Version bump only for package @argon/stories





## [0.2.20](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.19...@argon/stories@0.2.20) (2020-04-18)

**Note:** Version bump only for package @argon/stories





## [0.2.19](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.18...@argon/stories@0.2.19) (2020-04-18)

**Note:** Version bump only for package @argon/stories






## [0.2.18](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.17...@argon/stories@0.2.18) (2020-03-29)

**Note:** Version bump only for package @argon/stories





## [0.2.17](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.16...@argon/stories@0.2.17) (2020-03-24)

**Note:** Version bump only for package @argon/stories





## [0.2.16](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.15...@argon/stories@0.2.16) (2020-03-24)

**Note:** Version bump only for package @argon/stories





## [0.2.15](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.14...@argon/stories@0.2.15) (2020-03-24)

**Note:** Version bump only for package @argon/stories






## [0.2.14](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.13...@argon/stories@0.2.14) (2020-03-19)

**Note:** Version bump only for package @argon/stories






## [0.2.13](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.12...@argon/stories@0.2.13) (2020-03-18)

**Note:** Version bump only for package @argon/stories





## [0.2.12](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.11...@argon/stories@0.2.12) (2020-03-18)


### Bug Fixes

* **stories:** update stories ([3d2f504](https://bitbucket.org/dsSet/argon-lib/commits/3d2f504))





## [0.2.11](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.10...@argon/stories@0.2.11) (2020-02-14)

**Note:** Version bump only for package @argon/stories






## [0.2.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.9...@argon/stories@0.2.10) (2019-11-29)

**Note:** Version bump only for package @argon/stories





## [0.2.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.8...@argon/stories@0.2.9) (2019-11-22)

**Note:** Version bump only for package @argon/stories





## [0.2.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.7...@argon/stories@0.2.8) (2019-11-22)

**Note:** Version bump only for package @argon/stories





## [0.2.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.6...@argon/stories@0.2.7) (2019-11-22)

**Note:** Version bump only for package @argon/stories





## [0.2.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.5...@argon/stories@0.2.6) (2019-11-22)

**Note:** Version bump only for package @argon/stories






## [0.2.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.4...@argon/stories@0.2.5) (2019-10-31)

**Note:** Version bump only for package @argon/stories





## [0.2.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.3...@argon/stories@0.2.4) (2019-10-28)

**Note:** Version bump only for package @argon/stories





## [0.2.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.2...@argon/stories@0.2.3) (2019-10-11)

**Note:** Version bump only for package @argon/stories





## [0.2.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.1...@argon/stories@0.2.2) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))





## [0.2.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.2.0...@argon/stories@0.2.1) (2019-10-08)

**Note:** Version bump only for package @argon/stories





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.1.5...@argon/stories@0.2.0) (2019-09-21)


### Bug Fixes

* 🐛 story ([773e6c1](https://bitbucket.org/dsSet/argon-lib/commits/773e6c1))


### Features

* **storybook:** add form with checkboxes ([7929b55](https://bitbucket.org/dsSet/argon-lib/commits/7929b55))





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.1.4...@argon/stories@0.1.5) (2019-09-17)

**Note:** Version bump only for package @argon/stories





## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.1.3...@argon/stories@0.1.4) (2019-09-13)

**Note:** Version bump only for package @argon/stories






## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.1.2...@argon/stories@0.1.3) (2019-09-07)

**Note:** Version bump only for package @argon/stories





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.1.1...@argon/stories@0.1.2) (2019-09-06)

**Note:** Version bump only for package @argon/stories





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/stories@0.1.0...@argon/stories@0.1.1) (2019-08-14)

**Note:** Version bump only for package @argon/stories





# 0.1.0 (2019-08-11)


### Features

* **stories:** add stories package ([3ed8e62](https://bitbucket.org/dsSet/argon-lib/commits/3ed8e62))
