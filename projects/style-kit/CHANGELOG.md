# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@2.0.1...@argon/style-kit@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/style-kit





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@1.0.4...@argon/style-kit@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/style-kit






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@1.0.4...@argon/style-kit@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/style-kit






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@1.0.3...@argon/style-kit@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/style-kit





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@1.0.2...@argon/style-kit@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/style-kit





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@1.0.1...@argon/style-kit@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/style-kit






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.10...@argon/style-kit@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/style-kit





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.10...@argon/style-kit@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/style-kit






## [0.5.10](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.9...@argon/style-kit@0.5.10) (2020-08-04)

**Note:** Version bump only for package @argon/style-kit






## [0.5.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.8...@argon/style-kit@0.5.9) (2020-05-21)

**Note:** Version bump only for package @argon/style-kit






## [0.5.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.7...@argon/style-kit@0.5.8) (2020-04-18)

**Note:** Version bump only for package @argon/style-kit





## [0.5.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.6...@argon/style-kit@0.5.7) (2020-04-18)


### Bug Fixes

* **style-kit:** fix host binding props ([ed09b95](https://bitbucket.org/dsSet/argon-lib/commits/ed09b95))






## [0.5.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.5...@argon/style-kit@0.5.6) (2020-03-24)

**Note:** Version bump only for package @argon/style-kit





## [0.5.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.4...@argon/style-kit@0.5.5) (2020-03-24)

**Note:** Version bump only for package @argon/style-kit






## [0.5.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.3...@argon/style-kit@0.5.4) (2020-03-18)

**Note:** Version bump only for package @argon/style-kit





## [0.5.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.2...@argon/style-kit@0.5.3) (2020-02-14)

**Note:** Version bump only for package @argon/style-kit






## [0.5.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.1...@argon/style-kit@0.5.2) (2019-11-29)

**Note:** Version bump only for package @argon/style-kit





## [0.5.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.5.0...@argon/style-kit@0.5.1) (2019-11-22)

**Note:** Version bump only for package @argon/style-kit





# [0.5.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.4.0...@argon/style-kit@0.5.0) (2019-11-22)


### Bug Fixes

* **style-kit:** fix scroll container component selector ([8f3cc7c](https://bitbucket.org/dsSet/argon-lib/commits/8f3cc7c))
* **style-kit:** split scroll container component and directive ([3ee424d](https://bitbucket.org/dsSet/argon-lib/commits/3ee424d))


### Features

* **style-kit:** provide scroll container component as directive ([16a093a](https://bitbucket.org/dsSet/argon-lib/commits/16a093a))





# [0.4.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.3.1...@argon/style-kit@0.4.0) (2019-10-28)


### Bug Fixes

* **style-kit:** fix form control directive ([e58cf58](https://bitbucket.org/dsSet/argon-lib/commits/e58cf58))


### Features

* **style-kit:** add arTextCut directive ([eb6f8ac](https://bitbucket.org/dsSet/argon-lib/commits/eb6f8ac))





## [0.3.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.3.0...@argon/style-kit@0.3.1) (2019-10-11)

**Note:** Version bump only for package @argon/style-kit





# [0.3.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.2.0...@argon/style-kit@0.3.0) (2019-10-10)


### Bug Fixes

* **main:** fix package build ([3281c5d](https://bitbucket.org/dsSet/argon-lib/commits/3281c5d))


### Features

* **style-kit:** implement scroll container component ([c2b8aa9](https://bitbucket.org/dsSet/argon-lib/commits/c2b8aa9))





# [0.2.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.1.7...@argon/style-kit@0.2.0) (2019-10-08)


### Features

* **style-kit:** add FormControl directive ([cb37e45](https://bitbucket.org/dsSet/argon-lib/commits/cb37e45))
* **style-kit:** form control wrapper implementation ([3e48ea3](https://bitbucket.org/dsSet/argon-lib/commits/3e48ea3))





## [0.1.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.1.6...@argon/style-kit@0.1.7) (2019-09-21)

**Note:** Version bump only for package @argon/style-kit






## [0.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.1.5...@argon/style-kit@0.1.6) (2019-09-07)

**Note:** Version bump only for package @argon/style-kit





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.1.4...@argon/style-kit@0.1.5) (2019-08-11)

**Note:** Version bump only for package @argon/style-kit





## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.1.3...@argon/style-kit@0.1.4) (2019-07-28)

**Note:** Version bump only for package @argon/style-kit





## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.1.2...@argon/style-kit@0.1.3) (2019-07-14)

**Note:** Version bump only for package @argon/style-kit





## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.1.1...@argon/style-kit@0.1.2) (2019-07-14)


### Bug Fixes

* **dropdown:** mock import ([884e049](https://bitbucket.org/dsSet/argon-lib/commits/884e049))





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/style-kit@0.1.0...@argon/style-kit@0.1.1) (2019-07-14)

**Note:** Version bump only for package @argon/style-kit





# 0.1.0 (2019-07-14)


### Features

* **style-kit:** add package ([576c1d4](https://bitbucket.org/dsSet/argon-lib/commits/576c1d4))
