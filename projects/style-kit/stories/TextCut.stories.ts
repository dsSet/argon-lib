import { storiesOf, moduleMetadata } from '@storybook/angular';
import { StyleKitModule } from './index';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/TextCut.md').default
};

// tslint:disable-next-line:max-line-length
const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.`;

storiesOf('Argon|style-kit/TextCut', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        StyleKitModule
      ]
    })
  )
  .add('Basic usage', () => ({
    props: { loremIpsum },
    template: '<div style="width: 300px" [arTextCut]="loremIpsum">{{ loremIpsum }}</div>'
  }), { notes })
  .add('Use without title', () => ({
    props: { loremIpsum },
    template: '<div style="width: 300px" arTextCut>{{ loremIpsum }}</div>'
  }), { notes })
  .add('No overflow', () => ({
    props: { loremIpsum },
    template: '<div arTextCut="Title to display">Short message</div>'
  }), { notes });

