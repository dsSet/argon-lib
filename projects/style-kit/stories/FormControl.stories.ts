import { storiesOf, moduleMetadata } from '@storybook/angular';
import { UntypedFormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { StyleKitModule } from './index';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/FormControl.md').default
};


storiesOf('Argon|style-kit/Form Control', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        StyleKitModule,
        ReactiveFormsModule
      ]
    })
  )
  .add('Using as directive', () => ({
    props: {
      control: new UntypedFormControl(null, [Validators.required])
    },
    template: `
      <button (click)="control.disabled ? control.enable() : control.disable()">Disable toggle</button>
      <pre>Validation: required</pre>
      <input arBsSize="sm" arFormControl [formControl]="control" />
    `
  }), { notes })
  .add('Using as component', () => ({
    props: {
      control: new UntypedFormControl(null, [Validators.max(2)])
    },
    template: `
        <button (click)="control.disabled ? control.enable() : control.disable()">Disable toggle</button>
        <pre>Validation: max - 2</pre>
        <ar-form-control arBsSize="sm" >
          <input type="radio" [value]="1" [formControl]="control"/>
          <input type="radio" [value]="2" [formControl]="control"/>
          <input type="radio" [value]="3" [formControl]="control"/>
        </ar-form-control>
      `
  }), { notes });
