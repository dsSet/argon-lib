import { storiesOf, moduleMetadata } from '@storybook/angular';
import { StyleKitModule } from './index';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/Scrollbar.md').default
};

storiesOf('Argon|style-kit/Scrollbar', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        StyleKitModule
      ]
    })
  )
  .add('Scrollbar Styles', () => ({
    template: `
        <div arScrollbar style=" width: 20rem; height: 20rem; overflow-y: auto">
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
        </div>`
  }), { notes })
  .add('Scroll container', () => ({
    template: `
      <ar-scroll-container style=" width: 20rem; height: 20rem; ">
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem; width: 30rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
        </ar-scroll-container>
    `
  }), { notes })
  .add('Scroll container (disabled x scroll)', () => ({
    template: `
      <ar-scroll-container [xScroll]="false" style=" width: 20rem; height: 20rem; ">
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem; width: 30rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
        </ar-scroll-container>
    `
  }), { notes })
  .add('Scroll container (disabled y scroll)', () => ({
    template: `
      <ar-scroll-container [yScroll]="false" style=" width: 20rem; height: 20rem; ">
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem; width: 30rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
        </ar-scroll-container>
    `
  }), { notes })
  .add('Using scroll container as directive', () => ({
    template: `
        <div arScrollContainer [yScroll]="false" style=" width: 20rem; height: 20rem;">
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem; width: 30rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
          <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
        </div>`
  }), { notes });
