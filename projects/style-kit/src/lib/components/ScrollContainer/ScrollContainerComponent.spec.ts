import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { A11yModule } from '@angular/cdk/a11y';
import { ReactiveFormsModule } from '@angular/forms';
import { ScrollContainerComponent } from './ScrollContainerComponent';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';

describe('ScrollContainerComponent', () => {

  let component: ScrollContainerComponent;
  let fixture: ComponentFixture<ScrollContainerComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ ScrollContainerComponent ],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScrollContainerComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-scroll-container class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-scroll-container');
  });

  it('xScroll should toggle .ar-scroll-container--x class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'xScroll', 'ar-scroll-container--x');
  });

  it('yScroll should toggle .ar-scroll-container--y class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'yScroll', 'ar-scroll-container--y');
  });
});
