import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ScrollContainerDirective } from '../../directives/ScrollContainerDirective';

@Component({
  selector: 'ar-scroll-container',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScrollContainerComponent extends ScrollContainerDirective {

}
