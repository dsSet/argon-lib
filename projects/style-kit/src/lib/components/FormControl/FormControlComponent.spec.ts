import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { A11yModule } from '@angular/cdk/a11y';
import { ReactiveFormsModule } from '@angular/forms';
import { FormControlComponent } from './FormControlComponent';
import { FormControlDirective } from '../../directives/FormControlDirective';

describe('FormControlComponent', () => {

  let component: FormControlComponent;
  let fixture: ComponentFixture<FormControlComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ FormControlComponent ],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormControlComponent);
    component = fixture.componentInstance;
  });

  it('should extend FormControlDirective', () => {
    expect(component instanceof FormControlDirective).toBeTruthy();
  });

});
