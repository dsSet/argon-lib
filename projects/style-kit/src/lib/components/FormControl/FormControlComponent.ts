import {
  ChangeDetectionStrategy, ChangeDetectorRef,
  Component, ElementRef,
  Injector, NgZone,
  OnChanges,
  OnDestroy,
  OnInit, Optional,
  SimpleChanges
} from '@angular/core';
import { FormControlDirective } from '../../directives/FormControlDirective';
import { FocusMonitor } from '@angular/cdk/a11y';
import { NgControl } from '@angular/forms';

@Component({
  selector: 'ar-form-control',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormControlComponent extends FormControlDirective implements OnInit, OnDestroy, OnChanges {

  constructor(
    injector: Injector,
    protected element: ElementRef,
    protected focusMonitor: FocusMonitor,
    protected changeDetectorRef: ChangeDetectorRef,
    protected ngZone: NgZone,
    @Optional() protected parentControl: NgControl
  ) {
    super(injector, element, focusMonitor, changeDetectorRef, ngZone, parentControl);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
