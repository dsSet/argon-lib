import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ScrollbarDirective } from './ScrollbarDirective';

describe('ScrollbarDirective', () => {
  @Component({
    selector: 'spec-component',
    template: '<div arScrollbar></div>'
  })
  class FakeContainerComponent { }

  let component: FakeContainerComponent;
  let fixture: ComponentFixture<FakeContainerComponent>;
  let element: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ FakeContainerComponent, ScrollbarDirective ],
    });
    fixture = TestBed.createComponent(FakeContainerComponent);
    component = fixture.componentInstance;
    element = fixture.nativeElement.querySelector('div');
  });

  it('should decorate element with .ar-scrollbar class', () => {
    fixture.detectChanges();
    expect(element.classList.contains('ar-scrollbar')).toBeTruthy();
  });
});
