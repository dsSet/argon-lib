import { Component, ElementRef, Renderer2, ViewChild } from '@angular/core';
import { TextCutDirective } from './TextCutDirective';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HostBindingTestHelper } from '../../../../../test/helpers/HostBindingTestHelper';

describe('TextCutDirective', () => {

  @Component({ selector: 'spec-component', template: '<div #innerElement>Content</div>' })
  class MockComponent extends TextCutDirective {
    @ViewChild('innerElement', { static: true, read: ElementRef }) innerElement: ElementRef<any>;
    public get isTitleAvailable(): boolean { return this.hasTitle; }
    constructor(element: ElementRef<HTMLElement>, renderer: Renderer2) {
      super(element, renderer);
    }
    public setTitle(title: string) { this.renderer.setAttribute(this.element.nativeElement, 'title', title); }
    public setWidth(inner = 0, outer = 0) {
      this.renderer.setStyle(this.innerElement.nativeElement, 'width', `${inner}px`);
      this.renderer.setStyle(this.element.nativeElement, 'width', `${outer}px`);
      this.renderer.setStyle(this.innerElement.nativeElement, 'overflow', 'hidden');
      this.renderer.setStyle(this.innerElement.nativeElement, 'white-space', 'nowrap');
    }
  }

  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ MockComponent ],
    });
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
  });

  it('should decorate element with .ar-text-cut class', () => {
    fixture.detectChanges();
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-text-cut');
  });

  it('should add title attribute for text overflow', () => {
    fixture.detectChanges();
    component.arTextCut = 'Mock Title';
    component.setWidth(200, 150);
    fixture.detectChanges();
    expect(component.isTitleAvailable).toBeTruthy();
    expect(fixture.debugElement.attributes.title).toEqual(component.arTextCut);
  });

  it('should not add title if arTextCut not defined', () => {
    fixture.detectChanges();
    component.arTextCut = null;
    component.setWidth(200, 150);
    fixture.detectChanges();
    expect(component.isTitleAvailable).toBeFalsy();
    expect(fixture.debugElement.attributes.title).toBeFalsy();
  });

  it('should toggle title on resize', () => {
    fixture.detectChanges();
    component.arTextCut = 'Mock Title';

    component.setWidth(200, 150);
    fixture.detectChanges();
    expect(component.isTitleAvailable).toBeTruthy();
    expect(fixture.debugElement.attributes.title).toEqual(component.arTextCut);

    component.setWidth(200, 250);
    fixture.detectChanges();
    expect(component.isTitleAvailable).toBeFalsy();
    expect(fixture.debugElement.attributes.title).toBeFalsy();

    component.setWidth(800, 250);
    fixture.detectChanges();
    expect(component.isTitleAvailable).toBeTruthy();
    expect(fixture.debugElement.attributes.title).toEqual(component.arTextCut);

    component.setWidth(200, 200);
    fixture.detectChanges();
    expect(component.isTitleAvailable).toBeFalsy();
    expect(fixture.debugElement.attributes.title).toBeFalsy();
  });

  it('should not remove title if has no overflow', () => {
    fixture.detectChanges();
    const title = 'original Title';
    component.arTextCut = 'Mock title';
    component.setTitle(title);
    component.setWidth(150, 500);
    fixture.detectChanges();
    expect(fixture.debugElement.attributes.title).toEqual(title);
  });

});
