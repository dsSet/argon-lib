import {
  ChangeDetectorRef,
  ContentChild, Directive,
  ElementRef, EventEmitter,
  HostBinding,
  Injector,
  Input,
  NgZone, OnChanges,
  OnDestroy,
  OnInit,
  Optional, Output, SimpleChanges
} from '@angular/core';
import { ContextMixin, BsSizeType, BsContextType } from '@argon/bs-context';
import { FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import { NgControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Unsubscribable, UIShape } from '@argon/tools';

@Directive({
  selector: '[arFormControl]'
})
export class FormControlDirective extends ContextMixin(UIShape) implements OnInit, OnDestroy, OnChanges {

  @Input() arBsSize: BsSizeType;

  @Input() arBsContext: BsContextType;

  @Input() focused: boolean;

  @Output() click = new EventEmitter<void>();

  @HostBinding('class.ar-form-control') className = true;

  @HostBinding('class.ar-form-control--disabled')
  get isDisabled(): boolean { return this.control ? this.control.disabled : false; }

  @HostBinding('class.ar-form-control--focused')
  get isFocused(): boolean { return this.control ? !this.control.disabled && this.focused : this.focused; }

  @HostBinding('class.ar-form-control--valid')
  get valid(): boolean { return this.control ? !this.control.disabled && this.control.touched && this.control.valid : false; }

  @HostBinding('class.ar-form-control--invalid')
  get invalid(): boolean { return this.control ? !this.control.disabled && this.control.touched && this.control.invalid : false; }

  @ContentChild(NgControl) childControl: NgControl;

  get control(): NgControl | null {
    if (this.childControl) {
      return this.childControl;
    }

    return this.parentControl || null;
  }

  bsClassName = 'form-control';

  private focusSubscription: Subscription;

  private controlStateSubscription: Subscription;

  constructor(
    injector: Injector,
    protected element: ElementRef,
    protected focusMonitor: FocusMonitor,
    protected changeDetectorRef: ChangeDetectorRef,
    protected ngZone: NgZone,
    @Optional() protected parentControl: NgControl
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.focusSubscription = this.focusMonitor.monitor(this.element, true).subscribe(this.handleFocus);
    if (this.control && this.control.statusChanges) {
      this.controlStateSubscription = this.control.statusChanges.subscribe(() => {
        this.changeDetectorRef.markForCheck();
      });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    super.ngOnChanges(changes);
  }

  @Unsubscribable
  ngOnDestroy(): void {
  }

  private handleFocus = (origin: FocusOrigin) => {
    this.ngZone.run(() => {
      this.focused = Boolean(origin);
      this.changeDetectorRef.markForCheck();
    });
  }

}
