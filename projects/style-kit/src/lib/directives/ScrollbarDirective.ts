import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[arScrollbar]'
})
export class ScrollbarDirective {

  @HostBinding('class.ar-scrollbar') className = true;

}
