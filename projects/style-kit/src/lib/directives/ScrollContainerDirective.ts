import { Directive, HostBinding, Input } from '@angular/core';
import { ScrollbarDirective } from './ScrollbarDirective';

@Directive({
  selector: '[arScrollContainer]'
})
export class ScrollContainerDirective extends ScrollbarDirective {

  @HostBinding('class.ar-scroll-container') className = true;

  @HostBinding('class.ar-scroll-container--x') @Input() xScroll = true;

  @HostBinding('class.ar-scroll-container--y') @Input() yScroll = true;

}
