import { FormControlDirective } from './FormControlDirective';
import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { A11yModule, FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import { UntypedFormControl, ReactiveFormsModule } from '@angular/forms';
import createSpy = jasmine.createSpy;
import { Subject } from 'rxjs';


describe('FormControlDirective', () => {

  @Component({
    selector: 'spec-component',
    template: '<input #input arFormControl [formControl]="control" [focused]="focused"/>'
  })
  class MockComponent {
    @ViewChild('input', { read: ElementRef, static: true }) input: ElementRef<HTMLInputElement>;
    @ViewChild('input', { read: FormControlDirective, static: true }) directive: FormControlDirective;
    public control = new UntypedFormControl();
    public get classList(): DOMTokenList { return this.input.nativeElement.classList; }
    @Input() focused: boolean;
  }

  class MockFocusMonitor {
    monitorSubject = new Subject<FocusOrigin>();
    monitor = createSpy('monitor').and.returnValue(this.monitorSubject);
  }

  let component: MockComponent;
  let fixture: ComponentFixture<MockComponent>;
  let monitor: MockFocusMonitor;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ MockComponent, FormControlDirective ],
    providers: [
      { provide: FocusMonitor, useClass: MockFocusMonitor }
    ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(MockComponent);
    component = fixture.componentInstance;
    monitor = TestBed.inject(FocusMonitor) as any;
    fixture.detectChanges();
  });

  afterEach(() => {
    monitor.monitorSubject.complete();
  });

  it('should add .form-control class', () => {
    expect(component.classList.contains('form-control')).toBeTruthy();
  });

  it('should add .ar-form-control class', () => {
    expect(component.classList.contains('form-control')).toBeTruthy();
  });

  it('focused input should bind .ar-form-control--focused class', () => {
    expect(component.classList.contains('ar-form-control--focused')).toBeFalsy();
    component.focused = true;
    fixture.detectChanges();
    expect(component.classList.contains('ar-form-control--focused')).toBeTruthy();
    component.control.disable();
    fixture.detectChanges();
    expect(component.classList.contains('ar-form-control--focused')).toBeFalsy();
  });

  it('arFormControl should pick up control from input', () => {
    expect(component.directive.control.control).toEqual(component.control);
  });

  it('should base ar-form-control--disabled on control state', () => {
    expect(component.classList.contains('ar-form-control--disabled')).toBeFalsy();
    component.control.disable();
    fixture.detectChanges();
    expect(component.classList.contains('ar-form-control--disabled')).toBeTruthy();
  });

  it('should base ar-form-control--valid on control state', () => {
    expect(component.classList.contains('ar-form-control--valid')).toBeFalsy();
    component.control.markAllAsTouched();
    fixture.detectChanges();
    expect(component.classList.contains('ar-form-control--valid')).toBeTruthy();
    component.control.disable();
    fixture.detectChanges();
    expect(component.classList.contains('ar-form-control--valid')).toBeFalsy();
  });

  it('should base ar-form-control--invalid on control state', () => {
    expect(component.classList.contains('ar-form-control--invalid')).toBeFalsy();
    component.control.markAllAsTouched();
    component.control.setErrors({ mock: true });
    fixture.detectChanges();
    expect(component.classList.contains('ar-form-control--invalid')).toBeTruthy();
    component.control.disable();
    fixture.detectChanges();
    expect(component.classList.contains('ar-form-control--invalid')).toBeFalsy();
  });

  it('should connect to the focus monitor', () => {
    expect(monitor.monitor).toHaveBeenCalledWith(component.input, true);
  });

  it('monitor changes should update focus property', () => {
    monitor.monitorSubject.next('keyboard');
    expect(component.directive.focused).toEqual(true);
    monitor.monitorSubject.next(null);
    expect(component.directive.focused).toEqual(false);
  });
});
