import { AfterViewChecked, Directive, ElementRef, HostBinding, Input, Renderer2 } from '@angular/core';


@Directive({
  selector: '[arTextCut]'
})
export class TextCutDirective implements AfterViewChecked {

  @Input() arTextCut: string;

  @HostBinding('class.ar-text-cut') className = true;

  protected hasTitle: boolean;

  constructor(
    protected element: ElementRef<HTMLElement>,
    protected renderer: Renderer2
  ) { }

  ngAfterViewChecked(): void {
    if (!this.arTextCut) {
      return;
    }

    const scrollWidth = this.element.nativeElement.scrollWidth;
    const clientWidth = this.element.nativeElement.clientWidth;

    if (scrollWidth > clientWidth && !this.hasTitle) {
      this.hasTitle = true;
      this.renderer.setAttribute(this.element.nativeElement, 'title', this.arTextCut);
    } else if (scrollWidth <= clientWidth && this.hasTitle) {
      this.hasTitle = false;
      this.renderer.removeAttribute(this.element.nativeElement, 'title');
    }
  }

}
