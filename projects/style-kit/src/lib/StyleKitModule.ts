import { NgModule } from '@angular/core';
import { ScrollbarDirective } from './directives/ScrollbarDirective';
import { FormControlComponent } from './components/FormControl/FormControlComponent';
import { FormControlDirective } from './directives/FormControlDirective';
import { ScrollContainerComponent } from './components/ScrollContainer/ScrollContainerComponent';
import { A11yModule } from '@angular/cdk/a11y';
import { TextCutDirective } from './directives/TextCutDirective';
import { ScrollContainerDirective } from './directives/ScrollContainerDirective';

@NgModule({
  imports: [
    A11yModule
  ],
  declarations: [
    ScrollbarDirective,
    FormControlComponent,
    FormControlDirective,
    ScrollContainerComponent,
    ScrollContainerDirective,
    TextCutDirective,
  ],
  exports: [
    ScrollbarDirective,
    FormControlComponent,
    FormControlDirective,
    ScrollContainerComponent,
    ScrollContainerDirective,
    TextCutDirective
  ]
})
export class StyleKitModule { }
