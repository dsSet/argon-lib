#arTextCut directive

Directive will add text ellipsis overflow and will add html title attribute with full text to the truncated element

|Property |Default value|Description
|---      |---          |---
|`@Input() arTextCut: string` | `undefined` | Text to display with title attribute 

##Base usage

````html
<div arTextCut="This message will appear as title in text overflow case">
  Any content for cut.
</div>
````

##Use without title

````html
<div arTextCut>
  This text have no title on text overflow
</div>
````
