# Form Control

## `[arFormControl]` directive

Base form control view presentation. Compatible with bootstrap form control styles.
Also manage class names using hosted formControl or child form control.

|Property |Default value|Description
|---      |---          |---
|`@Input() focused: boolean` | `undefined` | Control focused state. You don't need to manage this property manually for the most cases. Directive set up this property automatically basing on `FocusMonitor` from angular cdk.
|`@Input() arBsSize: BsSizeEnum` | `undefined` | Bootstrap like size
|`@Input() arBsContext: BsContextEnum` | `undefined` | Bootstrap like color context

### Usage

#### With host

````html
<input arBsSize="sm" arFormControl [formControl]="control" />
```` 

#### With content

If you just need to form control wrapper then use `<ar-form-control>` component. This is `[arFormControl]` directive alias.

````html
<ar-form-control arBsSize="sm" >
  <input type="radio" [value]="1" [formControl]="control"/>
  <input type="radio" [value]="2" [formControl]="control"/>
  <input type="radio" [value]="3" [formControl]="control"/>
</ar-form-control>
````
