# Scroll

## `[arScrollbar]` directive

Add scrollbar classes to the markup. Can be used to customize app scrollbar. 

### Usage

````html
<div arScrollbar style=" width: 20rem; height: 20rem; overflow-y: auto">
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
</div>
````

## `<ar-scroll-container>`, `[arScrollContainer]` component

`[arScrollbar]` directive extension

|Property |Default value|Description
|---      |---          |---
|`@Input() xScroll: boolean` | `true` | Show horizontal scrollbar 
|`@Input() yScroll: boolean` | `true` | Show vertical scrollbar 

### Usage

````html
<ar-scroll-container style=" width: 20rem; height: 20rem; overflow-y: auto">
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem; width: 30rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
</ar-scroll-container>
````


````html
<div arScrollContainer [yScroll]="false" style=" width: 20rem; height: 20rem;">
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem; width: 30rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
  <div style="background-color: #f9cd0b; height: 3rem; margin-bottom: 3rem;">Content block</div>
</div>
````


