# @argon/style-kit package

## Versions

- `Angular`: ^15.2.9
- `Rxjs`: ~6.6.3  
- `Cdk`: ^15.2.9

## Install

> npm i --save @argon/style-kit 

## Documentation

[@argon/style-kit documentation](./docs/index.md)

## Package content

| Source | Content |
|--------|---------|
|`@argon/style-kit/assets` | static assets folder |
|`@argon/style-kit/style` | styles folder |
|`@argon/style-kit/stories` | storybook folder |
|`@argon/style-kit/docs` | documentation folder |

## Styling components

Package component are supporting Bootstrap 4 styles. Package also contains custom styles according by BEM technology.

> Custom styles should be included globally.

Custom styles can be applied in 3 ways.

### Include styles as is

Include `index.scss` file to your global app styles

```scss
@import "~@argon/style-kit/style/index.scss";
```  

### Override default variables

```scss
@import "~@argon/style-kit/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/style-kit/style/index.scss";
```

### Use mixins optional

```scss
@import "~@argon/style-kit/style/variables.scss";

$package-variable: my-custom-value;

@import "~@argon/style-kit/style/[package-mixin].scss";

.ar-component-to-styled {
  @include any-package-mixin();
  color: red;
}

```  

Otherwise you can skip components default styles and implement new ones by yourself. 
