# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@2.0.2...@argon/tags@2.0.3) (2023-12-12)


### Bug Fixes

* update control value accessors to handle disabled state correctly ([b9d29c5](https://bitbucket.org/dsSet/argon-lib/commits/b9d29c56fb1ba76738ce5fbf2824ede9b736af82))






## [2.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@2.0.1...@argon/tags@2.0.2) (2023-10-13)

**Note:** Version bump only for package @argon/tags





## [2.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@1.0.4...@argon/tags@2.0.1) (2023-10-12)

**Note:** Version bump only for package @argon/tags






# [2.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@1.0.4...@argon/tags@2.0.0) (2023-10-12)

**Note:** Version bump only for package @argon/tags






## [1.0.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@1.0.3...@argon/tags@1.0.4) (2020-10-02)

**Note:** Version bump only for package @argon/tags





## [1.0.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@1.0.2...@argon/tags@1.0.3) (2020-10-02)

**Note:** Version bump only for package @argon/tags





## [1.0.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@1.0.1...@argon/tags@1.0.2) (2020-10-02)

**Note:** Version bump only for package @argon/tags






## [1.0.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.9...@argon/tags@1.0.1) (2020-09-29)

**Note:** Version bump only for package @argon/tags





# [1.0.0](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.9...@argon/tags@1.0.0) (2020-09-29)

**Note:** Version bump only for package @argon/tags






## [0.1.9](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.8...@argon/tags@0.1.9) (2020-08-04)

**Note:** Version bump only for package @argon/tags






## [0.1.8](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.7...@argon/tags@0.1.8) (2020-06-04)


### Bug Fixes

* **tags:** remove unused interface ([0eb9e32](https://bitbucket.org/dsSet/argon-lib/commits/0eb9e32))






## [0.1.7](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.6...@argon/tags@0.1.7) (2020-05-21)

**Note:** Version bump only for package @argon/tags






## [0.1.6](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.5...@argon/tags@0.1.6) (2020-04-18)

**Note:** Version bump only for package @argon/tags





## [0.1.5](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.4...@argon/tags@0.1.5) (2020-04-18)

**Note:** Version bump only for package @argon/tags






## [0.1.4](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.3...@argon/tags@0.1.4) (2020-03-24)

**Note:** Version bump only for package @argon/tags





## [0.1.3](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.2...@argon/tags@0.1.3) (2020-03-24)

**Note:** Version bump only for package @argon/tags






## [0.1.2](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.1...@argon/tags@0.1.2) (2020-03-18)

**Note:** Version bump only for package @argon/tags





## [0.1.1](https://bitbucket.org/dsSet/argon-lib/compare/@argon/tags@0.1.0...@argon/tags@0.1.1) (2020-02-17)


### Bug Fixes

* **tags:** fix mixin initialization ([d5ae102](https://bitbucket.org/dsSet/argon-lib/commits/d5ae102))





# 0.1.0 (2020-02-14)


### Bug Fixes

* **tags:** fix build ([ab0b74c](https://bitbucket.org/dsSet/argon-lib/commits/ab0b74c))


### Features

* **tag-input:** initial commit ([b89ef00](https://bitbucket.org/dsSet/argon-lib/commits/b89ef00))
* **tags:** added styles for the package ([8b4f2e2](https://bitbucket.org/dsSet/argon-lib/commits/8b4f2e2))
* **tags:** implement base tags input ([6b8f12e](https://bitbucket.org/dsSet/argon-lib/commits/6b8f12e))
