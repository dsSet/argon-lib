import { CommonModule } from '@angular/common';
import { UntypedFormControl, ReactiveFormsModule, Validators } from '@angular/forms';
import { moduleMetadata, storiesOf } from '@storybook/angular';
import { TagsModule, TagValidators } from './index';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

storiesOf('Argon|tags/Tags Input', module)
  .addDecorator(
      moduleMetadata({
        imports: [CommonModule, TagsModule, ReactiveFormsModule]
      })
    )
    .add('Base', () => ({
      props: {
        control: new UntypedFormControl(['tag 12', 'Random tag 4555'])
      },
      template: `
        <div style="width: 500px">
          <ar-tags-input [formControl]="control">
            <ar-tags-set>
              <ar-tag-editor></ar-tag-editor>
            </ar-tags-set>
          </ar-tags-input>
        </div>
      `
    }))
    .add('Disabled', () => ({
      props: {
        control: new UntypedFormControl({ value: ['tag 12', 'Random tag 4555'], disabled: true })
      },
      template: `
          <div style="width: 500px">
            <ar-tags-input [formControl]="control">
              <ar-tags-set>
                <ar-tag-editor></ar-tag-editor>
              </ar-tags-set>
            </ar-tags-input>
          </div>
        `
    }))
    .add('Only uniq tags allowed', () => ({
      props: {
        control: new UntypedFormControl(['tag 12', 'Random tag 4555'])
      },
      template: `
          <div style="width: 500px">
            <ar-tags-input [formControl]="control">
              <ar-tags-set>
                <ar-tag-editor [allowDuplicates]="false"></ar-tag-editor>
              </ar-tags-set>
            </ar-tags-input>
          </div>
        `
    }))
  .add('Validation (required)', () => ({
    props: {
      control: new UntypedFormControl(['tag 12', 'Random tag 4555'], [Validators.required])
    },
    template: `
          <div style="width: 500px">
            <ar-tags-input [formControl]="control">
              <ar-tags-set>
                <ar-tag-editor [allowDuplicates]="false"></ar-tag-editor>
              </ar-tags-set>
            </ar-tags-input>
          </div>
        `
  }))
  .add('Validation (uniq validator)', () => ({
    props: {
      control: new UntypedFormControl(['tag 12', 'Random tag 4555'], [TagValidators.Uniq])
    },
    template: `
          <div style="width: 500px">
            <ar-tags-input [formControl]="control">
              <ar-tags-set>
                <ar-tag-editor></ar-tag-editor>
              </ar-tags-set>
            </ar-tags-input>
          </div>
        `
  }));
