/*
 * Public API Surface
 */

export { TagEditorComponent } from './lib/components/TagEditor/TagEditorComponent';
export { TagsInputComponent } from './lib/components/TagsInput/TagsInputComponent';
export { TagsSetComponent } from './lib/components/TagsSet/TagsSetComponent';

export { TagsModule } from './lib/TagsModule';

export { TagValidators } from './lib/validators/TagValidators';
