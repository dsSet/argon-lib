import uniq from 'lodash/uniq';
import size from 'lodash/size';
import { AbstractControl, ValidationErrors } from '@angular/forms';

export class TagValidators {

  static Uniq(control: AbstractControl): ValidationErrors | null {
    const value = control.value || [];
    return size(uniq(value)) !== size(value)
      ? { TagsUniq: true }
      : null;
  }

}
