import { A11yModule } from '@angular/cdk/a11y';
import { Component, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { TagsSetComponent } from '../components/TagsSet/TagsSetComponent';
import { TagsInputManager } from '../services/TagsInputManager';
import { TagEditorDirective } from './TagEditorDirective';
import createSpyObj = jasmine.createSpyObj;

describe('TagEditorDirective', () => {

  @Component({ selector: 'spec-component', template: '<input arTagEditor />' })
  class FakeComponent {
    @ViewChild(TagEditorDirective, { static: true, read: TagEditorDirective}) directive: TagEditorDirective;
  }

  let component: FakeComponent;
  let fixture: ComponentFixture<FakeComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ FakeComponent, TagEditorDirective ],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [TagsInputManager]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(FakeComponent);
    component = fixture.componentInstance;
  });

  it('inputBlur should drop value', () => {
    fixture.detectChanges();
    component.directive.value = 'qwe';
    component.directive.inputBlur();
    expect(component.directive.value).toEqual('');
  });

  it('inputBlur should call blur action', () => {
    fixture.detectChanges();
    spyOn(component.directive.element.nativeElement, 'blur');
    component.directive.inputBlur();
    expect(component.directive.element.nativeElement.blur).toHaveBeenCalled();
  });

  it('inputFocus should set initial value', () => {
    fixture.detectChanges();
    component.directive.inputFocus('test');
    expect(component.directive.value).toEqual('test');
    component.directive.inputFocus();
    expect(component.directive.value).toEqual('');
  });

  it('inputFocus should call focus action', () => {
    fixture.detectChanges();
    spyOn(component.directive.element.nativeElement, 'focus');
    component.directive.inputFocus();
    expect(component.directive.element.nativeElement.focus).toHaveBeenCalled();
  });

  it('clear should clean up value', () => {
    fixture.detectChanges();
    component.directive.value = 'qweqwe';
    component.directive.clear();
    expect(component.directive.value).toEqual('');
  });

  it('clear should call updateWidth', () => {
    fixture.detectChanges();
    spyOn(component.directive, 'updateWidth');
    component.directive.clear();
    expect(component.directive.updateWidth).toHaveBeenCalled();
  });

  it('handleChange should emit valueChange with current value', () => {
    const expected = 'EXPECTED VALUE';
    fixture.detectChanges();
    component.directive.value = expected;
    spyOn(component.directive.valueChange, 'emit');
    component.directive.handleChange();
    expect(component.directive.valueChange.emit).toHaveBeenCalledWith(expected);
  });

  it('handleKeydown should call event.stopPropagation if has value', () => {
    fixture.detectChanges();
    component.directive.value = '123';
    const event = createSpyObj(['stopPropagation']);
    component.directive.handleKeydown(event);
    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('handleKeydown should not call event.stopPropagation if has no value', () => {
    fixture.detectChanges();
    component.directive.value = '';
    const event = createSpyObj(['stopPropagation']);
    component.directive.handleKeydown(event);
    expect(event.stopPropagation).not.toHaveBeenCalled();
  });

  it('handleKeydown should emit valueChange with current value if Enter was pressed', () => {
    const event = new KeyboardEvent('keydown', { code: 'Enter' });
    const expectedValue = 'EXPECTED VALUE';
    spyOn(component.directive.valueChange, 'emit');
    component.directive.value = expectedValue;
    component.directive.handleKeydown(event);
    expect(component.directive.valueChange.emit).toHaveBeenCalledWith(expectedValue);
  });

  it('handleKeydown should clean value if Escape key was pressed', () => {
    const event = new KeyboardEvent('keydown', { code: 'Escape' });
    component.directive.value = 'qwe';
    component.directive.handleKeydown(event);
    expect(component.directive.value).toEqual('');
  });

  it('handleKeydown should call inputBlur if Escape key was pressed', () => {
    const event = new KeyboardEvent('keydown', { code: 'Escape' });
    spyOn(component.directive, 'inputBlur');
    component.directive.handleKeydown(event);
    expect(component.directive.inputBlur).toHaveBeenCalled();
  });
});
