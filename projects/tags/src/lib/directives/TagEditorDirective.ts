import size from 'lodash/size';
import { Directive, ElementRef, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Output, Renderer2 } from '@angular/core';
import { Unsubscribable } from '@argon/tools';
import { asapScheduler, asyncScheduler, fromEvent, scheduled, Subscription } from 'rxjs';
import { mergeAll } from 'rxjs/operators';

@Directive({
  selector: 'input[arTagEditor]'
})
export class TagEditorDirective implements OnInit, OnDestroy {

  @HostBinding('class.ar-tag-editor-input') className = true;

  @Output() valueChange = new EventEmitter<string>();

  @Output() resize = new EventEmitter<void>();

  public get value(): string { return this.element.nativeElement.value; }
  public set value(value: string) { this.element.nativeElement.value = value; }

  public get inputSizeCH(): number {
    const valueSize = size(this.value) || 1;
    return (1 + 1 / Math.log(valueSize + 1)) + valueSize;
  }

  private changeSubscription: Subscription;
  private keydownSubscription: Subscription;
  private updateWidthSubscription: Subscription;

  constructor(
    public element: ElementRef<HTMLInputElement>,
    public renderer: Renderer2
  ) { }

  ngOnInit(): void {
    this.updateWidth();
    this.changeSubscription = fromEvent(this.element.nativeElement, 'change').subscribe(this.handleChange);
    this.keydownSubscription = fromEvent(this.element.nativeElement, 'keydown').subscribe(this.handleKeydown);
    this.updateWidthSubscription = scheduled([
        fromEvent(this.element.nativeElement, 'input'),
        fromEvent(this.element.nativeElement, 'focus'),
        fromEvent(this.element.nativeElement, 'blur')
      ],
      asyncScheduler
    ).pipe(mergeAll()).subscribe(this.updateWidth);
  }

  @Unsubscribable
  ngOnDestroy(): void {
  }

  public inputBlur() {
    this.element.nativeElement.blur();
    this.value = '';
  }

  public inputFocus(value: string = '') {
    this.value = value;
    this.element.nativeElement.focus();
  }

  public clear() {
    this.value = '';
    this.updateWidth();
  }

  public handleChange = () => {
    this.valueChange.emit(this.value);
  }

  public handleKeydown = (event: KeyboardEvent) => {
    if (this.value) {
      event.stopPropagation();
    }

    if (event.code === 'Enter') {
      this.valueChange.emit(this.value);
    } else if (event.code === 'Escape') {
      this.value = '';
      this.inputBlur();
    }
  }

  public updateWidth = () => {
    this.renderer.setStyle(this.element.nativeElement, 'width', `${this.inputSizeCH}ch`);
  }

}
