import { Directive, ElementRef, Injector, OnDestroy, OnInit } from '@angular/core';
import { KeyCodesEnum, KeyboardService } from '@argon/keyboard';
import { Unsubscribable, BaseShape } from '@argon/tools';
import { Subscription } from 'rxjs';
import { TagsInputMixin } from '../mixins/TagsInputMixin';

@Directive({
  selector: '[arTagInputKeyManager]'
})
export class TagInputKeyManagerDirective
  extends TagsInputMixin(BaseShape)
  implements OnInit, OnDestroy {

  static KEY_MAP = [
    KeyCodesEnum.arrowLeft,
    KeyCodesEnum.arrowRight,
    KeyCodesEnum.Delete,
    KeyCodesEnum.Backspace
  ];

  private keySubscription: Subscription;

  constructor(
    private element: ElementRef<HTMLElement>,
    private keyboardService: KeyboardService,
    injector: Injector
  ) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.keySubscription = this.keyboardService.listenKeyDownEvent(
      TagInputKeyManagerDirective.KEY_MAP, this.element.nativeElement
    ).subscribe(this.handleKey);
  }

  @Unsubscribable
  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public handleKey = (event: KeyboardEvent) => {
    switch (event.code) {
      case KeyCodesEnum.arrowLeft:
        this.registry.selectPrev();
        break;
      case KeyCodesEnum.arrowRight:
        this.registry.selectNext();
        break;
      case KeyCodesEnum.Delete:
        this.registry.removeSelected(true);
        break;
      case KeyCodesEnum.Backspace:
        this.registry.removeOrSelectLast();
        break;
    }
  }

}
