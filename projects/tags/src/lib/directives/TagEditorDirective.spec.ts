import { A11yModule } from '@angular/cdk/a11y';
import { Component, NO_ERRORS_SCHEMA, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { KeyboardModule, KeyCodesEnum } from '@argon/keyboard';
import { TagsSetComponent } from '../components/TagsSet/TagsSetComponent';
import { TagsInputManager } from '../services/TagsInputManager';
import { TagEditorDirective } from './TagEditorDirective';
import { TagInputKeyManagerDirective } from './TagInputKeyManagerDirective';
import createSpyObj = jasmine.createSpyObj;

describe('TagInputKeyManagerDirective', () => {

  @Component({ selector: 'spec-component', template: '<input arTagInputKeyManager />' })
  class FakeComponent {
    @ViewChild(TagInputKeyManagerDirective, { static: true, read: TagInputKeyManagerDirective}) directive: TagInputKeyManagerDirective;
  }

  let component: FakeComponent;
  let fixture: ComponentFixture<FakeComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule, KeyboardModule ],
    declarations: [ FakeComponent, TagInputKeyManagerDirective ],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [TagsInputManager]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(FakeComponent);
    component = fixture.componentInstance;
  });

  it('handleKey should call selectPrev for arrow left', () => {
    fixture.detectChanges();
    spyOn(component.directive.registry, 'selectPrev');
    const event = new KeyboardEvent('keydown', { code: KeyCodesEnum.arrowLeft });
    component.directive.handleKey(event);
    expect(component.directive.registry.selectPrev).toHaveBeenCalled();
  });

  it('handleKey should call selectNext for arrow right', () => {
    fixture.detectChanges();
    spyOn(component.directive.registry, 'selectNext');
    const event = new KeyboardEvent('keydown', { code: KeyCodesEnum.arrowRight });
    component.directive.handleKey(event);
    expect(component.directive.registry.selectNext).toHaveBeenCalled();
  });

  it('handleKey should call removeSelected for Delete key', () => {
    fixture.detectChanges();
    spyOn(component.directive.registry, 'removeSelected');
    const event = new KeyboardEvent('keydown', { code: KeyCodesEnum.Delete });
    component.directive.handleKey(event);
    expect(component.directive.registry.removeSelected).toHaveBeenCalledWith(true);
  });

  it('handleKey should call removeOrSelectLast for Backspace key', () => {
    fixture.detectChanges();
    spyOn(component.directive.registry, 'removeOrSelectLast');
    const event = new KeyboardEvent('keydown', { code: KeyCodesEnum.Backspace });
    component.directive.handleKey(event);
    expect(component.directive.registry.removeOrSelectLast).toHaveBeenCalled();
  });
});
