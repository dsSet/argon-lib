
export interface TagsInputStateInterface {

  tags: Array<string>;

  isExternal: boolean;

  focused: boolean;

  disabled: boolean;

  selected: number;

}
