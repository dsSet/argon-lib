import { ChangeDetectorRef } from '@angular/core';
import { StateInterface } from '@argon/tools';
import { TagsInputManager } from '../services/TagsInputManager';
import { TagsInputStateInterface } from './TagsInputStateInterface';

export interface TagsInputRegistryExtensionInterface {

  registry: TagsInputManager;

  changeDetectorRef: ChangeDetectorRef;

  ngOnInit(): void;

  ngOnDestroy(): void;

  handleUpdateState(state: StateInterface<TagsInputStateInterface>): void;

}
