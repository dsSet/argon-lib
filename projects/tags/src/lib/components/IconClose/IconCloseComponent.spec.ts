import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { A11yModule } from '@angular/cdk/a11y';
import { ReactiveFormsModule } from '@angular/forms';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { IconCloseComponent } from './IconCloseComponent';

describe('IconCloseComponent', () => {

  let component: IconCloseComponent;
  let fixture: ComponentFixture<IconCloseComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ IconCloseComponent ],
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconCloseComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-tag-close-icon class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-tag-close-icon');
  });
});
