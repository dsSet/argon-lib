import trim from 'lodash/trim';
import { ChangeDetectionStrategy, Component, HostBinding, Injector, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BaseShape, StateInterface, CompareValuesMixin, CompareFn, Comparators } from '@argon/tools';
import { TagEditorDirective } from '../../directives/TagEditorDirective';
import { TagsInputStateInterface } from '../../interfaces/TagsInputStateInterface';
import { TagsInputMixin } from '../../mixins/TagsInputMixin';

@Component({
  selector: 'ar-tag-editor',
  template: `
    <input arTagEditor
           class="ar-tag-editor__input"
           (valueChange)="handleCreateTag($event)"
           (blur)="handleBlur()"
           (focus)="handleFocus()"
    />
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagEditorComponent extends TagsInputMixin(CompareValuesMixin(BaseShape))
  implements OnInit, OnDestroy  {

  @HostBinding('class.ar-tag-editor') className = true;

  @ViewChild(TagEditorDirective, { static: true, read: TagEditorDirective }) editor: TagEditorDirective;

  @Input() allowDuplicates = true;

  @Input() compareFn: CompareFn = Comparators.caseInsensitiveComparator;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public handleCreateTag(value: string) {
    const valueToAdd = trim(value);
    if (this.shouldCreateTag(valueToAdd)) {
      this.registry.addTag(valueToAdd);
      this.editor.clear();
    }
  }

  public shouldCreateTag(value: string): boolean {
    if (!value) {
      return false;
    }

    if (this.allowDuplicates) {
      return true;
    }

    return !this.find(this.registry.tags, value);
  }

  public handleBlur() {
    this.registry.setFocused(false);
  }

  public handleFocus() {
    this.registry.setFocused(true);
  }

  handleUpdateState(state: StateInterface<TagsInputStateInterface>): void {
    const { changes } = state;
    const { focused } = changes;
    if (focused && focused.currentValue) {
      this.editor.inputFocus();
    }
  }



}
