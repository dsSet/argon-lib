import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { A11yModule } from '@angular/cdk/a11y';
import { ReactiveFormsModule } from '@angular/forms';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { TagEditorDirective } from '../../directives/TagEditorDirective';
import { TagsInputManager } from '../../services/TagsInputManager';
import { TagEditorComponent } from './TagEditorComponent';

describe('TagEditorComponent', () => {

  let component: TagEditorComponent;
  let fixture: ComponentFixture<TagEditorComponent>;
  let registry: TagsInputManager;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ TagEditorComponent, TagEditorDirective ],
    providers: [ TagsInputManager ]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagEditorComponent);
    component = fixture.componentInstance;
    registry = TestBed.inject(TagsInputManager) as any;
  });

  it('className should toggle .ar-tag-editor class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-tag-editor');
  });

  it('should have default values', () => {
    fixture.detectChanges();
    expect(component.allowDuplicates).toEqual(true);
    expect(component.editor instanceof TagEditorDirective).toBeTruthy();
    expect(component.registry instanceof TagsInputManager).toBeTruthy();
  });

  it('handleFocus should call registry setFocused', () => {
    fixture.detectChanges();
    spyOn(registry, 'setFocused');
    component.handleFocus();
    expect(registry.setFocused).toHaveBeenCalledWith(true);
  });

  it('handleBlur should call registry setFocused', () => {
    fixture.detectChanges();
    spyOn(registry, 'setFocused');
    component.handleBlur();
    expect(registry.setFocused).toHaveBeenCalledWith(false);
  });

  it('shouldCreateTag should determinate tag creation', () => {
    fixture.detectChanges();
    expect(component.shouldCreateTag(null)).toEqual(false);

    component.allowDuplicates = true;
    expect(component.shouldCreateTag('qwd')).toEqual(true);

    registry.setTags(['Tag 1', 'Tag 2', 'Tag 3']);
    component.allowDuplicates = false;
    expect(component.shouldCreateTag('Tag 1')).toEqual(false);
    expect(component.shouldCreateTag('TAG 1')).toEqual(false);
    expect(component.shouldCreateTag('Tag 4')).toEqual(true);

    component.allowDuplicates = true;
    expect(component.shouldCreateTag('Tag 1')).toEqual(true);
    expect(component.shouldCreateTag('Tag 4')).toEqual(true);
  });

  it('handleCreateTag should call registry addTag', () => {
    const value = ' Mock Value  ';
    const expectedValue = 'Mock Value';
    fixture.detectChanges();
    spyOn(registry, 'addTag');
    spyOn(component, 'shouldCreateTag').and.returnValue(true);
    component.handleCreateTag(value);
    expect(component.shouldCreateTag).toHaveBeenCalledWith(expectedValue);
    expect(registry.addTag).toHaveBeenCalledWith(expectedValue);
  });

  it('handleCreateTag should not call registry addTag if shouldCreateTag returns false', () => {
    const value = ' Mock Value  ';
    const expectedValue = 'Mock Value';
    fixture.detectChanges();
    spyOn(registry, 'addTag');
    spyOn(component, 'shouldCreateTag').and.returnValue(false);
    component.handleCreateTag(value);
    expect(component.shouldCreateTag).toHaveBeenCalledWith(expectedValue);
    expect(registry.addTag).not.toHaveBeenCalled();
  });

  it('handleCreateTag should cleanup editor if tag was created', () => {
    fixture.detectChanges();
    spyOn(component.editor, 'clear');
    spyOn(component, 'shouldCreateTag').and.returnValue(true);
    component.handleCreateTag('Mock');
    expect(component.editor.clear).toHaveBeenCalled();
  });

  it('handleCreateTag should not cleanup editor if tag was not created', () => {
    fixture.detectChanges();
    spyOn(component.editor, 'clear');
    spyOn(component, 'shouldCreateTag').and.returnValue(false);
    component.handleCreateTag('Mock');
    expect(component.editor.clear).not.toHaveBeenCalled();
  });

  it('handleUpdateState should call inputFocus on editor if next state is focused', () => {
    fixture.detectChanges();
    spyOn(component.editor, 'inputFocus');
    const focused = new SimpleChange(false, true, false);
    component.handleUpdateState({
      state: { focused: true, isExternal: false, tags: [], disabled: false, selected: null},
      changes: { focused }
    });

    expect(component.editor.inputFocus).toHaveBeenCalled();
  });

  it('handleUpdateState should not call inputFocus on editor if next state is not focused', () => {
    fixture.detectChanges();
    spyOn(component.editor, 'inputFocus');
    const focused = new SimpleChange(false, false, false);
    component.handleUpdateState({
      state: { focused: true, isExternal: false, tags: [], disabled: false, selected: null},
      changes: { focused }
    });

    expect(component.editor.inputFocus).not.toHaveBeenCalled();
  });

  it('handleUpdateState should not call inputFocus on editor if has no focused changes', () => {
    fixture.detectChanges();
    spyOn(component.editor, 'inputFocus');
    component.handleUpdateState({
      state: { focused: true, isExternal: false, tags: [], disabled: false, selected: null},
      changes: { }
    });

    expect(component.editor.inputFocus).not.toHaveBeenCalled();
  });
});
