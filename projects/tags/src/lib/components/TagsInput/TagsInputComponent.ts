import isEqual from 'lodash/isEqual';
import {
  ChangeDetectionStrategy,
  Component,
  forwardRef,
  HostBinding,
  Input,
  OnDestroy,
  OnInit
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Unsubscribable, StateInterface } from '@argon/tools';
import { TagsInputStateInterface } from '../../interfaces/TagsInputStateInterface';
import { TagsInputManager } from '../../services/TagsInputManager';

@Component({
  selector: 'ar-tags-input',
  template: `
    <ar-form-control [arBsSize]="arBsSize" (click)="handleFocus()" class="ar-tags-input__container" arTagInputKeyManager>
      <ng-content></ng-content>
    </ar-form-control>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    TagsInputManager,
    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => TagsInputComponent), multi: true }
  ]
})
export class TagsInputComponent implements ControlValueAccessor, OnInit, OnDestroy {

  @Input() arBsSize: any;

  @HostBinding('class.ar-tags-input') className = true;

  private stateSubscription: Subscription;

  constructor(
    public manager: TagsInputManager
  ) { }

  public handleFocus() {
    this.manager.setFocused(true);
  }

  ngOnInit(): void {
    this.stateSubscription = this.manager.state.subscribe(this.handleStateChange);
  }

  @Unsubscribable
  ngOnDestroy(): void { }

  registerOnChange(fn: any): void {
    this.changeFn = fn;
  }

  registerOnTouched(fn: any): void {
    this.touchFn = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.manager.setDisabled(isDisabled);
  }

  writeValue(obj: Array<string>): void {
    this.manager.setTags(obj || [], true);
  }

  public handleStateChange = (state: StateInterface<TagsInputStateInterface>) => {
    const { changes } = state;
    const { tags, isExternal } = changes;
    if (isExternal && tags && !isExternal.currentValue && !isEqual(tags.currentValue, tags.previousValue)) {
      this.changeFn(tags.currentValue);
      this.touchFn();
    }
  }

  public changeFn = (value: Array<string>) => { };

  public touchFn = () => { };

}
