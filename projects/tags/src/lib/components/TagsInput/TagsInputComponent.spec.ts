import { A11yModule } from '@angular/cdk/a11y';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { TagsInputComponent } from './TagsInputComponent';


describe('TagsInputComponent', () => {

  let component: TagsInputComponent;
  let fixture: ComponentFixture<TagsInputComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ TagsInputComponent ],
    schemas: [NO_ERRORS_SCHEMA]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsInputComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-tags-input class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-tags-input');
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(component.className).toBeTruthy();
    expect(component.manager).toBeDefined();
  });

  it('should have registry subscription', () => {
    spyOn(component.manager.state, 'subscribe').and.returnValue(null);
    fixture.detectChanges();
    expect(component.manager.state.subscribe).toHaveBeenCalled();
  });

  it('handleFocus should call registry setFocused', () => {
    fixture.detectChanges();
    spyOn(component.manager, 'setFocused');
    component.handleFocus();
    expect(component.manager.setFocused).toHaveBeenCalledWith(true);
  });

  it('registerOnChange should register change fn', () => {
    fixture.detectChanges();
    component.changeFn = null;
    const expectedFn = () => { };
    component.registerOnChange(expectedFn);
    expect(component.changeFn).toEqual(expectedFn);
  });

  it('registerOnTouched should register touch fn', () => {
    fixture.detectChanges();
    component.touchFn = null;
    const expectedFn = () => { };
    component.registerOnTouched(expectedFn);
    expect(component.touchFn).toEqual(expectedFn);
  });

  it('setDisabledState should call manager setDisabled', () => {
    fixture.detectChanges();
    spyOn(component.manager, 'setDisabled');
    component.setDisabledState(true);
    expect(component.manager.setDisabled).toHaveBeenCalledWith(true);
  });

  it('writeValue should call setTags with value', () => {
    fixture.detectChanges();
    spyOn(component.manager, 'setTags');
    const value = ['123', '23123123', 'wqfr2354235'];
    component.writeValue(value);
    expect(component.manager.setTags).toHaveBeenCalledWith(value, true);
  });

  it('writeValue should call setTags with [] if value is empty', () => {
    fixture.detectChanges();
    spyOn(component.manager, 'setTags');
    component.writeValue(null);
    expect(component.manager.setTags).toHaveBeenCalledWith([], true);
  });

  it('handleStateChange should call change and touch fn if have internal changes', () => {
    const nextvalue = ['dqwdqw', '123123123', 'dwdqwd qwd qfreg324'];
    const tags = new SimpleChange([], nextvalue, false);
    const isExternal = new SimpleChange(false, false, false);
    spyOn(component, 'changeFn');
    spyOn(component, 'touchFn');
    component.handleStateChange({
      state: { tags: [], isExternal: false, selected: null, focused: false, disabled: false},
      changes: { tags, isExternal }
    });
    expect(component.changeFn).toHaveBeenCalledWith(nextvalue);
    expect(component.touchFn).toHaveBeenCalled();
  });

  it('handleStateChange should not trigger change if external changes', () => {
    const tags = new SimpleChange([], ['dqwdqw', '123123123', 'dwdqwd qwd qfreg324'], false);
    const isExternal = new SimpleChange(false, true, false);
    spyOn(component, 'changeFn');
    spyOn(component, 'touchFn');
    component.handleStateChange({
      state: { tags: [], isExternal: false, selected: null, focused: false, disabled: false},
      changes: { tags, isExternal }
    });
    expect(component.changeFn).not.toHaveBeenCalled();
    expect(component.touchFn).not.toHaveBeenCalled();
  });

  it('handleStateChange should not trigger change if tags equal', () => {
    const tags = new SimpleChange(['dqwdqw', '123123123', 'dwdqwd qwd qfreg324'], ['dqwdqw', '123123123', 'dwdqwd qwd qfreg324'], false);
    const isExternal = new SimpleChange(false, false, false);
    spyOn(component, 'changeFn');
    spyOn(component, 'touchFn');
    component.handleStateChange({
      state: { tags: [], isExternal: false, selected: null, focused: false, disabled: false},
      changes: { tags, isExternal }
    });
    expect(component.changeFn).not.toHaveBeenCalled();
    expect(component.touchFn).not.toHaveBeenCalled();
  });

  it('handleStateChange should not trigger change if tags not changed', () => {
    const isExternal = new SimpleChange(false, false, false);
    spyOn(component, 'changeFn');
    spyOn(component, 'touchFn');
    component.handleStateChange({
      state: { tags: [], isExternal: false, selected: null, focused: false, disabled: false},
      changes: { isExternal }
    });
    expect(component.changeFn).not.toHaveBeenCalled();
    expect(component.touchFn).not.toHaveBeenCalled();
  });
});
