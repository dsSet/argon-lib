import { ChangeDetectionStrategy, Component, HostBinding, Injector, OnDestroy, OnInit } from '@angular/core';
import { BaseShape, StateInterface } from '@argon/tools';
import { TagsInputStateInterface } from '../../interfaces/TagsInputStateInterface';
import { TagsInputMixin } from '../../mixins/TagsInputMixin';
import { TagsInputManager } from '../../services/TagsInputManager';


@Component({
  selector: 'ar-tags-set',
  template: `
    <ar-tag-item
      *ngFor="let tag of registry.tags; let i = index"
      [value]="tag"
      (valueChange)="handleTagValueChange($event, i)"
      (removeTag)="handleRemoveTag(i)"
      [active]="isTagSelected(i)"
      [disabled]="registry.currentState.disabled"
    ></ar-tag-item>
    <ng-content></ng-content>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagsSetComponent extends TagsInputMixin(BaseShape) implements OnInit, OnDestroy {

  @HostBinding('class.ar-tags-set') className = true;

  public registry: TagsInputManager;

  public selectedTag: number;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  public handleTagValueChange(value: string, index: number) {
    this.registry.updateTag(value, index);
  }

  public handleRemoveTag(index: number) {
    this.registry.removeTagByIndex(index);
  }

  public isTagSelected(index: number): boolean {
    return this.selectedTag === index;
  }

  handleUpdateState(state: StateInterface<TagsInputStateInterface>): void {
    const { changes } = state;
    const { selected, tags } = changes;
    if (selected) {
      this.selectedTag = selected.currentValue;
    }

    if (tags || selected) {
      super.handleUpdateState(state);
    }
  }


}
