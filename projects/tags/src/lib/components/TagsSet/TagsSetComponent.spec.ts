import { A11yModule } from '@angular/cdk/a11y';
import { NO_ERRORS_SCHEMA, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { TagsInputManager } from '../../services/TagsInputManager';
import { TagsSetComponent } from './TagsSetComponent';

describe('TagsSetComponent', () => {

  let component: TagsSetComponent;
  let fixture: ComponentFixture<TagsSetComponent>;

  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ TagsSetComponent ],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [TagsInputManager]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsSetComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-tags-set class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-tags-set');
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(component.className).toBeTruthy();
    expect(component.registry).toBeDefined();
  });

  it('handleTagValueChange should call registry updateTag', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'updateTag');
    component.handleTagValueChange('Mock value', 8);
    expect(component.registry.updateTag).toHaveBeenCalledWith('Mock value', 8);
  });

  it('handleRemoveTag should call registry removeTagByIndex', () => {
    fixture.detectChanges();
    spyOn(component.registry, 'removeTagByIndex');
    component.handleRemoveTag(15);
    expect(component.registry.removeTagByIndex).toHaveBeenCalledWith(15);
  });

  it('isTagSelected should basing on selectedTag property', () => {
    fixture.detectChanges();
    component.selectedTag = 7;
    expect(component.isTagSelected(2)).toEqual(false);
    expect(component.isTagSelected(7)).toEqual(true);
  });

  it('handleUpdateState should update selectedTag property', () => {
    fixture.detectChanges();
    const selected = new SimpleChange(0, 15, false);
    component.selectedTag = -1;
    component.handleUpdateState({
      state: { selected: 15, disabled: false, focused: false, tags: [], isExternal: false },
      changes: { selected }
    });
    expect(component.selectedTag).toEqual(15);
  });

});
