import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { A11yModule } from '@angular/cdk/a11y';
import { ReactiveFormsModule } from '@angular/forms';
import { HostBindingTestHelper } from '../../../../../../test/helpers/HostBindingTestHelper';
import { TagEditorDirective } from '../../directives/TagEditorDirective';
import { TagItemComponent } from './TagItemComponent';
import createSpyObj = jasmine.createSpyObj;

describe('TagItemComponent', () => {

  let component: TagItemComponent;
  let fixture: ComponentFixture<TagItemComponent>;


  beforeEach(waitForAsync(() => TestBed.configureTestingModule({
    imports: [ A11yModule, ReactiveFormsModule ],
    declarations: [ TagItemComponent, TagEditorDirective ],
    schemas: [NO_ERRORS_SCHEMA]
  }).compileComponents()));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagItemComponent);
    component = fixture.componentInstance;
  });

  it('className should toggle .ar-tag-item class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'className', 'ar-tag-item');
  });

  it('disabled should toggle .ar-tag-item--disabled class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'disabled', 'ar-tag-item--disabled');
  });

  it('active should toggle .ar-tag-item--active class', () => {
    HostBindingTestHelper.testToggleClass(component, fixture, 'active', 'ar-tag-item--active');
  });

  it('should have defaults', () => {
    fixture.detectChanges();
    expect(component.className).toEqual(true);
    expect(component.disabled).toBeFalsy();
    expect(component.active).toBeFalsy();
    expect(component.focused).toBeFalsy();
    expect(component.editor instanceof TagEditorDirective).toBeTruthy();
  });

  it('handleEditorFocus should change focused property if not disabled', () => {
    fixture.detectChanges();
    component.focused = true;
    component.handleEditorFocus(true);
    expect(component.focused).toEqual(true);
    component.handleEditorFocus(false);
    expect(component.focused).toEqual(false);
  });

  it('handleEditorFocus should set focused to false if disabled', () => {
    fixture.detectChanges();
    component.disabled = true;
    component.focused = true;
    component.handleEditorFocus(true);
    expect(component.focused).toEqual(false);
    component.handleEditorFocus(false);
    expect(component.focused).toEqual(false);
  });

  it('handleValueChange should emit value changes', () => {
    fixture.detectChanges();
    spyOn(component.valueChange, 'emit');
    component.value = '123';
    component.handleValueChange('222');
    expect(component.valueChange.emit).toHaveBeenCalledWith('222');
    component.handleValueChange('');
    expect(component.valueChange.emit).toHaveBeenCalledWith('123');
  });

  it('handleTextClick should stop event propagation', () => {
    fixture.detectChanges();
    const event = createSpyObj(['stopPropagation']);
    component.handleTextClick(event);
    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('handleTextClick should call editor inputFocus with current value', () => {
    fixture.detectChanges();
    const event = createSpyObj(['stopPropagation']);
    spyOn(component.editor, 'inputFocus');
    component.value = '32111';
    component.handleTextClick(event);
    expect(component.editor.inputFocus).toHaveBeenCalledWith(component.value);
  });

  it('handleClick should stop event propagation', () => {
    fixture.detectChanges();
    const event = createSpyObj(['stopPropagation']);
    component.handleClick(event);
    expect(event.stopPropagation).toHaveBeenCalled();
  });

  it('isLabelHidden should basing on focused value', () => {
    fixture.detectChanges();
    component.focused = false;
    expect(component.isLabelHidden).toEqual(false);
    component.focused = true;
    expect(component.isLabelHidden).toEqual(true);
  });

  it('isRemoveIconHidden should basing on focused and disabled values', () => {
    fixture.detectChanges();
    component.focused = false;
    component.disabled = false;
    expect(component.isRemoveIconHidden).toEqual(false);

    component.focused = true;
    component.disabled = false;
    expect(component.isRemoveIconHidden).toEqual(true);

    component.focused = false;
    component.disabled = true;
    expect(component.isRemoveIconHidden).toEqual(true);
  });

  it('isEditorHidden should basing on focused and disabled values', () => {
    fixture.detectChanges();
    component.focused = true;
    component.disabled = false;
    expect(component.isEditorHidden).toEqual(false);

    component.focused = false;
    component.disabled = false;
    expect(component.isEditorHidden).toEqual(true);

    component.focused = true;
    component.disabled = true;
    expect(component.isEditorHidden).toEqual(true);
  });
});
