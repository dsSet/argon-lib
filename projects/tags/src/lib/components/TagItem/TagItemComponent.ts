import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, HostListener, Input, Output, ViewChild } from '@angular/core';
import { TagEditorDirective } from '../../directives/TagEditorDirective';

@Component({
  selector: 'ar-tag-item',
  template: `
    <span class="ar-tag-item__label"
          [class.ar-tag-item__hidden]="isLabelHidden"
          [arTextCut]="value"
          (dblclick)="handleTextClick($event)"
    >{{ value }}</span>
    <ar-tag-close-icon class="ar-tag-item__icon"
                       [class.ar-tag-item__hidden]="isRemoveIconHidden"
                       (click)="removeTag.emit()"
    ></ar-tag-close-icon>
    <input class="ar-tag-item__editor"
           [class.ar-tag-item__hidden]="isEditorHidden"
           arTagEditor
           (valueChange)="handleValueChange($event)"
           (focus)="handleEditorFocus(true)"
           (blur)="handleEditorFocus(false)"
           [attr.tabindex]="-1"
    />
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagItemComponent {

  @ViewChild(TagEditorDirective, { static: true, read: TagEditorDirective }) editor: TagEditorDirective;

  @HostBinding('class.ar-tag-item') className = true;

  @HostBinding('class.ar-tag-item--disabled') @Input() disabled: boolean;

  @HostBinding('class.ar-tag-item--active') @Input() active: boolean;

  @Input() value: string;

  @Output() valueChange = new EventEmitter<string>();

  @Output() removeTag = new EventEmitter<void>();

  public focused: boolean;

  public get isEditorHidden(): boolean {
    return !this.focused || this.disabled;
  }

  public get isRemoveIconHidden(): boolean {
    return this.focused || this.disabled;
  }

  public get isLabelHidden(): boolean {
    return this.focused;
  }

  @HostListener('click', ['$event'])
  public handleClick(event: Event) {
    event.stopPropagation();
  }

  public handleTextClick(event: MouseEvent) {
    event.stopPropagation();
    this.editor.inputFocus(this.value);
  }

  public handleValueChange(value: string) {
    this.valueChange.emit(value || this.value);
    this.editor.inputBlur();
  }

  public handleEditorFocus(isFocused: boolean) {
    if (!this.disabled && isFocused) {
      this.focused = isFocused;
    } else {
      this.focused = false;
    }
  }

}
