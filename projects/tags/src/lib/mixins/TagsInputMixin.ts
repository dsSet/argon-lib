import { Constructor, HasInjectorInterface, HasLifecircleHooksInterface, Unsubscribable, StateInterface } from '@argon/tools';
import { ChangeDetectorRef, Injectable, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TagsInputRegistryExtensionInterface } from '../interfaces/TagsInputRegistryExtensionInterface';
import { TagsInputStateInterface } from '../interfaces/TagsInputStateInterface';
import { TagsInputManager } from '../services/TagsInputManager';

export function TagsInputMixin<T extends Constructor<HasInjectorInterface & HasLifecircleHooksInterface>>(
  BaseClass: T
): T & Constructor<TagsInputRegistryExtensionInterface> {

  @Injectable()
  class TagsInputRegistryExtension extends BaseClass implements TagsInputRegistryExtensionInterface, OnInit, OnDestroy {

    public registry: TagsInputManager;

    public changeDetectorRef: ChangeDetectorRef;

    private changesSubscription: Subscription;

    ngOnInit(): void {
      if (super.ngOnInit) {
        super.ngOnInit();
      }

      this.registry = this.injector.get(TagsInputManager);
      this.changeDetectorRef = this.injector.get(ChangeDetectorRef);
      this.changesSubscription = this.registry.state.subscribe((state: StateInterface<TagsInputStateInterface>) => {
        this.handleUpdateState(state);
      });
    }

    @Unsubscribable
    ngOnDestroy(): void {
      if (super.ngOnDestroy) {
        super.ngOnDestroy();
      }
    }

    public handleUpdateState(state: StateInterface<TagsInputStateInterface>) {
      this.changeDetectorRef.markForCheck();
    }
  }

  return TagsInputRegistryExtension;
}
