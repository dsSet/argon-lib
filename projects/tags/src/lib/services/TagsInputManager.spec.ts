import { TagsInputManager } from './TagsInputManager';

describe('TagsInputManager', () => {

  let service: TagsInputManager;

  beforeEach(() => {
    service = new TagsInputManager();
  });

  it('should have initials', () => {
    expect(service.state).toBeDefined();
    expect(service.currentState).toEqual(TagsInputManager.getInitialState());
  });

  it('tags should be map from the state', () => {
    const expected = ['qw', '123'];
    service.state.setState({ tags: expected});
    expect(service.tags).toEqual(expected);
  });

  it('getTagValue should return value by index', () => {
    const expected = ['qw', '123'];
    service.state.setState({ tags: expected});
    expect(service.getTagValue(0)).toEqual('qw');
    expect(service.getTagValue(1)).toEqual('123');
    expect(service.getTagValue(2)).toEqual('');
  });

  it('addTag should push new value', () => {
    const current = ['qw', '123'];
    const newValue = 'TEST';
    const expectedValue = [...current, newValue];
    service.state.setState({ tags: current});
    service.addTag(newValue);
    expect(service.tags).toEqual(expectedValue);
  });

  it('removeTag should remove tag', () => {
    const current = ['qw', '123', '8554', '000 12'];
    const value = '123';
    const expectedValue = ['qw', '8554', '000 12'];
    service.state.setState({ tags: current});
    service.removeTag(value);
    expect(service.tags).toEqual(expectedValue);
  });

  it('removeTagByIndex should remove tag by index', () => {
    const current = ['qw', '123', '8554', '000 12'];
    const expectedValue = ['qw', '8554', '000 12'];
    service.state.setState({ tags: current});
    service.removeTagByIndex(1);
    expect(service.tags).toEqual(expectedValue);
  });

  it('removeSelected should remove tag by selection index', () => {
    const current = ['qw', '123', '8554', '000 12'];
    const expectedValue = ['qw', '123', '000 12'];
    service.state.setState({ tags: current, selected: 2 });
    service.removeSelected();
    expect(service.tags).toEqual(expectedValue);
  });

  it('removeSelected should update selection', () => {
    const current = ['qw', '123', '8554', '000 12'];
    service.state.setState({ tags: current, selected: 2 });
    service.removeSelected();
    expect(service.currentState.selected).toEqual(1);

    service.state.setState({ tags: current, selected: 2 });
    service.removeSelected(true);
    expect(service.currentState.selected).toEqual(2);

    service.state.setState({ tags: current, selected: 0 });
    service.removeSelected();
    expect(service.currentState.selected).toEqual(-1);

    service.state.setState({ tags: current, selected: 4 });
    service.removeSelected();
    expect(service.currentState.selected).toEqual(3);

    service.state.setState({ tags: current, selected: 0 });
    service.removeSelected(true);
    expect(service.currentState.selected).toEqual(0);

    service.state.setState({ tags: current, selected: 4 });
    service.removeSelected(true);
    expect(service.currentState.selected).toEqual(3);
  });

  it('removeSelected should call removeTagByIndex if has selection', () => {
    const current = ['qw', '123', '8554', '000 12'];
    spyOn(service, 'removeTagByIndex');
    service.state.setState({ tags: current, selected: 2 });
    service.removeSelected(true);
    expect(service.removeTagByIndex).toHaveBeenCalledWith(2, true);
    service.removeSelected(false);
    expect(service.removeTagByIndex).toHaveBeenCalledWith(2, false);
  });

  it('removeSelected should not call removeTagByIndex if has no selection', () => {
    const current = ['qw', '123', '8554', '000 12'];
    spyOn(service, 'removeTagByIndex');
    service.state.setState({ tags: current, selected: -1 });
    service.removeSelected(true);
    expect(service.removeTagByIndex).not.toHaveBeenCalled();
  });

  it('removeOrSelectLast should call remove selected if has selection', () => {
    spyOn(service, 'removeSelected');
    service.state.setState({ tags: [], selected: 8 });
    service.removeOrSelectLast();
    expect(service.removeSelected).toHaveBeenCalled();
  });

  it('removeOrSelectLast should call selectLast if has no selection', () => {
    spyOn(service, 'selectLast');
    service.state.setState({ tags: [], selected: -1 });
    service.removeOrSelectLast();
    expect(service.selectLast).toHaveBeenCalled();
  });

  it('removeLastTag should remove last tag', () => {
    const current = ['qw', '123', '8554', '000 12'];
    const expected = ['qw', '123', '8554'];
    service.state.setState({ tags: current });
    service.removeLastTag();
    expect(service.tags).toEqual(expected);
  });

  it('selectLast should select last tag', () => {
    const current = ['qw', '123', '8554', '000 12'];
    service.state.setState({ tags: current, selected: -1 });
    service.selectLast();
    expect(service.currentState.selected).toEqual(3);
  });

  it('selectPrev should move selection to the left', () => {
    const setSelectedTagSpy = spyOn(service, 'setSelectedTag');
    const selectLastSpy = spyOn(service, 'selectLast');

    service.state.setState({ selected: -1 });
    service.selectPrev();
    expect(service.selectLast).toHaveBeenCalled();
    expect(service.setSelectedTag).not.toHaveBeenCalled();
    setSelectedTagSpy.calls.reset();
    selectLastSpy.calls.reset();

    service.state.setState({ selected: 4 });
    service.selectPrev();
    expect(service.selectLast).not.toHaveBeenCalled();
    expect(service.setSelectedTag).toHaveBeenCalledWith(3);
    setSelectedTagSpy.calls.reset();
    selectLastSpy.calls.reset();

    service.state.setState({ selected: 0 });
    service.selectPrev();
    expect(service.selectLast).not.toHaveBeenCalled();
    expect(service.setSelectedTag).not.toHaveBeenCalled();
  });

  it('selectNext should move selection to the right', () => {
    const setSelectedTagSpy = spyOn(service, 'setSelectedTag');

    service.state.setState({ tags: ['1', '2', '3', '4'], selected: -1 });
    service.selectNext();
    expect(service.setSelectedTag).toHaveBeenCalledWith(0);
    setSelectedTagSpy.calls.reset();

    service.state.setState({ tags: ['1', '2', '3', '4'], selected: 0 });
    service.selectNext();
    expect(service.setSelectedTag).toHaveBeenCalledWith(1);
    setSelectedTagSpy.calls.reset();

    service.state.setState({ tags: ['1', '2', '3', '4'], selected: 3 });
    service.selectNext();
    expect(service.setSelectedTag).not.toHaveBeenCalled();
    setSelectedTagSpy.calls.reset();
  });

  it('updateTag should update tag value', () => {
    const current = ['qw', '123', '8554', '000 12'];
    const value = 'NEW VALUE';
    service.state.setState({ tags: current });
    service.updateTag(value, 1);
    expect(service.tags).toEqual(['qw', value, '8554', '000 12']);

    service.state.setState({ tags: current });
    service.updateTag(value, 0);
    expect(service.tags).toEqual([value, '123', '8554', '000 12']);

    service.state.setState({ tags: current });
    service.updateTag(value, 3);
    expect(service.tags).toEqual(['qw', '123', '8554', value]);

    service.state.setState({ tags: current });
    service.updateTag(value, 99);
    expect(service.tags).toEqual(['qw', '123', '8554', '000 12']);

    service.state.setState({ tags: current });
    service.updateTag(value, -2);
    expect(service.tags).toEqual(['qw', '123', '8554', '000 12']);
  });

  it('setFocused should update focused state', () => {
    service.setFocused(true);
    expect(service.currentState.focused).toEqual(true);
    service.setFocused(false);
    expect(service.currentState.focused).toEqual(false);
  });

  it('setFocused should update selection', () => {
    service.state.setState({ selected: 8, focused: false });
    service.setFocused(true);
    expect(service.currentState.focused).toEqual(true);
    expect(service.currentState.selected).toEqual(8);

    service.setFocused(false);
    expect(service.currentState.focused).toEqual(false);
    expect(service.currentState.selected).toEqual(-1);
  });

});
