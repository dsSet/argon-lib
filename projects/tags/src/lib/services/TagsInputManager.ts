import without from 'lodash/without';
import slice from 'lodash/slice';
import size from 'lodash/size';
import { Injectable } from '@angular/core';
import { StateSubject } from '@argon/tools';
import { TagsInputStateInterface } from '../interfaces/TagsInputStateInterface';

@Injectable()
export class TagsInputManager {

  static getInitialState(): TagsInputStateInterface {
    return {
      focused: false,
      disabled: false,
      tags: [],
      isExternal: false,
      selected: -1
    };
  }

  public readonly state: StateSubject<TagsInputStateInterface>;

  public get currentState(): TagsInputStateInterface {
    return this.state.value.state;
  }

  public get tags(): Array<string> {
    return this.currentState.tags || [];
  }

  constructor() {
    this.state = new StateSubject<TagsInputStateInterface>(TagsInputManager.getInitialState());
  }

  public getTagValue(index: number): string {
    return this.tags[index] || '';
  }

  public addTag(tag: string) {
    this.setTags([...this.tags, tag]);
  }

  public removeTag(tag: string) {
    this.setTags(without(this.tags, tag));
  }

  public removeTagByIndex(index: number, inverseSelection?: boolean) {
    this.setTags([
      ...slice(this.tags, 0, index),
      ...slice(this.tags, index + 1)
    ], false, { selected: this.resolveSelectionAfterDelete(index, inverseSelection) });
  }

  public removeSelected(inverseSelection?: boolean) {
    if (this.currentState.selected >= 0) {
      this.removeTagByIndex(this.currentState.selected, inverseSelection);
    }
  }

  public removeOrSelectLast() {
    if (this.currentState.selected >= 0) {
      this.removeSelected();
    } else {
      this.selectLast();
    }
  }

  public removeLastTag() {
    const index = size(this.tags);
    if (index > 0) {
      this.setTags(
        slice(this.tags, 0, index - 1),
        false,
        { selected: this.resolveSelectionAfterDelete(index) }
        );
    }
  }

  public selectLast() {
    this.setSelectedTag(size(this.tags) - 1);
  }

  public selectPrev() {
    if (this.currentState.selected > 0) {
      this.setSelectedTag(this.currentState.selected - 1);
    } else if (this.currentState.selected < 0) {
      this.selectLast();
    }
  }

  public selectNext() {
    const lastIndex = size(this.currentState.tags) - 1;
    if (this.currentState.selected < lastIndex) {
      this.setSelectedTag(this.currentState.selected + 1);
    }
  }

  public setSelectedTag(selected: number = -1) {
    this.state.setState({ selected });
  }

  public setTags(tags: Array<string>, isExternal: boolean = false, additionalState: Partial<TagsInputStateInterface> = { }) {
    this.state.setState({ tags, focused: isExternal ? false : this.currentState.focused, isExternal, ...additionalState });
  }

  public updateTag(tag: string, index: number) {
    const prevValue = this.tags[index];
    if (index >= size(this.tags) || index < 0) {
      return;
    }

    if (tag !== prevValue && tag) {
      this.setTags([
        ...slice(this.tags, 0, index),
        tag,
        ...slice(this.tags, index + 1)
      ]);
    }
  }

  public setFocused(focused: boolean) {
    if (this.currentState.focused !== focused) {
      this.state.setState({ focused, selected: focused ? this.currentState.selected : -1 });
    }
  }

  public setDisabled(disabled: boolean) {
    this.state.setState({ disabled });
  }

  private resolveSelectionAfterDelete(indexToDelete: number, inverseSelection?: boolean): number {
    const selected = this.currentState.selected;
    const lastIndex = size(this.tags) - 1;
    if (inverseSelection) {
      const next = selected === indexToDelete ? selected : selected - 1;
      return next > lastIndex ? lastIndex : next;
    }

    return  selected < indexToDelete || selected < 0 ? selected : selected - 1;
  }

}
