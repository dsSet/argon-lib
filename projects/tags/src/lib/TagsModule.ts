import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { KeyboardModule } from '@argon/keyboard';
import { StyleKitModule } from '@argon/style-kit';
import { IconCloseComponent } from './components/IconClose/IconCloseComponent';
import { TagEditorComponent } from './components/TagEditor/TagEditorComponent';
import { TagItemComponent } from './components/TagItem/TagItemComponent';
import { TagsInputComponent } from './components/TagsInput/TagsInputComponent';
import { TagsSetComponent } from './components/TagsSet/TagsSetComponent';
import { TagEditorDirective } from './directives/TagEditorDirective';
import { TagInputKeyManagerDirective } from './directives/TagInputKeyManagerDirective';


@NgModule({
  imports: [
    CommonModule,
    StyleKitModule,
    KeyboardModule
  ],
  declarations: [
    TagsInputComponent,
    TagEditorDirective,
    TagItemComponent,
    IconCloseComponent,
    TagsSetComponent,
    TagEditorComponent,
    TagInputKeyManagerDirective
  ],
  exports: [
    TagsInputComponent,
    TagsSetComponent,
    TagEditorComponent
  ]
})
export class TagsModule {

}
