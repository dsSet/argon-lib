import { Provider } from '@angular/core';
import {
  ARGON_SERVER_DATE_DEFAULT_FORMAT,
  ARGON_SERVER_DATE_FORMAT,
  ARGON_CLIENT_DATE_FORMAT,
  ARGON_CLIENT_DATE_DEFAULT_FORMAT
} from '@argon/date';

export const CLIENT_MDY_FORMATS: Array<string> = [
  'MM.DD.YYYY',
  'MM-DD-YYYY',
  'MM/DD/YYYY',
  'MM.DD.YY',
  'MM-DD-YY',
  'MM/DD/YY',
  'MM.D.YYYY',
  'MM-D-YYYY',
  'MM/D/YYYY',
  'MM.D.YY',
  'MM-D-YY',
  'MM/D/YY',
  'M.DD.YY',
  'M-DD-YY',
  'M/DD/YY',
  'M.D.YYYY',
  'M-D-YYYY',
  'M/D/YYYY',
  'M.D.YY',
  'M-D-YY',
  'M/D/YY',
];

export const CLIENT_DMY_FORMATS: Array<string> = [
  'DD.MM.YYYY',
  'DD-MM-YYYY',
  'DD/MM/YYYY',
  'DD.MM.YY',
  'DD-MM-YY',
  'DD/MM/YY',
  'DD.M.YYYY',
  'DD-M-YYYY',
  'DD/M/YYYY',
  'DD.M.YY',
  'DD-M-YY',
  'DD/M/YY',
  'D.MM.YY',
  'D-MM-YY',
  'D/MM/YY',
  'D.M.YYYY',
  'D-M-YYYY',
  'D/M/YYYY',
  'D.M.YY',
  'D-M-YY',
  'D/M/YY',
];

export const UTCServerDateFormatsProviders: Array<Provider> = [
  { provide: ARGON_SERVER_DATE_DEFAULT_FORMAT, useValue: 'YYYY-MM-DDTHH:mm:ss\\Z' },
  { provide: ARGON_SERVER_DATE_FORMAT, useValue: [
      'YYYY-MM-DDTHH:mm:ss\\Z',
      'YYYY-MM-DDTHH:mm:ss',
      'YYYY-MM-DD'
    ]
  }
];

export const TimezoneServerDateFormatsProviders: Array<Provider> = [
  { provide: ARGON_SERVER_DATE_DEFAULT_FORMAT, useValue: 'YYYY-MM-DDTHH:mm:ssZ' },
  { provide: ARGON_SERVER_DATE_FORMAT, useValue: [
      'YYYY-MM-DDTHH:mm:ssZ',
      'YYYY-MM-DDTHH:mm:ssZZ',
      'YYYY-MM-DDTHH:mm:ss',
      'YYYY-MM-DD'
    ]
  }
];

export const MDYClientDateFormatProviders: Array<Provider> = [
  { provide: ARGON_CLIENT_DATE_DEFAULT_FORMAT, useValue: 'MM.DD.YYYY' },
  { provide: ARGON_CLIENT_DATE_FORMAT, useValue: CLIENT_MDY_FORMATS }
];

export const DMYClientDateFormatProviders: Array<Provider> = [
  { provide: ARGON_CLIENT_DATE_DEFAULT_FORMAT, useValue: 'DD.MM.YYYY' },
  { provide: ARGON_CLIENT_DATE_FORMAT, useValue: CLIENT_DMY_FORMATS }
];
