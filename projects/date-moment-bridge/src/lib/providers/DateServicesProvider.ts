import { Provider } from '@angular/core';
import {
  DateFormatService,
  DateTransformService,
  DateAdapter
} from '@argon/date';
import { MomentDateTransformService } from '../services/MomentDateTransformService';
import { MomentDateFormatService } from '../services/MomentDateFormatService';
import { UTCMomentDateAdapter } from '../services/UTCMomentDateAdapter';
import { TimezoneServerDateFormatsProviders, UTCServerDateFormatsProviders } from './DateFormatProviders';
import { TimezoneMomentDateAdapter } from '../services/TimezoneMomentDateAdapter';


export const DateServicesProvider: Array<Provider> = [
  { provide: DateTransformService, useClass: MomentDateTransformService },
  { provide: DateFormatService, useClass: MomentDateFormatService }
];

export const UTCDateAdapterProviders: Array<Provider> = [
  { provide: DateAdapter, useClass: UTCMomentDateAdapter },
  ...UTCServerDateFormatsProviders
];

export const TimezoneDateAdapterProviders: Array<Provider> = [
  { provide: DateAdapter, useClass: TimezoneMomentDateAdapter },
  ...TimezoneServerDateFormatsProviders
];
