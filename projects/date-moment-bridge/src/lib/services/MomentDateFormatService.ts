import { Injectable } from '@angular/core';
import { DateFormatService } from '@argon/date';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';

const moment = _rollupMoment || _moment;

@Injectable()
export class MomentDateFormatService extends DateFormatService {

  public formatWeek(date: Date): string {
    return date ? moment(date).format('WW') : null;
  }

  public formatWeekday(date: Date): string {
    if (!date) {
      return null;
    }
    return moment.weekdaysShort(moment(date).isoWeekday());
  }

  public formatDay(date: Date): string {
    return date ? moment(date).format('DD') : null;
  }

  public formatMonth(date: Date): string {
    return date ? moment(date).format('MMM') : null;
  }

  public formatYear(date: Date): string {
    return date ? moment(date).format('YYYY') : null;
  }

  public formatMonthYear(date: Date): string {
    return date ? moment(date).format('MMM YYYY') : null;
  }

  public formatMonthYearFull(date: Date): string {
    return date ? moment(date).format('MMMM YYYY') : null;
  }

}
