import { MomentDateTransformService } from './MomentDateTransformService';
import { DateIntervalEnum, DatePeriodEnum } from '@argon/date';


describe('MomentDateTransformService', () => {

  const service = new MomentDateTransformService();

  it('createDaysRange should create day range', () => {
    const start = new Date(2020, 7, 1);
    const end = new Date(2020, 7, 4);
    const output = Array.from(MomentDateTransformService.createDaysRange(start, end));
    expect(output).toEqual(
      [start,
      new Date(2020, 7, 2),
      new Date(2020, 7, 3),
      end]
    );
  });

  it('createDaysRange should create month range', () => {
    const start = new Date(2020, 1, 1);
    const end = new Date(2020, 3, 4);
    const output = Array.from(MomentDateTransformService.createDaysRange(start, end, DateIntervalEnum.month));
    expect(output).toEqual(
      [
        new Date(2020, 1, 1),
        new Date(2020, 2, 1),
        new Date(2020, 3, 1),
        new Date(2020, 4, 1),
      ]
    );
  });

  it('createDaysRange should create year range', () => {
    const start = new Date(2018, 1, 1);
    const end = new Date(2020, 3, 4);
    const output = Array.from(MomentDateTransformService.createDaysRange(start, end, DateIntervalEnum.year));
    expect(output).toEqual(
      [
        new Date(2018, 1, 1),
        new Date(2019, 1, 1),
        new Date(2020, 1, 1),
        new Date(2021, 1, 1),
      ]
    );
  });

  it('startOf should return start of period', () => {
    const date = new Date(2020, 7, 5, 15, 25, 33);
    expect(service.startOf(date, DatePeriodEnum.day)).toEqual(new Date(2020, 7, 5));
    expect(service.startOf(date, DatePeriodEnum.isoWeek, DatePeriodEnum.day)).toEqual(new Date(2020, 7, 3));
    expect(service.startOf(date, DatePeriodEnum.month, DatePeriodEnum.isoWeek)).toEqual(new Date(2020, 6, 27));
  });

  it('endOf should return end of period', () => {
    const date = new Date(2020, 7, 5, 15, 25, 33);
    expect(service.endOf(date, DatePeriodEnum.day)).toEqual(new Date(2020, 7, 5, 23, 59, 59, 999));
    expect(service.endOf(date, DatePeriodEnum.isoWeek, DatePeriodEnum.day)).toEqual(new Date(2020, 7, 9, 23, 59, 59, 999));
  });

  it('shiftDate should combine starOf and endOf', () => {
    const date = new Date(2020, 7, 5, 15, 25, 33);
    expect(service.shiftDate(date, ['startOf', DatePeriodEnum.isoWeek], ['endOf', DatePeriodEnum.day]))
      .toEqual(new Date(2020, 7, 3, 23, 59, 59, 999));
  });

  it('getDateIterator should return date range stuff', () => {
    const start = new Date(2020, 7, 1);
    const end = new Date(2020, 7, 4);
    expect(Array.from(service.getDateIterator(start, end))).toEqual(
      [start,
        new Date(2020, 7, 2),
        new Date(2020, 7, 3),
        end]
    );
  });

  it('getMonthWeekdays should return collection of weekdays for current month', () => {
    const date = new Date(2020, 7, 1);
    expect(service.getMonthWeekdays(date)).toEqual([
      new Date(2020, 7, 1),
      new Date(2020, 7, 8),
      new Date(2020, 7, 15),
      new Date(2020, 7, 22),
      new Date(2020, 7, 29)
    ]);
  });

  it('increment', () => {
    const date = new Date(2020, 7, 1);
    expect(service.increment(date, 2, DateIntervalEnum.day))
      .toEqual(new Date(2020, 7, 3));
    expect(service.increment(date, 1, DateIntervalEnum.week))
      .toEqual(new Date(2020, 7, 8));
    expect(service.increment(date, 1, DateIntervalEnum.month))
      .toEqual(new Date(2020, 8, 1));
    expect(service.increment(date, 1, DateIntervalEnum.year))
      .toEqual(new Date(2021, 7, 1));
  });

  it('decrement', () => {
    const date = new Date(2020, 7, 1);
    expect(service.decrement(date, 2, DateIntervalEnum.day))
      .toEqual(new Date(2020, 6, 30));
    expect(service.decrement(date, 1, DateIntervalEnum.week))
      .toEqual(new Date(2020, 6, 25));
    expect(service.decrement(date, 1, DateIntervalEnum.month))
      .toEqual(new Date(2020, 6, 1));
    expect(service.decrement(date, 1, DateIntervalEnum.year))
      .toEqual(new Date(2019, 7, 1));
  });

  it('isSameDay', () => {
    expect(service.isSameDay(
      new Date(2020, 1, 2, 14, 33),
      new Date(2020, 1, 2)
    )).toBeTruthy();
    expect(service.isSameDay(
      new Date(2020, 1, 2, 14, 33),
      new Date(2020, 1, 3)
    )).toBeFalsy();
  });

  it('isSameMonth', () => {
    expect(service.isSameMonth(
      new Date(2020, 1, 2, 14, 33),
      new Date(2020, 1, 5)
    )).toBeTruthy();
    expect(service.isSameMonth(
      new Date(2020, 1, 2, 14, 33),
      new Date(2020, 2, 3)
    )).toBeFalsy();
  });

  it('isSameYear', () => {
    expect(service.isSameYear(
      new Date(2020, 1, 2, 14, 33),
      new Date(2020, 8, 5)
    )).toBeTruthy();
    expect(service.isSameYear(
      new Date(2020, 1, 2, 14, 33),
      new Date(2019, 2, 3)
    )).toBeFalsy();
  });

  it('isSameWeek', () => {
    expect(service.isSameWeek(
      new Date(2020, 7, 4, 14, 33),
      new Date(2020, 7, 8)
    )).toBeTruthy();
    expect(service.isSameWeek(
      new Date(2020, 7, 4, 14, 33),
      new Date(2020, 7, 10)
    )).toBeFalsy();
  });

  it('isSameWeekday', () => {
    expect(service.isSameWeekday(
      new Date(2020, 6, 28, 14, 33),
      new Date(2020, 7, 4)
    )).toBeTruthy();
    expect(service.isSameWeekday(
      new Date(2020, 6, 29, 14, 33),
      new Date(2020, 7, 4)
    )).toBeFalsy();
  });

});
