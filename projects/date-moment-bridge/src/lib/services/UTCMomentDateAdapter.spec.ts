import { TestBed, waitForAsync } from '@angular/core/testing';
import { UTCMomentDateAdapter } from './UTCMomentDateAdapter';
import { UTCServerDateFormatsProviders, DMYClientDateFormatProviders } from '../providers/DateFormatProviders';
import {
  ARGON_CLIENT_DATE_DEFAULT_FORMAT,
  ARGON_CLIENT_DATE_FORMAT,
  ARGON_SERVER_DATE_DEFAULT_FORMAT,
  ARGON_SERVER_DATE_FORMAT
} from '@argon/date';
import { ValueProvider } from '@angular/core';

const serverDates = [
  '2020-07-23',
  '2020-07-23T00:00:00',
  '2020-07-23T00:00:00Z',
  '2020-07-23T00:00:00.0Z',
  '2020-07-23T00:00:00.00Z',
  '2020-07-23T00:00:00.000Z',
  '2020-07-23T00:00:00+00:00',
  '2020-07-23T00:00:00.0+00:00',
  '2020-07-23T00:00:00.00-03:00',
  '2020-07-23T00:00:00.000-05:00',
  '2020-07-23T00:00:00+0100',
  '2020-07-23T00:00:00.0+0700',
  '2020-07-23T00:00:00.00-0200',
  '2020-07-23T00:00:00.000+0000',
];

describe('UTCMomentDateAdapter', () => {
  let service: UTCMomentDateAdapter;

  beforeEach(waitForAsync(() => {
      return TestBed.configureTestingModule({
        providers: [UTCServerDateFormatsProviders, DMYClientDateFormatProviders, UTCMomentDateAdapter]
      });
    }
  ));

  beforeEach(() => {
    service = TestBed.inject(UTCMomentDateAdapter) as any;
  });

  it('serverToDate should transform server input to js Date', () => {
    const expectedDate = new Date(2020, 6, 23);
    expect(service.serverToDate(null)).toEqual(null);
    expect(service.serverToDate('')).toEqual(null);
    expect(service.serverToDate('Invalid')).toEqual('Invalid');
    serverDates.forEach(input => { expect(service.serverToDate(input)).toEqual(expectedDate); });
  });

  it('serverToClient should transform server input to client input', () => {
    const expected = '23.07.2020';
    expect(service.serverToDate(null)).toEqual(null);
    expect(service.serverToDate('')).toEqual(null);
    expect(service.serverToDate('Invalid')).toEqual('Invalid');
    serverDates.forEach(input => { expect(service.serverToClient(input)).toEqual(expected); });
  });

  it('getServerFormats should return list of server supported formats', () => {
    const formatProvider: ValueProvider = UTCServerDateFormatsProviders
      .find((provider: ValueProvider) => provider.provide === ARGON_SERVER_DATE_FORMAT) as ValueProvider;
    expect(service.getServerFormats()).toEqual(formatProvider.useValue);
  });

  it('getDefaultServerFormat should return default server format', () => {
    const formatProvider: ValueProvider = UTCServerDateFormatsProviders
      .find((provider: ValueProvider) => provider.provide === ARGON_SERVER_DATE_DEFAULT_FORMAT) as ValueProvider;
    expect(service.getDefaultServerFormat()).toEqual(formatProvider.useValue);
  });

  it('getClientFormats should return list of client supported formats', () => {
    const formatProvider: ValueProvider = DMYClientDateFormatProviders
      .find((provider: ValueProvider) => provider.provide === ARGON_CLIENT_DATE_FORMAT) as ValueProvider;
    expect(service.getClientFormats()).toEqual(formatProvider.useValue);
  });

  it('getDefaultClientFormat should return default client format', () => {
    const formatProvider: ValueProvider = DMYClientDateFormatProviders
      .find((provider: ValueProvider) => provider.provide === ARGON_CLIENT_DATE_DEFAULT_FORMAT) as ValueProvider;
    expect(service.getDefaultClientFormat()).toEqual(formatProvider.useValue);
  });

  it('dateToServer should transform date to default server format', () => {
    expect(service.dateToServer(null)).toEqual(null);
    expect(service.dateToServer(new Date(2020, 7, 2))).toEqual('2020-08-02T00:00:00Z');
  });

  it('dateToClient should transform date to default client format', () => {
    expect(service.dateToClient(null)).toEqual(null);
    expect(service.dateToClient(new Date(2020, 7, 2))).toEqual('02.08.2020');
  });

  it('clientToServer should transform client input to default server format', () => {
    expect(service.clientToServer(null)).toEqual(null);
    expect(service.clientToServer('Invalid input')).toEqual('Invalid input');
    expect(service.clientToServer('02.08.2020')).toEqual('2020-08-02T00:00:00Z');
    expect(service.clientToServer('02/08/2020')).toEqual('2020-08-02T00:00:00Z');
    expect(service.clientToServer('02-08-2020')).toEqual('2020-08-02T00:00:00Z');
  });

  it('clientToDate should transform client input to js Date', () => {
    expect(service.clientToDate(null)).toEqual(null);
    expect(service.clientToDate('Invalid input')).toEqual('Invalid input');
    expect(service.clientToDate('02.08.2020')).toEqual(new Date(2020, 7, 2));
    expect(service.clientToDate('02/08/2020')).toEqual(new Date(2020, 7, 2));
    expect(service.clientToDate('02-08-2020')).toEqual(new Date(2020, 7, 2));
  });

  it('anyToDate should parse any input and transform to js Date', () => {
    expect(service.anyToDate(null)).toEqual(null);
    expect(service.anyToDate('Invalid input')).toEqual('Invalid input');
    expect(service.anyToDate('02.08.2020')).toEqual(new Date(2020, 7, 2));
    expect(service.anyToDate('2020-08-02')).toEqual(new Date(2020, 7, 2));
    expect(service.anyToDate('2020-08-02T00:00:00Z')).toEqual(new Date(2020, 7, 2));
    expect(service.anyToDate('2020-08-02T00:00:00.000-05:00')).toEqual(new Date(2020, 7, 2));
    expect(service.anyToDate(new Date(2020, 7, 2))).toEqual(new Date(2020, 7, 2));
  });

});
