import { Inject, Injectable } from '@angular/core';
import {
  DateAdapter,
  ARGON_SERVER_DATE_FORMAT,
  ARGON_SERVER_DATE_DEFAULT_FORMAT,
  ARGON_CLIENT_DATE_FORMAT,
  ARGON_CLIENT_DATE_DEFAULT_FORMAT
} from '@argon/date';
import isDate from 'lodash/isDate';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';

const moment = _rollupMoment || _moment;

@Injectable()
export class UTCMomentDateAdapter extends DateAdapter {

  protected currentClientFormats: Array<string>;

  protected currentDefaultClientFormat: string;

  constructor(
    @Inject(ARGON_SERVER_DATE_FORMAT) public readonly serverFormats: Array<string>,
    @Inject(ARGON_SERVER_DATE_DEFAULT_FORMAT) public readonly defaultServerFormat: string,
    @Inject(ARGON_CLIENT_DATE_FORMAT) public readonly clientFormats: Array<string>,
    @Inject(ARGON_CLIENT_DATE_DEFAULT_FORMAT) public readonly defaultClientFormat: string,
  ) {
    super();
    this.currentClientFormats = this.clientFormats;
    this.currentDefaultClientFormat = this.defaultClientFormat;
  }

  setClientFormats(defaultFormat: string, additionalFormats: Array<string>): any {
    this.currentDefaultClientFormat = defaultFormat;
    this.currentClientFormats = additionalFormats;
  }

  anyToDate(input: Date | string | null, formats?: Array<string>): Date | null | string {
    if (!input) {
      return null;
    }

    if (isDate(input)) {
      return moment(input).toDate();
    }

    const date = moment(input, [...this.getClientFormats(), ...this.getServerFormats(), ...(formats || [])], false);

    return date && date.isValid() ? date.toDate() : input;
  }

  clientToDate(input: string | null, formats: Array<string> = []): Date | string | null {
    if (!input) {
      return null;
    }

    const date = moment(input, [...this.getClientFormats(), ...formats], true);

    return date.isValid() ? date.toDate() : input;
  }

  clientToServer(input: string | null, formats?: Array<string>): string | null {
    const date = this.clientToDate(input, formats);
    return isDate(date) ? this.dateToServer(date as Date) : date as string;
  }

  dateToClient(date: Date | null, format?: string): string | null {
    return date ? moment(date).format(format || this.getDefaultClientFormat()) : null;
  }

  dateToServer(date: Date | null): string | null {
    return date ? moment(date).format(this.getDefaultServerFormat()) : null;
  }

  getClientFormats(): Array<string> {
    return this.currentClientFormats;
  }

  getDefaultClientFormat(): string {
    return this.currentDefaultClientFormat;
  }

  getDefaultServerFormat(): string {
    return this.defaultServerFormat;
  }

  getServerFormats(): Array<string> {
    return this.serverFormats;
  }

  serverToClient(input: string | null, formats?: Array<string>): string | null {
    const date = this.serverToDate(input, formats);
    return isDate(date) ? this.dateToClient(date as Date) : date as string;
  }

  serverToDate(input: string | null, formats: Array<string> = []): Date | string | null {
    if (!input) {
      return null;
    }

    const date = moment(input, [...this.getServerFormats(), ...formats], false);

    return date.isValid() ? date.toDate() : input;
  }

}
