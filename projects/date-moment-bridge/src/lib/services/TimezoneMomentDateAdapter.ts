import { UTCMomentDateAdapter } from './UTCMomentDateAdapter';
import { Inject, Injectable } from '@angular/core';
import {
  ARGON_CLIENT_DATE_DEFAULT_FORMAT,
  ARGON_CLIENT_DATE_FORMAT,
  ARGON_SERVER_DATE_DEFAULT_FORMAT,
  ARGON_SERVER_DATE_FORMAT
} from '@argon/date';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';

const moment = _rollupMoment || _moment;

@Injectable()
export class TimezoneMomentDateAdapter extends UTCMomentDateAdapter {

  constructor(
    @Inject(ARGON_SERVER_DATE_FORMAT) public readonly serverFormats: Array<string>,
    @Inject(ARGON_SERVER_DATE_DEFAULT_FORMAT) public readonly defaultServerFormat: string,
    @Inject(ARGON_CLIENT_DATE_FORMAT) public readonly clientFormats: Array<string>,
    @Inject(ARGON_CLIENT_DATE_DEFAULT_FORMAT) public readonly defaultClientFormat: string,
  ) {
    super(serverFormats, defaultServerFormat, clientFormats, defaultClientFormat);
  }

  dateToServer(date: Date | null): string | null {
    return date ? moment(date).toISOString(true) : null;
  }

}
