import { MomentDateFormatService } from './MomentDateFormatService';

describe('MomentDateFormatService', () => {

  const service = new MomentDateFormatService();

  it('formatWeek', () => {
    expect(service.formatWeek(new Date(2020, 0, 6))).toEqual('02');
    expect(service.formatWeek(null)).toEqual(null);
  });

  it('formatWeekday', () => {
    expect(service.formatWeekday(new Date(2020, 7, 4))).toEqual('Tue');
    expect(service.formatWeekday(null)).toEqual(null);
  });

  it('formatDay', () => {
    expect(service.formatDay(new Date(2020, 7, 4))).toEqual('04');
    expect(service.formatDay(null)).toEqual(null);
  });

  it('formatMonth', () => {
    expect(service.formatMonth(new Date(2020, 7, 4))).toEqual('Aug');
    expect(service.formatMonth(null)).toEqual(null);
  });

  it('formatYear', () => {
    expect(service.formatYear(new Date(2020, 7, 4))).toEqual('2020');
    expect(service.formatYear(null)).toEqual(null);
  });

  it('formatMonthYear', () => {
    expect(service.formatMonthYear(new Date(2020, 7, 4))).toEqual('Aug 2020');
    expect(service.formatMonthYear(null)).toEqual(null);
  });

  it('formatMonthYearFull', () => {
    expect(service.formatMonthYearFull(new Date(2020, 7, 4))).toEqual('August 2020');
    expect(service.formatMonthYearFull(null)).toEqual(null);
  });

});
