import { TestBed, waitForAsync } from '@angular/core/testing';
import { TimezoneServerDateFormatsProviders, DMYClientDateFormatProviders } from '../providers/DateFormatProviders';
import { TimezoneMomentDateAdapter } from './TimezoneMomentDateAdapter';

describe('TimezoneMomentDateAdapter', () => {
  let service: TimezoneMomentDateAdapter;

  beforeEach(waitForAsync(() => {
      return TestBed.configureTestingModule({
        providers: [TimezoneServerDateFormatsProviders, DMYClientDateFormatProviders, TimezoneMomentDateAdapter]
      });
    }
  ));

  beforeEach(() => {
    service = TestBed.inject(TimezoneMomentDateAdapter) as any;
  });

  it('dateToServer', () => {
    expect(service.dateToServer(null)).toEqual(null);
    expect(service.dateToServer(new Date(2020, 7, 4))).toEqual(
      '2020-08-04T00:00:00.000+03:00'
    );
  });

});
