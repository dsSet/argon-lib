import { DateIntervalEnum, DatePeriodEnum, DateTransformService } from '@argon/date';
import { Injectable } from '@angular/core';
import * as _moment from 'moment';
import { default as _rollupMoment } from 'moment';

const moment = _rollupMoment || _moment;

@Injectable()
export class MomentDateTransformService extends DateTransformService {


  public static *createDaysRange(start: Date, end: Date, period: DateIntervalEnum = DateIntervalEnum.day) {
    const startDate = moment(start);
    const endDate = moment(end);
    yield startDate.toDate();
    do {
      yield startDate.add(1, period).toDate();
    } while (startDate.isBefore(endDate));
  }

  public startOf(date: Date, ...periods: Array<DatePeriodEnum>): Date {
    let internalDate = moment(date);
    for (const period of periods) {
      internalDate = internalDate.startOf(period);
    }
    return internalDate.toDate();
  }

  public endOf(date: Date, ...periods: Array<DatePeriodEnum>): Date {
    let internalDate = moment(date);
    for (const period of periods) {
      internalDate = internalDate.endOf(period);
    }
    return internalDate.toDate();
  }

  public shiftDate(date: Date, ...commands: Array<['startOf' | 'endOf', DatePeriodEnum]>): Date {
    let internalDate = moment(date);
    for (const [command, period] of commands) {
      if (command === 'startOf') {
        internalDate = internalDate.startOf(period);
      } else {
        internalDate = internalDate.endOf(period);
      }
    }
    return internalDate.toDate();
  }

  public getDateIterator(start: Date, end: Date, period: DateIntervalEnum = DateIntervalEnum.day): IterableIterator<Date> {
    return MomentDateTransformService.createDaysRange(start, end, period);
  }

  public getMonthWeekdays(date: Date): Array<Date> {
    const isoWeekday = moment(date).isoWeekday();
    let start = moment(this.startOf(date, DatePeriodEnum.month)).set({ isoWeekday }).toDate();
    if (!this.isSameMonth(start, date)) {
      start = this.increment(start, 1, DateIntervalEnum.week);
    }
    let end = moment(this.shiftDate(date, ['endOf', DatePeriodEnum.month], ['startOf', DatePeriodEnum.day])).set({ isoWeekday }).toDate();
    if (!this.isSameMonth(end, date)) {
      end = this.decrement(end, 1, DateIntervalEnum.week);
    }
    return Array.from(this.getDateIterator(start, end, DateIntervalEnum.week));
  }

  public increment(date: Date, value: number, period: DateIntervalEnum = DateIntervalEnum.day): Date {
    return moment(date).add(value, period).toDate();
  }

  public decrement(date: Date, value: number, period: DateIntervalEnum = DateIntervalEnum.day): Date {
    return moment(date).subtract(value, period).toDate();
  }

  public isSameDay(date1: Date, date2: Date): boolean {
    return moment(date1).format('YYYY-MM-DD') === moment(date2).format('YYYY-MM-DD');
  }

  public isSameMonth(date1: Date, date2: Date): boolean {
    return moment(date1).format('YYYY-MM') === moment(date2).format('YYYY-MM');
  }

  public isSameYear(date1: Date, date2: Date): boolean {
    return moment(date1).format('YYYY') === moment(date2).format('YYYY');
  }

  public isSameWeek(date1: Date, date2: Date): boolean {
    return moment(date1).format('WW') === moment(date2).format('WW');
  }

  public isSameWeekday(date1: Date, date2: Date): boolean {
    return moment(date1).isoWeekday() === moment(date2).isoWeekday();
  }

}
