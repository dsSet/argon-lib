# @argon/date-moment-bridge

MomentJS based implementation of @argon/date package. 

## Date Adapter

Package contains two different implementations of DateAdapter service.

 - UTCMomentDateAdapter - transform dates and ignore any timezone data from server side and client side.
 - TimezoneMomentDateAdapter - transform dates including timezone input
 
Providers:
 
````typescript
// Provide UTCMomentDateAdapter
import { UTCDateAdapterProviders } from '@argon/date-moment-bridge';

@NgModule({
  providers: [
    UTCDateAdapterProviders
  ]
})
````



````typescript
// Provide TimezoneMomentDateAdapter
import { TimezoneDateAdapterProviders } from '@argon/date-moment-bridge';

@NgModule({
  providers: [
    TimezoneDateAdapterProviders
  ]
})
````


> Date adapter providers already contains server date formats. To have working functionality you should also provide client formats with `ARGON_CLIENT_DATE_DEFAULT_FORMAT` and  `ARGON_CLIENT_DATE_FORMAT` or use one of client formats providers.



## Providers

 - `DateServicesProvider`: implementation of DateTransformService and DateFormatService services
 - `MDYClientDateFormatProviders`: `ARGON_CLIENT_DATE_DEFAULT_FORMAT` and `ARGON_CLIENT_DATE_FORMAT` values. Default format is `'MM.DD.YYYY'`
 - `DMYClientDateFormatProviders`: `ARGON_CLIENT_DATE_DEFAULT_FORMAT` and `ARGON_CLIENT_DATE_FORMAT` values. Default format is `'DD.MM.YYYY'`


## Constants
 
 - `CLIENT_MDY_FORMATS`: list of client input patterns in `'MM.DD.YYYY'` format
 - `CLIENT_DMY_FORMATS`: list of client input patterns in `'DD.MM.YYYY'` format




