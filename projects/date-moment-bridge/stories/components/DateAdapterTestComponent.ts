import { Component, Input } from '@angular/core';
import { DateAdapter } from '@argon/date';

@Component({
  selector: 'story-date-adapter-test',
  template: `
    <table class="table table-bordered">
      <thead>
        <tr>
          <td>Date</td>
          <td>Client</td>
          <td>Server</td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th colspan="3">Parse Server Date</th>
        </tr>
        <tr *ngFor="let date of serverDates">
          <td>
            {{ dateAdapter.serverToDate(date) }}
          </td>
          <td>
            {{ dateAdapter.serverToClient(date) }}
          </td>
          <td>
            <div>Input: {{ date }}</div>
            <div>Back conversion: {{ dateAdapter.dateToServer(dateAdapter.serverToDate(date)) }}</div>
          </td>
        </tr>
        <tr>
          <th colspan="3">Format Date</th>
        </tr>
        <tr>
          <td>
            {{ date }}
          </td>
          <td>
            {{ dateAdapter.dateToClient(date) }}
          </td>
          <td>
            {{ dateAdapter.dateToServer(date) }}
          </td>
        </tr>
        <tr>
          <th colspan="3">Parse Client Date</th>
        </tr>
        <tr *ngFor="let date of clientDates">
          <td>
            {{ dateAdapter.clientToDate(date) }}
          </td>
          <td>
            <div>Input: {{ date }}</div>
            <div>Back conversion: {{ dateAdapter.dateToClient(dateAdapter.clientToDate(date)) }}</div>
          </td>
          <td>
            {{ dateAdapter.clientToServer(date) }}
          </td>
        </tr>
      </tbody>
    </table>
  `
})
export class DateAdapterTestComponent {

  public serverDates = [
    '2020-07-23',
    '2020-07-23T00:00:00',
    '2020-07-23T00:00:00Z',
    '2020-07-23T00:00:00.0Z',
    '2020-07-23T00:00:00.00Z',
    '2020-07-23T00:00:00.000Z',
    '2020-07-23T00:00:00+00:00',
    '2020-07-23T00:00:00.0+00:00',
    '2020-07-23T00:00:00.00-03:00',
    '2020-07-23T00:00:00.000-05:00',
    '2020-07-23T00:00:00+0100',
    '2020-07-23T00:00:00.0+0700',
    '2020-07-23T00:00:00.00-0200',
    '2020-07-23T00:00:00.000+0000',
  ];

  public clientDates = [
    '03.15.2020',
    '03-15-2020',
    '03/15/2020',
  ];

  public date = new Date();

  constructor(
    public dateAdapter: DateAdapter
  ) { }

}
