import { Component, Input } from '@angular/core';
import { DateAdapter, HasDateAdapterInterface, DateTransform, DateTransformArgument } from '@argon/date';

@Component({
  selector: 'story-date-transform-decorator-test',
  template: `
    {{ date }}
    {{ date2 }}
    <div (click)="handleDate(a, 1, 2, 3, 4)">Method Test (set {{ a | json }} date)</div>
  `
})
export class DateTransformDecoratorTestComponent implements HasDateAdapterInterface {

  @Input() @DateTransform('client') date: Date | string;
  @Input() @DateTransform('client') date2: Date | string;

  public a = new Date();

  constructor(
    public dateAdapter: DateAdapter
  ) { }

  @DateTransformArgument('server')
  public handleDate(date: Date | string, ...args) {
    this.date = date;
    console.log(date, this.date);
  }
}
