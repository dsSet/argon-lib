import { moduleMetadata, storiesOf } from '@storybook/angular';
import { MDYClientDateFormatProviders, UTCDateAdapterProviders, TimezoneDateAdapterProviders } from './index';

import { DateAdapterTestComponent } from './components/DateAdapterTestComponent';
import { DateTransformDecoratorTestComponent } from './components/DateTransformDecoratorTestComponent';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('../docs/index.md').default + require('@argon/date/docs/index.md').default
};

storiesOf('Argon|Date/Moment Date', module)
  .addDecorator(
      moduleMetadata({
        providers: [
          MDYClientDateFormatProviders
        ],
        declarations: [
          DateAdapterTestComponent,
          DateTransformDecoratorTestComponent
        ]
      })
    )
    .add('Moment Date Adapter (UTC only)', () => ({
      component: DateAdapterTestComponent,
      moduleMetadata: {
        providers: [ UTCDateAdapterProviders ]
      }
    }), { notes })
    .add('Moment Date Adapter (with timezones)', () => ({
      component: DateAdapterTestComponent,
      moduleMetadata: {
        providers: [ TimezoneDateAdapterProviders ]
      }
    }), { notes });
