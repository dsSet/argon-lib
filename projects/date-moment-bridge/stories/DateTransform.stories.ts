import { moduleMetadata, storiesOf } from '@storybook/angular';
import {
  MDYClientDateFormatProviders, UTCDateAdapterProviders
} from './index';

import { DateTransformDecoratorTestComponent } from './components/DateTransformDecoratorTestComponent';

import '!!style-loader!css-loader!sass-loader!./styles.scss';

const notes = {
  markdown: require('@argon/date/docs/index.md').default
};

storiesOf('Argon|Date', module)
  .addDecorator(
      moduleMetadata({
        providers: [
          MDYClientDateFormatProviders,
          UTCDateAdapterProviders
        ],
        declarations: [
          DateTransformDecoratorTestComponent
        ]
      })
    )
    .add('Transform Properties (@DateTransform, @DateTransformArgument)', () => ({
      props: {
        date: '2/3/18',
        date2: '3/4/19',
      },
      template: `
        <p> original client value: {{ date }}</p>
        <story-date-transform-decorator-test [date]="date" [date2]="date2"></story-date-transform-decorator-test>
        <story-date-transform-decorator-test [date]="date2"></story-date-transform-decorator-test>
      `
    }), { notes });
