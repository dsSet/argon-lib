import { ComponentFixture } from '@angular/core/testing';
import Matchers = jasmine.Matchers;

export class HostBindingTestHelper {

  static testToggleClass<T>(component: T, fixture: ComponentFixture<T>, propertyName: string, className: string) {
    component[propertyName] = false;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, className).toBeFalsy();

    component[propertyName] = true;
    fixture.detectChanges();
    HostBindingTestHelper.testHasClassName(fixture, className).toBeTruthy();
  }

  static testPropertyRoleAttribute<T>(component: T, fixture: ComponentFixture<T>, propertyName: string) {
    component[propertyName] = 'FAKE_ROLE';
    expect(fixture.nativeElement.getAttribute('role')).toEqual('FAKE_ROLE');
  }

  static testRoleAttribute<T>(component: T, fixture: ComponentFixture<T>, role: string) {
    fixture.detectChanges();
    expect(fixture.nativeElement.getAttribute('role')).toEqual(role);
  }

  static testHasClassName<T>(fixture: ComponentFixture<T>, className: string): Matchers<any> {
    return expect(fixture.nativeElement.classList.contains(className));
  }

}
