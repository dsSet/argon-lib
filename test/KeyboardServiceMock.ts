import { Subject } from 'rxjs';

import createSpy = jasmine.createSpy;

export class KeyboardServiceMock {

  listenKeyDownEventSubject = new Subject<KeyboardEvent>();

  listenHotKeySubject = new Subject<KeyboardEvent>();

  createFromSourceSubject = new Subject<KeyboardEvent>();

  createFromSource = createSpy('createFromSource').and.returnValue(this.createFromSourceSubject);

  listenKeyDownEvent = createSpy('listenKeyDownEvent').and.returnValue(this.listenKeyDownEventSubject);

  listenHotKey = createSpy('listenHotKey').and.returnValue(this.listenHotKeySubject);

  dispose() {
    this.listenKeyDownEventSubject.complete();
    this.listenHotKeySubject.complete();
    this.createFromSourceSubject.complete();
  }

}
