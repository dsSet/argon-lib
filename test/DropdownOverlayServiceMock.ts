import { BehaviorSubject, Subject } from 'rxjs';
import { ElementRef } from '@angular/core';

// @ts-ignore
import createSpy = jasmine.createSpy;

export class DropdownOverlayServiceMock {

  registerDropdownSubject = new Subject<boolean>();

  registerDropdown = createSpy('registerDropdown').and.returnValue(this.registerDropdownSubject);

  stateChange = new BehaviorSubject<ElementRef<any> | null>(null);

  isMobile = new BehaviorSubject<boolean>(false);

  removeDropdown = createSpy('removeDropdown');

  createDropdown = createSpy('createDropdown');

  dispose() {
    this.registerDropdownSubject.complete();
    this.stateChange.complete();
    this.isMobile.complete();
  }

}
