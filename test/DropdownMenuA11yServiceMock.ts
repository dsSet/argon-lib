// @ts-ignore
import createSpy = jasmine.createSpy;

export class DropdownMenuA11yServiceMock {

  registerItems = createSpy('registerItems');

  selectItem = createSpy('selectItem');

  dispose() {

  }
}
