import { BehaviorSubject } from 'rxjs';


export class BreakpointServiceMock {

  isPhone = new BehaviorSubject<boolean>(false);

  isTablet = new BehaviorSubject<boolean>(false);

  isDesktop = new BehaviorSubject<boolean>(false);

  dispose() {
    this.isPhone.complete();
    this.isTablet.complete();
    this.isDesktop.complete();
  }

}
